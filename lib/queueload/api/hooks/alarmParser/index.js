/**
 * AlarmParser hook
 */

/* FileName: index.js
 *
 * @description: This file describes the hook to parse alerts
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 19/05/2017        Initial file creation                                       PPani
 * 23/05/2017        Adding the message handling and incident creation           SMandal
 *
 */
var AlarmService = require("../../services/utils/alarmservice.js");
var slackUtils = require("../../services/utils/slackutils.js");
var userUtils = require("../../services/utils/user.js");
var RSMQWorker = require( "rsmq-worker" );
var promise = require('bluebird');
var RedisMQ = require("rsmq");
var rsmqOptions = { ns: "rsmq" };
var enums = require('../../resources/Enums.js');
var _ = require('lodash');
var sg = require('sendgrid')(sails.config.sendgrid.apikey);
if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'test')
{
  rsmqOptions.host = sails.config.connections.localRefTknStore.host;
  rsmqOptions.port = sails.config.connections.localRefTknStore.port;
}
else if(process.env.NODE_ENV === 'production')
{
  rsmqOptions.host = sails.config.connections.awsRefTknStore.host;
  rsmqOptions.port = sails.config.connections.awsRefTknStore.port;
}
else
{
  sails.log.error("No environment set!!");
  process.exit(1);
}

var rsmq = new RedisMQ(rsmqOptions);
promise.promisifyAll(rsmq);

module.exports = function alarmParser(sails) {
  /**
   * Module dependencies.
   */
  var messageDeleteWindow = 120000;
  var messageTooOld = 30000;

  var msgQName = process.env.ACCOUNTID + "-alarms";
  var dlQName = process.env.ACCOUNTID + "-alarms-deadletter";

  var exdCheckFn = function(msg)
  {
    if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
    {
      return false;
    }
    else if(msg.sent < (Math.floor(new Date) - messageTooOld))
    {
      //Console.log("[excdfn] Sending message to DL");
      rsmq.sendMessageAsync({ qname: dlQName, message: msg.message })
      .then(function(id)
      {
        if(id)
        {
          //Console.log("[excdfn] Deleting message from main queue");
          return false;
        }
        else
        {
          return true;
        }
      })
      .catch(function()
      {
        return true;
      });
    }
    else
    {
      return true;
    }
  };

  var options = {
      timeout: 1,
      interval: [ 0.1, 0.3, 0.5, 1 ],
      invisibletime: 900, // Hide received message for 900 sec
      maxReceiveCount: 1, // Only receive a message once until delete
      autostart: true, // Start worker on init
      customExceedCheck: exdCheckFn,
      rsmq: rsmq
  };

  /**
   * Expose hook definition
   */

  return {
    // Run when sails loads-- be sure and call `next()`.
    initialize: function(next) {
      sails.after('hook:orm:loaded', function() {
        var msgWorker = new RSMQWorker(msgQName, options);

        msgWorker.on("data", function(msg)
        {
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            return exdCheckFn(msg);
          }
          else if(msg.sent < (Math.floor(new Date) - messageTooOld))
          {
            //Console.log("Directly calling exceed");
            return exdCheckFn(msg);
          }
          else if(msg.rc > options.maxReceiveCount)
          {
            //Console.log("[data] Message has already been proceed.");
            return;
          }
          else
          {
            if(!AlarmService.createAlarm(JSON.parse(msg.message)))
            {
              return;
            }
            else
            {
              var newIncident = {};
              var affectedService = {};
              var affectedCiId, ciAddress;
              var intConfig = {};
              var alarm = {};
              var needInc = false;
              var parsedMessage = JSON.parse(msg.message);
              if(parsedMessage.summary.length > 8192)
              {
                parsedMessage.summary = parsedMessage.summary.slice(0, 8191);
              }
              var openAlarms = [];
              Alarm.create(parsedMessage)
              .then(function(result)
              {
                alarm = result;
                if(process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'staging')
                {
                  Alarm.publishCreate(result);
                }
                return rsmq.sendMessageAsync({ qname: dlQName, message: msg.message });
              })
              .then(function(message)
              {
                return rsmq.deleteMessageAsync({ qname: msgQName, id: msg.id });
              })
              .then(function(message)
              {
                return AlarmService.isAnIncident(JSON.parse(msg.message));
              })
              .then(function(response)
              {
                if(response)
                {
                  needInc = response;
                }
                var pService = Service.findOne({ servicename: JSON.parse(msg.message).service });
                var pIntConfig = Integrationconfig.findOne({ select: [ "config" ] }).where({ integrationtype: JSON.parse(msg.message).source });
                var promises = [ pService, pIntConfig ];
                if(JSON.parse(msg.message).ci)
                {
                  promises.push(Cidetail.findOne({ ciname: JSON.parse(msg.message).ci }));
                }
                return promise.all(promises);
              })
              .then(function(results)
              {
                if(!results[ 0 ])
                {
                  //Sails.log.info("The service doesn't exist");
                  return;
                }
                else if(!results[ 0 ].supportqueue)
                {
                  //Sails.log.info("The service doesn't have a support queue");
                  return;
                }
                else if(!results [ 1 ])
                {
                  //Sails.log.info("Not creating incident as service has not been integrated with Queueload");
                  return;
                }
                else
                {
                  affectedService = results[ 0 ];
                  intConfig = results[ 1 ];
                  if(JSON.parse(msg.message).ci && results[ 2 ])
                  {
                    affectedCiId = results[ 2 ].ciid;
                    ciAddress = results[ 2 ].ipaddress;
                  }
                  var pInc = Incident.find().where({ state: { '!': [ enums.incidentStates.CLOSED ] } }).where({ reportertype: { '!': [ enums.incReporterType.USER ] } }).where({ serviceid: affectedService.serviceid }).where({ ciid: affectedCiId }).populate('alarmids');
                  var pAlarm = Alarm.find({ isack: false, ci: JSON.parse(msg.message).ci, service: JSON.parse(msg.message).service, incidentid: null });
                  return promise.all([ pInc, pAlarm ]);
                }
              })
              .then(function(result)
              {
                if(Array.isArray(result))
                {
                  if(intConfig.config && !result[ 0 ].length && needInc)
                  {
                    //Sails.log.info("No auto created open incidents for this service and ci combination. Creating a new incident");
                    var reportertype;
                    switch(JSON.parse(msg.message).source)
                    {
                      case enums.integrationService.CLOUDWATCH:
                        reportertype = enums.incReporterType.CLOUDWATCH;
                        break;
                      case enums.integrationService.SENTRY:
                        reportertype = enums.incReporterType.SENTRY;
                        break;
                      case enums.integrationService.WEAVED:
                        reportertype = enums.incReporterType.WEAVED;
                        break;
                      case enums.integrationService.NAGIOS:
                        reportertype = enums.incReporterType.NAGIOS;
                        break;
                      case enums.integrationService.LOGGLY:
                        reportertype = enums.incReporterType.LOGGLY;
                        break;
                      case enums.integrationService.CUSTOM:
                        reportertype = enums.incReporterType.CUSTOM;
                        break;
                    }
                    openAlarms = result[ 1 ];
                    var localOpenAlarms = _.map(openAlarms, 'id');
                    localOpenAlarms.push(alarm.id);
                    var pArray = [ AlarmService.createIncident(JSON.parse(msg.message), affectedService, intConfig, affectedCiId, reportertype, localOpenAlarms) ];
                    pArray.push(Queue.findOne({ queueid: affectedService.supportqueue }));
                    return promise.all(pArray);
                  }
                  else if(intConfig.config && (result[ 0 ].length || needInc))
                  {
                    //Sails.log.info("Found", result[ 0 ].length, "auto created open incidents for this service and ci combination. Not creating new incident");
                    openAlarms = result[ 1 ];
                    var currentAlarms = [];
                    currentAlarms = _.map(result[ 0 ][ 0 ].alarmids, 'id');
                    currentAlarms.push(alarm.id);
                    var localOpenAlarms = _.map(openAlarms, 'id');
                    var newAlarms = _.uniq(_.concat(currentAlarms, localOpenAlarms));
                    var pIncUpdate = Incident.update({ incidentid: result[ 0 ][ 0 ].incidentid }, { alarmids: newAlarms });
                    localOpenAlarms.push(alarm.id);
                    var pAlarmUpd = Alarm.update({ id: _.uniq(localOpenAlarms) }, { incidentid: result[ 0 ][ 0 ].incidentid });
                    return promise.all([ pIncUpdate, pAlarmUpd ]);
                  }
                }
                else
                {
                  return;
                }
              })
              .then(function(result)
              {
                if(Array.isArray(result))
                {
                  if(result[ 1 ].queueid)
                  {
                    newIncident = result[ 0 ];
                    newIncident.assignedqueuename = result[ 1 ].queuename;
                    newIncident.queueemail = result[ 1 ].queueemail;
                    var pPubInc = Incident.publishCreate(result[ 0 ]);
                    var pFindSlack = Integrationconfig.findOne({ integrationtype: enums.integrationService.SLACK });
                    var pFindIncident = Incident.find().where({ state: { '!': [ enums.incidentStates.CLOSED ] } }).where({ incidentid: { '!': [ newIncident.incidentid ] } }).where({ serviceid: newIncident.serviceid });
                    var localOpenAlarms = _.map(openAlarms, 'id');
                    localOpenAlarms.push(alarm.id);
                    var pAlarmUpdate = Alarm.update({ id: _.uniq(localOpenAlarms) }, { incidentid: result[ 0 ].incidentid });
                    var promises = [ pPubInc, pFindSlack, pFindIncident, pAlarmUpdate ];
                    var pGetUserDetails = userUtils.getUser([ intConfig.config.owneruserid ]);
                    promises.push(pGetUserDetails);
                    return promise.all(promises);
                  }
                  else if(result[ 1 ][ 0 ].id)
                  {
                    var alarmsToPublish = _.map(result[ 1 ], 'id');
                    alarmsToPublish.forEach(function(id, index)
                    {
                      Alarm.publishUpdate(id, result[ 1 ][ index ]);
                    });
                    result[ 0 ][ 0 ].newAlarm = result[ 1 ][ 0 ];
                    Incident.publishUpdate(result[ 0 ][ 0 ].incidentid, result[ 0 ][ 0 ]);
                    return;
                  }
                }
                else
                {
                  return;
                }
              })
              .then(function(result)
              {
                if(Array.isArray(result))
                {
                  var alarmsToPublish = _.map(result[ 3 ], 'id');
                  alarmsToPublish.forEach(function(id, index)
                  {
                    Alarm.publishUpdate(id, result[ 3 ][ index ]);
                  });
                  var enumToString = function(enums, value)
                  {
                    for (var k in enums)
                    {
                      if (enums[ k ] === value)
                        return k;
                    }
                    return null;
                  };
                  var incSev = enumToString(enums.incidentSeverity, newIncident.severity);
                  var padding = function(number, width, paddingchar)
                  {
                    paddingchar = paddingchar || '0';
                    number = number + '';
                    return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
                  };
                  //Create a payload and send notifications
                  var date = new Date(JSON.parse(msg.message).reportedAt * 1000);
                  var dateofmonth = "0" + date.getDate();
                  var month = "0" + (date.getMonth() + 1);
                  var year = date.getFullYear();
                  var hours = "0" + date.getHours();
                  var minutes = "0" + date.getMinutes();
                  var seconds = "0" + date.getSeconds();
                  var inclist = '';
                  var padding = function(number, width, paddingchar)
                  {
                    paddingchar = paddingchar || '0';
                    number = number + '';
                    return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
                  };
                  for(var iter = 0; iter < result[ 2 ].length; iter++)
                  {
                    inclist = inclist + "INC" + padding(result[ 2 ][ iter ].incidentid, 5, 0) + '\n';
                  }
                  var pNotifications = [];
                  if(result[ 1 ] && result[ 1 ].config.slackincomingwebhookurl)
                  {
                    var message =
                    {
                      "attachments":
                      [
                        {
                          "fallback": "[New Incident] INC" + padding(newIncident.incidentid, 5, 0),
                          "color": "#663399",
                          "pretext": "Incident details: INC" + padding(newIncident.incidentid, 5, 0),
                          "title": newIncident.title,
                          "title_link": "https://app.queueload.com/",
                          "text": newIncident.description,
                          "fields":
                          [
                            {
                              "title": "Service Name",
                              "value": JSON.parse(msg.message).service,
                              "short": true
                            },
                            {
                              "title": "Severity",
                              "value": incSev.charAt(0).toUpperCase() + incSev.substr(1).toLowerCase(),
                              "short": true
                            },
                            {
                              "title": "Start Time",
                              "value": dateofmonth.substr(-2) + "/" + month.substr(-2) + "/" + year + " " + hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2) + " GMT",
                              "short": true
                            },
                            {
                              "title": "Other active incidents of the service",
                              "value": inclist || "No other active incidents",
                              "short": true
                            },
                            {
                              "title": "Configuration Item",
                              "value": JSON.parse(msg.message).ci || "Not assigned",
                              "short": true
                            },
                            {
                              "title": "IP Address",
                              "value": ciAddress || "NA",
                              "short": true
                            },
                            {
                              "title": "Assigned Queue",
                              "value": newIncident.assignedqueuename,
                              "short": true
                            },
                            {
                              "title": "Assigned User",
                              "value": "Not assigned",
                              "short": true
                            }
                          ]
                        }
                      ]
                    };
                    pNotifications.push(slackUtils.notifySlack(message, result[ 1 ].config.slackincomingwebhookurl));
                  }
                  var custEmail = sg.emptyRequest({
                    method: 'POST',
                    path: '/v3/mail/send',
                    body: {
                    personalizations: [
                      {
                        to: [
                          {
                            email: newIncident.queueemail
                          }
                        ],
                        'substitutions': {
                          '-servicename-': JSON.parse(msg.message).service,
                          '-severity-': incSev.charAt(0).toUpperCase() + incSev.substr(1).toLowerCase(),
                          '-starttime-': dateofmonth.substr(-2) + "/" + month.substr(-2) + "/" + year + " " + hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2) + " GMT",
                          '-inclist-': inclist || "No other active incidents",
                          '-ci-': JSON.parse(msg.message).ci || "Not assigned",
                          '-ipaddress-': ciAddress || "NA",
                          '-assignedqueue-': newIncident.assignedqueuename,
                          '-assigneduser-': "Not assigned"
                        },
                        subject: '[QueueLoad] INC' + padding(newIncident.incidentid, 5, 0) + ' has been created'
                      }
                    ],
                    from: {
                      email: 'noreply@queueload.com'
                    },
                    'template_id': '02d922b7-2077-4240-8b39-18b4cc16ff37'
                    }
                  });
                  sails.log.info("Notifying the customer in email about the ticket");
                  pNotifications.push(sg.API(custEmail));
                  return promise.all(pNotifications);
                }
              })
              .catch(function(error)
              {
                sails.log.error("[alarmParser] error", error);
                return next(error);
              });
            }
          }
        });

        msgWorker.on('error', function(err, msg) {
            //Handle error
        });
        msgWorker.on('exceeded', function(msg) {
            //Console.log("Exceeded on event");
            //This one will delete the message regardless of what you do here
        });
        msgWorker.on('timeout', function(msg) {
            //Handle timeout
        });
        msgWorker.start();

        var dlExdCheckFn = function(msg)
        {
          //Console.log("[dlExdCheckFn] msg", msg);
          if(msg.sent < (Math.floor(new Date) - messageDeleteWindow))
          {
            //Console.log("[dlExdCheckFn] Deleting message");
            return false;
          }
          else
          {
            //Console.log("[dlExdCheckFn] Retaining message");
            return true;
          }
        };

        var dlOptions =
        {
          timeout: 1,
          interval: [ 0.1, 0.3, 0.5, 1 ],
          invisibletime: 60,                     // Hide received message for 6 hours
          maxReceiveCount: 1,                    // Only receive a message once until delete
          autostart: true,                       // Start worker on init
          customExceedCheck: dlExdCheckFn
        };

        var dlWorker = new RSMQWorker( dlQName, dlOptions);

        dlWorker.on("message", function(dlmsg, dlnext, dlid)
        {
          //Console.log("[dl] received message", dlid);
          dlnext({ delete: false });
        });

        // Optional error listeners
        dlWorker.on('error', function( err, dlmsg )
        {
            //Console.log( "[dl] ERROR", err, dlmsg.id );
        });
        dlWorker.on('exceeded', function( dlmsg )
        {
            //Console.log( "[dl] EXCEEDED", dlmsg.id );
        });
        dlWorker.on('timeout', function( dlmsg )
        {
            //Console.log( "[dl] TIMEOUT", dlmsg.id, dlmsg.rc );
        });

        dlWorker.start();
      });
      return next();
    }
  };
};
