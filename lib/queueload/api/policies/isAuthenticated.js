/**
 * FileName: isAuthenticated
 * @description :: Policy to verify authenticity of an user via JSON Web Token
 */
var errorCode = require("../resources/ErrorCodes.js");
var bluebird = require("bluebird");
var redis = require("redis");
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

module.exports = function(req, res, next)
{
  if(sails.config.environment === 'production' && !req.isSocket && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json") && !req.headers[ 'content-type' ].includes("application/x-www-form-urlencoded")))
  {
    var error = new Error();
    error.code = 400051;
    error.message = errorCode [ '400051' ];
    return res.negotiate(error);
  }
  else if (req.body.requesterid)
  {
    var error = new Error();
    error.code = 400051;
    error.message = errorCode [ '400051' ];
    return res.negotiate(error);
  }
  else if(req.body.userroles)
  {
    var error = new Error();
    error.code = 400051;
    error.message = errorCode [ '400051' ];
    return res.negotiate(error);
  }
  else if(req.body.accountid)
  {
    var error = new Error();
    error.code = 400051;
    error.message = errorCode [ '400051' ];
    return res.negotiate(error);
  }
  else if(!req.headers.authorization)
  {
    var error = new Error();
    error.code = 400051;
    error.message = errorCode [ '400051' ];
    return res.negotiate(error);
  }
  else
  {
    CipherService.verifyAccToken(req.headers.authorization, function(err, payload)
    {
      if (err)
      {
        var error = new Error();
        if(err.name === "TokenExpiredError")
        {
          error.code = 401505;
          error.message = errorCode[ "401505" ];
        }
        else
        {
          error.code = 401503;
          error.message = errorCode[ "401503" ];
        }
        return res.negotiate(error);
      }
      return client.hexistsAsync(process.env.ACCOUNTID + ":" + payload.user.emailid, "signedout")
      .then(function(result)
      {
        if(result)
        {
          var error = new Error();
          error.code = 401503;
          error.message = errorCode [ '401503' ];
          throw error;
        }
        req.body.requesterid = payload.user.userid;
        req.body.accountid = payload.user.accountid;
        return Role.findOne({ select: [ 'issuperuser', 'isbdadmin', 'isoperator' ], where: { userid: req.body.requesterid } });
      })
      .then(function(roles)
      {
        if(!roles)
        {
          return User.update({ userid: req.body.requesterid }, { isactive: false });
        }
        else
        {
          return roles;
        }
      })
      .then(function(result)
      {
        if(result.length)
        {
          return client.multi()
          .hmset(process.env.ACCOUNTID + ":" + payload.user.emailid, [ "signedout", true ])
          .hdel(process.env.ACCOUNTID + ":" + payload.user.emailid, [ "reftkn" ])
          .EXPIRE(process.env.ACCOUNTID + ":" + payload.user.emailid, sails.config.authcfg.accExpiresIn)
          .execAsync();
        }
        else
        {
          return result;
        }
      })
      .then(function(result)
      {
        if(result.length)
        {
          var error = new Error();
          error.code = 500001;
          error.message = errorCode[ "500001" ];
          throw error;
        }
        else
        {
          req.body.requesterroles = result;
          return next();
        }
      })
      .catch(function(error)
      {
        return next(error);
      });
    });
  }
};
