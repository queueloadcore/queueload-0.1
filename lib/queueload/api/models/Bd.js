/* FileName: Bd.js
 *
 * @description: This file describes the attributes of a Business Department.
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 10/12/2015        Initial file creation                                       SMandal
 *
 */

module.exports =
{

  tableName: "businessdepartments",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    bdid:
    {
        type: "integer",
        autoIncrement: true,
        primaryKey: true
    },
    bdname:
    {
        type: "string",
        required: true,
        minLength: 4,
        maxLength: 100,
        alphaNumericDotDashSpace: true,
        unique: true
    },
    isactive:
    {
        type: "boolean",
        defaultsTo: false,
        required: true
    },
    admins:
    {
        collection: "user",
        via: "administeredbds"
    },
    cbcodes:
    {
        collection: "cb",
        via: "bdid"
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
        var obj = this.toObject();
        delete obj.createdAt;
        delete obj.updatedAt;
        return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
