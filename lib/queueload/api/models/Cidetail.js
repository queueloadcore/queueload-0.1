/* FileName: Cidetail.js
* @description: This file describes the details of a CI.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 01/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "cidetails",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    ciid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    ciname:
    {
      type: "string",
      unique: true,
      required: true,
      minLength: 3,
      maxLength: 128,
      alphaNumericDotDash: true
    },
    assettag:
    {
      type: "string",
      unique: true,
      required: true,
      minLength: 3,
      maxLength: 128,
      alphaNumericDotDash: true
    },
    cistateid:
    {
      type: "integer",
      required: true,
      defaultsTo: 1
    },
    citypeid:
    {
      model: "citype",
      required: true
    },
    envid:
    {
      model: "cienvironment",
      required: true
    },
    //Supportqueue:
    //{
    //  model: "queue"
    //},
    contractid:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDash: true
    },
    vendorid:
    {
      model: "vendor",
      required: true
    },
    warrantyexpiry:
    {
      type: 'integer',
      min: 1
    },
    maintenanceend:
    {
      type: 'integer',
      min: 1
    },
    customerfacing:
    {
      type: "boolean",
      defaultsTo: false
    },
    isundermaint:
    {
      type: "boolean",
      required: true,
      defaultsTo: true
    },
    machineid:
    {
      model: "cihardware"
    },
    serviceids:
    {
      collection: "service",
      via: "ciids"
    },
    systemsoftware1:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    systemsoftware2:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    systemsoftware3:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    customapp1:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    customapp2:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    customapp3:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    ciownerid:
    {
      model: "user"
    },
    locationid:
    {
      model: "cilocation",
      required: true
    },
    installationdate:
    {
      type: 'integer',
      min: 1
    },
    isfrozen:
    {
      type: "boolean",
      required: true,
      defaultsTo: false
    },
    freezecause:
    {
      type: "integer",
      defaultsTo: 0
    },
    incidents:
    {
      collection: "incident",
      via: "ciid"
    },
    ipaddress:
    {
      type: "string",
      ip: true
    },
    primaryservice:
    {
      model: "service"
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
