/* FileName: Incident.js
* @description: This file describes the attributes of an Incident.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 15/10/2016        Initial file creation                                       SMandal
*
*/

module.exports = {

  tableName: 'incidents',

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    incidentid:
    {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    state:
    {
      type: 'integer',
      min: 1,
      max: 11,
      required: true,
      defaultsTo: 2
    },
    title:
    {
      type: 'string',
      required: true,
      maxLength: 128
    },
    description:
    {
      type: 'string',
      required: true,
      maxLength: 8192
    },
    serviceid:
    {
      model: 'service',
      required: true
    },
    ciid:
    {
      model: 'cidetail'
    },
    ownerqueueid:
    {
      model: 'queue',
      required: true
    },
    owneruserid:
    {
      model: 'user',
      required: true
    },
    assignedqueueid:
    {
      model: 'queue',
      required: true
    },
    assigneduserid:
    {
      model: 'user'
    },
    startedat:
    {
      type: 'integer',
      required: true,
      min: 1
    },
    endedat:
    {
      type: 'integer',
      min: 1
    },
    severity:
    {
      type: 'integer',
      min: 1,
      max: 3,
      required: true,
      defaultsTo: 3
    },
    impact:
    {
      type: 'integer',
      min: 1,
      max: 4
    },
    vendorid:
    {
      model: 'vendor'
    },
    vendorsr:
    {
      type: 'string'
    },
    pendinginc:
    {
      model: 'incident'
    },
    attachments:
    {
      collection: 'attachment',
      via: 'attachmentid'
    },
    journal:
    {
      type: 'json',
      required: true
    },
    // Not supported at the moment
    //relatedinc:
    //{
    //  collection: 'incident',
    //  via: 'incidentid',
    //  dominant: true
    //},
    resolutioncode:
    {
      type: 'integer',
      min: 1,
      max: 3
    },
    resolutionnotes:
    {
      type: 'string',
      maxLength: 8192
    },
    closurecomments:
    {
      type: 'string',
      maxLength: 8192
    },
    visibility:
    {
      type: 'string',
      enum: [ 'owner', 'operator' ],
      defaultsTo: 'operator'
    },
    resolvedat:
    {
      type: 'integer',
      min: 1
    },
    rating:
    {
      type: 'integer',
      min: 1,
      max: 3
    },
    dupticket:
    {
      model: 'incident'
    },
    logglylogs:
    {
      type: 'json'
    },
    reportertype:
    {
      type: 'integer',
      defaultsTo: 1
    },
    alarmids:
    {
      collection: 'alarm',
      via: 'id'
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
