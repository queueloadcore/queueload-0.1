/* FileName: Company.js
* @description: This file describes the attributes of a type of the company.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 25/11/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "companies",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    accountid:
    {
      type: 'string',
      primaryKey: true
    },
    name:
    {
      type: 'string',
      required: true,
      minLength: 3,
      maxLength: 64,
      unique: true
    },
    accntadmin:
    {
      model: "user",
      required: true
    },
    supportmgr:
    {
      type: "email",
      maxLength: 254
    },
    isactive:
    {
      type: "boolean",
      defaultsTo: true,
      required: true
    },
    globalconfig:
    {
      type: 'json'
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.awsexteranlid;
      delete obj.awsarn;
      delete obj.qlawsaccountid;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};

