/* FileName: User.js
* @description: This file describes the attributes of an USER which includes all types of users of this tool.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 06/12/2015        Initial file creation                                       SMandal
*
*/

var Promise = require("bluebird");
var PromisedCipherService = Promise.promisifyAll(require("../services/CipherService.js")); //Promisifying the CipherService functions

var pattern = require("../resources/ValidationPatterns.js");

module.exports =
{

  tableName: "users",
  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    userid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    fullname:
    {
      type: "string",
      maxLength: 64,
      alphaDotSpace: true
    },
    username:
    {
      type: "string",
      required: true,
      minLength: 8,
      maxLength: 64,
      alphaNumericDotDash: true,
      unique: true
    },
    password:
    {
      type: "string",
      required: true,
      minLength: 8,
      maxLength: 64
    },
    emailid:
    {
      type: "email",
      required: true,
      maxLength: 254,
      unique: true
    },
    countrycode:
    {
      type: "string",
      minLength: 2,
      maxLength: 4
    },
    phonenumber:
    {
      type: "string",
      minLength: 10,
      maxLength: 10,
      numeric: true
    },
    employeeid:
    {
      type: "string",
      minLength: 1,
      maxLength: 16,
      alphanumeric: true,
      unique: true
    },
    administeredbds:
    {
      collection: "bd",
      via: "admins"
    },
    ownedcbs:
    {
      collection: "cb",
      via: "cbownerid"
    },
    isactive:
    {
      type: "boolean",
      defaultsTo: true,
      required: true
    },
    passexpired:
    {
      type: 'boolean',
      defaultsTo: false
    },
    queuesenrolled:
    {
      collection: "queue",
      via: "members"
    },
    /* Attributes withheld until introduction of managers
    incmgrqueues:
    {
      collection: "queue",
      via: "incidentmanagerid"
    },
    chgmgrqueues:
    {
      collection: "queue",
      via: "changemanagerid"
    },
    srtmgrqueues:
    {
      collection: "queue",
      via: "srtmanagerid"
    },
    */
    servicestateapprovalids:
    {
      collection: "serviceapproval",
      via: "approverid"
    },
    roles:
    {
      collection: "role",
      via: "userid"
    },
    defaultqueue:
    {
      model: 'queue'
    },
    accountid:
    {
      model: 'company',
      required: true
    },
    userconfig:
    {
      type: 'json'
    },
    fullphonenumber: function()
    {
      return "(" + this.countrycode + ")" + " " + this.phonenumber;
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.password;
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  },

  types:
  {
    alphaNumericDotDash: function(attr)
    {
      return pattern.alphaNumericDotDash.test(attr);
    },
    alphaNumericDotDashSpace: function(attr)
    {
      return pattern.alphaNumericDotDashSpace.test(attr);
    },
    alphaSpace: function(attr)
    {
      return pattern.alphaSpace.test(attr);
    },
    macaddress: function(attr)
    {
      return pattern.macaddress.test(attr);
    },
    rackUnitLimit: function(attr)
    {
      if(attr > 0 && attr < 8193)
        return true;
      else
        return false;
    },
    alphaDotSpace: function(attr)
    {
      return pattern.alphaDotSpace.test(attr);
    }
  },

  beforeCreate: function(values, next)
  {
    PromisedCipherService.hashPasswordAsync(values.password)
    .then(function assignPasswordHash(hashedPass)
    {
      values.password = hashedPass;
      var error = new Error();
      if((values.countrycode && !values.phonenumber) || (!values.countrycode && values.phonenumber))
      {
        error.code = "E_UNIQUE";
        error.mode = "User";
        error.invalidAttributes = { phonenumber: values.phonenumber, countrycode: values.countrycode };
        error.status = 400;
        throw error;
      }
      next();
    })
    .catch(function(err) {next(err);});
  },

  beforeUpdate: function(values, next)
  {
    var error = new Error();
    if(values.countrycode !== undefined || values.phonenumber !== undefined)
    {
      if(values.countrycode !== undefined && values.countrycode === "" && values.phonenumber !== undefined && values.phonenumber !== "")
      {
        error.code = "E_UNIQUE";
        error.mode = "User";
        error.invalidAttributes = { countrycode: values.countrycode };
        error.status = 400;
      }
      if(values.phonenumber !== undefined && values.phonenumber === "" && values.countrycode !== undefined && values.countrycode !== "")
      {
        error.code = "E_UNIQUE";
        error.mode = "User";
        error.invalidAttributes = { phonenumber: values.phonenumber };
        error.status = 400;
      }
      if(error.code) next(error);
      else next();
    }
    else { next(); }
  }
};
