/* FileName: Service.js
*
* @description: This file describes attributes of a service.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 01/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{
  tableName: "services",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    serviceid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    servicename:
    {
      type: "string",
      required: true,
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true,
      unique: true
    },
    cbid:
    {
      model: "cb",
      required: true
    },
    servicestateid:
    {
      type: 'integer',
      required: true,
      defaultsTo: 1
    },
    details:
    {
      type: "string",
      maxLength: 200
    },
    ciids:
    {
      collection: "cidetail",
      via: "serviceids"
    },
    crntsrvsundappvl:
    {
      collection: "serviceapproval",
      via: "crntsrvids"
    },
    newsrvsundappvl:
    {
      collection: "serviceapproval",
      via: "newserviceids"
    },
    serviceidsforapproval:
    {
      collection: "serviceapproval",
      via: "approverserviceid"
    },
    isfrozen:
    {
      type: "boolean",
      defaultsTo: false
    },
    freezecause:
    {
      type: "integer",
      defaultsTo: 0
    },
    incidents:
    {
      collection: "incident",
      via: "serviceid"
    },
    supportqueue:
    {
      model: "queue"
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
