/* FileName: Cilocation.js
* @description: This file describes attributes of a CI location.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 01/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "cilocations",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    locationid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    locationname:
    {
      type: "string",
      required: true,
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    locationcode:
    {
      type: "string",
      required: true,
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true,
      unique: true
    },
    buildingname:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true,
      unique: true
    },
    sitecategory:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
