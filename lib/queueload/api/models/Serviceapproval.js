/* FileName: Serviceapproval.js
*
* @description: This file describes the attributes of a Service Approval
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 30/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{
  tableName: "serviceapprovals",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    approvalid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    crntsrvids:
    {
      collection: "service",
      via: "crntsrvsundappvl"
    },
    newserviceids:
    {
      collection: "service",
      via: "newsrvsundappvl"
    },
    isapproved:
    {
      type: "boolean",
      defaultsTo: null
    },
    approverid:
    {
      model: "user",
      required: true
    },
    approverserviceid:
    {
      model: "service",
      required: true
    },
    ciid:
    {
      model: "cidetail"
    },
    type:
    {
      type: "string",
      enum: [ "state", "assoc" ]
    },
    isactive:
    {
      type: "boolean",
      required: true,
      defaultsTo: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
