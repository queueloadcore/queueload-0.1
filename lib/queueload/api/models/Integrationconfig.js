/* FileName: Integrationconfig.js
 *
 * @description: This file describes the attributes of an Integration Configuration
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 25/05/2017        Initial file creation                                       SMandal
 *
 */

module.exports =
{

  tableName: "integrationconfigs",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    configid:
    {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    integrationtype:
    {
      type: 'integer',
      required: true,
      unique: true
    },
    integratorid:
    {
      model: 'user',
      required: true
    },
    config:
    {
      type: 'json'
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
          return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
        columnName: 'updatedAt',
        type: 'number',
        defaultsTo: function()
        {
          return Math.floor(new Date() / 1000);
        }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
  beforeValidate: function(values, next) {
      values.updatedAt = Math.floor(new Date() / 1000);
      next();
  }
};
