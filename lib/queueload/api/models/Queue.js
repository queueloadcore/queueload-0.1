  /* FileName: Queue.js
  * Description: This file describes the attributes of a Queue which includes describes its various attributes.
  *
  * Date              Change Description                                          Author
  * ---------         ---------------------------                                 -------
  * 13/12/2015        Initial file creation                                       SMandal
  *
  */

module.exports =
{

  tableName: "queues",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    queueid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    queuename:
    {
      type: "string",
      required: true,
      minLength: 4,
      maxLength: 150,
      alphaNumericDotDash: true,
      unique: true
    },
    cbid:
    {
      model: "cb"
    },
    queueemail:
    {
      type: "email",
      required: true,
      maxLength: 254
    },
    queueownerid:
    {
      model: "user"
    },
    members:
    {
      collection: "user",
      via: "queuesenrolled"
    },
    incidentmanagerid:
    {
      model: "user"
    },
    changemanagerid:
    {
      model: "user"
    },
    srtmanagerid:
    {
      model: "user"
    },
    alertl1:
    {
      type: "email",
      maxLength: 254
    },
    alertl2:
    {
      type: "email",
      maxLength: 254
    },
    alertl3:
    {
      type: "email",
      maxLength: 254
    },
    isactive:
    {
      type: "boolean",
      defaultsTo: false,
      required: true
    },
    isfrozen:
    {
      type: "boolean",
      defaultsTo: true
    },
    freezecause:
    {
      type: "integer",
      defaultsTo: 1
    },
    servicesmanaged:
    {
      collection: "service",
      via: "supportqueue"
    },
    assignedincidents:
    {
      collection: 'incident',
      via: "assignedqueueid"
    },
    ownedincidents:
    {
      collection: 'incident',
      via: "ownerqueueid"
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
