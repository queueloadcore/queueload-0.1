/* FileName: Cihardware.js
* @description: This file describes the attributes of a hardware CI.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 01/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "cihardwares",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    machineid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    hardwaretag:
    {
      type: "string",
      minLength: 4,
      maxLength: 64,
      alphaNumericDotDash: true,
      unique: true,
      required: true
    },
    manufacturervendorid:
    {
      model: "vendor",
      required: true
    },
    hwmodel:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    serialnumber:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDash: true
    },
    rack:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDash: true
    },
    rackunit:
    {
      type: "integer",
      rackUnitLimit: true
    },
    gridloc:
    {
      type: "string",
      minLength: 2,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    slot:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    macid:
    {
      type: "string",
      macaddress: true,
      unique: true,
      minLength: 17,
      maxLength: 17
    },
    osname:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    osvendorid:
    {
      model: "vendor"
    },
    osversion:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    biosserial:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    biosvendorid:
    {
      model: "vendor"
    },
    biosmodel:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    phymemmb:
    {
      type: "integer"
    },
    psuwattage:
    {
      type: "integer"
    },
    partnum:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
