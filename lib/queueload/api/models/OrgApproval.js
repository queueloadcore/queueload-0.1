/* FileName: OrgApproval.js
* @description: This file describes the attributes of the approvals generated for
+ entities in the organization module.
*
*   Date                      Change Description                       Author
* ---------               ---------------------------                 ---------
* 19/03/2016                  Initial file creation                     bones
*
*/

module.exports =
{

  tableName: "organizationapprovals",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    approvalid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    typeofapproval:
    {
      type: "integer",
      required: true
    },
    entityid:
    {
      type: "integer",
      required: true
    },
    targetentityid:
    {
      type: "integer",
      required: true
    },
    approverid:
    {
      model: "user",
      required: true
    },
    initiatorid:
    {
      model: "user",
      required: true
    },
    isapproved:
    {
      type: "boolean"
    },
    isactive:
    {
      type: "boolean",
      defaultsTo: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
