/* FileName: Cinetwork.js
* Description: This file describes attributes of network information of a CI.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 01/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "cinetworks",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    cinetworkid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    primaryip:
    {
      type: "string",
      ip: true,
      unique: true,
      required: true
    },
    networkname:
    {
      type: "string",
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    subnet:
    {
      type: "string",
      ip: true,
      required: true
    },
    gateway:
    {
      type: "string",
      ip: true,
      required: true
    },
    additionalip:
    {
      type: "string",
      ip: true
    },
    additionalsubnet:
    {
      type: "string",
      ip: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
