/* FileName: Vendor.js
* @description: This file describes the vendors of CIs.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 01/03/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "vendors",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    vendorid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    vendorname:
    {
      type: "string",
      required: true,
      minLength: 3,
      maxLength: 64,
      alphaSpace: true,
      unique: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
