/* FileName: Alarm.js
 *
 * @description: This file describes the attributes of an Alarm
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 19/05/2017        Initial file creation                                       PPani
 *
 */

module.exports = {

  tableName: "alarms",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    id:
    {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    type: {
      type: 'integer',
      required: true,
      defaultsTo: 1
    },
    ci:
    {
      type: 'string'
    },
    service:
    {
      type: 'string'
    },
    reportedAt:
    {
      type: 'numeric',
      min: 1,
      required: true
    },
    metric:
    {
      type: 'string',
      maxLength: 128
    },
    component:
    {
      type: 'string',
      maxLength: 128
    },
    summary:
    {
      type: 'string',
      maxLength: 8192
    },
    state:
    {
      type: 'integer'
    },
    region:
    {
      type: 'string',
      maxLength: 128
    },
    accountid:
    {
      model: 'company',
      required: true
    },
    name:
    {
      type: 'string',
      minLength: 2,
      maxLength: 256
    },
    source:
    {
      type: 'integer',
      required: true
    },
    isack:
    {
      type: 'boolean',
      defaultsTo: false
    },
    incidentid:
    {
      model: 'incident'
    },
    ackby:
    {
      model: 'user'
    },
    rawdata:
    {
      type: 'json'
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
          return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
        columnName: 'updatedAt',
        type: 'number',
        defaultsTo: function()
        {
          return Math.floor(new Date() / 1000);
        }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
  beforeValidate: function(values, next) {
      values.updatedAt = Math.floor(new Date() / 1000);
      next();
  }
};
