/* FileName: Inctemplate.js
 *
 * @description: This file describes the attributes of a Incident Templates
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 27/10/2016        Initial file creation                                       SMandal
 *
 */

module.exports =
{

  tableName: "inctemplates",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    templateid:
    {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    templatename:
    {
      type: 'string',
      minLength: 2,
      maxLength: 256,
      required: true,
      unique: true
    },
    lasteditorid:
    {
      model: 'user',
      required: true
    },
    title:
    {
      type: 'string',
      maxLength: 128
    },
    description:
    {
      type: 'string',
      maxLength: 8192
    },
    ownerqueuename:
    {
      type: 'string',
      minLength: 4,
      maxLength: 150,
      alphaNumericDotDash: true
    },
    ownerusername:
    {
      type: 'string',
      minLength: 8,
      maxLength: 32,
      alphaNumericDotDash: true
    },
    assignedqueuename:
    {
      type: 'string',
      minLength: 4,
      maxLength: 150,
      alphaNumericDotDash: true
    },
    assignedusername:
    {
      type: 'string',
      minLength: 8,
      maxLength: 32,
      alphaNumericDotDash: true
    },
    servicename:
    {
      type: 'string',
      minLength: 3,
      maxLength: 64,
      alphaNumericDotDashSpace: true
    },
    ciname:
    {
      type: 'string',
      minLength: 3,
      maxLength: 128,
      alphaNumericDotDash: true
    },
    severity:
    {
      type: 'integer',
      min: 1,
      max: 3
    },
    impact:
    {
      type: 'integer',
      min: 1,
      max: 4
    },
    attachments:
    {
      collection: 'attachment',
      via: 'attachmentid'
    },
    isactive:
    {
      type: 'boolean',
      defaultsTo: true
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};

