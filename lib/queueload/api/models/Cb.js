/* FileName: Cb.js
* @description: This file describes the attributes of a Chargeback Code.
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 12/12/2015        Initial file creation                                       SMandal
*
*/

module.exports =
{
  tableName: "chargebackcodes",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    cbid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    bdid:
    {
      model: "bd",
      required: true
    },
    isactive:
    {
      type: "boolean",
      defaultsTo: false,
      required: true
    },
    cbownerid:
    {
      model: "user"
    },
    ownedqueues:
    {
      collection: "queue",
      via: "cbid"
    },
    ownedservices:
    {
      collection: "service",
      via: "cbid"
    },
    isfrozen:
    {
      type: "boolean",
      defaultsTo: true
    },
    freezecause:
    {
      type: "integer",
      defaultsTo: 1
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
