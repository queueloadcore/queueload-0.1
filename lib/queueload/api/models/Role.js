/* FileName: Role.js
* @description: This file describes the attributes of a role
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 26/08/2016        Initial file creation                                       SMandal
*
*/

module.exports = {

  tableName: "roles",

  schema: true,
  autoPK: true,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    roleid:
    {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    userid:
    {
      model: "user",
      unique: true,
      required: true
    },
    issuperuser:
    {
      type: "boolean",
      required: true,
      defaultsTo: false
    },
    isbdadmin:
    {
      type: "boolean",
      required: true,
      defaultsTo: false
    },
    isoperator:
    {
      type: "boolean",
      required: true,
      defaultsTo: false
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.id;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
