/* FileName: Token.js
* @description: This file describes attributes of a Token
*
* Date              Change Description                                          Author
* ---------         ---------------------------                                 -------
* 27/07/2016        Initial file creation                                       SMandal
*
*/

module.exports =
{

  tableName: "authtokens",

  schema: true,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    emailid:
    {
      type: "email",
      required: true,
      maxLength: 254
    },
    tokentype:
    {
      type: "integer",
      required: true
    },
    token:
    {
      type: "string",
      required: true,
      minLength: 32,
      maxLength: 1024
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};

