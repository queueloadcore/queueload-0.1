/* FileName: Attachment.js
 *
 * @description: This file describes the attributes of an attachment
 *
 * Date              Change Description                                          Author
 * ---------         ---------------------------                                 -------
 * 14/09/2016        Initial file creation                                       SMandal
 *
 */

module.exports =
{

  tableName: "attachments",

  schema: true,
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes:
  {
    attachmentid:
    {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    key:
    {
      type: 'string',
      size: 32,
      required: true
    },
    filename:
    {
      type: 'string',
      required: true
    },
    size:
    {
      type: 'numeric',
      required: true
    },
    etag:
    {
      type: 'string',
      size: 32,
      required: true
    },
    incidents:
    {
      collection: 'incident',
      via: 'incidentid'
    },
    createdAt:
    {
      columnName: 'createdAt',
      type: 'string',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    updatedAt:
    {
      columnName: 'updatedAt',
      type: 'number',
      defaultsTo: function()
      {
        return Math.floor(new Date() / 1000);
      }
    },
    toJSON: function()
    {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      delete obj.etag;
      delete obj.key;
      return obj;
    }
  },
  beforeValidate:function(values, next)
  {
    values.updatedAt = Math.floor(new Date() / 1000);
    next();
  }
};
