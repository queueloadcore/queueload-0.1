/* FileName: ValidationPatterns.js
* @description: This file contains all the enums used in our system .
*
* Date                        Change Description                               Author
* ---------               ---------------------------                        -----------
* 07/04/2016                Initial file creation                              bones
*
*/

var enums =
{
  entity:
  {
    QUEUE: 1,
    CI: 2,
    CB: 3,
    LIMIT: 4
  },
  ciStateIds:
  {
    REGISTRATION: 1,
    APPROVED: 2,
    PLANNED: 3,
    INSTALLED: 4,
    COMMISSIONED: 5,
    RECLAIM: 6,
    DECOMMISSIONED: 7
  },
  serviceStateIds:
  {
    REGISTRATION: 1,
    APPROVED: 2,
    ARCHIVED: 3
  },
  approval:
  {
    TRANSFER_SERVICE: 1,
    TRANSFER_QUEUE: 2,
    TRANSFER_CB: 3,
    ASSIGN_Q2CB: 4,
    ASSIGN_CB2U: 5,
    ASSIGN_CI2Q: 6,
    ASSIGN_SERVICE2Q: 7,
    LIMIT: 8
  },
  tokenTypes:
  {
    ACCESS: 1,
    REFRESH: 2
  },
  freezeCauses:
  {
    NOT_FROZEN: 0,
    PENDING_CREATION_APPROVAL: 1,
    CREATION_DECLINED: 2,
    PENDING_CI_QUEUE_ASSIGNMENT: 3,
    PENDING_SERVICE_CI_ASSOCIATION: 4,
    PENDING_SERVICE_STATECHANGE: 5,
    PENDING_TRANSFER: 6,
    PENDING_SERVICE_QUEUE_ASSIGNMENT: 7
  },
  incidentStates:
  {
    REJECT: 1,
    OPEN: 2,
    ASSIGNED: 3,
    WORKINPROGRESS: 4,
    RESOLVED: 5,
    UNRESOLVED: 6,
    CLOSED: 7,
    PENDINGINC: 8,
    PENDINGCUSTOMER: 9,
    PENDINGVENDOR: 10,
    PENDINGOTHER: 11
  },
  incidentResCodes:
  {
    RESOLVED: 1,
    DUPTICKET: 2,
    CANCELLED: 3
  },
  incidentSeverity:
  {
    HIGH: 1,
    MEDIUM: 2,
    LOW: 3
  },
  incidentImpact:
  {
    ENTERPRISE: 1,
    REGION: 2,
    DATACENTER: 3,
    SINGLEUSER: 4
  },
  templateTypes:
  {
    INCIDENT: 1
  },
  incSatisfactionRating:
  {
    DISSATISFIED: 1,
    AGREEABLE: 2,
    SATISFIED: 3
  },
  incJournalTypes:
  {
    INCIDENT_CREATED: 1,
    ASSIGNED_TO_QUEUE: 2,
    ASSIGNED_TO_OPERATOR: 3,
    CHANGED_TO_STATE: 4,
    PENDING_FOR_INC: 5,
    PENDING_FOR_VENDORSR: 6,
    INCIDENT_CANCELLED: 7,
    MANUAL_ENTRY: 8,
    TICKET_RELATED: 9,
    DUPLICATE_TICKET: 10
  },
  configTypes:
  {
    GLOBAL: 1,
    USER: 2
  },
  incReporterType:
  {
    USER: 1,
    LOGGLY: 2,
    CLOUDWATCH: 3,
    SENTRY: 4,
    NAGIOS: 5,
    WEAVED: 6,
    CUSTOM: 7,
    AZURE: 8,
    LIMIT: 9
  },
  alarmType: {
    INFO: 1,
    WARNING: 2,
    ERROR: 3,
    CRITICAL: 4
  },
  alarmState: {
    OK: 0,
    ALARM: 1
  },
  integrationService:
  {
    CLOUDWATCH: 1,
    SENTRY: 2,
    WEAVED: 3,
    NAGIOS: 4,
    SLACK: 5,
    LOGGLY: 6,
    CUSTOM: 7,
    AZURE: 8,
    LIMIT: 9
  }
};

module.exports = enums;
