/* FileName     : ResetController.js
*  @description  : This file describes the various actions related to resetting the datastore of the customer.
*
*  Date          Change Description                            Author
*  ---------     ----------------------                        -------
*  5/1/2016    Initial file creation                         SMandal
*
*/

var Promise = require("bluebird");
var errorCode = require("../resources/ErrorCodes.js");

module.exports =
{
  /**
    @apiGroup Reset
    @apiName resetall
    @api {post} /reset/resetall Resetall
    @apiDescription Destroys all data in the system except the primary super user details
    @apiPermission superuser
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
        curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6I
        kpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwiYWNjb3VudGlkIjoiRFEwUTRZMzQiLCJ1c2VyaWQiOjEsImZpc
        nN0bmFtZSI6IlN1cGVyIiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJhZG1pbmlzdHJhdG9yIiwiZW1haWxpZCI6Im
        FkbWluaXN0cmF0b3JAdGVzdGRvbWFpbi5jb20iLCJjb3VudHJ5Y29kZSI6bnVsbCwicGhvbmVudW1iZXIiOm51bGwsImVtcGx
        veWVlaWQiOiJGVEUwMDAwMDEiLCJpc2FjdGl2ZSI6dHJ1ZSwicGFzc2V4cGlyZWQiOmZhbHNlfSwiaWF0IjoxNDgzNTk4MDI4
        LCJleHAiOjE0ODM2MTk2MjgsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.TU
        VqDow7VdUiYqpDpHcF3oT2T3GlrbrgJSNI3vRz0mw" "http://localhost:1337/reset/resetall"
    @apiUse 403502
    @apiSuccess NA No return on sucess
  */
  resetall: function(req, res)
  {
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode [ '403502' ];
      return res.negotiate(error);
    }
    else
    {
      var pIncident = Incident.destroy();
      var pCidetail = Cidetail.destroy();
      var pCienvironment = Cienvironment.destroy();
      var pCinetwork = Cinetwork.destroy();
      var pCitype = Citype.destroy();
      var pCilocation = Cilocation.destroy();
      var pCihardware = Cihardware.destroy();
      var pVendors = Vendor.destroy();
      var pService = Service.destroy();
      var pQueue = Queue.destroy();
      var pCb = Cb.destroy();
      var pBd = Bd.destroy();
      var pInctemplate = Inctemplate.destroy();
      var pOrgApproval = OrgApproval.destroy();
      var pServiceapproval = Serviceapproval.destroy();
      var pToken = Token.destroy();
      var pAttachment = Attachment.destroy();
      var pRole = Role.destroy({ where:{ userid: { '!': [ 1 ] } } } );
      var pUser = User.destroy({ where:{ userid: { '!': [ 1 ] } } } );
      var pCompany = Company.destroy({ where:{ accountid: { '!': [ req.body.accountid ] } } } );
      var pIntegration = Integrationconfig.destroy();
      var pAlarm = Alarm.destroy();
      var pArray = [ pIncident, pCidetail, pCienvironment, pCinetwork, pCitype, pCilocation, pCihardware, pVendors, pService, pQueue, pCb, pBd, pInctemplate, pOrgApproval, pServiceapproval, pToken, pAttachment, pRole, pUser, pCompany, pIntegration, pAlarm ];

      Promise.all(pArray)
      .then(function(result)
      {
        return res.ok();
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  }
};
