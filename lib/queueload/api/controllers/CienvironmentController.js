/* FileName: CienvironmentController.js
*  @description: Server-side logic for managing the different CI environments.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  23/03/2016        Initial file creation                         bones
*
*/

/**
 * @apiGroup Environment
 */

var errorCode = require("../resources/ErrorCodes.js");
var Promise = require("bluebird");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var _ = require('lodash');
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{
  /**
  @apiGroup Environment
  @apiName listEnvs
  @api {post} /cienvironment/list List
  @apiDescription This API lists environments in the system
  @apiPermission user
  @apiHeader (Content-Type) {Strng} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ey
     J1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iL
     CJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5
     OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjUzMjQ3MywiZXhwIjoxNDcyNTU0MDc
     zLCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.-S8nsNHDf2OlWzdCbYz96ambz1HsslwhiU
     NmcV73bHY" -H "Content-Type: application/json" -d '{"page" : 1, "limit" : 100}' "http://localhost:1337/cienvironment/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400311
  @apiSuccess (200) {Object[]} environments Array of environments
  @apiSuccessExample {json} Success-Response
    [
      {
        "envid": 1,
        "envname": "development",
        "numberofcis": 4
      },
      {
        "envid": 2,
        "envname": "testing",
        "numberofcis": 4
      },
      {
        "envid": 3,
        "envname": "production",
        "numberofcis": 4
      },
      {
        "envid": 4,
        "envname": "yelpdoc",
        "numberofcis": 4
      }
    ]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    var output = [];
    Cienvironment.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400311;
        error.message = errorCode[ "400311" ];
        throw error;
      }
      else
      {
        output = found;
        var countCis = function(element)
        {
          return Cidetail.count().where({ envid: element.envid });
        };
        var actions = found.map(countCis);
        return Promise.all(actions);
      }
    })
    .then(function(result)
    {
      var assign = function(element, index)
      {
        output[ index ].numberofcis = element;
      };
      result.forEach(assign);
      return res.ok(output);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @apiGroup Environment
  @apiName addEnvs
  @api {post} /cienvironment/add Add
  @apiDescription This API Add an environment to the system
  @apiPermission operator
  @apiHeader (Content-Type) {Strng} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZmlyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpc
    nN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bW
    JlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNTQ3MjA
    wLCJleHAiOjE0NzI1Njg4MDAsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.RmAS8
    LT-9ZKSCAUrky_LoRFmY_zMhFksNSNPu45X3dM" -H "Content-Type: application/json"
    -d '{"envnames": ["development", "testing", "production"]}' "http://localhost:1337/cienvironment/add"
  @apiParam {String[]} envnames Array of environments
  @apiParamExample {json} Request-Example
      {
        "envnames": ["development", "testing", "production"]
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400312
  @apiUse 403502
  @apiSuccess (200) {Object[]} environments Array of added environments
  @apiSuccessExample {json} Success-Response
      [{
        "envid": 1,
        "envname": "development"
      }, {
        "envid": 2,
        "envname": "testing"
      }, {
        "envid": 3,
        "envname": "production"
      }]
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.envnames || !(Array.isArray(req.body.envnames)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var names = _.uniq(req.body.envnames);

    // Check if the input has duplicate entries
    if(names.length !== req.body.envnames.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var results;
    Cienvironment.find().where({ envname:req.body.envnames })
    .then(function showRecs(records)
    {
      // Check if any of the input entries exist in the system
      if(records.length > 0)
      {
        var error = new Error();
        error.code = 400312;
        error.message = errorCode[ "400312" ];
        throw error;
      }
      else
      {
        var envObj = function(envname)
        {
          this.envname = envname;
        };
        var envs = [];
        for(var iter = 0; iter < req.body.envnames.length; iter++)
        {
          envs.push(new envObj(req.body.envnames[ iter ]));
        }
        return Cienvironment.create(envs);
      }
    })
    .then(function showRecs(result)
    {
      results = result;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(results);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @apiGroup Environment
  @apiName updateEnvs
  @api {post} /cienvironment/update Update
  @apiDescription This API updates environments in the system
  @apiPermission operator
  @apiHeader (Content-Type) {Strng} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hc
    mNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW
    1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU0NzA
    4NiwiZXhwIjoxNDcyNTY4Njg2LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.OVDR
    HfrePDa69X6aTaeUU46ptr6Hbg4FRH4t9sWII8w" -H "Content-Type: application/json"
        -d '[{"envid": 2, "envname": "mobproduction"},{"envid": 3, "envname": "mobdev"}]'
    "http://localhost:1337/cienvironment/update"
  @apiParam {Object[]} envname Array of environments
  @apiParamExample {json} Request-Example
      [
        {"envid": 2, "envname": "mobproduction"},
        {"envid": 3, "envname": "mobdev"}
      ]
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400312
  @apiUse 400313
  @apiUse 403502
  @apiSuccess (200) {Object[]} environments Array of updated environments
  @apiSuccessExample {json} Success-Response
      [
        [{
          "envid": 2,
          "envname": "mobproduction"
        }],
        [{
          "envid": 3,
          "envname": "mobdev"
        }]
      ]
  */
  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    var i = 0;
    var ids = [];
    var envs = [];
    var envnames = [];
    for(i = 0; i < req.body.length; i++)
    {
      if(!req.body[ i ].envname || !req.body[ i ].envid || !(_.isFinite(req.body[ i ].envid)))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
      else
      {
        // Check if any of the input objects attributes are arrays
        if(Array.isArray(req.body[ i ].envid) || Array.isArray(req.body[ i ].envname))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
        ids.push(req.body[ i ].envid);
        envs.push(req.body[ i ]);
        envnames.push(req.body[ i ].envname);
      }
    }

    var names = _.uniq(envnames);

    // Check if the input has any duplicate environments
    if(names.length !== envnames.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var results;
    Cienvironment.find().where({ envid: ids })
    .then(function found(records)
    {
      // Check if any the input has any non-existent environment ids. It also checks if there are duplicate envids in the input
      if(records.length !== ids.length)
      {
        var error = new Error();
        error.code = 400313;
        error.message = errorCode[ "400313" ];
        throw error;
      }
      else
      {
        return Cienvironment.find().where({ envname: envnames });
      }
    })
    .then(function showRecs(records)
    {
      // Check if the input has any existing environments
      if(records.length > 0)
      {
        var error = new Error();
        error.code = 400312;
        error.message = errorCode[ "400312" ];
        throw error;
      }
      else
      {
        var fn = function aysncupdate(environment)
        {
          // Even if any inidividual object fails validation, the rest of rows are updated. Bug# D5-12
          return Cienvironment.update({ envid: environment.envid }, environment);
        };

        var actions = envs.map(fn);
        return Promise.all(actions);
      }
    })
    .then(function showRecs(result)
    {
      results = result;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(results);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
