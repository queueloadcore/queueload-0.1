/* FileName: CbController.js
* @description: This file describes the various actions in the CbController.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 12/12/2015    Initial file creation                         SMandal
* 13/02/2016    Bring the current code base to                bones
*               comply with the new philosophy
*
*/

/**
 * @apiGroup Chargeback
 */

var userUtils = require("../services/utils/user.js");
var bdUtils = require("../services/utils/bdutils.js");
var cbUtils = require("../services/utils/cbutils.js");
var Promise = require("bluebird");
var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");
var _ = require("lodash");

module.exports =
{
	controlTest: function(req, res)
	{
    res.json({ message: "Cb controller is working. :)" });
  },
/**
  * Name: list
  * @description: This function lists all the CB codes in the system
  *
  * @param:
      {
        "page" : 1,
        "limit" : 10
      }
  * @returns: json Object with all the user details
      [{
        "bdid": 1,
        "cbownerid": 1,
        "cbid": 1,
        "isactive": true
      }, {
        "bdid": 1,
        "cbownerid": 1,
        "cbid": 2,
        "isactive": true
      }, {
        "bdid": 1,
        "cbownerid": 1,
        "cbid": 3,
        "isactive": false
      }]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    Cb.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400201;
        error.message = errorCode[ "400201" ];
        throw error;
      }
      else
      {
        return res.json(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },

/**
  * Name: add
  * @description: On receipt a pair of bdId-userId, it picks up each id and
  * and checks for the validity and active status of each id. If both are
  * valid and active, it goes ahead with the creation of the CB code.
  * @param:
    {
      "bdid": 2,
      "ownerid": 1
    }
  * @returns
    [
      {
        "cbid": 4,
        "bdid": 2,
        "isactive": true,
        "cbownerid": 1,
        "isfrozen": true
      },
      {
        "approvalid": 5,
        "typeofapproval": 5,
        "entityid": 4,
        "targetentityid": 1,
        "approverid": 1,
        "initiatorid": 2,
        "isapproved": null,
        "isactive": true,
        "createdAt": "2016-05-17T10:03:08.000Z",
        "updatedAt": "2016-05-17T10:03:08.000Z"
      }
    ]
  */
  add: function(req, res)
  {
    var initiatorid = req.body.requesterid;
    var addedCb;
    // The API is only open for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    //Only a single cb is allowed to be created at one time
    if((!req.body.bdid || !(_.isFinite(req.body.bdid))) || (!req.body.ownerid || !(_.isFinite(req.body.ownerid))) ||
        req.body.bdid.length || req.body.ownerid.length || req.body.isactive)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var bdId = [ req.body.bdid ];
      var ownerId = [ req.body.ownerid ];

      //Check if BDId and cbownerid exist and are active. Also get BdOwnership details of the requester
      var paramsCheck = function(bdId, ownerId)
      {
        var pBdStatus = bdUtils.isActive(bdId);
        var pUserStatus = userUtils.isActive(ownerId);
        var pGetBd = bdUtils.getBd(bdId);
        return Promise.all([ pBdStatus, pUserStatus, pGetBd ]);
      };

      paramsCheck(bdId, ownerId, bdId)
      .then(function processpResult(pResult)
      {
        if((pResult[ 0 ][ 0 ] && pResult[ 0 ][ 0 ].isactive === true) &&
           (pResult[ 1 ][ 0 ] && pResult[ 1 ][ 0 ].isactive === true))
        {
          var isAdmin = false;
          for(var i = 0; i < pResult[ 2 ][ 0 ].admins.length; i++)
          {
            // Check if the requester is an admin of req.body.bdid
            if(pResult[ 2 ][ 0 ].admins[ i ].userid === initiatorid)
            {
              isAdmin = true;
              return Cb.create({ bdid: bdId });
            }
          }
          if(!isAdmin)
          {
            var error = new Error();
            error.code = 403501;
            throw error;
          }
        }
        else
        {
          var error = new Error();
          if(!pResult[ 0 ][ 0 ])
          {
            error.code = 400154;
            error.message = errorCode[ "400154" ];
          }
          else if(pResult[ 0 ][ 0 ].isactive === false)
          {
            error.code = 400153;
            error.message = errorCode[ "400153" ];
          }
          else if(!pResult[ 1 ][ 0 ])
          {
            error.code = 400104;
            error.message = errorCode[ "400104" ];
          }
          else
          {
            error.code = 400103;
            error.message = errorCode[ "400103" ];
          }
          throw error;
        }
      })
      .then(function(createdCb)
      {
        addedCb = createdCb;
        return OrgApproval.create({ typeofapproval: enums.approval.ASSIGN_CB2U, entityid: createdCb.cbid, targetentityid: req.body.ownerid, approverid: req.body.ownerid, initiatorid: initiatorid, isapproved: null });
      })
      .then(function createdApproval(approval)
      {
        return res.json([ approval, addedCb ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  * Name: activation
  * @description: Changes the activity state of the CB. Expects an array of CB IDs and the action
  * (activation: true, deactivation: false) to perform.
  *
  * @param:
    {
      "cbids": [1,5],
      "action": true
    }
    * @returns
    * 400 for badRequest
    * 200 for successful
  */
  activation: function(req, res)
  {
    // The API is only open for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.cbids || !req.body.cbids.length || typeof(req.body.action) !== "boolean")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var reqCbids = _.uniq(req.body.cbids);
      var i;
      for(i = 0; i < reqCbids.length; i++)
      {
        if(!_.isFinite(reqCbids[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }

      var administeredbds = [];
      User.findOne({ userid: req.body.requesterid }).populate('administeredbds')
      .then(function(details)
      {
        for(var i = 0; i < details.administeredbds.length; i++)
        {
          administeredbds.push(details.administeredbds[ i ].bdid);
        }
        return cbUtils.getCB(reqCbids);
      })
      .then(function(records)
      {
        var i = 0, j = 0;

        //Check if the supplied IDs are valid
        for(i = 0; i < records.length; i++)
        {
          if(!records[ i ])
          {
            var error = new Error();
            error.code = 400204;
            error.message = errorCode[ "400204" ];
            throw error;
          }
          // The requester must be an admin of the BD
          if(!_.includes(administeredbds, records[ i ].bdid.bdid))
          {
            var error = new Error();
            error.code = 403501;
            error.message = errorCode[ '403501' ];
            throw error;
          }
          if(records[ i ].isfrozen)
          {
            var error = new Error();
            error.code = 400208;
            error.message = errorCode[ "400208" ];
            throw error;
          }
        }

        //For action = true
        if(req.body.action)
        {
          //Check if any of the supplied CBs are already active
          //Check if associated BD is active
          //Check if the associcated owner is active
          for(i = 0; i < records.length; i++)
          {
            if(records[ i ].isactive)
            {
              var error = new Error();
              error.code = 400202;
              error.message = errorCode[ "400202" ];
              throw error;
            }
            if(!records[ i ].bdid.isactive)
            {
              var error = new Error();
              error.code = 400153;
              error.message = errorCode[ "400153" ];
              throw error;
            }
            if(!records[ i ].cbownerid.isactive)
            {
              var error = new Error();
              error.code = 400206;
              error.message = errorCode[ "400206" ];
              throw error;
            }
          }
        }
        else
        {
          //Check if any of the supplied CBs are already inactive
          //Check if associated with an active queue
          for(i = 0; i < records.length; i++)
          {
            if(!records[ i ].isactive)
            {
              var error = new Error();
              error.code = 400203;
              error.message = errorCode[ "400203" ];
              throw error;
            }
            for(j = 0; j < records[ i ].ownedqueues.length; j++)
            {
              if(records[ i ].ownedqueues[ j ].isactive)
              {
                var error = new Error();
                error.code = 400207;
                error.message = errorCode[ "400207" ];
                throw error;
              }
            }
            // Check if any service owned by the CB is in registration or approved state
            for(var k = 0; k < records[ i ].ownedservices.length; k++)
            {
              if(records[ i ].ownedservices[ k ].servicestateid < enums.serviceStateIds.ARCHIVED)
              {
                var error = new Error();
                error.code = 400212;
                error.message = errorCode[ "400212" ];
                throw error;
              }
            }
          }
        }
        //Perform the action
        return Cb.update({ cbid: reqCbids }, { isactive: req.body.action });
      })
      .then(function(result)
      {
        return res.json(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },

/**
  * Name: getstatus
  * @description: gets the status of the CB ids supplied
  *
  * @param:
      {
        "cbids": [1,2,3]
      }
    @returns
      [
        {
          "cbid": 1,
          "status": true
        },
        {
          "cbid": 2,
          "status": false
        },
        {
          "cbid": 3,
          "status": false
        }
      ]
  */
  getstatus: function(req, res)
  {
    var error = new Error();
    if(!req.body.cbids || !req.body.cbids.length)
    {
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var i = 0;
    var reqCbids = _.uniq(req.body.cbids);
    for(i = 0; i < reqCbids.length; i++)
    {
      if(!_.isFinite(reqCbids[ i ]))
      {
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
    }
    function obj(cbid, isactive, isfrozen)
    {
      this.cbid = cbid;
      this.isactive = isactive;
      this.isfrozen = isfrozen;
    }
    cbUtils.isActive(reqCbids)
    .then(function(records)
    {
      var resultObj = [];
      for(i = 0; i < records.length; i++)
      {
        if(records[ i ])
        {
          resultObj.push(new obj(records[ i ].cbid, records[ i ].isactive, records[ i ].isfrozen));
        }
        else
        {
          //All the supplied cbids should be valid
          var error = new Error();
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
      }
      return res.json(resultObj);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: getdetail
  * @description: Fetches the details of the cbids supplied. Max 10 entries at a time.
  * @param:
      {
        "cbids": [1]
      }
  * @returns:
      [
      {
        "ownedqueues":
        [
          {
            "queueid": 1,
            "queuename": "TestQueue1",
            "cbid": 1,
            "queueemail": "testqueue1@test.com",
            "queueownerid": 1,
            "incidentmanagerid": 1,
            "changemanagerid": 1,
            "srtmanagerid": 1,
            "alertl1": "testqueue1alert1@test.com",
            "alertl2": "testqueue1alert2@test.com",
            "alertl3": "testqueue1alert3@test.com",
            "isactive": true
          }
        ],
        "bdid":
        {
          "bdid": 2,
          "bdname": "BusinessDepartment2",
          "isactive": true
        },
        "cbownerid":
        {
          "userid": 1,
          "firstname": "alphatester",
          "lastname": "rat",
          "username": "T3st.Rat_3",
          "emailid": "tester1.rat@testclient3.com",
          "countrycode": "+91",
          "phonenumber": "9872387192",
          "employeeid": "FTE817345",
          "isactive": true
        },
        "cbid": "CB00001",
        "isactive": true
      }
    ]
  */
  getdetail: function(req, res)
  {
    var ids = 0;
    var i = 0;
    var error = new Error();
    if(!req.body.cbids || !req.body.cbids.length || req.body.cbids.length > 10)
    {
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      ids = _.uniq(req.body.cbids);
      for(i = 0; i < ids.length; i++)
      {
        if(!_.isFinite(ids[ i ]))
        {
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }
    }

    cbUtils.getCB(ids)
    .then(function(data)
    {
      //Check if the all the cb ids exist in the system
      for(i = 0; i < data.length; i++)
      {
        if(!data[ i ])
        {
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
      }
      return res.json(data);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: search
  * @description: searches the model and returns the result. The number of search column and order by criteria are flexible.
  * @param:
      {
        "search":  { "cbownerid": "8" },
        "result" : [ "cbid", "bdid", "cbownerid", "isfrozen"],
        "orderby": "bdid"
      }
  * @returns:
      [
        4,
        [
          {
            "bdid": 1,
            "cbownerid": 1,
            "cbid": 1,
            "isfrozen": true
          },
          {
            "bdid": 1,
            "cbownerid": 1,
            "cbid": 2,
            "isfrozen": true
          },
          ...
        ]
      ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    /*
    //No String attributes in CB
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(CB.definition, key);
      if(!(_.isEmpty(obj)) && obj[key].type === 'string') return { 'like' : value };
      else return value;
    });
    */
    Cb.find({ select: req.body.result }).where(req.body.search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
};
