/* FileName: BdController.js
* @description: This file describes the various actions in the BD controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 10/12/2015    Initial file creation                         SMandal
* 10/02/2016    Bring the current code base to                bones
*               comply with the new philosophy
*
*/

/**
 * @apiGroup Business Department
 */

var userUtils = require("../services/utils/user.js");
var bdUtils = require("../services/utils/bdutils.js");
var errorCode = require("../resources/ErrorCodes.js");
var Promise = require("bluebird");
var _ = require("lodash");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{
  controlTest: function(req, res)
  {
    res.json({ message: "Bd controller is working. :)" });
  },
/**
  * Name: list
  * @description: This function lists all the BDs in the system
  *
  * @param: None
  * @returns: json Object with all the BD details
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    Bd.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400151;
        error.message = errorCode[ "400151" ];
        throw error;
      }
      else
      {
        return res.json(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },

/**
  * Name: add
  * @description: Creates a Business Department based on the name sent as the parameter.
    On success it returns the name stored in the DB and the generated id for the BD.
  * @param:
  * {
  *  "bdname": "BusinessDepartment 8"
  * }
  *
  * @returns:
  *  [{
  *      "bdname": "BusinessDepartment 8",
  *      "bdid": 8,
  *      "isactive": false
  *  }]
  *
  */
  add: function(req, res)
  {
    // The API is only open for superusers
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode [ '403502' ];
      return res.negotiate(error);
    }
    //Only a single BD is allowed to be created at one time
    else if(!req.body.bdname || Object.prototype.toString.call(req.body.bdname) === "[object Array]")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var result;
      //Create the BD
      Bd.create({ bdname:req.body.bdname })
      .then(function(bd)
      {
        result = bd;
        return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
      })
      .then(function(value)
      {
        return res.json(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },

    /**
    * Name: assign
    * @description: Assigns/Unassigns Users as admins from BDs.
    *
    * @param:
    *   {
    *     "bdid": 1,
    *     "admins": [10],
    *     "assign": false
    *   }
    * @returns:
    *   {
    *     "admins": [{
    *     "userid": 1,
    *     "firstname": "Test",
    *     "lastname": "User",
    *     "username": "T3st.User_1",
    *     "emailid": "test.user1@testclient1.com",
    *     "countrycode": "+1",
    *     "phonenumber": "9999999999",
    *     "employeeid": "FTE000001",
    *     "isactive": true
    *   }],
    *   "cbcodes": [],
    *   "bdid": "BD00001",
    *   "bdname": "BusinessDept1",
    *   "isactive": true
    * }
    *
    */
    assign: function(req, res)
    {
      // The API is only open for superusers and admin of the bd in question
      if(!req.body.requesterroles.issuperuser && !req.body.requesterroles.isbdadmin)
      {
        var error = new Error();
        error.code = 403502;
        return res.negotiate(error);
      }
      //Only one BD at a time
      else if(!req.body.bdid || !(_.isFinite(req.body.bdid)) || Object.prototype.toString.call(req.body.bdid) === "[object Array]" ||
      typeof(req.body.assign) !== "boolean" || !req.body.admins || !req.body.admins.length)
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }

      //Return true if any of the needles exist in the haystack
      var MemberExists = function(needles, haystack, exist)
      {
        return _.some(needles, function(needle)
        {
          return ((_.indexOf(haystack, needle) >= 0) === exist);
        });
      };

      //Remove duplicates
      var reqAdmins = _.uniq(req.body.admins);
      var i = 0;
      var response;
      for(i = 0; i < reqAdmins.length; i++)
      {
        if(!_.isFinite(reqAdmins[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }
      if(req.body.assign)
      {
        var adminIds = [];
        Bd.findOne({ bdid: req.body.bdid }).populate("admins")
        .then(function(records)
        {
          var i = 0;

          if(!records)
          {
            var error = new Error();
            error.code = 400154;
            error.message = errorCode[ "400154" ];
            throw error;
          }

          for(i = 0; i < records.admins.length; i++)
          {
            adminIds.push(records.admins[ i ].userid);
          }

          // The requester must be an admin of this BD or a superuser
          if(!_.includes(adminIds, req.body.requesterid) && !req.body.requesterroles.issuperuser)
          {
            var error = new Error();
            error.code = 403501;
            error.message = errorCode[ "403501" ];
            throw error;
          }

          //If an admin is already assigned, it throws an error
          if(MemberExists(reqAdmins, adminIds, true))
          {
            var error = new Error();
            error.code = 400156;
            error.message = errorCode[ "400156" ];
            throw error;
          }
          return userUtils.isActive(reqAdmins);
        })
        .then(function(results)
        {
          for(i = 0; i < results.length; i++)
          {
            if(!results[ i ])
            {
              var error = new Error();
              error.code = 400104;
              error.message = errorCode[ "400104" ];
              throw error;
            }
            if(!results[ i ].isactive)
            {
              var error = new Error();
              error.code = 400103;
              error.message = errorCode[ "400103" ];
              throw error;
              break;
            }
          }

          //Assign administratorship to users
          var updatedAdmins = _.union(adminIds, reqAdmins);
          var pBdUpdate = Bd.update({ bdid: req.body.bdid }, { admins: updatedAdmins });
          // Grant bdadmin roles
          var pRoleUpdate = Role.update({ userid: updatedAdmins }, { isbdadmin: true });
          var pDetails = userUtils.getUser(req.body.admins);
          return Promise.all([ pBdUpdate, pRoleUpdate, pDetails ]);
          //Return Bd.update({ bdid: req.body.bdid }, { admins: updatedAdmins });
        })
        .then(function(resolved)
        {
          response = resolved[ 0 ];
          var signOutCandiates = [];
          for (var i = 0; i < resolved[ 2 ].length; i++)
          {
            if(!resolved[ 2 ][ i ].administeredbds.length)
            {
              signOutCandiates.push(resolved[ 2 ][ i ].emailid);
            }
          }
          var signout = function(emailid)
          {
            return client.multi()
             .hmset(process.env.ACCOUNTID + ":" + emailid, [ "signedout", true ])
             .hdel(process.env.ACCOUNTID + ":" + emailid, [ "reftkn" ])
             .EXPIRE(process.env.ACCOUNTID + ":" + emailid, sails.config.authcfg.accExpiresIn)
             .execAsync();
          };
          var actions = signOutCandiates.map(signout);
          var pResults = Promise.all(actions);
          return pResults;
        })
        .then(function(role)
        {
          return res.json(response);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
      else
      {
        Bd.findOne({ bdid: req.body.bdid }).populate("admins")
        .then(function(records)
        {
          var i = 0;
          var adminIds = [];
          var activeAdminIds = [];

          if(!records)
          {
            var error = new Error();
            error.code = 400154;
            error.message = errorCode[ "400154" ];
            throw error;
          }

          for(i = 0; i < records.admins.length; i++)
          {
            if(records.admins[ i ].isactive) activeAdminIds.push(records.admins[ i ].userid);
            adminIds.push(records.admins[ i ].userid);
          }

          // The requester must be an admin of this BD or a superuser
          if(!_.includes(adminIds, req.body.requesterid) && !req.body.requesterroles.issuperuser)
          {
            var error = new Error();
            error.code = 403501;
            error.message = errorCode[ "403501" ];
            throw error;
          }

          //Check if all active admins are being removed from an active BD.
          if((records.isactive) && (_.intersection(activeAdminIds, reqAdmins).length === activeAdminIds.length))
          {
            var error = new Error();
            error.code = 400158;
            error.message = errorCode[ "400158" ];
            throw error;
          }

          //Check if any member is not an existing admin of the BD
          if(MemberExists(reqAdmins, adminIds, false))
          {
            var error = new Error();
            error.code = 400157;
            error.message = errorCode[ "400157" ];
            throw error;
          }

          //Remove administratorship from users
          var updatedAdmins = _.difference(adminIds, reqAdmins);
          var pBdUpdate = Bd.update({ bdid: req.body.bdid }, { admins: updatedAdmins });
          var adminNoMore = [];

          function updateAndRevoke(reqAdmins)
          {
            var first = pBdUpdate;
            var second = first.then(function(bd)
            {
              return userUtils.getBdAdmins(reqAdmins);
            })
            .then(function(admins)
            {
              for(var i = 0; i < admins.length; i++)
              {
                if(!admins[ i ].administeredbds.length)
                {
                  adminNoMore.push(admins[ i ].userid);
                }
              }
              return Role.update({ userid: adminNoMore }, { isbdadmin: false });
            });
            return Promise.all([ first, second, userUtils.getUser(req.body.admins) ]);
          }
          return updateAndRevoke(reqAdmins);
          //Return Bd.update({ bdid: req.body.bdid }, { admins: updatedAdmins });
        })
        .then(function(resolved)
        {
          response = resolved[ 0 ];
          var signOutCandiates = [];
          for (var i = 0; i < resolved[ 2 ].length; i++)
          {
            if(resolved[ 2 ][ i ].administeredbds.length === 1)
            {
              signOutCandiates.push(resolved[ 2 ][ i ].emailid);
            }
          }
          var signout = function(emailid)
          {
            return client.multi()
             .hmset(process.env.ACCOUNTID + ":" + emailid, [ "signedout", true ])
             .hdel(process.env.ACCOUNTID + ":" + emailid, [ "reftkn" ])
             .EXPIRE(process.env.ACCOUNTID + ":" + emailid, sails.config.authcfg.accExpiresIn)
             .execAsync();
          };
          var actions = signOutCandiates.map(signout);
          var pResults = Promise.all(actions);
          return pResults;
        })
        .then(function(roles)
        {
          return res.ok(response);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
    },
/**
  * Name: activation
  * @description: Changes the activity state of the BD
  *
  * @param:
    {
      "bdids": [1,5],
      "action": true
    }
  * @returns:
  * 200 OK on Success
  * OR
  * Error code on failure
  *
  */
  activation: function(req, res)
  {
    // The API is only open for superusers
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.bdids || Object.prototype.toString.call(req.body.bdids) !== "[object Array]" ||
    !req.body.bdids.length || typeof(req.body.action) !== "boolean")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var i = 0, j = 0, flag = "proceed";

      //Remove duplicates
      var reqBdids = _.uniq(req.body.bdids);
      for(i = 0; i < reqBdids.length; i++)
      {
        if(!_.isFinite(reqBdids[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }
      //If any bd is already in the exepected state, throw an error
      bdUtils.isActive(reqBdids)
      .then(function(results)
      {
        for( i = 0; i < results.length; i++)
        {
          if(!results[ i ])
          {
            var error = new Error();
            error.code = 400154;
            error.message = errorCode[ "400154" ];
            throw error;
          }
          if(results[ i ].isactive === req.body.action)
          {
            if(req.body.action)
            {
              var error = new Error();
              error.code = 400152;
              error.message = errorCode[ "400152" ];
              throw error;
            }
            else
            {
              var error = new Error();
              error.code = 400153;
              error.message = errorCode[ "400153" ];
              throw error;
            }
          }
        }
        return bdUtils.getBd(reqBdids);
      })
      .then(function(records)
      {
        if(req.body.action)
        {
          flag = "proceed";

          //Atleast one admin of the bd should be active
          for(i = 0; i < records.length; i++)
          {
            if(flag === "proceed") { flag = "noactivate"; }
            else { break; }

            for(j = 0; j < records[ i ].admins.length; j++)
            {
              if(records[ i ].admins[ j ] && records[ i ].admins[ j ].isactive)
              {
                flag = "proceed";
                break;
              }
            }
          }
        }
        else
        {
          flag = "proceed";

          //No Cb codes owned by the BD should be active
          for(i = 0; i < records.length; i++)
          {
            for(j = 0; j < records[ i ].cbcodes.length; j++)
            {
              if(records[ i ].cbcodes[ j ] && records[ i ].cbcodes[ j ].isactive)
              {
                flag = "nodeactivate";
                break;
              }
            }
            if(flag !== "proceed") { break; }
          }
        }
        return flag;
      })
      .then(function(flag)
      {
        if(flag === "proceed")
        {
          return Bd.update({ bdid: reqBdids }, { isactive: req.body.action });
        }
        else if(flag === "noactivate")
        {
          var error = new Error();
          error.code = 400158;
          error.message = errorCode[ "400158" ];
          throw error;
        }
        else if (flag === "nodeactivate")
        {
          var error = new Error();
          error.code = 400159;
          error.message = errorCode[ "400159" ];
          throw error;
        }
        else
        {
          var error = new Error();
          error.code = 500001;
          error.message = errorCode[ "500001" ];
          throw error;
        }
      })
      .then(function(results)
      {
        return res.json(results);
      })
      .catch(function(error)
      {
        res.negotiate(error);
      });
    }
  },
/**
  * Name: getstatus
  * @description: gets the status of the BD ids supplied
  *
  * @param:
      {
        "bdids": [1,2,3]
      }
    @returns
    [
      {
        "bdid": 1,
        "status": true
      },
      {
        "bdid": 2,
        "status": false
      },
      {
        "bdid": 3,
        "status": false
      }
    ]
  */
  getstatus: function(req, res)
  {
    if(!req.body.bdids || !req.body.bdids.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var i = 0;
    var reqBdids = _.uniq(req.body.bdids);
    for(i = 0; i < reqBdids.length; i++)
    {
      if(!_.isFinite(reqBdids[ i ]))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
    }

    function obj(bdid, success)
    {
      this.bdid = bdid;
      this.state = success;
    }

    bdUtils.isActive(reqBdids)
    .then(function(records)
    {
      var resultObj = [];
      for(i = 0; i < records.length; i++)
      {
        if(records[ i ])
        {
          resultObj.push(new obj(records[ i ].bdid, records[ i ].isactive));
        }
        else
        {
          //All the supplied bdids should be valid
          var error = new Error();
          error.code = 400154;
          error.message = errorCode[ "400154" ];
          throw error;
        }
      }
      return res.json(resultObj);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: getdetail
  * @description: Fetches the details of the bdids supplied. Max 10 entries at a time.
  * @param:
      {
        "bdids": [11]
      }
  * @returns:
    [
      {
        "admins":
        [
          {
            "userid": 2,
            "firstname": "betatester",
            "lastname": "rat",
            "username": "T3st.cat_2",
            "emailid": "tester2.rat@testclient3.com",
            "countrycode": "+92",
            "phonenumber": "9822387102",
            "employeeid": "FTE217345",
            "isactive": false
          }
        ],
        "cbcodes":
        [
          {
            "cbid": "CB00001",
            "cbname": "cbname1",
            "bdid": 2,
            "isactive": true,
            "cbownerid": 1
          }
        ],
        "bdid": 2,
        "bdname": "BusinessDepartment2",
        "isactive": true
      }
    ]
  */
  getdetail: function(req, res)
  {
    var ids = 0;
    var i = 0;
    if(!req.body.bdids || !req.body.bdids.length || req.body.bdids.length > 10)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      ids = _.uniq(req.body.bdids);
      for(i = 0; i < ids.length; i++)
      {
        if(!_.isFinite(ids[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }
    }

    bdUtils.getBd(ids)
    .then(function(data)
    {
      //Check if the all the bds exist in the system
      for(i = 0; i < data.length; i++)
      {
        if(!data[ i ])
        {
          //All the supplied bdids should be valid
          var error = new Error();
          error.code = 400154;
          error.message = errorCode[ "400154" ];
          throw error;
        }
      }
      return res.json(data);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: search
  * @description: searches the model and returns the result. The number of search column and order by criteria are flexible.
  * @param:
    {
      "search":  { "bdname": "B%" },
      "result" : [ "bdid", "bdname", "isactive"],
      "orderby": "bdid"
    }
  * @returns:
    [
      2,
      [
        {
          "bdid": 1,
          "bdname": "BusinessDepartment 1",
          "isactive": true
        },
        {
          "bdid": 2,
          "bdname": "BusinessDepartment 2",
          "isactive": true
        }
      ]
    ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Bd.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Bd.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
};
