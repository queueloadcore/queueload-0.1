/**
* CiNetworkController
*
* @description :: Server-side logic for managing cinetworks
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 25/03/2016    Initial file creation                         SMandal
*
*/

/**
 * @apiGroup Cinetwork
 */

var errorCode = require("../resources/ErrorCodes.js");
var _ = require("lodash");
var Promise = require("bluebird");

module.exports = {
  /**
  @apiGroup Cinetwork
  @apiName listCinetwork
  @api {post} /cinetwork/list List
  @apiDescription This function lists all the Ci Networks in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmly
    c3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28uc
    G9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZW
    VpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ
    3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ"
    -H "Content-Type: application/json" -d '{ "limit": 10, "page": 1}' "http://localhost:1337/cinetwork/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400341
  @apiSuccess (200) {Object[]} network Array of Networks
  @apiSuccessExample {json} Success-Response
    [{
      "cinetworkid": 1,
      "primaryip": "142.17.83.73",
      "networkname": "Upstream-Strike3-Gateway-Log",
      "subnet": "255.255.255.0",
      "gateway": "142.17.83.1",
      "additionalip": "142.17.83.91",
      "additionalsubnet": "255.255.255.0"
      "numberofcis" : 1
    },
    {
      "cinetworkid": 3,
      "primaryip": "161.10.18.83",
      "networkname": "Westwind-DMZ2-DNS",
      "subnet": "255.255.255.0",
      "gateway": "161.10.18.1",
      "additionalip": "161.10.18.14",
      "additionalsubnet": "255.255.255.0"
      "numberofcis" : 1
    }]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    var output = [];
    Cinetwork.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400341;
        error.message = errorCode[ "400341" ];
        throw error;
      }
      else
      {
        output = found;
        var countCis = function(element)
        {
          return Cidetail.count().where({ cinetworkid: element.cinetworkid });
        };
        var actions = found.map(countCis);
        return Promise.all(actions);
      }
    })
    .then(function(result)
    {
      var assign = function(element, index)
      {
        output[ index ].numberofcis = element;
      };
      result.forEach(assign);
      return res.ok(output);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup Cinetwork
  @apiName addCinetwork
  @api {post} /cinetwork/add Add
  @apiDescription On input of all valid parameters this function will add Ci networks
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ
     3fqnXwhQ" -H "Content-Type: application/json" -d
     '{
        "primaryip" : "142.17.83.73",
        "networkname" : "Upstream-Strike3-Gateway-Log",
        "subnet" : "255.255.255.0",
        "gateway" : "142.17.83.1",
        "additionalip" : "142.17.83.91",
        "additionalsubnet" : "255.255.255.0"
      }' "http://localhost:1337/cinetwork/add"
  @apiParam {IP} primaryip Unique IP of the network entity
  @apiParam {String} networkname The name of the network
  @apiParam {IP} subnet Subnet
  @apiParam {IP} gateway Gateway
  @apiParam {IP} [additionalip] Secondary IP of this network asset
  @apiParam {IP} [additionalsubnet] Subnet for the secondary IP
  @apiParamExample {json} Request-Example
    {
      "primaryip" : "10.83.219.34",
      "networkname" : "Upstream-Strike3-Gateway-Log",
      "subnet" : "255.255.255.0",
      "gateway" : "10.83.219.1",
      "additionalip" : "10.46.39.173",
      "additionalsubnet" : "255.255.255.0"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 403502
  @apiSuccess (200) {Object} network Added network
  @apiSuccessExample {json} Success-Response
    {
      "cinetworkid": 1,
      "primaryip": "142.17.83.73",
      "networkname": "Upstream-Strike3-Gateway-Log",
      "subnet": "255.255.255.0",
      "gateway": "142.17.83.1",
      "additionalip": "142.17.83.91",
      "additionalsubnet": "255.255.255.0"
    }
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    //If any of the mandatory attributes are missing give an error
    if(!req.body.primaryip || !req.body.networkname || !req.body.subnet || !req.body.gateway)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var newNetwork = {
        primaryip: req.body.primaryip,
        networkname: req.body.networkname,
        subnet: req.body.subnet,
        gateway: req.body.gateway
      };
      if(req.body.additionalip)
      {
        newNetwork.additionalip = req.body.additionalip;
      }
      if(req.body.additionalsubnet)
      {
        newNetwork.additionalsubnet = req.body.additionalsubnet;
      }
      Cinetwork.create(newNetwork)
      .then(function createdNetwork(network)
      {
        return res.json(network);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Cinetwork
  @apiName getDetail
  @api {post} /cinetwork/getdetail Getdetail
  @apiDescription On input of a Cinetworkid it retrieves the corresponding Cinetwork details
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ
     3fqnXwhQ" -H "Content-Type: application/json" -d
     '{"cinetworkid": 1}' "http://localhost:1337/cinetwork/getdetail"
  @apiParam {Number} cinetworkid ID of the network
  @apiParamExample {json} Request-Example
    {
      "cinetworkid": 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400342
  @apiSuccess (200) {Object} network Details of the requested network
  @apiSuccessExample {json} Success-Response
  {
    "cinetworkid": 1,
    "primaryip": "142.17.83.73",
    "networkname": "Upstream-Strike3-Gateway-Log",
    "subnet": "255.255.255.0",
    "gateway": "142.17.83.1",
    "additionalip": "142.17.83.91",
    "additionalsubnet": "255.255.255.0"
  }
  */
  getdetail: function(req, res)
  {
    if(!req.body.cinetworkid || !(_.isFinite(req.body.cinetworkid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Cinetwork.find({ cinetworkid: req.body.cinetworkid })
      .then(function foundnetwork(networks)
      {
         if(!networks.length)
        {
          var error = new Error();
          error.code = 400342;
          error.message = errorCode[ "400342" ];
          return res.negotiate(error);
        }
        else
        {
          return res.json(networks[ 0 ]);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Cinetwork
  @apiName update
  @api {post} /cinetwork/update Update
  @apiDescription Updates a network
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZm
      lyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY
      28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1w
      bG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJ
      hdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJ
      ScPrQ3fqnXwhQ" -H "Content-Type: application/json" -d
        '{
          "cinetworkid" : 1,
          "networkname" : "JellyStub"
        }' "http://localhost:1337/cinetwork/update"
  @apiParam {Number} primaryip ID of the network entity
  @apiParam {IP} [primaryip] Unique IP of the network entity
  @apiParam {String} [networkname] The name of the network
  @apiParam {IP} [subnet] Subnet
  @apiParam {IP} [gateway] Gateway
  @apiParam {IP} [additionalip] Secondary IP of this network asset
  @apiParam {IP} [additionalsubnet] Subnet for the secondary IP
  @apiParamExample {json} Request-Example
    {
      "cinetworkid" : 1,
      "networkname" : "JellyStub"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400342
  @apiUse 403502
  @apiSuccess (200) {Object} network Details of the updated network
  @apiSuccessExample {json} Success-Response
    {
      "cinetworkid": 1,
      "primaryip": "142.17.83.73",
      "networkname": "JellyStub",
      "subnet": "255.255.255.0",
      "gateway": "142.17.83.1",
      "additionalip": "142.17.83.91",
      "additionalsubnet": "255.255.255.0"
    }
  */

  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.cinetworkid || !(_.isFinite(req.body.cinetworkid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var updatedNetwork = {};
      if(req.body.primaryip)
      {
        updatedNetwork.primaryip = req.body.primaryip;
      }
      if(req.body.networkname)
      {
        updatedNetwork.networkname = req.body.networkname;
      }
      if(req.body.subnet)
      {
        updatedNetwork.subnet = req.body.subnet;
      }
      if(req.body.gateway)
      {
        updatedNetwork.gateway = req.body.gateway;
      }
      if(req.body.additionalip)
      {
        updatedNetwork.additionalip = req.body.additionalip;
      }
      if(req.body.additionalsubnet)
      {
        updatedNetwork.additionalsubnet = req.body.additionalsubnet;
      }
      Cinetwork.update({ cinetworkid: req.body.cinetworkid }, updatedNetwork)
      .then(function(records)
      {
        //Check if the cinetworkid exists
        if(!records.length)
        {
          var error = new Error();
          error.code = 400342;
          error.message = errorCode[ "400342" ];
          return res.negotiate(error);
        }
        else
        {
          return res.json(records[ 0 ]);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Cinetwork
  @apiName search
  @api {post} /cinetwork/search Search
  @apiDescription Searches the model and returns the result. The number of search column and order by criteria are flexible.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ
     3fqnXwhQ" -H "Content-Type: application/json" -d
     '{
        "search":  { "gateway": "142.17.83%" },
        "result" : [ "primaryip", "networkname", "subnet", "gateway"],
        "orderby": "networkname"
      }' "http://localhost:1337/cinetwork/search"
  @apiParam {Object} search Object listing the attrbutes of the searched asset
  @apiParam {String[]} result Array of attributes requested
  @apiParam {String} orderby Attribute to order by the results
  @apiParamExample {json} Request-Example
    {
      "search":  { "gateway": "142.17.83%" },
      "result" : [ "primaryip", "networkname", "subnet", "gateway"],
      "orderby": "networkname"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400053
  @apiSuccess (200) {Array} result First element-Number of records, Second element-Array of results
  @apiSuccessExample {json} Success-Response
  [
    2,
    [
      {
        "primaryip": "142.17.83.73",
        "networkname": "JellyStub",
        "subnet": "255.255.255.0",
        "gateway": "142.17.83.1"
      },
      {
        "primaryip": "142.17.83.74",
        "networkname": "Upstream-Strike3-Gateway-Log",
        "subnet": "255.255.255.0",
        "gateway": "142.17.83.1"
      }
    ]
  ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Cinetwork.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Cinetwork.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
};
