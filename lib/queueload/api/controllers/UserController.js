/* FileName: UserController.js
*  @description: This file describes the various actions in the User controller.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  07/12/2015        Initial file creation                         SMandal
*  02/03/2016        Bring the current code base to                bones
comply with the new philosophy
*
*/

/**
 * ApiGroup User
 */

var userUtils = require("../services/utils/user.js");
var bdUtils = require("../services/utils/bdutils.js");
var queueUtils = require("../services/utils/queueutils.js");
var _ = require("lodash");
var errorCode = require("../resources/ErrorCodes.js");
var Promise = require("bluebird");
var enums = require('../resources/Enums.js');
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var promisedCipherService = Promise.promisifyAll(require("../services/CipherService.js"));

if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{
  controlTest: function(req, res)
  {
    res.ok({ message: "User controller is working. :)" });
  },
  /**
    @api {post} /user/list List
    @apiName listUser
    @apiGroup User
    @apiDescription On input of all valid parameters this function will list all users in the system
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
       curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
       mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
       zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
       mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
       1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
       W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
       '{
          "page" : 1,
          "limit" : 100
        }' "http://localhost:1337/user/list"
    @apiParam {Number} limit Maximum number of results to show in a page
    @apiParam {Number} page The number of page to show
    @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 400101
    @apiUse 403502
    @apiSuccess (200) {Object[]} users Array of users
    @apiSuccessExample {json} Success-Response
      [

      ]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    User.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400101;
        error.message = errorCode[ "400101" ];
        throw error;
      }
      else
      {
        return res.ok(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @api {post} /user/add Add
    @apiName addUser
    @apiGroup User
    @apiDescription On input of all valid parameters this function will add one user to the system
    @apiPermission bdadmin
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
       curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
       mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
       zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
       mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
       1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
       W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
       '{
         "firstname": "Test",
         "lastname": "User",
         "username": "T3st.User11",
         "password": "P@5#word",
         "emailid": "test.user11@testclient1.com",
         "countrycode": "+1",
         "phonenumber": "9999999999",
        }' "http://localhost:1337/user/add"
    @apiParam {String} firstname First name of the user
    @apiParam {String} lastname Last name of the user
    @apiParam {String} username username of the user
    @apiParam {String} password password of the user
    @apiParam {String} emailid email of the user
    @apiParam {String} countrycode countrycode of the phone number of the user
    @apiParam {Number} phonenumber phone number of the user
    @apiParamExample {json} Request-Example
      {
        "firstname": "Test",
        "lastname": "User",
        "username": "T3st.User11",
        "password": "P@5#word",
        "emailid": "test.user11@testclient1.com",
        "countrycode": "+1",
        "phonenumber": "9999999999",
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 403502
    @apiSuccess (200) {Object[]} user user added to the system
    @apiSuccessExample {json} Success-Response
      {
        "userid": 2,
        "firstname": "Test",
        "lastname": "User",
        "username": "T3st.User11",
        "emailid": "test.user11@testclient1.com",
        "countrycode": "+1",
        "phonenumber": "9999999999",
        "isactive": true,
        "passexpired": false,
        "defaultqueue": null,
        "accountid": 1
      }
  */
  add: function(req, res)
  {
    // The API is open for bdadmins and superusers
    if(!req.body.requesterroles.isbdadmin && !req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    //Isactive field is is set to true by default. If supplied, it must be a boolean.
    else if(!req.body.fullname || !req.body.username || !req.body.password ||
      !req.body.emailid || (req.body.isactive !== undefined && typeof(req.body.isactive) !== "boolean"))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    //Only one user may be added at a time
    else if
    (
      Array.isArray(req.body.fullname) ||
      Object.prototype.toString.call(req.body.username) === "[object Array]" ||
      Object.prototype.toString.call(req.body.password) === "[object Array]" ||
      Object.prototype.toString.call(req.body.emailid) === "[object Array]" ||
      Object.prototype.toString.call(req.body.countrycode) === "[object Array]" ||
      Object.prototype.toString.call(req.body.phonenumber) === "[object Array]")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      if(req.body.isactive === undefined ) req.body.isactive = true;
      if(typeof(req.body.passexpired) === 'boolean' && req.body.passexpired === true) req.body.passexpired = true;

      var newUser = {};
      //Create the user
      User.create(req.body)
      .then(function createUser(created)
      {
        newUser = created;
        return Role.create({ userid: created.userid });
      })
      .then(function createdRole()
      {
        return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
      })
      .then(function()
      {
        return res.ok(newUser);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
    @api {post} /user/update Update
    @apiName updateUser
    @apiGroup User
    @apiDescription On input of valid parameters this function will update the fields of a user
    @apiPermission bdadmin
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
       curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
       mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
       zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
       mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
       1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
       W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
       '{
         "userid": 5,
         "emailid": "test.user11@testclient1.com",
       }' "http://localhost:1337/user/update"
    @apiParam {Number} userid user ID of the user
    @apiParam {String} [emailid] email of the user
    @apiParam {String} [countrycode] countrycode of the phone number of the user
    @apiParam {Number} [phonenumber] phone number of the user
    @apiParam {Number} [defaultqueue] Queue ID of the default queue for the user
    @apiParam {Boolean} [passexpired] force user to change password on next login
    @apiParam {String} [oldpass] Old password of the user
    @apiParam {String} [newpass] New password of the user
    @apiParamExample {json} Request-Example
    {
      "userid": 5,
      "emailid": "test.user11@testclient1.com",
    }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 400052
    @apiUse 400104
    @apiUse 400258
    @apiUse 403502
    @apiSuccess (200) {Object[]} user updated user
    @apiSuccessExample {json} Success-Response
    [{
      defaultqueue: null,
      userid: 5,
      firstname: 'Super',
      lastname: 'User',
      username: 'administrator',
      emailid: 'test.user11@testclient1.com',
      countrycode: null,
      phonenumber: null,
      isactive: true
    }]
  */
  update: function(req, res)
  {
    // The API is open for bdadmins, superusers and the user themselves
    if(req.body.userid !== req.body.requesterid && !req.body.requesterroles.isbdadmin && !req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    // Required parameters must be supplied
    if(!req.body.userid  || !(_.isFinite(req.body.userid)) || req.body.password)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else if(req.body.newpass || req.body.oldpass)
    {
      if((req.body.oldpass && !req.body.newpass) || (req.body.newpass && !req.body.oldpass) || req.body.oldpass === req.body.newpass)
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
      else if(typeof req.body.oldpass !== 'string' || typeof req.body.newpass !== 'string')
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
    }
    //Only one user may be updated at a time
    else if
    (Object.prototype.toString.call(req.body.userid) === "[object Array]" ||
     Object.prototype.toString.call(req.body.password) === "[object Array]" ||
     Object.prototype.toString.call(req.body.emailid) === "[object Array]" ||
     Object.prototype.toString.call(req.body.countrycode) === "[object Array]" ||
     Object.prototype.toString.call(req.body.phonenumber) === "[object Array]" ||
     Object.prototype.toString.call(req.body.defaultqueue) === "[object Array]")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var updateObj = {};
    var userObj = {};
    User.findOne({ userid: req.body.userid }).populate('roles')
    .then(function(result)
    {
      // Check if the use exists
      if(!result)
      {
        var error = new Error();
        error.code = 400104;
        error.message = errorCode[ '400104' ];
        throw error;
      }
      // Inactive users cannot be updated
      if(!result.isactive)
      {
        var error = new Error();
        error.code = 400103;
        error.message = errorCode[ "400103" ];
        throw error;
      }
      // If the user is a super user, the requester must be herself or another super user
      else if(result.roles[ 0 ].issuperuser && req.body.userid !== req.body.requesterid && !req.body.requesterroles.issuperuser)
      {
        var error = new Error();
        error.code = 403502;
        error.message = errorCode[ '403502' ];
        throw error;
      }
      else
      {
        userObj = result;
        updateObj.userid = req.body.userid;
        if(req.body.emailid) updateObj.emailid = req.body.emailid;
        if(req.body.countrycode !== undefined) updateObj.countrycode = req.body.countrycode;
        if(req.body.phonenumber !== undefined) updateObj.phonenumber = req.body.phonenumber;
        if(req.body.userid !== req.body.requesterid && typeof(req.body.passexpired) === 'boolean')
        {
          updateObj.passexpired = req.body.passexpired;
        }
        if(req.body.defaultqueue)
        {
          return queueUtils.isMember(req.body.defaultqueue, req.body.userid);
        }
        else
        {
          return updateObj;
        }
      }
    })
    .then(function(result)
    {
      //If the user is a member of the queue
      if(result === true)
      {
        updateObj.defaultqueue = req.body.defaultqueue;
      }
      //If the user is not a member of the queue
      else if(result === false)
      {
        var error = new Error();
        error.code = 400258;
        error.message = errorCode[ '400258' ];
        throw error;
      }
      // If password asked to be reset
      if(req.body.newpass)
      {
        if(req.body.userid !== req.body.requesterid)
        {
          var error = new Error();
          error.code = 403502;
          error.message = errorCode[ '403502' ];
          throw error;
        }
        else
        {
          return Promise.all([ promisedCipherService.comparePasswordAsync(req.body.oldpass, userObj.password), promisedCipherService.hashPasswordAsync(req.body.newpass) ]);
        }
      }
      else
      {
        return updateObj;
      }
    })
    .then(function(result)
    {
      if(req.body.newpass)
      {
        // Check if the supplied old password is incorrect
        if(!result[ 0 ])
        {
          var error = new Error();
          error.code = 401509;
          error.message = errorCode[ '401509' ];
          throw error;
        }
        else
        {
          updateObj.password = result[ 1 ];
        }
      }
      // If no attributes are there to update
      if(_.size(updateObj) <= 1)
      {
        var error = new Error();
        error.code = 400052;
        error.message = errorCode[ "400052" ];
        throw error;
      }
      else
      {
        return User.update({ userid: req.body.userid }, updateObj);
      }
    })
    .then(function(user)
    {
      return res.ok(user);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },

/**
  @api {post} /user/getdetail GetDetail
  @apiName getdetailUser
  @apiGroup User
  @apiDescription Fetches the details of the userids supplied. Max 10 userids allowed.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
     mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
     zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
     mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
     1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
     W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
     '{
       "userids": [3]
      }' "http://localhost:1337/user/getdetail"
  @apiParam {Number} userids array of user IDs
  @apiParamExample {json} Request-Example
  {
    "userids": [3]
  }
  @apiUse 401
  @apiUse 401505
  @apiUse 400052
  @apiUse 400104
  @apiUse 403502
  @apiSuccess (200) {Object[]} user array of user details
  @apiSuccessExample {json} Success-Response
    [
      {
        "administeredbds": [],
        "ownedcbs": [
          {
            "cbid": 1,
            "bdid": 1,
            "isactive": true,
            "cbownerid": 3,
            "isfrozen": false,
            "freezecause": 0
          }
        ],
        "ownedqueues": [
          {
            "queueid": 2,
            "queuename": "secondQueue",
            "cbid": 1,
            "queueemail": "secondqueue@queueload.com",
            "queueownerid": 3,
            "incidentmanagerid": 3,
            "changemanagerid": 3,
            "srtmanagerid": 3,
            "alertl1": "secondqueue@queueload.com",
            "alertl2": "secondqueue@queueload.com",
            "alertl3": "secondqueue@queueload.com",
            "isactive": false,
            "isfrozen": false,
            "freezecause": 0
          }
        ],
        "queuesenrolled": [],
        "servicestateapprovalids": [],
        "roles": [
          {
            "userid": 3,
            "issuperuser": false,
            "isbdadmin": false,
            "isoperator": false
          }
        ],
        "defaultqueue": null,
        "accountid": {
          "accountid": 1,
          "name": "Wayne Enterprise",
          "address": "Gotham",
          "supportmgr": "support@queueload.com",
          "isactive": true,
          "accntadmin": 1
        },
        "userid": 3,
        "firstname": "firstCB",
        "lastname": "owner",
        "username": "firstcbowner",
        "emailid": "firstcbowner@queueload.com",
        "countrycode": "",
        "phonenumber": "",
        "isactive": true,
        "passexpired": false
      }
    ]
  */
  getdetail: function(req, res)
  {
    var ids = 0;
    if(!req.body.userids || !req.body.userids.length || req.body.userids.length > 10)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      ids = _.uniq(req.body.userids);
      for(var i = 0; i < ids.length; i++)
      {
        if(!_.isFinite(ids[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }
    }

    userUtils.getUser(ids)
    .then(function(data)
    {
      //Check if the all the users exist in the system
      for(var i = 0; i < data.length; i++)
      {
        if(!data[ i ])
        {
          var error = new Error();
          error.code = 400104;
          error.message = errorCode[ "400104" ];
          throw error;
        }
      }
      return res.ok(data);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /user/search Search
  @apiName searchUser
  @apiGroup User
  @apiDescription searches the system and returns the result. The number of search column and order by criteria are flexible.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
     mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
     zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
     mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
     1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
     W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
     '{
        "search":  { "username": "T3st.User1%" },
        "result" : [ "firstname", "lastname", "username", "emailid"],
        "orderby": "firstname"
      }' "http://localhost:1337/user/search"
  @apiParam {Object} search Object listing the attrbutes of the searched user
  @apiParam {String[]} result Array of attributes requested
  @apiParam {String} orderby Attribute to order by the results
  @apiParamExample {json} Request-Example
  {
   "search":  { "username": "T3st.User1%" },
   "result" : [ "firstname", "lastname", "username", "emailid"],
   "orderby": "firstname"
  }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400053
  @apiUse 400258
  @apiUse 403502
  @apiSuccess (200) {Object[]} search result
  @apiSuccessExample {json} Success-Response
  [
  49,
  [
    {
      "firstname": "Test",
      "lastname": "User",
      "username": "T3st.User1",
      "emailid": "test.user1@betaclient.com"
    },
    {
      "firstname": "Test",
      "lastname": "User",
      "username": "T3st.User10",
      "emailid": "test.user10@betaclient.com"
    },
    ...
  ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(User.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    User.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.ok([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  },

/**
  @api {post} /user/activation Activation
  @apiName activationUser
  @apiGroup User
  @apiDescription Checks the validity and active status of the userids supplied. If the states are as expected the API proceeds to make changes to the users.
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
     mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
     zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
     mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
     1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
     W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
     '{
       "userids": [ 2],
       "action": true
      }' "http://localhost:1337/user/activation"
  @apiParam {array} userids array of userids to activate/deactivate
  @apiParam {boolean} action true to activate and false to deactivate
  @apiParamExample {json} Request-Example
  {
    "userids": [ 2 ],
    "action": true
  }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400104
  @apiUse 400102
  @apiUse 400103
  @apiUse 400104
  @apiUse 400107
  @apiUse 400109
  @apiUse 400111
  @apiUse 400158
  @apiUse 400258
  @apiUse 400260
  @apiUse 403502
  @apiSuccess (200) {Object[]} updated users result
  @apiSuccessExample {json} Success-Response
  [
    {
      "userid": 2,
      "firstname": "betatester",
      "lastname": "rat",
      "username": "T3st.cat2",
      "emailid": "tester2.rat@testclient3.com",
      "countrycode": "+92",
      "phonenumber": "9822387102",
      "isactive": true,
      "passexpired": null,
      "defaultqueue": null
    }
  ]
  */
  activation: function(req, res)
  {
    // The API is open for bdadmins, superusers
    if(!req.body.requesterroles.isbdadmin && !req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    // An user cannot operate on itself
    else if(_.includes(req.body.userids, req.body.requesterid))
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.userids || req.body.action === undefined || !req.body.userids.length ||
      typeof(req.body.action) !== "boolean")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var userIds = _.uniq(req.body.userids);
    for(var i = 0; i < userIds.length; i++)
    {
      if(!_.isFinite(userIds[ i ]))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
    }
    userUtils.isActive(userIds)
    .then(function(results)
    {
      var i = 0;

      //Check if the supplied IDs are valid
      for(i = 0; i < results.length; i++)
      {
        if(!results[ i ])
        {
          var error = new Error();
          error.code = 400104;
          error.message = errorCode[ "400104" ];
          throw error;
        }
      }

      //For action = true
      if(req.body.action)
      {
        //Check if any of the supplied users are already active
        for(i = 0; i < results.length; i++)
        {
          if(results[ i ].isactive)
          {
            var error = new Error();
            error.code = 400102;
            error.message = errorCode[ "400102" ];
            throw error;
          }
        }
        //Perform activation
        return User.update({ userid: userIds }, { isactive: req.body.action });
      }
      else
      {
        //Check if any of the supplied users are already inactive
        for(i = 0; i < results.length; i++)
        {
          if(!results[ i ].isactive)
          {
            var error = new Error();
            error.code = 400103;
            error.message = errorCode[ "400103" ];
            throw error;
          }
        }
        /*
        Any of the following causes a badRequest:
        User is the owner of an active CB.
        User is the owner of a active queue
        User is the manager of an active queue.
        User is the only superuser in the system
        User is the only member of an active queue.
        User is the only active admin of an active BD
        User has open incidents
        */
        var pUserDetails = userUtils.getUser(userIds);
        var pOpenIncs = Incident.count().where({ or: [ { owneruserid: userIds }, { assigneduserid: userIds } ], state: { '!': enums.incidentStates.CLOSED } });
        var pSuperUsers = userUtils.getSuperUsers(userIds);
        var pIntegrations = Integrationconfig.find({ select: [ 'config' ] });
        var pUserUpdate = User.update({ userid: userIds }, { isactive: req.body.action });

        var deactivateUsers = function()
        {
          var first = Promise.all([ pUserDetails, pOpenIncs, pIntegrations ]);
          var second = first.then(function(records)
          {
            var bds = [], queues = [];
            if(records[ 1 ])
            {
              var error = new Error();
              error.code = 400111;
              error.message = errorCode[ "400111" ];
              throw error;
            }
            else if(records[ 2 ].length)
            {
              var linkedToIntegration = false;
              records[ 2 ].every(function(config)
              {
                if(req.body.userids.indexOf(config.config.owneruserid) !== -1)
                {
                  linkedToIntegration = true;
                  return false;
                }
                else
                {
                  return true;
                }
              });
              if(linkedToIntegration)
              {
                var error = new Error();
                error.code = 400114;
                error.message = errorCode[ "400114" ];
                throw error;
              }
            }
            for(i = 0; i < records[ 0 ].length; i++)
            {
              var ownedCbs = _.filter(records[ 0 ][ i ].ownedcbs, { 'isactive': true });
              if(ownedCbs.length)
              {
                var error = new Error();
                error.code = 400107;
                error.message = errorCode[ "400107" ];
                throw error;
              }
              /* Attributes withheld until introduction of managers
              var incmgrQueues = _.filter(records[ 0 ][ i ].incmgrqueues, { 'isactive': true });
              var chgmgrQueues = _.filter(records[ 0 ][ i ].chgmgrqueues, { 'isactive': true });
              var srtmgrQueues = _.filter(records[ 0 ][ i ].srtmgrqueues, { 'isactive': true });
              if(incmgrQueues.length || chgmgrQueues.length || srtmgrQueues.length)
              {
                var error = new Error();
                error.code = 400109;
                error.message = errorCode[ "400109" ];
                throw error;
              }
              */
              if(records[ 0 ][ i ].administeredbds.length > 0) bds.push(_.map(_.filter(records[ 0 ][ i ].administeredbds, { 'isactive': true }), 'bdid'));
              if(records[ 0 ][ i ].queuesenrolled.length > 0) queues.push(_.map(_.filter(records[ 0 ][ i ].queuesenrolled, { 'isactive': true }), 'queueid'));
            }
            bds = _.uniq(_.flattenDeep(bds));
            queues = _.uniq(_.flattenDeep(queues));
            var pBdDetails = bdUtils.getBd(bds);
            var pQueueDetails = queueUtils.getQueue(queues);
            return Promise.all([ pBdDetails, pQueueDetails, pSuperUsers ]);
          })
          .then(function(records)
          {
            var i = 0;
            for(i = 0; i < records[ 0 ].length; i++)
            {
              var adminIds = _.map(_.filter(records[ 0 ][ i ].admins, { 'isactive': true }), 'userid');
              if(_.difference(adminIds, userIds).length === 0)
              {
                var error = new Error();
                error.code = 400158;
                error.message = errorCode[ '400158' ];
                throw error;
              }
            }

            for(i = 0; i < records[ 1 ].length; i++)
            {
              var memberIds = _.map(_.filter(records[ 1 ][ i ].members, { 'isactive': true }), 'userid');
              if(_.difference(memberIds, userIds).length === 0)
              {
                var error = new Error();
                error.code = 400260;
                error.message = errorCode[ '400260' ];
                throw error;
              }
            }
            // Only a super user can deactivate another superuser
            records[ 2 ] = _.filter(records[ 2 ], undefined);
            if(records[ 2 ].length > 0 && !req.body.requesterroles.issuperuser)
            {
              var error = new Error();
              error.code = 403502;
              error.message = errorCode[ '403502' ];
              throw error;
            }
            return pUserUpdate;
          });
          return Promise.all([ first, second ]);
        };
        return deactivateUsers();
      }
    })
    .then(function(users)
    {
      if(req.body.action) return res.ok(users);
      else return res.ok(users[ 1 ]);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /user/getstatus GetStatus
  @apiName getstatusUser
  @apiGroup User
  @apiDescription Gets the Status of the user ids supplied.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
     mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
     zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
     mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
     1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
     W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
     '{
       "userids": [ 2, 3 ],
      }' "http://localhost:1337/user/getstatus"
  @apiParam {array} userids array of userids
  @apiParamExample {json} Request-Example
  {
    "userids": [ 2, 3 ],
  }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400104
  @apiUse 403502
  @apiSuccess (200) {Object[]} updated users result
  @apiSuccessExample {json} Success-Response
  [
    {
      "userid": 2,
      "status": false
    },
    {
      "userid": 3,
      "status": false
    }
  ]
  */
  getstatus: function(req, res)
  {
    if(!req.body.userids || !req.body.userids.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var ids = _.uniq(req.body.userids);
    var i = 0;
    for(i = 0; i < ids.length; i++)
    {
      if(!_.isFinite(ids[ i ]))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
    }

    userUtils.isActive(ids)
    .then(function(records)
    {
      var resultObj = [];
      var obj = function(userid, status)
      {
        this.userid = userid;
        this.status = status;
      };
      for(i = 0; i < records.length; i++)
      {
        if(records[ i ])
        {
          resultObj.push(new obj(records[ i ].userid, records[ i ].isactive));
        }
        else
        {
          var error = new Error();
          error.code = 400104;
          error.message = errorCode[ "400104" ];
          throw error;
        }
      }
      return res.ok(resultObj);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /user/makesuperuser MakeSuperUser
  @apiName makesuperuser
  @apiGroup User
  @apiDescription This add or removes the superuser role to an user
  @apiPermission superuser
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
     mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
     zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
     mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
     1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
     W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
     '{
       "userid" : 2,
       "action" : true
      }' "http://localhost:1337/user/makesuperuser"
  @apiParam {number} userid user id of the user
  @apiParam {boolean} action true to activate and false to deactivate
  @apiParamExample {json} Request-Example
  {
    "userid" : 2,
    "action" : true
   }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400104
  @apiUse 403502
  @apiSuccess (200) {Object[]} updated users result
  @apiSuccessExample {json} Success-Response
  [
    {
      "id": 2,
      "userid": 2,
      "issuperuser": true,
      "isbdadmin": true,
      "isoperator": false,
      "isuser": true,
      "createdAt": "2016-08-26T08:55:52.000Z",
      "updatedAt": "2016-08-26T14:05:15.000Z"
    }
  ]
  */
  makesuperuser: function(req, res)
  {
    // The API is open only for superusers and one cannot operate on self
    if(!req.body.requesterroles.issuperuser || (req.body.userid === req.body.requesterid))
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else
    {
      var result;
      Role.update({ userid: req.body.userid }, { issuperuser: req.body.action })
      .then(function(role)
      {
        // If user doesn't exist
        if(!role.length)
        {
          var error = new Error();
          error.code = 400104;
          error.message = errorCode[ '400104' ];
          throw error;
        }
        else
        {
          result = role;
          return User.findOne({ userid: req.body.userid });
        }
      })
      .then(function(user)
      {
        return client.multi()
        .hmset(process.env.ACCOUNTID + ":" + user.emailid, [ "signedout", true ])
        .hdel(process.env.ACCOUNTID + ":" + user.emailid, [ "reftkn" ])
        .EXPIRE(process.env.ACCOUNTID + ":" + user.emailid, sails.config.authcfg.accExpiresIn)
        .execAsync();
      })
      .then(function(role)
      {
        return res.ok(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  @api {post} /user/setconfig SetConfig
  @apiName setconfig
  @apiGroup User
  @apiDescription This sets user and global configuration settings
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6
     bnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjEsImZ1bGxuYW1lIjpudWxsLCJmaXJzdG5hbWUiOiJTdXBlciIsI
     mxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb2
     1haW4uY29tIiwiY291bnRyeWNvZGUiOm51bGwsInBob25lbnVtYmVyIjpudWxsLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXN
     hY3RpdmUiOnRydWUsInBhc3NleHBpcmVkIjpmYWxzZSwidXNlcmNvbmZpZyI6bnVsbH0sImlhdCI6MTQ5MDc1OTQ1OSwiZXhwIjox
     NDkwNzgxMDU5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.iR1BcWQXV0CUCgrvg
     apd20kTvysQSFJ3Tt7ht7xMhKE" -H "Content-Type: application/json" -d
     '{
        "settings" :
        [{
          "config" : "tz",
          "value" : "IST",
          "mode" : 2
        }]
      }' "http://localhost:1337/user/setconfig"
  @apiParam {string} config Name of the cofiguration
  @apiParam {string} value Value of the config
  @apiParam {number=1,2} mode Settings type being changed
  @apiParamExample {json} Request-Example
    {
      "settings" :
      [{
        "config" : "tz",
        "value" : "IST",
        "mode" : 2
      }]
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 403502
  */
  setconfig: function(req, res)
  {
    if(!req.body.settings || !Array.isArray(req.body.settings) || !req.body.settings.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var error = {};
    var checkCfgPayload = function(cfg)
    {
      if(!cfg.mode || cfg.mode < 1 || cfg.mode > 2 || !cfg.config || !cfg.value)
      {
        error.code = 400051;
        error.message = errorCode[ '400051' ];
        return false;
      }
      else if(cfg.mode === enums.configTypes.GLOBAL && !req.body.requesterroles.issuperuser)
      {
        error.code = 403502;
        return false;
      }
      else
      {
        return true;
      }
    };
    req.body.settings.forEach(checkCfgPayload);
    if(error.code)
    {
      return res.negotiate(error);
    }
    else
    {
      var pGetConfig = [ Company.findOne({ accountid: req.body.accountid }), User.findOne({ userid: req.body.requesterid }) ];
      Promise.all(pGetConfig)
      .then(function(configurations)
      {
        var hasGlConfig = false;
        var hasLcConfig = false;
        var loggedOut = false;
        var localConfig = false;
        var pCalls = [];

        var configUpdate = function(cfg)
        {
          if(cfg.mode === enums.configTypes.GLOBAL)
          {
            if(!configurations[ 0 ].globalconfig && !hasGlConfig)
            {
              hasGlConfig = !hasGlConfig;
              configurations[ 0 ].globalconfig = {};
            }
            configurations[ 0 ].globalconfig[ cfg.config ] = cfg.value;
            if(!loggedOut)
            {
              pCalls.push(client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true));
              loggedOut = !loggedOut;
            }
          }
          else
          {
            if(!configurations[ 1 ].userconfig && !hasLcConfig)
            {
              hasLcConfig = !hasLcConfig;
              configurations[ 1 ].userconfig = {};
            }
            configurations[ 1 ].userconfig[ cfg.config ] = cfg.value;
            localConfig = true;
          }
        };
        req.body.settings.forEach(configUpdate);
        if(loggedOut)
        {
          pCalls.push(configurations[ 0 ].save());
        }
        if(localConfig)
        {
          pCalls.push(configurations[ 1 ].save());
        }
        return Promise.all(pCalls);
      })
      .then(function()
      {
        return res.ok();
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  @api {post} /user/getconfig GetConfig
  @apiName getconfig
  @apiGroup User
  @apiDescription This gets user and global configuration settings
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6
     bnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjEsImZ1bGxuYW1lIjpudWxsLCJmaXJzdG5hbWUiOiJTdXBlciIsI
     mxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb2
     1haW4uY29tIiwiY291bnRyeWNvZGUiOm51bGwsInBob25lbnVtYmVyIjpudWxsLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXN
     hY3RpdmUiOnRydWUsInBhc3NleHBpcmVkIjpmYWxzZSwidXNlcmNvbmZpZyI6eyJ0aW1lem9uZSI6IklTVCIsImRlZmF1bHR2aWV3
     IjoiYXBwcm92YWwifX0sImlhdCI6MTQ5MDc3NzA4NywiZXhwIjoxNDkwNzk4Njg3LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsI
     mlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.CBN4V1XpUGyzuZJR6WFo1LSAsbJkGUn0udDcQBI1LbY"
     -H "Content-Type: application/json" -d
     '{
        "configs" : ["defaultview"]
      }' "http://localhost:1337/user/getconfig"
  @apiParam {array} config Array of configurations
  @apiParamExample {json} Request-Example
      {
        "configs": ["defaultview"]
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400112
  @apiUse 403502
  @apiSuccess (200) {Object[]} updated users result
  @apiSuccessExample {json} Success-Response
      [{
        "config": "defaultview",
        "value": "approval",
        "mode": 2
      }]
  */
  getconfig: function(req, res)
  {
    if(!req.body.configs || !Array.isArray(req.body.configs) || !req.body.configs.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var pCompanyConfig = Company.findOne({ accountid: req.body.accountid }, { select: [ 'globalconfig' ] });
    var pUserConfig = User.findOne({ userid: req.body.requesterid }, { select: [ 'userconfig' ] });
    Promise.all([ pCompanyConfig, pUserConfig ])
    .then(function(configs)
    {
      if(!configs[ 0 ].globalconfig && !configs[ 1 ].userconfig)
      {
        var error = new Error();
        error.code = 400112;
        error.message = errorCode[ '400112' ];
        throw error;
      }
      var error = {};
      var values = [];
      var getValues = function(cfg)
      {
        if(configs[ 1 ].userconfig && configs[ 1 ].userconfig[ cfg ])
        {
          values.push({ config: cfg, value: configs[ 1 ].userconfig[ cfg ], mode: enums.configTypes.USER });
          return true;
        }
        else if(configs[ 0 ].globalconfig && configs[ 0 ].globalconfig[ cfg ])
        {
         values.push({ config: cfg, value: configs[ 0 ].globalconfig[ cfg ], mode: enums.configTypes.GLOBAL });
         return true;
        }
        else
        {
          error.code = 400112;
          error.message = errorCode[ '400112' ];
          return false;
        }
      };
      req.body.configs.forEach(getValues);
      if(error.code)
      {
        throw error;
      }
      else
      {
        return res.ok(values);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /user/resetconfig ResetConfig
  @apiName resetconfig
  @apiGroup User
  @apiDescription This removes user configuration settings
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6
     bnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjEsImZ1bGxuYW1lIjpudWxsLCJmaXJzdG5hbWUiOiJTdXBlciIsI
     mxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb2
     1haW4uY29tIiwiY291bnRyeWNvZGUiOm51bGwsInBob25lbnVtYmVyIjpudWxsLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXN
     hY3RpdmUiOnRydWUsInBhc3NleHBpcmVkIjpmYWxzZSwidXNlcmNvbmZpZyI6eyJ0aW1lem9uZSI6IklTVCIsImRlZmF1bHR2aWV3
     IjoiYXBwcm92YWwifX0sImlhdCI6MTQ5MDc3NzA4NywiZXhwIjoxNDkwNzk4Njg3LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsI
     mlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.CBN4V1XpUGyzuZJR6WFo1LSAsbJkGUn0udDcQBI1LbY"
     -H "Content-Type: application/json" -d
     '{
        "configs" : ["defaultview"]
      }' "http://localhost:1337/user/resetconfig"
  @apiParam {array} config Array of configurations
  @apiParamExample {json} Request-Example
      {
        "configs": ["defaultview"]
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400112
  @apiUse 403502
  @apiSuccess (200) No return on sucess
  */
  resetconfig: function(req, res)
  {
    if(!req.body.configs || !Array.isArray(req.body.configs) || !req.body.configs.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    User.findOne({ userid: req.body.requesterid }, { select: [ 'userconfig' ] })
    .then(function(configs)
    {
      if(!configs.userconfig)
      {
        var error = new Error();
        error.code = 400112;
        error.message = errorCode[ '400112' ];
        throw error;
      }
      else
      {
        req.body.configs.forEach(function(config)
        {
          if(!configs.userconfig[ config ])
          {
            var error = new Error();
            error.code = 400112;
            error.message = errorCode[ '400112' ];
            throw error;
          }
          else
          {
            delete configs.userconfig[ config ];
          }
        });
        return configs.save();
      }
    })
    .then(function()
    {
      return res.ok();
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
