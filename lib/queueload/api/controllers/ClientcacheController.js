/* FileName     : ClientcacheController.js
*  @description  : This file describes the various actions related to updating the local storage
*                 on the client browser and keeping it in sync with the changes on the server DB.
*
*  Date          Change Description                            Author
*  ---------     ----------------------                        -------
*  17/12/2015    Initial file creation                         SMandal
*  05/10/2016    Adding the APIs                               SMandal
*
*/

/**
 * @apiGroup Clientcache
 */

var Promise = require("bluebird");
var errorCode = require("../resources/ErrorCodes.js");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports = {
  /**
  @apiGroup Clientcache
  @apiName Getdallata
  @api {post} /clientcache/getalldata Getalldata
  @apiDescription Sends key-value pairs to the client for caching
  @apiPermission user
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkb
    WluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicG
    hvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc
    1NjQ4MDQyLCJleHAiOjE0NzU2Njk2NDIsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20i
    fQ.YUjKq-HPEH9_SiNE8f0-PlL0KBB6W2URoIq1eDfXLHw" "http://localhost:1337/clientcache/getalldata"
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiSuccess (200) {Object} cachedata Data to be cached
  @apiSuccessExample {json} Success-Response
    {
      "users": [
        {
          "userid": 1,
          "username": "administrator"
        }
      ],
      "bds": [
        {
          "bdid": 1,
          "bdname": "BusinessDepartment 1"
        }
      ],
      "queues": [
        {
          "queueid": 1,
          "queuename": "TestQueue1"
        }
      ],
      "services": [
        {
          "serviceid": 1,
          "servicename": "Cinderella Spanking Workshop"
        }
      ],
      "cis": [
        {
          "ciid": 1,
          "ciname": "HD63J-34JD3"
        }
      ],
      "citypes": [
        {
          "citypeid": 1,
          "citype": "switch"
        }
      ],
      "vendors": [
        {
          "vendorid": 1,
          "vendorname": "IBM"
        }
      ],
      "cienvironments": [
        {
          "envid": 1,
          "envname": "development"
        }
      ],
      "timestamp": 1475757978527
    }
  */
  getalldata: function(req, res) {
    var result;
    var expired = false;
    return client.hmgetAsync(process.env.ACCOUNTID + ":clientcache", "expired")
    .then(function(value)
    {
      if(value && value[ 0 ] === 'false')
      {
        return client.hmgetAsync(process.env.ACCOUNTID + ":clientcache", "data", "timestamp");
      }
      else
      {
        expired = !expired;
        var pData = Promise.all([
          User.find({ select: [ "userid", "username" ] }),
          Bd.find({ select: [ "bdid", "bdname" ] }),
          Queue.find({ select: [ "queueid", "queuename" ] }),
          Service.find({ select: [ "serviceid", "servicename" ] }),
          Cidetail.find({ select: [ "ciid", "ciname" ] }),
          Citype.find({ select: [ "citypeid", "citype" ] }),
          Vendor.find({ select: [ "vendorid", "vendorname" ] }),
          Cienvironment.find({ select: [ "envid", "envname" ] }),
          Company.findOne({ accountid: req.body.accountid, select: [ "globalconfig" ] })
        ]);
        return pData;
      }
    })
    .then(function(data)
    {
      if(!expired)
      {
        result = data;
        return result;
      }
      else
      {
        var cacheobj = function(users, bds, queues, services, cis, citypes, vendors, cienvironments, globalconfig)
        {
          this.users = users;
          this.bds = bds;
          this.queues = queues;
          this.services = services;
          this.cis = cis;
          this.citypes = citypes;
          this.vendors = vendors;
          this.cienvironments = cienvironments;
          this.globalconfig = globalconfig;
        };
        result = new cacheobj(data[ 0 ], data[ 1 ], data[ 2 ], data[ 3 ], data[ 4 ], data[ 5 ], data[ 6 ], data[ 7 ], data[ 8 ].globalconfig);
        var stringified = JSON.stringify(result);
        result.timestamp = Date.now();
        return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "data", stringified, "timestamp", result.timestamp, "expired", false);
      }
    })
    .then(function(value)
    {
      if(!expired)
      {
        var response = JSON.parse(result[ 0 ]);
        response.timestamp = parseInt(result [ 1 ]);
        return res.ok(response);
      }
      else
      {
        return res.ok(result);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup Clientcache
  @apiName isvalid
  @api {post} /clientcache/isvalid isValid
  @apiDescription Validates the cache and sends a new data set incase of validation failure.
      Sending a timestamp earlier then 1 results in error.
  @apiPermission user
  @apiHeader (Content-Type) {Strng} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmly
    c3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRta
    W5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW
    1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc1NjU0MTY3LCJleHAiOjE0NzU2NzU3NjcsImF
    1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.iZAEucKg4Oaf2lwuPi4LE3sChJdn31rSa
    BagO7zu0Fg" -H "Content-Type: application/json" -d '{ "timestamp" : 1475675137161 }'
    "http://localhost:1337/clientcache/isvalid"
  @apiParam {Number} timestamp Timestamp of the existig cache on the client
  @apiParamExample {json} Request-Example
    {
      "timestamp" : 1475675137161
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiSuccess (200) {Object} cachedata Data to be cached
  @apiSuccessExample {json} 'Client Cache Invalid'
    {
      "users": [
        {
          "userid": 1,
          "username": "administrator"
        }
      ],
      "bds": [
        {
          "bdid": 1,
          "bdname": "BusinessDepartment 1"
        }
      ],
      "queues": [
        {
          "queueid": 1,
          "queuename": "TestQueue1"
        }
      ],
      "services": [
        {
          "serviceid": 1,
          "servicename": "Cinderella Spanking Workshop"
        }
      ],
      "cis": [
        {
          "ciid": 1,
          "ciname": "HD63J-34JD3"
        }
      ],
      "citypes": [
        {
          "citypeid": 1,
          "citype": "switch"
        }
      ],
      "vendors": [
        {
          "vendorid": 1,
          "vendorname": "IBM"
        }
      ],
      "cienvironments": [
        {
          "envid": 1,
          "envname": "development"
        }
      ],
      "timestamp": 1475757978527
    }
  @apiSuccessExample {json} 'Client Cache Valid'
    true
  */
  isvalid: function(req, res)
  {
    var valid = true;
    var gencache = false;
    var result;
    if(typeof(req.body.timestamp) === 'undefined')
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    client.hmgetAsync(process.env.ACCOUNTID + ":clientcache", "expired", "timestamp")
    .then(function(values)
    {
      // Check if the cache on server is valid and the client has the same copy
      if(values[ 0 ] === 'false')
      {
        if(req.body.timestamp == values[ 1 ])
        {
          return true;
        }
        else
        {
          valid = !valid;
          return client.hmgetAsync(process.env.ACCOUNTID + ":clientcache", "data", "timestamp");
        }
      }
      else
      {
        valid = !valid;
        gencache = !gencache;
        var pData = Promise.all([
          User.find({ select: [ "userid", "username" ] }),
          Bd.find({ select: [ "bdid", "bdname" ] }),
          Queue.find({ select: [ "queueid", "queuename" ] }),
          Service.find({ select: [ "serviceid", "servicename" ] }),
          Cidetail.find({ select: [ "ciid", "ciname" ] }),
          Citype.find({ select: [ "citypeid", "citype" ] }),
          Vendor.find({ select: [ "vendorid", "vendorname" ] }),
          Cienvironment.find({ select: [ "envid", "envname" ] }),
          Company.findOne({ accountid: req.body.accountid, select: [ "globalconfig" ] })
        ]);
        return pData;
      }
    })
    .then(function(data)
    {
      if(valid)
      {
        return true;
      }
      else
      {
        if(!gencache)
        {
          return data;
        }
        else
        {
          var cacheobj = function(users, bds, queues, services, cis, citypes, vendors, cienvironments, globalconfig)
          {
            this.users = users;
            this.bds = bds;
            this.queues = queues;
            this.services = services;
            this.cis = cis;
            this.citypes = citypes;
            this.vendors = vendors;
            this.cienvironments = cienvironments;
            this.globalconfig = globalconfig;
          };
          result = new cacheobj(data[ 0 ], data[ 1 ], data[ 2 ], data[ 3 ], data[ 4 ], data[ 5 ], data[ 6 ], data[ 7 ], data[ 8 ].globalconfig);
          var stringified = JSON.stringify(result);
          result.timestamp = Date.now();
          return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "data", stringified, "timestamp", result.timestamp, "expired", false);
        }
      }
    })
    .then(function(value)
    {
      if(valid)
      {
        return res.ok(valid);
      }
      else
      {
        if(gencache)
        {
          return res.ok(result);
        }
        else
        {
          var response = JSON.parse(value[ 0 ]);
          response.timestamp = parseInt(value [ 1 ]);
          return res.ok(response);
        }
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
