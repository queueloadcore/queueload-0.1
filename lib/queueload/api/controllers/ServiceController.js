/* FileName: ServiceController.js
*  @description: Server-side logic for managing services.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  23/03/2016        Initial file creation                         SMandal
*
*/

/**
 * @apiGroup Service
 */

var errorCode = require("../resources/ErrorCodes.js");
var servUtils = require("../services/utils/serviceutils.js");
var userUtils = require("../services/utils/user.js");
var enums = require("../resources/Enums.js");
var _ = require("lodash");
var Promise = require('bluebird');
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{
  /**
  @apiGroup Service
  @apiName listService
  @api {post} /service/list List
  @apiDescription This function lists all the services in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hc
    mNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW
    1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc
    1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2Ayj
    N_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ" -d
    '{
      "page" : 1,
      "limit" : 100
    }' "http://localhost:1337/service/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400361
  @apiSuccess (200) {Object[]} service Array of Services
  @apiSuccessExample {json} Success-Response
    [
      {
        "cbid": 1,
        "servicestateid": 1,
        "serviceid": 1,
        "servicename": "Wayne Trainning Service",
        "details": "Trainning service for Wayne Family",
        "isfrozen": false,
        "freezecause": "0"
      }
    ]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    Service.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      // If there are no services
      if(!found.length)
      {
        var error = new Error();
        error.code = 400361;
        error.message = errorCode[ "400361" ];
        throw error;
      }
      else
      {
        return res.json(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup Service
  @apiName addService
  @api {post} /service/add Add
  @apiDescription On input of all valid parameters this function will add a service
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjY
     yNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0
     .2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ" -d
     '{
        "servicename" : "Cinderella Spanking Workshop",
        "cbid" : 1,
        "details" : "The castle has much more to offer."
      }' "http://localhost:1337/service/add"
  @apiParam {String} servicename Name of the service
  @apiParam {Number} cbid Chargeback code of the service
  @apiParam {String} [details] Description of service
  @apiParamExample {json} Request-Example
    {
      "servicename" : "Cinderella Spanking Workshop",
      "cbid" : 1,
      "details" : "The castle has much more to offer."
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400203
  @apiUse 400204
  @apiUse 400208
  @apiUse 403502
  @apiSuccess (200) {Object} network Added network
  @apiSuccessExample {json} Success-Response
    {
      "serviceid": 1,
      "servicename": "Cinderella Spanking Workshop",
      "cbid": 1,
      "servicestateid": 1,
      "details": "The castle has much more to offer.",
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  add: function(req, res)
  {
    // The API is open only for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.servicename || !req.body.cbid || !_.isFinite(req.body.cbid) || req.body.servicestateid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else if(Array.isArray(req.body.servicename))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var result;
      var pGetCb = Cb.findOne({ cbid: req.body.cbid });
      var pGetUser = userUtils.getBdAdmins([ req.body.requesterid ]);
      Promise.all([ pGetCb, pGetUser ])
      .then(function(status)
      {
        // Check if the Chargeback code exists
        if(!status[ 0 ])
        {
          var error = new Error();
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
        // Check if the chargeback code is frozen
        else if(status[ 0 ].isfrozen)
        {
          var error = new Error();
          error.code = 400208;
          error.message = errorCode[ "400208" ];
          throw error;
        }
        // Check if the chargeback code is active
        else if(!status[ 0 ].isactive)
        {
          var error = new Error();
          error.code = 400203;
          error.message = errorCode[ "400203" ];
          throw error;
        }
        // Check if the requester is an admin of the bd
        else if(!_.includes(_.map(status[ 1 ][ 0 ].administeredbds, 'bdid'), status[ 0 ].bdid))
        {
          var error = new Error();
          error.code = 403501;
          error.message = errorCode [ '403501' ];
          throw error;
        }
        else
        {
          var servicedetails = "";
          if(req.body.details)
          {
            servicedetails =  req.body.details;
          }
          return Service.create({ servicename: req.body.servicename, cbid: req.body.cbid, details: servicedetails });
        }
      })
      .then(function created(service)
      {
        result = service;
        return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
      })
      .then(function(value)
      {
        return res.json(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Service
  @apiName getDetail
  @api {post} /service/getdetail Getdetail
  @apiDescription On input of a ServiceId it retrieves the corresponding service details
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjY
     yNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0
     .2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ" -d '{"serviceid": 1}' "http://localhost:1337/service/getdetail"
  @apiParam {Number} serviceid ID of the service
  @apiParamExample {json} Request-Example
    {
      "serviceid": 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400362
  @apiSuccess (200) {Object} service Details of the requested service
  @apiSuccessExample {json} Success-Response
    {
      "ciids":
       [
        {
          "ciid": 1,
          "ciname": "HJ4K2-74JK",
          "assettag": "JFU5-73JD",
          "cistateid": 1,
          "contractid": null,
          "warrantyexpiry": null,
          "maintenanceend": null,
          "customerfacing": false,
          "isundermaint": true,
          "systemsoftware1": null,
          "systemsoftware2": null,
          "systemsoftware3": null,
          "customapp1": null,
          "customapp2": null,
          "customapp3": null,
          "installationdate": 1480516283,
          "isfrozen": false,
          "freezecause": 0,
          "citypeid": 1,
          "envid": 1,
          "supportqueue": null,
          "vendorid": 1,
          "machineid": null,
          "cinetworkid": null,
          "ciownerid": 15,
          "locationid": 1
        },
        {
          "ciid": 3,
          "ciname": "JSDKA-23L",
          "assettag": "FJ72-KWDJ",
          "cistateid": 1,
          "contractid": null,
          "warrantyexpiry": null,
          "maintenanceend": null,
          "customerfacing": false,
          "isundermaint": true,
          "systemsoftware1": null,
          "systemsoftware2": null,
          "systemsoftware3": null,
          "customapp1": null,
          "customapp2": null,
          "customapp3": null,
          "installationdate": 1480516283,
          "isfrozen": false,
          "freezecause": 0,
          "citypeid": 1,
          "envid": 1,
          "supportqueue": null,
          "vendorid": 1,
          "machineid": null,
          "cinetworkid": null,
          "ciownerid": 15,
          "locationid": 1
        }
      ],
      "crntsrvsundappvl": [],
      "newsrvsundappvl": [],
      "serviceidsforapproval": [],
      "incidents":
      [
        {
          "incidentid": 2,
          "state": 5,
          "title": "This is a test incident ticket",
          "description": "Description of the test incident ticket",
          "serviceid": 1,
          "ciid": 1,
          "ownerqueueid": 1,
          "owneruserid": 9,
          "assignedqueueid": 3,
          "assigneduserid": 13,
          "startedat": 1479845009,
          "endtime": 1480084112,
          "severity": 2,
          "impact": 3,
          "vendorid": null,
          "vendorsr": null,
          "pendinginc": null,
          "journal": [Object],
          "resolutioncode": 1,
          "resolutionnotes": "resolved",
          "closurecomments": null,
          "visibility": "owner",
          "rating": null,
          "updatedAt": '2016-11-25T14:28:32.000Z'
        },
        {
          "incidentid": 3,
          "state": 2,
          "title": "This is a test incident ticket",
          "description": "Description of the test incident ticket",
          "serviceid": 1,
          "ciid": null,
          "ownerqueueid": 2,
          "owneruserid": 11,
          "assignedqueueid": 3,
          "assigneduserid": 13,
          "startedat": 1480084108,
          "endtime": null,
          "severity": 3,
          "impact": 4,
          "vendorid": null,
          "vendorsr": null,
          "pendinginc": null,
          "journal": [Object],
          "resolutioncode": null,
          "resolutionnotes": null,
          "closurecomments": null,
          "visibility": "operator",
          "rating": null,
          "updatedAt": "2016-11-25T14:28:30.000Z"
        }
      ],
      "cbid:
      {
        "cbid": 1,
        "isactive": true,
        "isfrozen": false,
        "freezecause": 0,
        "bdid": 1,
        "cbownerid": 6
      },
      "serviceid": 1,
      "servicename": "incservice1",
      "servicestateid": 2,
      "details": "inc service1 details",
      "isfrozen": false,
      "freezecause": 0
    }
  */
  getdetail: function(req, res)
  {
    if(!req.body.serviceid || !(_.isFinite(req.body.serviceid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Service.findOne({ serviceid: req.body.serviceid })
      .populate("ciids")
      .populate("serviceidsforapproval", { isactive: true })
      .populate("cbid")
      .populate("incidents", { state: { '!': [ enums.incidentStates.CLOSED ] } })
      .populate("supportqueue")
      .then(function showRec(service)
      {
        // Check if the service ID exists
        if(!service)
        {
          var error = new Error();
          error.code = 400362;
          error.message = errorCode[ "400362" ];
          return res.negotiate(error);
        }
        else
        {
          return res.json(service);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Service
  @apiName update
  @api {post} /service/update Update
  @apiDescription Updates a service
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I
      m1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv
      bmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ
      3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2
      9tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ" -d
      '{
        "serviceid" : 2,
        "servicename" : "Wayne Trainning Service",
        "details" : "Trainning service for Wayne Family"
      }' "http://localhost:1337/service/update"
  @apiParam {Number} serviceid ID of the service
  @apiParam {String} [servicename] Name of the service
  @apiParam {String} [details] Short detail of the service
  @apiParam {Number} [cbid] Chargeback code of the service
  @apiParamExample {json} Request-Example
    {
      "serviceid" : 1,
      "servicename" : "Wayne Trainning Service",
      "details" : "Trainning service for Wayne Family"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400362
  @apiUse 400363
  @apiUse 400376
  @apiUse 403502
  @apiSuccess (200) {Object} service Details of the updated service
  @apiSuccessExample {json} Success-Response
    {
      "serviceid": 1,
      "servicename": "Wayne Trainning Service",
      "cbid": 1,
      "servicestateid": 1,
      "details": "Trainning service for Wayne Family",
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  update: function(req, res)
  {
    // The API is open only for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.serviceid || req.body.servicestateid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var result;
      var pGetService = Service.findOne({ serviceid: req.body.serviceid }).populate('cbid');
      var pGetUser = userUtils.getBdAdmins([ req.body.requesterid ]);
      Promise.all([ pGetService, pGetUser ])
      .then(function showRec(service)
      {
        // Check if the service ID exists
        if(!service[ 0 ])
        {
          var error = new Error();
          error.code = 400362;
          error.message = errorCode[ "400362" ];
          throw error;
        }

        // Check if the service is frozen
        else if(service[ 0 ].isfrozen)
        {
          var error = new Error();
          error.code = 400363;
          error.message = errorCode[ "400363" ];
          throw error;
        }
        // Check if the requester is an admin of the bd
        else if(!_.includes(_.map(service[ 1 ][ 0 ].administeredbds, 'bdid'), service[ 0 ].cbid.bdid))
        {
          var error = new Error();
          error.code = 403501;
          error.message = errorCode [ '403501' ];
          throw error;
        }
        else
        {
          var updObj = {};
          // Check if the service id is in Approved state
          if(req.body.cbid)
          {
            if(service[ 0 ].servicestateid > enums.serviceStateIds.REGISTRATION)
            {
              var error = new Error();
              error.code = 400376;
              error.message = errorCode[ "400376" ];
              throw error;
            }
            else
            {
              updObj.cbid = req.body.cbid;
            }
          }
          if(req.body.servicename)
          {
            updObj.servicename = req.body.servicename;
          }
          if(typeof(req.body.details) !== undefined)
          {
            updObj.details = req.body.details;
          }
          return Service.update({ serviceid: req.body.serviceid }, updObj);
        }
      })
      .then(function updatedServices(services)
      {
        result = services[ 0 ];
        return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
      })
      .then(function(value)
      {
        return res.ok(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  @apiGroup Service
  @apiName next
  @api {post} /service/next Next
  @apiDescription Initiates the state transition of a service
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I
      m1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv
      bmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ
      3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2
      9tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ" -d
      '{
        "serviceid" : 1
      }' "http://localhost:1337/service/next"
  @apiParam {Number} serviceid ID of the service
  @apiParamExample {json} Request-Example
    {
      "serviceid" : 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400208
  @apiUse 400362
  @apiUse 400363
  @apiUse 400364
  @apiUse 400365
  @apiUse 400371
  @apiUse 403502
  @apiSuccess (200) {Object} service Details of the service
  @apiSuccessExample {json} 'Registration' ==> 'Approved'
    {
      "serviceid": 1,
      "servicename": "Wayne Trainning Service",
      "cbid": 1,
      "servicestateid": 1,
      "details": "Trainning service for Wayne Family",
      "isfrozen": true,
      "freezecause": "5"
    }
  @apiSuccessExample {json} 'Approved' ==> 'Archived'
    {
      "serviceid": 1,
      "servicename": "Wayne Trainning Service",
      "cbid": 1,
      "servicestateid": 3,
      "details": "Trainning service for Wayne Family",
      "isfrozen": false,
      "stateapprovalids": null,
      "freezecause": "0"
    }
  */
  next: function(req, res)
  {
    // The API is open only for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.serviceid || !(_.isFinite(req.body.serviceid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var serviceDet = {};
      var pGetService = Service.findOne({ serviceid: req.body.serviceid }).populate("ciids").populate('cbid');
      var pGetOpenIncs = Incident.count().where({ serviceid: req.body.serviceid, state: { '!': enums.incidentStates.CLOSED } });
      var pGetBds = userUtils.getBdAdmins([ req.body.requesterid ]);

      Promise.all([ pGetService, pGetOpenIncs, pGetBds ])
      .then(function(service)
      {
        serviceDet = service[ 0 ];
        // If service doesn't exist then error out.
        if(!serviceDet)
        {
          var error = new Error();
          error.code = 400362;
          error.message = errorCode[ "400362" ];
          throw error;
        }
        else if(serviceDet.servicestateid < 1 || serviceDet.servicestateid > enums.serviceStateIds.APPROVED)
        {
          // Service cannot advance to any other state
          var error = new Error();
          error.code = 400365;
          error.message = errorCode[ "400365" ];
          throw error;
        }
        // Check if there are open incidents
        else if(service[ 1 ])
        {
          var error = new Error();
          error.code = 400211;
          error.message = errorCode[ "400211" ];
          throw error;
        }
        // If service is frozen then error out.
        else if(serviceDet.isfrozen)
        {
          var error = new Error();
          error.code = 400363;
          error.message = errorCode[ "400363" ];
          throw error;
        }
        // If the service is associated with any CI
        else if(serviceDet.ciids.length)
        {
          var error = new Error();
          error.code = 400364;
          error.message = errorCode[ "400364" ];
          throw error;
        }
        // Check if the CBid is frozen
        else if(serviceDet.cbid.isfrozen)
        {
          var error = new Error();
          error.code = 400208;
          error.message = errorCode[ "400208" ];
          throw error;
        }
        // Check if the requester is an admin of the bd
        else if(!_.includes(_.map(service[ 2 ][ 0 ].administeredbds, 'bdid'), serviceDet.cbid.bdid))
        {
          var error = new Error();
          error.code = 403501;
          error.message = errorCode [ '403501' ];
          throw error;
        }
        // Check if the service doesn't have a support queue
        else if(!service[ 0 ].supportqueue)
        {
          var error = new Error();
          error.code = 400388;
          error.message = errorCode [ '400388' ];
          throw error;
        }
        else
        {
          // Retrieve the outstanding approvals for this service
          return Serviceapproval.find({ approverserviceid: req.body.serviceid, isactive: true });
        }
      })
      .then(function(approvals)
      {
        // If the service has any active approvals associated with it
        if(approvals.length)
        {
          var error = new Error();
          error.code = 400371;
          error.message = errorCode[ "400371" ];
          throw error;
        }

        // If the service is in Registration phase
        if(serviceDet.servicestateid === enums.serviceStateIds.REGISTRATION)
        {
          // Send approval to the chargeback code owner
          return servUtils.sendApproval(serviceDet.serviceid, serviceDet.serviceid, serviceDet.cbid.cbid, serviceDet.serviceid, "state");
        }

        // If the service is in Approved phase
        else
        {
          // Change the state to Archived
          return Service.update({ serviceid: req.body.serviceid }, { servicestateid: serviceDet.servicestateid + 1 });
        }
      })
      .then(function(result)
      {
        if(serviceDet.servicestateid === enums.serviceStateIds.REGISTRATION)
        {
          return Service.update({ serviceid: req.body.serviceid }, { isfrozen: true, freezecause: enums.freezeCauses.PENDING_SERVICE_STATECHANGE });
        }
        else
        {
          return result;
        }
      })
      .then(function(result)
      {
        return res.json(result[ 0 ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Service
  @apiName approve
  @api {post} /service/approve Approve
  @apiDescription Approves service state transfer requests
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} State Transition
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZmlyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6I
      mZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9u
      ZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc
      yNjUxNDA1LCJleHAiOjE0NzI2NzMwMDUsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb2
      0ifQ.q71r83U-qIXUc8gQB2CJv2HNRCtz0KMrpGGQMCbwO4Y" -d
        '{
          "type" : "state",
          "approvalid" : 1,
          "isapproved" : true,
          "serviceid" : 1
        }' "http://localhost:1337/service/approve"
  @apiExample {curl} Association
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZmlyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6I
      mZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9u
      ZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc
      yNjUxNDA1LCJleHAiOjE0NzI2NzMwMDUsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb2
      0ifQ.q71r83U-qIXUc8gQB2CJv2HNRCtz0KMrpGGQMCbwO4Y" -d
        '{
          "type" : "assoc",
          "approvalid" : 4,
          "serviceid" : 1,
          "isapproved" : true,
          "ciid" : 1
        }' "http://localhost:1337/service/approve"
  @apiParam {String} type Type of the request. Possible values are "assoc", "state"
  @apiParam {Number} approvalid ID of the approval
  @apiParam {Number} [serviceid] ID of the service
  @apiParam {Boolean} isapproved 'true' for approved, 'false' for reject
  @apiParam {Number} [ciid] ID of the CI
  @apiParamExample {json} State Transition
    {
      "type" : "state",
      "approvalid" : 1,
      "isapproved" : true,
      "serviceid" : 1
    }
  @apiParamExample {json} Association
    {
      "type" : "assoc",
      "approvalid" : 4,
      "serviceid" : 1,
      "isapproved" : true,
      "ciid" : 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400366
  @apiUse 400367
  @apiUse 400368
  @apiUse 400370
  @apiUse 400375
  @apiSuccess (200) {Object} service Details of the service
  @apiSuccess (200) {Object} approval Details of the approval
  @apiSuccessExample {json} State Transition
    {
      "serviceid": 1,
      "servicename": "Wayne Trainning Service",
      "cbid": 1,
      "servicestateid": 2,
      "details": "Trainning service for Wayne Family",
      "isfrozen": false,
      "freezecause" : "0"
    }
  @apiSuccessExample {json} Association - Pending approvals
    {
      "approverid": 1,
      "approverserviceid": 2,
      "ciid": 1,
      "approvalid": 5,
      "isapproved": null,
      "type": "assoc",
      "isactive": true
    }
  @apiSuccessExample {json} Association - Last approval
    {
      "ciid": 1,
      "ciname": "HSG4-JSJKAQM-T",
      "assettag": "SAMAK82KD",
      "cistateid": 1,
      "citypeid": 3,
      "envid": 2,
      "supportqueue": 1,
      "contractid": null,
      "vendorid": 2,
      "warrantyexpiry": null,
      "maintenanceend": null,
      "customerfacing": false,
      "isundermaint": true,
      "machineid": null,
      "cinetworkid": null,
      "systemsoftware1": "Splunk",
      "systemsoftware2": null,
      "systemsoftware3": null,
      "customapp1": null,
      "customapp2": null,
      "customapp3": null,
      "ciownerid": 8,
      "locationid": 1,
      "installationdate": 1480516283,
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  approve: function(req, res)
  {
    if((req.body.type !== "state" && req.body.type !== "assoc") || !(_.isFinite(req.body.approvalid)) ||
      typeof(req.body.isapproved) !== "boolean" || req.body.isapproved === "null" || !req.body.serviceid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var initialApproval = {};
      var assocComplete = false;
      Serviceapproval.findOne({ approvalid: req.body.approvalid })
      .populate("newserviceids")
      .populate("crntsrvids")
      .then(function(approval)
      {
        initialApproval = approval;

        // If the approvalID doesn't exist
        if(!approval)
        {
          var error = new Error();
          error.code = 400368;
          error.message = errorCode[ "400368" ];
          throw error;

        }

        // If the approval ID is already processed
        else if(!approval.isactive)
        {
          var error = new Error();
          error.code = 400366;
          error.message = errorCode[ "400366" ];
          throw error;
        }

        // If the user is not authorised to approve
        else if(approval.approverid !== req.body.requesterid)
        {
          var error = new Error();
          error.code = 400367;
          error.message = errorCode[ "400367" ];
          throw error;
        }

        // If the approval is not for the correct service
        else if(approval.approverserviceid !== req.body.serviceid)
        {
          var error = new Error();
          error.code = 400370;
          error.message = errorCode[ "400370" ];
          throw error;
        }

        // If an assoc request approval is missiing ciid
        else if(req.body.type === "assoc" && !req.body.ciid)
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          throw error;
        }

        // If the approval is not for the correct CIid
        else if(req.body.type === "assoc" && approval.ciid !== req.body.ciid)
        {
          var error = new Error();
          error.code = 400375;
          error.message = errorCode[ "400375" ];
          throw error;
        }
        else
        {
          // Update the approval with the appropriate "isapproved" value
          return Serviceapproval.update({ approvalid: req.body.approvalid }, { isapproved: req.body.isapproved, isactive: false });
        }
      })
      .then(function(records)
      {
        return Service.findOne({ serviceid: records[ 0 ].approverserviceid });
      })
      .then(function(record)
      {
        // If it is a state transition request
        if(req.body.type === "state")
        {
          // If request is approved
          if(req.body.isapproved)
          {
            return Service.update({ serviceid: record.serviceid }, { isfrozen: false, servicestateid: enums.serviceStateIds.APPROVED, freezecause: enums.freezeCauses.NOT_FROZEN });
          }

          // If the request is declined
          else
          {
            return Service.update({ serviceid: record.serviceid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
          }
        }

        // If it is an Service and Ci assoication request
        else if(req.body.type === "assoc")
        {
            return Promise.all([ Serviceapproval.find({ ciid: req.body.ciid, isactive: true }), Cidetail.findOne({ ciid: req.body.ciid }) ]);
        }
        else
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          throw error;
        }
      })
      .then(function(result)
      {
        // If it is a state transition request
        if(req.body.type === "state")
        {
          return result;
        }

        // If it is an Service and CI assoication request
        else
        {
          // If the request is approved
          if(req.body.isapproved)
          {
            // If there are more approvals to obtain
            if(result[ 0 ].length)
            {
              return result[ 0 ];
            }

            // If this is the last approval
            else
            {
              assocComplete = !assocComplete;
              var serviceList = [];
              for(var iter = 0; iter < initialApproval.newserviceids.length; iter++)
              {
                serviceList.push(initialApproval.newserviceids[ iter ].serviceid);
              }
              if(result[ 1 ].primaryservice)
              {
                if(serviceList.length)
                {
                  return Cidetail.update({ ciid: req.body.ciid }, { serviceids: serviceList, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
                }
                else
                {
                  return Cidetail.update({ ciid: req.body.ciid }, { serviceids: serviceList, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN, primaryservice: null });
                }
              }
              else
              {
                return Cidetail.update({ ciid: req.body.ciid }, { serviceids: serviceList, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN, primaryservice: req.body.serviceid });
              }
            }
          }

          // If the request is declined
          else
          {
            var approvalUpd = Serviceapproval.update({ ciid: req.body.ciid, isactive: true }, { isactive: false });
            var ciUpdate = Cidetail.update({ ciid: req.body.ciid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            var pResults = Promise.all([ approvalUpd, ciUpdate ]);
            return pResults;
          }
        }
      })
      .then(function(result)
      {
        if(req.body.type === "assoc" && !req.body.isapproved)
        {

          return res.json(result[ 1 ]);
        }
        else
        {
          return res.json(result[ 0 ]);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Service
  @apiName assoc
  @api {post} /service/assoc Assoc
  @apiDescription associated CI to Service
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I
      m1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv
      bmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ
      3MjcwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2
      9tIn0.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM" -d
      '{
        "serviceids": [4],
        "ciid" : 1
      }' "http://localhost:1337/service/assoc"
  @apiParam {Number[]} serviceid Array of service IDs
  @apiParam {Number} ciid Name of the service
  @apiParamExample {json} Request-Example
    {
      "serviceids": [4],
      "ciid" : 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400362
  @apiUse 400363
  @apiUse 400369
  @apiUse 400372
  @apiUse 400373
  @apiUse 400382
  @apiUse 400383
  @apiUse 400387
  @apiUse 403502
  @apiSuccess (200) {Object[]} approval List of approvals to obtain
  @apiSuccessExample {json} Success-Response
    [
      [
        {
          "approvalid": 4,
          "isapproved": null,
          "approverid": 2,
          "approverserviceid": 1,
          "ciid": 1,
          "type": "assoc",
          "isactive": true,
          "createdAt": "2016-05-10T16:50:21.000Z",
          "updatedAt": "2016-05-10T16:50:21.000Z"
        }
      ],
      [
        {
          "approvalid": 5,
          "isapproved": null,
          "approverid": 1,
          "approverserviceid": 2,
          "ciid": 1,
          "type": "assoc",
          "isactive": true,
          "createdAt": "2016-05-10T16:50:21.000Z",
          "updatedAt": "2016-05-10T16:50:21.000Z"
        }
      ]
    ]
  */
  assoc: function(req, res)
  {
    // The API is open only for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    else if(!req.body.serviceids || !req.body.ciid || !_.isFinite(req.body.ciid))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var currentserviceids = [];
      var affectedserviceids = [];
      var finalResult = {};
      var uniquesrvids = _.uniq(req.body.serviceids);

      // If input parameters have duplicate serviceids
      if(uniquesrvids.length !== req.body.serviceids.length)
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        throw error;
      }
      var removeList = [];
      Service.find().where({ serviceid: req.body.serviceids })
      .then(function(services)
      {
        // If any of the services don't exist then error out.
        if(services.length !== req.body.serviceids.length)
        {
          var error = new Error();
          error.code = 400362;
          error.message = errorCode[ "400362" ];
          throw error;
        }

        for(var iter = 0; iter < services.length; iter++)
        {
          // If any service is frozen then error out.
          if(services[ iter ].isfrozen)
          {
            var error = new Error();
            error.code = 400363;
            error.message = errorCode[ "400363" ];
            throw error;
          }

          // If any of the services are not in the approved phase(ID=2) error out
          else if(services[ iter ].servicestateid !== enums.serviceStateIds.APPROVED)
          {
            var error = new Error();
            error.code = 400369;
            error.message = errorCode[ "400369" ];
            throw error;
          }
        }
        return Cidetail.findOne({ ciid: req.body.ciid }).populate("serviceids");
      })
      .then(function(cidetail)
      {
        // Check if the CI exists
        if(!cidetail)
        {
          var error = new Error();
          error.code = 400382;
          error.message = errorCode[ "400382" ];
          throw error;
        }
        // The Configuration Item shouldn't be in Decommissioned)
        else if(cidetail.cistateid > enums.ciStateIds.RECLAIM)
        {
          var error = new Error();
          error.code = 400387;
          error.message = errorCode[ "400387" ];
          throw error;
        }

        // The Configuration Item shouldn't be in frozen state
        else if(cidetail.isfrozen)
        {
          var error = new Error();
          error.code = 400383;
          error.message = errorCode[ "400383" ];
          throw error;
        }

        // Ci between Approved and Reclaim, included, state cannot exist without services
        else if(cidetail.cistateid > enums.ciStateIds.REGISTRATION && cidetail.cistateid < enums.ciStateIds.RECLAIM && req.body.serviceids.length === 0)
        {
          var error = new Error();
          error.code = 400373;
          error.message = errorCode[ "400373" ];
          throw error;
        }
        // Check if the primary service id for the CI is missing in the provided service collection
        else if(cidetail.cistateid < enums.ciStateIds.RECLAIM && cidetail.primaryservice && !_.includes(req.body.serviceids, cidetail.primaryservice))
        {
          var error = new Error();
          error.code = 400551;
          error.message = errorCode[ "400551" ];
          throw error;
        }
        else
        {
          for(var iter = 0; iter < cidetail.serviceids.length; iter++)
          {
            currentserviceids.push(cidetail.serviceids[ iter ].serviceid);
          }

          // If all the services in the input list are already associated with the configuraton item
          if(JSON.stringify(currentserviceids.sort()) === JSON.stringify(req.body.serviceids.sort()))
          {
            var error = new Error();
            error.code = 400372;
            error.message = errorCode[ "400372" ];
            throw error;
          }
          else
          {
            affectedserviceids = currentserviceids.concat(req.body.serviceids.filter(function(element) { return currentserviceids.indexOf(element) === -1; }));
            removeList = currentserviceids.filter(function(element) { return req.body.serviceids.indexOf(element) === -1;});
            var pOpenIncs;
            // Look for open incidents for this CI with any of the services being dissociated
            if(removeList.length)
            {
              pOpenIncs = Incident.count().where({ ciid: req.body.ciid, serviceid: removeList, state: { '!': enums.incidentStates.CLOSED } });
            }
            return Promise.all([ pOpenIncs, Service.find({ serviceid: affectedserviceids }) ]);
          }
        }
      })
      .then(function(services)
      {
        var approvalObj = function(newserviceids, currentserviceids, cbid, serviceid, type, ciid)
        {
          this.newserviceids = newserviceids;
          this.currentserviceids = currentserviceids;
          this.cbid = cbid;
          this.serviceid = serviceid;
          this.type = type;
          this.ciid = ciid;
        };
        // Check if any Incidents are open for this CI with any of the services being dissociated
        if(removeList)
        {
          if(services[ 0 ])
          {
            var error = new Error();
            error.code = 400379;
            error.message = errorCode[ '400379' ];
            throw error;
          }
        }
        var approvalArray = [];
        for(var iter = 0; iter < services[ 1 ].length; iter++)
        {
          approvalArray.push(new approvalObj(req.body.serviceids, currentserviceids, services[ 1 ][ iter ].cbid, services[ 1 ][ iter ].serviceid, "assoc", req.body.ciid));
        }
        var fn = function initiateApproval(obj)
        {
          return servUtils.sendApproval(obj.newserviceids, obj.currentserviceids, obj.cbid, obj.serviceid, obj.type, obj.ciid);
        };
        var callUtil = approvalArray.map(fn);
        var pResults = Promise.all(callUtil);
        return pResults;
      })
      .then(function(results)
      {
        finalResult = results;
        return Cidetail.update({ ciid: req.body.ciid }, { isfrozen: true, freezecause: enums.freezeCauses.PENDING_SERVICE_CI_ASSOCIATION });
      })
      .then(function(cidetail)
      {
        return res.json(finalResult);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Service
  @apiName cancel
  @api {post} /service/cancel Cancel
  @apiDescription Cancels a Service in Registration Phase
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I
      m1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv
      bmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ
      3MjcwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2
      9tIn0.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM" -d '{"serviceid" : 1}'
      "http://localhost:1337/service/cancel"
  @apiParam {Number} serviceid ID of the service
  @apiParamExample {json} Request-Example
    {
      "serviceid" : 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400362
  @apiUse 400363
  @apiUse 400374
  @apiUse 403502
  @apiSuccess (200) {Object} service Details of the cancelled service
  @apiSuccessExample {json} Success-Response
    {
      "serviceid": 1,
      "servicename": "Wayne Trainning Service",
      "cbid": 1,
      "servicestateid": 3,
      "details": "Trainning service for Wayne Family",
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  cancel: function(req, res)
  {
    // The API is open only for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.serviceid || !_.isFinite(req.body.serviceid))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var pGetService = Service.findOne({ serviceid: req.body.serviceid }).populate('cbid');
      var pGetUser = userUtils.getUser([ req.body.requesterid ]);
      Promise.all([ pGetService, pGetUser ])
      .then(function showRec(record)
      {
        // Check if the Serviceid in the input exists
        if(!record[ 0 ])
        {
          var error = new Error();
          error.code = 400362;
          error.message = errorCode[ "400362" ];
          throw error;
        }
        // If the service is frozen
        else if(record[ 0 ].isfrozen)
        {
          var error = new Error();
          error.code = 400363;
          error.message = errorCode[ "400363" ];
          throw error;
        }
        // If the service is not in Registration phase
        else if(record[ 0 ].servicestateid !== enums.serviceStateIds.REGISTRATION)
        {
          var error = new Error();
          error.code = 400374;
          error.message = errorCode[ "400374" ];
          throw error;
        }
        // Check if the requester is an admin of the bd
        else if(!_.includes(_.map(record[ 1 ][ 0 ].administeredbds, 'bdid'), record[ 0 ].cbid.bdid))
        {
          var error = new Error();
          error.code = 403501;
          error.message = errorCode [ '403501' ];
          throw error;
        }
        else
        {
          return Service.update({ serviceid: req.body.serviceid }, { servicestateid: enums.serviceStateIds.ARCHIVED });
        }
      })
      .then(function(records)
      {
        return res.ok(records[ 0 ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Service
  @apiName listapproval
  @api {post} /service/listapproval ListApproval
  @apiDescription Lists all the active service approvals for the requester
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I
      m1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv
      bmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ
      3MjcwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2
      9tIn0.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM" "http://localhost:1337/service/listapporval"
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400377
  @apiSuccess (200) {Object[]} approval List of approvals pending for the requester
  @apiSuccessExample {json} Success-Response
    [
      {
        "crntsrvids":
          [
            {
              "serviceid": 2,
              "servicename": "Sober Name",
              "servicestateid": 2,
              "details": "unfreezing after next check",
              "isfrozen": false,
              "freezecause": 0,
              "cbid": 1
            }
          ],
        "newserviceids": [],
        "approverid": 6,
        "approverserviceid": 2,
        "ciid": 2,
        "approvalid": 5,
        "isapproved": null,
        "type": 'assoc',
        "isactive": true
      }
    ]
  */
  listapproval: function(req, res)
  {
    Serviceapproval.find({ approverid: req.body.requesterid, isactive: true })
    .populate("newserviceids")
    .populate("crntsrvids")
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400377;
        error.message = errorCode[ "400377" ];
        throw error;
      }
      else
      {
        return res.ok(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @apiGroup Service
    @apiName search
    @api {post} /service/search Search
    @apiDescription Searches the model and returns the result. The number of search column and order by criteria are flexible.
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
       curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml
       yc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R
       1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l
       lZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNzEyNTkxLCJleHAiOjE0NzI3MzQxOTEsImF1ZCI
       6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.ySVS_rf2Qw-9U1c6pcxliNDE-JQiZ7gQ5eJG
       aXt2Z7Q" -H "Content-Type: application/json" -d
       '{
          "search":  { "servicename": "Wayne%" },
          "result" : [ "servicename", "cbid", "servicestateid", "details" ],
          "orderby": "servicename"
        }' "http://localhost:1337/service/search"
    @apiParam {Object} search Object listing the attrbutes of the searched service
    @apiParam {String[]} result Array of attributes requested
    @apiParam {String} orderby Attribute to order by the results
    @apiParamExample {json} Request-Example
      {
        "search":  { "servicename": "Wayne%" },
        "result" : [ "servicename", "cbid", "servicestateid", "details" ],
        "orderby": "servicename"
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 400053
    @apiSuccess (200) {Array} result First element-Number of records, Second element-Array of results
    @apiSuccessExample {json} Success-Response
    [
      1,
      [
        {
          "cbid": 1,
          "servicestateid": 1,
          "servicename": "Wayne Trainning Service",
          "details": "Trainning service for Wayne Family"
        }
      ]
    ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Service.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Service.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  },
  /**
  @apiGroup Service
  @apiName queueassign
  @api {post} /service/queueassign Queueassign
  @apiDescription Assigns a queue to the service
  @apiPermission bdadmin
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI
      6bnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjI3LCJmdWxsbmFtZSI6ImluY2ZpcnN0IGluY09wZXJhdG9yIiw
      iZmlyc3RuYW1lIjoiaW5jZmlyc3QiLCJsYXN0bmFtZSI6ImluY09wZXJhdG9yIiwidXNlcm5hbWUiOiJpbmNmaXJzdC5vcGVyYXR
      vciIsImVtYWlsaWQiOiJpbmNmaXJzdC5vcGVyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjpudWxsLCJwaG9uZW5
      1bWJlciI6bnVsbCwiZW1wbG95ZWVpZCI6IklOQ0ZURTAwMDAwOSIsImlzYWN0aXZlIjp0cnVlLCJwYXNzZXhwaXJlZCI6ZmFsc2U
      sInVzZXJzZXR0aW5ncyI6bnVsbH0sImlhdCI6MTQ4OTQwNzQ3OCwiZXhwIjoxNDg5NDI5MDc4LCJhdWQiOiJ3d3cucXVldWVsb2F
      kLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.TPycl3k6BQ-ZiXNr2lpT78nWBmX9k4WzfGIpRktYQfs"
      -H "Content-Type: application/json" -d
      '{
        "serviceid" : 4,
        "supportqueue" : 3,
        "assign" : true
      }' "http://localhost:1337/service/queueassign"
  @apiParam {Number} serviceid ID of the service
  @apiParamExample {json} Request-Example
      {
        "serviceid" : 4,
        "supportqueue" : 3,
        "assign" : true
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400253
  @apiUse 400254
  @apiUse 400362
  @apiUse 400363
  @apiUse 400264
  @apiUse 400362
  @apiUse 400378
  @apiUse 400380
  @apiUse 400388
  @apiUse 400391
  @apiUse 403501
  @apiUse 403502
  @apiSuccess (200) {Object} approval Details of the approval
  @apiSuccessExample {json} Success-Response
    {
      "approvalid": 60,
      "typeofapproval": 7,
      "entityid": 4,
      "targetentityid": 3,
      "approverid": 24,
      "initiatorid": 20,
      "isapproved": null,
      "isactive": true
    }
  */
  queueassign: function(req, res)
  {
    // The API is open only for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.serviceid || !req.body.supportqueue || typeof(req.body.assign) !== "boolean" || req.body.assign === "null")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      if(req.body.assign)
      {
        var approvalDet = {};
        var pServiceDetails = Service.findOne({ serviceid: req.body.serviceid }).populate('cbid');
        var pQueue = Queue.findOne({ queueid: req.body.supportqueue }).populate("cbid");
        var pGetBds = userUtils.getBdAdmins([ req.body.requesterid ]);

        Promise.all([ pServiceDetails, pQueue, pGetBds ])
        .then(function(statusResults)
        {
          // Check if the service is valid
          if(!statusResults[ 0 ])
          {
            var error = new Error();
            error.code = 400362;
            error.message = errorCode[ "400362" ];
            throw error;
          }

          // Check if the Service is frozen
          else if(statusResults[ 0 ].isfrozen)
          {
            var error = new Error();
            error.code = 400363;
            error.message = errorCode[ "400363" ];
            throw error;
          }

          // Check if the Service is in Archived state
          else if(statusResults[ 0 ].servicestateid > enums.serviceStateIds.APPROVED)
          {
            var error = new Error();
            error.code = 400380;
            error.message = errorCode[ '400380' ];
            throw error;
          }

          // Check if the supportqueue is valid
          else if(!statusResults[ 1 ])
          {
            var error = new Error();
            error.code = 400254;
            error.message = errorCode[ "400254" ];
            throw error;
          }

          // Check if the queue is frozen
          else if(statusResults[ 1 ].isfrozen)
          {
            var error = new Error();
            error.code = 400264;
            error.message = errorCode[ "400264" ];
            throw error;
          }

          // Check if the supportqueue is active
          else if(!statusResults[ 1 ].isactive)
          {
            var error = new Error();
            error.code = 400253;
            error.message = errorCode[ "400253" ];
            throw error;
          }
          // Check if the requester is an admin of the bd
          else if(!_.includes(_.map(statusResults[ 2 ][ 0 ].administeredbds, 'bdid'), statusResults[ 0 ].cbid.bdid))
          {
            var error = new Error();
            error.code = 403501;
            error.message = errorCode [ '403501' ];
            throw error;
          }
          else
          {
            return statusResults;
          }
        })
        .then(function(statusResults)
        {
          return OrgApproval.create({ typeofapproval: enums.approval.ASSIGN_SERVICE2Q, entityid: req.body.serviceid, targetentityid: req.body.supportqueue, approverid: statusResults[ 1 ].cbid.cbownerid, initiatorid: req.body.requesterid, isapproved: null });
        })
        .then(function(approval)
        {
          approvalDet = approval;
          return Service.update({ serviceid: req.body.serviceid }, { isfrozen: true, freezecause: enums.freezeCauses.PENDING_SERVICE_QUEUE_ASSIGNMENT });
        })
        .then(function(service)
        {
          return res.ok(approvalDet);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
      else
      {
        var pServiceDetails = Service.findOne({ serviceid: req.body.serviceid });
        var pOpenIncs = Incident.count().where({ serviceid: req.body.serviceid, assignedqueueid: req.body.queueid, state: { '!': enums.incidentStates.CLOSED } });
        Promise.all([ pServiceDetails, pOpenIncs ])
        .then(function showRec(record)
        {
          // Check if the Service has any open incidents
          if(record[ 1 ])
          {
            var error = new Error();
            error.code = 400391;
            error.message = errorCode[ '400391' ];
            throw error;
          }
          // Check if the service exists
          if(!record [ 0 ])
          {
            var error = new Error();
            error.code = 400362;
            error.message = errorCode[ '400362' ];
            throw error;
          }
          // Check if the Service is in Approved state
          if(record[ 0 ].servicestateid === enums.serviceStateIds.APPROVED)
          {
            var error = new Error();
            error.code = 400378;
            error.message = errorCode[ "400378" ];
            throw error;
          }

          // Check if there is no support queue at the moment
          else if(record[ 0 ].supportqueue === null)
          {
            var error = new Error();
            error.code = 400388;
            error.message = errorCode[ "400388" ];
            throw error;
          }
          return record;
        })
        .then(function(record)
        {
          return Service.update({ serviceid: record[ 0 ].serviceid }, { supportqueue: null });
        })
        .then(function showRecs(results)
        {
          var result = results [ 0 ];
          return res.ok(result);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
    }
  }
};
