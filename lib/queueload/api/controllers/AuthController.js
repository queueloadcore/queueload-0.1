/* FileName: AuthController.js
* @description: This file describes the various actions in the authorization controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 27/07/2016    Initial file creation                         SMandal
*
*/

/**
 * @apiGroup AAA
 */

var Promise = require("bluebird");
var promisedCipherService = Promise.promisifyAll(require("../services/CipherService.js"));
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");
var sg = require('sendgrid')(sails.config.sendgrid.apikey);

if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{
  /**
  @apiGroup AAA
  @apiName signin
  @api {post} /auth/signin Signin
  @apiDescription This function compares the supplied credentials with the stored ones and on
          success issues details of the user along with an access token and refresh token
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -d
     '{
          "emailid" : "administrator@testdomain.com",
          "password" : "P@5sW0rd"
      }' "http://localhost:1337/auth/signin"
  @apiParam {Email} emailid EmailID of the user
  @apiParam {String} password Password of the user
  @apiParamExample {json} Request-Example
     {
        "emailid":"marco.polo@testclient.com",
        "password":"P@5#word"
     }
  @apiUse 400051
  @apiUse 401503
  @apiUse 401508
  @apiUse 500001
  @apiSuccess (200) {Object} user Details of the user
  @apiSuccess (200) {String} acctoken Access Token
  @apiSuccess (200) {String} reftoken Refresh Token
  @apiSuccessExample {json} Success-Response
    {
      "user": {
        "userid": 1,
        "firstname": "Super",
        "lastname": "User",
        "username": "administrator",
        "emailid": "administrator@testdomain.com",
        "countrycode": "+91",
        "phonenumber": "9999999999",
        "employeeid": "FTE000001",
        "isactive": true
      },
      "acctoken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZ
      XIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0
      ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI
      6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyODI3OTA5LCJleHAiOjE0NzI4NDk1MDksImF1ZCI6Ind3dy
      5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.6THJtbRD0igM-jONnfe73ISr2MhKEwEHjGjnd4RSn-0",
      "reftoken": "38452cdf995851becd15ad0d81623177fa2b7f5eaafec268de18c8f3dd165bc1"
    }
  */
  signin: function(req, res)
  {
    if(sails.config.environment === 'production' && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json")))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if(!req.body || !req.body.emailid || !req.body.password)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var resObj = {};
      User.findOne({ emailid: req.body.emailid })
      .then(function(user)
      {
        // Check if the user exists in the system
        if(!user)
        {
          //Console.log("Didn't find the user");
          var error = new Error();
          error.code = 401503;
          error.messge = errorCode[ "401503" ];
          throw error;
        }
        // Check if the user is active in the system
        else if(!user.isactive)
        {
          //Console.log("Inactive user");
          var error = new Error();
          error.code = 401503;
          error.messge = errorCode[ "401503" ];
          throw error;
        }
        // Check if the user password has expired
        else if(user.passexpired)
        {
          var error = new Error();
          error.code = 401508;
          error.message = errorCode[ '401508' ];
          throw error;
        }
        resObj.user = user;
        return Role.findOne({ userid: user.userid });
      })
      .then(function(role)
      {
        if(!role)
        {
          return User.update({ userid: resObj.user.userid }, { isactive: false });
        }
        else
        {
          delete role.userid;
          resObj.roles = role;
          return promisedCipherService.comparePasswordAsync(req.body.password, resObj.user.password);
        }
      })
      .then(function(result)
      {
        // Check if the user doesn't have roles
        if(Array.isArray(result))
        {
          var error = new Error();
          error.code = 500001;
          error.message = errorCode[ "500001" ];
          throw error;
        }
        // Check if the supplied credential is valid
        else if(!result)
        {
          //Console.log("Invalid credential");
          var error = new Error();
          error.code = 401503;
          error.message = errorCode[ "401503" ];
          throw error;
        }
        else
        {
          // Create an access token
          var pAccToken = CipherService.createAccToken(resObj.user);
          // Create a refresh token
          var pRefToken = CipherService.createRefToken();

          var pTokens = Promise.all([ pAccToken, pRefToken ]);
          return pTokens;
        }
      })
      .then(function(tokens)
      {
        resObj.acctoken = tokens[ 0 ];
        resObj.reftoken = tokens[ 1 ];

        // Record the token in the token table
        var pStorAccToken = Token.create({ emailid: req.body.emailid, tokentype: enums.tokenTypes.ACCESS, token: resObj.acctoken });
        var pStorRefToken = Token.create({ emailid: req.body.emailid, tokentype: enums.tokenTypes.REFRESH, token: resObj.reftoken });

        var pStorTokens = Promise.all([ pStorAccToken, pStorRefToken ]);
        return pStorTokens;
      })
      .then(function(token)
      {
        return client.multi()
          .hmset(process.env.ACCOUNTID + ":" + req.body.emailid, [ "reftkn", resObj.reftoken ])
          .hdel(process.env.ACCOUNTID + ":" + req.body.emailid, [ "signedout" ])
          .EXPIRE(process.env.ACCOUNTID + ":" + req.body.emailid, sails.config.authcfg.refExpiresIn)
          .execAsync();
      })
      .then(function(redis)
      {
        return res.ok(resObj);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup AAA
  @apiName reftkn
  @api {post} /auth/reftkn RefTkn
  @apiDescription This function checks the authenticity of the supplied refresh token and on
     success issues details of the user along with an access token and refresh token
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -d
     '{
        "emailid":"administrator@testdomain.com",
        "reftkn": "ed5758c7197f6c031889575bbe1fa164ea39337de30e93736ba36f3308b03d1d"
      }' "http://localhost:1337/auth/reftkn"
  @apiParam {Email} emailid EmailID of the user
  @apiParam {String} reftoken Refresh Token
  @apiParamExample {json} Request-Example
    {
      "emailid":"administrator@testdomain.com",
      "reftkn": "ed5758c7197f6c031889575bbe1fa164ea39337de30e93736ba36f3308b03d1d"
    }
  @apiUse 400051
  @apiUse 401503
  @apiUse 401508
  @apiSuccess (200) {Object} user Details of the user
  @apiSuccess (200) {String} acctoken Access Token
  @apiSuccess (200) {String} reftoken Refresh Token
  @apiSuccessExample {json} Success-Response
    {
      "user": {
        "userid": 1,
        "firstname": "Super",
        "lastname": "User",
        "username": "administrator",
        "emailid": "administrator@testdomain.com",
        "countrycode": "+91",
        "phonenumber": "9999999999",
        "employeeid": "FTE000001",
        "isactive": true
      },
      "acctoken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZ
      XIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0
      ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI
      6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyODI4NDI3LCJleHAiOjE0NzI4NTAwMjcsImF1ZCI6Ind3dy
      5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.iMnmXbaiBpo1bD5M9d5uIJURmPAMNKufXfjb2IrFzRw",
      "reftoken": "0670696aca6c5d8665098aaa1b9e63a8040354cd35754f22680f17bad9768956"
    }
  */
  reftkn: function(req, res)
  {
    if(sails.config.environment === 'production' && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json")))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if(!req.body || !req.body.emailid || !req.body.reftkn)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var resObj = {};
      var userObj = {};
      client.hgetAsync(process.env.ACCOUNTID + ":" + req.body.emailid, "reftkn")
      .then(function(token)
      {
        // Check if the user is signed-in
        if(!token)
        {
          var error = new Error();
          error.code = 401503;
          error.message = errorCode[ "401503" ];
          throw error;
        }
        // Check if the sent token matches with the stored one
        else if(token !== req.body.reftkn)
        {
          sails.log.error("[AuthController.reftkn] Possible Security Threat - Client is using an incorrect refresh token");
          var error = new Error();
          error.code = 401503;
          error.message = errorCode[ "401503" ];
          throw error;
        }
        else
        {
          return User.findOne({ emailid: req.body.emailid });
        }
      })
      .then(function(user)
      {
        userObj = user;
        // Check if the user is inactive
        if(!user.isactive)
        {
          var error = new Error();
          error.code = 401503;
          error.message = errorCode[ "401503" ];
          throw error;
        }
        // Check if the password has expired
        else if(user.passexpired)
        {
          return client.multi()
          .hmset(process.env.ACCOUNTID + ":" + req.body.emailid, [ "signedout", true ])
          .hdel(process.env.ACCOUNTID + ":" + req.body.emailid, [ "reftkn" ])
          .EXPIRE(process.env.ACCOUNTID + ":" + req.body.emailid, sails.config.authcfg.accExpiresIn)
          .execAsync();
        }
        else
        {
          resObj.user = user;
          return Role.findOne({ userid: user.userid });
        }
      })
      .then(function(role)
      {
        if(userObj.passexpired)
        {
          return role;
        }
        if(!role)
        {
          return User.update({ userid: resObj.user.userid }, { isactive: false });
        }
        else
        {
          delete role.userid;
          resObj.roles = role;
          return role;
        }
      })
      .then(function(result)
      {
        if(userObj.passexpired)
        {
          return result;
        }
        // Check if the supplied credential is valid
        if(Array.isArray(result))
        {
          var error = new Error();
          error.code = 500001;
          error.message = errorCode[ "500001" ];
          throw error;
        }
        else
        {
          // Create an access token
          var pAccToken = CipherService.createAccToken(resObj.user);
          // Create a refresh token
          var pRefToken = CipherService.createRefToken();

          var pTokens = Promise.all([ pAccToken, pRefToken ]);
          return pTokens;
        }
      })
      .then(function(tokens)
      {
        if(userObj.passexpired)
        {
          return tokens;
        }
        resObj.acctoken = tokens[ 0 ];
        resObj.reftoken = tokens[ 1 ];

        // Record the token in the token table
        var pStorAccToken = Token.create({ emailid: req.body.emailid, tokentype: enums.tokenTypes.ACCESS, token: resObj.acctoken });
        var pStorRefToken = Token.create({ emailid: req.body.emailid, tokentype: enums.tokenTypes.REFRESH, token: resObj.reftoken });

        var pStorTokens = Promise.all([ pStorAccToken, pStorRefToken ]);
        return pStorTokens;
      })
      .then(function(token)
      {
        if(userObj.passexpired)
        {
          var error = new Error();
          error.code = 401508;
          error.message = errorCode[ '401508' ];
          throw error;
        }
        else
        {
          return client.multi()
          .hmset(process.env.ACCOUNTID + ":" + req.body.emailid, [ "reftkn", resObj.reftoken ])
          .EXPIRE(process.env.ACCOUNTID + ":" + req.body.emailid, sails.config.authcfg.refExpiresIn)
          .execAsync();
        }
      })
      .then(function(status)
      {
        return res.ok(resObj);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup AAA
  @apiName signout
  @api {post} /auth/signout Signout
  @apiDescription This function checks the authenticity of the supplied refresh token and on
     success signs out the user from the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -d
     '{
        "emailid":"administrator@testdomain.com",
        "reftkn": "4bb0a7a5b5c8a679abe5d54c8c0017f494f7ed703310bda5cbd8acb5cfe20b7e"
      }' "http://localhost:1337/auth/signout"
  @apiParam {Email} emailid EmailID of the user
  @apiParam {String} reftoken Refresh Token
  @apiParamExample {json} Request-Example
    {
      "emailid":"administrator@testdomain.com",
      "reftkn": "4bb0a7a5b5c8a679abe5d54c8c0017f494f7ed703310bda5cbd8acb5cfe20b7e"
    }
  @apiUse 400051
  @apiUse 401503
  @apiSuccess NA No return on sucess
  */
  signout: function(req, res)
  {
    if(sails.config.environment === 'production' && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json")))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if(!req.body || !req.body.emailid || !req.body.reftkn)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      client.hgetAsync(process.env.ACCOUNTID + ":" + req.body.emailid, "reftkn")
      .then(function(token)
      {
        // If it has been longer then the reftkn life since the user has used the system after logging-in
        if(!token)
        {
          var error = new Error();
          error.code = 401503;
          error.message = errorCode[ "401503" ];
          throw error;
        }
        else if(token !== req.body.reftkn)
        {
          sails.log.error("[AuthController.reftkn] Possible Security Threat - Client is using an incorrect refresh token");
          var error = new Error();
          error.code = 401503;
          error.message = errorCode[ "401503" ];
          throw error;
        }
        else
        {
          return client.multi()
          .hmset(process.env.ACCOUNTID + ":" + req.body.emailid, [ "signedout", true ])
          .hdel(process.env.ACCOUNTID + ":" + req.body.emailid, [ "reftkn" ])
          .EXPIRE(process.env.ACCOUNTID + ":" + req.body.emailid, sails.config.authcfg.accExpiresIn)
          .execAsync();
        }
      })
      .then(function(status)
      {
        return res.ok();
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup AAA
  @apiName resetpass
  @api {post} /auth/resetpass ResetPass
  @apiDescription This function checks the authenticity of the old password and on
     success sets user password to the provided new passowrd
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -d
    '{
      "emailid" : "test.user11@testclient1.com",
      "oldpass" : "password",
      "newpass" : "password1"
    }' "http://localhost:1337/auth/resetpass"
  @apiParam {Email} emailid EmailID of the user
  @apiParam {String} oldpass Old password
  @apiParam {String} newpass New password
  @apiParamExample {json} Request-Example
    {
      "emailid" : "test.user11@testclient1.com",
      "oldpass" : "password",
      "newpass" : "password1"
    }
  @apiUse 400051
  @apiUse 401503
  @apiSuccess NA No return on sucess
    }
  */
  resetpass: function(req, res)
  {
    if(sails.config.environment === 'production' && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json")))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if(!req.body.emailid || !req.body.oldpass || !req.body.newpass || (req.body.oldpass === req.body.newpass))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    User.findOne({ emailid: req.body.emailid }).populate('roles')
    .then(function(user)
    {
      if(!user)
      {
        var error = new Error();
        error.code = 401503;
        error.message = errorCode[ '401503' ];
        throw error;
      }
      else if(!user.roles.length)
      {
        return User.update({ emailid: user.emailid }, { isactive: false });
      }
      else
      {
        return Promise.all([ promisedCipherService.comparePasswordAsync(req.body.oldpass, user.password), promisedCipherService.hashPasswordAsync(req.body.newpass) ]);
      }
    })
    .then(function(result)
    {
      if(result[ 0 ].userid)
      {
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        throw error;
      }
      else if(!result[ 0 ])
      {
        var error = new Error();
        error.code = 401503;
        error.message = errorCode[ '401503' ];
        throw error;
      }
      else
      {
        return User.update({ emailid: req.body.emailid }, { password: result[ 1 ], passexpired: false });
      }
    })
    .then(function(result)
    {
      return res.ok();
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup AAA
  @apiName forgotpass
  @api {post} /auth/forgotpass ForgotPass
  @apiDescription This function checks the availability of the email and sends a token to be used
        to set new password
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -d
    '{
      "emailid" : "test.user11@testclient1.com"
    }' "http://localhost:1337/auth/forgotpass"
  @apiParam {Email} emailid EmailID of the user
  @apiParamExample {json} Request-Example
    {
      "emailid" : "test.user11@testclient1.com"
    }
  @apiUse 400051
  @apiUse 401503
  @apiSuccess NA No return on sucess
  */
  forgotpass: function(req, res)
  {
    if(sails.config.environment === 'production' && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json")))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if(!req.body.emailid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var pUserLookup = User.findOne({ emailid: req.body.emailid });
    var createToken = CipherService.createRefToken();
    var token;
    Promise.all([ pUserLookup, createToken ])
    .then(function(result)
    {
      if(!result[ 0 ])
      {
        var error = new Error();
        error.code = 401503;
        error.message = errorCode[ '401503' ];
        throw error;
      }
      else
      {
        token = result[ 1 ];
        return client.multi()
        .set(process.env.ACCOUNTID + ":" + 'passsettkn:' + req.body.emailid,  result[ 1 ])
        .EXPIRE(process.env.ACCOUNTID + ":" + 'passsettkn:' + req.body.emailid, 86400)
        .execAsync();
      }
    })
    .then(function(result)
    {
      var custEmail = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
          personalizations: [
            {
              to: [
                {
                  email: req.body.emailid
                }
              ],
              'substitutions': {
                '-token-': token
              },
              subject: 'QueueLoad Password Reset Request'
            }
          ],
          from: {
            email: 'noreply@queueload.com'
          },
          'template_id': '3982c6b1-8304-47a1-b48a-5f0474b4e720'
        }
      });

      sails.log.info("Notifying the customer in email and cleaning up the token");
      return sg.API(custEmail);
    })
    .then(function(result)
    {
      return res.ok();
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup AAA
  @apiName setpass
  @api {post} /auth/setpass SetPass
  @apiDescription This function checks the authenticity of the provided token and on
     success sets user password to the provided new passowrd
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -d
    '{
      "emailid" : "test.user11@testclient1.com",
      "token" : "0a70693fe3babe1bdd57f3ef9ea9b6827cc77a8460c3c96b27d0673a7c539e4c",
      "newpass" : "password"
    }' "http://localhost:1337/auth/setpass"
  @apiParam {Email} emailid EmailID of the user
  @apiParam {String} token Token recevied in the email
  @apiParam {Password} newpass New password
  @apiParamExample {json} Request-Example
    {
      "emailid" : "test.user11@testclient1.com",
      "token" : "0a70693fe3babe1bdd57f3ef9ea9b6827cc77a8460c3c96b27d0673a7c539e4c",
      "newpass" : "password"
    }
  @apiUse 400051
  @apiUse 401503
  @apiUse 401508
  @apiSuccess (200) No return on sucess
  */
  setpass: function(req, res)
  {
    if(sails.config.environment === 'production' && (!req.headers[ 'content-type' ] || !req.headers[ 'content-type' ].includes("application/json")))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if(!req.body.emailid || !req.body.token || !req.body.newpass)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var pUserLookup = User.findOne({ emailid: req.body.emailid });
    var pTokenLookup = client.getAsync(process.env.ACCOUNTID + ":" + 'passsettkn:' + req.body.emailid);
    Promise.all([ pUserLookup, pTokenLookup ])
    .then(function(result)
    {
      if(!result[ 0 ])
      {
        var error = new Error();
        error.code = 401503;
        error.message = errorCode[ '401503' ];
        throw error;
      }
      else if(result[ 1 ] !== req.body.token)
      {
        var error = new Error();
        error.code = 401503;
        throw error;
      }
      else
      {
        return promisedCipherService.hashPasswordAsync(req.body.newpass);
      }
    })
    .then(function(result)
    {
      return User.update({ emailid: req.body.emailid }, { password: result, passexpired: false });
    })
    .then(function(user)
    {
      if(!user)
      {
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        throw error;
      }
      return client.del(process.env.ACCOUNTID + ":" + 'passsettkn:' + req.body.emailid);
    })
    .then(function(result)
    {
      return res.ok();
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
