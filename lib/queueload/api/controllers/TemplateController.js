/* FileName: TemplateController.js
*  @description: This file describes the various actions in the Template controller.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  27/10/2016        Initial file creation                         SMandal
*
*/

/**
 * @apiGroup Template
 */

var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");
var _ = require("lodash");

module.exports =
{
  /**
  @apiGroup Template
  @apiName listTemplate
  @api {post} /template/list List
  @apiDescription This function lists all the templates in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV
    XNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2
    91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R
    pdmUiOnRydWV9LCJpYXQiOjE0Nzc1NzQ5NTQsImV4cCI6MTQ3NzU5NjU1NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi
    OiJ3d3cucXVldWVsb2FkLmNvbSJ9.Nmadtgu6EXBxMJMc8ZcmNvo92V2aqATW5ZcU4IiecTo" -d
    '{
      "type" : 1
    }' "http://localhost:1337/template/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 500001
  @apiSuccess (200) {Object[]} templates Array of templates
  @apiSuccessExample {json} Success-Response
  [
    {
      "lasteditorid": 1,
      "templateid": 1,
      "templatename": "test inc templt1",
      "title": "title of the inc",
      "description": "description of the inc",
      "servicename": "some service",
      "ciname": "some ci",
      "severity": 1,
      "impact": 2,
      "isactive": true,
      "updatedAt": "2016-10-27T13:29:26.000Z"
    }
  ]
  */
	list: function(req, res)
  {
    if(!req.body.page || !req.body.limit || !req.body.type || req.body.type !== enums.templateTypes.INCIDENT)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    switch(req.body.type)
    {
      case enums.templateTypes.INCIDENT:
        Inctemplate.find().paginate({ page: req.body.page, limit: req.body.limit })
        .then(function(templates)
        {
          return res.ok(templates);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
        break;
      default:
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        return res.negotiate(error);
    }
  },
  /**
  @apiGroup Template
  @apiName addTemplate
  @api {post} /template/add Add
  @apiDescription This function adds a template to the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV
    XNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2
    91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R
    pdmUiOnRydWV9LCJpYXQiOjE0Nzc1NzQ5NTQsImV4cCI6MTQ3NzU5NjU1NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi
    OiJ3d3cucXVldWVsb2FkLmNvbSJ9.Nmadtgu6EXBxMJMc8ZcmNvo92V2aqATW5ZcU4IiecTo" -d
      '{
        "type" : 1,
        "templatename" : "test inc templt5",
        "title" : "title of the inc",
        "description" : "description of the inc",
        "servicename" : "some servive",
        "ciname" : "some ci",
        "severity" : 1,
        "impact" : 2,
        "attachments" : [1,2,3]
      }' "http://localhost:1337/template/add"
  @apiParam {Number} type Type of the ticket template being created. E.g. Incident: 1
  @apiParam {String} templatename Name of the template being created. Must be unique
  @apiParam {String} [title] Title of the ticket
  @apiParam {String} [description] Description of the ticket
  @apiParam {String} [servicename] Name of the affected service
  @apiParam {String} [ciname] Name of the affected configuration item
  @apiParam {Number} [ownerqueuename] Name of the owner queue
  @apiParam {Number} [ownerusername] Name of the owner user
  @apiParam {Number} [assignedqueuename] Name of the assigned queue
  @apiParam {Number} [assignedusername] Name of the assgined user
  @apiParam {Number} [severity] Severity of the incident
  @apiParam {Number} [impact] Impact of the incident
  @apiParam {Number[]} [attachments] Attachments
  @apiParam {Boolean} [isactive=true] Visibility status of the template
  @apiParamExample {json} Request-Example
    {
      "type" : 1,
      "templatename" : "test inc templt5",
      "title" : "title of the inc",
      "description" : "description of the inc",
      "servicename" : "some servive",
      "ciname" : "some ci",
      "severity" : 1,
      "impact" : 2,
      "attachments" : [1,2,3]
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 500001
  @apiSuccess (200) {Object} templates Details of the added template
  @apiSuccessExample {json} Success-Response
    {
      "templateid": 7,
      "templatename": "test inc templt5",
      "lasteditorid": 1,
      "title": "title of the inc",
      "description": "description of the inc",
      "ownerqueuename": null,
      "ownerusername": null,
      "assignedqueuename": null,
      "assignedusername": null,
      "servicename": "some servive",
      "ciname": "some ci",
      "severity": 1,
      "impact": 2,
      "isactive": true,
      "updatedAt": "2016-10-29T04:21:06.000Z"
    }
  */
  add: function(req, res)
  {
    if(!req.body.templatename || !req.body.type || req.body.type !== enums.templateTypes.INCIDENT)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var template = {};
    template.templatename = req.body.templatename;
    template.lasteditorid = req.body.requesterid;
    switch(req.body.type)
    {
      case enums.templateTypes.INCIDENT:
        if(typeof req.body.title !== 'undefined') {template.title = req.body.title;}
        if(typeof req.body.description !== 'undefined') {template.description = req.body.description;}
        if(typeof req.body.ownerqueuename !== 'undefined') {template.ownerqueuename = req.body.ownerqueuename;}
        if(typeof req.body.ownerusername !== 'undefined') {template.ownerusername = req.body.ownerusername;}
        if(typeof req.body.assignedqueuename !== 'undefined') {template.assignedqueuename = req.body.assignedqueuename;}
        if(typeof req.body.assignedusername !== 'undefined') {template.assignedusername = req.body.assignedusername;}
        if(typeof req.body.servicename !== 'undefined') {template.servicename = req.body.servicename;}
        if(typeof req.body.ciname !== 'undefined') {template.ciname = req.body.ciname;}
        if(typeof req.body.severity !== 'undefined') {template.severity = req.body.severity;}
        if(typeof req.body.impact !== 'undefined') {template.impact = req.body.impact;}
        if(typeof req.body.attachments !== 'undefined') {template.attachments = req.body.attachments;}
        if(typeof req.body.isactive !== 'undefined') {template.isactive = req.body.isactive;}
        Inctemplate.create(template)
        .then(function(template)
        {
          return res.ok(template);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
        break;
      default:
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        return res.negotiate(error);
    }
  },
  /**
  @apiGroup Template
  @apiName getdetailTemplate
  @api {post} /template/getdetail Getdetail
  @apiDescription This function requests for the detail of a template
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV
    XNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2
    91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R
    pdmUiOnRydWV9LCJpYXQiOjE0Nzc1NzQ5NTQsImV4cCI6MTQ3NzU5NjU1NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi
    OiJ3d3cucXVldWVsb2FkLmNvbSJ9.Nmadtgu6EXBxMJMc8ZcmNvo92V2aqATW5ZcU4IiecTo" -d
    '{
      "type" : 1,
      "templateid" : 7
    }' "http://localhost:1337/template/getdetail"
  @apiParam {Number} type Type of the ticket template being created. E.g. Incident: 1
  @apiParam {Number} templateid ID of the template being sought the details of
  @apiParamExample {json} Request-Example
    {
      "type" : 1,
      "templateid" : 7
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400501
  @apiUse 500001
  @apiSuccess (200) {Object} templates Details of the sought template
  @apiSuccessExample {json} Success-Response
    {
      "lasteditorid": 1,
      "templateid": 7,
      "templatename": "test inc templt5",
      "title": "title of the inc",
      "description": "description of the inc",
      "ownerqueuename": null,
      "ownerusername": null,
      "assignedqueuename": null,
      "assignedusername": null,
      "servicename": "some servive",
      "ciname": "some ci",
      "severity": 1,
      "impact": 2,
      "isactive": true,
      "updatedAt": "2016-10-29T04:21:06.000Z"
    }
  */
  getdetail: function(req, res)
  {
    if(!req.body.templateid || isNaN(req.body.templateid) || !req.body.type || req.body.type !== enums.templateTypes.INCIDENT)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    switch(req.body.type)
    {
      case enums.templateTypes.INCIDENT:
        Inctemplate.findOne({ templateid: req.body.templateid }).populate('attachments')
        .then(function(template)
        {
          if(!template)
          {
            var error = new Error();
            error.code = 400501;
            error.message = errorCode[ '400501' ];
            throw error;
          }
          else
          {
            return res.ok(template);
          }
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
        break;
      default:
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        return res.negotiate(error);
    }
  },
  /**
  @apiGroup Template
  @apiName updateTemplate
  @api {post} /template/update Update
  @apiDescription This function updates attributes of a template
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV
    XNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2
    91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R
    pdmUiOnRydWV9LCJpYXQiOjE0Nzc1NTE1NDQsImV4cCI6MTQ3NzU3MzE0NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi
    OiJ3d3cucXVldWVsb2FkLmNvbSJ9.tFwQlQZ8CTABuxEas87c5uzOCgJH9-6tGh10nBAbP6E" -d
      '{
        "type" : 1,
        "templateid" : 7,
        "ownerqueuename" : "pen-making",
        "ownerusername" : "ek.worker",
        "assignedqueuename" : "refill-making",
        "assignedusername" : "aurek.worker"
      }' "http://localhost:1337/template/update"
  @apiParam {Number} templateid ID of the template being modified
  @apiParam {Number} type Type of the ticket template being created. E.g. Incident: 1
  @apiParam {String} [title] Title of the ticket
  @apiParam {String} [description] Description of the ticket
  @apiParam {String} [servicename] Name of the affected service
  @apiParam {String} [ciname] Name of the affected configuration item
  @apiParam {Number} [ownerqueuename] Name of the owner queue
  @apiParam {Number} [ownerusername] Name of the owner user
  @apiParam {Number} [assignedqueuename] Name of the assigned queue
  @apiParam {Number} [assignedusername] Name of the assgined user
  @apiParam {Number} [severity] Severity of the incident
  @apiParam {Number} [impact] Impact of the incident
  @apiParam {Number[]} [attachments] Attachments
  @apiParam {Boolean} [isactive=true] Visibility status of the template
  @apiParamExample {json} Request-Example
    {
      "type" : 1,
      "templateid" : 7,
      "ownerqueuename" : "pen-making",
      "ownerusername" : "ek.worker",
      "assignedqueuename" : "refill-making",
      "assignedusername" : "aurek.worker"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400501
  @apiUse 500001
  @apiSuccess (200) {Object} template Details of the updated template
  @apiSuccessExample {json} Success-Response
    {
      "templateid": 7,
      "templatename": "test inc templt5",
      "lasteditorid": 1,
      "title": "title of the inc",
      "description": "description of the inc",
      "ownerqueuename" : "pen-making",
      "ownerusername" : "ek.worker",
      "assignedqueuename" : "refill-making",
      "assignedusername" : "aurek.worker",
      "servicename": "some servive",
      "ciname": "some ci",
      "severity": 1,
      "impact": 2,
      "isactive": true,
      "updatedAt": "2016-10-29T04:25:56.000Z"
    }
  */
  update: function(req, res)
  {
    if(!req.body.templateid || req.body.templatename || !req.body.type || req.body.type !== enums.templateTypes.INCIDENT)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var template = {};
    template.lasteditorid = req.body.requesterid;
    switch(req.body.type)
    {
      case enums.templateTypes.INCIDENT:
        if(typeof req.body.title !== 'undefined') {template.title = req.body.title;}
        if(typeof req.body.description !== 'undefined') {template.description = req.body.description;}
        if(typeof req.body.ownerqueuename !== 'undefined') {template.ownerqueuename = req.body.ownerqueuename;}
        if(typeof req.body.ownerusername !== 'undefined') {template.ownerusername = req.body.ownerusername;}
        if(typeof req.body.assignedqueuename !== 'undefined') {template.assignedqueuename = req.body.assignedqueuename;}
        if(typeof req.body.assignedusername !== 'undefined') {template.assignedusername = req.body.assignedusername;}
        if(typeof req.body.servicename !== 'undefined') {template.servicename = req.body.servicename;}
        if(typeof req.body.ciname !== 'undefined') {template.ciname = req.body.ciname;}
        if(typeof req.body.severity !== 'undefined') {template.severity = req.body.severity;}
        if(typeof req.body.impact !== 'undefined') {template.impact = req.body.impact;}
        if(typeof req.body.attachments !== 'undefined') {template.attachments = req.body.attachments;}
        if(typeof req.body.isactive !== 'undefined') {template.isactive = req.body.isactive;}
        Inctemplate.update({ templateid: req.body.templateid }, template)
        .then(function(template)
        {
          if(!template[ 0 ])
          {
            var error = new Error();
            error.code = 400501;
            error.message = errorCode[ '400501' ];
            throw error;
          }
          else
          {
            return res.ok(template[ 0 ]);
          }
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
        break;
      default:
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        return res.negotiate(error);
    }
  },
  /**
  @apiGroup Template
  @apiName deleteTemplate
  @api {post} /template/delete Delete
  @apiDescription This function deletes the mentioned template
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV
    CJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV
    XNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2
    91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R
    pdmUiOnRydWV9LCJpYXQiOjE0Nzc1NTE1NDQsImV4cCI6MTQ3NzU3MzE0NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi
    OiJ3d3cucXVldWVsb2FkLmNvbSJ9.tFwQlQZ8CTABuxEas87c5uzOCgJH9-6tGh10nBAbP6E" -d
    '{
      "type" : 1,
      "templateid" : 7
    }' "http://localhost:1337/template/delete"
  @apiParam {Number} templateid ID of the template being sought the details of
  @apiParam {Number} type Type of the ticket template being created. E.g. Incident: 1
  @apiParamExample {json} Request-Example
    {
      "type" : 1,
      "templateid" : 7
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400501
  @apiUse 500001
  @apiSuccess (200) {Object} template Details of the deleted template
  @apiSuccessExample {json} Success-Response
    {
      "templateid": 7,
      "templatename": "test inc templt5",
      "lasteditorid": 1,
      "title": "title of the inc",
      "description": "description of the inc",
      "ownerqueuename" : "pen-making",
      "ownerusername" : "ek.worker",
      "assignedqueuename" : "refill-making",
      "assignedusername" : "aurek.worker",
      "servicename": "some servive",
      "ciname": "some ci",
      "severity": 1,
      "impact": 2,
      "isactive": true,
      "updatedAt": "2016-10-29T04:25:56.000Z"
    }
  */
  delete: function(req, res)
  {
    if(!req.body.templateid || Array.isArray(req.body.templateid) || !req.body.type || req.body.type !== enums.templateTypes.INCIDENT)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    switch(req.body.type)
    {
      case enums.templateTypes.INCIDENT:
        Inctemplate.findOne({ templateid: req.body.templateid })
        .then(function(template)
        {
          if(!template)
          {
            var error = new Error();
            error.code = 400501;
            error.message = errorCode[ '400501' ];
            throw error;
          }
          else
          {
            return Inctemplate.destroy({ templateid: req.body.templateid });
          }
        })
        .then(function(templates)
        {
          return res.ok(templates[ 0 ]);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
        break;
      default:
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        return res.negotiate(error);
    }
  },
  /**
  @apiGroup Template
  @apiName templateSearch
  @api {post} /template/search Search
  @apiDescription On input of a valid search parameters this function returns the selected attributes in the chosen sorting method
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
    curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS
    I6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc
    3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9u
    ZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3RpdmUiOnRydWV9LCJpYXQiOjE0Nzc
    1NTE1NDQsImV4cCI6MTQ3NzU3MzE0NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3MiOiJ3d3cucXVldWVsb2FkLmNvbS
    J9.tFwQlQZ8CTABuxEas87c5uzOCgJH9-6tGh10nBAbP6E" -H "Content-Type: application/json" -d
    '{
       "type" : 1,
       "search":  { "templateid" : 1 },
       "result" : [ "templateid", "lasteditorid", "isactive"],
       "orderby": "templateid"
    }' "http://localhost:1337/template/search"
  @apiParam {Object} search Object listing the attrbutes of the searched CI
  @apiParam {String[]} result Array of attributes requested
  @apiParam {String} orderby Attribute to order by the results
  @apiParamExample {json} Request-Example
    {
       "type" : 1,
       "search":  { "templateid" : 1 },
       "result" : [ "templateid", "lasteditorid", "isactive"],
       "orderby": "templateid"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400053
  @apiSuccess (200) {Object[]} templateList List of templates
  @apiSuccessExample {json} Success-Response
    [
      1,
      [
        {
          "lasteditorid": 1,
          "templateid": 1,
          "isactive": true
        }
      ]
    ]
  */
  search: function(req, res)
  {
    if(!req.body.type || !req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var type = parseInt(req.body.type);
    switch(type)
    {
      case enums.templateTypes.INCIDENT:
        var search = _.mapValues(req.body.search, function(value, key)
        {
          obj = _.pick(Inctemplate.definition, key);
          if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
          else return value;
        });
        Inctemplate.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
        .then(function(found)
        {
          return res.json([ found.length, found ]);
        })
        .catch(function(error)
        {
          if(_.includes(error.details, "does not exist"))
          {
            error = new Error();
            error.code = 400053;
            error.message = errorCode[ "400053" ];
            return res.negotiate(error);
          }
          else
          {
            sails.log.error("Error occured while searching", error);
            return res.ok([ 0, [] ]);
          }
        });
        break;
      default:
        var error = new Error();
        error.code = 500001;
        error.message = errorCode[ '500001' ];
        return res.negotiate(error);
    }
  }
};
