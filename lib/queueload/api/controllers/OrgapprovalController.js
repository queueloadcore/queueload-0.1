/* FileName: OrgApprovalController.js
* @description: This file describes the various Apis to handle approval process in the
* organization module
*
*   Date                Change Description                           Author
* ---------           ------------------------                     ---------
* 17/05/2016           Initial file creation                        bones
*
*/

/**
 * @apiGroup OrgApproval
 */

var enums = require("../resources/Enums.js");
var errorCode = require("../resources/ErrorCodes.js");
var userUtils = require("../services/utils/user.js");
var cbUtils = require("../services/utils/cbutils.js");
var Promise = require("bluebird");
var _ = require("lodash");

module.exports =
{
/**
  * Name: list
  * @description: This function lists all the approvals of the organization module in the system
  *
  * @param: None
  * @returns:
  */
  list: function(req, res)
  {
    OrgApproval.find({ approverid: req.body.requesterid, isactive: true })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400209;
        error.message = errorCode[ "400209" ];
        throw error;
      }
      else
      {
        return res.ok(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: initiatetransfer
  * @description: Initiates the trasfer process to transfer an entity(Queues and Services)
  * across chargeback codes. In case of chargeback codes, they are transferred across users.
  * This transfer process is a two step approval process.
  * @param:
      {
        entityid: 2,
        targetentityid: 3,
        approvaltype: 2
      }
  * @returns:

  */
  initiatetransfer: function(req, res)
  {
    // The API is open for bdadmins and operators
    if(!req.body.requesterroles.isbdadmin && !req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    var error = new Error();
    if(!req.body.entityid || Array.isArray(req.body.entityid) || !(_.isFinite(req.body.entityid)) ||
    !req.body.targetentityid || Array.isArray(req.body.targetentityid) || !(_.isFinite(req.body.targetentityid)) ||
    !req.body.approvaltype || Array.isArray(req.body.approvaltype) || !(_.isFinite(req.body.approvaltype)) || req.body.approvaltype >= enums.approval.LIMIT)
    {
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var newOwner, aprvCreated;
    if(req.body.approvaltype === enums.approval.TRANSFER_QUEUE)
    {
      cbUtils.getCB([ req.body.targetentityid ])
      .then(function foundTargetCB(foundCb)
      {
        if(!foundCb[ 0 ])
        {
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
        if(foundCb[ 0 ].isactive === false)
        {
          error.code = 400203;
          error.message = errorCode[ "400203" ];
          throw error;
        }
        if(foundCb[ 0 ].isfrozen)
        {
          error.code = 400208;
          error.message = errorCode[ "400208" ];
          throw error;
        }
        newOwner = foundCb[ 0 ].cbownerid.userid;
        return Queue.findOne({ queueid: req.body.entityid });
      })
      .then(function found(foundQueue)
      {
        if(!foundQueue)
        {
          error.code = 400254;
          error.message = errorCode[ "400254" ];
          throw error;
        }
        if(foundQueue.isactive === false)
        {
          error.code = 400253;
          error.message = errorCode[ "400253" ];
          throw error;
        }
        if(foundQueue.isfrozen)
        {
          error.code = 400264;
          error.message = errorCode[ "400264" ];
          throw error;
        }
        if(req.body.targetentityid === foundQueue.cbid)
        {
          error.code = 400210;
          error.message = errorCode[ "400210" ];
          throw error;
        }
        return Cb.findOne({ cbid: foundQueue.cbid });
      })
      .then(function foundEntityCB(foundCB)
      {
        //Create the two approvals: 1. present owner of CB associated with the Queue,
        //2. new owner of the CB to which the Queue is to be transferred
        return Promise.all([
          OrgApproval.create({ typeofapproval: enums.approval.TRANSFER_QUEUE, entityid: req.body.entityid, targetentityid: req.body.targetentityid, approverid: foundCB.cbownerid, initiatorid: req.body.requesterid, isapproved: null }),
          OrgApproval.create({ typeofapproval: enums.approval.TRANSFER_QUEUE, entityid: req.body.entityid, targetentityid: req.body.targetentityid, approverid: newOwner, initiatorid: req.body.requesterid, isapproved: null })
        ]);
      })
      .then(function created(created)
      {
        aprvCreated = created;

        //Set the Queue flag isfrozen to true
        return Queue.update({ queueid: req.body.entityid }, { isfrozen: true, freezecause: enums.freezeCauses.PENDING_TRANSFER });
      })
      .then(function updated(update)
      {
        return res.ok([ aprvCreated, update ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
    else if(req.body.approvaltype === enums.approval.TRANSFER_SERVICE)
    {
      cbUtils.getCB([ req.body.targetentityid ])
      .then(function foundTargetCB(foundCb)
      {
        if(!foundCb[ 0 ])
        {
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
        if(foundCb[ 0 ].isactive === false)
        {
          error.code = 400203;
          error.message = errorCode[ "400203" ];
          throw error;
        }
        if(foundCb[ 0 ].isfrozen)
        {
          error.code = 400208;
          error.message = errorCode[ "400208" ];
          throw error;
        }
        newOwner = foundCb[ 0 ].cbownerid.userid;
        return Service.findOne({ serviceid: req.body.entityid });
      })
      .then(function found(foundService)
      {
        if(!foundService)
        {
          error.code = 400362;
          error.message = errorCode[ "400362" ];
          throw error;
        }
        if(foundService.isfrozen)
        {
          error.code = 400363;
          error.message = errorCode[ "400363" ];
          throw error;
        }
        if(req.body.targetentityid === foundService.cbid)
        {
          error.code = 400210;
          error.message = errorCode[ "400210" ];
          throw error;
        }
        return Cb.findOne({ cbid: foundService.cbid });
      })
      .then(function foundEntityCB(foundCB)
      {
        //Create the two approvals: 1. present owner of CB associated with the service,
        //2. new owner of the CB to which the service is to be transferred
        return Promise.all([
          OrgApproval.create({ typeofapproval: enums.approval.TRANSFER_SERVICE, entityid: req.body.entityid, targetentityid: req.body.targetentityid, approverid: foundCB.cbownerid, initiatorid: req.body.requesterid, isapproved: null }),
          OrgApproval.create({ typeofapproval: enums.approval.TRANSFER_SERVICE, entityid: req.body.entityid, targetentityid: req.body.targetentityid, approverid: newOwner, initiatorid: req.body.requesterid, isapproved: null })
        ]);
      })
      .then(function created(created)
      {
        aprvCreated = created;

        //Set the Service flag isfrozen to true
        return Service.update({ serviceid: req.body.entityid }, { isfrozen: true, freezecause: enums.freezeCauses.PENDING_TRANSFER });
      })
      .then(function updated(update)
      {
        return res.ok([ aprvCreated, update ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
    else
    {
      userUtils.isActive([ req.body.targetentityid ])
      .then(function foundTargetUser(foundUser)
      {
        if(!foundUser[ 0 ])
        {
          error.code = 400104;
          error.message = errorCode[ "400104" ];
          throw error;
        }
        if(foundUser[ 0 ].isactive === false)
        {
          error.code = 400103;
          error.message = errorCode[ "400103" ];
          throw error;
        }
        return Cb.findOne({ cbid: req.body.entityid });
      })
      .then(function foundCB(foundCb)
      {
        if(!foundCb)
        {
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
        if(foundCb.isactive === false)
        {
          error.code = 400203;
          error.message = errorCode[ "400203" ];
          throw error;
        }
        if(foundCb.isfrozen)
        {
          error.code = 400208;
          error.message = errorCode[ "400208" ];
          throw error;
        }
        if(req.body.targetentityid === foundCb.cbownerid)
        {
          error.code = 400210;
          error.message = errorCode[ "400210" ];
          throw error;
        }

        //Create the two approvals: 1. present owner of CB, 2. new owner of the CB
        return Promise.all([
          OrgApproval.create({ typeofapproval: enums.approval.TRANSFER_CB, entityid: req.body.entityid, targetentityid: req.body.targetentityid, approverid: foundCb.cbownerid, initiatorid: req.body.requesterid, isapproved: null }),
          OrgApproval.create({ typeofapproval: enums.approval.TRANSFER_CB, entityid: req.body.entityid, targetentityid: req.body.targetentityid, approverid: req.body.targetentityid, initiatorid: req.body.requesterid, isapproved: null })
        ]);
      })
      .then(function created(created)
      {
        aprvCreated = created;

        //Set the cb flag isfrozen to true
        return Cb.update({ cbid: req.body.entityid }, { isfrozen: true, freezecause: enums.freezeCauses.PENDING_TRANSFER });
      })
      .then(function updated(update)
      {
        return res.ok([ aprvCreated, update ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },

/**
  * Name: approve
  * @description: Approves or declines various approvals generated for the
  * Organization module. This api supports both one and two step approval process
  *
  * @param:
    {
      "approvalid": 6,
      "approved" : true
    }
  * @returns:
    if approved === true and transfer and first approval --> updated approval
    if approved === false and transfer and first approval --> updated approvals
    if approved === true || false and transfer and second approval --> updated approvals and updated entity
    if approved === true and assign --> updated approval and entity
    if approved === false and assign --> updated approval (for Q and CB), updated approval and updated entity for CI

  */
  approve: function(req, res)
  {
    if(!(req.body.approvalid && _.isFinite(req.body.approvalid)) || typeof(req.body.approved) !== "boolean" || req.body.approved === "null")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var origApprovalDetails = {};
    OrgApproval.findOne({ approvalid: req.body.approvalid })
    .then(function foundApproval(approval)
    {
      origApprovalDetails = approval;
      var error = new Error();

      // If the approvalID doesn't exist.
      if(!approval)
      {
        error.code = 400368;
        error.message = errorCode[ "400368" ];
        throw error;
      }

      // If the approval ID is already processed
      else if(approval.isactive !== true)
      {
        error.code = 400263;
        error.message = errorCode[ "400263" ];
        throw error;
      }
      // If the requster is not the actual approver
      else if(approval.approverid !== req.body.requesterid)
      {
        error.code = 400367;
        error.message = errorCode[ "400367" ];
        throw error;
      }
      // Update the approval entry
      // if the approvals are of TRANSFER type
      //if(approval.typeofapproval >= enums.approval.TRANSFER_SERVICE && approval.typeofapproval <= enums.approval.TRANSFER_CB)
      //{
      //  return OrgApproval.update({ approvalid: req.body.approvalid }, { isapproved: req.body.approved });
      //}
      //// if the approval is of ASSIGNMENT type
      //else
      //{
      //  return OrgApproval.update({ approvalid: req.body.approvalid }, { isapproved: req.body.approved, isactive: false });
      //}
      if(req.body.approved)
      {
        return OrgApproval.update({ approvalid: req.body.approvalid }, { isapproved: req.body.approved, isactive: false });
      }
      else
      {
        var pUpdApproval = OrgApproval.update({ approvalid: req.body.approvalid }, { isapproved: req.body.approved, isactive: false });
        var pDeactiveApprovals = OrgApproval.update({ typeofapproval: origApprovalDetails.typeofapproval, entityid: origApprovalDetails.entityid, isactive: true }, { isactive: false });
        return Promise.all([ pUpdApproval, pDeactiveApprovals ]);
      }
    })
    .then(function updatedApproval(approval)
    {
        return OrgApproval.find({ typeofapproval: origApprovalDetails.typeofapproval, entityid: origApprovalDetails.entityid, isactive: true });
    })
    .then(function foundApprovals(approvals)
    {
      if(approvals.length)
      {
        // There are pending approvals to be processed
        return;
      }
      else
      {
        switch(origApprovalDetails.typeofapproval)
        {
          case (enums.approval.ASSIGN_CI2Q):
          {
            if(req.body.approved)
            {
              return Cidetail.update({ ciid: origApprovalDetails.entityid }, { supportqueue: origApprovalDetails.targetentityid, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Cidetail.update({ ciid: origApprovalDetails.entityid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            break;
          }
          case (enums.approval.ASSIGN_SERVICE2Q):
          {
            if(req.body.approved)
            {
              return Service.update({ serviceid: origApprovalDetails.entityid }, { supportqueue: origApprovalDetails.targetentityid, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Service.update({ serviceid: origApprovalDetails.entityid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            break;
          }
          case (enums.approval.ASSIGN_CB2U):
          {
            if(req.body.approved)
            {
              return Cb.update({ cbid: origApprovalDetails.entityid }, { cbownerid: origApprovalDetails.targetentityid, isfrozen: false, isactive: true, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Cb.update({ cbid: origApprovalDetails.entityid }, { freezecause: enums.freezeCauses.CREATION_DECLINED });
            }
            break;
          }
          case (enums.approval.ASSIGN_Q2CB):
          {
            if(req.body.approved)
            {
              return Queue.update({ queueid: origApprovalDetails.entityid }, { cbid: origApprovalDetails.targetentityid, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Queue.update({ queueid: origApprovalDetails.entityid }, { freezecause: enums.freezeCauses.CREATION_DECLINED });
            }
            break;
          }
          case (enums.approval.TRANSFER_CB):
          {
            if(req.body.approved)
            {
              return Cb.update({ cbid: origApprovalDetails.entityid }, { cbownerid: origApprovalDetails.targetentityid, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Cb.update({ cbid: origApprovalDetails.entityid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            break;
          }
          case (enums.approval.TRANSFER_QUEUE):
          {
            if(req.body.approved)
            {
              return Queue.update({ queueid: origApprovalDetails.entityid }, { cbid: origApprovalDetails.targetentityid, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Queue.update({ queueid: origApprovalDetails.entityid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            break;
          }
          case (enums.approval.TRANSFER_SERVICE):
          {
            if(req.body.approved)
            {
              return Service.update({ serviceid: origApprovalDetails.entityid }, { cbid: origApprovalDetails.targetentityid, isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            else
            {
              return Service.update({ serviceid: origApprovalDetails.entityid }, { isfrozen: false, freezecause: enums.freezeCauses.NOT_FROZEN });
            }
            break;
          }
          default:
            break;
        }
      }
    })
    .then(function performed(result)
    {
      return res.ok(result);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
