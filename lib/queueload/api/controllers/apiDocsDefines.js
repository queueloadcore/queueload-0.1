/**
 * @apiDefine superuser Superuser access rights needed.
 * This is a cross business department action and requires superuser access rights.
 *
 */

/**
 * @apiDefine bdadmin Business Department administrator access rights needed.
 * This action is allowed for all active bdadmins in the system.
 *
 */

/**
 * @apiDefine user User access rights needed.
 * This action is allowed for all active users in the system.
 *
 */

/**
 * @apiDefine operator Operator access rights needed.
 * This action is allowed for all active operators in the system.
 *
 */

/**
@apiDefine 401
@apiError (401) {String} 401 Unauthorized user
@apiErrorExample {json} 401
 {
  "code": 401
  }
*/

/**
 @apiDefine 401503
 @apiError (401) {String} 401503 Provided credentials are incorrect
 @apiErrorExample {json} 401503
  {
   "code": 401503
  }
*/

/**
 @apiDefine 401505
 @apiError (401) {String} 401505 Token is expired
 @apiErrorExample {json} 401505
  {
   "code": 401505
  }
*/

/**
 @apiDefine 401508
 @apiError (401) {String} 401508 User password expired
 @apiErrorExample {json} 401508
  {
   "code": 401508
  }
*/

/**
  @apiDefine 400051
  @apiError (400) {String} 400051 Incorrect Parameters. Correct and try again
  @apiErrorExample {json} 400051
    {
     "code": 400051
    }
 */

 /**
   @apiDefine 400052
   @apiError (400) {String} 400052 No Valid fields submitted for update.
   @apiErrorExample {json} 400052
     {
      "code": 400052
     }
  */

/**
   @apiDefine 400053
   @apiError (400) {String} 400053 One or more entered columns in the search parameters do not exist
   @apiErrorExample {json} 400053
 {
   "code": 400053
 }
 */

 /**
   @apiDefine 400101
   @apiError (400) {String} 400101 No users available in the system
   @apiErrorExample {json} 400101
 {
   "code": 400101
 }
 */

 /**
   @apiDefine 400102
   @apiError (400) {String} 400102 One or more of the entered users are active
   @apiErrorExample {json} 400102
 {
   "code": 400102
 }
 */

 /**
   @apiDefine 400103
   @apiError (400) {String} 400103 One or more of the entered users are inactive
   @apiErrorExample {json} 400103
 {
   "code": 400103
 }
 */

 /**
   @apiDefine 400104
   @apiError (400) {String} 400104 One or more of the entered users do not exist
   @apiErrorExample {json} 400104
 {
   "code": 400104
 }
 */

 /**
   @apiDefine 400107
   @apiError (400) {String} 400107 One of the users is an owner of an active Chargeback code
   @apiErrorExample {json} 400107
 {
   "code": 400107
 }
 */

 /**
   @apiDefine 400108
   @apiError (400) {String} 400108 One of the users is an owner of an active queue
   @apiErrorExample {json} 400108
 {
   "code": 400108
 }
 */

 /**
   @apiDefine 400109
   @apiError (400) {String} 400109 One of the users is a manager of an active queue
   @apiErrorExample {json} 400109
 {
   "code": 400109
 }
 */

 /**
   @apiDefine 400111
   @apiError (400) {String} 400111 One or more users have open incidents
   @apiErrorExample {json} 400111
 {
   "code": 400111
 }
 */

 /**
   @apiDefine 400112
   @apiError (400) {String} 400112 Requested configuration doesn't exist
   @apiErrorExample {json} 400112
 {
   "code": 400112
 }
 */

 /**
   @apiDefine 400158
   @apiError (400) {String} 400158 An Active Business Department must have atleast one active admin
   @apiErrorExample {json} 400158
 {
   "code": 400158
 }
 */

 /**
   @apiDefine 400203
   @apiError (400) {String} 400203 One or more of the entered Chargeback codes are inactive
   @apiErrorExample {json} 400203
 {
   "code": 400203
 }
 */

 /**
   @apiDefine 400204
   @apiError (400) {String} 400204 One or more of the entered Chargeback codes do not exist
   @apiErrorExample {json} 400204
 {
   "code": 400204
 }
 */

 /**
   @apiDefine 400208
   @apiError (400) {String} 400208 One or more of the chargeback codes entered is frozen. Frozen chargeback codes cannot be processed
   @apiErrorExample {json} 400208
 {
   "code": 400208
 }
 */

  /**
   @apiDefine 400253
   @apiError (400) {String} 400253 One or more of the entered Queues are inactive
   @apiErrorExample {json} 400253
 {
   "code": 400253
 }
 */

  /**
   @apiDefine 400254
   @apiError (400) {String} 400254 One or more of the entered Queues do not exist
   @apiErrorExample {json} 400254
 {
   "code": 400254
 }
 */

 /**
   @apiDefine 400258
   @apiError (400) {String} 400258 One or more of the entered users is not a member of the queue
   @apiErrorExample {json} 400258
 {
   "code": 400258
 }
 */

 /**
   @apiDefine 400260
   @apiError (400) {String} 400260 There must be atleast one active member in the queue
   @apiErrorExample {json} 400260
 {
   "code": 400260
 }
 */

 /**
   @apiDefine 400264
   @apiError (400) {String} 400264 One or more of the queues entered is frozen. Frozen queues cannot be processed
   @apiErrorExample {json} 400264
 {
   "code": 400264
 }
 */

/**
   @apiDefine 400302
   @apiError (400) {String} 400302 One or more entered Configuration Item Types already exist
   @apiErrorExample {json} 400302
 {
   "code": 400302
 }
 */

/**
   @apiDefine 400303
   @apiError (400) {String} 400303 One or more entered Configuration Item Types do not exist
   @apiErrorExample {json} 400303
 {
   "code": 400303
 }
 */

/**
  @apiDefine 400311
  @apiError (400) {String} 400311 No Configuration Item environments available in the system
  @apiErrorExample {json} 400311
    {
     "code": 400311
    }
 */

/**
  @apiDefine 400312
  @apiError (400) {String} 400312 One or more environments entered already exist in the system
  @apiErrorExample {json} 400312
  {
    "code": 400312
  }
*/

/**
  @apiDefine 400313
  @apiError (400) {String} 400313 The entered environments do not exist
  @apiErrorExample {json} 400313
{
  "code": 400313
}
*/

/**
    @apiDefine 400321
    @apiError (400) {String} 400321 No Configuration Item locations available in the system
    @apiErrorExample {json} 400321
  {
    "code": 400321
  }
*/

/**
    @apiDefine 400322
    @apiError (400) {String} 400322 The entered location does not exist
    @apiErrorExample {json} 400322
  {
    "code": 400322
  }
*/

/**
  @apiDefine 400331
  @apiError (400) {String} 400331 No vendors available in the system
  @apiErrorExample {json} 400331
{
  "code": 400331
}
*/

/**
  @apiDefine 400332
  @apiError (400) {String} 400332 One or more of entered vendors do not exist
  @apiErrorExample {json} 400332
{
  "code": 400332
}
*/

/**
  @apiDefine 400333
  @apiError (400) {String} 400333 One or more vendors entered already exist in the system
  @apiErrorExample {json} 400333
{
  "code": 400333
}
*/

/**
  @apiDefine 400341
  @apiError (400) {String} 400341 No Configuration Item Network available in the system
  @apiErrorExample {json} 400341
{
  "code": 400341
}
*/

/**
  @apiDefine 400342
  @apiError (400) {String} 400342 The entered Configuration Item Network does not exist
  @apiErrorExample {json} 400342
{
  "code": 400342
}
*/

/**
  @apiDefine 400351
  @apiError (400) {String} 400351 No Configuration Item Hardware available in the system
  @apiErrorExample {json} 400351
{
  "code": 400351
}
*/

/**
  @apiDefine 400352
  @apiError (400) {String} 400352 The entered Configuration Item Hardware does not exist
  @apiErrorExample {json} 400352
{
  "code": 400352
}
*/

/**
  @apiDefine 400361
  @apiError (400) {String} 400361 No Services available in the system
  @apiErrorExample {json} 400361
{
  "code": 400361
}
*/

/**
  @apiDefine 400362
  @apiError (400) {String} 400362 One or more of the entered service do not exist
  @apiErrorExample {json} 400362
{
  "code": 400362
}
*/

/**
  @apiDefine 400363
  @apiError (400) {String} 400363 The requested service is frozen
  @apiErrorExample {json} 400363
{
  "code": 400363
}
*/

/**
  @apiDefine 400364
  @apiError (400) {String} 400364 The service is associated with one or more CIs
  @apiErrorExample {json} 400364
{
  "code": 400364
}
*/

/**
  @apiDefine 400365
  @apiError (400) {String} 400365 The service cannot be taken to an undefined state
  @apiErrorExample {json} 400365
{
  "code": 400365
}
*/

/**
  @apiDefine 400366
  @apiError (400) {String} 400366 The approval has already been processed
  @apiErrorExample {json} 400366
{
  "code": 400366
}
*/

/**
  @apiDefine 400367
  @apiError (400) {String} 400367 The user is not the designated approver
  @apiErrorExample {json} 400367
{
  "code": 400367
}
*/

/**
  @apiDefine 400368
  @apiError (400) {String} 400368 The approval ID doesn't exist
  @apiErrorExample {json} 400368
{
  "code": 400368
}
*/

/**
  @apiDefine 400369
  @apiError (400) {String} 400369 One or more services is not in approved state
  @apiErrorExample {json} 400369
{
  "code": 400369
}
*/

/**
  @apiDefine 400370
  @apiError (400) {String} 400370 Incorrect service
  @apiErrorExample {json} 400370
{
  "code": 400370
}
*/

/**
  @apiDefine 400371
  @apiError (400) {String} 400371 The service has pending approvals associated with it
  @apiErrorExample {json} 400371
{
  "code": 400371
}
*/

/**
  @apiDefine 400372
  @apiError (400) {String} 400372 No new service association requested
  @apiErrorExample {json} 400372
{
  "code": 400372
}
*/

/**
  @apiDefine 400373
  @apiError (400) {String} 400373 Configuration Item between Approved and Commissioned state must be associated with a service
  @apiErrorExample {json} 400373
{
  "code": 400373
}
*/

/**
  @apiDefine 400374
  @apiError (400) {String} 400374 Service cannot be cancelled as it's not in the registration state
  @apiErrorExample {json} 400374
{
  "code": 400374
}
*/

/**
  @apiDefine 400375
  @apiError (400) {String} 400375 Incorrect CI
  @apiErrorExample {json} 400375
{
  "code": 400375
}
*/

/**
  @apiDefine 400376
  @apiError (400) {String} 400376 Service state doesn't permit action. Please use '/orgapproval/initiatetransfer' to update chargeback code.
  @apiErrorExample {json} 400376
{
  "code": 400376
}
*/

/**
  @apiDefine 400377
  @apiError (400) {String} 400377 The requester has no pending service approvals
  @apiErrorExample {json} 400377
{
  "code": 400377
}
*/

/**
  @apiDefine 400378
  @apiError (400) {String} 400378 Service not in 'approved' state
  @apiErrorExample {json} 400378
{
  "code": 400378
}
*/

/**
  @apiDefine 400380
  @apiError (400) {String} 400380 Service is in Archived state
  @apiErrorExample {json} 400380
{
  "code": 400380
}
*/

/**
  @apiDefine 400381
  @apiError (400) {String} 400381 No Configuration Items available in the system
  @apiErrorExample {json} 400381
{
  "code": 400381
}
*/

/**
  @apiDefine 400382
  @apiError (400) {String} 400382 One or more of entered Configuration Items do not exist
  @apiErrorExample {json} 400382
{
  "code": 400382
}
*/

/**
  @apiDefine 400383
  @apiError (400) {String} 400383 Requested Configuration Item is frozen at the moment.
  @apiErrorExample {json} 400383
{
  "code": 400383
}
*/

/**
  @apiDefine 400384
  @apiError (400) {String} 400384 Configuration Item cannot be moved to undefined state
  @apiErrorExample {json} 400384
{
  "code": 400384
}
*/

/**
  @apiDefine 400385
  @apiError (400) {String} 400385 Configuration Item is associated with services
  @apiErrorExample {json} 400385
{
  "code": 400385
}
*/

/**
  @apiDefine 400386
  @apiError (400) {String} 400386 Configuration Item must be associted with atleast one service
  @apiErrorExample {json} 400386
{
  "code": 400386
}
*/

/**
  @apiDefine 400387
  @apiError (400) {String} 400387 Configuration Item state doesn't support the requested operation
  @apiErrorExample {json} 400387
{
  "code": 400387
}
*/

/**
  @apiDefine 400388
  @apiError (400) {String} 400388 Configuration Item doesn't have any support queue
  @apiErrorExample {json} 400388
{
  "code": 400388
}
*/

/**
  @apiDefine 400389
  @apiError (400) {String} 400389 Configuration Item is not associated with the service
  @apiErrorExample {json} 400389
{
  "code": 400389
}
*/

/**
  @apiDefine 400391
  @apiError (400) {String} 400391 Configuration Item has open incidents with the requested queue
  @apiErrorExample {json} 400391
{
  "code": 400391
}
*/

/**
  @apiDefine 400401
  @apiError (400) {String} 400401 No file attached
  @apiErrorExample {json} 400401
{
  "code": 400401
}
*/

/**
  @apiDefine 400402
  @apiError (400) {String} 400402 Requested file doesn't exist
  @apiErrorExample {json} 400402
{
  "code": 400402
}
*/

/**
  @apiDefine 400405
  @apiError (400) {String} 400405 One or more of the attachments do not exist
  @apiErrorExample {json} 400405
{
  "code": 400405
}
*/

/**
  @apiDefine 400406
  @apiError (400) {String} 400406 Please provide unique set of attachments
  @apiErrorExample {json} 400406
{
  "code": 400406
}
*/

/**
  @apiDefine 403501
  @apiError (403) {String} 403501 The requester is not an admin of the BD
  @apiErrorExample {json} 403501
{
  "code": 403501
}
*/

/**
  @apiDefine 403502
  @apiError (403) {String} 403502 The requester doesn't have the required privilege
  @apiErrorExample {json} 403502
{
  "code": 403502
}
*/

/**
  @apiDefine 400451
  @apiError (400) {String} 400451 No incidents in the system
  @apiErrorExample {json} 400451
{
  "code": 400451
}
*/

/**
  @apiDefine 400452
  @apiError (400) {String} 400452 Invalid severity code
  @apiErrorExample {json} 400452
{
  "code": 400452
}
*/

/**
  @apiDefine 400453
  @apiError (400) {String} 400453 Invalid impact code
  @apiErrorExample {json} 400453
{
  "code": 400453
}
*/

/**
  @apiDefine 400454
  @apiError (400) {String} 400454 One or more of the entered Incidents do not exist
  @apiErrorExample {json} 400454
{
  "code": 400454
}
*/

/**
  @apiDefine 400456
  @apiError (400) {String} 400456 The current state doesn't allow transition to the requested state
  @apiErrorExample {json} 400456
{
  "code": 400456
}
*/

/**
  @apiDefine 400458
  @apiError (400) {String} 400458 Ticket needs an assignee to move to further states
  @apiErrorExample {json} 400458
{
  "code": 400458
}
*/

/**
  @apiDefine 400459
  @apiError (400) {String} 400459 Must provide a queueid to transisition into OPEN state
  @apiErrorExample {json} 400459
{
  "code": 400459
}
*/

/**
  @apiDefine 400461
  @apiError (400) {String} 400461 Please provide an explanation
  @apiErrorExample {json} 400461
{
  "code": 400461
}
*/

/**
  @apiDefine 400462
  @apiError (400) {String} 400462 Please provide the res code and res notes and endtime
  @apiErrorExample {json} 400462
{
  "code": 400462
}
*/

/**
  @apiDefine 400463
  @apiError (400) {String} 400463 Only journal entries are allowed while marking UNRESOLVED
  @apiErrorExample {json} 400463
{
  "code": 400463
}
*/

/**
  @apiDefine 400464
  @apiError (400) {String} 400464 Please provide the closure notes
  @apiErrorExample {json} 400464
{
  "code": 400464
}
*/

/**
  @apiDefine 400465
  @apiError (400) {String} 400465 Only closure notes are allowed while marking CLOSED
  @apiErrorExample {json} 400465
{
  "code": 400465
}
*/

/**
  @apiDefine 400466
  @apiError (400) {String} 400466 Please provide the incident number causing the pendency of this ticket
  @apiErrorExample {json} 400466
{
  "code": 400466
}
*/

/**
  @apiDefine 400467
  @apiError (400) {String} 400467 Please provide the vendor
  @apiErrorExample {json} 400467
{
  "code": 400467
}
*/

/**
  @apiDefine 400468
  @apiError (400) {String} 400468 The ticket is already assigned to the requested member
  @apiErrorExample {json} 400468
{
  "code": 400468
}
*/

/**
  @apiDefine 400469
  @apiError (400) {String} 400469 End time cannot be earlier then start time
  @apiErrorExample {json} 400469
{
  "code": 400469
}
*/

/**
  @apiDefine 400470
  @apiError (400) {String} 400470 Requester is not part of any of the queues
{
  "code": 400470
}
*/

/**
  @apiDefine 400471
  @apiError (400) {String} 400471 Requester doesn't have the privilege to perform the action
  @apiErrorExample {json} 400471
{
  "code": 400471
}
*/

/**
  @apiDefine 400472
  @apiError (400) {String} 400472 All the sent attachments are already attached
  @apiErrorExample {json} 400472
{
  "code": 400472
}
*/

/**
  @apiDefine 400473
  @apiError (400) {String} 400473 Current state of the incident doesn't allow cancelation
  @apiErrorExample {json} 400473
{
  "code": 400473
}
*/

/**
  @apiDefine 400477
  @apiError (400) {String} 400477 Invalid satisfaction rating
  @apiErrorExample {json} 400477
{
  "code": 400477
}
*/

/**
  @apiDefine 400521
  @apiError (400) {String} 400521 Company doesn't exist
  @apiErrorExample {json} 400521
{
  "code": 400521
}
*/

/**
  @apiDefine 400522
  @apiError (400) {String} 400522 Account administrator must have the super user privilege
  @apiErrorExample {json} 400522
{
  "code": 400522
}
*/

/**
  @apiDefine 400501
  @apiError (400) {String} 400501 The requested template doesn't exist
  @apiErrorExample {json} 400501
{
  "code": 400501
}
*/

/**
  @apiDefine 400502
  @apiError (400) {String} 400502 Invalid keys in the template
  @apiErrorExample {json} 400502
{
  "code": 400502
}
*/

/**
  @apiDefine 400503
  @apiError (500) {String} 400503 Template is not active
  @apiErrorExample {json} 400503
{
  "code": 400503
}
*/

/**
  @apiDefine 400525
  @apiError (400) {String} 400525 Didn\'t find the loggly config id
  @apiErrorExample {json} 400525
{
  "code": 400525
}
*/

/**
  @apiDefine 500001
  @apiError (500) {String} 500001 System Error. Please contact your administrator
  @apiErrorExample {json} 500001
{
  "code": 500001
}
*/
