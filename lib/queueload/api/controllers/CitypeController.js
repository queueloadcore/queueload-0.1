/* FileName: CitypeController.js
* @description: This file describes the various actions in the Citype controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 01/03/2016    Initial file creation                         SMandal
*
*
*/

/**
 * @apiGroup Citype
 */

var errorCode = require("../resources/ErrorCodes.js");
var Promise = require("bluebird");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var _ = require("lodash");
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{

  controlTest: function(req, res)
  {
    res.json({ message: "Citype controller is working. :)" });
  },
/**
  @apiGroup Citype
  @apiName listCitypes
  @api {post} /citype/list List
  @apiDescription This API lists the CI types in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjY
     yNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.
     2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ" -d '{ "limit": 10, "page": 1}'
     "http://localhost:1337/citype/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400321
  @apiSuccess (200) {Object[]} locations Array of Citypes
  @apiSuccessExample {json} Success-Response
    [{
      "citypeid": 1,
      "citype": "switch"
    },
    {
      "citypeid": 2,
      "citype": "rack"
    },
    {
      "citypeid": 3,
      "citype": "blade"
    }]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    var output = [];
    Citype.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400301;
        error.message = errorCode[ "400301" ];
        throw error;
      }
      else
      {
        output = found;
        var countCis = function(element)
        {
          return Cidetail.count().where({ citypeid: element.citypeid });
        };
        var actions = found.map(countCis);
        return Promise.all(actions);
      }
    })
    .then(function(result)
    {
      var assign = function(element, index)
      {
        output[ index ].numberofcis = element;
      };
      result.forEach(assign);
      return res.ok(output);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /citype/add Add
  @apiName addCitype
  @apiGroup Citype
  @apiDescription On input of all valid parameters this function will add ci types
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGVzdC5
     1c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGx
     veWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3NTczMTgyMywiZXhwIjoxNDc1NzUzNDIzLCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.RCcGmdowferp2sMlFWTKRHkZkeowIs4Twl4
     ZiCzlDck" -H "Content-Type: application/json" -d
     '{
        "citypes": ["mudhi dabba"]
      }' "http://localhost:1337/citype/add"
  @apiParam {String[]} citypes Array of citypes
  @apiParamExample {json} Request-Example
     {
      "citypes": ["mudhi dabba"]
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400302
  @apiUse 403502
  @apiSuccess (200) {Object[]} Citype Array of added CI Types
  @apiSuccessExample {json} Success-Response
    [
      {
        "citypeid": 5,
        "citype": "mudhi dabba"
      }
    ]
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    // Check if it is not an array
    if(!req.body.citypes || !(Array.isArray(req.body.citypes)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var types = _.uniq(req.body.citypes);

    // Check if the input has duplicate entries
    if(types.length !== req.body.citypes.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var results;
    Citype.find({ citype:req.body.citypes })
    .then(function showRecs(records)
    {
      // Check if any of the input entries exist in the system
      if(records.length > 0)
      {
        var error = new Error();
        error.code = 400302;
        error.message = errorCode[ "400312" ];
        throw error;
      }
      else
      {
        var citypeObj = function(citype)
        {
          this.citype = citype;
        };
        var types = [];
        for(var iter = 0; iter < req.body.citypes.length; iter++)
        {
          types.push(new citypeObj(req.body.citypes[ iter ]));
        }
        return Citype.create(types);
      }
    })
    .then(function showRecs(result)
    {
      results = result;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(results);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /citype/update Update
  @apiName updateCitype
  @apiGroup Citype
  @apiDescription On input of all valid parameters this function updates the type definitions
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGVzdC5
     1c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGx
     veWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3NTczMTgyMywiZXhwIjoxNDc1NzUzNDIzLCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.RCcGmdowferp2sMlFWTKRHkZkeowIs4Twl4
     ZiCzlDck" -H "Content-Type: application/json" -d
      '[
         {"citypeid": 2, "citype": "abra ka dabra"},
         {"citypeid": 3, "citype": "khulja simsim"}
       ]'
      "http://localhost:1337/citype/update"
  @apiParam {Object[]} citype Array of Citypes
  @apiParamExample {json} Request-Example
     [
      {"citypeid": 2, "citype": "abra ka dabra"},
      {"citypeid": 3, "citype": "khulja simsim"}
    ]
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400302
  @apiUse 400303
  @apiUse 403502
  @apiSuccess (200) {Object} location Details of the added location
  @apiSuccessExample {json} Success-Response
    [
      [
        {
          "citypeid": 2,
          "citype": "abra ka dabra"
        }
      ],
      [
        {
          "citypeid": 3,
          "citype": "khulja simsim"
        }
      ]
    ]
  */
  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    var i = 0;
    var ids = [];
    var citypes = [];
    var types = [];
    for(i = 0; i < req.body.length; i++)
    {
      if(!req.body[ i ].citype || !req.body[ i ].citypeid || !(_.isFinite(req.body[ i ].citypeid)))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
      else
      {
        // Check if any of the input objects attributes are arrays
        if(Array.isArray(req.body[ i ].citypeid) || Array.isArray(req.body[ i ].citype))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
        ids.push(req.body[ i ].citypeid);
        citypes.push(req.body[ i ]);
        types.push(req.body[ i ].citype);
      }
    }

    var names = _.uniq(types);

    // Check if the input has any duplicate citypes
    if(names.length !== types.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var results;
    Citype.find({ citypeid: ids })
    .then(function found(records)
    {
      // Check if any the input has any non-existent citype ids. It also checks if there are duplicate citypeids in the input
      if(records.length !== ids.length)
      {
        var error = new Error();
        error.code = 400303;
        error.message = errorCode[ "400303" ];
        throw error;
      }
      else
      {
        return Citype.find({ citype: types });
      }
    })
    .then(function showRecs(records)
    {
      // Check if the input has any existing citypes
      if(records.length > 0)
      {
        var error = new Error();
        error.code = 400302;
        error.message = errorCode[ "400302" ];
        throw error;
      }
      else
      {
        var fn = function aysncupdate(type)
        {
          // Even if any inidividual object fails validation, the rest of rows are updated. Bug# D5-12
          return Citype.update({ citypeid: type.citypeid }, type);
        };

        var actions = citypes.map(fn);
        return Promise.all(actions);
      }
    })
    .then(function showRecs(result)
    {
      results = result;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(results);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
