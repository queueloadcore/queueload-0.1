/* FileName: CompanyController.js
*  @description: This file describes the various actions in the Company controller.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  25/11/2016        Initial file creation                         SMandal
*
*/

/**
 * ApiGroup Company
 */
var enums = require('../resources/Enums.js');
var errorCode = require("../resources/ErrorCodes.js");
var userUtils = require("../services/utils/user.js");
var queueUtils = require("../services/utils/queueutils.js");
var genUtils = require('../services/utils/genutils.js');
var _ = require("lodash");

module.exports =
{
	/**
  @api {post} /company/update Update
  @apiName updateLocation
  @apiGroup Company
  @apiDescription On input of all valid parameters this function will update the company details
  @apiPermission superuser
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwiY29tcGFueWlkIjoxLCJ1c2VyaWQiOjEsImZpcnN0bmFtZSI6IlN1cGV
     yIiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJhZG1pbmlzdHJhdG9yIiwiZW1haWxpZCI6ImFkbWluaXN0cmF0b3JAdGV
     zdGRvbWFpbi5jb20iLCJjb3VudHJ5Y29kZSI6Iis5MSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGxveWVlaWQiOiJ
     GVEUwMDAwMDEiLCJpc2FjdGl2ZSI6dHJ1ZSwicGFzc2V4cGlyZWQiOm51bGx9LCJpYXQiOjE0ODAwNjA1OTEsImV4cCI6MTQ4MDA
     4MjE5MSwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3MiOiJ3d3cucXVldWVsb2FkLmNvbSJ9.76qrozCqQyYMALABqcBFeM
     TQYVdPLlJSQlN0t4cfbiM" -d
      '{
        "supportmgr" : "support@queueload.com"
      }' "http://localhost:1337/company/update"
  @apiParam {String} [name] Name of the company
  @apiParam {String} [address] Address
  @apiParam {Number} [accntadmin] User id of the acount admin(Must be an active superuser)
  @apiParam {Email} [supportmgr] Email ID of the support manager from queueload
  @apiParamExample {json} Request-Example
    {
      "supportmgr" : "support@queueload.com"
    }
  @apiUse 401
  @apiUse 400103
  @apiUse 400051
  @apiUse 400322
  @apiUse 403502
  @apiUse 400521
  @apiuse 400522
  @apiSuccess (200) {Object} location Details of the added location
  @apiSuccessExample {json} Success-Response
    [
      {
        "accountid": "qlodtest",
        "name": "Wayne Enterprise",
        "address": "Gotham",
        "accntadmin": 1,
        "supportmgr": "support@queueload.com",
        "isactive": null
      }
    ]
  */
  update: function(req, res)
  {
    // The API is open only for superusers
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    var checks = [];
    if(typeof req.body.isactive !== 'undefined')
    {
      var error = new Error();
      error.code = 400051;
      return res.negotiate(error);
    }
    if(req.body.accntadmin)
    {
      checks.push(userUtils.getUser([ req.body.accntadmin ]));
    }
    var seek = 0;
    Promise.all(checks)
    .then(function(results)
    {
      if(req.body.accntadmin)
      {
        //The account admin must exist
        if(!results[ seek ][ 0 ])
        {
          var error = new Error();
          error.code = 400104;
          error.message = errorCode[ '400104' ];
          throw error;
        }
        //The account admin must be active
        else if(!results[ seek ][ 0 ].isactive)
        {
          var error = new Error();
          error.code = 400103;
          error.message = errorCode[ '400103' ];
          throw error;
        }
        //The account admin must be a superuser
        else if(!results[ seek ][ 0 ].roles[ 0 ].issuperuser)
        {
          var error = new Error();
          error.code = 400522;
          error.message = errorCode[ '400522' ];
          throw error;
        }
        seek = seek + 1;
      }
      return Company.update({ accountid: req.body.accountid }, req.body);
    })
    .then(function(company)
    {
      return res.json(company[ 0 ]);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  getintegrationinfo: function(req, res)
  {
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.integrationtype || (req.body.integrationtype < 1 || req.body.integrationtype >= enums.integrationService.LIMIT))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      Integrationconfig.findOne({ integrationtype: req.body.integrationtype })
      .then(function(config)
      {
        if(config)
        {
          config.config.isintegrated = true;
          config.config.integrationtype = req.body.integrationtype;
          switch(req.body.integrationtype)
          {
            case enums.integrationService.CLOUDWATCH:
              config.config.endpoint = sails.config.intcfg.qlIntegrationUrl + "event/receivecwevents?qlaccountid=" + process.env.ACCOUNTID + "&awscwkey=" + config.config.awscwkey;
              delete config.config.awscwkey;
              break;
            case enums.integrationService.SENTRY:
              config.config.endpoint = sails.config.intcfg.qlIntegrationUrl + "event/receivesentryevents?qlaccountid=" + process.env.ACCOUNTID + "&sentrykey=" + config.config.sentrykey;
              delete config.config.sentrykey;
              break;
            case enums.integrationService.WEAVED:
              break;
            case enums.integrationService.NAGIOS:
              config.config.endpoint = sails.config.intcfg.qlIntegrationUrl + "event/receivenagiosevents?qlaccountid=" + process.env.ACCOUNTID + "&nagioskey=" + config.config.nagioskey;
              delete config.config.nagioskey;
              break;
            case enums.integrationService.SLACK:
              break;
            case enums.integrationService.LOGGLY:
              config.config.endpoint = sails.config.intcfg.qlIntegrationUrl + "event/receivelogglyevents?qlaccountid=" + process.env.ACCOUNTID + "&logglykey=" + config.config.logglykey;
              delete config.config.logglykey;
              break;
            case enums.integrationService.CUSTOM:
              config.config.endpoint = sails.config.intcfg.qlIntegrationUrl + "event/receivecustomevents?qlaccountid=" + process.env.ACCOUNTID + "&customkey=" + config.config.customkey;
              delete config.config.customkey;
              break;
            case enums.integrationService.AZURE:
              config.config.endpoint = sails.config.intcfg.qlIntegrationUrl + "event/receiveazureevents?qlaccountid=" + process.env.ACCOUNTID + "&azurekey=" + config.config.azurekey;
              delete config.config.azurekey;
              break;
            default:
              break;
          }
          return res.json(config.config);
        }
        else
        {
          var config =
          {
            isintegrated: false,
            endpoint: genUtils.genHookAddress(req.body.integrationtype),
            integrationtype: req.body.integrationtype
          };
          return res.json(config);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  addintegration: function(req, res)
  {
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.endpoint || !req.body.ownerqueueid || !req.body.owneruserid || !req.body.integrationtype || (req.body.integrationtype < 1 || req.body.integrationtype >= enums.integrationService.LIMIT))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      var pCheckMembership = queueUtils.isMember(req.body.ownerqueueid, req.body.owneruserid);
      var pCheckIntegration = Integrationconfig.findOne({ integrationtype: req.body.integrationtype });
      var pCheckUser = User.findOne({ userid: req.body.owneruserid, isactive: true });
      var pCheckQueue = Queue.findOne({ queueid: req.body.ownerqueueid, isactive: true });
      Promise.all([ pCheckMembership, pCheckIntegration, pCheckUser, pCheckQueue ])
      .then(function(results)
      {
        if(!results[ 0 ])
        {
          var error = new Error();
          error.code = 400258;
          error.message = errorCode [ '400258' ];
          throw error;
        }
        else if(results[ 1 ])
        {
          var error = new Error();
          error.code = 400527;
          error.message = errorCode [ '400527' ];
          throw error;
        }
        else if(!results[ 2 ])
        {
          var error = new Error();
          error.code = 400103;
          error.message = errorCode [ '400103' ];
          throw error;
        }
        else if(!results[ 3 ])
        {
          var error = new Error();
          error.code = 400253;
          error.message = errorCode [ '400253' ];
          throw error;
        }
        else
        {
          var config =
          {
            ownerqueueid: req.body.ownerqueueid,
            owneruserid: req.body.owneruserid
          };
          switch(req.body.integrationtype)
          {
            case enums.integrationService.CLOUDWATCH:
              if(!req.body.endpoint.includes('&awscwkey='))
              {
                var error = new Error();
                error.code = 400528;
                error.message = errorCode[ '400528' ];
                throw error;
              }
              else
              {
                config.awscwkey = req.body.endpoint.slice(req.body.endpoint.lastIndexOf("&awscwkey=") + "&awscwkey=".length);
                config.confirmed = false;
              }
              break;
            case enums.integrationService.SENTRY:
              if(!req.body.endpoint.includes('&sentrykey='))
              {
                var error = new Error();
                error.code = 400528;
                error.message = errorCode[ '400528' ];
                throw error;
              }
              else
              {
                config.sentrykey = req.body.endpoint.slice(req.body.endpoint.lastIndexOf("&sentrykey=") + "&sentrykey=".length);
              }
              break;
            case enums.integrationService.WEAVED:
              break;
            case enums.integrationService.NAGIOS:
              if(!req.body.endpoint.includes('&nagioskey='))
              {
                var error = new Error();
                error.code = 400528;
                error.message = errorCode[ '400528' ];
                throw error;
              }
              else
              {
                config.nagioskey = req.body.endpoint.slice(req.body.endpoint.lastIndexOf("&nagioskey=") + "&nagioskey=".length);
              }
              break;
            case enums.integrationService.SLACK:
              break;
            case enums.integrationService.LOGGLY:
              if(!req.body.endpoint.includes('&logglykey='))
              {
                var error = new Error();
                error.code = 400528;
                error.message = errorCode[ '400528' ];
                throw error;
              }
              else
              {
                config.logglykey = req.body.endpoint.slice(req.body.endpoint.lastIndexOf("&logglykey=") + "&logglykey=".length);
              }
              break;
            case enums.integrationService.CUSTOM:
              if(!req.body.endpoint.includes('&customkey='))
              {
                var error = new Error();
                error.code = 400528;
                error.message = errorCode[ '400528' ];
                throw error;
              }
              else
              {
                config.customkey = req.body.endpoint.slice(req.body.endpoint.lastIndexOf("&customkey=") + "&customkey=".length);
              }
              break;
            case enums.integrationService.AZURE:
              if(!req.body.endpoint.includes('&azurekey='))
              {
                var error = new Error();
                error.code = 400528;
                error.message = errorCode[ '400528' ];
                throw error;
              }
              else
              {
                config.azurekey = req.body.endpoint.slice(req.body.endpoint.lastIndexOf("&azurekey=") + "&azurekey=".length);
              }
              break;
            default:
              break;
          }
          return Integrationconfig.create({ integrationtype: req.body.integrationtype, integratorid: req.body.requesterid, config: config });
        }
      })
      .then(function(result)
      {
        return res.json(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  updateintegration: function(req, res)
  {
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(req.body.endpoint || !req.body.ownerqueueid || !req.body.owneruserid || !req.body.integrationtype || (req.body.integrationtype < 1 || req.body.integrationtype >= enums.integrationService.LIMIT))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      var pCheckMembership = queueUtils.isMember(req.body.ownerqueueid, req.body.owneruserid);
      var pCheckIntegration = Integrationconfig.findOne({ integrationtype: req.body.integrationtype });
      var pCheckUser = User.findOne({ userid: req.body.owneruserid, isactive: true });
      var pCheckQueue = Queue.findOne({ queueid: req.body.ownerqueueid, isactive: true });
      Promise.all([ pCheckMembership, pCheckIntegration, pCheckUser, pCheckQueue ])
      .then(function(results)
      {
        if(!results[ 0 ])
        {
          var error = new Error();
          error.code = 400258;
          error.message = errorCode [ '400258' ];
          throw error;
        }
        else if(!results[ 1 ])
        {
          var error = new Error();
          error.code = 400526;
          error.message = errorCode[ '400526' ];
          throw error;
        }
        else if(!results[ 2 ])
        {
          var error = new Error();
          error.code = 400103;
          error.message = errorCode [ '400103' ];
          throw error;
        }
        else if(!results[ 3 ])
        {
          var error = new Error();
          error.code = 400253;
          error.message = errorCode [ '400253' ];
          throw error;
        }
        else
        {
          var config = results[ 1 ].config;
          config.ownerqueueid = req.body.ownerqueueid;
          config.owneruserid = req.body.owneruserid;
          return Integrationconfig.update({ integrationtype: req.body.integrationtype }, { config: config });
        }
      })
      .then(function(result)
      {
        return res.json(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  removeintegration: function(req, res)
  {
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.integrationtype || (req.body.integrationtype < 1 || req.body.integrationtype >= enums.integrationService.LIMIT))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      Integrationconfig.destroy({ integrationtype: req.body.integrationtype })
      .then(function(config)
      {
        if(!config.length)
        {
          var error = new Error();
          error.code = 400526;
          error.message = errorCode[ '400526' ];
          throw error;
        }
        else
        {
          return res.json(config);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  logglyalertmap: function(req, res)
  {
    if(!req.body.requesterroles.issuperuser)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!Array.isArray(req.body.maps) || !req.body.maps.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      var correctMaps = true;
      var serviceids = [];
      var alertnames = [];
      correctMaps = req.body.maps.every(function(map)
      {
        if(typeof map !== 'object' || !map.hasOwnProperty('serviceid') || !map.hasOwnProperty('alertname'))
        {
          return false;
        }
        else
        {
          serviceids.push(map.serviceid);
          alertnames.push(map.alertname);
          return true;
        }
      });
      if(!correctMaps || req.body.maps.length !== _.uniq(serviceids).length || req.body.maps.length !== _.uniq(alertnames).length)
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ '400051' ];
        return res.negotiate(error);
      }
      else
      {
        Service.find({ serviceid: serviceids })
        .then(function(services)
        {
          if(services.length !== serviceids.length)
          {
            var error = new Error();
            error.code = 400362;
            error.message = errorCode[ '400362' ];
            throw error;
          }
          else
          {
            return Integrationconfig.findOne({ integrationtype: enums.integrationService.LOGGLY });
          }
        })
        .then(function(intconfig)
        {
          intconfig.config.servicetoalertmaps = req.body.maps;
          return Integrationconfig.update({ integrationtype: enums.integrationService.LOGGLY }, { config: intconfig.config });
        })
        .then(function(result)
        {
          return res.json(result);
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
    }
  },
  listintegrations: function(req, res)
  {
    Integrationconfig.find({ select: [ 'integrationtype', 'config' ] })
    .then(function(configs)
    {
      return res.json(configs);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
