/* FileName: QueueController.js
* @description: This file describes the various actions in the Queue controller.
*
* Date          Change Description                            Author
* ---------     ---------------------------                   -------
* 13/12/2015    Initial file creation                         SMandal
* 18/02/2016    Bring the current code base to                bones
                comply with the new philosophy
*
*/

/**
 * @apiGroup Queue
 */

var userUtils = require("../services/utils/user.js");
var cbUtils = require("../services/utils/cbutils.js");
var queueUtils = require("../services/utils/queueutils.js");
var Promise = require("bluebird");
var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");
var _ = require("lodash");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports =
{
/**
  * Name: list
  * @description: This function lists all the Queues in the system
  *
  * @param: None
  * @returns: json Object with all the user details
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    Queue.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400251;
        error.message = errorCode[ "400251" ];
        throw error;
      }
      else
      {
        return res.json(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: add
  * @description: On receipt of the details of the queue to be added, existance and active status of all the supplied attributes are checked.
  * On failure of any attribute, a bad request is sent with 400 code.
  * On success a queue is created, the details of the queue are sent along with a 200 code.
  * @param:
    {
      "queuename":"TestQueue69",
      "cbid": 1,
      "queueemail":"testqueue69@test.com"
    }
  * @returns:
    [
      {
        "approvalid": 6,
        "typeofapproval": 4,
        "entityid": 6,
        "targetentityid": 1,
        "approverid": 1,
        "initiatorid": 1,
        "isapproved": null,
        "isactive": true,
        "createdAt": "2016-05-17T10:14:52.000Z",
        "updatedAt": "2016-05-17T10:14:52.000Z"
      },
      {
        "queueid": 6,
        "queuename": "TestQueue69",
        "cbid": null,
        "queueemail": "testqueue69@test.com"
        "isactive": false,
        "isfrozen": true
      }
    ]
  */
  add: function(req, res)
  {
    // The API is open for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.queuename || !req.body.cbid || !req.body.queueemail)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else if (Object.prototype.toString.call(req.body.queuename) === "[object Array]" ||
             Object.prototype.toString.call(req.body.cbid) === "[object Array]" ||
             Object.prototype.toString.call(req.body.queueemail) === "[object Array]")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var newOwner;
      var initiatorid = req.body.requesterid;
      var result = [];
      var paramsIsActiveCheck = function(cbid, initiatorid)
      {
        var pCbStatus = Cb.find({ cbid: cbid });
        var pGetAdmins = userUtils.getBdAdmins([ initiatorid ]);
        var pResult = Promise.all([ pCbStatus, pGetAdmins ]);
        return pResult;
      };

      paramsIsActiveCheck(req.body.cbid, initiatorid)
      .then(function(statusResult)
      {
        //Check if the chargeback code is valid, active and not frozen
        if(!statusResult[ 0 ][ 0 ])
        {
          var error = new Error();
          error.code = 400204;
          error.message = errorCode[ "400204" ];
          throw error;
        }
        if(!statusResult[ 0 ][ 0 ].isactive)
        {
          var error = new Error();
          error.code = 400203;
          error.message = errorCode[ "400203" ];
          throw error;
        }
        if(statusResult[ 0 ][ 0 ].isfrozen)
        {
          var error = new Error();
          error.code = 400208;
          error.message = errorCode[ "400208" ];
          throw error;
        }
        newOwner = statusResult[ 0 ][ 0 ].cbownerid;
        if(statusResult[ 1 ])
        {
          //Check if all the users provided are valid and active
          for(var i = 0; i < statusResult[ 1 ].length; i++)
          {
            if (!statusResult[ 1 ][ i ])
            {
              var error = new Error();
              error.code = 400104;
              error.message = errorCode[ "400104" ];
              throw error;
            }
            if(!statusResult[ 1 ][ i ].isactive)
            {
              var error = new Error();
              error.code = 400103;
              error.message = errorCode[ "400103" ];
              throw error;
            }
          }
          var administeredbds = [];
          for(var i = 0; i <  statusResult[ 1 ][ 0 ].administeredbds.length; i++)
          {
            administeredbds.push(statusResult[ 1 ][ 0 ].administeredbds[ i ].bdid);
          }
          // The requester must be an admin of the BD
          if(!_.includes(administeredbds, statusResult[ 0 ][ 0 ].bdid))
          {
            var error = new Error();
            error.code = 403501;
            error.message = errorCode[ '403501' ];
            throw error;
          }
          //Create the queue
          return Queue.create( { queuename:req.body.queuename, queueemail: req.body.queueemail } );
        }
      })
      .then(function showQueue(createdQueue)
      {
        //AddedQueue = createdQueue;
        result.push(createdQueue);
        return OrgApproval.create({ typeofapproval: enums.approval.ASSIGN_Q2CB, entityid: createdQueue.queueid, targetentityid: req.body.cbid, approverid: newOwner, initiatorid: initiatorid, isapproved: null });
      })
      .then(function createdApproval(approval)
      {
        result.push(approval);
        return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
      })
      .then(function(value)
      {
        //Return res.json([ approval, addedQueue ]);
        return res.json(result);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  * Name: Update
  * Desctoption: On receipt of a queue id to be updated, it checks the existance and active status of all supplied attributes.
  * On failure of any attribute a 400 status code is sent.
  * On success of all attributes, the queue is updated and the updated queue is sent along with a 200 status code.
  * Queue ID is mandatory and atleast one of the below attributes are mandatory.
  *
  *   - Queue Owner ID
  *   - QueueEmail
  *   - Incident Manager ID
  *   - Change Manager ID
  *   - Service Request Manager ID
  *   - Any or all of the alert emails
  *
  * @param:
      {
        "queueid": 1,
      }
  * @return:
    [
      {
        "queueid": 1,
        "queuename": "TestQueue1",
        "cbid": 3,
        "queueemail": "testqueue1@test.com",
        "isactive": true
      }
    ]
  */
  update: function(req, res)
  {
    // The API is open for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.queueid || !(_.isFinite(req.body.queueid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else if (Object.prototype.toString.call(req.body.queueid) === "[object Array]" ||
             Object.prototype.toString.call(req.body.queueemail) === "[object Array]")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var paramsIsActiveCheck = function(queueid)
    {
      var pQueueStatus = Queue.findOne({ queueid: queueid }).populate('cbid');
      var pGetAdmins = userUtils.getBdAdmins([ req.body.requesterid ]);
      var pResult = Promise.all([ pQueueStatus, pGetAdmins ]);
      return pResult;
    };
    paramsIsActiveCheck(req.body.queueid)
    .then(function(statusResult)
    {
      //Check if the queue is valid
      if(!statusResult[ 0 ])
      {
        var error = new Error();
        error.code = 400254;
        error.message = errorCode[ "400254" ];
        throw error;
      }

      // Check if the Queue is frozen
      if(statusResult[ 0 ].isfrozen)
      {
        var error = new Error();
        error.code = 400264;
        error.message = errorCode[ "400264" ];
        throw error;
      }

      var administeredbds = [];
      for(var i = 0; i <  statusResult[ 1 ][ 0 ].administeredbds.length; i++)
      {
        administeredbds.push(statusResult[ 1 ][ 0 ].administeredbds[ i ].bdid);
      }
      // The requester must be an admin of the BD
      if(!_.includes(administeredbds, statusResult[ 0 ].cbid.bdid))
      {
        var error = new Error();
        error.code = 403501;
        error.message = errorCode[ '403501' ];
        throw error;
      }

      var updateObj = {};
      updateObj.queueid = req.body.queueid;
      if(req.body.queueemail) updateObj.queueemail = req.body.queueemail;

      //Update the queue with the new information
      if(_.size(updateObj) <= 1)
      {
        var error = new Error();
        error.code = 400052;
        error.message = errorCode[ "400052" ];
        throw error;
      }
      else return Queue.update({ queueid: req.body.queueid }, updateObj);
    })
    .then(function showQueue(updatedQueue)
    {
      return res.json(updatedQueue);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  * Name: memberassoc
  * Desctoption: Expects a Queue ID, an array of valid user IDs and the action(association: true, dissociation: false) to perform.
  *
  * @param:
    {
      "queueid": 13,
      "members": [5, 1],
      "associate": true
    }
  * @returns:
    200 OK
    OR
    400 Bad Request
    OR
    500 Server Error
  *
  */
  memberassoc: function(req, res)
  {
    // The API is open for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.queueid || !(_.isFinite(req.body.queueid)) || req.body.queueid.length ||
        typeof(req.body.associate) !== "boolean" || !req.body.members || !req.body.members.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var reqMembers = _.uniq(req.body.members);

      for(i = 0; i < reqMembers.length; i++)
      {
        if(!_.isFinite(reqMembers[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }

      var queueStatus = false;

      var paramsIsActiveCheck = function(queueid, users)
      {
        var pQueueStatus = Queue.findOne({ queueid: queueid }).populate('cbid');
        var pUserStatus = userUtils.isActive(users);
        var pGetAdmins = userUtils.getBdAdmins([ req.body.requesterid ]);
        var pIntegrations = Integrationconfig.find({ select: [ 'config' ] });
        var pResult = Promise.all([ pQueueStatus, pUserStatus, pGetAdmins, pIntegrations ]);
        return pResult;
      };

      var i = 0;
      var rolUpdCandidates = [];
      var newMembers = [];
      var response;
      paramsIsActiveCheck(req.body.queueid, reqMembers)
      .then(function(results)
      {
        var administeredbds = [];
        for(var i = 0; i <  results[ 2 ][ 0 ].administeredbds.length; i++)
        {
          administeredbds.push(results[ 2 ][ 0 ].administeredbds[ i ].bdid);
        }
        //Check if the queue is valid
        if(!results[ 0 ])
        {
          var error = new Error();
          error.code = 400254;
          error.message = errorCode[ "400254" ];
          throw error;
        }

        // Check if the Queue is frozen
        if(results[ 0 ].isfrozen)
        {
          var error = new Error();
          error.code = 400264;
          error.message = errorCode[ "400264" ];
          throw error;
        }

        queueStatus = results[ 0 ].isactive;

        // The requester must be an admin of the BD
        if(!_.includes(administeredbds, results[ 0 ].cbid.bdid))
        {
          var error = new Error();
          error.code = 403501;
          error.message = errorCode[ '403501' ];
          throw error;
        }

        //Check for Integration link
        if(results[ 3 ].length && !req.body.associate)
        {
          var linkedToIntegration = false;
          results[ 3 ].every(function(config)
          {
            if(req.body.members.indexOf(config.config.owneruserid) !== -1 && req.body.queueid === config.config.ownerqueueid)
            {
              linkedToIntegration = true;
              return false;
            }
            else
            {
              return true;
            }
          });
          if(linkedToIntegration)
          {
            var error = new Error();
            error.code = 400114;
            error.message = errorCode[ "400114" ];
            throw error;
          }
        }

        //Check if the members to be associated are valid and active
        for(i = 0; i < results[ 1 ].length; i++)
        {
          if(!results[ 1 ][ i ])
          {
            var error = new Error();
            error.code = 400104;
            error.message = errorCode[ "400104" ];
            throw error;
          }
          if(!results[ 1 ][ i ].isactive && req.body.associate)
          {
            var error = new Error();
            error.code = 400103;
            error.message = errorCode[ "400103" ];
            throw error;
          }
        }
        return Queue.findOne({ queueid: req.body.queueid }).populate("members");
      })
      .then(function(record)
      {
        var MemberExists = function(needles, haystack, exist)
        {
          return _.some(needles, function(needle)
          {
            return ((_.indexOf(haystack, needle) >= 0) === exist);
          });
        };

        var haystack = [];
        if(req.body.associate)
        {
          //If the queue has existing members, check if any of the provided users are already members of the queue
          if(record.members.length)
          {
            for(i = 0; i < record.members.length; i++)
            {
              haystack.push(record.members[ i ].userid);
            }

            if(MemberExists(reqMembers, haystack, true))
            {
              var error = new Error();
              error.code = 400256;
              error.message = errorCode[ "400256" ];
              throw error;
            }
          }
          newMembers = _.union(haystack, reqMembers);
          //RolUpdCandidates = newMembers;    // These members can be granted operator role. Use req.body.associate === Role.operator
          //return rolUpdCandidates;
          return userUtils.getUser(newMembers);
        }
        else
        {
          //If the queue doesn't have any members at all exit with 400.
          if(record.members.length)
          {
            for(i = 0; i < record.members.length; i++)
            {
              haystack.push(record.members[ i ].userid);
            }

            //Check if any of the provided users are not members of the queue.
            if(MemberExists(reqMembers, haystack, false))
            {
              var error = new Error();
              error.code = 400258;
              error.message = errorCode[ "400258" ];
              throw error;
            }
          }
          else
          {
            var error = new Error();
            error.code = 400257;
            error.message = errorCode[ "400257" ];
            throw error;
          }
          newMembers = _.difference(haystack, reqMembers);    // ReqMembers need to be checked for other queue associations and for the ones that don't have any assoc, they can be revoked of operator role
          if(queueStatus)
          {
            var flag = false;
            for(i = 0; i < newMembers.length; i++)
            {
              if(_.result(_.find(record.members, [ "userid", newMembers[ i ] ]), "isactive") === true)
              {
                flag = true;
                break;
              }
            }
            if(!flag)
            {
              var error = new Error();
              error.code = 400259;
              error.message = errorCode[ "400259" ];
              throw error;
            }
          }
          var pOpenIncs = Incident.count().where({ or: [ { ownerqueueid: req.body.queueid, owneruserid: req.body.members }, { assignedqueueid: req.body.queueid, assigneduserid: req.body.members } ], state: { '!': enums.incidentStates.CLOSED } });
          return Promise.all([ userUtils.getUser(reqMembers), pOpenIncs ]);
        }
      })
      .then(function(result)
      {
        if(req.body.associate)
        {
          // Filter out users which require role updation
          for(var i = 0; i < result.length; i++)
          {
            if(result[ i ].queuesenrolled.length)
            {
              _.pull(reqMembers, result[ i ].userid);
            }
          }
          var pQupd = Queue.update({ queueid: req.body.queueid }, { members: newMembers });
          var pRlUpd = Role.update({ userid: reqMembers }, { isoperator: req.body.associate });
          return Promise.all([ pQupd, pRlUpd ]);
        }
        else
        {
          if(result[ 1 ])
          {
            var error = new Error();
            error.code = 400266;
            error.message = errorCode[ '400266' ];
            throw error;
          }
          // Filter out users which require role updation
          for(var i = 0; i < result[ 0 ].length; i++)
          {
            if(result[ 0 ][ i ].queuesenrolled.length > 1)
            {
              _.pull(reqMembers, result[ 0 ][ i ].userid);
            }
          }
          var pQupd = Queue.update({ queueid: req.body.queueid }, { members: newMembers });
          var pRlUpd = Role.update({ userid: reqMembers }, { isoperator: req.body.associate });
          return Promise.all([ pQupd, pRlUpd ]);
        }
      })
      .then(function showQueue(updatedQueue)
      {
        response = updatedQueue[ 0 ];
        return userUtils.getUser(reqMembers);
      })
      .then(function(userDetails)
      {
        var signOutCandidates = [];
        for (var i = 0; i < userDetails.length; i++)
        {
          signOutCandidates.push(userDetails[ i ].emailid);
        }
        var signout = function(emailid)
        {
          return client.multi()
           .hmset(process.env.ACCOUNTID + ":" + emailid, [ "signedout", true ])
           .hdel(process.env.ACCOUNTID + ":" + emailid, [ "reftkn" ])
           .EXPIRE(process.env.ACCOUNTID + ":" + emailid, sails.config.authcfg.accExpiresIn)
           .execAsync();
        };
        var actions = signOutCandidates.map(signout);
        var pResults = Promise.all(actions);
        return pResults;
      })
      .then(function(result)
      {
        return res.ok(response);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  * Name: activation
  * @description: Changes the active status of the Queue. For:
  * Deactivate: Check for open tickets in the queue before deactivating. If open, fail.
  * Activate: Check for active status of cb before activating. If inactive, fail.
  *
  * @param:
  {
    "queueids": [6, 3],
    "action": true
  }
  * @return:
  [
    {
      "queueid": 3,
      "queuename": "TestQueue3",
      "cbid": 2,
      "queueemail": "testqueue3@test.com",
      "isactive": true
    },
    {
      "queueid": 6,
      "queuename": "TestQueue6",
      "cbid": 3,
      "queueemail": "testqueue6@test.com",
      "isactive": true
    }
  ]
  */
  activation: function(req, res)
  {
    // The API is open for bdadmins
    if(!req.body.requesterroles.isbdadmin)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ "403502" ];
      return res.negotiate(error);
    }
    if(!req.body.queueids || req.body.queueids.length === 0 || typeof(req.body.action) !== "boolean")
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      //Remove duplicates
      var reqQueueids = _.uniq(req.body.queueids);

      //Check if all provided queue IDs are finite numbers
      for(i = 0; i < reqQueueids.length; i++)
      {
        if(!_.isFinite(reqQueueids[ i ]))
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }

      var i = 0, j = 0;
      var flag = false;
      var cbArray = [], userArray = [];

      var bdArray = [];
      //Check if all provided queues are valid IDs
      queueUtils.getQueue(reqQueueids)
      .then(function(records)
      {
        for(i = 0; i < records.length; i++)
        {
          if(!records[ i ])
          {
            //All the supplied queueids should be valid
            var error = new Error();
            error.code = 400254;
            error.message = errorCode[ "400254" ];
            throw error;
          }
          if(records[ i ].isfrozen)
          {
            //All the supplied queueids should be valid
            var error = new Error();
            error.code = 400264;
            error.message = errorCode[ "400264" ];
            throw error;
          }
          if(records[ i ].isactive === req.body.action)
          {
            if(req.body.action)
            {
              var error = new Error();
              error.code = 400252;
              error.message = errorCode[ "400252" ];
              throw error;
            }
            else
            {
              var error = new Error();
              error.code = 400253;
              error.message = errorCode[ "400253" ];
              throw error;
            }
          }
          if(req.body.action)
          {
            flag = false;
            for(j = 0; j < records[ i ].members.length; j++)
            {
              if(records[ i ].members[ j ].isactive === true)
              {
                flag = true;
                break;
              }
            }
            if(!flag)
            {
              //There must be atleast one active member
              var error = new Error();
              error.code = 400260;
              error.message = errorCode[ "400260" ];
              throw error;
            }
          }
          else
          {
            // Check if any active services are depending on the queue
            _.forEach(records[ 0 ].servicesmanaged, function(service)
            {
              if(service.servicestateid < enums.serviceStateIds.ARCHIVED)
              {
                var error = new Error();
                error.code = 400267;
                error.message = errorCode[ "400267" ];
                throw error;
              }
            });
          }

          cbArray.push(records[ i ].cbid.cbid);
          bdArray.push(records[ i ].cbid.bdid);
        }
        userArray = _.uniq(userArray);
        cbArray = _.uniq(cbArray);
        var pCbStatus = cbUtils.isActive(cbArray);
        var pUserStatus = userUtils.isActive(userArray);
        var pGetAdmins = userUtils.getBdAdmins([ req.body.requesterid ]);
        var pArray = [ pCbStatus, pUserStatus, pGetAdmins ];
        if(!req.body.action)
        {
          var pIntegrations = Integrationconfig.find({ select: [ 'config' ] });
          pArray.push(pIntegrations);
        }
        return Promise.all(pArray);
      })
      .then(function cbUserStatus(statusRecords)
      {
        if(req.body.action)
        {
          for(i = 0; i < statusRecords[ 0 ].length; i++)
          {
            if(!statusRecords[ 0 ][ i ].isactive)
            {
              var error = new Error();
              error.code = 400261;
              error.message = errorCode[ "400261" ];
              throw error;
            }
          }
          for(i = 0; i < statusRecords[ 1 ].length; i++)
          {
            if(!statusRecords[ 1 ][ i ].isactive)
            {
              var error = new Error();
              error.code = 400262;
              error.message = errorCode[ "400262" ];
              throw error;
            }
          }
          var administeredbds = [];
          for(var i = 0; i <  statusRecords[ 2 ][ 0 ].administeredbds.length; i++)
          {
            administeredbds.push(statusRecords[ 2 ][ 0 ].administeredbds[ i ].bdid);
          }
          // The requester must be an admin of the BD
          bdArray.forEach(function isAdmined(ele)
          {
            if(!_.includes(administeredbds, ele))
            {
              var error = new Error();
              error.code = 403501;
              error.message = errorCode[ '403501' ];
              throw error;
            }
          });
          return statusRecords;
        }
        else
        {
          //// Check if any CIs within APPROVED and RECLAIM state are associated with this queue. This checks if services in approved phase are depending on the queue.
          //for(var i = 0; i < statusRecords[ 3 ].length; i++)
          //{
          //  if(statusRecords[ 3 ][ i ].cistateid > enums.ciStateIds.REGISTRATION && statusRecords[ 3 ][ i ].cistateid < enums.ciStateIds.DECOMMISSIONED)
          //  {
          //    var error = new Error();
          //    error.code = 400267;
          //    error.message = errorCode[ '400267' ];
          //    throw error;
          //  }
          //}

            if(statusRecords[ 3 ].length)
            {
              var linkedToIntegration = false;
              statusRecords[ 3 ].every(function(config)
              {
                if(req.body.queueids.indexOf(config.config.ownerqueueid) !== -1)
                {
                  linkedToIntegration = true;
                  return false;
                }
                else
                {
                  return true;
                }
              });
              if(linkedToIntegration)
              {
                var error = new Error();
                error.code = 400268;
                error.message = errorCode[ "400268" ];
                throw error;
              }
            }
          return Incident.count().where({ or: [ { ownerqueueid: req.body.queueids }, { assignedqueueid: req.body.queueids } ], state: { '!': enums.incidentStates.CLOSED } });
        }
      })
      .then(function(result)
      {
        if(!req.body.action && result)
        {
          var error = new Error();
          error.code = 400265;
          error.message = errorCode[ '400265' ];
          throw error;
        }
        return Queue.update({ queueid: reqQueueids }, { isactive: req.body.action });
      })
      .then(function showQueue(updatedQueue)
      {
        return res.json(updatedQueue);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
/**
  * Name: getdetail
  * @description: Fetches the details of the queueids supplied. Max 10 entries at a time.
  * @param:
      {
        "queueids": [11]
      }
  * @returns:


  */
  getdetail: function(req, res)
  {
    var ids = 0;
    var i = 0;
    var error = new Error();
    if(!req.body.queueids || !req.body.queueids.length || req.body.queueids.length > 10)
    {
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      ids = _.uniq(req.body.queueids);
      for(i = 0; i < ids.length; i++)
      {
        if(!_.isFinite(ids[ i ]))
        {
          error.code = 400051;
          error.message = errorCode[ "400051" ];
          return res.negotiate(error);
        }
      }
    }

    queueUtils.getQueue(ids)
    .then(function(data)
    {
      //Check if the all the supplied queue ids exist in the system
      for(i = 0; i < data.length; i++)
      {
        if(!data[ i ])
        {
          //All the supplied queueids should be valid
          error.code = 400254;
          error.message = errorCode[ "400254" ];
          throw error;
        }
      }
      return res.json(data);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: getstatus
  * @description: gets the status of the Queue ids supplied
  *
  * @param:
    {
      "queueids": [1,2,3]
    }
    @returns

  */
  getstatus: function(req, res)
  {
    var error = new Error();
    if(!req.body.queueids || !req.body.queueids.length)
    {
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var i = 0;
    var reqQueueids = _.uniq(req.body.queueids);
    for(i = 0; i < reqQueueids.length; i++)
    {
      if(!_.isFinite(reqQueueids[ i ]))
      {
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
    }

    function obj(queueid, state, isfrozen)
    {
      this.queueid = queueid;
      this.isActive = state;
      this.isFrozen = isfrozen;
    }

    queueUtils.isActive(reqQueueids)
    .then(function(records)
    {
      var resultObj = [];
      for(i = 0; i < records.length; i++)
      {
        if(records[ i ])
        {
          resultObj.push(new obj(records[ i ].queueid, records[ i ].isactive, records[ i ].isfrozen));
        }
        else
        {
          //All the supplied Queue ids should be valid
          error.code = 400254;
          error.message = errorCode[ "400254" ];
          throw error;
        }
      }
      return res.json(resultObj);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  * Name: search
  * @description: searches the model and returns the result. The number of search column and order by criteria are flexible.
  * @param:
    {
      "search":  { "queuename": "Te%" },
      "result" : [ "queuename", "queueemail", "isactive", "isfrozen"],
      "orderby": "queuename"
    }
  * @returns:
    [
      6,
      [
        {
          "queuename": "TestQueue1",
          "queueemail": "testqueue1@test.com",
          "isactive": true,
          "isfrozen": false
        },
        {
          "queuename": "TestQueue29",
          "queueemail": "testqueue29@test.com",
          "isactive": false,
          "isfrozen": true
        },
        ...
      ]
    ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Queue.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Queue.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
};
