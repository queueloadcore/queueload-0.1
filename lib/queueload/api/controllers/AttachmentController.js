/* FileName: AttachmentController.js
* @description: This file describes the various actions in the attachment controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 14/09/2016    Initial file creation                         SMandal
*
*/

/**
 * @apiGroup Attachment
 */

var tempdir = process.env.TEMPDIR || '/tmp';
var bluebird = require('bluebird');
var AWS = require('aws-sdk');
AWS.config.setPromisesDependency(require('bluebird'));
var fs = require('fs');
var md5 = require('md5');
var s3 = new AWS.S3({ apiVersion: sails.config.awscfg.s3apiversion, region: sails.config.awscfg.s3region, accessKeyId: sails.config.awscfg.accessKeyId, secretAccessKey: sails.config.awscfg.secretAccessKey });
var errorCode = require("../resources/ErrorCodes.js");

module.exports = {
  /**
  @apiGroup Attachment
  @apiName upload
  @api {post} /attachment/upload Upload
  @apiDescription This API uploads a file
  @apiPermission user
  @apiHeader (Content-Type) {Strng} Content-Type multipart/form-data
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {web-form} Example Usage:
     Send a file setting the UI element name attribute as "attachment"
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400401
  @apiSuccess (200) {String} key Key to the file
  @apiSuccess (200) {String} filename Name of the file
  @apiSuccess (200) {String} size Size of the file in bytes
  @apiSuccessExample {json} Success-Response
      {
        attachmentid: 3,
        filename: '37774-1920x1200.jpg',
        size: '3135190'
      }
  */
  upload: function(req, res)
  {
    var s3params = {};
    var readStream = {};
    var uploadObj = {};
    if(req.headers[ 'content-type' ] === undefined)
    {
      sails.log.debug("Didn't receive any file.");
      var error = new Error();
      error.code = 400401;
      error.message = errorCode[ '400401' ];
      return res.negotiate(error);
    }
    if(!req.file('attachment')._files.length)
    {
      var error = new Error();
      error.code = 400401;
      error.message = errorCode[ '400401' ];
      return res.negotiate(error);
    }
    else
    {
      var uploadFile = req.file('attachment');
      bluebird.promisifyAll(uploadFile);
      return uploadFile.uploadAsync({ dirname: tempdir, saveAs: uploadFile._files[ 0 ].stream.filename, maxBytes: 5242880 })
      .then(function onUploadComplete(localFiles)
      {
        uploadObj.filename = localFiles[ 0 ].filename;
        uploadObj.size = localFiles[ 0 ].size;
        uploadObj.key = md5(localFiles[ 0 ].fd);
        readStream = fs.createReadStream(localFiles[ 0 ].fd);
        s3params = {
            Bucket: sails.config.awscfg.attachmentbucket,
            Key: uploadObj.key,
            Body: readStream
        };
        return Attachment.findOne({ key: uploadObj.key });
      })
      .then(function(result)
      {
        if(!result)
        {
          var putObjectPromise = s3.putObject(s3params).promise();
          return putObjectPromise;
        }
        else
        {
          return result;
        }
      })
      .then(function(data) {
        if(data.createdAt)
        {
          return data;
        }
        else
        {
          uploadObj.etag = data.ETag;
          return Attachment.create(uploadObj);
        }
      })
      .then(function(data)
      {
        fs.unlink(tempdir + '/' + uploadObj.filename);
        return res.json(data);
      })
      .catch(function(err)
      {
        if(err.code === 'E_EXCEEDS_UPLOAD_LIMIT')
        {
          var error = new Error();
          error.code = 400403;
          error.message = errorCode[ '400403' ];
          return res.negotiate(error);
        }
        else
        {
          return res.negotiate(err);
        }
      });
    }
  },
  /**
  @apiGroup Attachment
  @apiName download
  @api {post} /attachment/download Download
  @apiDescription This API downloads a file
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImF
     kbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiw
     icGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0Ijo
     xNDc0NjA5NTE2LCJleHAiOjE0NzQ2MzExMTYsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5
     jb20ifQ.L5VK-C4lLqiEjl-opeKW_mJ71sIxFWiSTKnlb-5mHUc" -d
     '{
        "attachmentid" : 3
      }' "http://localhost:1337/attachment/download"
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400402
  @apiSuccess (200) {String} filename Name of the file
  @apiSuccess (200) {String} url Download link of the file
  @apiSuccessExample {json} Success-Response
      {
        "filename": "37774-1920x1200.jpg",
        "url": "https://qlticketattachments-test.s3.ap-south-1.amazonaws.com/30ece804a870b66900fbb53bafefe5a7
        ?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIK5FY66CJ7TFKHBA%2F20160923%2Fap-south-1%2Fs3
        %2Faws4_request&X-Amz-Date=20160923T055120Z&X-Amz-Expires=10&X-Amz-Signature=a7544c36d746c5144130bf30
        e7a6d6265bc509a3f4d786d45ddbb4dc3eb08a7b&X-Amz-SignedHeaders=host"
      }
  */
  download: function(req, res)
  {
    if(!req.body.attachmentid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var filename;
    return Attachment.findOne({ attachmentid: req.body.attachmentid })
    .then(function(result)
    {
      if(!result)
      {
        var error = new Error();
        error.code = 400402;
        error.message = errorCode[ '400402' ];
        throw error;
      }
      else
      {
        filename = result.filename;
        var s3params =
        {
          Bucket: sails.config.awscfg.attachmentbucket,
          Key: result.key,
          Expires: 10
        };
        var getSignedUrlPromise = s3.getSignedUrl('getObject', s3params);
        return getSignedUrlPromise;
      }
    })
    .then(function(url)
    {
      return res.json({ "filename": filename, "url": url });
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
