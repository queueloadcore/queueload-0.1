/* FileName: CidetailController.js
*  @description: Server-side logic for managing cidetails.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  23/03/2016        Initial file creation                         bones
*  07/04/2016        API Drafting                                  SMandal
*/

/**
 * @apiGroup CI
 */

var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");
var Promise = require("bluebird");
var _ = require("lodash");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var AWS = require('aws-sdk');
AWS.config.setPromisesDependency(require('bluebird'));
var sts = new AWS.STS({ accessKeyId: "AKIAIWY5I4Q7H5N5NWEQ", secretAccessKey: "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" }); // Belongs to AWS account# 262517602169
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports = {
  /**
  @apiGroup CI
  @apiName listCis
  @api {post} /cidetail/list List
  @apiDescription This API lists configuration items in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ey
     J1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iL
     CJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5
     OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjUzMjQ3MywiZXhwIjoxNDcyNTU0MDc
     zLCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.-S8nsNHDf2OlWzdCbYz96ambz1HsslwhiU
     NmcV73bHY" -d '{"page" : 1, "limit" : 100}' "http://localhost:1337//list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400381
  @apiSuccess (200) {Object[]} ci Array of CIs
  @apiSuccessExample {json} Success-Response
    [
      {
        "cistateid": 6,
        "citypeid": 1,
        "envid": 1,
        "vendorid": 1,
        "machineid": null,
        "cinetworkid": null,
        "ciownerid": 1,
        "locationid": 1,
        "ciid": 1,
        "ciname": "HD63J-34JD3",
        "assettag": "UJF73-DJRI",
        "contractid": null,
        "warrantyexpiry": null,
        "maintenanceend": null,
        "customerfacing": false,
        "isundermaint": true,
        "systemsoftware1": "Splunk",
        "systemsoftware2": null,
        "systemsoftware3": null,
        "customapp1": null,
        "customapp2": null,
        "customapp3": null,
        "installationdate": 1480516228,
        "isfrozen": false,
        "freezecause": "0"
      }
    ]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    Cidetail.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400381;
        error.message = errorCode[ "400381" ];
        throw error;
      }
      else
      {
        return res.json(found);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup CI
  @apiName getdetail
  @api {post} /cidetail/getdetail Getdetail
  @apiDescription This API fetches the details of a configuration item
  @apiPermission user
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImF
     kbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiw
     icGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0Ijo
     xNDcyODI2ODAzLCJleHAiOjE0NzI4NDg0MDMsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5
     jb20ifQ.7kaen692bFKfxERnBZgXxm0l0GUbJ0biz6xY7Fime9s" -d '{"ciid" :1}' "http://localhost:1337/cidetail/getdetail"
  @apiParam {Number} ciid ID of the CI
  @apiParamExample {json} Request-Example
     {"ciid" :1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400382
  @apiSuccess (200) {Object[]} ci Array of CIs
  @apiSuccessExample {json} Success-Response
    {
      "citypeid": {
        "citypeid": 1,
        "citype": "network switches",
        "cikey": 6
      },
      "envid": {
        "envid": 1,
        "envname": "development"
      },
      "vendorid": {
        "vendorid": 1,
        "vendorname": "Cyberdyne Systems"
      },
      "machineid": null,
      "cinetworkid": null,
      "ciownerid": {
        "userid": 1,
        "firstname": "Super",
        "lastname": "User",
        "username": "administrator",
        "emailid": "administrator@testdomain.com",
        "countrycode": "+91",
        "phonenumber": "9999999999",
        "employeeid": "FTE000001",
        "isactive": true
      },
      "locationid": {
        "locationid": 1,
        "locationname": "Rome-9W",
        "locationcode": "IT-9W-F0L",
        "buildingname": "Pani house",
        "sitecategory": "Live Site"
      },
      "ciid": 1,
      "ciname": "HD63J-34JD3",
      "assettag": "UJF73-DJRI",
      "contractid": null,
      "warrantyexpiry": null,
      "maintenanceend": null,
      "customerfacing": false,
      "isundermaint": true,
      "systemsoftware1": "Splunk",
      "systemsoftware2": null,
      "systemsoftware3": null,
      "customapp1": null,
      "customapp2": null,
      "customapp3": null,
      "installationdate": 1480516228,
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  getdetail: function(req, res)
  {
    if(!req.body.ciid || Array.isArray(req.body.ciid) || !(_.isFinite(req.body.ciid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Cidetail.findOne({ ciid: req.body.ciid })
      .populate("citypeid")
      .populate("envid")
      .populate("vendorid")
      .populate("locationid")
      .populate("serviceids")
      .populate("machineid")
      .populate("primaryservice")
      .populate("incidents", { state: { '!': [ enums.incidentStates.CLOSED ] } })
      .then(function showRecs(record)
      {
        // Check if the Ciid exists in the system
        if(!record)
        {
          var error = new Error();
          error.code = 400382;
          error.message = errorCode[ "400382" ];
          throw error;
        }
        else
        {
          return res.json(record);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup CI
  @apiName addCI
  @api {post} /cidetail/add Add
  @apiDescription On input of all valid parameters this function will add a CI
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGVzdC5
     1c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGx
     veWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3NTU1ODMyMCwiZXhwIjoxNDc1NTc5OTIwLCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.KGRM4EMjlM1vTh3jmm8ZdFE4cFRsO1a71zm
     LNY4q9ac" -H "Content-Type: application/json" -d
     '{
          "ciname": "HJ4K2-74JK",
          "assettag": "JFU5-73JD",
          "citypeid": 1,
          "envid": 1,
          "vendorid": 1,
          "ciownerid": 2,
          "locationid": 1,
          "installationdate": 1480516228
      }' "http://localhost:1337/cidetail/add"
  @apiParam {String} ciname Name of the CI
  @apiParam {String} assettag Unique tag assigned to the asset
  @apiParam {Number} citypeid Ci Type ID
  @apiParam {Number} envid Env Type ID
  @apiParam {Date} installationdate Time in epoch format
  @apiParam {Number} vendorid Vendor ID
  @apiParam {Number} ciownerid User ID of the CI Owner
  @apiParam {Number} locationid ID of the location
  @apiParam {Date} [warrantyexpiry] Time in epoch format
  @apiParam {Date} [maintenanceend] Time in epoch format
  @apiParam {Boolean} [customerfacing] If customer facing-Yes, else-No. default: No
  @apiParam {Boolean} [isundermaint] If CI under maintenance
  @apiParam {Number} [machineid] ID of the hardware
  @apiParam {Number} [cinetworkid] ID of the network
  @apiParam {String} [contractid] ID of the maintenance contract
  @apiParam {String} [systemsoftware1] Name of Installed System Software 1
  @apiParam {String} [systemsoftware2] Name of Installed System Software 2
  @apiParam {String} [systemsoftware3] Name of Installed System Software 3
  @apiParam {String} [customapp1] Name of the custom application 1
  @apiParam {String} [customapp2] Name of the custom application 2
  @apiParam {String} [customapp3] Name of the custom application 3
  @apiParamExample {json} Request-Example
    {
      "ciname": "HJ4K2-74JK",
      "assettag": "JFU5-73JD",
      "citypeid": 1,
      "envid": 1,
      "vendorid": 1,
      "ciownerid": 2,
      "locationid": 1,
      "installationdate": 1480516283
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400104
  @apiUse 400103
  @apiUse 400303
  @apiUse 400313
  @apiUse 400322
  @apiUse 400332
  @apiUse 400342
  @apiUse 400352
  @apiUse 403502
  @apiSuccess (200) {Object} ci Added CI
  @apiSuccessExample {json} Success-Response
    {
      "ciid": 4,
      "ciname": "HJ4K2-74JK",
      "assettag": "JFU5-73JD",
      "cistateid": 1,
      "citypeid": 1,
      "envid": 1,
      "contractid": null,
      "vendorid": 1,
      "warrantyexpiry": null,
      "maintenanceend": null,
      "customerfacing": false,
      "isundermaint": true,
      "machineid": null,
      "cinetworkid": null,
      "systemsoftware1": null,
      "systemsoftware2": null,
      "systemsoftware3": null,
      "customapp1": null,
      "customapp2": null,
      "customapp3": null,
      "ciownerid": 2,
      "locationid": 1,
      "installationdate": 1480516283,
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.ciname || !req.body.assettag || req.body.cistateid || !req.body.citypeid ||
      req.body.serviceids || !req.body.envid || !req.body.vendorid ||
      !req.body.locationid || req.body.isfrozen)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    if((req.body.customerfacing !== undefined) && (typeof(req.body.customerfacing) !== "boolean") ||
      (req.body.isundermaint !== undefined) && (typeof(req.body.isundermaint) !== "boolean"))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var promiseArray = [];
    promiseArray.push(Citype.findOne({ citypeid: req.body.citypeid }));
    promiseArray.push(Cienvironment.findOne({ envid: req.body.envid }));
    promiseArray.push(Vendor.findOne({ vendorid: req.body.vendorid }));
    promiseArray.push(Cilocation.findOne({ locationid: req.body.locationid }));
    if(req.body.machineid)
    {
      promiseArray.push(Cihardware.findOne({ machineid: req.body.machineid }));
    }
    if(req.body.cinetworkid)
    {
      promiseArray.push(Cinetwork.findOne({ cinetworkid: req.body.cinetworkid }));
    }

    var result;
    var seek = 0;
    Promise.all(promiseArray)
    .then(function(statusResults)
    {
      // Check if the citype is valid
      if(!statusResults[ seek ])
      {
        var error = new Error();
        error.code = 400303;
        error.message = errorCode[ "400303" ];
        throw error;
      }
      seek++;

      // Check if the environment is valid
      if(!statusResults[ seek ])
      {
        var error = new Error();
        error.code = 400313;
        error.message = errorCode[ "400313" ];
        throw error;
      }
      seek++;

      // Check if the vendor is valid
      if(!statusResults[ seek ])
      {
        var error = new Error();
        error.code = 400332;
        error.message = errorCode[ "400332" ];
        throw error;
      }
      seek++;

      // Check if the CIlocation is valid
      if(!statusResults[ seek ])
      {
        var error = new Error();
        error.code = 400322;
        error.message = errorCode[ "400322" ];
        throw error;
      }
      seek++;

      if(req.body.machineid)
      {
        // Check if the machineid is valid
        if(!statusResults[ seek ])
        {
          var error = new Error();
          error.code = 400352;
          error.message = errorCode[ "400352" ];
          throw error;
        }
        else
        {
          seek++;
        }
      }

      if(req.body.cinetworkid)
      {
        // Check if the networkid is valid
        if(!statusResults[ seek ])
        {
          var error = new Error();
          error.code = 400342;
          error.message = errorCode[ "400342" ];
          throw error;
        }
        else
        {
          seek++;
        }
      }
      return statusResults;
    })
    .then(function(statusResults)
    {
      return Cidetail.create(req.body);
    })
    .then(function(ci)
    {
      result = ci;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(result);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup CI
  @apiName update
  @api {post} /cidetail/update Update
  @apiDescription This function updates a CI in the system
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Mjc
     wNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0
     .awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM" -d
     '{
        "ciid" : 1,
        "systemsoftware1" : "Splunk"
      }' "http://localhost:1337/cidetail/update"
  @apiParam {Number} ciid ID of the CI
  @apiParam {Number} [citypeid] CI Type ID
  @apiParam {Number} [envid] Env Type ID
  @apiParam {Date} [installationdate] Time in epoch format
  @apiParam {Number} [vendorid] Vendor ID
  @apiParam {Number} [ciownerid] User ID of the CI Owner
  @apiParam {Number} [locationid] ID of the location
  @apiParam {Date} [warrantyexpiry] Time in epoch format
  @apiParam {Date} [maintenanceend] Time in epoch format
  @apiParam {Boolean} [customerfacing] If customer facing-Yes, else-No. default: No
  @apiParam {Boolean} [isundermaint] If CI under maintenance
  @apiParam {Number} [machineid] ID of the hardware
  @apiParam {Number} [cinetworkid] ID of the network
  @apiParam {String} [contractid] ID of the maintenance contract
  @apiParam {String} [systemsoftware1] Name of Installed System Software 1
  @apiParam {String} [systemsoftware2] Name of Installed System Software 2
  @apiParam {String} [systemsoftware3] Name of Installed System Software 3
  @apiParam {String} [customapp1] Name of the custom application 1
  @apiParam {String} [customapp2] Name of the custom application 2
  @apiParam {String} [customapp3] Name of the custom application 3
  @apiParamExample {json} Request-Example
    {
      "ciid" : 1,
      "systemsoftware1" : "Splunk"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400382
  @apiUse 400383
  @apiUse 403502
  @apiSuccess (200) {Object} ci Details of the updated CI
  @apiSuccessExample {json} Success-Response
  {
    "ciid": 1,
    "ciname": "HD63J-34JD3",
    "assettag": "UJF73-DJRI",
    "cistateid": 1,
    "citypeid": 1,
    "envid": 1,
    "contractid": null,
    "vendorid": 1,
    "warrantyexpiry": null,
    "maintenanceend": null,
    "customerfacing": false,
    "isundermaint": true,
    "machineid": null,
    "cinetworkid": null,
    "systemsoftware1": "Splunk",
    "systemsoftware2": null,
    "systemsoftware3": null,
    "customapp1": null,
    "customapp2": null,
    "customapp3": null,
    "ciownerid": 1,
    "locationid": 1,
    "installationdate": 1480516283,
    "isfrozen": false,
    "freezecause": "0"
  }
  */
  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.ciid || !(_.isFinite(req.body.ciid)) || req.body.ciname ||
      req.body.assettag || req.body.cistateid ||
      req.body.isfrozen !== undefined || req.body.serviceids || req.body.approvalids)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Cidetail.findOne({ ciid: req.body.ciid })
      .then(function showRec(record)
      {
        if(!record)
        {
          var error = new Error();
          error.code = 400382;
          error.message = errorCode[ "400382" ];
          throw error;
        }
        else if(record.isfrozen)
        {
          var error = new Error();
          error.code = 400383;
          error.message = errorCode[ "400383" ];
          throw error;
        }
        else if(record.state > enums.ciStateIds.RECLAIM)
        {
          var error = new Error();
          error.code = 400387;
          error.message = errorCode[ '400387' ];
          throw error;
        }
        else
        {
          return Cidetail.update({ ciid: req.body.ciid }, req.body);
        }
      })
      .then(function(ci)
      {
        return res.json(ci [ 0 ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup CI
  @apiName next
  @api {post} /cidetail/next Next
  @apiDescription Initiates state transition of a Configuration Item
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Mjc
     yNDk3NiwiZXhwIjoxNDcyNzQ2NTc2LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0
     .asVcWWy3NlnF130JNOw3rA5epEBBcfIik0TPJ-6Xu0A" -d
     '{
        "ciid" : 1
      }' "http://localhost:1337/cidetail/next"
  @apiParam {Number} ciid ID of the CI
  @apiParamExample {json} Request-Example
    {
      "ciid" : 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400382
  @apiUse 400383
  @apiUse 400384
  @apiUse 400385
  @apiUse 400386
  @apiUse 400388
  @apiUse 403502
  @apiSuccess (200) {Object} ci Details of the CI
  @apiSuccessExample {json} Success-Response
  {
    "ciid": 1,
    "ciname": "HD63J-34JD3",
    "assettag": "UJF73-DJRI",
    "cistateid": 2,
    "citypeid": 1,
    "envid": 1,
    "contractid": null,
    "vendorid": 1,
    "warrantyexpiry": null,
    "maintenanceend": null,
    "customerfacing": false,
    "isundermaint": true,
    "machineid": null,
    "cinetworkid": null,
    "systemsoftware1": "Splunk",
    "systemsoftware2": null,
    "systemsoftware3": null,
    "customapp1": null,
    "customapp2": null,
    "customapp3": null,
    "ciownerid": 1,
    "locationid": 1,
    "installationdate": 1480516283,
    "isfrozen": false,
    "freezecause": "0"
  }
  */
  next: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.ciid || !(_.isFinite(req.body.ciid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      throw error;
    }
    else
    {
      var pCiDetails = Cidetail.findOne({ ciid: req.body.ciid }).populate("serviceids");
      var pOpenIncs = Incident.count().where({ ciid: req.body.ciid, state: { '!': enums.incidentStates.CLOSED } });

      Promise.all([ pCiDetails, pOpenIncs ])
      .then(function(result)
      {
        // If the CI doesn't exist in the system
        if(!result[ 0 ])
        {
          var error = new Error();
          error.code = 400382;
          error.message = errorCode[ "400382" ];
          throw error;
        }

        // If the CI is frozen
        else if(result[ 0 ].isfrozen)
        {
          var error = new Error();
          error.code = 400383;
          error.message = errorCode[ "400383" ];
          throw error;
        }

        // If the CI is in Decommissioned state
        else if(result[ 0 ].cistateid > enums.ciStateIds.RECLAIM)
        {
          var error = new Error();
          error.code = 400384;
          error.message = errorCode[ "400384" ];
          throw error;
        }

        // If the CI is not associated with any services
        else if(result[ 0 ].cistateid < enums.ciStateIds.RECLAIM && !result[ 0 ].serviceids.length)
        {
          var error = new Error();
          error.code = 400386;
          error.message = errorCode[ "400386" ];
          throw error;
        }

        // If the CI is in Commissioned State and has open incidents
        else if(result[ 0 ].cistateid === enums.ciStateIds.COMMISSIONED && result[ 1 ])
        {
          var error = new Error();
          error.code = 400390;
          error.message = errorCode[ '400390' ];
          throw error;
        }

        // If the CI is in Reclaim state and associated with services
        else if(result[ 0 ].cistateid === enums.ciStateIds.RECLAIM && result[ 0 ].serviceids.length)
        {
          var error = new Error();
          error.code = 400385;
          error.message = errorCode[ "400385" ];
          throw error;
        }

        // If the CI is in Installed state and there are no support queues associated
        //else if(result[ 0 ].cistateid === enums.ciStateIds.INSTALLED && !result[ 0 ].supportqueue)
        //{
        //  var error = new Error();
        //  error.code = 400388;
        //  error.message = errorCode[ "400388" ];
        //  throw error;
        //}
        else
        {
          var newState = result[ 0 ].cistateid + 1;
          if(newState === enums.ciStateIds.COMMISSIONED)
          {
            return Cidetail.update({ ciid: req.body.ciid }, { cistateid: newState, isundermaint: false });
          }
          else
          {
            return Cidetail.update({ ciid: req.body.ciid }, { cistateid: newState, isundermaint: true });
          }        
        }
      })
      .then(function(result)
      {
        return res.json(result[ 0 ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup CI
  @apiName cancel
  @api {post} /cidetail/cancel Cancel
  @apiDescription Cancels a CI
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage
      curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp
      XVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I
      m1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv
      bmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ
      3MjcyNDk3NiwiZXhwIjoxNDcyNzQ2NTc2LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2
      9tIn0.asVcWWy3NlnF130JNOw3rA5epEBBcfIik0TPJ-6Xu0A" d
      '{
        "ciid" : 1
      }' "http://localhost:1337/cidetail/cancel"
  @apiParam {Number} ciid ID of the CI
  @apiParamExample {json} Request-Example
    {
      "ciid" : 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400382
  @apiUse 400383
  @apiUse 400387
  @apiUse 403502
  @apiSuccess (200) {Object} service Details of the cancelled service
  @apiSuccessExample {json} Success-Response
    {
      "ciid": 1,
      "ciname": "HD63J-34JD3",
      "assettag": "UJF73-DJRI",
      "cistateid": 6,
      "citypeid": 1,
      "envid": 1,
      "contractid": null,
      "vendorid": 1,
      "warrantyexpiry": null,
      "maintenanceend": null,
      "customerfacing": false,
      "isundermaint": true,
      "machineid": null,
      "cinetworkid": null,
      "systemsoftware1": "Splunk",
      "systemsoftware2": null,
      "systemsoftware3": null,
      "customapp1": null,
      "customapp2": null,
      "customapp3": null,
      "ciownerid": 1,
      "locationid": 1,
      "installationdate": 1480516283,
      "isfrozen": false,
      "freezecause": "0"
    }
  */
  cancel: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.ciid || !(_.isFinite(req.body.ciid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Cidetail.findOne({ ciid: req.body.ciid })
      .then(function(result)
      {
        // Check if the CIid in the input exists
        if(!result)
        {
          var error = new Error();
          error.code = 400382;
          error.message = errorCode[ "400382" ];
          throw error;
        }

        // If the CI is frozen
        if(result.isfrozen)
        {
          var error = new Error();
          error.code = 400383;
          error.message = errorCode[ "400383" ];
          throw error;
        }

        // Check if the Cistate is higher then Installed
        else if(result.cistateid > enums.ciStateIds.INSTALLED)
        {
          var error = new Error();
          error.code = 400387;
          error.message = errorCode[ "400387" ];
          throw error;
        }
        else
        {
          // Move the Configuration Item to Reclaim state
          return Cidetail.update({ ciid: req.body.ciid }, { cistateid: enums.ciStateIds.RECLAIM });
        }
      })
      .then(function(result)
      {
        return res.json(result[ 0 ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup CI
  @apiName search
  @api {post} /cidetail/search Search
  @apiDescription Searches the model and returns the result. The number of search column and order by criteria are flexible.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjcyNDk3NiwiZXhwIjoxNDcyNzQ2NTc2LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.asVcWWy3NlnF130JNOw3rA5epEBBcfIik0T
     PJ-6Xu0A" -H "Content-Type: application/json" -d
     '{
        "search":  { "ciname": "HD63J%" },
        "result" : [ "ciname", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate"],
        "orderby": "assettag"
      }' "http://localhost:1337/cidetail/search"
  @apiParam {Object} search Object listing the attrbutes of the searched CI
  @apiParam {String[]} result Array of attributes requested
  @apiParam {String} orderby Attribute to order by the results
  @apiParamExample {json} Request-Example
    {
      "search":  { "ciname": "HD63J%" },
      "result" : [ "ciname", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate"],
      "orderby": "assettag"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400053
  @apiSuccess (200) {Array} result First element-Number of records, Second element-Array of results
  @apiSuccessExample {json} Success-Response
  [
    1,
    [
      {
        "cistateid": 6,
        "citypeid": 1,
        "envid": 1,
        "ciownerid": 1,
        "locationid": 1,
        "ciname": "HD63J-34JD3",
        "assettag": "UJF73-DJRI",
        "installationdate": 1480516283
      }
    ]
  ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Cidetail.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Cidetail.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  },
  /**
  @apiGroup CI
  @apiName makeserviceprimary
  @api {post} /cidetail/makeserviceprimary Makeserviceprimary
  @apiDescription Makes the supplied service ID, the primary service of the requested configuration item
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6
     bnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjI3LCJmdWxsbmFtZSI6ImluY2ZpcnN0IGluY09wZXJhdG9yIiwiZ
     mlyc3RuYW1lIjoiaW5jZmlyc3QiLCJsYXN0bmFtZSI6ImluY09wZXJhdG9yIiwidXNlcm5hbWUiOiJpbmNmaXJzdC5vcGVyYXRvci
     IsImVtYWlsaWQiOiJpbmNmaXJzdC5vcGVyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjpudWxsLCJwaG9uZW51bWJ
     lciI6bnVsbCwiZW1wbG95ZWVpZCI6IklOQ0ZURTAwMDAwOSIsImlzYWN0aXZlIjp0cnVlLCJwYXNzZXhwaXJlZCI6ZmFsc2UsInVz
     ZXJzZXR0aW5ncyI6bnVsbH0sImlhdCI6MTQ4OTQwNzQ3OCwiZXhwIjoxNDg5NDI5MDc4LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvb
     SIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.TPycl3k6BQ-ZiXNr2lpT78nWBmX9k4WzfGIpRktYQfs"
     -H "Content-Type: application/json" -d
       '{
          "ciid" : 4,
          "serviceid" : 4
        }' "http://localhost:1337/cidetail/makeserviceprimary"
  @apiParam {Number} ciid ID of the CI
  @apiParam {Number} serviceid ID of the Service
  @apiParamExample {json} Request-Example
    {
      "ciid" : 4,
      "serviceid" : 4
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 403502
  @apiUse 400051
  @apiUse 400389
  @apiSuccess (200) {Object} cidetil Details of the CI
  @apiSuccessExample {json} Success-Response
    {
      "ciid": 4,
      "ciname": "HJ4K2-74JK",
      "assettag": "JFU5-73JD",
      "cistateid": 1,
      "citypeid": 9,
      "envid": 8,
      "contractid": null,
      "vendorid": 9,
      "warrantyexpiry": null,
      "maintenanceend": null,
      "customerfacing": false,
      "isundermaint": true,
      "machineid": null,
      "systemsoftware1": null,
      "systemsoftware2": null,
      "systemsoftware3": null,
      "customapp1": null,
      "customapp2": null,
      "customapp3": null,
      "ciownerid": 33,
      "locationid": 5,
      "installationdate": 1480515761,
      "isfrozen": false,
      "freezecause": 0,
      "ipaddress": null,
      "primaryservice": 4
    }
  */
  makeserviceprimary: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    else if(!req.body.ciid || !req.body.serviceid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Cidetail.findOne({ ciid: req.body.ciid }).populate('serviceids')
      .then(function(result)
      {
        if(!_.includes(_.map(result.serviceids, 'serviceid'), req.body.serviceid))
        {
          var error = new Error();
          error.code = 400389;
          error.message = errorCode [ '400389' ];
          throw error;
        }
        else
        {
          return Cidetail.update({ ciid: req.body.ciid }, { primaryservice: req.body.serviceid });
        }
      })
      .then(function(cidtails)
      {
        return res.ok(cidtails[ 0 ]);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  }
};
