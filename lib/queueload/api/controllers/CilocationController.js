 /* FileName: CilocationController.js
 *  @description: Server-side logic for managing the different CI locations.
 *
 *    Date            Change Description                            Author
 *  ---------     ---------------------------                     ----------
 *  23/03/2016        Initial file creation                         bones
 *
 */

/**
 * @apiGroup Location
 */

 var errorCode = require("../resources/ErrorCodes.js");
 var _ = require("lodash");
 var Promise = require("bluebird");

module.exports =
{
  /**
  @apiGroup Location
  @apiName listLocations
  @api {post} /cilocation/list List
  @apiDescription This API lists the locations in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml
     yc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R
     1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l
     lZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNjI0NzAzLCJleHAiOjE0NzI2NDYzMDMsImF1ZCI
     6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.2QdRMod08w12s83GjbLduW5RRyCi2npgbhOt
     jj2_Lk8" -H "Content-Type: application/json" -d '{"page": 1,"limit": 10}' "http://localhost:1337/cilocation/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400321
  @apiSuccess (200) {Object[]} locations Array of Locations
  @apiSuccessExample {json} Success-Response
    [
      {
        "locationid": 1,
        "locationname": "Rome-9W",
        "locationcode": "IT-9W-F0L",
        "buildingname": "EU AD",
        "sitecategory": "Live Site",
        "numberofcis": 4
      }
    ]
  */

  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    var output = [];
    Cilocation.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400321;
        error.message = errorCode[ "400321" ];
        throw error;
      }
      else
      {
        output = found;
        var countCis = function(element)
        {
          return Cidetail.count().where({ locationid: element.locationid });
        };
        var actions = found.map(countCis);
        return Promise.all(actions);
      }
    })
    .then(function(result)
    {
      var assign = function(element, index)
      {
        output[ index ].numberofcis = element;
      };
      result.forEach(assign);
      return res.ok(output);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
/**
  @api {post} /cilocation/add Add
  @apiName addLocation
  @apiGroup Location
  @apiDescription On input of all valid parameters this function will add locations to the system
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ
     3fqnXwhQ" -H "Content-Type: application/json" -d
      '{
        "locationname": "Rome-9W",
        "locationcode": "IT-9W-F0L",
        "buildingname": "EU AD",
        "sitecategory": "Live Site"
      }' "http://localhost:1337/cilocation/add"
  @apiParam {String} locationname Name of the location
  @apiParam {String} locationcode Code of the location
  @apiParam {String} buildingname Name of the building
  @apiParam {String} sitecategory Category of the site
  @apiParamExample {json} Request-Example
     {
        "locationname": "Rome-9W",
        "locationcode": "IT-9W-F0L",
        "buildingname": "EU AD",
        "sitecategory": "Live Site"
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 403502
  @apiSuccess (200) {Object} location Details of the added location
  @apiSuccessExample {json} Success-Response
    {
      "locationid": 1,
      "locationname": "Rome-9W",
      "locationcode": "IT-9W-F0L",
      "buildingname": "EU AD",
      "sitecategory": "Live Site"
    }
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.locationname || !req.body.locationcode)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

      //Only one location may be added at a time
    else if
    (Array.isArray(req.body.locationname) || Array.isArray(req.body.locationcode) ||
      Array.isArray(req.body.buildingname) || Array.isArray(req.body.sitecategory))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      //Create the location
      Cilocation.create(req.body)
      .then(function createLocation(created)
      {
        return res.json(created);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },

/**
  @api {post} /cilocation/update Update
  @apiName updateLocation
  @apiGroup Location
  @apiDescription On input of all valid parameters this function will update the location
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ
     3fqnXwhQ" -H "Content-Type: application/json" -d
      '{
        "locationid": 1,
        "buildingname": "Pani house"
      }' "http://localhost:1337/cilocation/add"
  @apiParam {String} locationid ID of the location
  @apiParam {String} [locationname] Name of the location
  @apiParam {String} [buildingname] Name of the building
  @apiParam {String} [sitecategory] Category of the site
  @apiParamExample {json} Request-Example
     {
        "locationid": 1,
        "buildingname": "Pani house"
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400322
  @apiUse 403502
  @apiSuccess (200) {Object} location Details of the added location
  @apiSuccessExample {json} Success-Response
    [{
      "locationid": 1,
      "locationname": "Rome-9W",
      "locationcode": "IT-9W-F0L",
      "buildingname": "Pani house",
      "sitecategory": "Live Site"
    }]
  */

  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    // Location code cannot be updated
    if((!req.body.locationid  || !(_.isFinite(req.body.locationid))) || req.body.locationcode)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    //Only one location may be updated at a time
    else if (Array.isArray(req.body.locationid) || Array.isArray(req.body.locationname) ||
     Array.isArray(req.body.buildingname) || Array.isArray(req.body.sitecategory))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    Cilocation.find().where({ locationid:req.body.locationid })
    .then(function showRecs(records)
    {
      if(!records.length)
      {
        var error = new Error();
        error.code = 400322;
        error.message = errorCode[ "400322" ];
        throw error;
      }
      else
      {
        return Cilocation.update({ locationid: req.body.locationid }, req.body);
      }
    })
    .then(function updated(update)
    {
      return res.json(update);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**

  @api {post} /cilocation/search Search
  @apiName searchLocation
  @apiGroup Location
  @apiDescription searches the model and returns the result. The number of search column and order by criteria are flexible.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml
     yc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R
     1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l
     lZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNjI0NzAzLCJleHAiOjE0NzI2NDYzMDMsImF1ZCI
     6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.2QdRMod08w12s83GjbLduW5RRyCi2npgbhOt
     jj2_Lk8" -H "Content-Type: application/json" -d
     '{
        "search":  { "sitecategory": "Live%" },
        "result" : [ "locationname", "locationcode", "buildingname", "sitecategory"],
        "orderby": "locationcode"
      }' "http://localhost:1337/cilocation/search"
  @apiParam {Object} search Object listing the attrbutes of the searched asset
  @apiParam {String[]} result Array of attributes requested
  @apiParam {String} orderby Attribute to order by the results
  @apiParamExample {json} Request-Example
     {
        "search":  { "sitecategory": "Live%" },
        "result" : [ "locationname", "locationcode", "buildingname", "sitecategory"],
        "orderby": "locationcode"
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400053
  @apiSuccess (200) {Array} result First element-Number of records, Second element-Array of results
  @apiSuccessExample {json} Success-Response
  [
    1, [{
      "locationname": "Rome-9W",
      "locationcode": "IT-9W-F0L",
      "buildingname": "Pani house",
      "sitecategory": "Live Site"
    }]
  ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Cilocation.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Cilocation.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
 };
