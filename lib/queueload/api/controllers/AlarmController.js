/* FileName: AlarmController.js
* @description: This file describes the various actions in the alarm controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 26/05/2017    Initial file creation                         SMandal
*
*/

/**
 * @apiGroup Alarm
 */

var errorCode = require("../resources/ErrorCodes.js");
var _ = require("lodash");

module.exports =
{
  acknowledge: function(req, res)
  {
    if(!req.body.alarmids || Array.isArray(!req.body.alarmids))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      Alarm.update({ id: req.body.alarmids, isack: false }, { isack: true, ackby: req.body.requesterid })
      .then(function(alarms)
      {
        sails.log.info("Acknowledged Alarms", alarms);
        var alarmsToPublish = _.map(alarms, 'id');
        alarmsToPublish.forEach(function(id, index)
        {
          Alarm.publishUpdate(id, alarms[ index ]);
        });
        return res.json(alarms);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Cidetail.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Alarm.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
};

