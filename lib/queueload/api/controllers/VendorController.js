/**
* VendorController
*
* @description :: Server-side logic for managing civendors
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 25/03/2016    Initial file creation                         SMandal
*
*/

/**
 * @apiGroup Vendor
 */

var errorCode = require("../resources/ErrorCodes.js");
var Promise = require("bluebird");
var redis = require("redis");
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
var _ = require("lodash");
if(sails.config.environment === 'development' || sails.config.environment === 'test' || sails.config.environment === 'staging')
{
  var client = redis.createClient(sails.config.connections.localRefTknStore);
}
else if(sails.config.environment === 'production')
{
  var client = redis.createClient(sails.config.connections.awsRefTknStore);
}
else
{
  sails.log.error("No environment set!!");
}

client.on("error", function(err)
  {
    sails.log.error("[AuthController.redisClientError] " + err);
  }
);

module.exports = {
  /**
  @apiGroup Vendor
  @apiName listVendors
  @api {post} /vendor/list List
  @apiDescription This API lists the vendors in the system
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MTYsImZpcnN0bmFtZSI6Ik1hcmNvIiwibGFzdG5hbWUiOiJQb2xvIiwidXNlcm5hbWUiOiJ
     tYXJjby5wb2xvIiwiZW1haWxpZCI6Im1hcmNvLnBvbG9AdGVzdGNsaWVudC5jb20iLCJjb3VudHJ5Y29kZSI6Iis5MSIsInBob25
     lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGxveWVlaWQiOiJGVEU4NzYyMzg5IiwiaXNhY3RpdmUiOnRydWV9LCJpYXQiOjE0NzI
     xMzExNDYsImV4cCI6MTQ3MjE1Mjc0NiwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3MiOiJ3d3cucXVldWVsb2FkLmNvbSJ
     9.Hq0pyeg3CxoJTaBDhHCQ26blL98n_Ac17ep2vixJmmc" -d '{"page" : 1,"limit" : 100}'
     "http://localhost:1337/vendor/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400331
  @apiSuccess (200) {Object[]} vendors Array of Vendors
  @apiSuccessExample {json} Success-Response
    [
      {
        "vendorid": 1,
        "vendorname": "IBM",
        "numberofcis": 4
      },
      {
        "vendorid": 2,
        "vendorname": "Quantum",
        "numberofcis": 0
      },
      {
        "vendorid": 3,
        "vendorname": "Hewlett Packard",
        "numberofcis": 0
      },
      {
        "vendorid": 4,
        "vendorname": "Keystashio",
        "numberofcis": 0
      }
    ]
  */
  list: function(req, res)
  {
    if((!req.body.page  || !(_.isFinite(req.body.page))) ||
    (!req.body.limit  || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    var output = [];
    Vendor.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400331;
        error.message = errorCode[ "400331" ];
        throw error;
      }
      else
      {
        output = found;
        var countCis = function(element)
        {
          return Cidetail.count().where({ vendorid: element.vendorid });
        };
        var actions = found.map(countCis);
        return Promise.all(actions);
      }
    })
    .then(function(result)
    {
      var assign = function(element, index)
      {
        output[ index ].numberofcis = element;
      };
      result.forEach(assign);
      return res.ok(output);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @api {post} /vendor/add Add
  @apiName addVendors
  @apiGroup Vendor
  @apiDescription On input of all valid parameters this function will add vendors to the system
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU
     1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0
     .jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYEJdx6oNW4" -d
      '{"vendornames": ["IBM", "Quantum", "Hewlett Packard"]}'
      "http://localhost:1337/vendor/add"
  @apiParam {String[]} vendornames Array of vendor names
  @apiParamExample {json} Request-Example
     {
        "vendornames": ["IBM", "Quantum", "Hewlett Packard"]
      }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400333
  @apiUse 403502
  @apiSuccess (200) {Object[]} vendors Array of Vendors
  @apiSuccessExample {json} Success-Response
    [
      {
        "vendorid": 1,
        "vendorname": "IBM"
      },
      {
        "vendorid": 2,
        "vendorname": "Quantum"
      },
      {
        "vendorid": 3,
        "vendorname": "Hewlett Packard"
      }
    ]
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode [ '403502' ];
      return res.negotiate(error);
    }
    if(!req.body.vendornames || !Array.isArray(req.body.vendornames))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var names = _.uniq(req.body.vendornames);

    // Check if the input has any duplicates
    if(names.length !== req.body.vendornames.length)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var results;
    Vendor.find().where({ vendorname:req.body.vendornames })
    .then(function showRecs(records)
    {
      // Check if any vendor in the input already exists
      if(records.length > 0)
      {
        var error = new Error();
        error.code = 400333;
        error.message = errorCode[ "400333" ];
        throw error;
      }
      else
      {
        var fn = function aysncadd(vendor)
        {
          return Vendor.create({ vendorname: vendor });
        };

        var actions = req.body.vendornames.map(fn);
        return Promise.all(actions);
      }
    })
    .then(function created(created)
    {
      results = created;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(results);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @api {post} /vendor/update Update
  @apiName updateVendors
  @apiGroup Vendor
  @apiDescription Updates a vendor name.
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Content-Type: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX
     VCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1
     hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV
     udW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU
     1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0
     .jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYEJdx6oNW4" -d
        '[{"vendorid": 2, "vendorname": "Stark Enterprises"},
          {"vendorid": 1, "vendorname": "Cyberdyne Systems"}]'
      "http://localhost:1337/vendor/update"
  @apiParam {Object[]} vendors Array of vendor id:name pairs
  @apiParamExample {json} Request-Example
    [{
      "vendorid": 2,
      "vendorname": "Stark Enterprises"
    }, {
      "vendorid": 1,
      "vendorname": "Cyberdyne Systems"
    }]
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400332
  @apiUse 400333
  @apiUse 403502
  @apiSuccess (200) {Object[]} vendors Array of Vendors
  @apiSuccessExample {json} Success-Response
    [
      [{
        "vendorid": 2,
        "vendorname": "Stark Enterprises"
      }],
      [{
        "vendorid": 1,
        "vendorname": "Cyberdyne Systems"
      }]
    ]
  */
  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    var vendorids = [];
    var vendors = [];
    var vendornames = [];
    for(var i = 0; i < req.body.length; i++)
    {
      if(!req.body[ i ].vendorname || !req.body[ i ].vendorid || !(_.isFinite(req.body[ i ].vendorid)))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
      else if(Array.isArray(req.body[ i ].vendorid) || Array.isArray(req.body[ i ].vendorname))
      {
        var error = new Error();
        error.code = 400051;
        error.message = errorCode[ "400051" ];
        return res.negotiate(error);
      }
      else
      {
        vendorids.push(req.body[ i ].vendorid);
        vendors.push(req.body[ i ]);
        vendornames.push(req.body[ i ].vendorname);
      }
    }

    vendornames = _.uniq(vendornames);
    vendorids = _.uniq(vendorids);

    if((vendornames.length !== req.body.length) || (vendorids.length !== req.body.length))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var results;
    Vendor.find().where({ vendorid: vendorids })
    .then(function showRecs(records)
    {
      // Check if the input has any non-existent ids
      if(records.length !== vendorids.length)
      {
        var error = new Error();
        error.code = 400332;
        error.message = errorCode[ "400332" ];
        throw error;
      }
      else
      {
        return Vendor.find().where({ vendorname: vendornames });
      }
    })
    .then(function showRecs(records)
    {
      // Check if input contains any existing vendor names
      if(records.length > 0)
      {
        var error = new Error();
        error.code = 400333;
        error.message = errorCode[ "400333" ];
        throw error;
      }
      else
      {
        var fn = function aysncupdate(vendor)
        {
          return Vendor.update({ vendorid: vendor.vendorid }, vendor);
        };

        var actions = vendors.map(fn);
        return Promise.all(actions);
      }
    })
    .then(function updated(update)
    {
      results = update;
      return client.hmsetAsync(process.env.ACCOUNTID + ":clientcache", "expired", true);
    })
    .then(function(value)
    {
      return res.json(results);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
