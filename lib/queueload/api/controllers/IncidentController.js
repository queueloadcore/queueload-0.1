/* FileName: IncidentController.js
*  @description: This file describes the various actions in the Incident controller.
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  15/10/2016        Initial file creation                         SMandal
*
*/

/**
 * ApiGroup Incident
 */

"use strict";
var Promise = require("bluebird");
var errorCode = require('../resources/ErrorCodes.js');
var enums = require('../resources/Enums.js');
var queueUtils = require('../services/utils/queueutils.js');
var stateMachine = require("javascript-state-machine");
var genUtils = require('../services/utils/genutils.js');
var serviceUtils = require("../services/utils/serviceutils.js");
var slackUtils = require("../services/utils/slackutils.js");
var userUtils = require("../services/utils/user.js");
var _ = require("lodash");
var sg = require('sendgrid')(sails.config.sendgrid.apikey);

module.exports = {
  /**
    @api {post} /incident/list List
    @apiName listIncident
    @apiGroup Incident
    @apiDescription On input of all valid parameters this function will list incidents
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
       curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
       mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
       zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
       mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
       1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
       W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
       '{
          "page" : 1,
          "limit" : 100
        }' "http://localhost:1337/incident/list"
    @apiParam {Number} limit Maximum number of results to show in a page
    @apiParam {Number} page The number of page to show
    @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 400451
    @apiUse 403502
    @apiSuccess (200) {Object[]} incidents Array of incidents
    @apiSuccessExample {json} Success-Response
      [
        {
          "serviceid": 1,
          "ciid": null,
          "ownerqueueid": 3,
          "owneruserid": 2,
          "assignedqueueid": 3,
          "assigneduserid": null,
          "vendorid": null,
          "pendinginc": null,
          "attachments": null,
          "relatedinc": null,
          "incidentid": 1,
          "state": "open",
          "title": "This is a test incident ticket",
          "description": "Description of the test incident ticket",
          "startedat": "1476788690",
          "endedat": null,
          "severity": 3,
          "vendorsr": null,
          "journal": [
            {
              "type": 1,
              "time": 1484226873,
              "operatorid": 27
            }
          ],
          "resolutioncode": null,
          "resolutionnotes": null,
          "closurecomments": null,
          "visibility": "operator"
        }
      ]
  */
  list: function(req, res)
  {
    if(!req.body.limit || isNaN(req.body.limit) || !req.body.page || isNaN(req.body.page))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      res.negotiate(error);
    }
    else
    {
      Incident.find().paginate({ page: req.body.page, limit: req.body.limit })
      .then(function(results)
      {
        if(!results.length)
        {
          var error = new Error();
          error.code = 400451;
          error.message = errorCode[ '400451' ];
          throw error;
        }
        else
        {
          return res.ok(results);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
    @api {post} /incident/add Add
    @apiName addIncident
    @apiGroup Incident
    @apiDescription On input of all valid parameters this function will add an incident
    @apiPermission operator
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
       curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ
       mlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV
       zdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI
       mVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ
       1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW
       W6z9ZSV89AUWUFZG_0" -H "Content-Type: application/json" -d
       '{
          "title": "This is a test incident ticket",
          "description": "Description of the test incident ticket",
          "serviceid": 1,
          "ownerqueueid": 3,
          "owneruserid": 2,
          "assignedqueueid": 3,
          "severity": 3,
          "impact": 1,
          "startedat" : 1476788690,
          "attachments" : [1,2,3]
        }' "http://localhost:1337/incident/add"
    @apiParam {String} title Title of the incident
    @apiParam {String} description Description of the incident
    @apiParam {Number} serviceid ID of the affected service
    @apiParam {Number} [ciid] ID of the affected CI
    @apiParam {Number} ownerqueueid ID of the queue owning the incident
    @apiParam {Number} owneruserid ID of the user owning the incident
    @apiParam {Number} assignedqueueid ID of the queue the ticket being assigned to
    @apiParam {Number} [assigneduserid] ID of the user the ticket being assigned to
    @apiParam {Number} startedat Time in epoch format. Must be equal or less then the current time and endedat(if present)
    @apiParam {Number} [endedat] Time in epoch format. Must be equal or less then the current time. Must be equal or more then the starttime.
    @apiParam {Number} severity Severity of the incident
    @apiParam {Array} [attachments] Array of attachments IDs
    @apiParamExample {json} Request-Example
      {
        "title": "This is the 1st test inc",
        "description": "Description of the test incident ticket",
        "serviceid": 1,
        "ownerqueueid": 3,
        "owneruserid": 2,
        "assignedqueueid": 3,
        "severity": 3,
        "impact": 1,
        "startedat" : 1476788690,
        "attachments" : [1,3,5]
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 403502
    @apiUse 400452
    @apiUse 400453
    @apiUse 400103
    @apiUse 400253
    @apiUse 400254
    @apiUse 400258
    @apiUse 400362
    @apiUse 400369
    @apiUse 400402
    @apiSuccess (200) {Object} incident Details of the added incident
    @apiSuccessExample {json} Success-Response
    {
      "attachments": [
        {
          "attachmentid": 1,
          "filename": "51xk3hyQvHL.jpg",
          "size": "33003"
        },
        {
          "attachmentid": 3,
          "filename": "37774-1920x1200.jpg",
          "size": "3135190"
        },
        {
          "attachmentid": 5,
          "filename": "steam.desktop",
          "size": "2191"
        }
      ],
      "relatedinc": [],
      "serviceid": {
        "serviceid": 1,
        "servicename": "Cinderella Spanking Workshop",
        "servicestateid": 2,
        "details": "The castle has much more to offer.",
        "isfrozen": false,
        "freezecause": 0,
        "cbid": 6,
        "bdid": 1
      },
      "ownerqueueid": {
        "queueid": 3,
        "queuename": "TestQueue3",
        "queueemail": "testqueue3@test.com",
        "alertl1": "testqueue3alert1@test.com",
        "alertl2": "testqueue3alert2@test.com",
        "alertl3": "testqueue3alert3@test.com",
        "isactive": true,
        "isfrozen": false,
        "freezecause": 0,
        "cbid": 6,
        "queueownerid": 2,
        "incidentmanagerid": 2,
        "changemanagerid": 2,
        "srtmanagerid": 2
      },
      "owneruserid": {
        "userid": 2,
        "firstname": "Test",
        "lastname": "User",
        "username": "T3st.User11",
        "emailid": "test.user11@testclient1.com",
        "countrycode": "+1",
        "phonenumber": "9999999999",
        "employeeid": "FTE000002",
        "isactive": true,
        "defaultqueue": 4
      },
      "assignedqueueid": {
        "queueid": 3,
        "queuename": "TestQueue3",
        "queueemail": "testqueue3@test.com",
        "alertl1": "testqueue3alert1@test.com",
        "alertl2": "testqueue3alert2@test.com",
        "alertl3": "testqueue3alert3@test.com",
        "isactive": true,
        "isfrozen": false,
        "freezecause": 0,
        "cbid": 6,
        "queueownerid": 2,
        "incidentmanagerid": 2,
        "changemanagerid": 2,
        "srtmanagerid": 2
      },
      "incidentid": 1,
      "state": 2,
      "title": "This is the 1st test inc",
      "description": "Description of the test incident ticket",
      "startedat": 1476788690,
      "endedat": null,
      "severity": 3,
      "vendorsr": null,
      "journal": [
        {
          "type": 1,
          "time": 1484226873,
          "operatorid": 27
        }
      ],
      "resolutioncode": null,
      "resolutionnotes": null,
      "closurecomments": null,
      "visibility": "operator"
    }
  */
  add: function(req, res)
  {
    if(!req.body.alertticket && !req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    else if(req.body.state || !req.body.title || (req.body.title.length > 128) || !req.body.description ||
     (req.body.description.length > 8192) || isNaN(req.body.serviceid) || isNaN(req.body.ownerqueueid) ||
      isNaN(req.body.owneruserid) || isNaN(req.body.assignedqueueid) || isNaN(req.body.startedat) ||
      (req.body.startedat < 1 || req.body.startedat > Math.floor(new Date() / 1000)) || !req.body.severity)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else if(req.body.severity < enums.incidentSeverity.HIGH || req.body.severity > enums.incidentSeverity.LOW)
    {
      var error = new Error();
      error.code = 400452;
      error.message = errorCode[ '400452' ];
      return res.negotiate(error);
    }
    else if(req.body.endedat && (isNaN(req.body.endedat) || req.body.startedat > req.body.endedat))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else
    {
      var reply;
      var assignedqueueDet = {};
      var incidentObj = {};
      var ciDetails = {};
      Service.findOne({ serviceid: req.body.serviceid })
      .then(function(service)
      {
        if(!service)
        {
          var error = new Error();
          error.code = 400362;
          error.message = errorCode[ '400362' ];
          throw error;
        }
        else if(service.servicestateid != enums.serviceStateIds.APPROVED)
        {
          var error = new Error();
          error.code = 400369;
          error.message = errorCode[ '400369' ];
          throw error;
        }
        else
        {
          var pGetQueue = queueUtils.getQueue([ req.body.ownerqueueid, req.body.assignedqueueid ]);
          var pGetUserDetails = userUtils.getUser([ req.body.requesterid ]);
          return Promise.all([ pGetQueue, pGetUserDetails ]);
        }
      })
      .then(function(results)
      {
        // Checking if the queues are active
        if((typeof results[ 0 ][ 0 ]) === "undefined")
        {
          var error = new Error();
          error.code = 400254;
          error.message = errorCode[ '400254' ];
          throw error;
        }
        if(!results[ 0 ][ 0 ].isactive || !results[ 0 ][ 1 ].isactive)
        {
          var error = new Error();
          error.code = 400253;
          error.message = errorCode[ '400253' ];
          throw error;
        }
        // Check if the owneruserid is a memeber of the ownerqueue
        else
        {
          // Checking the membership and active status of the member
          var membership = function(array, id)
          {
            for (var i = 0; i < array.length; i++)
            {
              if (array[ i ].userid === id)
              {
                if(!array[ i ].isactive)
                {
                  var error = new Error();
                  error.code = 400103;
                  error.message = errorCode[ '400103' ];
                  throw error;
                }
                else
                {
                  return true;
                }
              }
            }
            return false;
          };
          // Checking the membership of the ticket owner in the owner queue
          if(!membership(results[ 0 ][ 0 ].members, req.body.owneruserid))
          {
            var error = new Error();
            error.code = 400258;
            error.message = errorCode[ '400258' ];
            throw error;
          }
          else
          {
            assignedqueueDet = results[ 0 ][ 1 ];
            incidentObj.state = enums.incidentStates.OPEN;
            incidentObj.title = req.body.title;
            incidentObj.description = req.body.description;
            incidentObj.serviceid = req.body.serviceid;
            incidentObj.ownerqueueid = req.body.ownerqueueid;
            incidentObj.owneruserid = req.body.owneruserid;
            incidentObj.assignedqueueid = req.body.assignedqueueid;
            incidentObj.startedat = req.body.startedat;
            if(req.body.reportertype)
            {
              incidentObj.reportertype = enums.incReporterType.LOGGLY;
            }
            if(req.body.logglylogs && results[ 1 ].accountid.logglysubdomain)
            {
              incidentObj.logglylogs = req.body.logglylogs;
            }
            if(req.body.endedat)
            {
              incidentObj.endedat = req.body.endedat;
            }
            var journalEntry =
            {
              type: enums.incJournalTypes.INCIDENT_CREATED,
              time: Math.floor(new Date() / 1000),
              operatorid: req.body.requesterid
            };
            incidentObj.journal = [ journalEntry ];
            if(req.body.assigneduserid)
            {
              if(isNaN(req.body.assigneduserid))
              {
                var error = new Error();
                error.code = 400051;
                error.message = errorCode[ '4000051' ];
                throw error;
              }
              else
              {
                // Checking the membership of the assignee in the assigned queue
                if(!membership(results[ 0 ][ 1 ].members, req.body.assigneduserid))
                {
                  var error = new Error();
                  error.code = 400258;
                  error.message = errorCode[ '400258' ];
                  throw error;
                }
                else
                {
                  incidentObj.assigneduserid = req.body.assigneduserid;
                }
              }
            }
            if(req.body.ciid)
            {
              var pServRunsOn = serviceUtils.runsOn(req.body.serviceid, req.body.ciid);
              var pCiDetails = Cidetail.findOne({ select: [ 'cistateid', 'ciname', 'ipaddress' ] } ).where( { ciid: req.body.ciid } );
              var pArray = [ pServRunsOn, pCiDetails ];
              return Promise.all(pArray);
            }
            else
            {
              return incidentObj;
            }
          }
        }
      })
      .then(function(result)
      {
        if(req.body.ciid)
        {
          ciDetails = result[ 1 ];
          if(!result[ 0 ])
          {
            var error = new Error();
            error.code = 400389;
            error.message = errorCode[ '400389' ];
            throw error;
          }
          else if(ciDetails.cistateid < enums.ciStateIds.APPROVED)
          {
            var error = new Error();
            error.code = 400387;
            error.message = errorCode[ '400387' ];
            throw error;
          }
          else
          {
            incidentObj.ciid = req.body.ciid;
          }
        }
        else if(req.body.attachments)
        {
          return Attachment.find({ attachmentid: req.body.attachments });
        }
        else
        {
          return result[ 0 ];
        }
      })
      .then(function(result)
      {
        if(Array.isArray(result) && (result.length !== req.body.attachments.length))
        {
          var error = new Error();
          error.code = 400405;
          error.message = errorCode[ '400405' ];
          throw error;
        }
        else
        {
          if(req.body.severity)
          {
            incidentObj.severity = req.body.severity;
          }
          if(req.body.impact)
          {
            incidentObj.impact = req.body.impact;
          }
          return Incident.create(incidentObj);
        }
      })
      .then(function(incident)
      {
        if(req.body.attachments)
        {
          return Incident.update({ incidentid: incident.incidentid }, { attachments: req.body.attachments });
        }
        else
        {
          return incident;
        }
      })
      .then(function(result)
      {
        if(req.body.attachments)
        {
          reply = result[ 0 ];
        }
        else
        {
          reply = result;
        }
        return reply;
      })
      .then(function(reply)
      {
        var pFindSlack = Integrationconfig.findOne({ integrationtype: enums.integrationService.SLACK });
        var pFindIncident = Incident.find().where({ state: { '!': [ enums.incidentStates.CLOSED ] } }).where({ incidentid: { '!': [ reply.incidentid ] } }).where({ serviceid: reply.serviceid });
        var pServiceName = Service.findOne({ select: [ 'servicename' ] } ).where( { serviceid: reply.serviceid } );
        var pGetUserDetails = userUtils.getUser([ req.body.assigneduserid, req.body.owneruserid ]);
        var promiseArray = [ pFindSlack, pFindIncident, pServiceName, pGetUserDetails ];
        if(process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'staging')
        {
          sails.log.info("publishing create");
          Incident.publishCreate(reply);
        }
        return Promise.all(promiseArray);
      })
      .then(function(result)
      {
        var date = new Date(reply.startedat * 1000);
        var dateofmonth = "0" + date.getDate();
        var month = "0" + (date.getMonth() + 1);
        var year = date.getFullYear();
        var hours = "0" + date.getHours();
        var minutes = "0" + date.getMinutes();
        var seconds = "0" + date.getSeconds();
        var inclist = '';
        var padding = function(number, width, paddingchar)
        {
          paddingchar = paddingchar || '0';
          number = number + '';
          return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
        };
        for(var iter = 0; iter < result[ 1 ].length; iter++)
        {
          inclist = inclist + "INC" + padding(result[ 1 ][ iter ].incidentid, 5, 0) + '\n';
        }
        var enumToString = function(enums, value)
        {
          for (var k in enums)
          {
            if (enums[ k ] === value)
              return k;
          }
          return null;
        };
        var incSev = enumToString(enums.incidentSeverity, reply.severity);
        var message =
        {
          "attachments":
          [
            {
              "fallback": "[New Incident] INC" + padding(reply.incidentid, 5, 0),
              "color": "#663399",
              "pretext": "Incident details: INC" + padding(reply.incidentid, 5, 0),
              "title": reply.title,
              "title_link": "https://app.queueload.com/",
              "text": reply.description,
              "fields":
              [
                {
                  "title": "Service Name",
                  "value": result[ 2 ].servicename,
                  "short": true
                },
                {
                  "title": "Severity",
                  "value": incSev.charAt(0).toUpperCase() + incSev.substr(1).toLowerCase(),
                  "short": true
                },
                {
                  "title": "Start Time",
                  "value": dateofmonth.substr(-2) + "/" + month.substr(-2) + "/" + year + " " + hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2) + " GMT",
                  "short": true
                },
                {
                  "title": "Other active incidents of the service",
                  "value": inclist || "No other active incidents",
                  "short": true
                },
                {
                  "title": "Configuration Item",
                  "value": req.body.ciid ? ciDetails.ciname : "Not assigned",
                  "short": true
                },
                {
                  "title": "IP Address",
                  "value": req.body.ciid ? ciDetails.ipaddress : "NA",
                  "short": true
                },
                {
                  "title": "Assigned Queue",
                  "value": assignedqueueDet.queuename,
                  "short": true
                },
                {
                  "title": "Assigned User",
                  "value": req.body.assigneduserid ? result[ 3 ][ 0 ].username : "Not assigned",
                  "short": true
                }
              ]
            }
          ]
        };
        var pNotifications = [];
        var custEmail = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: assignedqueueDet.queueemail
                  }
                ],
                'substitutions': {
                  '-servicename-': result[ 2 ].servicename,
                  '-severity-': incSev.charAt(0).toUpperCase() + incSev.substr(1).toLowerCase(),
                  '-starttime-': dateofmonth.substr(-2) + "/" + month.substr(-2) + "/" + year + " " + hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2) + " GMT",
                  '-inclist-': inclist || "No other active incidents",
                  '-ci-': ciDetails.ciname ? ciDetails.ciname : "Not assigned",
                  '-ipaddress-': ciDetails.ipaddress ? ciDetails.ipaddress : "NA",
                  '-assignedqueue-': assignedqueueDet.queuename,
                  '-assigneduser-': req.body.assigneduserid ? result[ 3 ][ 0 ].username : "Not assigned"
                },
                subject: '[QueueLoad] INC' + padding(reply.incidentid, 5, 0) + ' has been created'
              }
            ],
            from: {
              email: 'noreply@queueload.com'
            },
            'template_id': '02d922b7-2077-4240-8b39-18b4cc16ff37'
          }
        });
        sails.log.info("Notifying the customer in email about the ticket");
        pNotifications.push(sg.API(custEmail));
        if(result[ 0 ] && result[ 0 ].config.slackincomingwebhookurl)
        {
          pNotifications.push(slackUtils.notifySlack(message, result[ 0 ].config.slackincomingwebhookurl));
        }
        return Promise.all(pNotifications);
      })
      .then(function()
      {
        return res.ok(reply);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
    @api {post} /incident/update Update
    @apiName updateIncident
    @apiGroup Incident
    @apiDescription On input of all valid parameters this function will update an incident
    @apiPermission operator
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxd
      WV1ZSI6bnVsbCwidXNlcmlkIjo4LCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJ
      UM3N0LlVzZXIxMyIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxM0B0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxI
      iwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAxMyIsImlzYWN0aXZlIjp0cnVlfSw
      iaWF0IjoxNDc3Mzk3NjQyLCJleHAiOjE0Nzc0MTkyNDIsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3L
      nF1ZXVlbG9hZC5jb20ifQ.tWn2v19UkgGWVPHwHPd2SwDxwp7vD6-ps0LlJTUattU" -H "Content-Type: application/json" -d
        '{
          "incidentid": 1,
          "state" : 4
        }' "http://localhost:1337/incident/update"
    @apiParam {Number} incidentid ID of the incident
    @apiParam {Number} [state] State the ticket to be transitioned into
    @apiParam {Number} [ciid] ID of the affected CI
    @apiParam {Number} [owneruserid] ID of the user owning the incident. The user must be a member of ownerqueueid
    @apiParam {Number} [assignedqueueid] ID of the queue the ticket being assigned to
    @apiParam {Number} [assigneduserid] ID of the user the ticket being assigned to. The user must be a member of assignedqueueid
    @apiParam {Number} startedat Time in epoch format. Must be equal or less then the current time and endedat(if present)
    @apiParam {Number} [endedat] Time in epoch format. Must be equal or less then the current time. Must be equal or more then the starttime.
    @apiParam {Number} [severity] Severity of the incident
    @apiParam {Number} [impact] Impact of the incident
    @apiParam {Number} [vendorid] ID of the vendor
    @apiParam {String} [vendorsr] SR# of opened with the vendor
    @apiParam {Number} [pendinginc] ID of the incident causing pendency
    @apiParam {String} [journal] Comments to be entered in the journal
    @apiParam {Number[]} [relatedinc] ID of the related incidents
    @apiParam {Number} [resolutioncode] Code of the resolution
    @apiParam {String} [resolutionnotes] Notes from the operator
    @apiParam {String} [closurecomments] Notes from the owner at the time of closure
    @apiParam {Array} [attachments] Array of attachments IDs
    @apiParam {Number} [rating] Rating of service satisfaction
    @apiParamExample {json} Request-Example
    {
      "incidentid": 1,
      "state" : 4
    }
    @apiUse 401
    @apiUse 401505
    @apiUse 403502
    @apiUse 400051
    @apiUse 400253
    @apiUse 400258
    @apiUse 400332
    @apiUse 400389
    @apiUse 400405
    @apiUse 400406
    @apiUse 400454
    @apiUse 400456
    @apiUse 400458
    @apiUse 400459
    @apiUse 400461
    @apiUse 400462
    @apiUse 400463
    @apiUse 400464
    @apiUse 400465
    @apiUse 400466
    @apiUse 400467
    @apiUse 400468
    @apiUse 400469
    @apiUse 400470
    @apiUse 400471
    @apiUse 400472
    @apiUse 400477
    @apiUse 500001
    @apiSuccess (200) {Object} incident Details of the added incident
    @apiSuccessExample {json} Success-Response
    {
      "incidentid": 1,
      "state": 4,
      "title": "This is the 1st test inc",
      "description": "Description of the test incident ticket",
      "serviceid": 1,
      "ciid": null,
      "ownerqueueid": 1,
      "owneruserid": 2,
      "assignedqueueid": 3,
      "assigneduserid": 8,
      "startedat": 1476788690,
      "endedat": null,
      "severity": 1,
      "impact": 1,
      "vendorid": null,
      "vendorsr": null,
      "pendinginc": null,
      "journal": [
        {
          "type": 1,
          "time": 1484226873,
          "operatorid": 27
        }
      ],
      "resolutioncode": null,
      "resolutionnotes": null,
      "closurecomments": null,
      "visibility": "operator"
    }
  */
  update: function(req, res)
  {
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    if(req.body.title || req.body.description || !req.body.incidentid || req.body.visibility || (req.body.assignedqueueid && req.body.state && req.body.state !== enums.incidentStates.OPEN) ||
     req.body.ownerqueueid || req.body.serviceid || (req.body.state < enums.incidentStates.REJECT || req.body.state > enums.incidentStates.PENDINGOTHER) ||
     (req.body.pendinginc && (req.body.incidentid === req.body.pendinginc)) || (req.body.startedat && (isNaN(req.body.startedat) || (req.body.startedat < 1) || (req.body.startedat > Math.floor(new Date() / 1000)))) ||
     (req.body.endedat && (isNaN(req.body.endedat) || (req.body.endedat > Math.floor(new Date() / 1000)))) || (req.body.attachments && !(Array.isArray(req.body.attachments))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    if(req.body.state === enums.incidentStates.OPEN && !req.body.assignedqueueid)
    {
      var error = new Error();
      error.code = 400459;
      error.message = errorCode[ '400459' ];
      throw error;
    }
    var fsmConstruct = function(currentIncident)
    {
      this.initial = genUtils.enumtostr(enums.incidentStates, currentIncident.state),
      this.events = [
        { name: 'OPEN', from: [ 'OPEN', 'REJECT', 'ASSIGNED', 'WORKINPROGRESS', 'UNRESOLVED', 'PENDINGINC', 'PENDINGCUSTOMER', 'PENDINGVENDOR', 'PENDINGOTHER' ], to: 'OPEN' },
        { name: 'REJECT', from: [ 'OPEN', 'REJECT', 'WORKINPROGRESS', 'ASSIGNED' ], to: 'REJECT' },
        { name: 'ASSIGNED', from: [ 'ASSIGNED', 'OPEN' ], to: 'ASSIGNED' },
        { name: 'WORKINPROGRESS', from: [ 'WORKINPROGRESS', 'ASSIGNED', 'PENDINGINC', 'PENDINGOTHER', 'PENDINGVENDOR', 'UNRESOLVED', 'PENDINGCUSTOMER' ], to: 'WORKINPROGRESS' },
        { name: 'RESOLVED', from: [ 'RESOLVED', 'WORKINPROGRESS', 'UNRESOLVED' ], to: 'RESOLVED' },
        { name: 'UNRESOLVED', from: [ 'UNRESOLVED', 'RESOLVED' ], to: 'UNRESOLVED' },
        { name: 'CLOSED', from: [ 'RESOLVED' ], to: 'CLOSED' },
        { name: 'PENDINGINC', from: [ 'PENDINGINC', 'WORKINPROGRESS' ], to: 'PENDINGINC' },
        { name: 'PENDINGCUSTOMER', from: [ 'PENDINGCUSTOMER', 'WORKINPROGRESS' ], to: 'PENDINGCUSTOMER' },
        { name: 'PENDINGVENDOR', from: [ 'PENDINGVENDOR', 'WORKINPROGRESS' ], to: 'PENDINGVENDOR' },
        { name: 'PENDINGOTHER', from: [ 'PENDINGOTHER', 'WORKINPROGRESS' ], to: 'PENDINGOTHER' }
      ],
      this.callbacks =
      {
        /** Order of callbacks execution
        * onbeforeEVENT - specific handler for the specified event only
        * onbeforeevent - generic handler for all events
        * onleaveSTATE - specific handler for the specified state only
        * onleavestate - generic handler for all states
        * onenterSTATE - specific handler for the specified state only
        * onenterstate - generic handler for all states
        * onafterEVENT - specific handler for the specified event only
        * onafterevent - generic handler for all events
        */
        // Generic callbacks
        //onbeforeevent: function() {console.log('genric onbeforeevent');},
        //onleavestate: function() {console.log('generic onleavestate');},
        //onenterstate: function() {console.log('generic onenterstate');},
        //onafterevent: function() {console.log('generic on afterevent');},
        // Event/State specific callbacks
        // OPEN Callbacks
        onbeforeOPEN: function()
        {
          incidentUpdObj.journal = currentIncident.journal;
          if(currentIncident.assignedqueueid === req.body.assignedqueueid && currentIncident.state !== enums.incidentStates.REJECT)
          {
            return false;
          }
          else
          {
            var journalEntry =
            {
              type: enums.incJournalTypes.ASSIGNED_TO_QUEUE,
              time: Math.floor(new Date() / 1000),
              operatorid: req.body.requesterid,
              currentassgniedqueueid: currentIncident.assignedqueueid,
              newassignedqueueid: req.body.assignedqueueid
            };
            incidentUpdObj.journal.push(journalEntry);
          }
          if(typeof req.body.assigneduserid !== 'undefined')
          {
            if(!currentIncident.assigneduserid)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.ASSIGNED_TO_OPERATOR,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                currentassigneduserid: "",
                newassigneduserid: req.body.assigneduserid
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            else if(currentIncident.assigneduserid !== req.body.assigneduserid)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.ASSIGNED_TO_OPERATOR,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                currentassigneduserid: currentIncident.assigneduserid,
                newassigneduserid: req.body.assigneduserid
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            if(currentIncident.state !== enums.incidentStates.OPEN)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.OPEN
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            var pQStatus = queueUtils.isActive([ req.body.assignedqueueid ]);
            var pIsMember = queueUtils.isMember(req.body.assignedqueueid, req.body.assigneduserid);
            promiseArray.push(pQStatus, pIsMember);
          }
          else
          {
            if(currentIncident.assigneduserid)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.ASSIGNED_TO_OPERATOR,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                currentassigneduserid: currentIncident.assigneduserid,
                newassigneduserid: ""
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            if(currentIncident.state !== enums.incidentStates.OPEN)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.OPEN
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            var pQStatus = queueUtils.isActive([ req.body.assignedqueueid ]);
            promiseArray.push(pQStatus);
          }
          return;
        },
        //OnleaveOPEN: function() {console.log('before leaving state OPEN');},
        //onenterOPEN: function() {console.log ('on entering OPEN');},
        //onafterOPEN: function() {console.log ('on after OPEN');},
        // ASSIGNED Callbacks
        onbeforeASSIGNED: function()
        {
          // Check if the assignee is provided or the ticket alreay has an assignee
          if(req.body.assigneduserid || currentIncident.assigneduserid)
          {
            incidentUpdObj.journal = currentIncident.journal;
            // Checking only supplied userid as queueid cannot be changed while moving to ASSIGNED state
            if(typeof req.body.assigneduserid !== 'undefined')
            {
              if(req.body.assigneduserid !== currentIncident.assigneduserid)
              {
                var journalEntry =
                {
                  type: enums.incJournalTypes.ASSIGNED_TO_OPERATOR,
                  time: Math.floor(new Date() / 1000),
                  operatorid: req.body.requesterid,
                  currentassigneduserid: currentIncident.assigneduserid,
                  newassigneduserid: req.body.assigneduserid
                };
                incidentUpdObj.journal.push(journalEntry);
              }
              var pIsMember = queueUtils.isMember(currentIncident.assignedqueueid, req.body.assigneduserid);
              promiseArray.push(pIsMember);
            }
            if(currentIncident.state !== enums.incidentStates.ASSIGNED)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.ASSIGNED
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            return true;
          }
          else
          {
            return false;
          }
        },
        //OnleaveASSIGNED: function() {console.log('before leaving state ASSIGNED');},
        //onenterASSIGNED: function() {console.log('on entering ASSIGNED');},
        //onafterASSIGNED: function() {console.log('on after ASSIGNED');},
        // REJECT Callbacks
        onbeforeREJECT: function()
        {
          if(!req.body.journal || req.body.assigneduserid)
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            if(currentIncident.state !== enums.incidentStates.REJECT)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.REJECT
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            return true;
          }
        },
        //OnleaveREJECT: function() {console.log('before leaving state REJECT');},
        //OnenterREJECT: function() {console.log ("on entering REJECT");},
        //OnafterREJECT: function() {console.log ('on after REJECT');},
        // WORKINPROGRESS Callbacks
        onbeforeWORKINPROGRESS: function()
        {
          // Checking only supplied userid as queueid cannot be changed while moving to ASSIGNED state
          incidentUpdObj.journal = currentIncident.journal;
          if(currentIncident.state !== enums.incidentStates.WORKINPROGRESS)
          {
            var journalEntry =
            {
              type: enums.incJournalTypes.CHANGED_TO_STATE,
              time: Math.floor(new Date() / 1000),
              operatorid: req.body.requesterid,
              state: enums.incidentStates.WORKINPROGRESS
            };
            incidentUpdObj.journal.push(journalEntry);
          }
          if(typeof req.body.assigneduserid !== 'undefined')
          {
            if(req.body.assigneduserid !== currentIncident.assigneduserid)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.ASSIGNED_TO_OPERATOR,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                currentassigneduserid: currentIncident.assigneduserid,
                newassigneduserid: req.body.assigneduserid
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            var pIsMember = queueUtils.isMember(currentIncident.assignedqueueid, req.body.assigneduserid);
            promiseArray.push(pIsMember);
          }
          incidentUpdObj.visibility = 'operator';
          return true;
        },
        //OnleaveWORKINPROGRESS: function() {console.log('before leaving WORKINPROGRESS');},
        //OnenterWORKINPROGRESS: function() {console.log('on entering WORKINPROGRESS');},
        //OnafterWORKINPROGRESS: function() {console.log('on after WORKINPROGRESS');},
        onbeforeRESOLVED: function()
        {
          if((!req.body.endedat && !currentIncident.endedat) || (req.body.resolutioncode < 1 || req.body.resolutioncode > 3) || (!req.body.resolutioncode) || (!req.body.resolutionnotes))
          {
            return false;
          }
          else if((req.body.resolutioncode === enums.incidentResCodes.DUPTICKET && !req.body.dupticket) || (req.body.dupticket === req.body.incidentid))
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            incidentUpdObj.resolutioncode = req.body.resolutioncode;
            incidentUpdObj.resolutionnotes = req.body.resolutionnotes;
            incidentUpdObj.endedat = req.body.endedat;
            if(currentIncident.state !== enums.incidentStates.RESOLVED)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.RESOLVED
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            if(typeof req.body.assigneduserid !== 'undefined')
            {
              var pIsMember = queueUtils.isMember(currentIncident.assignedqueueid, req.body.assigneduserid);
              promiseArray.push(pIsMember);
            }
            if(req.body.resolutioncode === enums.incidentResCodes.DUPTICKET)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.DUPLICATE_TICKET,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                dupticket: req.body.dupticket
              };
              incidentUpdObj.journal.push(journalEntry);
              var pFindIncident = Incident.findOne({ incidentid: req.body.dupticket });
              promiseArray.push(pFindIncident);
            }
            incidentUpdObj.resolvedat = Math.floor(new Date() / 1000);
            return true;
          }
        },
        //OnleaveRESOLVED: function() {console.log('on leaving RESOLVED');},
        //OnenterRESOLVED: function() {console.log('on entering RESOLVED');},
        //onafterRESOLVED: function() {console.log('on after RESOLVED');},
        onbeforeUNRESOLVED: function()
        {
          if(!req.body.journal)
          {
            return false;
          }
          else if(req.body.title || req.body.description || req.body.serviceid || req.body.ciid || req.body.owneruserid ||
            req.body.assignedqueueid || req.body.assigneduserid || req.body.startedat || req.body.endedat || req.body.severity || req.body.impact ||
            req.body.vendor || req.body.vendorsr || req.body.pendinginc || req.body.attachments || req.body.resolutioncode ||
            req.body.resolutionnotes)
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            if(currentIncident.state !== enums.incidentStates.UNRESOLVED)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.UNRESOLVED
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            incidentUpdObj.resolutioncode = null;
            incidentUpdObj.resolutionnotes = null;
            incidentUpdObj.resolvedat = null;
            incidentUpdObj.dupticket = null;
            return true;
          }
        },
        //OnleaveUNRESOLVED: function() {console.log('before leaving UNRESOLVED');},
        //onenterUNRESOLVED: function() {console.log('on entering UNRESOLVED');},
        //onafterUNRESOLVED: function() {console.log('on after UNRESOLVED');},
        onbeforeCLOSED: function()
        {
          if(!req.body.closurecomments || (req.body.rating && (req.body.rating < enums.incSatisfactionRating.DISSATISFIED) || (req.body.rating > enums.incSatisfactionRating.SATISFIED)))
          {
            return false;
          }
          else if(req.body.title || req.body.description || req.body.serviceid || req.body.ciid || req.body.owneruserid ||
            req.body.assignedqueueid || req.body.assigneduserid || req.body.startedat || req.body.endedat || req.body.severity || req.body.impact ||
            req.body.vendor || req.body.vendorsr || req.body.pendinginc || req.body.attachments || req.body.resolutioncode ||
            req.body.resolutionnotes || req.body.journal)
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            incidentUpdObj.closurecomments = req.body.closurecomments;
            if(req.body.rating)
            {
              incidentUpdObj.rating = req.body.rating;
            }
            incidentUpdObj.visibility = null;
            if(currentIncident.state !== enums.incidentStates.CLOSED)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.CLOSED
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            return true;
          }
        },
        //OnleaveCLOSED: function() {console.log('before leaving CLOSED');},
        onenterCLOSED: function()
        {
          incidentUpdObj.resolutioncode = currentIncident.resolutioncode;
          incidentUpdObj.resolutionnotes = currentIncident.resolutionnotes;
          incidentUpdObj.endedat = currentIncident.endedat;
        },
        //OnafterCLOSED: function() {console.log('on after CLOSED');},
        onbeforePENDINGINC: function()
        {
          if(!req.body.pendinginc || (req.body.pendinginc === currentIncident.pendinginc))
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            var journalEntry =
            {
              type: enums.incJournalTypes.PENDING_FOR_INC,
              time: Math.floor(new Date() / 1000),
              operatorid: req.body.requesterid,
              pendinginc: req.body.pendinginc
            };
            incidentUpdObj.journal.push(journalEntry);
            if(currentIncident.state !== enums.incidentStates.PENDINGINC)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.PENDINGINC
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            var pIncStatus = Incident.findOne({ incidentid: req.body.pendinginc });
            promiseArray.push(pIncStatus);
          }
          return true;
        },
        onleavePENDINGINC: function()
        {
          incidentUpdObj.pendinginc = null;
        },
        //OnenterPENDINGINC: function() {console.log('on entering PENDINGINC');},
        //onafterPENDINGINC: function() {console.log('on after PENDINGINC');},
        onbeforePENDINGCUSTOMER: function()
        {
          if(!req.body.journal)
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            if(currentIncident.state !== enums.incidentStates.PENDINGCUSTOMER)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.PENDINGCUSTOMER
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            incidentUpdObj.visibility = 'owner';
            return true;
          }
        },
        onleavePENDINGCUSTOMER: function()
        {
          if(!req.body.journal)
          {
            return false;
          }
        },
        //OnenterPENDINGCUSTOMER: function() {console.log('on entering PENDINGCUSTOMER');},
        //onafterPENDINGCUSTOMER: function() {console.log('on after PENDINGCUSTOMER');},
        onbeforePENDINGVENDOR: function()
        {
          if(!req.body.vendorid)
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            if(req.body.vendorsr)
            {
              var journalEntry =
              {
                 type: enums.incJournalTypes.PENDING_FOR_VENDORSR,
                 time: Math.floor(new Date() / 1000),
                 operatorid: req.body.requesterid,
                 vendorsr: req.body.vendorsr,
                 vendorid: req.body.vendorid
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            else
            {
              var journalEntry =
              {
                 type: enums.incJournalTypes.PENDING_FOR_VENDORSR,
                 time: Math.floor(new Date() / 1000),
                 operatorid: req.body.requesterid,
                 vendorsr: "",
                 vendorid: req.body.vendorid
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            if(currentIncident.state !== enums.incidentStates.PENDINGVENDOR)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.PENDINGVENDOR
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            var pVendStatus = Vendor.findOne({ vendorid: req.body.vendorid });
            promiseArray.push(pVendStatus);
            return true;
          }
        },
        onleavePENDINGVENDOR: function()
        {
          incidentUpdObj.vendorid = null;
          incidentUpdObj.vendorsr = null;
        },
        //OnenterPENDINGVENDOR: function() {console.log('on entering PENDINGVENDOR');},
        //onafterPENDINGVENDOR: function() {console.log('on after PENDINGVENDOR');},
        onbeforePENDINGOTHER: function()
        {
          if(!req.body.journal)
          {
            return false;
          }
          else
          {
            incidentUpdObj.journal = currentIncident.journal;
            if(currentIncident.state !== enums.incidentStates.PENDINGOTHER)
            {
              var journalEntry =
              {
                type: enums.incJournalTypes.CHANGED_TO_STATE,
                time: Math.floor(new Date() / 1000),
                operatorid: req.body.requesterid,
                state: enums.incidentStates.PENDINGOTHER
              };
              incidentUpdObj.journal.push(journalEntry);
            }
            return true;
          }
        }
        //OnleavePENDINGOTHER: function() {console.log('before leaving PENDINGOTHER');},
        //onenterPENDINGOTHER: function() {console.log('on entering PENDINGOTHER');},
        //onafterPENDINGOTHER: function() {console.log('on after PENDINGOTHER');},
      };
    };
    var currentIncident = {};
    var incidentUpdObj = {};
    var fsm = {};
    var promiseArray = [];
    var isOperator, isOwner;
    var moveToOpen = function()
    {
      if(!fsm.can('OPEN'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.OPEN() === 3)
      {
        var error = new Error();
        error.code = 400475;
        error.message = errorCode[ '400475' ];
        throw error;
      }
      else
      {
        return true;
      }
    };
    var moveToAssigned = function()
    {
      if(!fsm.can('ASSIGNED'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      // Not sure why 2 is not getting evaluated as the value of fsm.ASSIGNED() is 2. Running with negative test for now
      else if(fsm.ASSIGNED() === 3)
      {
        var error = new Error();
        error.code = 400458;
        error.message = errorCode[ '400458' ];
        throw error;
      }
      else
      {
        var pResults = Promise.all(promiseArray);
        return pResults;
      }
    };
    var moveToReject = function()
    {
      if(!fsm.can('REJECT'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.REJECT() === 3)
      {
        if(!req.body.journal)
        {
          var error = new Error();
          error.code = 400461;
          error.message = errorCode[ '400461' ];
          throw error;
        }
        else
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ '400051' ];
          throw error;
        }
      }
      else
      {
        return true;
      }
    };
    var moveToWORKINPROGRESS = function()
    {
      if(!fsm.can('WORKINPROGRESS'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else
      {
        if(fsm.WORKINPROGRESS() === 3)
        {
          if(!req.body.journal)
          {
            var error = new Error();
            error.code = 400461;
            error.message = errorCode[ '400461' ];
            throw error;
          }
        }
        else
        {
          var pResults = Promise.all(promiseArray);
          return pResults;
        }
      }
    };
    var moveToRESOVLED = function()
    {
      if(!fsm.can('RESOLVED'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if((req.body.endedat !== 'undefined') && (currentIncident.startedat > req.body.endedat))
      {
        var error = new Error();
        error.code = 400469;
        error.message = errorCode[ '400469' ];
        throw error;
      }
      else if(fsm.RESOLVED() === 3)
      {
        if(req.body.resolutioncode === enums.incidentResCodes.DUPTICKET && !req.body.dupticket)
        {
          var error = new Error();
          error.code = 400478;
          error.message = errorCode[ '400478' ];
          throw error;
        }
        else if(req.body.dupticket === req.body.incidentid)
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ '400051' ];
          throw error;
        }
        else
        {
          var error = new Error();
          error.code = 400462;
          error.message = errorCode[ '400462' ];
          throw error;
        }
      }
      else
      {
        var pResults = Promise.all(promiseArray);
        return pResults;
      }
    };
    var moveToUNRESOLVED = function()
    {
      if(!fsm.can('UNRESOLVED'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.UNRESOLVED() === 3)
      {
        if(!req.body.journal)
        {
          var error = new Error();
          error.code = 400461;
          error.message = errorCode[ '400461' ];
          throw error;
        }
        else if(req.body.title || req.body.description || req.body.serviceid || req.body.ciid ||
          req.body.assignedqueueid || req.body.assigneduserid || req.body.startedat || req.body.endedat || req.body.severity || req.body.impact ||
          req.body.vendor || req.body.vendorsr || req.body.pendinginc || req.body.resolutioncode ||
          req.body.resolutionnotes)
        {
          var error = new Error();
          error.code = 400463;
          error.message = errorCode[ '400463' ];
          throw error;
        }
      }
      else
      {
        return true;
      }
    };
    var moveToCLOSED = function()
    {
      if(!fsm.can('CLOSED'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.CLOSED() === 3)
      {
        if(!req.body.closurecomments)
        {
          var error = new Error();
          error.code = 400464;
          error.message = errorCode[ '400464' ];
          throw error;
        }
        else if(req.body.title || req.body.description || req.body.serviceid || req.body.ciid || req.body.owneruserid ||
            req.body.assignedqueueid || req.body.assigneduserid || req.body.startedat || req.body.endedat || req.body.severity || req.body.impact ||
            req.body.vendor || req.body.vendorsr || req.body.pendinginc || req.body.attachments || req.body.resolutioncode ||
            req.body.resolutionnotes || req.body.journal)
        {
          var error = new Error();
          error.code = 400465;
          error.message = errorCode[ '400465' ];
          throw error;
        }
        else if(req.body.rating && (req.body.rating < enums.incSatisfactionRating.DISSATISFIED) || (req.body.rating > enums.incSatisfactionRating.SATISFIED))
        {
          var error = new Error();
          error.code = 400477;
          error.message = errorCode[ '400477' ];
          throw error;
        }
      }
      else
      {
        return true;
      }
    };
    var moveToPENDINGINC = function()
    {
      if(!fsm.can('PENDINGINC'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.PENDINGINC() === 3)
      {
        var error = new Error();
        error.code = 400466;
        error.message = errorCode[ '400466' ];
        throw error;
      }
      else
      {
        var pResults = Promise.all(promiseArray);
        return pResults;
      }
    };
    var moveToPENDINGCUSTOMER = function()
    {
      if(!fsm.can('PENDINGCUSTOMER'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.PENDINGCUSTOMER() === 3)
      {
        var error = new Error();
        error.code = 400461;
        error.message = errorCode[ '400461' ];
        throw error;
      }
      else
      {
        return true;
      }
    };
    var moveToPENDINGVENDOR = function()
    {
      if(!fsm.can('PENDINGVENDOR'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.PENDINGVENDOR() === 3)
      {
        var error = new Error();
        error.code = 400467;
        error.message = errorCode[ '400467' ];
        throw error;
      }
      else
      {
        var pResults = Promise.all(promiseArray);
        return pResults;
      }
    };
    var moveToPENDINGOTHER = function()
    {
      if(!fsm.can('PENDINGOTHER'))
      {
        var error = new Error();
        error.code = 400456;
        error.message = errorCode[ '400456' ];
        throw error;
      }
      else if(fsm.PENDINGOTHER() === 3)
      {
        var error = new Error();
        error.code = 400461;
        error.message = errorCode[ '400461' ];
        throw error;
      }
      else
      {
        return true;
      }
    };
    // Incident update logic block
    Incident.findOne({ incidentid: req.body.incidentid }).populate('attachments').populate('alarmids')
    .then(function(incident)
    {
      if(!incident)
      {
        var error = new Error();
        error.code = 400454;
        error.message = errorCode[ '400454' ];
        throw error;
      }
      else if(incident.state === enums.incidentStates.CLOSED)
      {
        var error = new Error();
        error.code = 400455;
        error.message = errorCode[ '400455' ];
        throw error;
      }
      else
      {
        currentIncident = incident;
        var promiseArray = [];
        var isMember1 = queueUtils.isMember(currentIncident.assignedqueueid, req.body.requesterid);
        var isMember2 = queueUtils.isMember(currentIncident.ownerqueueid, req.body.requesterid);
        promiseArray = [ isMember1, isMember2 ];
        if(req.body.logglylogs)
        {
          promiseArray.push(Integrationconfig.findOne({ integrationtype: enums.integrationService.LOGGLY }));
        }
        if(req.body.state === enums.incidentStates.RESOLVED && incident.alarmids.length)
        {
          var unackAlarms = false;
          incident.alarmids.every(function(alarm) {
            if(!alarm.isack)
            {
              unackAlarms = !unackAlarms;
              return false;
            }
            else
            {
              return true;
            }
          });
          if(unackAlarms)
          {
            var error = new Error();
            error.code = 400480;
            error.message = errorCode[ '400480' ];
            throw error;
          }
        }
        return Promise.all(promiseArray);
      }
    })
    .then(function(result)
    {
      isOperator = result[ 0 ];
      isOwner = result[ 1 ];
      if(isOperator || isOwner)
      {
        if((currentIncident.visibility === 'operator' && !isOperator) || (currentIncident.visibility === 'owner' && !isOwner))
        {
          if(req.body.attachments || req.body.journal || req.body.owneruserid)
          {
            if(req.body.state || req.body.ciid || req.body.assigneduserid || req.body.assignedqueueid ||
             req.body.startedat || req.body.endedat || req.body.severity || req.body.impact ||
             req.body.vendorid || req.body.vendorsr || req.body.pendinginc || req.body.resolutioncode ||
             req.body.resolutionnotes || req.body.closurecomments)
            {
              var error = new Error();
              error.code = 400471;
              error.message = errorCode[ '400471' ];
              throw error;
            }
          }
          else
          {
            var error = new Error();
            error.code = 400471;
            error.message = errorCode[ '400471' ];
            throw error;
          }
        }
        else
        {
          if(req.body.logglylogs)
          {
            if(!result[ 2 ])
            {
              var error = new Error();
              error.code = 400479;
              error.message = errorCode[ '400479' ];
              throw error;
            }
            if(currentIncident.logglylogs)
            {
              incidentUpdObj.logglylogs = currentIncident.logglylogs;
            }
            else
            {
              incidentUpdObj.logglylogs = [];
            }
            incidentUpdObj.logglylogs.push(req.body.logglylogs);
          }
          return currentIncident;
        }
      }
      else
      {
        var error = new Error();
        error.code = 400470;
        error.message = errorCode[ '400470' ];
        throw error;
      }
    })
    .then(function(incident)
    {
      fsm = stateMachine.create(new fsmConstruct(currentIncident));
      if(req.body.journal)
      {
        if(!incidentUpdObj.journal)
        {
          incidentUpdObj.journal = currentIncident.journal;
        }
        var journalEntry =
        {
          type: enums.incJournalTypes.MANUAL_ENTRY,
          time: Math.floor(new Date() / 1000),
          operatorid: req.body.requesterid,
          message: req.body.journal
        };
        incidentUpdObj.journal.push(journalEntry);
      }
      if(req.body.assignedqueueid && req.body.assignedqueueid !== currentIncident.assignedqueueid)
      {
        if(!fsm.can('OPEN'))
        {
          var error = new Error();
          error.code = 400456;
          error.message = errorCode[ '400456' ];
          throw error;
        }
        else
        {
          fsm.OPEN();
          var pResults = Promise.all(promiseArray);
          return pResults;
        }
      }
      else if(req.body.state)
      {
        switch(genUtils.enumtostr(enums.incidentStates, req.body.state))
        {
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.OPEN):
            return moveToOpen();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.ASSIGNED):
            return moveToAssigned();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.REJECT):
            return moveToReject();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.WORKINPROGRESS):
            return moveToWORKINPROGRESS();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.RESOLVED):
            return moveToRESOVLED();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.UNRESOLVED):
            return moveToUNRESOLVED();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.CLOSED):
            return moveToCLOSED();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.PENDINGINC):
            return moveToPENDINGINC();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.PENDINGCUSTOMER):
            return moveToPENDINGCUSTOMER();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.PENDINGVENDOR):
            return moveToPENDINGVENDOR();
          case genUtils.enumtostr(enums.incidentStates, enums.incidentStates.PENDINGOTHER):
            return moveToPENDINGOTHER();
          default:
            return incident;
        }
      }
      else
      {
        return incident;
      }
    })
    .then(function(results)
    {
      if((currentIncident.state === enums.incidentStates.RESOLVED || currentIncident.state === enums.incidentStates.UNRESOLVED) && (req.body.assigneduserid || req.body.startedat || req.body.endedat || req.body.severity || req.body.impact || req.body.vendorid || req.body.vendorsr || req.body.pendinginc) && !req.body.state)
      {
        var error = new Error();
        error.code = 400476;
        error.message = errorCode[ '400476' ];
        throw error;
      }
      if(req.body.assignedqueueid && req.body.assignedqueueid !== currentIncident.assignedqueueid)
      {
        if(typeof req.body.assigneduserid !== 'undefined')
        {
          var assignMStatus = results[ 1 ];
          if(!results[ 0 ][ 0 ])
          {
            var error = new Error();
            error.code = 400254;
            error.message = errorCode[ '400254' ];
            throw error;
          }
          if(!results[ 0 ][ 0 ].isactive)
          {
            var error = new Error();
            error.code = 400253;
            error.message = errorCode[ '400253' ];
            throw error;
          }
          else if(!assignMStatus)
          {
            var error = new Error();
            error.code = 400258;
            error.message = errorCode[ '400258' ];
            throw error;
          }
          else
          {
            incidentUpdObj.assignedqueueid = req.body.assignedqueueid;
            incidentUpdObj.assigneduserid = req.body.assigneduserid;
          }
        }
        else
        {
          if(!results[ 0 ][ 0 ])
          {
            var error = new Error();
            error.code = 400254;
            error.message = errorCode[ '400254' ];
            throw error;
          }
          if(!results[ 0 ][ 0 ].isactive)
          {
            var error = new Error();
            error.code = 400253;
            error.message = errorCode[ '400253' ];
            throw error;
          }
          else
          {
            incidentUpdObj.assignedqueueid = req.body.assignedqueueid;
            incidentUpdObj.assigneduserid = null;
          }
        }
        incidentUpdObj.state = enums.incidentStates.OPEN;
        incidentUpdObj.visibility = 'operator';
      }
      else if(req.body.state)
      {
        if(typeof req.body.assigneduserid !== 'undefined')
        {
          var assignMStatus = results[ 0 ];
          if(!assignMStatus)
          {
            var error = new Error();
            error.code = 400258;
            error.message = errorCode[ '400258' ];
            throw error;
          }
          else
          {
            incidentUpdObj.assigneduserid = req.body.assigneduserid;
            incidentUpdObj.state = req.body.state;
          }
        }
        if(req.body.state === enums.incidentStates.REJECT)
        {
          incidentUpdObj.state = req.body.state;
          incidentUpdObj.assignedqueueid = currentIncident.ownerqueueid;
          incidentUpdObj.assigneduserid = currentIncident.owneruserid;
        }
        else
        {
          incidentUpdObj.state = req.body.state;
        }
        if(req.body.state === enums.incidentStates.RESOLVED || req.body.state === enums.incidentStates.REJECT) {incidentUpdObj.visibility = 'owner';}
        if(req.body.state === enums.incidentStates.RESOLVED && req.body.resolutioncode === enums.incidentResCodes.DUPTICKET)
        {
          // Check if the duplicate incident id is valid
          if(!results[ 0 ])
          {
            var error = new Error();
            error.code = 400454;
            error.message = errorCode[ '400454' ];
            throw error;
          }
          else
          {
            incidentUpdObj.dupticket = req.body.dupticket;
          }
        }
        if(req.body.state === enums.incidentStates.UNRESOLVED) {incidentUpdObj.visibility = 'operator';}
        if(req.body.state === enums.incidentStates.PENDINGINC)
        {
          if(!results[ 0 ])
          {
            var error = new Error();
            error.code = 400454;
            error.message = errorCode[ '400454' ];
            throw error;
          }
          else
          {
            incidentUpdObj.pendinginc = req.body.pendinginc;
          }
        }
        if(req.body.state === enums.incidentStates.PENDINGVENDOR)
        {
          if(!results[ 0 ])
          {
            var error = new Error();
            error.code = 400332;
            error.message = errorCode[ '400332' ];
            throw error;
          }
          else
          {
            incidentUpdObj.vendorid = req.body.vendorid;
            if(req.body.vendorsr) {incidentUpdObj.vendorsr = req.body.vendorsr;}
          }
        }
      }
      if(req.body.severity)
      {
        if(req.body.severity >= enums.incidentSeverity.HIGH && req.body.severity <= enums.incidentSeverity.LOW)
        {
          incidentUpdObj.severity = req.body.severity;
        }
        else
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ '400051' ];
          throw error;
        }
      }
      if(req.body.impact)
      {
        if(req.body.impact >= enums.incidentImpact.ENTERPRISE && req.body.impact <= enums.incidentImpact.SINGLEUSER)
        {
          incidentUpdObj.impact = req.body.impact;
        }
        else
        {
          var error = new Error();
          error.code = 400051;
          error.message = errorCode[ '400051' ];
          throw error;
        }
      }
      promiseArray = [];
      if(req.body.owneruserid)
      {
          var pIsMember = queueUtils.isMember(currentIncident.ownerqueueid, req.body.owneruserid);
          var pValidRequester = queueUtils.isMember(currentIncident.ownerqueueid, req.body.requesterid);
          promiseArray.push(pIsMember, pValidRequester);
      }
      if(typeof req.body.assigneduserid !== 'undefined' && !req.body.assignedqueueid)
      {
        if(req.body.assigneduserid === currentIncident.assigneduserid)
        {
          var error = new Error();
          error.code = 400468;
          error.message = errorCode[ '400468' ];
          throw error;
        }
        else
        {
          if(req.body.assigneduserid !== '')
          {
            var pIsMember = queueUtils.isMember(currentIncident.assignedqueueid, req.body.assigneduserid);
            promiseArray.push(pIsMember);
          }
        }
      }
      if(req.body.owneruserid || req.body.assigneduserid)
      {
        var pResults = Promise.all(promiseArray);
        return pResults;
      }
      else
      {
        return incidentUpdObj;
      }
    })
    .then(function(results)
    {
      var seek = 0;
      if(req.body.owneruserid)
      {
        var ownerMStatus = results[ 0 ];
        var requesterStatus = results[ 1 ];
        if(!ownerMStatus)
        {
          var error = new Error();
          error.code = 400258;
          error.message = errorCode[ '400258' ];
          throw error;
        }
        else if(!requesterStatus)
        {
          var error = new Error();
          error.code = 400474;
          error.message = errorCode[ '400474' ];
          throw error;
        }
        else
        {
          incidentUpdObj.owneruserid = req.body.owneruserid;
          seek = seek + 1;
        }
      }
      if(typeof req.body.assigneduserid !== 'undefined' && !req.body.assignedqueueid)
      {
        if(req.body.assigneduserid === '')
        {
          incidentUpdObj.assigneduserid = null;
        }
        else
        {
          var assigneeMStatus = results[ seek ];
          if(!assigneeMStatus)
          {
            var error = new Error();
            error.code = 400258;
            error.message = errorCode[ '400258' ];
            throw error;
          }
          else
          {
            incidentUpdObj.assigneduserid = req.body.assigneduserid;
          }
        }
        if(!incidentUpdObj.journal)
        {
          incidentUpdObj.journal = currentIncident.journal;
        }
        if(!req.body.state)
        {
          var journalEntry =
          {
            type: enums.incJournalTypes.ASSIGNED_TO_OPERATOR,
            time: Math.floor(new Date() / 1000),
            operatorid: req.body.requesterid,
            currentassigneduserid: currentIncident.assigneduserid,
            newassigneduserid: req.body.assigneduserid
          };
          incidentUpdObj.journal.push(journalEntry);
        }
      }
      promiseArray = [];
      if(req.body.attachments)
      {
        if(req.body.attachments.length !== _.uniq(req.body.attachments).length)
        {
          var error = new Error();
          error.code = 400406;
          error.message = errorCode[ '400406' ];
          throw error;
        }
        var pAttachments = Attachment.find({ attachmentid: req.body.attachments });
        promiseArray.push(pAttachments);
      }
      if(req.body.ciid !== undefined)
      {
        if(currentIncident.state > 4 && currentIncident.state < 7)
        {
          var error = new Error();
          error.code = 400476;
          error.message = errorCode[ '400476' ];
          throw error;
        }
        else
        {
          var pRunsOn = serviceUtils.runsOn(currentIncident.serviceid, req.body.ciid);
          var pCiDetails = Cidetail.findOne({ select: [ 'cistateid', 'ciname', 'ipaddress' ] } ).where( { ciid: req.body.ciid } );
          promiseArray.push(pRunsOn, pCiDetails);
        }
      }
      if(promiseArray.length)
      {
        return Promise.all(promiseArray);
      }
      else
      {
        return incidentUpdObj;
      }
    })
    .then(function(result)
    {
      if(req.body.startedat && req.body.endedat)
      {
        if(req.body.startedat > req.body.endedat)
        {
          var error = new Error();
          error.code = 400469;
          error.message = errorCode[ '400469' ];
          throw error;
        }
        else
        {
          incidentUpdObj.startedat = req.body.startedat;
          incidentUpdObj.endedat = req.body.endedat;
        }
      }
      else
      {
        if(req.body.startedat)
        {
          if(currentIncident.endedat && (currentIncident.endedat < req.body.startedat))
          {
            var error = new Error();
            error.code = 400469;
            error.message = errorCode[ '400469' ];
            throw error;
          }
          incidentUpdObj.startedat = req.body.startedat;
        }
        if(req.body.endedat)
        {
          if(currentIncident.startedat > req.body.endedat)
          {
            var error = new Error();
            error.code = 400469;
            error.message = errorCode[ '400469' ];
            throw error;
          }
          incidentUpdObj.endedat = req.body.endedat;
        }
      }
      var seek = 0;
      if(result.incidentid)
      {
        return Incident.update({ incidentid: req.body.incidentid }, incidentUpdObj);
      }
      if(req.body.attachments)
      {
        seek = seek + 1;
        if(result[ 0 ].length !== req.body.attachments.length)
        {
          var error = new Error();
          error.code = 400405;
          error.message = errorCode[ '400405' ];
          throw error;
        }
        else
        {
          incidentUpdObj.attachments = req.body.attachments;
        }
      }
      if(req.body.ciid !== undefined)
      {
        if(!result[ seek ])
        {
          var error = new Error();
          error.code = 400389;
          error.message = errorCode[ '400389' ];
          throw error;
        }
        else if(result[ seek + 1 ].cistateid < enums.ciStateIds.APPROVED)
        {
          var error = new Error();
          error.code = 400387;
          error.message = errorCode[ '400387' ];
          throw error;
        }
        else
        {
          incidentUpdObj.ciid = req.body.ciid;
        }
      }
      return Incident.update({ incidentid: req.body.incidentid }, incidentUpdObj);
    })
    .then(function(incident)
    {
      if(process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'staging')
      {
        sails.log.info("Publishing");
        Incident.publishUpdate(incident[ 0 ].incidentid, incident[ 0 ]);
      }
      return res.ok(incident[ 0 ]);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @api {post} /incident/cancel Cancel
    @apiName cancelIncident
    @apiGroup Incident
    @apiDescription On input of all valid parameters this function will cancel an incident
    @apiPermission operator
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS
      I6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM
      SIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi
      OiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDYxMzc0LCJ
      leHAiOjE0Nzc0ODI5NzQsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.16VKw8l
      4qWn9_dgQ4VvPfNgMWgftBXVMESSMvnS4C7I" -H "Content-Type: application/json" -d
      '{
        "incidentid": 2,
        "journal" : "auto fixed"
      }' "http://localhost:1337/incident/cancel"
    @apiParam {Number} incidentid ID of the incident
    @apiParam {String} [journal] Comments to be entered in the journal
    @apiParamExample {json} Request-Example
      {
        "incidentid": 2,
        "journal" : "auto fixed"
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 403502
    @apiUse 400051
    @apiUse 400454
    @apiUse 400461
    @apiUse 400471
    @apiUse 400473
    @apiUse 403502
    @apiSuccess (200) {Object} incident Details of the added incident
    @apiSuccessExample {json} Success-Response
    {
      "incidentid": 2,
      "state": 7,
      "title": "This is the 2nd test inc",
      "description": "Description of the test incident ticket",
      "serviceid": 1,
      "ciid": null,
      "ownerqueueid": 1,
      "owneruserid": 2,
      "assignedqueueid": 3,
      "assigneduserid": null,
      "startedat": 1476788690,
      "endedat": null,
      "severity": 3,
      "impact": 4,
      "vendorid": null,
      "vendorsr": null,
      "pendinginc": null,
      "journal": [
        {
          "type": 1,
          "time": 1484226873,
          "operatorid": 27
        }
      ],
      "resolutioncode": 3,
      "resolutionnotes": "Cancelled",
      "closurecomments": "Cancelled",
      "visibility": null
    }
  */
  cancel: function(req, res)
  {
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    if(!req.body.incidentid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    else if(!req.body.journal)
    {
      var error = new Error();
      error.code = 400461;
      error.message = errorCode[ '400461' ];
      return res.negotiate(error);
    }

    var currentIncident = {};
    var incidentUpdObj = {};
    Incident.findOne({ incidentid: req.body.incidentid }).populate('alarmids')
    .then(function(incident)
    {
      if(!incident)
      {
        var error = new Error();
        error.code = 400454;
        error.message = errorCode[ '400454' ];
        throw error;
      }
      else if(incident.state > enums.incidentStates.WORKINPROGRESS && incident.state < enums.incidentStates.PENDINGINC)
      {
        var error = new Error();
        error.code = 400473;
        error.message = errorCode[ '400473' ];
        throw error;
      }
      else if(incident.alarmids.length)
      {
        var unackAlarms = false;
        incident.alarmids.every(function(alarm) {
          if(!alarm.isack)
          {
            unackAlarms = !unackAlarms;
            return false;
          }
          else
          {
            return true;
          }
        });
        if(unackAlarms)
        {
          var error = new Error();
          error.code = 400480;
          error.message = errorCode[ '400480' ];
          throw error;
        }
      }
      currentIncident = incident;
      return Promise.all([ queueUtils.isMember(currentIncident.ownerqueueid, req.body.requesterid) ]);
    })
    .then(function(result)
    {
      if(!result[ 0 ])
      {
        var error = new Error();
        error.code = 400471;
        error.message = errorCode[ '400471' ];
        throw error;
      }
      else
      {
        incidentUpdObj.journal = currentIncident.journal;
        var journalEntry1 =
        {
          type: enums.incJournalTypes.MANUAL_ENTRY,
          time: Math.floor(new Date() / 1000),
          operatorid: req.body.requesterid,
          message: req.body.journal
        };
        var journalEntry2 =
        {
          type: enums.incJournalTypes.INCIDENT_CANCELLED,
          time: Math.floor(new Date() / 1000),
          operatorid: req.body.requesterid
        };
        incidentUpdObj.journal.push(journalEntry1, journalEntry2);
        incidentUpdObj.resolutioncode = enums.incidentResCodes.CANCELLED;
        incidentUpdObj.resolutionnotes = "Cancelled";
        incidentUpdObj.closurecomments = "Cancelled";
        incidentUpdObj.state = enums.incidentStates.CLOSED;
        incidentUpdObj.visibility = null;
        return Incident.update({ incidentid: req.body.incidentid }, incidentUpdObj);
      }
    })
    .then(function(result)
    {
      if(result[ 0 ].incidentid && (process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'staging'))
      {
        Incident.publishUpdate(result[ 0 ].incidentid, result[ 0 ]);
      }
      return res.ok(result[ 0 ]);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  relatetickets: function(req, res)
  {
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    if(!req.body.incidentid || !req.body.tickets || !req.body.tickets.incidents || !Array.isArray(req.body.tickets.incidents) || !req.body.tickets.incidents.length || (req.body.tickets.incidents.indexOf(req.body.incidentid) >= 0))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var incArray = [];
    var assign = function(element, index)
    {
      incArray[ index ] = element;
    };
    req.body.tickets.incidents.forEach(assign);
    incArray.push(req.body.incidentid);
    incArray = _.uniq(incArray);
    Incident.find({ incidentid: incArray })
    .then(function(incidents)
    {
      if(!incidents || (incidents.length !== incArray.length))
      {
        var error = new Error();
        error.code = 400454;
        error.message = errorCode[ '400454' ];
        throw error;
      }
      else
      {
        var incidentUpdObj = {};
//        IncidentUpdObj.relatedinc = _.uniq(req.body.tickets.incidents);
        var incId = function(inc) {
          return inc.incidentid === req.body.incidentid;
        };
        var incident = incidents.filter(incId);
//        IncidentUpdObj.journal = incident[ 0 ].journal;
//        var journalEntry =
//        {
//          type: enums.incJournalTypes.TICKET_RELATED,
//          time: Math.floor(new Date() / 1000),
//          operatorid: req.body.requesterid,
//          relatedinc: incidentUpdObj.relatedinc
//        };
//        incidentUpdObj.journal.push(journalEntry);
        //IncidentUpdObj.journal.push(JSON.parse('{"time" :' + Math.floor(new Date() / 1000) + ', "userid" : ' + req.body.requesterid + ', "source" : ' + enums.journalSources.AUTO + ', "message" : "The ticket has been related to incidents: ' +  incidentUpdObj.relatedinc  + '"}'));
        return Incident.update({ incidentid: req.body.incidentid }, incidentUpdObj);
      }
    })
    .then(function(result)
    {
      if(process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'staging')
      {
        Incident.publishUpdate(result[ 0 ].incidentid, result[ 0 ]);
      }
      return res.ok(result[ 0 ]);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @api {post} /incident/getdetail GetDetail
    @apiName getDetail
    @apiGroup Incident
    @apiDescription On input of all valid parameters this function will receive details of the an incident
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS
      6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxMS
      IsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIiO
      iI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDYxMzc0LCJl
      eHAiOjE0Nzc0ODI5NzQsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.16VKw8l4
      qWn9_dgQ4VvPfNgMWgftBXVMESSMvnS4C7I" -H "Content-Type: application/json" -d
      '{
        "incidentid" : 1
      }' "http://localhost:1337/incident/getdetail"
    @apiParam {Number} incidentid ID of the incident
    @apiParamExample {json} Request-Example
      {
        "incidentid" : 1
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 400454
    @apiSuccess (200) {Object} incident Details of the modified incident
    @apiSuccessExample {json} Success-Response
    {
      "attachments": [],
      "relatedinc": [],
      "serviceid": {
        "serviceid": 1,
        "servicename": "Cinderella Spanking Workshop",
        "servicestateid": 2,
        "details": "The castle has much more to offer.",
        "isfrozen": false,
        "freezecause": 0,
        "cbid": 6,
        "bdid": 1
      },
      "ownerqueueid": {
        "queueid": 1,
        "queuename": "TestQueue1",
        "queueemail": "testqueue1@test.com",
        "alertl1": "testqueue1alert1@test.com",
        "alertl2": "testqueue1alert2@test.com",
        "alertl3": "testqueue1alert3@test.com",
        "isactive": true,
        "isfrozen": false,
        "freezecause": null,
        "cbid": 5,
        "queueownerid": 2,
        "incidentmanagerid": 2,
        "changemanagerid": 2,
        "srtmanagerid": 2
      },
      "owneruserid": {
        "userid": 2,
        "firstname": "Test",
        "lastname": "User",
        "username": "T3st.User11",
        "emailid": "test.user11@testclient1.com",
        "countrycode": "+1",
        "phonenumber": "9999999999",
        "employeeid": "FTE000002",
        "isactive": true,
        "defaultqueue": 4
      },
      "assignedqueueid": {
        "queueid": 3,
        "queuename": "TestQueue3",
        "queueemail": "testqueue3@test.com",
        "alertl1": "testqueue3alert1@test.com",
        "alertl2": "testqueue3alert2@test.com",
        "alertl3": "testqueue3alert3@test.com",
        "isactive": true,
        "isfrozen": false,
        "freezecause": 0,
        "cbid": 6,
        "queueownerid": 2,
        "incidentmanagerid": 2,
        "changemanagerid": 2,
        "srtmanagerid": 2
      },
      "pendinginc": {
        "incidentid": 7,
        "state": 2,
        "title": "This is the 1st test inc",
        "description": "Description of the test incident ticket",
        "startedat": 1476788690,
        "endedat": null,
        "severity": 3,
        "impact": 4,
        "vendorsr": null,
        "journal": [
          {
            "type": 1,
            "time": 1484226873,
            "operatorid": 27
          }
        ],
        "resolutioncode": null,
        "resolutionnotes": null,
        "closurecomments": null,
        "visibility": "operator",
        "serviceid": 1,
        "ciid": null,
        "ownerqueueid": 1,
        "owneruserid": 2,
        "assignedqueueid": 3,
        "assigneduserid": null,
        "vendorid": null,
        "pendinginc": 7
      },
      "incidentid": 1,
      "state": 7,
      "title": "This is the 1st test inc",
      "description": "Description of the test incident ticket",
      "startedat": 1476788690,
      "endedat": null,
      "severity": 3,
      "impact": 4,
      "vendorsr": null,
      "journal": [
        {
          "type": 1,
          "time": 1484226873,
          "operatorid": 27
        }
      ],
      "resolutioncode": 3,
      "resolutionnotes": "Cancelled",
      "closurecomments": "Cancelled",
      "visibility": null
    }
  */
  getdetail: function(req, res)
  {
    if(!req.body.incidentid || isNaN(req.body.incidentid))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    Incident.findOne({ incidentid: req.body.incidentid })
      .populate('serviceid')
      .populate('ciid')
      .populate('ownerqueueid')
      .populate('owneruserid')
      .populate('assignedqueueid')
      .populate('assigneduserid')
      .populate('attachments')
      .populate('vendorid')
      .populate('pendinginc')
//      .populate('relatedinc')
      .populate('dupticket')
      .populate('alarmids')
    .then(function(incident)
    {
      if(!incident)
      {
        var error = new Error();
        error.code = 400454;
        error.message = errorCode[ '400454' ];
        throw error;
      }
      else
      {
        return res.ok(incident);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @api {post} /incident/userlist UserList
    @apiName userList
    @apiGroup Incident
    @apiDescription On input of a valid userid this function will send all incidents currently requiring attention from the user
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS
      I6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM
      SIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi
      OiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDg4Mjc5LCJ
      leHAiOjE0Nzc1MDk4NzksImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.o5tOeaK
      0w5BihwC2jEa59RpTKcHF3J9lmJ43BUjxxH4" -H "Content-Type: application/json" -d
      '{
        "userid": 8
      }' "http://localhost:1337/incident/userlist"
    @apiParam {Number} userid ID of the user
    @apiParamExample {json} Request-Example
      {
        "userid": 8
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiSuccess (200) {Object[]} incidentList List of incidents
    @apiSuccessExample {json} Success-Response
      [
        {
          "serviceid": 1,
          "ciid": null,
          "ownerqueueid": 1,
          "owneruserid": 2,
          "assignedqueueid": 3,
          "assigneduserid": 8,
          "incidentid": 2,
          "title": "This is the 1st test inc",
          "state": 3,
          "startedat": 1476788690,
          "endedat": null,
          "severity": 3,
          "impact": 4,
          "updatedAt": "2016-10-28T12:55:24.000Z",
          "visibility": "operator"
        }
      ]
  */
  userlist: function(req, res)
  {
    if(!req.body.userid || isNaN(req.body.userid))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var incList = [];
    Incident.find({ select: [ 'incidentid', 'title', 'state', 'startedat', 'endedat', 'severity', 'serviceid', 'ciid', 'assignedqueueid', 'assigneduserid', 'createdAt', 'updatedAt', 'ownerqueueid', 'owneruserid', 'visibility' ], where: { state: { '!': [ enums.incidentStates.CLOSED ] } } })
    .then(function(incidents)
    {
      if(!incidents)
      {
        return incList;
      }
      else
      {
        var pickIncidents = function(element, index, array)
        {
          if(((element.visibility === 'operator') && (element.assigneduserid === req.body.userid)) || (element.visibility === 'owner') && (element.owneruserid === req.body.userid))
          {
            incList.push(element);
          }
        };
        incidents.forEach(pickIncidents);
        return res.ok(incList);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @api {post} /incident/queuelist QueueList
    @apiName queueList
    @apiGroup Incident
    @apiDescription On input of a valid queueid this function will send all incidents currently requiring attention from the queue
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS
      I6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM
      SIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi
      OiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDg4Mjc5LCJ
      leHAiOjE0Nzc1MDk4NzksImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.o5tOeaK
      0w5BihwC2jEa59RpTKcHF3J9lmJ43BUjxxH4" -H "Content-Type: application/json" -d
      '{
        "queueid": 3
      }' "http://localhost:1337/incident/queuelist"
    @apiParam {Number} queueid ID of the queue
    @apiParamExample {json} Request-Example
      {
        "queueid": 3
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiSuccess (200) {Object[]} incidentList List of incidents
    @apiSuccessExample {json} Success-Response
      [
        {
          "serviceid": 1,
          "ciid": null,
          "ownerqueueid": 1,
          "owneruserid": 2,
          "assignedqueueid": 3,
          "assigneduserid": null,
          "incidentid": 2,
          "title": "This is the 1st test inc",
          "state": 2,
          "startedat": 1476788690,
          "endedat": null,
          "severity": 3,
          "impact": 4,
          "updatedAt": "2016-10-28T08:03:06.000Z",
          "visibility": "operator"
        }
      ]
  */
  queueList: function(req, res)
  {
    if(!req.body.queueid || isNaN(req.body.queueid))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var incList = [];
    Incident.find({ select: [ 'incidentid', 'title', 'state', 'startedat', 'endedat', 'severity', 'serviceid', 'ciid', 'assignedqueueid', 'assigneduserid', 'createdAt', 'updatedAt', 'ownerqueueid', 'owneruserid', 'visibility' ], where: { state: { '!': [ enums.incidentStates.CLOSED ] } } })
    .then(function(incidents)
    {
      if(!incidents)
      {
        return incList;
      }
      else
      {
        var pickIncidents = function(element, index, array)
        {
          if(((element.visibility === 'operator') && (element.assignedqueueid === req.body.queueid)) || (element.visibility === 'owner') && (element.ownerqueueid === req.body.queueid))
          {
            incList.push(element);
          }
        };
        incidents.forEach(pickIncidents);
        return res.ok(incList);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
    @api {post} /incident/search Search
    @apiName incidentSearch
    @apiGroup Incident
    @apiDescription On input of a valid search parameters this function returns the selected attributes in the chosen sorting method
    @apiPermission user
    @apiHeader (Content-Type) {String} Content-Type application/json
    @apiHeader (Authorization) {String} Authorization unique access-key of the requester
    @apiExample {curl} Example Usage:
      curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS
      I6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM
      SIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi
      OiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDg4Mjc5LCJ
      leHAiOjE0Nzc1MDk4NzksImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.o5tOeaK
      0w5BihwC2jEa59RpTKcHF3J9lmJ43BUjxxH4" -H "Content-Type: application/json" -d
        '{
          "search":  { "state": 3 },
          "result" : [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid"],
          "orderby": "incidentid"
        }' "http://localhost:1337/incident/search"
    @apiParam {Object} search Object search criteria
    @apiParam {String[]} result Array of attributes requested
    @apiParam {String} orderby Attribute to order by the results
    @apiParamExample {json} Example-Simple
      {
        "search":  { "state": 3 },
        "result" : [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid"],
        "orderby": "incidentid"
      }
    @apiParamExample {json} Example-Before startedat
      {
        "search":  {"startedat": { "<=": 1489740150 }},
        "result" : ["incidentid", "serviceid", "startedat", "endedat", "resolvedat"],
        "orderby": "incidentid"
      }
    @apiParamExample {json} Example-After endedat
      {
        "search":  {"endedat": { ">=": 1489740000 }},
        "result" : ["incidentid", "serviceid", "startedat", "endedat", "resolvedat"],
        "orderby": "incidentid"
      }
    @apiParamExample {json} Example-Between startime and endedat
      {
        "search":  {"startedat": { ">=": 1489501000}, "endedat": { "<=": 1489740200 }},
        "result" : ["incidentid", "serviceid", "startedat", "endedat", "resolvedat"],
        "orderby": "incidentid"
      }
    @apiParamExample {json} Example-Between startime range
      {
        "search":  {"startedat": { ">=": 1489749058, "<=": 1489749070 }},
        "result" : ["incidentid", "serviceid", "startedat", "endedat", "resolvedat"],
        "orderby": "incidentid"
      }
    @apiUse 401
    @apiUse 401505
    @apiUse 400051
    @apiUse 400053
    @apiSuccess (200) {Object[]} incidentList List of incidents
    @apiSuccessExample {json} Success-Response
      [
        1,
        [
          {
            "assignedqueueid": 3,
            "assigneduserid": 8,
            "incidentid": 1,
            "title": "This is the 1st test inc",
            "state": 3
          }
        ]
      ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Incident.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });

    Incident.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.ok([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  },
  addbytemplate: function(req, res)
  {
    let self = this;
    if(!req.body.alertticket && !req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    else if(!req.body.templateid)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var tempObj = {};
    Inctemplate.findOne({ templateid: req.body.templateid })
    .then(function(template)
    {
      if(!template)
      {
        var error = new Error();
        error.code = 400501;
        error.message = errorCode[ '400501' ];
        throw error;
      }
      tempObj = template;
      var keys = Object.keys(template);
      for(var i = 0; i < keys.length; i++)
      {
        if(!req.body[ keys[ i ] ])
        {
          req.body[ keys[ i ] ] = template[ keys[ i ] ];
        }
      }
      var pService = Service.findOne({ servicename: template.servicename }, { select: [ 'serviceid', 'supportqueue' ] });
      var pQueues = Queue.find({ queuename: [ template.ownerqueuename, template.assignedqueuename ] });
      var pUsers = User.find({ username: [ template.ownerusername, template.assignedusername ] });
      var pCi = Cidetail.findOne({ ciname: template.ciname }, { select: [ 'ciid' ] });
      return Promise.all([ pService, pQueues, pUsers, pCi ]);
    })
    .then(function(results)
    {
      if(!results[ 0 ] || !results[ 1 ][ 0 ] || !results[ 1 ][ 1 ] || !results[ 2 ][ 0 ] || (tempObj.assignedusername && !results[ 2 ][ 1 ]) || (tempObj.ciname && !results[ 3 ]))
      {
        var error = new Error();
        error.code = 400502;
        error.message = errorCode[ '400502' ];
        throw error;
      }
      else
      {
        req.body.serviceid = results[ 0 ].serviceid;
        req.body.ownerqueueid = _.find(results[ 1 ], _.matchesProperty('queuename', tempObj.ownerqueuename)).queueid;
        req.body.assignedqueueid = _.find(results[ 1 ], _.matchesProperty('queuename', tempObj.assignedqueuename)).queueid;
        req.body.owneruserid = _.find(results[ 2 ], _.matchesProperty('username', tempObj.ownerusername)).userid;
        if(tempObj.assignedusername)
        {
          req.body.assigneduserid = _.find(results[ 2 ], _.matchesProperty('username', tempObj.assignedusername)).userid;
        }
        if(tempObj.ciname)
        {
          req.body.ciid = results[ 3 ].ciid;
        }
        if(!req.body.startedat)
        {
          req.body.startedat = Math.floor(new Date() / 1000);
        }
        self.add(req, res);
      }
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  assocalarms: function(req, res)
  {
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      error.message = errorCode[ '403502' ];
      return res.negotiate(error);
    }
    if(!req.body.incidentid || !req.body.alarmids || !Array.isArray(req.body.alarmids))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ '400051' ];
      return res.negotiate(error);
    }
    var uniqIds = _.uniq(req.body.alarmids);
    var pAlarms = Alarm.find({ alarmids: uniqIds });
    var pInc = Incident.findOne({ incidentid: req.body.incident }).populate('alarmids');
    Promise.all([ pAlarms, pInc ])
    .then(function(results)
    {
      if(!results[ 0 ].length)
      {
        var error = new Error();
        error.code = 400631;
        error.message = errorCode[ '400631' ];
        throw error;
      }
      else if(results[ 0 ].length !== uniqIds.length)
      {
        var error = new Error();
        error.code = 400631;
        error.message = errorCode[ '400631' ];
        throw error;
      }
      else if(!results[ 1 ])
      {
        var error = new Error();
        error.code = 400454;
        error.message = errorCode[ '400454' ];
        throw error;
      }
      else
      {
        var currentAlarmIds = _.map(results[ 1 ].alarmids, 'id');
        var pIncUpdate = Incident.update({ incidentid: results[ 1 ].incidentid }, { alarmids: _.concat(currentAlarmIds, uniqIds) });
        var pAlarmUpdAdd = Alarm.update({ id: uniqIds }, { incidentid: results[ 1 ].incidentid });
        var pArray = [ pIncUpdate, pAlarmUpdAdd ];
        if(_.difference(currentAlarmIds, uniqIds).length)
        {
          var pAlarmUpdRmv = Alarm.update({ id: _.difference(currentAlarmIds, uniqIds) }, { incidentid: null });
          pArray.push(pAlarmUpdRmv);
        }
        return Promise.all(pArray);
      }
    })
    .then(function(results)
    {
      return res.json(results[ 0 ]);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
