/* FileName: SubscribeController.js
*  @description: Server-side logic for managing the event subscriptions
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  22/05/2017        Initial file creation                         bones
*  27/05/2017        Additional APIs                               SMandal
*
*/
var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");

module.exports =
{
  subscribetoalarms: function(req, res)
  {
    if (!req.isSocket)
    {
      var error = new Error();
      error.code = 400600;
      error.message = errorCode[ '400600' ];
      return res.negotiate(error);
    }
    Alarm.watch(req);
    Alarm.find({ isack: false })
    .then(function(alarms)
    {
      var ids = [];
      alarms.forEach(function(alarms)
      {
        ids.push(alarms.id);
      });
      Alarm.subscribe(req, ids);
      return res.json(alarms);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  subscribetoincidents: function(req, res)
  {
    if (!req.isSocket)
    {
      var error = new Error();
      error.code = 400600;
      error.message = errorCode[ '400600' ];
      return res.negotiate(error);
    }
    Incident.watch(req);
    Incident.find({ select: [ 'incidentid', 'title', 'state', 'startedat', 'endedat', 'severity', 'serviceid', 'ciid', 'assignedqueueid', 'assigneduserid', 'createdAt', 'updatedAt', 'ownerqueueid', 'owneruserid', 'visibility' ], where: { state: { '!': [ enums.incidentStates.CLOSED, enums.incidentStates.RESOLVED ] } } })
    .then(function(incidents)
    {
      var ids = [];
      incidents.forEach(function(incident)
      {
        ids.push(incident.incidentid);
      });
      Incident.subscribe(req, ids);
      return res.json(incidents);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  }
};
