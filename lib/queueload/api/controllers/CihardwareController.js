/**
 * CihardwareController
 *
 * @description :: Server-side logic for managing cihardwares
 *
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 29/03/2016    Initial file creation                         SMandal
 */

/**
 * @apiGroup Hardware
 */

var errorCode = require("../resources/ErrorCodes.js");
var _ = require("lodash");
var Promise = require("bluebird");

module.exports = {
  /**
  @apiGroup Hardware
  @apiName listHardware
  @api {post} /cihardware/list List
  @apiDescription On input of all valid parameters this function will list the hardware assets
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml
     yc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R
     1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l
     lZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNTU3OTk0LCJleHAiOjE0NzI1Nzk1OTQsImF1ZCI
     6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.M4X6sDthnrHlIyoztUDoOB37OOSAg4EpZMhr
     nAQUxYc" -H "Content-Type: application/json" -d '{"page":1,"limit": 10}' "http://localhost:1337/cihardware/list"
  @apiParam {Number} limit Maximum number of results to show in a page
  @apiParam {Number} page The number of page to show
  @apiParamExample {json} Request-Example
     { "limit": 10, "page": 1}
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400351
  @apiSuccess (200) {Object[]} hardwares Array of Hardwares
  @apiSuccessExample {json} Success-Response
    [{
      "manufacturervendorid": 1,
      "osvendorid": null,
      "biosvendorid": null,
      "machineid": 1,
      "hardwaretag": "TGY47IK829",
      "hwmodel": "HF-482R",
      "serialnumber": null,
      "rack": null,
      "rackunit": null,
      "gridloc": "AF-YH5",
      "slot": null,
      "macid": null,
      "osname": null,
      "osversion": null,
      "biosserial": null,
      "biosmodel": null,
      "phymemmb": null,
      "psuwattage": null,
      "partnum": "IYU3M14O4Q",
      "createdAt": "2016-08-30T13:25:50.000Z",
      "updatedAt": "2016-08-30T13:25:50.000Z"
    }]
  */
  list: function(req, res)
  {
    if((!req.body.page || !(_.isFinite(req.body.page))) ||
    (!req.body.limit || !(_.isFinite(req.body.limit))))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }

    var pageNumber = req.body.page;
    var limitNumber = req.body.limit;

    var output = [];
    Cihardware.find().paginate({ page: pageNumber, limit: limitNumber })
    .then(function showRecs(found)
    {
      if(!found.length)
      {
        var error = new Error();
        error.code = 400351;
        error.message = errorCode[ "400351" ];
        throw error;
      }
      else
      {
        output = found;
        var countCis = function(element)
        {
          return Cidetail.count().where({ machineid: element.machineid });
        };
        var actions = found.map(countCis);
        return Promise.all(actions);
      }
    })
    .then(function(result)
    {
      var assign = function(element, index)
      {
        output[ index ].numberofcis = element;
      };
      result.forEach(assign);
      return res.ok(output);
    })
    .catch(function(error)
    {
      return res.negotiate(error);
    });
  },
  /**
  @apiGroup Hardware
  @apiName addHardware
  @api {post} /cihardware/add Add
  @apiDescription On input of all valid parameters this function will add Ci hardware to the system
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYE
     Jdx6oNW4" -H "Content-Type: application/json"
      -d '{
        "hardwaretag" : "TGY47IK829",
        "manufacturervendorid" : 3,
        "hwmodel" : "HF-482R",
        "gridloc" : "AF-YH5",
        "partnum" : "IYU3M14O4Q"
        }' "http://localhost:1337/cihardware/add"
  @apiParam {String} hardwaretag Unique Tag ID of the hardware asset
  @apiParam {Number} manufacturervendorid ID of the Vendor
  @apiParam {String} hwmodel Model of the asset
  @apiParam {String} partnum  Unique partnumber for the asset
  @apiParam {String} [serialnumber] Unique serial number of the asset
  @apiParam {String} [rack] Rack# of the asset
  @apiParam {Number} [rackunit] Form factor of the hardware in RUs
  @apiParam {String} gridloc Grid Location of the hardware
  @apiParam {String} [slot] Slot number of the asset
  @apiParam {String} [macid] MAC Address (Valid delimiters between octets are : and -)
  @apiParam {String} [osname] Operating System
  @apiParam {Number} [osvendorid] Vendor ID of the operating system
  @apiParam {String} [osversion] Release version of the operating system
  @apiParam {String} [biosserial] Serial Number of the BIOS
  @apiParam {Number} [biosvendorid] Vendor ID of the BIOS manufacturer
  @apiParam {String} [biosmodel] Model of the BIOS
  @apiParam {Number} [phymemmb] Physical Memory in MB
  @apiParam {Number} [psuwattage] Power Consumption of PSU in Watts
  @apiParamExample {json} Request-Example
    {
      "hardwaretag": "TGY47IK829",
      "manufacturervendorid": 3,
      "hwmodel": "HF-482R",
      "gridloc": "AF-YH5",
      "partnum": "IYU3M14O4Q"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400332
  @apiUse 403502
  @apiSuccess (200) {Object} hardware Added hardware
  @apiSuccessExample {json} Success-Response
    {
      "machineid": 1,
      "hardwaretag": "TGY47IK829",
      "manufacturervendorid": 1,
      "hwmodel": "HF-482R",
      "serialnumber": null,
      "rack": null,
      "rackunit": null,
      "gridloc": "AF-YH5",
      "slot": null,
      "macid": null,
      "osname": null,
      "osvendorid": null,
      "osversion": null,
      "biosserial": null,
      "biosvendorid": null,
      "biosmodel": null,
      "phymemmb": null,
      "psuwattage": null,
      "partnum": "IYU3M14O4Q",
      "createdAt": "2016-08-30T13:25:50.000Z",
      "updatedAt": "2016-08-30T13:25:50.000Z"
    }
  */
  add: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    //If any of the mandatory attributes are missing give an error
    if(!req.body.hardwaretag || !req.body.manufacturervendorid || !(_.isFinite(req.body.manufacturervendorid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var vendors = [ req.body.manufacturervendorid ];
      if(req.body.osvendorid)
      {
        vendors.push(req.body.osvendorid);
      }
      if(req.body.biosvendorid)
      {
        vendors.push(req.body.biosvendorid);
      }
      vendors = _.uniq(vendors);
      Vendor.find({ vendorid: vendors })
      .then(function showRecs(records)
      {
        // Check if the input has any non-existent vendors
        if(records.length === vendors.length)
        {
          return records;
        }
        else
        {
          var error = new Error();
          error.code = 400332;
          error.message = errorCode[ "400332" ];
          throw error;
        }
      })
      .then(function(records)
      {
        var newHw =
        {
          hardwaretag: req.body.hardwaretag,
          manufacturervendorid: req.body.manufacturervendorid,
          hwmodel: req.body.hwmodel,
          gridloc: req.body.gridloc,
          partnum: req.body.partnum
        };
        if(req.body.rack)
        {
          newHw.rack = req.body.rack;
        }
        if(req.body.rackunit)
        {
          newHw.rackunit = req.body.rackunit;
        }
        if(req.body.slot)
        {
          newHw.slot = req.body.slot;
        }
        if(req.body.macid)
        {
          newHw.macid = req.body.macid;
        }
        if(req.body.osname)
        {
          newHw.osname = req.body.osname;
        }
        if(req.body.osvendorid)
        {
          newHw.osvendorid = req.body.osvendorid;
        }
        if(req.body.osversion)
        {
          newHw.osversion = req.body.osversion;
        }
        if(req.body.biosserial)
        {
          newHw.biosserial = req.body.biosserial;
        }
        if(req.body.biosvendorid)
        {
          newHw.biosvendorid = req.body.biosvendorid;
        }
        if(req.body.biosmodel)
        {
          newHw.biosmodel = req.body.biosmodel;
        }
        if(req.body.phymemmb)
        {
          newHw.phymemmb = req.body.phymemmb;
        }
        if(req.body.psuwattage)
        {
          newHw.psuwattage = req.body.psuwattage;
        }
        if(req.body.serialnumber)
        {
          newHw.serialnumber = req.body.serialnumber;
        }
        return Cihardware.create(newHw);
      })
      .then(function createdHardware(hardware)
      {
        return res.json(hardware);
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Hardware
  @apiName getDetail
  @api {post} /cihardware/getdetail Getdetail
  @apiDescription On input of a machineid it retrieves the corresponding Cihardware details
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml
     yc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R
     1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l
     lZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNTU3OTk0LCJleHAiOjE0NzI1Nzk1OTQsImF1ZCI
     6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.M4X6sDthnrHlIyoztUDoOB37OOSAg4EpZMhr
     nAQUxYc" -H "Content-Type: application/json" -d '{"machineid": 1}' "http://localhost:1337/cihardware/getdetail"
  @apiParam {Number} machineid ID of the machine
  @apiParamExample {json} Request-Example
    {
     "machineid": 1
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400352
  @apiUse 403502
  @apiSuccess (200) {Object} hardware Details of the requested hardware
  @apiSuccessExample {json} Success-Response
  {
    "manufacturervendorid": {
      "vendorid": 1,
      "vendorname": "Cyberdyne Systems"
    },
    "osvendorid": null,
    "biosvendorid": null,
    "machineid": 1,
    "hardwaretag": "TGY47IK829",
    "hwmodel": "HF-482R",
    "serialnumber": null,
    "rack": null,
    "rackunit": null,
    "gridloc": "AF-YH5",
    "slot": null,
    "macid": null,
    "osname": null,
    "osversion": null,
    "biosserial": null,
    "biosmodel": null,
    "phymemmb": null,
    "psuwattage": null,
    "partnum": "IYU3M14O4Q",
    "createdAt": "2016-08-30T13:25:50.000Z",
    "updatedAt": "2016-08-30T13:25:50.000Z"
  }
  */
  getdetail: function(req, res)
  {
    //If any of the mandatory attributes are missing give an error
    if(!req.body.machineid || !(_.isFinite(req.body.machineid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      Cihardware.findOne({ machineid: req.body.machineid })
      .populate("manufacturervendorid")
      .then(function showRecs(record)
      {
        // Check if the requested hardware exists
        if(!record)
        {
          var error = new Error();
          error.code = 400352;
          error.message = errorCode[ "400352" ];
          throw error;
        }
        else
        {
          return res.json(record);
        }
      })
      .catch(function(error)
      {
        return res.negotiate(error);
      });
    }
  },
  /**
  @apiGroup Hardware
  @apiName update
  @api {post} /cihardware/update Update
  @apiDescription On input of a machineid it updates a Hardware
  @apiPermission operator
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYE
     Jdx6oNW4" -H "Content-Type: application/json" -d '
      {
        "machineid" : 1,
        "gridloc" : "BK-LR9"
      }'
      "http://localhost:1337/cihardware/update"
  @apiParam {Number} machineid ID of the machine
  @apiParam {String} [hardwaretag] Unique Tag ID of the hardware asset
  @apiParam {Number} [manufacturervendorid] ID of the Vendor
  @apiParam {String} [hwmodel] Model of the asset
  @apiParam {String} [partnum]  Unique partnumber for the asset
  @apiParam {String} [serialnumber] Unique serial number of the asset
  @apiParam {String} [rack] Rack# of the asset
  @apiParam {Number} [rackunit] Form factor of the hardware in RUs
  @apiParam {String} [gridloc] Grid Location of the hardware
  @apiParam {String} [slot] Slot number of the asset
  @apiParam {String} [macid] MAC Address (Valid delimiters between octets are : and -)
  @apiParam {String} [osname] Operating System
  @apiParam {Number} [osvendorid] Vendor ID of the operating system
  @apiParam {String} [osversion] Release version of the operating system
  @apiParam {String} [biosserial] Serial Number of the BIOS
  @apiParam {Number} [biosvendorid] Vendor ID of the BIOS manufacturer
  @apiParam {String} [biosmodel] Model of the BIOS
  @apiParam {Number} [phymemmb] Physical Memory in MB
  @apiParam {Number} [psuwattage] Power Consumption of PSU in Watts
  @apiParamExample {json} Request-Example
    {
      "machineid" : 1,
      "gridloc" : "BK-LR9"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400352
  @apiUse 403502
  @apiSuccess (200) {Object} hardware Details of the updated hardware
  @apiSuccessExample {json} Success-Response
  {
    "machineid": 1,
    "hardwaretag": "TGY47IK829",
    "manufacturervendorid": 1,
    "hwmodel": "HF-482R",
    "serialnumber": null,
    "rack": null,
    "rackunit": null,
    "gridloc": "BK-LR9",
    "slot": null,
    "macid": null,
    "osname": null,
    "osvendorid": null,
    "osversion": null,
    "biosserial": null,
    "biosvendorid": null,
    "biosmodel": null,
    "phymemmb": null,
    "psuwattage": null,
    "partnum": "IYU3M14O4Q",
    "createdAt": "2016-08-30T13:25:50.000Z",
    "updatedAt": "2016-08-30T14:06:57.000Z"
  }
  */
  update: function(req, res)
  {
    // The API is open only for operators
    if(!req.body.requesterroles.isoperator)
    {
      var error = new Error();
      error.code = 403502;
      return res.negotiate(error);
    }
    if(!req.body.machineid || !(_.isFinite(req.body.machineid)))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    else
    {
      var updatedHw = {};
      var vendors = [];
      if(req.body.manufacturervendorid)
      {
        vendors.push(req.body.manufacturervendorid);
      }
      if(req.body.osvendorid)
      {
        vendors.push(req.body.osvendorid);
      }
      if(req.body.biosvendorid)
      {
        vendors.push(req.body.biosvendorid);
      }
      if(req.body.hardwaretag)
      {
        updatedHw.hardwaretag = req.body.hardwaretag;
      }
      if(req.body.manufacturervendorid)
      {
        updatedHw.manufacturervendorid = req.body.manufacturervendorid;
      }
      if(req.body.hwmodel)
      {
        updatedHw.hwmodel = req.body.hwmodel;
      }
      if(req.body.gridloc)
      {
        updatedHw.gridloc = req.body.gridloc;
      }
      if(req.body.partnum)
      {
        updatedHw.partnum = req.body.partnum;
      }
      if(req.body.rackunit)
      {
        updatedHw.rackunit = req.body.rackunit;
      }
      if(req.body.slot)
      {
        updatedHw.slot = req.body.slot;
      }
      if(req.body.macid)
      {
        updatedHw.macid = req.body.macid;
      }
      if(req.body.osname)
      {
        updatedHw.osname = req.body.osname;
      }
      if(req.body.osvendorid)
      {
        updatedHw.osvendorid = req.body.osvendorid;
      }
      if(req.body.osversion)
      {
        updatedHw.osversion = req.body.osversion;
      }
      if(req.body.biosserial)
      {
        updatedHw.biosserial = req.body.biosserial;
      }
      if(req.body.biosvendorid)
      {
        updatedHw.biosvendorid = req.body.biosvendorid;
      }
      if(req.body.biosmodel)
      {
        updatedHw.biosmodel = req.body.biosmodel;
      }
      if(req.body.phymemmb)
      {
        updatedHw.phymemmb = req.body.phymemmb;
      }
      if(req.body.psuwattage)
      {
        updatedHw.psuwattage = req.body.psuwattage;
      }
      if(req.body.serialnumber)
      {
        updatedHw.serialnumber = req.body.serialnumber;
      }

      if(vendors.length)
      {
        vendors = _.uniq(vendors);
        Vendor.find({ vendorid: vendors })
        .then(function showRecs(records)
        {
          // Check if the input has any non-existent vendors
          if(records.length !== vendors.length)
          {
            var error = new Error();
            error.code = 400332;
            error.message = errorCode[ "400332" ];
            throw error;
          }
          else
          {
            return Cihardware.update({ machineid: req.body.machineid }, updatedHw);
          }
        })
        .then(function(records)
        {
          //Check if the machineid exists
          if(!records.length)
          {
            var error = new Error();
            error.code = 400352;
            error.message = errorCode[ "400352" ];
            throw error;
          }
          else
          {
            return res.json(records[ 0 ]);
          }
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
      else
      {
        Cihardware.update({ machineid: req.body.machineid }, updatedHw)
        .then(function(records)
        {
          //Check if the machineid exists
          if(!records.length)
          {
            var error = new Error();
            error.code = 400352;
            error.message = errorCode[ "400352" ];
            throw error;
          }
          else
          {
            return res.json(records[ 0 ]);
          }
        })
        .catch(function(error)
        {
          return res.negotiate(error);
        });
      }
    }
  },
  /**
  @apiGroup Hardware
  @apiName search
  @api {post} /cihardware/search Search
  @apiDescription Searches the model and returns the result. The number of search column and order by criteria are flexible.
  @apiPermission user
  @apiHeader (Content-Type) {String} Content-Type application/json
  @apiHeader (Authorization) {String} Authorization unique access-key of the requester
  @apiExample {curl} Example Usage:
     curl -X POST -H "Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml
     yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28
     ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9
     5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQ
     iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYE
     Jdx6oNW4" -H "Content-Type: application/json" -d
    '{
      "search": {"partnum": "IYU3M14%"},
      "result": ["hardwaretag", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum"],
      "orderby": "hardwaretag"
    }' "http://localhost:1337/cihardware/search"
  @apiParam {Object} search Object listing the attrbutes of the searched asset
  @apiParam {String[]} result Array of attributes requested
  @apiParam {String} orderby Attribute to order by the results
  @apiParamExample {json} Request-Example
    {
      "search":  { "partnum": "IYU3M14%" },
      "result" : [ "hardwaretag", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum"],
      "orderby": "hardwaretag"
    }
  @apiUse 401
  @apiUse 401505
  @apiUse 400051
  @apiUse 400053
  @apiSuccess (200) {Array} result First element-Number of records, Second element-Array of results
  @apiSuccessExample {json} Success-Response
  [
    1, [{
      "manufacturervendorid": 1,
      "hardwaretag": "TGY47IK829",
      "hwmodel": "HF-482R",
      "serialnumber": null,
      "gridloc": "BK-LR9",
      "partnum": "IYU3M14O4Q"
    }]
  ]
  */
  search: function(req, res)
  {
    if(!req.body.search || !req.body.result || !req.body.orderby)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var obj;
    var search = _.mapValues(req.body.search, function(value, key)
    {
      obj = _.pick(Cihardware.definition, key);
      if(!(_.isEmpty(obj)) && obj[ key ].type === "string") return { "like": value };
      else return value;
    });
    Cihardware.find({ select: req.body.result }).where(search).sort(req.body.orderby + " asc")
    .then(function(found)
    {
      return res.json([ found.length, found ]);
    })
    .catch(function(error)
    {
      if(_.includes(error.details, "does not exist"))
      {
        error = new Error();
        error.code = 400053;
        error.message = errorCode[ "400053" ];
        return res.negotiate(error);
      }
      else
      {
        sails.log.error("Error occured while searching", error);
        return res.ok([ 0, [] ]);
      }
    });
  }
};
