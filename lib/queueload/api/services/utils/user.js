/* FileName     : User.js
 *
 * @description  : All the commonly used functions for the User.
 *                Unless specified all the functions return a promise object.
 *
 *   Date              Change Description                            Author
 * ---------         ----------------------                        ---------
 * 17/12/2015         Initial file creation                         SMandal
 * 02/03/2016         Added comments, getUserByState                bones
 *
 */

 var Promise = require("bluebird");
 var _ = require("lodash");

 module.exports =
 {
/**
  * Name: getUser
  * @description: This function fetches all information about an user, including:
  * his associations with queues if any, owned CBs if any, administered BDs if any.
  *
  * @param:  array of User Ids
  * @returns: Promise Object
  */
  getUser: function(userIds)
  {
    var fn = function asyncLookup(id)
    {
      return  User.findOne({ userid: id })
                  .populate("administeredbds")
                  .populate("ownedcbs")
                  .populate("queuesenrolled")
                  /* Attributes withheld until introduction of managers
                  .populate("incmgrqueues")
                  .populate("chgmgrqueues")
                  .populate("srtmgrqueues")
                  */
                  .populate("servicestateapprovalids", { where: { isactive: true } })
                  .populate("accountid")
                  .populate("roles");
    };
    var actions = userIds.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
/**
  * Name: isActive
  * @description: This function fetches the active status of all the users passed
  * to it as parameters.
  *
  * @param:  array of User Ids
  * @returns: Promise Object
  */
  isActive: function(idArray)
  {
    var fn = function asyncLookup(id)
    {
      return User.findOne({ select: [ "userid", "isactive" ] }).where({ userid: id });
    };
    var actions = idArray.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
/**
  * Name: getUserByState
  * @description: This function fetches all information about an user, including:
  * his associations with queues if any, owned CBs if any, administered BDs if any
  * based on the state of the user.
  *
  * @param:  array of User Ids, state to filter the results on
  * @returns: Promise Object
  */
  getUserByState: function(userIds, state)
  {
    var fn = function asyncLookup(id)
    {
      return  User.findOne({ userid: id })
                  .populate("administeredbds", { isactive: state })
                  .populate("ownedcbs", { isactive: state })
                  .populate("queuesenrolled", { isactive: state })
                  .populate("incmgrqueues", { isactive: state })
                  .populate("chgmgrqueues", { isactive: state })
                  .populate("srtmgrqueues", { isactive: state });
    };
    var actions = userIds.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
/**
  * Name: getSuperUsers
  * @description: This function fetches the super users from the supplied array
  *
  * @param:  array of User Ids
  * @returns: Promise Object
  */
  getSuperUsers: function(userIds)
  {
    var fn = function asyncLookup(id)
    {
      return Role.findOne({ select: [ "userid" ] }).where({ userid: id, issuperuser: true });
    };
    var actions = userIds.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
  /**
  * Name: getBdAdmins
  * @description: This function fetches the administered BDs of supplied users
  *
  * @param: arrays of User ids
  @ @returns: Promise Object
  */
  getBdAdmins: function(userIds)
  {
    var fn = function asyncLookup(id)
    {
      return User.findOne({ userid: id }).populate("administeredbds");
    };
    var actions = userIds.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
  isAdminOfCbCode: function(userId, cbId)
  {
    var getUser = User.findOne({ userid: userId }).populate("administeredbds");
    var getCb = Cb.findOne({ cbid: cbId });
    var pResults = Promise.all([ getUser, getCb ])
    .then(function(result)
    {
      var adminBds = [];
      for(var i = 0; i < result[ 0 ].administeredbds.length; i++)
      {
        adminBds.push(result[ 0 ].administeredbds[ i ].bdid);
      }
      return _.includes(adminBds, result[ 1 ].bdid.bdid);
    });
    return pResults;
  }
 };
