/* FileName: AlarmService.js
 * @description: This file contains the various globally exposed functions related to Event Creation.
 *
 * Date	    			  Change Description									   Author
 * ---------			---------------------------						          ---------
 * 22/05/2017		     Initial file creation								       PPani
 *
 */

var enums = require('../../resources/Enums.js');
var errorCode = require("../../resources/ErrorCodes.js");

 module.exports = {

  /**
   * Examine a received alarm and decide to make an entry or disregard
   *
  *  @param: alarmObj
  *  @returns: Promise Object
  */
  createAlarm: function(alarmObj) {

    //TBD:Check if event recieved should be an alarm

    //console.log("alarm object:", alarmObj);

    return true;
  },
  isAnIncident: function(alarmObj) {
    //Sails.log.info"[alarmservice.js] alarmObj", alarmObj);
    if(alarmObj.service === null || alarmObj.type < enums.alarmType.ERROR)
    {
        //Sails.log.info("[alarmservice.isAnIncident] don't create ticket");
        return false;
    }
    else
    {
        //Console.log("create ticket");
        return true;
    }
  },
  createIncident: function(alarmObj, service, config, ciid, reportertype, alarmids)
  {
    var payLoadConstruct = function(title, description, serviceid, ownerqueueid, owneruserid, assignedqueueid, startedat, reportertype, requesterid, ciid)
    {
      this.title = title;
      if(description.length >= 8192)
      {
        this.description = description.slice(0, 8191);
      }
      else
      {
        this.description = description;
      }
      this.serviceid = serviceid;
      this.ownerqueueid = ownerqueueid;
      this.owneruserid = owneruserid;
      this.assignedqueueid = assignedqueueid;
      this.startedat = startedat;
      this.reportertype = reportertype;
      this.alarmids = alarmids;
      this.journal =
      [ {
        type: enums.incJournalTypes.INCIDENT_CREATED,
        time: Math.floor(new Date() / 1000),
        operatorid: requesterid
      } ];
      this.ciid = ciid;
    };
    var incPayload = new payLoadConstruct(alarmObj.name, alarmObj.summary, service.serviceid, config.config.ownerqueueid, config.config.owneruserid, service.supportqueue, alarmObj.reportedAt, reportertype, config.config.owneruserid, ciid);
    var pCreateInc = Incident.create(incPayload);
    return pCreateInc;
  }
};
