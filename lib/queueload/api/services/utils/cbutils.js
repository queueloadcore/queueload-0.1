/* FileName     : CbUtils.js
 *
 * @description  : All the commonly used functions for the Chargeback Codes.
 *                Unless specified all the functions return a promise object.
 *
 * Date          Change Description                            Author
 * ---------     ----------------------                        -------
 * 25/12/2015    Initial file creation                         SMandal
 *
 */

var Promise = require("bluebird");

module.exports =
{

/**
  *  Name: getCB
  *  @description: This function fetches all information about a cb, including:
  *  all queues owned by the CB and the owner details.
  *
  *  @param: Array of Cb Ids
  *  @return: Promise Object
  */
  getCB: function(cbIds)
  {
    var fn = function asyncLookup(cbId)
    {
      return Cb.findOne({ cbid: cbId })
               .populate("ownedqueues")
               .populate("cbownerid")
               .populate("bdid")
               .populate("ownedservices");
    };

    var actions = cbIds.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
/**
  * Name: isActive
  * @description: This function fetches the active status of all the CB codes passed
  * to it as parameters.
  *
  * @param:  array of CB Ids
  * @returns: Promise Object
  */
  isActive: function(idArray)
  {
    var fn = function asyncLookup(id)
    {
      return Cb.findOne({ select: [ "cbid", "isactive", "isfrozen" ] }).where({ cbid: id });
    };

    var actions = idArray.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  }

};
