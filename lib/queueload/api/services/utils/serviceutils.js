  /** FileName     : serviceutils.js
  *
  * @description  :All the commonly used functions for Services.
  *                Unless specified all the functions return a promise object.
  *
  * Date          Change Description                            Author
  * ---------     ----------------------                        -------
  * 30/03/2016    Initial file creation                         SMandal
  *
  */

  module.exports =
  {
    /**
    *  Name: sendStateTransitionApproval
    *  @description: This function gets the details of a service and
    *  makes an entry in the Serviceapproval table for the appropriate approver
    *
    *
    *  @param: serviceid
    *  @returns: Promise Object
    */
    sendApproval: function(newserviceids, currentserviceids, cbid, serviceid, type, ciid)
    {
      var pResult = Cb.findOne({ cbid: cbid })
      .then(function(cb)
      {
        if(type === "state")
        {
          return Serviceapproval.create({ approverid: cb.cbownerid, approverserviceid: serviceid, type: type });
        }
        else
        {
          return Serviceapproval.create({ approverid: cb.cbownerid, approverserviceid: serviceid, type: type, ciid: ciid });
        }
      })
      .then(function(result)
      {
        if(type === "state")
        {
          return result;
        }
        else
        {
          return Serviceapproval.update({ approvalid: result.approvalid }, { crntsrvids: currentserviceids });
        }
      })
      .then(function(result)
      {
        if(type === "state")
        {
          return result;
        }
        else
        {
          return Serviceapproval.update({ approvalid: result[ 0 ].approvalid }, { newserviceids: newserviceids });
        }
      });
      return pResult;
    },
    /**
    * Name: runsOn
    * @description: The function checks if a service runs on a CI
    *
    * @param: serviceid, ciid
    * @returns: boolean
    * Service.findOne({serviceid: 1}).populate('ciids', {where: {ciid: [4]}})
    */
    runsOn: function(serviceid, ciid)
    {
      var pResult = Service.findOne({ serviceid: serviceid }).populate('ciids', { where: { ciid: [ ciid ] } })
      .then(function(result)
      {
        // A service doesn't necessarily need a CI to be active
        if(result.ciids.length || (ciid === null))
        {
          return true;
        }
        else
        {
          return false;
        }
      }).catch(function(error)
      {
        throw error;
      });
      return pResult;
    }
  };
