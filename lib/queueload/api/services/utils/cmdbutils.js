  /** FileName     : CmdbUtils.js
  *
  * @description  : All the commonly used functions for the Configuration Item.
  *                Unless specified all the functions return a promise object.
  *
  * Date          Change Description                            Author
  * ---------     ----------------------                        -------
  * 01/03/2015    Initial file creation                         SMandal
  *
  */

  module.exports =
  {
  /**
    *  Name: sendApproval
    *  @description: This function gets the details of a configuration item and
    *  makes an entry in the Ciapproval table for the appropriate approver
    *
    *
    *  @param: ciid
    *  @returns: Promise Object
    */
    sendApproval: function(ciid, cbid)
    {
      var pResult = Cb.findOne({ cbid: cbid })
      .then(function(cb)
      {
        return Ciapproval.create({ ciid: ciid, approverid: cb.cbownerid });
      });
      return pResult;
    }
  };
