/* FileName: genutils.js
 *  @description: Describes the frequently used general purpose utilities.
 *
 *  Date          Change Description                            Author
 *  ---------     ---------------------------                   -------
 *  19/12/2015    Initial file creation                         SMandal
 *
 */

var sizeof = require("object-sizeof");
var enums = require('../../resources/Enums.js');
var _ = require("lodash");

module.exports = {

  size: function(obj)
  {
    var size = 0,
      key;
    for (key in obj)
    {
      if (obj.hasOwnProperty(key))
      {
        size++;
      }
    }
    return size;
  },
  enumtostr: function(enumObject, value)
  {
    for (var k in enumObject)
    {
      if (enumObject[ k ] == value)
      {
        return k;
      }
    }
    return null;
  },
  sizestat: function(object) {
      var totalsize = 0;
      var thisobj = sizeof(object);
      totalsize += thisobj;
      sails.log.debug("Size of response body(in Bytes):", thisobj);
      sails.log.debug("Content sent to far(in Bytes):", totalsize);
      return;
  },
  randgen: function(length, type)
  {
    var text = "";
    var possible;
    // 1 = Alphabetic
    // 2 = Numeric
    // 3 = Alphanumeric
    switch(type)
    {
      case 1:
        possible = "abcdefghijklmnopqrstuzwxyz";
        break;
      case 2:
        possible = "0123456789";
        break;
      case 3:
        possible = "abcdefghijklmnopqrstuzwxyz0123456789";
        break;
      default:
        return;
    }
    for( var i = 0; i < length; i++ )
    {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  },
  genHookAddress: function(type)
  {
    switch(type) {
      case enums.integrationService.CLOUDWATCH:
        var endPoint = sails.config.intcfg.qlIntegrationUrl +  "event/receivecwevents?qlaccountid=<accountid>&awscwkey=<awscwkey>";
        var awscwkey = this.randgen(12, 3);
        endPoint = _.replace(endPoint, "<accountid>", process.env.ACCOUNTID);
        endPoint = _.replace(endPoint, "<awscwkey>", awscwkey);
        return endPoint;
      case enums.integrationService.NAGIOS:
        var endPoint = sails.config.intcfg.qlIntegrationUrl + "event/receivenagiosevents?qlaccountid=<accountid>&nagioskey=<nagioskey>";
        var nagioskey = this.randgen(12, 3);
        endPoint = _.replace(endPoint, "<accountid>", process.env.ACCOUNTID);
        endPoint = _.replace(endPoint, "<nagioskey>", nagioskey);
        return endPoint;
      case enums.integrationService.SENTRY:
        var endPoint = sails.config.intcfg.qlIntegrationUrl + "event/receivesentryevents?qlaccountid=<accountid>&sentrykey=<sentrykey>";
        var sentrykey = this.randgen(12, 3);
        endPoint = _.replace(endPoint, "<accountid>", process.env.ACCOUNTID);
        endPoint = _.replace(endPoint, "<sentrykey>", sentrykey);
        return endPoint;
      case enums.integrationService.LOGGLY:
        var endPoint = sails.config.intcfg.qlIntegrationUrl + "event/receivelogglyevents?qlaccountid=<accountid>&logglykey=<logglykey>";
        var logglykey = this.randgen(12, 3);
        endPoint = _.replace(endPoint, "<accountid>", process.env.ACCOUNTID);
        endPoint = _.replace(endPoint, "<logglykey>", logglykey);
        return endPoint;
      case enums.integrationService.CUSTOM:
        var endPoint = sails.config.intcfg.qlIntegrationUrl + "event/receivecustomevents?qlaccountid=<accountid>&customkey=<customkey>";
        var customkey = this.randgen(12, 3);
        endPoint = _.replace(endPoint, "<accountid>", process.env.ACCOUNTID);
        endPoint = _.replace(endPoint, "<customkey>", customkey);
        return endPoint;
      case enums.integrationService.AZURE:
        var endPoint = sails.config.intcfg.qlIntegrationUrl + "event/receiveazureevents?qlaccountid=<accountid>&azurekey=<azurekey>";
        var azurekey = this.randgen(12, 3);
        endPoint = _.replace(endPoint, "<accountid>", process.env.ACCOUNTID);
        endPoint = _.replace(endPoint, "<azurekey>", azurekey);
        return endPoint;
      default:
        return;
    }
  }
};
