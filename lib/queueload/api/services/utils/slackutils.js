/* FileName     : SlackUtils.js
 *
 * @description  : All the commonly used functions for the Slack.
 *                Unless specified all the functions return a promise object.
 *
 * Date          Change Description                            Author
 * ---------     ----------------------                        -------
 * 28/02/2017    Initial file creation                         SMandal
 *
 */
var IncomingWebhook = require('@slack/client').IncomingWebhook;
var Promise = require("bluebird");

module.exports =
{
	/**
  *  Name: notifySlack
  *  @description: This function takes the message payload and sends it to slack channel
  *
  *  @param: Message paylod, slackURL
  *  @returns: Promise Object
  */
  notifySlack: function(message, url)
  {
    var promisedHook = Promise.promisifyAll(new IncomingWebhook(url));
    var pSendNotification = promisedHook.sendAsync(message)
    .then(function(reply)
    {
      return reply;
    })
    .catch(function(error)
    {
      return error;
    });
    return pSendNotification;
  }
};
