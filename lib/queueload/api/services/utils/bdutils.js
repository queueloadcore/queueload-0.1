/* FileName     : BbUtils.js
 *
 * @description  : All the commonly used functions for the Business Department.
 *                Unless specified all the functions return a promise object.
 *
 * Date          Change Description                            Author
 * ---------     ----------------------                        -------
 * 20/12/2015    Initial file creation                         SMandal
 *
 */

  var Promise = require("bluebird");

  module.exports = {

    /**
    *  Name: getBd
    *  @description: This function fetches all information about a bd, including:
    *  administrator(s), chargeback codes under the BD.
    *
    *  @param: Array of Bd Ids
    *  @returns: Promise Object
    */
    getBd: function(idArray)
    {
      var fn = function asyncLookup(id)
      {
        return Bd.findOne({ bdid: id }).populate("admins").populate("cbcodes");
      };

      var actions = idArray.map(fn);
      var pResults = Promise.all(actions);
      return pResults;
    },
    /**
    * Name: isActive
    * @description: This function fetches the active status of all the BDs passed
    * to it as parameters.
    *
    * @param:  array of BD Ids
    * @returns: Promise Object
    */
    isActive: function(idArray)
    {
      var fn = function asyncLookup(id)
      {
        return Bd.findOne({ select: [ "bdid", "isactive" ] }).where({ bdid: id });
      };

      var actions = idArray.map(fn);
      var pResults = Promise.all(actions);
      return pResults;
    }
 };
