/* FileName     : QueueUtils.js
 *
 * @description  : All the commonly used functions for the Queues.
 *                Unless specified all the functions return a promise object.
 *
 * Date          Change Description                            Author
 * ---------     ----------------------                        -------
 * 04/01/2016    Initial file creation                         SMandal
 *
 */

var Promise = require("bluebird");

module.exports =
{
/**
  * Name: getQueue
  * @description: This function fetches all information about a queue.
  *
  *  @Params:  Array of Queue Ids
  *  @Return:  Promise Object
  */
  getQueue: function(queueIdArray)
  {
    var fn = function asyncLookup(queueId)
    {
      return Queue.findOne({ queueid: queueId })
                  .populate("members")
                  .populate('queueownerid')
                  .populate('incidentmanagerid')
                  .populate('changemanagerid')
                  //.populate('cismanaged')
                  .populate('srtmanagerid')
                  .populate('cbid')
                  .populate('servicesmanaged');
    };

    var actions = queueIdArray.map(fn);
    var pQueueData = Promise.all(actions);
    return pQueueData;
  },
/**
  * Name: isActive
  * @description: This function fetches the active status of all the Queue Ids passed
  * to it as parameters.
  *
  * @param: array of CB Ids
  * @returns: Promise Object
  */
  isActive: function(idArray)
  {
    var fn = function asyncLookup(id)
    {
      return Queue.findOne({ select: [ "queueid", "isactive", "isfrozen" ] }).where({ queueid: id });
    };

    var actions = idArray.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  },
/**
  * Name: isMember
  * @description: This function checks if a given User Id is a member of a queue or not
  *
  * @param: Queue Id, user Id
  * @returns: True if the user is a member and False if he is not
  *
  */

  isMember: function(queueId, userId)
  {
    var pResults = Queue.find({ queueid: queueId }, { select: [ "queueid" ] }).populate("members")
    .then(function(records)
    {
      if (records.length === 0)
      {
        return false;
      }
      var memberCount = records[ 0 ].members.length;
      if (memberCount === 0)
      {
        return false;
      }
      for(var i = 0; i < memberCount; i++)
      {
        if(records[ 0 ].members[ i ].userid === userId)
        {
          return true;
        }
      }
      return false;
    })
    .catch(function(error)
    {
      throw error;
    });
    return pResults;
  },
/**
  * Name: belongsTo
  * @description: This function checks if a given property value is related to a queue
  *
  * @param: Queue Id, property name(relation to the Queue eg: Incident Manager, queueemail, etc) and propValue
  * @returns: True if the property belongs to the queue and False if it is not
  */
  belongsTo: function(queueId, prop, propValue)
  {
    Queue.find({ prop: propValue }, { select: [ "queueid" ] })
    .then(function(records)
    {
      if(records.length === 0) { return false; }
      if(records[ 0 ].queueid === queueId) { return true; }

      return false;
    })
    .catch(function(error)
    {
      return false;
    });
  }
};
