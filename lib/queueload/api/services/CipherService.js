/* FileName: CipherService.js
* @description: This file contains the various globally exposed functions related to user authentication.
*
* Date				  Change Description											Author
* ---------			---------------------------							-------
* 06/12/2015		Initial file creation										SMandal
*
*/

var bcrypt = require("bcrypt");
var SALT_WORK_FACTOR = sails.config.authcfg.saltFactor;
var jwt = require("jsonwebtoken");
var crypto = require("crypto");

module.exports = {

  /**
  * Hash the password field of the passed user. Asynchronous)
  */
  hashPassword: function(password, cb) {
    return bcrypt.hash(password, SALT_WORK_FACTOR, cb);
  },

  /**
  * Compare user password hash with unhashed password (Asynchronous)
  * @returns boolean indicating a match
  */
  comparePassword: function(password1, password2, cb) {
    return bcrypt.compare(password1, password2, cb);
  },

  /**
   * Create an access token based on the passed user
   * @param user
   */
  createAccToken: function(user)
  {
    return jwt.sign({
      user: user.toJSON()
      },
      sails.config.authcfg.accTknSecret,
      {
        algorithm: sails.config.authcfg.accTknAlgorithm,
        expiresIn: sails.config.authcfg.accExpiresIn,
        issuer: sails.config.authcfg.accTknIssuer,
        audience: sails.config.authcfg.accTknAudience
      }
    );
  },

  /**
   * Create a refresh token based on the passed user
   */
  createRefToken: function()
  {
    var buf = crypto.randomBytes(sails.config.authcfg.refTknLen);
    var hash = crypto.createHash(sails.config.authcfg.refTknkAlgorithm);
    hash.update(buf.toString("hex") + process.hrtime().toString());
    return hash.digest("hex");
  },

  verifyAccToken: function(accTkn, callback)
  {
    return jwt.verify(
      accTkn,
      sails.config.authcfg.accTknSecret,
      callback
    );
  }
};
