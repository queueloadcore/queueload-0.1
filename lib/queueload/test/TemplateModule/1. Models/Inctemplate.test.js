/* FileName: Inctemplate.test.js
 * Description: This file defines all the unit test cases for the Incident Template Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 07/11/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;

var validIncTemplate =
{
	"templatename": "firstIncTemplate",
  "lasteditorid": 1,
  "title": "This is the title of the first incident template",
  "description": "This is a sample description of the incident template",
  "ownerqueuename": "welladal",
  "owneruserid": "uchaltijawani",
  "assignedqueueid": "thakahuadal",
  "assigneduserid": "dadaji",
  "servicename": "dummyservice",
  "ciname": "DummyNameOfDummyCI",
  "severity": 3,
  "impact": 3
};

describe('Incident Template Model', function()
{
  describe('#Create', function()
  {
    it('should not create an incident template - missing templatename', function(done)
    {
      var temp = validIncTemplate.templatename;
      delete validIncTemplate.templatename;
      Inctemplate.create(validIncTemplate)
      .then(function createTemplate(template)
      {
        expect(template).to.not.exist;
      })
      .catch(function(error)
      {
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        validIncTemplate.templatename = temp;
        done();
      });
    });

    it('should not create an incident template - missing lasteditor id', function(done)
    {
      var temp = validIncTemplate.lasteditorid;
      delete validIncTemplate.lasteditorid;
      Inctemplate.create(validIncTemplate)
      .then(function createTemplate(template)
      {
        expect(template).to.not.exist;
      })
      .catch(function(error)
      {
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        validIncTemplate.lasteditorid = temp;
        done();
      });
    });

    it('should create an incident template', function(done)
    {
      Inctemplate.create(validIncTemplate)
      .then(function createTemplate(template)
      {
        expect(template.isactive).to.equal(true);
        validIncTemplate.templateid = template.templateid;
        done();
      })
      .catch(done);
    });
  });

  describe('#Update()', function()
  {
    it('should not update an incident template - invalid template id', function(done)
    {
      Inctemplate.update({ templateid: -1 }, { severity: 2 })
      .then(function updateTemplate(template)
      {
        expect(template).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        done();
      });
    });

    it('should update an incident template', function(done)
    {
      Inctemplate.update({ templateid: validIncTemplate.templateid }, { severity: 2 })
      .then(function updateTemplate(template)
      {
        expect(template).to.exist;
        expect(template[ 0 ].severity).to.equal(2);
        done();
      })
      .catch(done);
    });
  });

  describe('#Find()', function()
  {
    it('should not find the template - non-existent templateid', function(done)
    {
      Inctemplate.findOne({ templateid: -1 })
      .then(function findTemplate(template)
      {
        expect(template).to.not.exist;
        done();
      })
      .catch(done);
    });

    it('should find the template', function(done)
    {
      Inctemplate.findOne({ templateid: validIncTemplate.templateid })
      .then(function findTemplate(template)
      {
        expect(template).to.exist;
        done();
      })
      .catch(done);
    });
  });

  describe('#Destroy', function()
  {
    it('#should not destroy the template - non-existent id', function(done)
    {
      Inctemplate.destroy({ templateid: -1 })
      .then(function destTemplate(template)
      {
        expect(template[ 0 ]).to.not.exist;
        done();
      })
      .catch(done);
    });

    it('#should destroy the template', function(done)
    {
      Inctemplate.destroy({ templateid: validIncTemplate.templateid })
      .then(function destTemplate(template)
      {
        expect(template).to.exist;
        done();
      })
      .catch(done);
    });

    it('#should not find the template', function(done)
    {
      Inctemplate.findOne({ templateid: validIncTemplate.templateid })
      .then(function destTemplate(template)
      {
        expect(template).to.not.exist;
        done();
      })
      .catch(done);
    });
  });
});
