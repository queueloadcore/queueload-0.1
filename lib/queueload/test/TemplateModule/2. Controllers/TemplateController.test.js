/* FileName: TemplateController.test.js
* Description: This file defines all the test cases for the Template Controller.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 07/11/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;

var validAccTkn, validRefTkn;

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001"
};

var firstUser =
{
  "firstname":"tmplttestbatch",
  "lastname":"tmplttesting",
  "fullname":"tmplttestbatch tmplttesting",
  "username":"tmpltT3st.bat1",
  "password":"P@55word",
  "emailid":"tmplttest.bat1@testclients.com",
  "countrycode":"+91",
  "phonenumber":"9351256745",
  "employeeid":"TMPLTFTE000001"
};

var validIncTemplate =
{
  "templatename": "sampleIncTemplate",
  "lasteditorid": 1,
  "type": 1,
  "title": "This is the title of the first incident template",
  "description": "This is a sample description of the incident template",
  "ownerqueuename": "chotadal",
  "ownerusername": "panautiwalla",
  "assignedqueuename": "badadal",
  "assignedusername": "bakrahalal",
  "servicename": "dummyservice",
  "ciname": "DummyNameOfDummyCI",
  "severity": 3,
  "impact": 3
};

describe('Template Controller', function()
{
  before('Setting the stage for Template tests', function(done)
  {
    User.findOrCreate({ emailid: "administrator@testdomain.com" }, superUser)
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send({ emailid: superUser.emailid, password: superUser.password });
      }
    })
    .then(function(res)
    {
      var superToken = res.body.acctoken;
      return request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", superToken)
      .send(firstUser);
    })
    .then(function(res)
    {
      return request(sails.hooks.http.app)
      .post("/auth/signin")
      .send({ emailid: firstUser.emailid, password: firstUser.password });
    })
    .then(function(res)
    {
      expect(res.body.acctoken).to.exist;
      expect(res.body.reftoken).to.exist;
      validAccTkn = res.body.acctoken;
      validRefTkn = res.body.reftoken;
      done();
    })
    .catch(done);
  });

  describe('Incdient Templates', function()
  {
    describe('#add()', function()
    {
      it('should not add the template(missing data) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", validAccTkn)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not add the template(missing templatename) - 400051', function(done)
      {
        var temp = validIncTemplate.templatename;
        delete validIncTemplate.templatename;
        request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", validAccTkn)
        .send(validIncTemplate)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          validIncTemplate.templatename = temp;
          done();
        })
        .catch(done);
      });

      it('should not add the template(missing type) - 400051', function(done)
      {
        var temp = validIncTemplate.type;
        delete validIncTemplate.type;
        request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", validAccTkn)
        .send(validIncTemplate)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          validIncTemplate.type = temp;
          done();
        })
        .catch(done);
      });

      it('should not add the template(type not incident) - 400051', function(done)
      {
        var temp = validIncTemplate.type;
        validIncTemplate.type = 2342;
        request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", validAccTkn)
        .send(validIncTemplate)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          validIncTemplate.type = temp;
          done();
        })
        .catch(done);
      });

      it('should add the template', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", validAccTkn)
        .send(validIncTemplate)
        .then(function(res)
        {
          expect(res.body.isactive).to.equal(true);
          validIncTemplate.templateid = res.body.templateid;
          done();
        })
        .catch(done);
      });
    });

    describe('#list()', function()
    {
      it('should not list the templates(missing input) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/list")
        .set("Authorization", validAccTkn)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not list the templates(missing type) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/list")
        .set("Authorization", validAccTkn)
        .send({ limit: 10, page: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not list the templates(missing type) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/list")
        .set("Authorization", validAccTkn)
        .send({ limit: 10, page: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

       it('should not list the templates(type not incident) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/list")
        .set("Authorization", validAccTkn)
        .send({ limit: 10, page: 1, type: 2342 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should list the templates', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/list")
        .set("Authorization", validAccTkn)
        .send({ limit: 10, page: 1, type: 1 })
        .then(function(res)
        {
          expect(res.body[ 0 ].templatename).to.equal('sampleIncTemplate');
          done();
        })
        .catch(done);
      });
    });
    describe('#update()', function()
    {
      it('should not update the template(missing input) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/update")
        .set("Authorization", validAccTkn)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not update the template(missing templateid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/update")
        .set("Authorization", validAccTkn)
        .send({ type: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not update the template(missing type) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/update")
        .set("Authorization", validAccTkn)
        .send({ templateid: validIncTemplate.templateid })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not update the template(non-existent templateid) - 400501', function(done)
      {
        var temp = validIncTemplate.templateid;
        validIncTemplate.templateid = 10876;
        delete validIncTemplate.templatename;
        request(sails.hooks.http.app)
        .post("/template/update")
        .set("Authorization", validAccTkn)
        .send(validIncTemplate)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400501);
          validIncTemplate.templateid = temp;
          done();
        })
        .catch(done);
      });

      it('should update the template', function(done)
      {
        validIncTemplate.servicename = "anotherservice";
        request(sails.hooks.http.app)
        .post("/template/update")
        .set("Authorization", validAccTkn)
        .send(validIncTemplate)
        .then(function(res)
        {
          expect(res.body.servicename).to.equal('anotherservice');
          validIncTemplate = res.body;
          done();
        })
        .catch(done);
      });
    });
    describe('#search()', function()
    {
      it('should not search the template(missing input) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not search the template(missing type) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ search: { templateid: validIncTemplate.templateid }, result: [ "templateid", "lasteditorid", "isactive" ], orderby: "templateid" })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not search the template(missing type) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ search: { templateid: validIncTemplate.templateid }, result: [ "templateid", "lasteditorid", "isactive" ], orderby: "templateid" })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not search the template(missing search parameter) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ type: 1, result: [ "templateid", "lasteditorid", "isactive" ], orderby: "templateid" })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not search the template(missing result columns) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ search: { templateid: validIncTemplate.templateid }, type: 1, orderby: "templateid" })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not search the template(missing ordering suggestion) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ search: { templateid: validIncTemplate.templateid }, type: 1, result: [ "templateid", "lasteditorid", "isactive" ] })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not search the template(non-existent columns) - 400053', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ search: { templateid: validIncTemplate.templateid }, type: 1, result: [ "templateid", "beltid", "isactive" ], orderby: "templateid" })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400053);
          done();
        })
        .catch(done);
      });

      it('should search the template', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/search")
        .set("Authorization", validAccTkn)
        .send({ search: { templateid: validIncTemplate.templateid }, type: 1, result: [ "templateid", "lasteditorid", "isactive" ], orderby: "templateid" })
        .then(function(res)
        {
          expect(res.body[ 1 ][ 0 ].templateid).to.equal(2);
          done();
        })
        .catch(done);
      });
    });
    describe('#getdetail()', function()
    {
      it('should not get the template details(missing input) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/getdetail")
        .set("Authorization", validAccTkn)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not get the template details(missing templateid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/getdetail")
        .set("Authorization", validAccTkn)
        .send({ type: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not get the template details(type not incident) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/getdetail")
        .set("Authorization", validAccTkn)
        .send({ templateid: validIncTemplate.templateid, type: 567 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not get the template(non-existent templateid) - 400501', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/getdetail")
        .set("Authorization", validAccTkn)
        .send({ templateid: 34257, type: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400501);
          done();
        })
        .catch(done);
      });

      it('should get the template details', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/getdetail")
        .set("Authorization", validAccTkn)
        .send({ templateid: validIncTemplate.templateid, type: 1 })
        .then(function(res)
        {
          expect(res.body.severity).to.equal(3);
          done();
        })
        .catch(done);
      });
    });
    describe('#delete()', function()
    {
      it('should not delete the template(missing input) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not delete the template(missing templateid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .send({ type: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not delete the template(multiple templateids) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .send({ templateid: [ 18, 19 ], type: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not delete the template(template type missing) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .send({ templateid: validIncTemplate.templateid })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not delete the template(type is not incident) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .send({ templateid: validIncTemplate.templateid, type: 23891 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });

      it('should not delete the template(non-existent templateid) - 400501', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .send({ templateid: 23345, type: 1 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400501);
          done();
        })
        .catch(done);
      });

      it('should delete the template', function(done)
      {
        request(sails.hooks.http.app)
        .post("/template/delete")
        .set("Authorization", validAccTkn)
        .send({ templateid: validIncTemplate.templateid, type: 1 })
        .then(function(res)
        {
          expect(res.body.templatename).to.equal('sampleIncTemplate');
          done();
        })
        .catch(done);
      });
    });
  });
});
