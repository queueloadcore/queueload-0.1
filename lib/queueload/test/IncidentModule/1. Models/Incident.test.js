/* FileName: Incident.test.js
 * Description: This file defines all the unit test cases for the Incident Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 17/10/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;

var invalidCreateIncident_missingTitle =
{
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_invalidState =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "state": "khulla",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_tooLongTitle =
{
  "title": "Toooooooooooooooooooooooo loooooonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnggggggg aaaa tttttttttiiiiiiiiiiiittttttttttttttttttlllllllleeeeeee",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_missingDesc =
{
  "title": "This is a test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_missingServ =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_missingOwnerQ =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_missingOwnerUser =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "assignedqueueid": 1
};
var invalidCreateIncident_missingAssignedQ =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1
};
var invalidCreateIncident_invalidSeverity =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1,
  "severity": "invalid"
};
var invalidCreateIncident_invalidImpact =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1,
  "impact": "incorrectimpact"
};
var validCreateIncident =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "serviceid": 1,
  "ownerqueueid": 1,
  "owneruserid": 1,
  "assignedqueueid": 1
};

describe("Incident Model Test", function()
{
	describe("#Create", function()
  {
    it("should not add an incident - missing title", function(done)
    {
      invalidCreateIncident_missingTitle.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_missingTitle.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_missingTitle)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - too long title", function(done)
    {
      invalidCreateIncident_tooLongTitle.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_tooLongTitle.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_tooLongTitle)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing description", function(done)
    {
      invalidCreateIncident_missingDesc.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_missingDesc.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_missingDesc)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing service", function(done)
    {
      invalidCreateIncident_missingServ.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_missingServ.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_missingServ)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing owner queue", function(done)
    {
      invalidCreateIncident_missingOwnerQ.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_missingOwnerQ.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_missingOwnerQ)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing owner user", function(done)
    {
      invalidCreateIncident_missingOwnerUser.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_missingOwnerUser.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_missingOwnerUser)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing assigned queue", function(done)
    {
      invalidCreateIncident_missingAssignedQ.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_missingAssignedQ.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_missingAssignedQ)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing startedat", function(done)
    {
      validCreateIncident.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(validCreateIncident)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - invalid severity", function(done)
    {
      invalidCreateIncident_invalidSeverity.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_invalidSeverity.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_invalidSeverity)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - invalid impact", function(done)
    {
      invalidCreateIncident_invalidImpact.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_invalidImpact.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_invalidImpact)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - missing journal", function(done)
    {
      delete validCreateIncident.journal;
      validCreateIncident.startedat = Math.floor(new Date() / 1000);
      Incident.create(validCreateIncident)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should not add an incident - invalid state", function(done)
    {
      invalidCreateIncident_invalidState.startedat = Math.floor(new Date() / 1000);
      invalidCreateIncident_invalidState.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(invalidCreateIncident_invalidState)
      .then(function(incident)
      {
        expect(incident).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
        done();
      });
    });
    it("should add an Incident to the system", function(done)
    {
      validCreateIncident.startedat = Math.floor(new Date() / 1000);
      validCreateIncident.journal = JSON.parse('[{"Time" :' + Math.floor(new Date() / 1000) + ', "Message" : "Incident Opened"}]');
      Incident.create(validCreateIncident)
      .then(function(incident)
      {
        expect(incident).to.exist;
        validCreateIncident.incidentid = incident.incidentid;
        done();
      })
      .catch(function(error)
      {
        done(error);
      });
    });
  });
  describe('#Update()', function()
  {
    it("should not update the incident - invalid incidentid", function(done)
    {
      Incident.update({ incidentid: 8193819 }, { startedat: Math.floor(new Date() / 1000) - 3600 })
      .then(function(incident)
      {
        expect(incident).to.be.empty;
        done();
      })
      .catch(function(error)
      {
        done(error);
      });
    });
    it("should update the incident", function(done)
    {
      Incident.update({ incidentid: validCreateIncident.incidentid }, { startedat: Math.floor(new Date() / 1000) - 3600 })
      .then(function(incident)
      {
        expect(incident[ 0 ]).to.have.property('startedat').and.below(Math.floor(new Date() / 1000));
        done();
      })
      .catch(function(error)
      {
        done(error);
      });
    });
  });
  describe('#Destroy()', function()
  {
    it("should not destroy the incident - invalid incidentid", function(done)
    {
      Incident.destroy({ incidentid: 8193819 })
      .then(function(incident)
      {
        expect(incident).to.be.empty;
        done();
      })
      .catch(function(error)
      {
        done(error);
      });
    });
    it("should update the incident", function(done)
    {
      Incident.destroy({ incidentid: validCreateIncident.incidentid })
      .then(function(incident)
      {
        expect(incident[ 0 ]).to.have.property('incidentid').and.equal(validCreateIncident.incidentid);
        done();
      })
      .catch(function(error)
      {
        done(error);
      });
    });
  });
});
