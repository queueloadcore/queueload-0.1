/* FileName: IncidentModule.test.js
* Description: This file defines the unit and integration test cases for the Incident Module.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 14/09/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var enums = require('../../../api/resources/Enums.js');

var company =
{
  "accountid": "qlodtest",
  "name": "Wayne Enterprises",
  "address": "Gotham City",
  "accntadmin": 1,
  "globalconfig": {}
};

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001"
};

var firstBDAdmin =
{
  "firstname": "incfirstAdmin",
  "lastname": "incBDOne",
  "fullname": "incfirstAdmin incBDOne",
  "username": "incfirst.bdoneadmin",
  "password": "password",
  "emailid": "incfirst.bd1admin@testdomain.com",
  "employeeid": "INCFTE000002"
};

var secondBDAdmin =
{
  "firstname": "incsecondAdmin",
  "lastname": "incBDOne",
  "fullname": "incsecondAdmin incBDOne",
  "username": "incsecond.bdoneadmin",
  "password": "password",
  "emailid": "incsecond.bd1admin@testdomain.com",
  "employeeid": "INCFTE000003"
};

var thirdBDAdmin =
{
  "firstname": "incthirdAdmin",
  "lastname": "incBDTwo",
  "fullname": "incthirdAdmin incBDTwo",
  "username": "incfirst.bdtwoadmin",
  "password": "password",
  "emailid": "incfirst.bd2admin@testdomain.com",
  "employeeid": "INCFTE000004"
};

var fourthBDAdmin =
{
  "firstname": "incfourthAdmin",
  "lastname": "incBDTwo",
  "fullname":"incfourthAdmin incBDTwo",
  "username": "incsecond.bdtwoadmin",
  "password": "password",
  "emailid": "incsecond.bd2admin@testdomain.com",
  "employeeid": "INCFTE000005"
};

var firstCBOwner =
{
  "firstname": "incfirst",
  "lastname": "incCBOwner",
  "fullname":"incfirst incCBOwner",
  "username": "incfirst.cbowner",
  "password": "password",
  "emailid": "incfirst.cbowner@testdomain.com",
  "employeeid": "INCFTE000006"
};

var secondCBOwner =
{
  "firstname": "incsecond",
  "lastname": "incCBOwner",
  "fullname":"incsecond incCBOwner",
  "username": "incsecond.cbowner",
  "password": "password",
  "emailid": "incsecond.cbowner@testdomain.com",
  "employeeid": "INCFTE000007"
};

var thirdCBOwner =
{
  "firstname": "incthird",
  "lastname": "incCBOwner",
  "fullname": "incthird incCBOwner",
  "username": "incthird.cbowner",
  "password": "password",
  "emailid": "incthird.cbowner@testdomain.com",
  "employeeid": "INCFTE000008"
};

var firstOperator =
{
  "firstname": "incfirst",
  "lastname": "incOperator",
  "fullname": "incfirst incOperator",
  "username": "incfirst.operator",
  "password": "password",
  "emailid": "incfirst.operator@testdomain.com",
  "employeeid": "INCFTE000009"
};

var secondOperator =
{
  "firstname": "incsecond",
  "lastname": "incOperator",
  "fullname": "incsecond incOperator",
  "username": "incsecond.Operator",
  "password": "password",
  "emailid": "incsecond.operator@testdomain.com",
  "employeeid": "INCFTE000010"
};

var thirdOperator =
{
  "firstname": "incthird",
  "lastname": "incOperator",
  "fullname":"incthird incOperator",
  "username": "incthird.operator",
  "password": "password",
  "emailid": "incthird.operator@testdomain.com",
  "employeeid": "INCFTE000011"
};

var fourthOperator =
{
  "firstname": "incfourth",
  "lastname": "incOperator",
  "fullname":"incfourth incOperator",
  "username": "incfourth.Operator",
  "password": "password",
  "emailid": "incfourth.operator@testdomain.com",
  "employeeid": "INCFTE000012"
};

var fifthOperator =
{
  "firstname": "incfifth",
  "lastname": "incOperator",
  "fullname":"incfifth incOperator",
  "username": "incfifth.Operator",
  "password": "password",
  "emailid": "incfifth.operator@testdomain.com",
  "employeeid": "INCFTE000013"
};

var sixthOperator =
{
  "firstname": "incsixth",
  "lastname": "incOperator",
  "fullname": "incsixth incOperator",
  "username": "incsixth.Operator",
  "password": "password",
  "emailid": "incsixth.operator@testdomain.com",
  "employeeid": "INCFTE000014"
};

var firstUser =
{
  "firstname": "incfirst",
  "lastname": "incUser",
  "fullname": "incfirst incUser",
  "username": "incfirst.User",
  "password": "password",
  "emailid": "incfirst.user@testdomain.com",
  "employeeid": "INCFTE000015"
};

var bdOne = {};
var bdTwo = {};
var cbOne = {};
var cbTwo = {};
var cbThree = {};
var orgApproval1 = {};
var orgApproval2 = {};
var attachmentOne = {};

var queueOne =
{
  "queuename":"incTestQueue1",
  "queueemail":"inctestqueue1@testdomain.com",
  "alertl1":"inctestqueue1alert1@testdomain.com",
  "alertl2":"inctestqueue1alert2@testdomain.com",
  "alertl3":"inctestqueue1alert3@testdomain.com"
};

var queueTwo =
{
  "queuename":"incTestQueue2",
  "queueemail":"inctestqueue2@testdomain.com",
  "alertl1":"inctestqueue2alert1@testdomain.com",
  "alertl2":"inctestqueue2alert2@testdomain.com",
  "alertl3":"inctestqueue2alert3@testdomain.com"
};

var queueThree =
{
  "queuename":"incTestQueue3",
  "queueemail":"inctestqueue3@testdomain.com",
  "alertl1":"inctestqueue3alert1@testdomain.com",
  "alertl2":"inctestqueue3alert2@testdomain.com",
  "alertl3":"inctestqueue3alert3@testdomain.com"
};

var serviceOne = {};
var serviceTwo = {};

var citypeOne = {};
var envOne = {};
var vendorOne = {};
var locationOne = {};

var cidetailOne = {};
var cidetailTwo = {};
var cidetailThree = {};

var serviceApprovalOne =
{
  "approved": false
};

var validCreateIncident =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "severity": 1
};

var pendingTicket =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "severity": 1
};

var incidentForListing =
{
  "title": "This is a test incident ticket",
  "description": "Description of the test incident ticket",
  "severity": 1
};

var validServiceAssocParams = {};

var validIncTemplate =
{
  "templatename": "templateForIncTest",
  "type": 1,
  "title": "This is the title of the first incident template",
  "description": "This is a sample description of the incident template",
  "severity": 3,
  "impact": 3
};

var validIncTemplate2 =
{
  "templatename": "templateForIncTest2",
  "type": 1,
  "servicename": "invalidServicename",
  "title": "This is the title of the first incident template",
  "description": "This is a sample description of the incident template",
  "severity": 3,
  "impact": 3
};

var validLogglyIntegration =
{
  "subdomain": "smrutimandal",
  "integrationtype": enums.integrationService.LOGGLY
};

describe('Incident Module Unit Tests', function()
{
	before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.accTkn = res.body.acctoken;
        superUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st BDAdmin");
      }
      else
      {
        firstBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd BDAdmin");
      }
      else
      {
        secondBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd BDAdmin");
      }
      else
      {
        thirdBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 4th BDAdmin");
      }
      else
      {
        fourthBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CBOwner");
      }
      else
      {
        firstCBOwner.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd CBOwner");
      }
      else
      {
        secondCBOwner.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd CBOwner");
      }
      else
      {
        thirdCBOwner.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st Operator");
      }
      else
      {
        firstOperator.userid = res.body.userid;
        validIncTemplate.assignedusername = firstOperator.username;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd Operator");
      }
      else
      {
        secondOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd Operator");
      }
      else
      {
        thirdOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fourthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 4th Operator");
      }
      else
      {
        fourthOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fifthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 5th Operator");
      }
      else
      {
        fifthOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(sixthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 6th Operator");
      }
      else
      {
        sixthOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st User");
      }
      else
      {
        firstUser.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/bd/add')
        .set('Authorization', superUser.accTkn)
        .send({ bdname: "incbusinessdeptone" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st Business Department");
      }
      else
      {
        bdOne.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post('/bd/add')
        .set('Authorization', superUser.accTkn)
        .send({ bdname: "incbusinessdepttwo" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd Business Department");
      }
      else
      {
        bdTwo.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post('/bd/assign')
        .set('Authorization', superUser.accTkn)
        .send({ "bdid": bdOne.bdid, "admins": [ firstBDAdmin.userid, secondBDAdmin.userid ], "assign": true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign admins to 1st BD");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/bd/assign')
        .set('Authorization', superUser.accTkn)
        .send({ "bdid": bdTwo.bdid, "admins": [ thirdBDAdmin.userid, fourthBDAdmin.userid ], "assign": true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign admins to 2nd BD");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/bd/activation')
        .set('Authorization', superUser.accTkn)
        .send({ bdids: [ bdOne.bdid, bdTwo.bdid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate the BDs");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in firstBDAdmin");
      }
      else
      {
        firstBDAdmin.accTkn = res.body.acctoken;
        firstBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(secondBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in secndBDAdmin");
      }
      else
      {
        secondBDAdmin.accTkn = res.body.acctoken;
        secondBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(thirdBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in thirdBDAdmin");
      }
      else
      {
        thirdBDAdmin.accTkn = res.body.acctoken;
        thirdBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in fourthBDAdmin");
      }
      else
      {
        fourthBDAdmin.accTkn = res.body.acctoken;
        fourthBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in firstCBOwner");
      }
      else
      {
        firstCBOwner.accTkn = res.body.acctoken;
        firstCBOwner.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(secondCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in secondCBOwner");
      }
      else
      {
        secondCBOwner.accTkn = res.body.acctoken;
        secondCBOwner.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(thirdCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in thirdCBOwner");
      }
      else
      {
        thirdCBOwner.accTkn = res.body.acctoken;
        thirdCBOwner.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in first User");
      }
      else
      {
        firstUser.accTkn = res.body.acctoken;
        firstUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: bdOne.bdid, ownerid: firstCBOwner.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CB for BD 1");
      }
      else
      {
        orgApproval1 = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the 1st CB");
      }
      else
      {
        cbOne = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: bdOne.bdid, ownerid: secondCBOwner.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd CB for BD 1");
      }
      else
      {
        orgApproval1 = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', secondCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the 2nd CB");
      }
      else
      {
        cbTwo = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ bdid: bdTwo.bdid, ownerid: thirdCBOwner.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CB for BD 2");
      }
      else
      {
        orgApproval1 = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', thirdCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the 3rd CB");
      }
      else
      {
        cbThree = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/activation')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ cbids: [ cbTwo.cbid ], action: false });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate the CB 3");
      }
      else
      {
        queueOne.cbid = cbOne.cbid;
        queueOne.queueownerid = thirdBDAdmin.userid;
        queueOne.changemanagerid = thirdBDAdmin.userid;
        queueOne.srtmanagerid = thirdBDAdmin.userid;
        queueOne.incidentmanagerid = thirdBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send(queueOne);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add queue 1");
      }
      else
      {
        orgApproval1 = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 1");
      }
      else
      {
        queueOne = res.body[ 0 ];
        validIncTemplate.assignedqueueid = queueOne.queueid;
        validIncTemplate.assignedqueuename = queueOne.queuename;
        queueTwo.cbid = cbThree.cbid;
        queueTwo.queueownerid = thirdBDAdmin.userid;
        queueTwo.changemanagerid = thirdBDAdmin.userid;
        queueTwo.srtmanagerid = thirdBDAdmin.userid;
        queueTwo.incidentmanagerid = thirdBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send(queueTwo);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add queue 2");
      }
      else
      {
        orgApproval1 = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', thirdCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 2");
      }
      else
      {
        queueTwo = res.body[ 0 ];
        validIncTemplate.ownerqueueid = queueTwo.queueid;
        validIncTemplate.ownerqueuename = queueTwo.queuename;
        queueThree.cbid = cbThree.cbid;
        queueThree.queueownerid = thirdBDAdmin.userid;
        queueThree.changemanagerid = thirdBDAdmin.userid;
        queueThree.srtmanagerid = thirdBDAdmin.userid;
        queueThree.incidentmanagerid = thirdBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send(queueThree);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add queue 3");
      }
      else
      {
        orgApproval1 = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', thirdCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 3");
      }
      else
      {
        queueThree = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueid: queueOne.queueid, members: [ firstOperator.userid, secondOperator.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue 1");
      }
      else
      {
        validIncTemplate.assigneduserid = firstOperator.userid;
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ queueid: queueTwo.queueid, members: [ thirdOperator.userid, fourthOperator.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue 2");
      }
      else
      {
        validIncTemplate.owneruserid = thirdOperator.userid;
        validIncTemplate.ownerusername = thirdOperator.username;
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ queueid: queueThree.queueid, members: [ fifthOperator.userid, sixthOperator.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue 3");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in first Operator");
      }
      else
      {
        firstOperator.accTkn = res.body.acctoken;
        firstOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(secondOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in second Operator");
      }
      else
      {
        secondOperator.accTkn = res.body.acctoken;
        secondOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(thirdOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in third Operator");
      }
      else
      {
        thirdOperator.accTkn = res.body.acctoken;
        thirdOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(fourthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in fourth Operator");
      }
      else
      {
        fourthOperator.accTkn = res.body.acctoken;
        fourthOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(fifthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in fifth Operator");
      }
      else
      {
        fifthOperator.accTkn = res.body.acctoken;
        fifthOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(sixthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in sixth Operator");
      }
      else
      {
        sixthOperator.accTkn = res.body.acctoken;
        sixthOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueids: [ queueOne.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 1");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ queueids: [ queueTwo.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 2");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ queueids: [ queueThree.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 3");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ servicename: "incservice1", cbid: cbOne.cbid, details: "inc service1 details" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create service 1");
      }
      else
      {
        serviceOne = res.body;
        validServiceAssocParams.serviceids = [ serviceOne.serviceid ];
        return request(sails.hooks.http.app)
        .post('/service/add')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ servicename: "incservice2", cbid: cbThree.cbid, details: "inc service2 details" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create service 2");
      }
      else
      {
        serviceTwo = res.body;
        return request(sails.hooks.http.app)
        .post('/service/queueassign')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ serviceid: serviceOne.serviceid, supportqueue: queueOne.queueid, assign: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approvals for service 1 queue association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ approvalid: res.body.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 1 queue association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/queueassign')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ serviceid: serviceTwo.serviceid, supportqueue: queueOne.queueid, assign: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approvals for service 2 queue association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ approvalid: res.body.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 2 queue association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/next')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ serviceid: serviceOne.serviceid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approvals for service 1");
      }
      else
      {
        serviceOne = res.body;
        validIncTemplate.servicename = serviceOne.servicename;
        return Serviceapproval.findOne({ approverserviceid: serviceOne.serviceid, type: "state", isactive: true });
      }
    })
    .then(function(approval)
    {
      if(!approval)
      {
        throw new Error("Couldn't find approvals for service 1 Reg=> Approved");
      }
      else
      {
        serviceApprovalOne = approval;
        return request(sails.hooks.http.app)
        .post('/service/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ type: serviceApprovalOne.type, approvalid: serviceApprovalOne.approvalid, serviceid: serviceOne.serviceid, isapproved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 1");
      }
      else
      {
        serviceOne = res.body;
        return request(sails.hooks.http.app)
        .post('/service/next')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ serviceid: serviceTwo.serviceid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approvals for service 2");
      }
      else
      {
        serviceTwo = res.body;
        return Serviceapproval.findOne({ approverserviceid: serviceTwo.serviceid, type: "state", isactive: true });
      }
    })
    .then(function(approval)
    {
      if(!approval)
      {
        throw new Error("Couldn't find approvals for service 2 Reg=> Approved");
      }
      else
      {
        serviceApprovalOne = approval;
        return request(sails.hooks.http.app)
        .post('/service/approve')
        .set('Authorization', thirdCBOwner.accTkn)
        .send({ type: serviceApprovalOne.type, approvalid: serviceApprovalOne.approvalid, serviceid: serviceTwo.serviceid, isapproved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 2");
      }
      else
      {
        serviceTwo = res.body;
        return request(sails.hooks.http.app)
        .post('/citype/add')
        .set('Authorization', firstOperator.accTkn)
        .send({ "citypes": [ "somecitype" ] });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create citype");
      }
      else
      {
        citypeOne = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cienvironment/add')
        .set('Authorization', firstOperator.accTkn)
        .send({ "envnames": [ "staging" ] });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create environment");
      }
      else
      {
        envOne = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/vendor/add')
        .set('Authorization', firstOperator.accTkn)
        .send({ "vendornames": [ "IBM" ] });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create vendor");
      }
      else
      {
        vendorOne = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cilocation/add')
        .set('Authorization', firstOperator.accTkn)
        .send({ "locationname": "Rome-9W", "locationcode": "IT-9W-F0L", "buildingname": "EU AD", "sitecategory": "Live Site" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create location");
      }
      else
      {
        locationOne = res.body;
        return request(sails.hooks.http.app)
        .post('/cidetail/add')
        .set('Authorization', firstOperator.accTkn)
        .send({ "ciname": "HJ4K2-74JK", "assettag": "JFU5-73JD", "citypeid": citypeOne.citypeid, "envid": envOne.envid, "vendorid": vendorOne.vendorid, "ciownerid": firstUser.userid, "locationid": locationOne.locationid, "installationdate": 1480515761 });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create CI 1");
      }
      else
      {
        cidetailOne = res.body;
        validIncTemplate.ciname = cidetailOne.ciname;
        validServiceAssocParams.ciid = cidetailOne.ciid;
        return request(sails.hooks.http.app)
        .post('/cidetail/add')
        .set('Authorization', thirdOperator.accTkn)
        .send({ "ciname": "JQK3M-82L", "assettag": "HDY6-0GJM", "citypeid": citypeOne.citypeid, "envid": envOne.envid, "vendorid": vendorOne.vendorid, "ciownerid": firstUser.userid, "locationid": locationOne.locationid, "installationdate": 1480515761 });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create CI 2");
      }
      else
      {
        cidetailTwo = res.body;
        return request(sails.hooks.http.app)
        .post('/cidetail/add')
        .set('Authorization', thirdOperator.accTkn)
        .send({ "ciname": "JSDKA-23L", "assettag": "FJ72-KWDJ", "citypeid": citypeOne.citypeid, "envid": envOne.envid, "vendorid": vendorOne.vendorid, "ciownerid": firstUser.userid, "locationid": locationOne.locationid, "installationdate": 1480515761 });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create CI 3");
      }
      else
      {
        cidetailThree = res.body;
        return request(sails.hooks.http.app)
        .post('/service/assoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ "serviceids": [ serviceOne.serviceid ], "ciid": cidetailOne.ciid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approval");
      }
      else
      {
        serviceApprovalOne = res.body[ 0 ][ 0 ];
        return request(sails.hooks.http.app)
        .post('/service/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ "type": serviceApprovalOne.type, "approvalid": serviceApprovalOne.approvalid, "isapproved": true, "serviceid": serviceApprovalOne.approverserviceid, "ciid":serviceApprovalOne.ciid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 1 ci 1 association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/assoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ "serviceids": [ serviceTwo.serviceid ], "ciid": cidetailTwo.ciid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approval");
      }
      else
      {
        serviceApprovalOne = res.body[ 0 ][ 0 ];
        return request(sails.hooks.http.app)
        .post('/service/approve')
        .set('Authorization', thirdCBOwner.accTkn)
        .send({ "type": serviceApprovalOne.type, "approvalid": serviceApprovalOne.approvalid, "isapproved": true, "serviceid": serviceApprovalOne.approverserviceid, "ciid":serviceApprovalOne.ciid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 2 ci 2 association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/assoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ "serviceids": [ serviceOne.serviceid ], "ciid": cidetailThree.ciid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approval");
      }
      else
      {
        serviceApprovalOne = res.body[ 0 ][ 0 ];
        return request(sails.hooks.http.app)
        .post('/service/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ "type": serviceApprovalOne.type, "approvalid": serviceApprovalOne.approvalid, "isapproved": true, "serviceid": serviceApprovalOne.approverserviceid, "ciid":serviceApprovalOne.ciid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service 1 ci 3 association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/attachment/upload')
        .attach('attachment', 'README.md')
        .set('Authorization', firstUser.accTkn);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't upload the file");
      }
      else
      {
        attachmentOne = res.body;
        return request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validIncTemplate);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add the first template");
      }
      else
      {
        validIncTemplate.templateid = res.body.templateid;
        validIncTemplate.updatedAt = res.body.updatedAt;
        validIncTemplate.isactive = res.body.isactive;
        return request(sails.hooks.http.app)
        .post("/template/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validIncTemplate2);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add the second template");
      }
      else
      {
        validIncTemplate2.templateid = res.body.templateid;
        validIncTemplate2.updatedAt = res.body.updatedAt;
        validIncTemplate2.isactive = res.body.isactive;
        done();
      }
    })
    .catch(done);
  });

  describe('Incident Controller', function()
  {
    describe('#list()', function()
    {
      it('should not list the incidents(incorrect params) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/list")
        .set('Authorization', firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property('code').and.equal(400051);
          done();
        });
      });

      it('should list the incidents - empty list', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/list")
        .set('Authorization', firstUser.accTkn)
        .send({ page: 1, limit: 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400451);
          done();
        });
      });
    });
    describe('#add()', function()
    {
      it('should not add an incident(user without privilege) - 403502', function(done)
      {
        validCreateIncident.startedat = Math.floor(new Date() / 1000) - 3600;
        validCreateIncident.endedat = Math.floor(new Date() / 1000) - 1800;
        validCreateIncident.serviceid = serviceOne.serviceid;
        validCreateIncident.ownerqueueid = queueOne.queueid;
        validCreateIncident.owneruserid = firstOperator.userid;
        validCreateIncident.assignedqueueid = queueTwo.queueid;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstUser.accTkn)
        .send(validCreateIncident)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(403502);
          done();
        });
      });
      it('should not add an incident(contains incident state) - 400051', function(done)
      {
        validCreateIncident.state = enums.incidentStates.OPEN;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          delete validCreateIncident.state;
          done();
        });
      });
      it('should not add an incident(missing title) - 400051', function(done)
      {
        var temp = validCreateIncident.title;
        delete validCreateIncident.title;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.title = temp;
          done();
        });
      });
      it('should not add an incident(missing description) - 400051', function(done)
      {
        var temp = validCreateIncident.description;
        delete validCreateIncident.description;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.description = temp;
          done();
        });
      });
      it('should not add an incident(missing serviceid) - 400051', function(done)
      {
        var temp = validCreateIncident.serviceid;
        delete validCreateIncident.serviceid;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.serviceid = temp;
          done();
        });
      });
      it('should not add an incident(non-integer serviceid) - 400051', function(done)
      {
        var temp = validCreateIncident.serviceid;
        validCreateIncident.serviceid = 'bigservice';
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.serviceid = temp;

          done();
        });
      });
      it('should not add an incident(missing owner queue) - 400051', function(done)
      {
        var temp = validCreateIncident.ownerqueueid;
        delete validCreateIncident.ownerqueueid;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.ownerqueueid = temp;
          done();
        });
      });
      it('should not add an incident(missing owner user) - 400051', function(done)
      {
        var temp = validCreateIncident.owneruserid;
        delete validCreateIncident.owneruserid;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.owneruserid = temp;
          done();
        });
      });
      it('should not add an incident(missing assigned queue) - 400051', function(done)
      {
        var temp = validCreateIncident.assignedqueueid;
        delete validCreateIncident.assignedqueueid;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.assignedqueueid = temp;
          done();
        });
      });
      it('should not add an incident(missing startedat) - 400051', function(done)
      {
        var temp = validCreateIncident.startedat;
        delete validCreateIncident.startedat;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.startedat = temp;
          done();
        });
      });
      it('should not add an incident(earlier then epoch) - 400051', function(done)
      {
        var temp = validCreateIncident.startedat;
        validCreateIncident.startedat = -2;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.startedat = temp;
          done();
        });
      });
      it('should not add an incident(startedat in future) - 400051', function(done)
      {
        var temp = validCreateIncident.startedat;
        validCreateIncident.startedat = Math.floor(new Date() / 1000) + 86400;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.startedat = temp;
          done();
        });
      });
      it('should not add an incident(endedat is not a number)', function(done)
      {
        var temp = validCreateIncident.endedat;
        validCreateIncident.endedat = 'done';
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.endedat = temp;
          done();
        });
      });

      it('should not add an incident(endedat smaller then startedat)', function(done)
      {
        var temp = validCreateIncident.endedat;
        validCreateIncident.endedat = validCreateIncident.startedat - 3600;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.endedat = temp;
          done();
        });
      });

      it('should not add an incident(missing severity) - 400051', function(done)
      {
        var temp = validCreateIncident.severity;
        delete validCreateIncident.severity;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validCreateIncident.severity = temp;
          done();
        });
      });
      it('should not add an incident(invalid severity) - 400452', function(done)
      {
        var temp = validCreateIncident.severity;
        validCreateIncident.severity = 23423;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400452);
          validCreateIncident.severity = temp;
          done();
        });
      });
      it('should not add an incident(non-existent service) - 400362', function(done)
      {
        var temp = validCreateIncident.serviceid;
        validCreateIncident.serviceid = 23423;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400362);
          validCreateIncident.serviceid = temp;
          done();
        });
      });
      it('should not add an incident(service not in approved phase) - 400369', function(done)
      {
        Service.update({ serviceid: serviceOne.serviceid }, { servicestateid: enums.serviceStateIds.ARCHIVED })
        .then(function(service)
        {
          return request(sails.hooks.http.app)
          .post("/incident/add")
          .set('Authorization', firstOperator.accTkn)
          .send(validCreateIncident);
        })
        .then(function(res)
        {
          expect(res.status === 400);
          expect(res.body.code).to.equal(400369);
          return Service.update({ serviceid: serviceOne.serviceid }, { servicestateid: enums.serviceStateIds.APPROVED });
        })
        .then(function(services)
        {
          expect(services[ 0 ].servicestateid === enums.serviceStateIds.APPROVED);
          done();
        });
      });
      it('should not add an incident(non-existent owner queue) - 400254', function(done)
      {
        var temp = validCreateIncident.ownerqueueid;
        validCreateIncident.ownerqueueid = 23423;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400254);
          validCreateIncident.ownerqueueid = temp;
          done();
        });
      });
      it('should not add an incident(assigned queue not in active phase) - 400253', function(done)
      {
        Queue.update({ queueid: validCreateIncident.assignedqueueid }, { isactive: false })
        .then(function(queue)
        {
          return request(sails.hooks.http.app)
          .post("/incident/add")
          .set('Authorization', firstOperator.accTkn)
          .send(validCreateIncident);
        })
        .then(function(res)
        {
          expect(res.status === 400);
          expect(res.body.code).to.equal(400253);
          return Queue.update({ queueid: validCreateIncident.assignedqueueid }, { isactive: true });
        })
        .then(function(queues)
        {
          expect(queues[ 0 ].isactive === true);
          done();
        });
      });
      it('should not add an incident(inactive owning user) - 400103', function(done)
      {
        User.update({ userid: validCreateIncident.owneruserid }, { isactive: false })
        .then(function(user)
        {
          return request(sails.hooks.http.app)
          .post("/incident/add")
          .set('Authorization', firstOperator.accTkn)
          .send(validCreateIncident);
        })
        .then(function(res)
        {
          expect(res.status === 400);
          expect(res.body.code).to.equal(400103);
          return User.update({ userid: validCreateIncident.owneruserid }, { isactive: true });
        })
        .then(function(users)
        {
          expect(users[ 0 ].isactive === true);
          done();
        });
      });
      it('should not add an incident(owning user is not a member of the owner queue) - 400258', function(done)
      {
        var temp = validCreateIncident.owneruserid;
        validCreateIncident.owneruserid = 432;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400258);
          validCreateIncident.owneruserid = temp;
          done();
        });
      });
      it('should not add an incident(non-integer assigneduserid) - 400051', function(done)
      {
        validCreateIncident.assigneduserid = 'userid';
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          delete validCreateIncident.assigneduserid;
          done();
        });
      });
      it('should not add an incident(assigned user is not a member of the assigned queue) - 400258', function(done)
      {
        validCreateIncident.assigneduserid = 32;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400258);
          delete validCreateIncident.assigneduserid;
          done();
        });
      });
      it('should not add an incident(affected ciid is not running the service) - 400389', function(done)
      {
        validCreateIncident.ciid = 32;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400389);
          delete validCreateIncident.ciid;
          done();
        });
      });
      it('should not add an incident(invalid attachments) - 400405', function(done)
      {
        validCreateIncident.attachments = [ 2234, 456, 9758 ];
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400405);
          delete validCreateIncident.attachments;
          done();
        });
      });
      it('should add an incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(validCreateIncident)
        .then(function(res)
        {
          expect(res.body.state).to.equal(enums.incidentStates.OPEN);
          expect(res.body.severity).to.equal(validCreateIncident.severity);
          expect(res.body.endedat).to.equal(validCreateIncident.endedat);
          validCreateIncident = res.body;
          done();
        })
        .catch(done);
      });
      it('should list the incidents', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/list")
        .set('Authorization', firstUser.accTkn)
        .send({ page: 1, limit: 100 })
        .then(function(res)
        {
          if(res.status !== 200)
            throw new Error("Couldn't list the incidents");
          else
            done();
        })
        .catch(done);
      });
    });
    describe('#update()', function()
    {
      it('should not update the incident(requester doesn\'t have privilege) - 403502', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstUser.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, affectedci: cidetailOne.ciid })
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(403502);
          done();
        });
      });
      it('should not update the incident(doesn\'t contain incidentid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ affectedci: cidetailOne.ciid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(non-existent incidentid) - 400454', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: 123456 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400454);
          done();
        });
      });

      it('should not update the incident(contains title) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, title: "new title" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(contains description) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, description: "new desc" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(contains visibility) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, visibility: 'owner' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(contains queueid and requested state is not \'OPEN\') - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: 34534, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(contains owner queueid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, ownerqueueid: 34534 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the ownership of incident(requester doesn\'t belong to the ownerqueue - 400474', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, owneruserid: secondOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400474);
          done();
        });
      });
      it('should not update the incident(requested state is lower then REJECT) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: -3 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(requested state is above PENDINGOTHER) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: 78 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(pending due to self) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC, pendinginc: validCreateIncident.incidentid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(startedat is a string) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: 'start' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(startedat is earlier then epoch) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: -34 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(startedat is in future) - 400051', function(done)
      {
        var now = Math.floor(new Date() / 1000);
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: now + 200 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(endedat is a string) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, endedat: 'end' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(endedat is in future) - 400051', function(done)
      {
        var now = Math.floor(new Date() / 1000);
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, endedat: now + 200 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not update the incident(endedat is earlier then startedat) - 400469', function(done)
      {
        var now = Math.floor(new Date() / 1000);
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: now - 8000, endedat: now - 10000 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400469);
          done();
        });
      });
      it('should update the incident(times are correct)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: validCreateIncident.startedat, endedat: validCreateIncident.endedat })
        .then(function(res)
        {
          expect(res.body.startedat).to.equal(validCreateIncident.startedat);
          expect(res.body.endedat).to.equal(validCreateIncident.endedat);
          done();
        })
        .catch(done);
      });
      it('should not update the incident(startedat is later then endedat) - 400469', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: validCreateIncident.endedat + 1 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400469);
          done();
        });
      });
      it('should update the incident(valid startedat)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: 1000 })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.startedat).to.equal(1000);
          done();
        });
      });
      it('should not update the incident(adding logs with no loggly integration)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, logglylogs: [ { dummykey: "dummyvalue" } ] })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400479);
          done();
        });
      });
      it('should update the incident with the logs', function(done)
      {
        request(sails.hooks.http.app)
        .post("/company/getintegrationinfo")
        .set("Authorization", superUser.accTkn)
        .send(validLogglyIntegration)
        .then(function(res)
        {
          validLogglyIntegration.endpoint = res.body.endpoint;
          if(res.body.ownerqueueid)
          {
            validLogglyIntegration.ownerqueueid = res.body.ownerqueueid;
            validLogglyIntegration.owneruserid = res.body.owneruserid;
            validLogglyIntegration.servicetoalertmaps = res.body.servicetoalertmaps;
            return;
          }
          else
          {
            validLogglyIntegration.ownerqueueid = queueOne.queueid;
            validLogglyIntegration.owneruserid = firstOperator.userid;
            return request(sails.hooks.http.app)
            .post("/company/addintegration")
            .set("Authorization", superUser.accTkn)
            .send(validLogglyIntegration);
          }
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/incident/update")
          .set('Authorization', thirdOperator.accTkn)
          .send({ incidentid: validCreateIncident.incidentid, logglylogs: [ { dummykey: "dummyvalue" } ] });
        })
        .then(function(res)
        {
          expect(res.body.logglylogs[ 0 ][ 0 ].dummykey).equal("dummyvalue");
          return request(sails.hooks.http.app)
          .post("/company/removeintegration")
          .set("Authorization", superUser.accTkn)
          .send(validLogglyIntegration);
        })
        .then(function(res)
        {
          expect(res.body[ 0 ].integrationtype).to.equal(validLogglyIntegration.integrationtype);
          done();
        })
        .catch(done);
      });
      it('should not update the incident(endedat is erlier then startedat) - 400469', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, endedat: 1 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400469);
          done();
        });
      });
      it('should update the incident(times are correct)', function(done)
      {
        var now = Math.floor(new Date() / 1000);
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: now - 239100, endedat: now })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.startedat).to.equal(now - 239100);
          expect(res.body.endedat).to.equal(now);
          done();
        });
      });
      it('should not update the incident(moving to OPEN state and missig assignedqueueid) - 400459', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400459);
          done();
        });
      });
      it('should not update the incident(moving to open state with the same queue) - 400475', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueTwo.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400475);
          done();
        });
      });
      it('should not update the incident(incorrect operator attempting update) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueTwo.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should update the incident(assigning the ticket to another queue - without state)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueThree.queueid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.OPEN);
          expect(res.body.assignedqueueid).to.equal(queueThree.queueid);
          done();
        });
      });
      it('should not update the incident(editing the ticket as a member of the owner queue while the ticket with operator) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueTwo.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should not update the incident(adding colosure comments as a member of the owner queue while the ticket with operator) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, attachments: [ attachmentOne.attachmentid ], closurecomments: 'bye bye' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should not update the incident(assigning the ticket to another queue - with non-OPEN state) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assignedqueueid: queueTwo.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should update the incident(assigning the ticket to another queue - with OPEN state)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueOne.queueid })
        .then(function(res)
        {
          if(res.status !== 200)
            throw new Error("Couldn't update the incident");
          else
          {
            expect(res.body.assignedqueueid).to.equal(queueOne.queueid);
            return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', firstOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueTwo.queueid });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
            throw new Error("Couldn't update the incident");
          else
          {
            done();
          }
        });
      });
      it('should not update the incident(assigning the ticket to a non-member of the assigned queue) - 400258', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assigneduserid: secondOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400258);
          done();
        });
      });
      it('should not update the incident(non-existent document) - 400405', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, attachments: [ 12345 ] })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400405);
          done();
        });
      });
      it('should update the incident(attaching a document)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, attachments: [ attachmentOne.attachmentid ] })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          done();
        });
      });
      it('should not update the incident(list contains duplicate attachments) - 400406', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, attachments: [ attachmentOne.attachmentid, attachmentOne.attachmentid ] })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400406);
          done();
        });
      });
      it('should update the incident(attachments have been removed)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, attachments: [] })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          done();
        });
      });

      it('should update the incident(assigning the ticket to a member of the assigned queue)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assigneduserid: thirdOperator.userid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.assigneduserid).to.equal(thirdOperator.userid);
          done();
        });
      });
      it('should not update the incident(assigning the ticket to the same member) - 400468', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assigneduserid: thirdOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400468);
          done();
        });
      });
      it('should not reject the incident(rejecting as not a member of the assigned queue) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, journal: "Rejecting the incident" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should not reject the incident(rejecting as a member of the assigned queue without a comment) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should reject the incident(rejecting as a member of the assigned queue with a comment)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, journal: "Rejecting the incident" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(1);
          expect(res.body.assignedqueueid).to.equal(res.body.ownerqueueid);
          expect(res.body.assigneduserid).to.equal(res.body.assigneduserid);
          expect(res.body.visibility).to.equal('owner');
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to WORKINPROGRESS (the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in REJECT state) 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR(the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER(the ticket is in REJECT state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not open the incident(updating as a non-member of the owner) - 400470', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueTwo.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400470);
          done();
        });
      });
      it('should not open the incident(assigning to a non-existent queue and non-existent member) - 400254', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: 22433, assigneduserid: 12345 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400254);
          done();
        });
      });
      it('should not open the incident(assigning to an inactive queue and a non-member as assigneduser) - 400253', function(done)
      {
        Queue.update({ queueid: queueTwo.queueid }, { isactive: false })
        .then(function(queues)
        {
          expect(queues[ 0 ].isactive).to.equal(false);
          return request(sails.hooks.http.app)
          .post("/incident/update")
          .set('Authorization', firstOperator.accTkn)
          .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queues[ 0 ].queueid, assigneduserid: firstOperator.userid });
        })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400253);
          return Queue.update({ queueid: queueTwo.queueid }, { isactive: true });
        })
        .then(function(queues)
        {
          expect(queues[ 0 ].isactive).to.equal(true);
          done();
        });
      });
      it('should not open the incident(assigning to a valid queue and a non-member as assigneduser) - 400258', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueTwo.queueid, assigneduserid: firstOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400258);
          done();
        });
      });
      it('should not open the incident(assigning to a non-existent queue) - 400254', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: 22433 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400254);
          done();
        });
      });
      it('should open the incident(assigning to a valid queue)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueTwo.queueid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.OPEN);
          expect(res.body.assignedqueueid).to.equal(res.body.assignedqueueid);
          expect(res.body.assigneduserid).to.equal(null);
          done();
        });
      });
      it('should not move the ticket to WORKINPROGRESS (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in OPEN state) 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in OPEN state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is missing assigned user) - 400458', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400458);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the user is not a member of the queue) - 400258', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assigneduserid: fifthOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400258);
          done();
        });
      });
      it('should assign an user and move the ticket to ASSIGNED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assigneduserid: thirdOperator.userid, startedat: validCreateIncident.startedat + 1 })
        .then(function(res)
        {
          expect(res.body.state).to.equal(enums.incidentStates.ASSIGNED);
          expect(res.body.assigneduserid).to.equal(thirdOperator.userid);
          done();
        })
        .catch(done);
      });
      it('should not move the ticket to OPEN(with state as OPEN and same assignedqueueid) - 400475', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueTwo.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400475);
          done();
        });
      });
      it('should not move the ticket to OPEN(with state as OPEN and no assignedqueueid) - 400459', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400459);
          done();
        });
      });
      it('should not move the ticket to OPEN(requester is not part of the assigned queue) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueThree.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should move the ticket to OPEN(new queue)=>OPEN(original queue)=>ASSIGNED(new asisgned user)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueThree.queueid })
        .then(function(res)
        {
          expect(res.body.state).to.equal(2);
          expect(res.body.assignedqueueid).to.equal(queueThree.queueid);
          expect(res.body.assigneduserid).to.equal(null);
          return request(sails.hooks.http.app)
          .post("/incident/update")
          .set('Authorization', fifthOperator.accTkn)
          .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueTwo.queueid });
        })
        .then(function(res)
        {
          expect(res.body.state).to.equal(2);
          expect(res.body.assignedqueueid).to.equal(queueTwo.queueid);
          expect(res.body.assigneduserid).to.equal(null);
          return request(sails.hooks.http.app)
          .post("/incident/update")
          .set('Authorization', thirdOperator.accTkn)
          .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assigneduserid: thirdOperator.userid });
        })
        .then(function(res)
        {
          expect(res.body.state).to.equal(3);
          expect(res.body.assigneduserid).to.equal(thirdOperator.userid);
          done();
        })
        .catch(done);
      });
      it('should not move the ticket to REJECT(input is missing comment) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should not move the ticket to REJECT(input contains assignedqueueid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, assignedqueueid: queueThree.queueid, journal: "don't like this ticket" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not move the ticket to REJECT(input contains assigneduserid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, assigneduserid: firstOperator.userid, journal: "don't like this ticket" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not move the ticket to REJECT(input contains ownerqueueid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, ownerqueueid: queueOne.queueid, journal: "don't like this ticket" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not move the ticket to REJECT(requester is not part of assigned queue) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, journal: "don't like this ticket" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should move the ticket to REJECT', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, journal: "don't like this ticket" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(1);
          expect(res.body.assignedqueueid).to.equal(res.body.ownerqueueid);
          expect(res.body.assigneduserid).to.equal(res.body.owneruserid);
          expect(res.body.visibility).to.equal('owner');
          done();
        });
      });
      it('should not update the ticket(requester doesn\'t belong to the owner queue) - 400470', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400470);
          done();
        });
      });
      it('should move the ticket to OPEN', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueTwo.queueid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(2);
          expect(res.body.assignedqueueid).to.equal(queueTwo.queueid);
          done();
        });
      });
      it('should move the ticket to ASSIGNED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assigneduserid: thirdOperator.userid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(3);
          expect(res.body.assigneduserid).to.equal(thirdOperator.userid);
          done();
        });
      });
      it('should not update(CI doesn\'t run the service) - 400389', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, ciid: cidetailTwo.ciid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400389);
          done();
        });
      });
      it('should not update(CI is in Registration state) - 400387', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, ciid: cidetailOne.ciid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400387);
          done();
        });
      });
      it('should update(CI runs on the service)', function(done)
      {
        request(sails.hooks.http.app)
          .post("/cidetail/next")
          .set("Authorization", firstOperator.accTkn)
          .send({ ciid: cidetailOne.ciid })
        .then(function(res)
        {
          expect(res.body).to.have.property("ciid").and.equal(cidetailOne.ciid);
          expect(res.body).to.have.property("cistateid").and.equal(enums.ciStateIds.APPROVED);
          expect(res.body).to.have.property("isfrozen").and.equal(false);
          return request(sails.hooks.http.app)
          .post("/incident/update")
          .set('Authorization', thirdOperator.accTkn)
          .send({ incidentid: validCreateIncident.incidentid, ciid: cidetailOne.ciid });
        })
        .then(function(res)
        {
          expect(res.body.ciid).to.equal(cidetailOne.ciid);
          done();
        })
        .catch(done);
      });
      it('#CI getdetail - should list the incident in the detail', function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/getdetail")
        .set('Authorization', thirdOperator.accTkn)
        .send({ ciid: cidetailOne.ciid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.incidents).to.have.length.above(0);
          done();
        });
      });
      it('#Service Archival request - should not archive(service has open incidents) - 400211', function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/next")
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ serviceid: serviceOne.serviceid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400211);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in ASSIGNED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should move the ticket to WORKINPROGRESS', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(4);
          done();
        });
      });
      it('should not change the severity(invalid severity code)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, severity: 1234 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should change the severity', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, severity: enums.incidentSeverity.MEDIUM })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.severity).to.equal(enums.incidentSeverity.MEDIUM);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC(missing pending ticket number) - 400466', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400466);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC(non-existent pending ticket number) - 400454', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC, pendinginc: 34252 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400454);
          done();
        });
      });
      it('should move the ticket to PENDINGINC', function(done)
      {
        pendingTicket.serviceid = serviceOne.serviceid;
        pendingTicket.ownerqueueid = queueTwo.queueid;
        pendingTicket.owneruserid = thirdOperator.userid;
        pendingTicket.assignedqueueid = queueThree.queueid;
        pendingTicket.assigneduserid = fifthOperator.userid;
        pendingTicket.startedat = validCreateIncident.startedat;
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', thirdOperator.accTkn)
        .send(pendingTicket)
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't create the pending incident");
          }
          else
          {
            expect(res.body.state).to.equal(2);
            pendingTicket = res.body;
            return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', thirdOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC, pendinginc: pendingTicket.incidentid });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
            throw new Error("Couldn't move the ticket to pending incident state");
          else
          {
            expect(res.body.pendinginc).to.equal(pendingTicket.incidentid);
            done();
          }
        })
        .catch(done);
      });
      it('should not move the ticket to REJECT (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in PENDINGINC state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should move the ticket to WORKINPROGRESS', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
          expect(res.body.pendinginc).to.equal(null);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER(missing the journal entry) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should move the ticket to PENDINGCUSTOMER', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER, journal: "need info from customer" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.PENDINGCUSTOMER);
          expect(res.body.visibility).to.equal('owner');
          done();
        });
      });
      it('should not update the startedat(requester is not part of the owner queue) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, start: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should not move the ticket to REJECT (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in PENDINGCUSTOMER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to WORKINPROGRESS(requester is not part of the owner queue) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS, journal: "info gathered" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should not move the ticket to WORKINPROGRESS(missing journal) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should move the ticket to WORKINPROGRESS', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS, journal: "info gathered" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
          expect(res.body.visibility).to.equal('operator');
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR(missing vendorid) - 400467', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400467);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR(non-existent vendorid) - 400332', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR, vendorid: 341, vendorsr: "imaginary sr" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400332);
          done();
        });
      });
      it('should move the ticket to PENDINGVENDOR', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR, vendorid: vendorOne.vendorid, vendorsr: "imaginary sr" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.PENDINGVENDOR);
          expect(res.body.vendorid).to.equal(vendorOne.vendorid);
          done();
        });
      });
      it('should not move the ticket to REJECT (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in PENDINGVENDOR state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should move the ticket to WORKINPROGRESS', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
          expect(res.body.vendorid).to.equal(null);
          expect(res.body.vendorsr).to.equal(null);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (missing journal entry) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should move the ticket to PENDINGOTHER', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER, journal: "pending other" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.PENDINGOTHER);
          done();
        });
      });
      it('should not move the ticket to REJECT (the ticket is in PENDINGOTHER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in PENDINGOTHER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED (the ticket is in PENDINGOTHER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in PENDINGOTHER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in PENDINGOTHER state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should move the ticket to WORKINPROGRESS', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
          done();
        });
      });
      it('should make journal entry as operator', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fourthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, journal: "making a journal entry as operator in WIP state" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          done();
        });
      });
      it('should make journal entry as owner', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', secondOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, journal: "making a journal entry as owner in WIP state" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          done();
        });
      });
      it('should not make journal entry as by-stander - 400470', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', sixthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, journal: "making a journal entry as by-stander in WIP state" })
        .expect(400)
        .end(function(err, res)
        {
          expect(res.body.code).to.equal(400470);
          if(err) return done(err);
          done();
        });
      });
      it('should reject the ticket(WIP=>REJECT=>OPEN=>ASSIGNED=>WIP)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT, journal: "not my service" })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't reject the ticket in WIP state");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.REJECT);
            expect(res.body.assignedqueueid).to.equal(res.body.ownerqueueid);
            expect(res.body.assigneduserid).to.equal(res.body.owneruserid);
            expect(res.body.visibility).to.equal('owner');
             return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', firstOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.OPEN, assignedqueueid: queueTwo.queueid });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't OPEN the ticket in REJECT state");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.OPEN);
            expect(res.body.assignedqueueid).to.equal(queueTwo.queueid);
            expect(res.body.assigneduserid).to.equal(null);
            expect(res.body.visibility).to.equal('operator');
             return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', thirdOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assigneduserid: thirdOperator.userid });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't ASSIGN the ticket in OPEN state");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.ASSIGNED);
            expect(res.body.assigneduserid).to.equal(thirdOperator.userid);
             return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', thirdOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS, assigneduserid: fourthOperator.userid });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't ASSIGN the ticket in OPEN state");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
            done();
          }
        })
        .catch(done);
      });
      it('should not move the ticket to ASSIGNED (the ticket is in WORKINPROGRESS state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED (the ticket is in WORKINPROGRESS state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to CLOSED (the ticket is in WORKINPROGRESS state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to RESOLVED state(missing resolution code, resolution notes and endedat) - 400462', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400462);
          done();
        });
      });
      it('should not move the ticket to RESOLVED state(invalid resolutioncode) - 400462', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: 34, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400462);
          done();
        });
      });
      it('should not move the ticket to RESOLVED state(endedat earlier then startedat) - 400469', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.RESOLVED, resolutionnotes: "resolved", endedat: 0 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400469);
          done();
        });
      });
      it('should move the ticket to RESOLVED state', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, assigneduserid: thirdOperator.userid, resolutioncode: enums.incidentResCodes.RESOLVED, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.RESOLVED);
          expect(res.body.visibility).to.equal('owner');
          expect(res.body.resolutioncode).to.not.equal(null);
          expect(res.body.resolutionnotes).to.not.equal(null);
          expect(res.body.resolvedat).to.not.equal(null);
          done();
        });
      });
      it('should not update the ticket(affected CI cannot be changd beyond WIP state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, ciid: cidetailThree.ciid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(requested owner is not a member of the owner queue) - 400258', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, owneruserid: thirdOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400258);
          done();
        });
      });
      it('should update the ticket(owneruserid can be changed)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, owneruserid: firstOperator.userid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.owneruserid).to.equal(firstOperator.userid);
          done();
        });
      });
      it('should not update the ticket(assigneduserid cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assigneduserid: fourthOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(startedat cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: 12345 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(endedat cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, endedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(severity cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, severity: enums.incidentSeverity.MEDIUM })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(vendorid cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, vendorid: vendorOne.vendorid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(vendorsr cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, vendorsr: 'srsr' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(pendinginc cannot be changed in RESOLVED state) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, pendinginc: pendingTicket.incidentid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not move the ticket to REJECT (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to OPEN (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueThree.queueid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to WORKINPROGRESS (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC, pendinginc: pendingTicket.incidentid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR, vendorid: vendorOne.vendorid, vendorsr: "srsr" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER, journal: "entry" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in RESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED(missing journal entry) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should not move the ticket to UNRESOLVED(updating other attributes) - 400463', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED, journal: 'entry', resolutionnotes: 'ghar jana hai' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400463);
          done();
        });
      });
      it('should move the ticket to UNRESOLVED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED, journal: 'entry' })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.UNRESOLVED);
          expect(res.body.visibility).to.equal('operator');
          expect(res.body.resolutioncode).to.equal(null);
          expect(res.body.resolutionnotes).to.equal(null);
          expect(res.body.resolvedat).to.equal(null);
          done();
        });
      });
      it('should not update the ticket(assigneduserid cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assigneduserid: fourthOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(startedat cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: 12345 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(endedat cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, endedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(severity cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, severity: enums.incidentSeverity.MEDIUM })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(vendor cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, vendorid: vendorOne.vendorid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(vendorsr cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, vendorsr: 'sr sr' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not update the ticket(pendinginc cannot be updated at this point) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, pendinginc: pendingTicket.incidentid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not move the ticket to REJECT (the ticket is in UNRESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.REJECT })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to ASSIGNED (the ticket is in UNRESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGINC (the ticket is in UNRESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGINC })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGCUSTOMER (the ticket is in UNRESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGCUSTOMER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGVENDOR (the ticket is in UNRESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGVENDOR })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should not move the ticket to PENDINGOTHER (the ticket is in UNRESOLVED state) - 400456', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.PENDINGOTHER })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400456);
          done();
        });
      });
      it('should move the ticket to OPEN (UNRESOLVED=>OPEN=>ASSIGNED=>WORKINPROGRESS=>RESOLVED=>UNRESOLVED)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assignedqueueid: queueThree.queueid })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't move to OPEN");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.OPEN);
            expect(res.body.assignedqueueid).to.equal(queueThree.queueid);
            expect(res.body.visibility).to.equal('operator');
            return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', fifthOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.ASSIGNED, assigneduserid: fifthOperator.userid });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't move to ASSIGNED");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.ASSIGNED);
            expect(res.body.assignedqueueid).to.equal(queueThree.queueid);
            expect(res.body.visibility).to.equal('operator');
            return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', fifthOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't move to WORKINPROGRESS");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
            expect(res.body.assignedqueueid).to.equal(queueThree.queueid);
            expect(res.body.visibility).to.equal('operator');
            return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', fifthOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.RESOLVED, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't move to RESOLVED");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.RESOLVED);
            expect(res.body.assignedqueueid).to.equal(queueThree.queueid);
            expect(res.body.visibility).to.equal('owner');
            return request(sails.hooks.http.app)
            .post("/incident/update")
            .set('Authorization', firstOperator.accTkn)
            .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED, journal: "jam nahi raha hai" });
          }
        })
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't move to UNRESOLVED");
          }
          else
          {
            done();
          }
        })
        .catch(done);
      });
      it('should move the ticket to RESOLVED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.RESOLVED, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.RESOLVED);
          done();
        });
      });
      it('should move the ticket to UNRESOLVED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.UNRESOLVED, journal: 'entry' })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.UNRESOLVED);
          expect(res.body.visibility).to.equal('operator');
          done();
        });
      });
      it('should move the ticket to WORKINPROGRESS', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.WORKINPROGRESS })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.WORKINPROGRESS);
          done();
        });
      });
      it('should not move the ticket to RESOLVED(Resoluton code DUPTICKET demands a duplicate ticket) - 400478', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.DUPTICKET, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400478);
          done();
        });
      });
      it('should not move the ticket to RESOLVED(duplicate ticket must not be the ticket itself) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.DUPTICKET, dupticket: validCreateIncident.incidentid, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not move the ticket to RESOLVED(duplicate ticket must be a valid ticket) - 400454', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.DUPTICKET, dupticket: 146, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400454);
          done();
        });
      });

      it('should move the ticket to RESOLVED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', fifthOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.RESOLVED, resolutioncode: enums.incidentResCodes.DUPTICKET, dupticket: pendingTicket.incidentid, resolutionnotes: "resolved", endedat: Math.floor(new Date() / 1000) })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.dupticket).to.equal(pendingTicket.incidentid);
          expect(res.body.state).to.equal(enums.incidentStates.RESOLVED);
          done();
        });
      });
      it('should not move the ticket to CLOSED(missing closurecomments) - 400464', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400464);
          done();
        });
      });
      it('should not update the ticket(updating other details) - 400476', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, assigneduserid: sixthOperator.userid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400476);
          done();
        });
      });
      it('should not move the ticket to CLOSED(updating other details) - 400465', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED, closurecomments: "closing", journal: 'sahi main ghar jana hai' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400465);
          done();
        });
      });
      it('should not move the ticket to CLOSED(invalid rating) - 400477', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED, closurecomments: "closing", rating: 44 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400477);
          done();
        });
      });
      var numOfIncs;
      it('#Service - getdetail() should list the open incidents in service getdetail', function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/getdetail")
        .set('Authorization', firstOperator.accTkn)
        .send({ serviceid: validCreateIncident.serviceid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.incidents).to.have.length.above(0);
          numOfIncs = res.body.incidents.length;
          return done();
        });
      });
      it('should move the ticket to CLOSED', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, state: enums.incidentStates.CLOSED, closurecomments: "closing", rating: enums.incSatisfactionRating.SATISFIED })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.CLOSED);
          expect(res.body.rating).to.equal(enums.incSatisfactionRating.SATISFIED);
          expect(res.body.visibility).to.equal(null);
          done();
        });
      });
      it('#Service - getdetail() should list the open incidents in service getdetail - should be one less', function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/getdetail")
        .set('Authorization', firstOperator.accTkn)
        .send({ serviceid: validCreateIncident.serviceid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.incidents).to.have.lengthOf(numOfIncs - 1);
          return done();
        });
      });

      it('should not update(the ticket is in CLOSED state) - 400455', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/update")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: validCreateIncident.incidentid, startedat: 12345 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400455);
          done();
        });
      });
    });
    describe('#cancel()', function()
    {
      it('should not cancel the incident(requester doesn\'t have the privilege) - 403502', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/cancel")
        .set('Authorization', firstUser.accTkn)
        .send({ incidentid: pendingTicket.incidentid, journal: "Self-healed" })
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(403502);
          done();
        });
      });
      it('should not cancel the incident(input missing incidentid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/cancel")
        .set('Authorization', thirdOperator.accTkn)
        .send({ journal: "Self-healed" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not cancel the incident(input missing journal) - 400461', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/cancel")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: pendingTicket.incidentid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400461);
          done();
        });
      });
      it('should not cancel the incident(invalid incidentid) - 400454', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/cancel")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: 12345, journal: "Self-healed" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400454);
          done();
        });
      });
      it('should not cancel the incident(incident is already resolved) - 400473', function(done)
      {
        Incident.update({ incidentid: pendingTicket.incidentid }, { state: enums.incidentStates.RESOLVED })
        .then(function(incidents)
        {
          expect(incidents[ 0 ].incidentid).to.equal(pendingTicket.incidentid);
          expect(incidents[ 0 ].state).to.equal(enums.incidentStates.RESOLVED);
          return request(sails.hooks.http.app)
          .post("/incident/cancel")
          .set('Authorization', thirdOperator.accTkn)
          .send({ incidentid: pendingTicket.incidentid, journal: "Self-healed" });
        })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400473);
          return Incident.update({ incidentid: pendingTicket.incidentid }, { state: enums.incidentStates.OPEN });
        })
        .then(function(incidents)
        {
          expect(incidents[ 0 ].incidentid).to.equal(pendingTicket.incidentid);
          expect(incidents[ 0 ].state).to.equal(enums.incidentStates.OPEN);
          done();
        })
        .catch(done);
      });
      it('should not cancel the incident(requester is not a member of the owner queue) - 400471', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/cancel")
        .set('Authorization', firstOperator.accTkn)
        .send({ incidentid: pendingTicket.incidentid, journal: "Self-healed" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400471);
          done();
        });
      });
      it('should cancel the incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/cancel")
        .set('Authorization', thirdOperator.accTkn)
        .send({ incidentid: pendingTicket.incidentid, journal: "Self-healed" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.state).to.equal(enums.incidentStates.CLOSED);
          expect(res.body.resolutioncode).to.equal(enums.incidentResCodes.CANCELLED);
          expect(res.body.visibility).to.equal(null);
          done();
        });
      });
    });
    //Describe('#relateTickets()', function()
    //{
    //  it('should not relate the ticket(requester doesn\'t have the privilege) - 403502', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', firstUser.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: { incidents: [ pendingTicket.incidentid ] } })
    //    .expect(403)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(403502);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(missing the incidentid) - 400051', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ tickets: { incidents: [ pendingTicket.incidentid ] } })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400051);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(missing the tickets to relate) - 400051', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400051);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(missing the incidents collection to relate) - 400051', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: {} })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400051);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(the incidents to relate is not a list) - 400051', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: { incidents: 'id' } })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400051);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(the list of incidents to relate is empty) - 400051', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: { incidents: [] } })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400051);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(the list of incidents to relate contains the incidentid) - 400051', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: { incidents: [ pendingTicket.incidentid, validCreateIncident.incidentid ] } })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400051);
    //      done();
    //    });
    //  });
    //  it('should not relate the ticket(the list of incidents to relate contains non-existent incidentids) - 400454', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: { incidents: [ pendingTicket.incidentid, 12345 ] } })
    //    .expect(400)
    //    .end(function(err, res)
    //    {
    //      if(err) return done(err);
    //      expect(res.body.code).to.equal(400454);
    //      done();
    //    });
    //  });
    //  it('should relate the tickets', function(done)
    //  {
    //    request(sails.hooks.http.app)
    //    .post("/incident/relatetickets")
    //    .set('Authorization', thirdOperator.accTkn)
    //    .send({ incidentid: validCreateIncident.incidentid, tickets: { incidents: [ pendingTicket.incidentid ] } })
    //    .then(function(res)
    //    {
    //      if(res.status !== 200)
    //      {
    //        throw new Error("Couldn't relate the incident");
    //      }
    //      else
    //      {
    //        return Incident.findOne({ incidentid: validCreateIncident.incidentid }).populate('relatedinc');
    //      }
    //    })
    //    .then(function(incident)
    //    {
    //      expect(incident.relatedinc[ 0 ].incidentid).to.equal(pendingTicket.incidentid);
    //      done();
    //    })
    //    .catch(done);
    //  });
    //});
    describe('#getdetail()', function()
    {
      it('should not get the details of the incident(missing incidentid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/getdetail")
        .set('Authorization', firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not get the details of the incident(non-integer incidentid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/getdetail")
        .set('Authorization', firstUser.accTkn)
        .send({ incidentid: "id" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not get the details of the incident(non-integer incidentid) - 400454', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/getdetail")
        .set('Authorization', firstUser.accTkn)
        .send({ incidentid: 12345 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400454);
          done();
        });
      });
      it('should get the details of the incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/getdetail")
        .set('Authorization', firstUser.accTkn)
        .send({ incidentid: validCreateIncident.incidentid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.incidentid).to.equal(validCreateIncident.incidentid);
          done();
        });
      });
    });
    describe('#userlist()', function()
    {
      it('should not get the list of the incident(missing userid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/userlist")
        .set('Authorization', firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not get the list of the incident(the user doesn\'t have any incidents assigned to him)', function(done)
      {
        incidentForListing.serviceid = serviceOne.serviceid;
        incidentForListing.ownerqueueid = queueOne.queueid;
        incidentForListing.owneruserid = firstOperator.userid;
        incidentForListing.assignedqueueid = queueTwo.queueid;
        incidentForListing.assigneduserid = thirdOperator.userid;
        incidentForListing.startedat = Math.floor(new Date() / 1000);
        request(sails.hooks.http.app)
        .post("/incident/add")
        .set('Authorization', firstOperator.accTkn)
        .send(incidentForListing)
        .then(function(res)
        {
          if(res.status !== 200)
          {
            throw new Error("Couldn't create the incident for listing");
          }
          else
          {
            expect(res.body.state).to.equal(enums.incidentStates.OPEN);
            expect(res.body.assignedqueueid).to.equal(queueTwo.queueid);
            expect(res.body.assigneduserid).to.equal(thirdOperator.userid);
            expect(res.body.visibility).to.equal('operator');
            incidentForListing = res.body;
            return request(sails.hooks.http.app)
            .post("/incident/userlist")
            .set('Authorization', firstOperator.accTkn)
            .send({ userid: firstOperator.userid });
          }
        })
        .then(function(res)
        {
          expect(res.body).to.have.lengthOf(0);
          done();
        })
        .catch(done);
      });
      it('should get the list of the incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/userlist")
        .set('Authorization', firstUser.accTkn)
        .send({ userid: thirdOperator.userid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ].assigneduserid).to.equal(thirdOperator.userid);
          done();
        });
      });
    });
    describe('#queuelist()', function()
    {
      it('should not get the list of the incident(missing queueid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/queuelist")
        .set('Authorization', firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not get the list of the incident(queue doesn\'t have any incidents assigned to it)', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/queuelist")
        .set('Authorization', firstUser.accTkn)
        .send({ queueid: queueOne.queueid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.lengthOf(0);
          done();
        });
      });
      it('should get the list of the incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/queuelist")
        .set('Authorization', firstUser.accTkn)
        .send({ queueid: queueTwo.queueid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ].assignedqueueid).to.equal(queueTwo.queueid);
          done();
        });
      });
    });
    describe('#search()', function()
    {
      it('should not search the incident(search parameter is missing) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/search")
        .set('Authorization', firstUser.accTkn)
        .send({ "result": [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid" ], "orderby": "incidentid" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not search the incident(result columns are missing) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/search")
        .set('Authorization', firstUser.accTkn)
        .send({ "search":  { "state": enums.incidentStates.OPEN }, "orderby": "incidentid" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not search the incident(order by advice are missing) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/search")
        .set('Authorization', firstUser.accTkn)
        .send({ "search":  { "state": enums.incidentStates.OPEN }, "result": [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid" ] })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          done();
        });
      });
      it('should not search the incident(invalid result parameter) - 400053', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/search")
        .set('Authorization', firstUser.accTkn)
        .send({ "search":  { "panilevel": 'very high' }, "result": [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid" ], "orderby": "incidentid" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400053);
          done();
        });
      });
      it('should not find any incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/search")
        .set('Authorization', firstUser.accTkn)
        .send({ "search":  { "state": enums.incidentStates.WORKINPROGRESS }, "result": [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid" ], "orderby": "incidentid" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.equal(0);
          done();
        });
      });
      it('should search the incident', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/search")
        .set('Authorization', firstUser.accTkn)
        .send({ "search":  { "state": enums.incidentStates.OPEN }, "result": [ "incidentid", "title", "state", "assignedqueueid", "assigneduserid" ], "orderby": "incidentid" })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 1 ][ 0 ].state).to.equal(enums.incidentStates.OPEN);
          done();
        });
      });
    });
    describe('#addbytemplate()', function()
    {
      it('should not add an incident by template(missing templateid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/addbytemplate")
        .set('Authorization', firstOperator.accTkn)
        .send({ templatenumber: validIncTemplate.templateid })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400051);
          done();
        })
        .catch(done);
      });
      it('should not add an incident by template(invalid templateid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/addbytemplate")
        .set('Authorization', firstOperator.accTkn)
        .send({ templateid: 13268 })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400501);
          done();
        })
        .catch(done);
      });
      it('should not add an incident by template(invalid keys) - 400502', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/addbytemplate")
        .set('Authorization', firstOperator.accTkn)
        .send({ templateid: validIncTemplate2.templateid })
        .then(function(res)
        {
          expect(res.body.code).to.equal(400502);
          done();
        })
        .catch(done);
      });
      it('should add an incident by template', function(done)
      {
        request(sails.hooks.http.app)
        .post("/incident/addbytemplate")
        .set('Authorization', firstOperator.accTkn)
        .send({ templateid: validIncTemplate.templateid })
        .then(function(res)
        {
          expect(res.body.title).to.equal(validIncTemplate.title);
          done();
        })
        .catch(done);
      });
    });
  });
});
