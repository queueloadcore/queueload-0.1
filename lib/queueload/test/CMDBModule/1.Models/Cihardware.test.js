/* FileName: 10.Cihardware.test.js
 * Description: This file defines all the unit test cases for the Cihardware Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 19/03/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("CI Hardware Model", function()
{
	describe("#Create", function()
	{
		var validHardware =
		{
			"manufacturervendorid": 8,
      "hardwaretag": "FK3JNB693QD",
			"rack": "119",
			"rackunit": "4"
		};

		var inValidHardware =
		{
      "hardwaretag": "F @",
			"manufacturerid": "Hewlett P@ck@rd",
			"rack": "#119",
			"rackunit": "(80)"
		};

		it("should create a cihardware in the system", function()
		{
			return Cihardware.create(validHardware)
			.then(function hardwareCreated(hardware)
			{
				expect(hardware).to.exist;
			});
		});

		it("should not create a cihardware in the system", function()
		{
			return Cihardware.create(inValidHardware)
			.then(function hardwareCreated(hardware)
			{
				expect(hardware).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
				var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(parseInt(numOfAttributes)).to.equal(4);
			});
		});
	});

	describe("#Update", function()
	{
		var validUpdate =
		{
			"rack": "14C"
		};

		var invalidUpdate =
		{
			"osversion": "Server 2014 R4"
		};

		it("should update a cihardware in the system", function()
		{
			return Cihardware.update({ hardwaretag: "FK3JNB693QD" }, validUpdate)
			.then(function updateCihardware(hardware)
			{
				expect(hardware).to.exist;
				expect(hardware[ 0 ]).to.have.property("rack").and.equal(validUpdate.rack);
			});
		});

		it("should not update a cihardware in the system", function()
		{
			return Cihardware.update({ hardwaretag: "FK3JNB693Q" }, invalidUpdate)
			.then(function updateCihardware(hardware)
			{
				expect(hardware).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
			});
		});
	});

	describe("#Destroy", function()
	{
		it("should not delete a cihardware from the system", function()
		{
			return Cihardware.destroy({ hardwaretag: "INVALIDTAG" })
			.then(function destroyed(hardware)
			{
				return Cihardware.find({ hardwaretag: "FK3JNB693QD" });
			})
			.then(function found(hardware)
			{
				expect(hardware).not.to.be.empty;
			});
		});

		it("should delete a cihardware from the system", function()
		{
			return Cihardware.destroy({ hardwaretag: "FK3JNB693QD" })
			.then(function destroyed(hardware)
			{
				return Cihardware.find({ hardwaretag: "FK3JNB693QD" });
			})
			.then(function found(hardware)
			{
				expect(hardware).to.be.empty;
			});
		});
	});
});
