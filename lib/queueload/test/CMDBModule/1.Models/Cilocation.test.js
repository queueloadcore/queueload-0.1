/* FileName: 9.Cilocation.test.js
 * Description: This file defines all the unit test cases for the Cilocation Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 18/03/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("Cilocation Model", function()
{
	describe("#Create", function()
	{
		var validCiLocation =
		{
			"locationname": "HSR Layout",
			"locationcode": "INDBLRHSR 8"
		};
		var invalidCilocation =
		{
      //Missing Location Code
			"locationname": "Invalid@Name"
		};
		it("should create a cilocation in the system", function()
		{
			return Cilocation.create(validCiLocation)
			.then(function ciLocationCreated(location)
			{
				expect(location).to.exist;
			});
		});
		it("should not create a cilocation in the system", function()
		{
			return Cilocation.create(invalidCilocation)
			.then(function createdService(location)
			{
				expect(location).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
				var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(parseInt(numOfAttributes)).to.equal(2);
			});
		});
	});
	describe("#Update", function()
	{
		var validUpdatedLocation =
		{
			"locationcode": "INDBLRHSR 9"
		};

		var invalidUpdatedLocation =
		{
			"locationcode": "Prime#1"
		};

		it("should update a cilocation in the system", function()
		{
			return Cilocation.update({ locationname: "HSR Layout" }, validUpdatedLocation)
			.then(function updatedLocation(location)
			{
				expect(location).to.exist;
				expect(location[ 0 ]).to.have.property("locationcode", validUpdatedLocation.locationcode);
			});
		});

		it("should not update a cilocation in the system", function()
		{
			return Cilocation.update({ locationname: "HSR Layout" }, invalidUpdatedLocation)
			.then(function updatedLocation(location)
			{
				expect(location).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
				var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
			});
		});
	});
	describe("#Destroy", function()
	{
    it("should delete a cilocation from the system", function()
    {
      return Cilocation.destroy({ locationname: "HSR Layout" })
      .then(function deletedUser(deleted)
      {
        return Cilocation.find({ locationname: "HSR Layout" });
      })
      .then(function found(cilocation)
      {
        expect(cilocation).to.be.empty;
      });
    });
	});
});
