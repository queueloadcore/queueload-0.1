/* FileName: 11.Cinetwork.test.js
 * Description: This file defines all the unit test cases for the Cinetwork Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 19/03/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("Network Model", function()
{
	describe("#Create", function()
	{
		var validNetwork =
		{
			"primaryip": "172.34.26.124",
			"networkname": "TGT Lan",
			"subnet": "255.240.0.0",
			"gateway": "172.34.26.1"
		};

		var invalidNetwork =
		{
			"primaryip": "172.34.26.124.34",
			"networkname": "Invalid@Name",
			"subnet": "255.255.0.0.23",
			"gateway": "172.34.26.1.23"
		};

		it("should not add a network to the system", function()
		{
			return Cinetwork.create(invalidNetwork)
			.then(function createdNetwork(network)
			{
				expect(network).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
				var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
				expect(numOfAttributes).to.equal("4");
			});
		});

		it("should add a network to the system", function()
		{
			return Cinetwork.create(validNetwork)
			.then(function createdNetwork(network)
			{
				expect(network).to.exist;
			});
		});
	});

	describe("#Update", function()
	{
		var validUpdate =
		{
			"primaryip": "10.65.41.91",
			"networkname": "Trilogy Wan",
			"subnet": "255.0.0.0",
			"gateway": "10.65.41.1"
		};

		var inValidUpdate =
		{
			"primaryip": "10.65.41-91",
			"networkname": "Inv@lid#ame",
			"subnet": "255.X.X.X",
			"gateway": "10-65-41-1"
		};

		it("should not update a network in the system", function()
		{
			return Cinetwork.update({ primaryip: "172.34.26.124" }, inValidUpdate)
			.then(function updatedNetwork(network)
			{
				expect(network).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
				var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("4");
			});
		});

		it("should update a network in the system", function()
		{
			return Cinetwork.update({ primaryip: "172.34.26.124" }, validUpdate)
			.then(function updatedNetwork(network)
			{
				expect(network).to.exist;
			});
		});
	});

	describe("#Destroy", function()
	{
		it("should not delete a network in the system", function()
		{
			return Cinetwork.destroy({ primaryip: "INVALIDIP" })
			.then(function destoyed(network)
			{
				return Cinetwork.find({ primaryip: "10.65.41.91" });
			})
			.then(function found(network)
			{
				expect(network).not.to.be.empty;
			});
		});

		it("should delete a network in the system", function()
		{
			return Cinetwork.destroy({ primaryip: "10.65.41.91" })
			.then(function destroyed(network)
			{
				return Cinetwork.find({ primaryip: "10.65.41.91" });
			})
			.then(function found(network)
			{
				expect(network).to.be.empty;
			});
		});
	});
});
