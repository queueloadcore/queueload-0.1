/* FileName: 8.Service.test.js
 * Description: This file defines all the unit test cases for the Service Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 18/03/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("Service Model", function()
{
	describe("#Create", function()
	{
		var validService =
		{
			"servicename": "Monogram Service",
			"cbid": 4,
			"servicestateid": 2,
			"details": "This is a service for monogram(4th version) provision"
		};

		var invalidService =
		{
			"servicename": "Inv@lid-service",
			"cbid": "not-supposed-to-be-string",
			"bdid": "supposed-2-b-integer"
		};

		it("should create a service in the system", function()
		{
			return Service.create(validService)
			.then(function createdService(service)
			{
				expect(service).to.exist;
				expect(service).to.have.property("servicename", validService.servicename);
			});
		});
		it("should not create a service in the system", function()
		{
			return Service.create(invalidService)
			.then(function createdService(service)
			{
				expect(service).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
				var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("2");
			});
		});
	});
	describe("#Destroy", function()
	{
		it("should delete a service from the system", function()
		{
			return Service.destroy({ servicename: "Monogram Service" })
			.then(function deleted(service)
			{
				return Service.find({ servicename: "Monogram Service" });
			})
			.then(function found(service)
			{
				expect(service).to.be.empty;
			});
		});
	});
});
