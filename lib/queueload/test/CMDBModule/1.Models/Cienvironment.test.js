/* FileName: 5.Cienvironment.test.js
* Description: This file defines all the unit test cases for the Cienvironment Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 18/03/2016          Initial file creation                           SMandal
*
*/

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("Cienvironment Model", function()
{
	describe("#Create", function()
	{
		var validCiEnvironment =
		{
			"envname": "India Development"
		};
		var invalidCiEnvironment =
		{
			"envname": "Stage#4"
		};
		it("should create a Cienvironment", function()
		{
			return Cienvironment.create(validCiEnvironment)
			.then(function ciEnvCreated(created)
			{
				expect(created).to.exist;
			});
		});
		it("should not create a Cienvironment", function()
		{
			return Cienvironment.create(invalidCiEnvironment)
			.then(function ciEnvCreated(created)
			{
				expect(create).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
			});
		});
	});
	describe("#Destroy", function()
	{
		it("should delete a Cienvironment", function()
		{
			return Cienvironment.destroy({ envname: "India Development" })
			.then(function ciDeleted(destroyed)
			{
				return Cienvironment.find({ envname: "india Development" });
			})
			.then(function found(cienv)
			{
				expect(cienv).to.be.empty;
			});
		});
	});
});
