/* FileName: 4.Citype.test.js
* Description: This file defines all the unit test cases for the Citype Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 18/03/2016          Initial file creation                           SMandal
*
*/

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("Citype Model", function()
{
	describe("#Create", function()
	{
		var validCiType =
		{
			"citype": "big server"
		};
		var invalidCiType =
		{
			"citype": "ru-(34)"
		};
		it("should add a CI type to the system", function()
		{
			return Citype.create(validCiType)
			.then(function createCiType(created)
			{
				expect(created).to.exist;
				expect(created).to.have.property("citype", validCiType.citype);
			});
		});
		it("should not add a CI type to the the system", function()
		{
			return Citype.create(invalidCiType)
			.then(function createCiType(created)
			{
				expect(created).to.not.exist;
			})
			.catch(function(error)
			{
				expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
			});
		});
	});
	describe("#Destroy", function()
	{
		it("should delete a CI type", function()
		{
			return Citype.destroy({ citypeid: 1 })
			.then(function deletedType(destoyed)
			{
				return Citype.find({ citypeid: 1 });
			})
			.then(function found(ciType)
			{
				expect(ciType).to.be.empty;
			});
		});
	});
});
