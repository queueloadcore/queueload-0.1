/* FileName: Cidetail.test.js
 * Description: This file defines all the unit test cases for the Cidetail Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 23/03/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("CI Detail Model", function()
  {
    describe("#Create", function()
    {
      var validCiDetail =
      {
        "ciname": "BLRP-TSCJKA-C.5",
        "assettag": "TZ-U49Y.7JI-12",
        "barcode": 2378193,
        "cistateid": 1,
        "citypeid": 3,
        "envid": 2,
        "supportqueue": 19,
        "vendorid": 34,
        "machineid": 389,
        "primaryserviceid": 83,
        "ciownerid": 713,
        "locationid": 3,
        "cbid": 16,
        "bdid": 3,
        "ipaddress": "10.89.103.76"
      };

      var inValidCiDetail =
      {
        "ciname": "Invalid@Name",
        "assettag": "InvalidT@g",
        "barcode": "blue",
        "cistateid": "NYC",
        "citypeid": "Gr33nZ0ne",
        "envid": "Tall Jackal",
        "supportqueue": "Gellatin",
        "vendorid": "Str!9g",
        "machineid": "M3l0Dr@ma",
        "service1id": "Kurkure",
        "service2id": "Frodo",
        "service3id": "KKR",
        "ciownerid": "Cool@M!ckey",
        "locationid": "Icrom",
        "cbid": "Pr!m3",
        "bdid": "J!773ry",
        "ipaddress": "invalidIpAddress"
      };

      it("should not add a CI to the system", function()
      {
        return Cidetail.create(inValidCiDetail)
        .then(function createdCi(ci)
        {
          expect(ci).to.not.exist;
        })
        .catch(function(error)
        {
          expect(error).to.exist;
          var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
          expect(parseInt(numOfAttributes)).to.equal(8);
        });
      });

      it("should add a CI to the system", function()
      {
        return Cidetail.create(validCiDetail)
        .then(function createdCi(ci)
        {
          expect(ci).to.exist;
        });
      });
    });

    describe("#Update", function()
    {
      var inValidUpdateCi =
      {
        "contractid": "(ik72).i64le#GJ",
        "customapp1": "Pr()bl3m!7!c@p!"
      };

      var validUpdateCi =
      {
        "contractid": "uil45-8ME3",
        "customapp1": "Rabbit MQ"
      };

      it("should not update a CI in the system", function()
      {
        return Cidetail.update({ assettag: "TZ-U49Y.7JI-12" }, inValidUpdateCi)
        .then(function updatedCi(ci)
        {
          expect(ci).to.not.exist;
        })
        .catch(function(error)
        {
          expect(error).to.exist;
          var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
          expect(numOfAttributes).to.equal("2");
        });
      });

      it("should update a CI in the system", function()
      {
        return Cidetail.update({ assettag: "TZ-U49Y.7JI-12" }, validUpdateCi)
        .then(function updatedCi(ci)
        {
          return Cidetail.find({ assettag: "TZ-U49Y.7JI-12" });
        })
        .then(function foundCi(ci)
        {
          expect(ci).not.to.be.empty;
          expect(ci[ 0 ]).to.have.property("contractid", validUpdateCi.contractid);
          expect(ci[ 0 ]).to.have.property("customapp1", validUpdateCi.customapp1);
        });
      });
    });

    describe("#Destroy", function()
    {
      it("should delete a CI from the system", function()
      {
        return Cidetail.destroy({ assettag: "TZ-U49Y.7JI-12" })
        .then(function deletedCi(ci)
        {
          return Cidetail.find({ assettag: "TZ-U49Y.7JI-12" });
        })
        .then(function foundCi(ci)
        {
          expect(ci).to.be.empty;
        });
      });
    });
  });
