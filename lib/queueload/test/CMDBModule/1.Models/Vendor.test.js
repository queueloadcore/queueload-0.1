/* FileName: 6.Vendor.test.js
* Description: This file defines all the unit test cases for the Civendor Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 18/03/2016          Initial file creation                           SMandal
*
*/

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("Vendor Model", function()
{
  describe("#Create", function()
  {
    var validVendor =
    {
      "vendorname": "QueueLoad Inc"
    };
    var invalidVendor =
    {
      "vendorname": "Inv@lid-Vend0r"
    };
    it("should create a vendor in the system", function()
    {
      return Vendor.create(validVendor)
      .then(function vendorCreated(created)
      {
        expect(created).to.exist;
      });
    });
    it("should not create a vendor in the system", function()
    {
      return Vendor.create(invalidVendor)
      .then(function vendorCreated(created)
      {
        expect(created).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
      });
    });
  });
  describe("#Destroy", function()
  {
    it("should delete a vendor from the system", function()
    {
      return Vendor.destroy({ vendorname: "QueueLoad Inc" })
      .then(function deletedVendor(vendor)
      {
        return Vendor.find({ vendorname: "QueueLoad Inc" });
      })
      .then(function found(vendor)
      {
        expect(vendor).to.be.empty;
      });
    });
  });
});
