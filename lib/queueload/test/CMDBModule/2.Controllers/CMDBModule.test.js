/* FileName: CMDBModule.test.js
* Description: This file defines the unit and integration test cases for the CMDB Module.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 24/05/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var enums = require('../../../api/resources/Enums.js');

var company =
{
  "accountid": "qlodtest",
  "name": "Wayne Enterprises",
  "address": "Gotham City",
  "accntadmin": 1,
  "globalconfig": {}
};

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001"
};

var firstBDAdmin =
{
  "firstname": "cmdbfirstAdmin",
  "lastname": "cmdbBDOne",
  "fullname": "cmdbfirstAdmin cmdbBDOne",
  "username": "cmdbfirst.bdoneadmin",
  "password": "password",
  "emailid": "cmdbfirst.bd1admin@testdomain.com",
  "employeeid": "CMDBFTE000002"
};

var secondBDAdmin =
{
  "firstname": "cmdbsecondAdmin",
  "lastname": "cmdbBDOne",
  "fullname": "cmdbsecondAdmin cmdbBDOne",
  "username": "cmdbsecond.bdoneadmin",
  "password": "password",
  "emailid": "cmdbsecond.bd1admin@testdomain.com",
  "employeeid": "CMDBFTE000003"
};

var thirdBDAdmin =
{
  "firstname": "cmdbthirdAdmin",
  "lastname": "cmdbBDTwo",
  "fullname": "cmdbthirdAdmin cmdbBDTwo",
  "username": "cmdbfirst.bdtwoadmin",
  "password": "password",
  "emailid": "cmdbfirst.bd2admin@testdomain.com",
  "employeeid": "CMDBFTE000004"
};

var fourthBDAdmin =
{
  "firstname": "cmdbfourthAdmin",
  "lastname": "cmdbBDTwo",
  "fullname": "cmdbfourthAdmin cmdbBDTwo",
  "username": "cmdbsecond.bdtwoadmin",
  "password": "password",
  "emailid": "cmdbsecond.bd2admin@testdomain.com",
  "employeeid": "CMDBFTE000005"
};

var firstCBOwner =
{
  "firstname": "cmdbfirst",
  "lastname": "cmdbCBOwner",
  "fullname": "cmdbfirst cmdbCBOwner",
  "username": "cmdbfirst.cbowner",
  "password": "password",
  "emailid": "cmdbfirst.cbowner@testdomain.com",
  "employeeid": "CMDBFTE000006"
};

var secondCBOwner =
{
  "firstname": "cmdbsecond",
  "lastname": "cmdbCBOwner",
  "fullname": "cmdbsecond cmdbCBOwner",
  "username": "cmdbsecond.cbowner",
  "password": "password",
  "emailid": "cmdbsecond.cbowner@testdomain.com",
  "employeeid": "CMDBFTE000007"
};

var thirdCBOwner =
{
  "firstname": "cmdbthird",
  "lastname": "cmdbCBOwner",
  "fullname": "cmdbthird cmdbCBOwner",
  "username": "cmdbthird.cbowner",
  "password": "password",
  "emailid": "cmdbthird.cbowner@testdomain.com",
  "employeeid": "CMDBFTE000008"
};

var firstOperator =
{
  "firstname": "cmdbfirst",
  "lastname": "cmdbOperator",
  "fullname": "cmdbfirst cmdbOperator",
  "username": "cmdbfirst.operator",
  "password": "password",
  "emailid": "cmdbfirst.operator@testdomain.com",
  "employeeid": "CMDBFTE000009"
};

var secondOperator =
{
  "firstname": "cmdbsecond",
  "lastname": "cmdbOperator",
  "fullname": "cmdbsecond cmdbOperator",
  "username": "cmdbsecond.Operator",
  "password": "password",
  "emailid": "cmdbsecond.operator@testdomain.com",
  "employeeid": "CMDBFTE000010"
};

var firstUser =
{
  "firstname": "cmdbfirst",
  "lastname": "cmdbUser",
  "fullname": "cmdbfirst cmdbUser",
  "username": "cmdbfirst.User",
  "password": "cmdbpassword",
  "emailid": "cmdbfirst.user@testdomain.com",
  "employeeid": "CMDBFTE000011"
};

var bdOne = {};
var bdTwo = {};
var cbOne = {};
var cbTwo = {};
var cbThree = {};
var orgApproval1 = {};
var orgApproval2 = {};

var queueOne =
{
  "queuename":"TestQueue1",
  "queueemail":"testqueue1@testdomain.com"
};

var serviceOne = {};
var serviceTwo = {};

var cidetailOne = {};

var serviceApprovalOne =
{
  "approved": false
};

var invalidParams_serviceNameArray =
{
  "servicename": [ "Cool name", "Super Cool name" ],
  "details": "Cocky description",
  "servicestateid": 2
};

var invalidParams_nonExistentcbCode =
{
  "servicename": "Cool name",
  "cbid": 45668,
  "details": "Still doesn't matter"
};

var validParams_ServiceEntry =
{
  "servicename": "Sober Name",
  "details": "Understandable description"
};

var invalidParams_noServiceId =
{
  "serviceidentity": 2
};

var invalidCiServiceApproveParams_missingCiid =
{
  "type": "assoc",
  "isapproved": false
};

var validCiServiceApproveParams_decline =
{
  "type": "assoc",
  "isapproved": false
};

var validCiServiceApproveParams_approve =
{
  "type": "assoc",
  "isapproved": true
};

var invalidParams_nonExistentServiceId =
{
  "serviceid": 1435,
  "details": "Updated description"
};

var validParams_getServiceDetails = {};

var validServiceUpdateParams =
{
  "details": "Updated description"
};

var validParams_serviceId = {};

var invalidApproveParams_withoutType =
{
  "isapproved": true
};

var invalidApproveParams_invalidApprovalId =
{
  "type": "state",
  "approvalid": "zoo",
  "isapproved": true
};

var invalidApproveParams_nonExistentisApproved =
{
  "type": "state"
};

var invalidApproveParams_nullIsApproved =
{
  "type": "state",
  "isapproved": null
};

var invalidApproveParams_nonExistentServiceId =
{
  "type": "state",
  "isapproved": null
};

var invalidApproveParams_nonExistentApprovalId =
{
  "type": "state",
  "approvalid": 2890,
  "isapproved": true
};

var invalidApproveParams_incorrectApprover =
{
  "type": "state",
  "isapproved": true
};

var invalidApproveParams_incorrectServiceId =
{
  "type": "state",
  "isapproved": true,
  "serviceid": 1456
};

var validApproveParams_declineStateRequest =
{
  "type": "state",
  "isapproved": false
};

var validApproveParams_approveRequest =
{
  "type": "assoc",
  "isapproved": true
};

var invalidAssocParams_noServiceIds = {};

var invalidAssocParams_noCiId =
{
  "serviceids": []
};

var invalidAssocParams_nonIntegerCiId =
{
  "serviceids": [],
  "ciid": "zoo"
};

var invalidAssocParams_duplicateServiceId = {};

var invalidAssocParams_nonExistentServiceId = {};

var invalidAssocParams_FrozenServiceId = {};

var validServiceAssocParams = {};

var invalidCiEnvAddprams_missingEnvnames =
{
  "invalidEnvNameAttr": [ "development", "testing", "production" ]
};

var invalidCiEnvAddParams_dupEnvnames =
{
  "envnames": [ "development", "testing", "development" ]
};

var invalidCiEnvAddprams_nonArrayEnvnames =
{
  "envnames": "development"
};

var validCiEnvAddParams =
{
  "envnames": [ "development", "testing", "production" ]
};

var firstEnv = {};
var secondEnv = {};
var thirdEnv = {};

var invalidCiEnvUpdateParams_missingEnvname =
[
  {
    "envid": secondEnv.envid
  },
  {
    "envid": thirdEnv.envid,
    "envname": "mobdev"
  }
];

var invalidCiEnvUpdateParams_missingEnvId =
[
  {
    "envname": "mobproduction"
  },
  {
    "envid": thirdEnv.envid,
    "envname": "mobdev"
  }
];

var invalidCiEnvUpdateParams_nonIntEnvId =
[
  {
    "envname": "mobproduction",
    "envid": "three"
  },
  {
    "envid": thirdEnv.envid,
    "envname": "mobdev"
  }
];

var invalidCiEnvUpdateParams_arrayEnvName =
[
  {
    "envname": [ "mobproduction", "choudwar", "lingarajmarket" ],
    "envid": secondEnv.envid
  },
  {
    "envid": thirdEnv.envid,
    "envname": "mobdev"
  }
];

var invalidCiEnvUpdateParams_arrayEnvId =
[
  {
    "envname": "mobproduction",
    "envid": [ secondEnv.envid, thirdEnv.envid, 5 ]
  },
  {
    "envid": thirdEnv.envid,
    "envname": "mobdev"
  }
];

var invalidCiEnvUpdateParams_dupEnvNames =
[
  {
    "envname": "mobdev",
    "envid": secondEnv.envid
  },
  {
    "envid": thirdEnv.envid,
    "envname": "mobdev"
  }
];

var invalidCiEnvUpdateParams_nonExistentEnvIds =
[
  {
    "envname": "mobdev"
  },
  {
    "envid": 334535,
    "envname": "mobproduction"
  }
];

var invalidCiEnvUpdateParams_existingEnvNames =
[
  {
    "envname": "mobdev",
    "envid": secondEnv.envid
  },
  {
    "envid": thirdEnv.envid,
    "envname": "production"
  }
];

var validCiEnvUpdateParams =
[
  { "envid": firstEnv.envid, "envname": "mobproduction" },
  { "envid": secondEnv.envid, "envname": "mobdev" }
];

var validVendorAddParams =
{
  "vendornames": [ "IBM", "Quantum", "Hewlett Packard", "Amazon Web Services" ]
};

var vendorOne = {};
var vendorTwo = {};
var vendorThree = {};

var invalidVendorAddParams_missingVendorNames =
{
  "companynames": [ "IBM", "Quantum", "Hewlett Packard" ]
};

var invalidVendorAddParams_nonArrayVendorNames =
{
  "vendornames": "IBM"
};

var invalidVendorAddParams_duplicateVendorNames =
{
  "vendornames": [ "IBM", "IBM", "Hewlett Packard" ]
};

var invalidVendorUpdateParams_missingVendorId =
[
  { "vendorname": "Stark Enterprises" },
  { "vendorname": "Cyberdyne Systems" }
];

var invalidVendorUpdateParams_missingVendorName =
[
  { "vendorid": vendorOne.vendorid },
  { "vendorid": vendorTwo.vendorid, "vendorname": "Cyberdyne Systems" }
];

var invalidVendorUpdateParams_nonIntVendorName =
[
  { "vendorid": "jungle", "vendorname": "Stark Enterprises" },
  { "vendorid": vendorTwo.vendorid, "vendorname": "Cyberdyne Systems" }
];

var invalidVendorUpdateParams_arrayVendorId =
[
  { "vendorid": [ vendorOne.vendorid, vendorTwo.vendorid ], "vendorname": "Stark Enterprises" },
  { "vendorid": vendorThree.vendorid, "vendorname": "Cyberdyne Systems" }
];

var invalidVendorUpdateParams_arrayVendorName =
[
  { "vendorid": vendorOne.vendorid, "vendorname": [ "Malgudi Ltd", "Kelvin Projects" ] },
  { "vendorid": vendorTwo.vendorid, "vendorname": "Nano Systems" }
];

var invalidVendorUpdateParams_arrayVendorName =
[
  { "vendorid": vendorOne.vendorid, "vendorname": [ "Malgudi Ltd", "Kelvin Projects" ] },
  { "vendorid": vendorTwo.vendorid, "vendorname": "Nano Systems" }
];

var invalidVendorUpdateParams_nonExistentVendorIds =
[
  { "vendorname": "Malgudi Ltd" },
  { "vendorid": 46665, "vendorname": "Nano Systems" }
];

var invalidVendorUpdateParams_ExistingVendorNames =
[
  { "vendorid": vendorOne.vendorid, "vendorname": "IBM" },
  { "vendorid": vendorTwo.vendorid, "vendorname": "Malgudi Ltd" }
];

var validVendorUpdateParams =
[
  { "vendorid": vendorOne.vendorid, "vendorname": "Nano Systems" },
  { "vendorid": vendorTwo.vendorid, "vendorname": "Malgudi Ltd" }
];

var invalidCilocationAddParams_missingLocationName =
{
  "locationcode": "RS-12T-LK61",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationAddParams_missingLocationCode =
{
  "locationname": "Stockholm-12T-LK",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationAddParams_missingBuildingName =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": "RS-12T-LK61",
  "sitecategory": "DR Site"
};

var invalidCilocationAddParams_missingSiteCategory =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": "RS-12T-LK61",
  "buildingname": "Nordic Offsite"
};

var invalidCilocationAddParams_arrayLocationName =
{
  "locationname": [ "Stockholm-12T-LK", "Gothenburg-9F-OGT" ],
  "locationcode": "RS-12T-LK61",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationAddParams_arrayLocationCode =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": [ "RS-12T-LK61", "JN-9F-OGT5" ],
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationAddParams_arrayBuildingName =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": "RS-12T-LK61",
  "buildingname": [ "Nordic Offsite", "Northern Vault" ],
  "sitecategory": "DR Site"
};

var invalidCilocationAddParams_arraySiteCategory =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": "RS-12T-LK61",
  "buildingname": "Nordic Offsite",
  "sitecategory": [ "DR Site", "Juggle Yard" ]
};

var validCilocationAddParams =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": "RS-12T-LK61",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var AWSMumbaiSiteLocation =
{
  "locationname": "AWS - Asia Pacific Mumbai",
  "locationcode": "AWS - ap-south-1"
};

var locationOne = {};

var invalidCilocationUpdateParams_missingLocationId =
{
  "locationname": "Stockholm-12T-LK",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationUpdateParams_nonIntLocationId =
{
  "locationid": "zuzu",
  "locationname": "Stockholm-12T-LK",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationUpdateParams_withLocationCode =
{
  "locationname": "Stockholm-12T-LK",
  "locationcode": "RS-12T-LK61",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationUpdateParams_arrayLocationId =
{
  "locationid": [ locationOne.locationid, 4 ],
  "locationname": "Stockholm-12T-LK",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationUpdateParams_arrayLocationName =
{
  "locationid": locationOne.locationid,
  "locationname": [ "Stockholm-12T-LK", "Gothenburg-9F-OGT" ],
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var invalidCilocationUpdateParams_arrayBuildingName =
{
  "locationid": locationOne.locationid,
  "locationname": "Stockholm-12T-LK",
  "buildingname": [ "Nordic Offsite", "Northern Vault" ],
  "sitecategory": "DR Site"
};

var invalidCilocationUpdateParams_arraySiteCategory =
{
  "locationid": locationOne.locationid,
  "locationname": "Stockholm-12T-LK",
  "buildingname": "Nordic Offsite",
  "sitecategory": [ "DR Site", "Juggle Yard" ]
};

var invalidCilocationUpdateParams_nonExistentLocationId =
{
  "locationid": 55677,
  "locationname": "Stockholm-12T-LK",
  "buildingname": "Nordic Offsite",
  "sitecategory": "DR Site"
};

var validCilocationUpdateParams =
{
  "locationid": locationOne.locationid,
  "locationname": "Gothenburg-9F-OGT",
  "buildingname": "Northern Vault",
  "sitecategory": "Juggle Yard"
};

var validCitypeAddParams =
{
  "citypes": [ "switch", "rack", "blade", "ec2.t2.micro", "ec2.ebs.gp2", "db.t2.micro" ]
};

var citypeOne = {};
var citypeTwo = {};
var citypeThree = {};

var invalidCitypeAddParams_duplicateCiTypes =
{
  "citypes": [ "switch", "switch", "blade" ]
};

var validCitypeUpdateParams =
[
  { "citypeid": citypeOne.citypeid, "citype": "network switches" },
  { "citypeid": citypeTwo.citypeid, "citype": "firewalls" },
  { "citypeid": citypeThree.citypeid, "citype": "IDS" }
];

var invalidCinetworkAddParams_missingPrimaryIp =
{
  "networkname": "Upstream-Strike3-Gateway-Log",
  "subnet": "255.255.255.0",
  "gateway": "10.83.219.1"
};

var invalidCinetworkAddParams_missingNetworkName =
{
  "primaryip": "10.83.219.34",
  "subnet": "255.255.255.0",
  "gateway": "10.83.219.1"
};

var invalidCinetworkAddParams_missingSubnet =
{
  "primaryip": "10.83.219.34",
  "networkname": "Upstream-Strike3-Gateway-Log",
  "gateway": "10.83.219.1"
};

var invalidCinetworkAddParams_missingGateway =
{
  "primaryip": "10.83.219.34",
  "networkname": "Upstream-Strike3-Gateway-Log",
  "subnet": "255.255.255.0"
};

var validCinetworkAddParams =
{
  "primaryip": "10.83.219.34",
  "networkname": "Upstream-Strike3-Gateway-Log",
  "subnet": "255.255.255.0",
  "gateway": "10.83.219.1"
};

var networkOne = {};

var invalidCinetworkGetdetailParams_missingCinetworkid =
{
  "cinetworkidentity": 2
};

var invalidCinetworkGetdetailParams_arrayCinetworkid =
{
  "cinetworkidentity": [ 2, 4 ]
};

var invalidCinetworkGetdetailParams_nonExistentCinetworkid =
{
  "cinetworkid": 1452
};

var validCinetworkGetdetailParams = {};

var invalidCinetworkUpdateParams_missingCinetworkid =
{
  "cinetworkidentity": 2
};

var invalidCinetworkUpdateParams_arrayCinetworkid =
{
  "cinetworkidentity": [ 2, 4 ]
};

var invalidCinetworkUpdateParams_nonExistentCinetworkid =
{
  "cinetworkid": 14734
};

var validCinetworkUpdateParams =
{
  "cinetworkid": networkOne.cinetworkid,
  "networkname": "JellyStub"
};

var validCihardwareAddParams =
{
  "hardwaretag": "TGY47IK829",
  "manufacturervendorid": vendorOne.vendorid,
  "hwmodel": "HF-482R",
  "gridloc": "AF-YH5",
  "partnum": "IYU3M14O4Q"
};

var hardwareOne = {};

var invalidCihardwareAddParams_missingHardwareTag =
{
  "manufacturervendorid": hardwareOne.machineid,
  "hwmodel": "HF-482R",
  "gridloc": "AF-YH5",
  "partnum": "IYU3M14O4Q"
};

var invalidCihardwareAddParams_missingVendorId =
{
  "hardwaretag": "TGY47IK829",
  "hwmodel": "HF-482R",
  "gridloc": "AF-YH5",
  "partnum": "IYU3M14O4Q"
};

var invalidCihardwareAddParams_nonIntVendorId =
{
  "hardwaretag": "TGY47IK829",
  "manufacturervendorid": "Pebble",
  "hwmodel": "HF-482R",
  "gridloc": "AF-YH5",
  "partnum": "IYU3M14O4Q"
};

var invalidCihardwareAddParams_missingModel =
{
  "hardwaretag": "TGY47IK829",
  "manufacturervendorid": vendorOne.vendorid,
  "gridloc": "AF-YH5",
  "partnum": "IYU3M14O4Q"
};

var invalidCihardwareAddParams_missingGridloc =
{
  "hardwaretag": "TGY47IK829",
  "manufacturervendorid": vendorOne.vendorid,
  "hwmodel": "HF-482R",
  "partnum": "IYU3M14O4Q"
};

var invalidCihardwareAddParams_missingPartNum =
{
  "hardwaretag": "TGY47IK829",
  "manufacturervendorid": vendorOne.vendorid,
  "hwmodel": "HF-482R",
  "gridloc": "AF-YH5"
};

var invalidCihardwareAddParams_nonExistentVendorId =
{
  "hardwaretag": "TGY47IK829",
  "manufacturervendorid": 13420,
  "hwmodel": "HF-482R",
  "gridloc": "AF-YH5",
  "partnum": "IYU3M14O4Q"
};

var invalidCihardwareGetDetailParams_missingMachineId =
{
  "machineIdentity": 2
};

var invalidCihardwareGetDetailParams_nonIntMachineId =
{
  "machineid": "bigmachine"
};

var validCihardwareGetDetailParams =
{
  "machineid": hardwareOne.machineid
};

var validCihardwareUpdateParams =
{
  "gridloc": "BK-LR9",
  "manufacturervendorid": vendorTwo.vendorid,
  "osvendorid": vendorThree.vendorid
};

var validCidetailAddParams =
{
  "ciname": "HSG4-JSJKAQM-T",
  "assettag": "SAMAK82KD",
  "installationdate": 1480515761
};

var validCidetailUpdateParams =
{
  "systemsoftware1": "Splunk"
};

var invalidCidetailGetdetail_missingCiid =
{
  "ciidentity": 2
};

var invalidCidetailGetdetail_arrayCiid =
{
  "ciid": [ 24352, 32354 ]
};

var invalidCidetailGetdetail_nonIntCiid =
{
  "ciid": "two"
};

var invalidCidetailGetdetail_invalidCiid =
{
  "ciid": 24879
};

var validCidetailGetDetailParams = {};

var invalidCidetailCancelParams_missingCiid =
{
  "ciidentity": 2
};

var invalidCidetailCancelParams_nonIntCiid =
{
  "ciid": "two"
};

var invalidCidetailCancelParams_invalidCiid =
{
  "ciid": 44476
};

var validCidetailCancelParams = {};

var validServiceCancelParams = {};

var invalidCidetailNextParams_nonIntCiid =
{
  "ciidentity": "two"
};

var invalidCidetailNextParams_invalidCiid =
{
  "ciidentity": 42
};

var validCidetailsNextParams = {};

var invalidCidetailQueueAssignParams_missingCiid =
{
  "assign": true
};

var invalidCidetailQueueAssignParams_missingQueueId =
{
  "assign": true
};

var invalidCidetailQueueAssignParams_nonBoolAssign =
{
  "assign": "absolutetruth"
};

var invalidCidetailQueueAssignParams_nullAssign =
{
  "assign": null
};

var invalidCidetailQueueAssignParams_invalidCiid =
{
  "ciid": 25234,
  "assign": true
};

var invalidCidetailQueueAssignParams_invalidQueueid =
{
  "supportqueue": 37865,
  "assign": true
};

var validCidetailQueueAssignParams =
{
  "assign": true
};

var invalidOrgapprovalApproveParams_missingApprovalId =
{
  "approved": false
};

var invalidOrgapprovalApproveParams_nonBoolApproved =
{
  "approved": "false"
};

var invalidOrgapprovalApproveParams_nullApproved =
{
  "approved": null
};

var invalidOrgapprovalApproveParams_invalidApprovalId =
{
  "approvalid": 199,
  "approved": false
};

var invalidOrgapprovalApproveParams_incorrectApproverId =
{
  "approved": false
};

var validOrgapprovalApproveParams =
{
  "approved": false
};

var invalidServiceTransferInitiateParams_missingEntityId =
{
  "approvaltype": 1
};

var invalidServiceTransferInitiateParams_nonIntEntityId =
{
  "entityid": "two",
  "approvaltype": 1
};

var invalidServiceTransferInitiateParams_invalidEntityId =
{
  "entityid": 4453,
  "approvaltype": 1
};

var invalidServiceTransferInitiateParams_missingTargetEntityId =
{
  "approvaltype": 1
};

var invalidServiceTransferInitiateParams_nonIntTargetEntityId =
{
  "targetentityid": "six",
  "approvaltype": 1
};

var invalidServiceTransferInitiateParams_invalidTargetEntityId =
{
  "targetentityid": 676,
  "approvaltype": 1
};

var invalidServiceTransferInitiateParams_missingApprovalType = {};

var invalidServiceTransferInitiateParams_nonIntApprovalType =
{
  "approvaltype": "one"
};

var invalidServiceTransferInitiateParams_invalidApprovalType =
{
  "approvaltype": 254
};

var validServiceTransferInitiateParams =
{
  "approvaltype": 1
};

var  invalidCilocationSearchParams_invalid1stParam =
{
  "searched":  { "sitecategory": "Juggle%" },
  "result": [ "locationname", "locationcode", "buildingname", "sitecategory" ],
  "orderby": "locationcode"
};
var invalidCilocationSearchParams_invalid2ndParam =
{
  "search":  { "sitecategory": "Juggle%" },
  "results": [ "locationname", "locationcode", "buildingname", "sitecategory" ],
  "orderby": "locationcode"
};
var invalidCilocationSearchParams_invalid3rdParam =
{
  "search":  { "sitecategory": "Juggle%" },
  "result": [ "locationname", "locationcode", "buildingname", "sitecategory" ],
  "ordersby": "locationcode"
};
var invalidCilocationSearchParams_nonExistent1stColumn =
{
  "search":  { "sitecategory": "Juggle%" },
  "result": [ "nonexistentclmn", "locationcode", "buildingname", "sitecategory" ],
  "orderby": "locationcode"
};
var invalidCilocationSearchParams_nonExistent2ndColumn =
{
  "search":  { "sitecategory": "Juggle%" },
  "result": [ "locationname", "nonexistentclmn", "buildingname", "sitecategory" ],
  "orderby": "locationcode"
};
var invalidCilocationSearchParams_nonExistent3rdColumn =
{
  "search":  { "sitecategory": "Juggle%" },
  "result": [ "locationname", "locationcode", "nonexistentclmn", "sitecategory" ],
  "orderby": "locationcode"
};
var validCilocationSearchParams =
{
  "search":  { "sitecategory": "Juggle%" },
  "result": [ "locationname", "locationcode", "buildingname", "sitecategory" ],
  "orderby": "locationcode"
};
var invalidCinetworkSearchParams_invalid1stParam =
{
  "searchh":  { "gateway": "10.83.219%" },
  "result": [ "primaryip", "networkname", "subnet", "gateway" ],
  "orderby": "networkname"
};
var invalidCinetworkSearchParams_invalid2ndParam =
{
  "search":  { "gateway": "10.83.219%" },
  "results": [ "primaryip", "networkname", "subnet", "gateway" ],
  "orderby": "networkname"
};
var invalidCinetworkSearchParams_invalid3rdParam =
{
  "search":  { "gateway": "10.83.219%" },
  "result": [ "primaryip", "networkname", "subnet", "gateway" ],
  "ordersby": "networkname"
};
var invalidCinetworkSearchParams_nonExistent2ndColumn =
{
  "search":  { "gateway": "10.83.219%" },
  "result": [ "primaryip", "networkid", "subnet", "gateway" ],
  "orderby": "networkname"
};
var invalidCinetworkSearchParams_nonExistent3rdColumn =
{
  "search":  { "gateway": "10.83.219%" },
  "result": [ "primaryip", "networkname", "subnett", "gateway" ],
  "orderby": "networkname"
};
var validCinetworkSearchParams =
{
  "search":  { "gateway": "10.83.219%" },
  "result": [ "primaryip", "networkname", "subnet", "gateway" ],
  "orderby": "networkname"
};
var invalidCihardwareSearchParams_invalid1stParam =
{
  "searchh":  { "partnum": "IYU3M14%" },
  "result": [ "hardwaretag", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum" ],
  "orderby": "hardwaretag"
};
var invalidCihardwareSearchParams_invalid2ndParam =
{
  "search":  { "partnum": "IYU3M14%" },
  "results": [ "hardwaretag", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum" ],
  "orderby": "hardwaretag"
};
var invalidCihardwareSearchParams_invalid3rdParam =
{
  "search":  { "partnum": "IYU3M14%" },
  "result": [ "hardwaretag", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum" ],
  "ordersby": "hardwaretag"
};
var invalidCihardwareSearchParams_nonExistent1stColumn =
{
  "search":  { "partnum": "IYU3M14%" },
  "result": [ "hardwaretags", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum" ],
  "orderby": "hardwaretag"
};
var invalidCihardwareSearchParams_nonExistent2ndColumn =
{
  "search":  { "partnum": "IYU3M14%" },
  "result": [ "hardwaretag", "manufacturervendorids", "hwmodel", "serialnumber", "gridloc", "partnum" ],
  "orderby": "hardwaretag"
};
var invalidCihardwareSearchParams_nonExistent3rdColumn =
{
  "search":  { "partnum": "IYU3M14%" },
  "result": [ "hardwaretag", "manufacturervendorid", "hwmodels", "serialnumber", "gridloc", "partnum" ],
  "orderby": "hardwaretag"
};
var validCihardwareSearchParams =
{
  "search":  { "partnum": "IYU3M14%" },
  "result": [ "hardwaretag", "manufacturervendorid", "hwmodel", "serialnumber", "gridloc", "partnum" ],
  "orderby": "hardwaretag"
};
var invalidServiceSearchParams_invalid1stParam =
{
  "searchh":  { "servicename": "Cinderella%" },
  "result": [ "servicename", "cbid", "bdid", "servicestateid", "details" ],
  "orderby": "servicename"
};
var invalidServiceSearchParams_invalid2ndParam =
{
  "search":  { "servicename": "Cinderella%" },
  "results": [ "servicename", "cbid", "bdid", "servicestateid", "details" ],
  "orderby": "servicename"
};
var invalidServiceSearchParams_invalid3rdParam =
{
  "search":  { "servicename": "Cinderella%" },
  "result": [ "servicename", "cbid", "bdid", "servicestateid", "details" ],
  "ordersby": "servicename"
};
var invalidServiceSearchParams_nonExistent1stColumn =
{
  "search":  { "servicename": "Cinderella%" },
  "result": [ "servicenamme", "cbid", "servicestateid", "details" ],
  "orderby": "servicename"
};
var invalidServiceSearchParams_nonExistent2ndColumn =
{
  "search":  { "servicename": "Cinderella%" },
  "result": [ "servicename", "cbbid", "servicestateid", "details" ],
  "orderby": "servicename"
};
var invalidServiceSearchParams_nonExistent3rdColumn =
{
  "search":  { "servicename": "Cinderella%" },
  "result": [ "servicename", "cbid", "bddid", "servicestateid", "details" ],
  "orderby": "servicename"
};
var validServiceSearchParams =
{
  "search":  { "servicename": "Sober%" },
  "result": [ "servicename", "cbid", "servicestateid", "details" ],
  "orderby": "servicename"
};
var invalidCidetailSearchParams_invalid1stParam =
{
  "searchh":  { "ciname": "HSG4-JSJKA%" },
  "result": [ "ciname", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "orderby": "assettag"
};
var invalidCidetailSearchParams_invalid2ndParam =
{
  "search":  { "ciname": "HSG4-JSJKA%" },
  "results": [ "ciname", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "orderby": "assettag"
};
var invalidCidetailSearchParams_invalid3rdParam =
{
  "search":  { "ciname": "HSG4-JSJKA%" },
  "result": [ "ciname", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "ordersby": "assettag"
};
var invalidCidetailSearchParams_nonExistent1stColumn =
{
  "search":  { "ciname": "HSG4-JSJKA%" },
  "result": [ "cinamee", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "orderby": "assettag"
};
var invalidCidetailSearchParams_nonExistent2ndColumn =
{
  "search":  { "ciname": "HSG4-JSJKA%" },
  "result": [ "ciname", "assettagg", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "orderby": "assettag"
};
var invalidCidetailSearchParams_nonExistent3rdColumn =
{
  "search":  { "ciname": "HSG4-JSJKA%" },
  "result": [ "ciname", "assettag", "cistateidd", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "orderby": "assettag"
};
var validCidetailSearchParams =
{
  "search":  { "ciname": "HSG4-JSJKA%" },
  "result": [ "ciname", "assettag", "cistateid", "citypeid", "envid", "ciownerid", "locationid", "installationdate" ],
  "orderby": "assettag"
};
var validServiceQueueAssignParams =
{
  assign: true
};

var validServiceQueueAssignApprovalParams =
{
  "approved": true
};

describe("CMDB Unit Tests", function()
{
  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.accTkn = res.body.acctoken;
        superUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st BDAdmin");
      }
      else
      {
        firstBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd BDAdmin");
      }
      else
      {
        secondBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd BDAdmin");
      }
      else
      {
        thirdBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 4th BDAdmin");
      }
      else
      {
        fourthBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CBOwner");
      }
      else
      {
        firstCBOwner.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd CBOwner");
      }
      else
      {
        secondCBOwner.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd CBOwner");
      }
      else
      {
        thirdCBOwner.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st Operator");
      }
      else
      {
        firstOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd Operator");
      }
      else
      {
        secondOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st User");
      }
      else
      {
        firstUser.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/bd/add')
        .set('Authorization', superUser.accTkn)
        .send({ bdname: "BusinessDeptOne" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st Business Department");
      }
      else
      {
        bdOne.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post('/bd/add')
        .set('Authorization', superUser.accTkn)
        .send({ bdname: "BusinessDeptTwo" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd Business Department");
      }
      else
      {
        bdTwo.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post('/bd/assign')
        .set('Authorization', superUser.accTkn)
        .send({ "bdid": bdOne.bdid, "admins": [ firstBDAdmin.userid, secondBDAdmin.userid ], "assign": true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign admins to 1st BD");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/bd/assign')
        .set('Authorization', superUser.accTkn)
        .send({ "bdid": bdTwo.bdid, "admins": [ thirdBDAdmin.userid, fourthBDAdmin.userid ], "assign": true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign admins to 2nd BD");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/bd/activation')
        .set('Authorization', superUser.accTkn)
        .send({ bdids: [ bdOne.bdid, bdTwo.bdid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate the BDs");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in firstBDAdmin");
      }
      else
      {
        firstBDAdmin.accTkn = res.body.acctoken;
        firstBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(secondBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in secndBDAdmin");
      }
      else
      {
        secondBDAdmin.accTkn = res.body.acctoken;
        secondBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(thirdBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in thirdBDAdmin");
      }
      else
      {
        thirdBDAdmin.accTkn = res.body.acctoken;
        thirdBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in fourthBDAdmin");
      }
      else
      {
        fourthBDAdmin.accTkn = res.body.acctoken;
        fourthBDAdmin.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in firstCBOwner");
      }
      else
      {
        firstCBOwner.accTkn = res.body.acctoken;
        firstCBOwner.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(secondCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in secondCBOwner");
      }
      else
      {
        secondCBOwner.accTkn = res.body.acctoken;
        secondCBOwner.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(thirdCBOwner);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in thirdCBOwner");
      }
      else
      {
        thirdCBOwner.accTkn = res.body.acctoken;
        thirdCBOwner.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in first User");
      }
      else
      {
        firstUser.accTkn = res.body.acctoken;
        firstUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: bdOne.bdid, ownerid: firstCBOwner.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CB for BD 1");
      }
      else
      {
        orgApproval1 = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the 1st CB");
      }
      else
      {
        cbOne = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: bdOne.bdid, ownerid: secondCBOwner.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd CB for BD 1");
      }
      else
      {
        orgApproval1 = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', secondCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the 2nd CB");
      }
      else
      {
        cbTwo = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', thirdBDAdmin.accTkn)
        .send({ bdid: bdTwo.bdid, ownerid: thirdCBOwner.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CB for BD 2");
      }
      else
      {
        orgApproval1 = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', thirdCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the 3rd CB");
      }
      else
      {
        cbThree = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/activation')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ cbids: [ cbTwo.cbid ], action: false });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate the CB 3");
      }
      else
      {
        queueOne.cbid = cbOne.cbid;
        queueOne.queueownerid = thirdBDAdmin.userid;
        queueOne.changemanagerid = thirdBDAdmin.userid;
        queueOne.srtmanagerid = thirdBDAdmin.userid;
        queueOne.incidentmanagerid = thirdBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send(queueOne);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add queue 1");
      }
      else
      {
        orgApproval1 = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstCBOwner.accTkn)
        .send({ approvalid: orgApproval1.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 1");
      }
      else
      {
        queueOne = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueid: queueOne.queueid, members: [ firstOperator.userid, secondOperator.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue 1");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in first Operator");
      }
      else
      {
        firstOperator.accTkn = res.body.acctoken;
        firstOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(secondOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign-in second Operator");
      }
      else
      {
        secondOperator.accTkn = res.body.acctoken;
        secondOperator.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueids: [ queueOne.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 1");
      }
      else
      {
        done();
      }
    })
    .catch(done);
  });

  describe("Cienvironment Controller", function()
  {

    describe("#list()", function()
    {

      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no environment to list) - 400311", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400311);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set("Authorization", firstUser.accTkn)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter with incorrect envnames attribute) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvAddprams_missingEnvnames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(envnames attribute is not an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvAddprams_nonArrayEnvnames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has duplicate envnames) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvAddParams_dupEnvnames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should add the requested environments", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCiEnvAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.have.property("envname").and.equal("development");
          expect(res.body[ 1 ]).to.have.property("envname").and.equal("testing");
          expect(res.body[ 2 ]).to.have.property("envname").and.equal("production");
          firstEnv = res.body[ 0 ];
          secondEnv = res.body[ 1 ];
          thirdEnv = res.body[ 2 ];
          done();
        });
      });

      it("should return error(one of the envnames already exist) - 400312", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCiEnvAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400312);
          done();
        });
      });
    });

    describe("#update()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {

        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstUser.accTkn)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(an object is missing envnames) - 400051", function(done)
      {

        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_missingEnvname)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(an object is missing envid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_missingEnvId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(an object has non integer envid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_nonIntEnvId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(an object has an array of envnames) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_arrayEnvName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(an object has an array of envids) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_arrayEnvId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has duplicate envnames) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_dupEnvNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has duplicate envnames) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_dupEnvNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has nonexistent envids) - 400313", function(done)
      {
        invalidCiEnvUpdateParams_nonExistentEnvIds[ 0 ].envid = secondEnv.envid;
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_nonExistentEnvIds)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400313);
          done();
        });
      });

      it("should return error(parameter has existing envnames) - 400312", function(done)
      {
        invalidCiEnvUpdateParams_existingEnvNames[ 0 ].envid = secondEnv.envid;
        invalidCiEnvUpdateParams_existingEnvNames[ 1 ].envid = thirdEnv.envid;
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCiEnvUpdateParams_existingEnvNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400312);
          done();
        });
      });

      it("should update the requested environments", function(done)
      {
        validCiEnvUpdateParams[ 0 ].envid = secondEnv.envid;
        validCiEnvUpdateParams[ 1 ].envid = thirdEnv.envid;
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCiEnvUpdateParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ][ 0 ]).to.have.property("envname").and.equal("mobproduction");
          expect(res.body[ 1 ][ 0 ]).to.have.property("envname").and.equal("mobdev");
          done();
        });
      });
    });
  });

  describe("Vendor Controller", function()
  {

    describe("#list()", function()
    {

      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no vendors to list) - 400331", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400331);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set("Authorization", firstUser.accTkn)
        .send(invalidVendorAddParams_missingVendorNames)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter is missing vendornames attribute) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorAddParams_missingVendorNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(vendornames attribute is not an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorAddParams_nonArrayVendorNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(vendornames attribute has duplicates) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorAddParams_duplicateVendorNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should add the requested vendors", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validVendorAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.have.property("vendorname").and.equal("IBM");
          expect(res.body[ 1 ]).to.have.property("vendorname").and.equal("Quantum");
          expect(res.body[ 2 ]).to.have.property("vendorname").and.equal("Hewlett Packard");
          expect(res.body[ 3 ]).to.have.property("vendorname").and.equal("Amazon Web Services");
          vendorOne = res.body[ 0 ];
          vendorTwo = res.body[ 1 ];
          vendorThree = res.body[ 2 ];
          done();
        });
      });
      it("should return a list", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.length.above(0);
          done();
        });
      });
      it("should not add the requested vendors(list contains existing vendors) - 400333", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validVendorAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400333);
          done();
        });
      });
    });

    describe("#update()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstUser.accTkn)
        .send(invalidVendorUpdateParams_missingVendorId)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter is missing vendorid) - 400051", function(done)
      {
        invalidVendorUpdateParams_missingVendorId.vendorid = vendorOne.vendorid;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_missingVendorId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter is missing vendorname) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_missingVendorName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has non integer vendorid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_nonIntVendorName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter vendorid is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_arrayVendorId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter vendorname is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_arrayVendorName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has non-existent vendor ids) - 400332", function(done)
      {
        invalidVendorUpdateParams_nonExistentVendorIds[ 0 ].vendorid = vendorOne.vendorid;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_nonExistentVendorIds)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400332);
          done();
        });
      });

      it("should return error(parameter has existing vendor names) - 400333", function(done)
      {
        invalidVendorUpdateParams_ExistingVendorNames[ 0 ].vendorid = vendorOne.vendorid;
        invalidVendorUpdateParams_ExistingVendorNames[ 1 ].vendorid = vendorTwo.vendorid;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_ExistingVendorNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400333);
          done();
        });
      });

      it("should return error(one of the vendorid is an array) - 400051", function(done)
      {
        invalidVendorUpdateParams_ExistingVendorNames[ 0 ].vendorid = [ vendorOne.vendorid ];
        invalidVendorUpdateParams_ExistingVendorNames[ 1 ].vendorid = vendorTwo.vendorid;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_ExistingVendorNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(one of the vendorid is an array) - 400051", function(done)
      {
        invalidVendorUpdateParams_ExistingVendorNames[ 0 ].vendorname = [ vendorOne.vendorname ];
        invalidVendorUpdateParams_ExistingVendorNames[ 1 ].vendorname = vendorTwo.vendorname;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidVendorUpdateParams_ExistingVendorNames)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should update the requested vendor details", function(done)
      {
        validVendorUpdateParams[ 0 ].vendorid = vendorOne.vendorid;
        validVendorUpdateParams[ 1 ].vendorid = vendorTwo.vendorid;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validVendorUpdateParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect("[ [ { vendorid: 2, vendorname: 'Nano Systems' } ], [ { vendorid: 3, vendorname: 'Malgudi Ltd' } ] ]");
          done();
        });
      });

      it("should not update the requested vendor details(contains duplicate vendornames) - 400051", function(done)
      {
        var temp = validVendorUpdateParams[ 1 ].vendorname;
        validVendorUpdateParams[ 1 ].vendorname = validVendorUpdateParams[ 0 ].vendorname;
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validVendorUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400051);
          validVendorUpdateParams[ 1 ].vendorname = temp;
          done();
        });
      });
    });
  });

  describe("Cilocation Controller", function()
  {

    describe("#list()", function()
    {

      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no cilocations to list) - 400321", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400321);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationAddParams_missingLocationName)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter has no location name)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationAddParams_missingLocationName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has no location code)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationAddParams_missingLocationCode)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(building name is an array)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationAddParams_arrayBuildingName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(site category is an array)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationAddParams_arraySiteCategory)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should add the requested locations - all fields", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCilocationAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect('{"locationid": 1, "locationname": "Stockholm-12T-LK", "locationcode": "RS-12T-LK61", "buildingname": "Nordic Offsite", "sitecategory": "DR Site"}');
          locationOne = res.body;
          done();
        });
      });

      it("should add the requested locations - compulsory fields only", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/add")
        .set("Authorization", firstOperator.accTkn)
        .send(AWSMumbaiSiteLocation)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("locationid");
          done();
        });
      });
    });

    describe("#update()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationUpdateParams_missingLocationId)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter is missing location id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_missingLocationId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter is non integer location id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_nonIntLocationId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has location code) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_withLocationCode)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(location id is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_arrayLocationId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(location name is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_arrayLocationName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(building name is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_arrayBuildingName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(site category is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_arraySiteCategory)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non existent location id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCilocationUpdateParams_nonExistentLocationId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400322);
          done();
        });
      });

      it("should update the requested ci location", function(done)
      {
        validCilocationUpdateParams.locationid = locationOne.locationid;
        request(sails.hooks.http.app)
        .post("/cilocation/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCilocationUpdateParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.have.property('locationcode').and.equal('RS-12T-LK61');
          done();
        });
      });
    });

    describe("#search()", function()
    {
      it("should return error(empty search is not allowed)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField1) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_invalid1stParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField2) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_invalid2ndParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField3) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_invalid3rdParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn1) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_nonExistent1stColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn2) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_nonExistent2ndColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn3) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_nonExistent3rdColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should search for the ci location", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cilocation/search")
        .set("Authorization", firstUser.accTkn)
        .send(validCilocationSearchParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.be.above(0);
          done();
        });
      });
    });
  });

  describe("Citype Controller", function()
  {

    describe("#list()", function()
    {

      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no citypes to list) - 400301", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400301);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/add")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCitypeAddParams_duplicateCiTypes)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(a parameter has duplicate ci types)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCitypeAddParams_duplicateCiTypes)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(a parameter is not an array)", function(done)
      {
        var temp = validCitypeAddParams.citypes;
        validCitypeAddParams.citypes = 'jelly';
        request(sails.hooks.http.app)
        .post("/citype/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCitypeAddParams.citypes = temp;
          done();
        });
      });

      it("should add the requested citypes", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          citypeOne = res.body[ 0 ];
          citypeTwo = res.body[ 1 ];
          citypeThree = res.body[ 2 ];
          done();
        });
      });

      it("should return error(a parameter has existing ci types)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/add")
        .set("Authorization", firstOperator.accTkn)
        .send({ citypes: [ validCitypeAddParams.citypes[ 0 ] ] })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400302);
          done();
        });
      });
    });

    describe("#update()", function()
    {
      before('Setting the stage for CIType update', function(done)
      {
        validCitypeUpdateParams[ 0 ].citypeid = citypeOne.citypeid;
        validCitypeUpdateParams[ 1 ].citypeid = citypeTwo.citypeid;
        validCitypeUpdateParams[ 2 ].citypeid = citypeThree.citypeid;
        done();
      });

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstUser.accTkn)
        .send(validCitypeUpdateParams)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(a parameter is missing typeid) - 400051", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citypeid;
        delete validCitypeUpdateParams[ 0 ].citypeid;
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCitypeUpdateParams[ 0 ].citypeid = temp;
          done();
        });
      });

      it("should return error(a parameter has non integer typeid) - 400051", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citypeid;
        validCitypeUpdateParams[ 0 ].citypeid = 'jalebi';
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCitypeUpdateParams[ 0 ].citypeid = temp;
          done();
        });
      });

      it("should return error(a parameter has array of citypes) - 400051", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citype;
        validCitypeUpdateParams[ 0 ].citype = [ 'jalebi', 'jamun' ];
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCitypeUpdateParams[ 0 ].citype = temp;
          done();
        });
      });

      it("should return error(parameter has duplicate citypeids) - 400303", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citypeid;
        validCitypeUpdateParams[ 0 ].citypeid = validCitypeUpdateParams[ 1 ].citypeid;
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400303);
          validCitypeUpdateParams[ 0 ].citypeid = temp;
          done();
        });
      });

      it("should return error(parameter has duplicate citypes) - 400051", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citype;
        validCitypeUpdateParams[ 0 ].citype = validCitypeUpdateParams[ 1 ].citype;
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCitypeUpdateParams[ 0 ].citype = temp;
          done();
        });
      });

      it("should return error(parameter has non existent citype ids) - 400303", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citypeid;
        validCitypeUpdateParams[ 0 ].citypeid = 378234;
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400303);
          validCitypeUpdateParams[ 0 ].citypeid = temp;
          done();
        });
      });

      it("should return error(parameter has existing citypes) - 400302", function(done)
      {
        var temp = validCitypeUpdateParams[ 0 ].citype;
        validCitypeUpdateParams[ 0 ].citype = validCitypeAddParams.citypes[ 0 ];
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400302);
          validCitypeUpdateParams[ 0 ].citype = temp;
          done();
        });
      });

      it("should update the requested citypes", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCitypeUpdateParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect("[ [ { citypeid: 2, citype: 'network switches', cikey: 6 } ], [ { citypeid: 3, citype: 'firewalls', cikey: 2 } ], [ { citypeid: 4, citype: 'IDS', cikey: 8 } ] ]");
          done();
        });
      });
    });
  });

  describe("Cinetwork Controller", function()
  {

    describe("#list()", function()
    {

      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no citypes to list) - 400341", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400341);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/add")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkAddParams_missingPrimaryIp)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter does not have primaryip) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkAddParams_missingPrimaryIp)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter does not have network name) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkAddParams_missingNetworkName)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter does not have subnet) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkAddParams_missingSubnet)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter does not have gateway) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkAddParams_missingGateway)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should add the requested cinetwork", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCinetworkAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("primaryip").and.equal("10.83.219.34");
          expect(res.body).to.have.property("networkname").and.equal("Upstream-Strike3-Gateway-Log");
          expect(res.body).to.have.property("subnet").and.equal("255.255.255.0");
          expect(res.body).to.have.property("gateway").and.equal("10.83.219.1");
          networkOne = res.body;
          done();
        });
      });
    });

    describe("#getdetail()", function()
    {

      it("should return error(parameter without a ci network id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkGetdetailParams_missingCinetworkid)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(cinetworkid is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkGetdetailParams_arrayCinetworkid)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has a non existent network id) - 400342", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkGetdetailParams_nonExistentCinetworkid)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400342);
          done();
        });
      });

      it("should get details of the requested cinetwork", function(done)
      {
        validCinetworkGetdetailParams.cinetworkid = networkOne.cinetworkid;
        request(sails.hooks.http.app)
        .post("/cinetwork/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(validCinetworkGetdetailParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("cinetworkid").and.equal(networkOne.cinetworkid);
          expect(res.body).to.have.property("primaryip").and.equal("10.83.219.34");
          expect(res.body).to.have.property("networkname").and.equal("Upstream-Strike3-Gateway-Log");
          expect(res.body).to.have.property("subnet").and.equal("255.255.255.0");
          expect(res.body).to.have.property("gateway").and.equal("10.83.219.1");
          done();
        });
      });
    });

    describe("#update()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/update")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkUpdateParams_missingCinetworkid)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter is missing network id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkUpdateParams_missingCinetworkid)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(ci network id is an array) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkUpdateParams_arrayCinetworkid)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter has a non existent network id) - 400342", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCinetworkUpdateParams_nonExistentCinetworkid)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400342);
          done();
        });
      });

      it("should update the requested cinetwork", function(done)
      {
        validCinetworkUpdateParams.cinetworkid = networkOne.cinetworkid;
        request(sails.hooks.http.app)
        .post("/cinetwork/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCinetworkUpdateParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("cinetworkid").and.equal(networkOne.cinetworkid);
          expect(res.body).to.have.property("networkname").and.equal("JellyStub");
          done();
        });
      });
    });

    describe("#search()", function()
    {
      it("should return error(empty search is not allowed)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField1) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkSearchParams_invalid1stParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField2) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkSearchParams_invalid2ndParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField3) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkSearchParams_invalid3rdParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn1) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCilocationSearchParams_nonExistent1stColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn2) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkSearchParams_nonExistent2ndColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn3) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCinetworkSearchParams_nonExistent3rdColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should search for the ci network", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cinetwork/search")
        .set("Authorization", firstUser.accTkn)
        .send(validCinetworkSearchParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.be.above(0);
          done();
        });
      });
    });
  });

  describe("Cihardware Controller", function()
  {

    describe("#list()", function()
    {
      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no cihardware to list) - 400351", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400351);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/add")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareAddParams_missingHardwareTag)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(missing hardwaretag) - 400051", function(done)
      {
        invalidCihardwareAddParams_missingHardwareTag.manufacturervendorid = vendorOne.vendorid;
        request(sails.hooks.http.app)
        .post("/cihardware/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCihardwareAddParams_missingHardwareTag)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(missing vendorid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCihardwareAddParams_missingVendorId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non-interger vendorid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCihardwareAddParams_nonIntVendorId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non existent vendor) - 400332", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/add")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCihardwareAddParams_nonExistentVendorId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400332);
          done();
        });
      });

      it("should add the requested hardware successfully", function(done)
      {
        validCihardwareAddParams.manufacturervendorid = vendorOne.vendorid;
        validCihardwareAddParams.osvendorid = vendorThree.vendorid;
        validCihardwareAddParams.osname = 'HP-UX';
        validCihardwareAddParams.osversion = 'B 11.31';
        validCihardwareAddParams.biosvendorid = vendorThree.vendorid;
        request(sails.hooks.http.app)
        .post("/cihardware/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCihardwareAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("hardwaretag").and.equal("TGY47IK829");
          expect(res.body).to.have.property("manufacturervendorid").and.equal(validCihardwareAddParams.manufacturervendorid);
          expect(res.body).to.have.property("hwmodel").and.equal("HF-482R");
          expect(res.body).to.have.property("gridloc").and.equal("AF-YH5");
          expect(res.body).to.have.property("partnum").and.equal("IYU3M14O4Q");
          hardwareOne = res.body;
          done();
        });
      });

      it("should list the hardware", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ].gridloc).to.equal("AF-YH5");
          done();
        });
      });
    });

    describe("#getdetail()", function()
    {

      it("should return error(missing machine id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareGetDetailParams_missingMachineId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non-integer machine id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareGetDetailParams_nonIntMachineId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non-existent machine id) - 400352", function(done)
      {
        var temp = validCihardwareGetDetailParams.machineid;
        validCihardwareGetDetailParams.machineid = 45281;
        request(sails.hooks.http.app)
        .post("/cihardware/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(validCihardwareGetDetailParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400352);
          validCihardwareGetDetailParams.machineid = temp;
          done();
        });
      });

      it("should detail the requested hardware successfully", function(done)
      {
        validCihardwareGetDetailParams.machineid = hardwareOne.machineid;
        request(sails.hooks.http.app)
        .post("/cihardware/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send(validCihardwareGetDetailParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("machineid").and.equal(validCihardwareGetDetailParams.machineid);
          expect(res.body).to.have.property("hardwaretag").and.equal("TGY47IK829");
          expect(res.body).to.have.property("hwmodel").and.equal("HF-482R");
          expect(res.body).to.have.property("gridloc").and.equal("AF-YH5");
          expect(res.body).to.have.property("partnum").and.equal("IYU3M14O4Q");
          done();
        });
      });
    });

    describe("#update()", function()
    {
      before('Setting the stage for hardware update', function(done)
      {
        validCihardwareUpdateParams.machineid = hardwareOne.machineid;
        done();
      });

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/update")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareGetDetailParams_missingMachineId)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(missing machine id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCihardwareGetDetailParams_missingMachineId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non-integer machine id) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/update")
        .set("Authorization", firstOperator.accTkn)
        .send(invalidCihardwareGetDetailParams_nonIntMachineId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(non-existent machine id) - 400352", function(done)
      {
        var temp = validCihardwareUpdateParams.machineid;
        validCihardwareUpdateParams.machineid = 147274;
        request(sails.hooks.http.app)
        .post("/cihardware/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCihardwareUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400352);
          validCihardwareUpdateParams.machineid = temp;
          done();
        });
      });

      it("should return error(non-existent manufacturer vendorid id) - 400332", function(done)
      {
        var temp = validCihardwareUpdateParams.manufacturervendorid;
        validCihardwareUpdateParams.manufacturervendorid = 12345;
        request(sails.hooks.http.app)
        .post("/cihardware/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCihardwareUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400332);
          validCihardwareUpdateParams.manufacturervendorid = temp;
          done();
        });
      });

      it("should update the requested hardware", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCihardwareUpdateParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("machineid").and.equal(validCihardwareUpdateParams.machineid);
          expect(res.body).to.have.property("gridloc").and.equal("BK-LR9");
          done();
        });
      });
    });

    describe("#search()", function()
    {
      it("should return error(empty search is not allowed)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField1) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareSearchParams_invalid1stParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField2) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareSearchParams_invalid2ndParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField3) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareSearchParams_invalid3rdParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn1) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareSearchParams_nonExistent1stColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn2) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareSearchParams_nonExistent2ndColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn3) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCihardwareSearchParams_nonExistent3rdColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should search for the ci hardware", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cihardware/search")
        .set("Authorization", firstUser.accTkn)
        .send(validCihardwareSearchParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.be.above(0);
          done();
        });
      });
    });
  });

  describe("Service Controller", function()
  {
    describe("#list()", function()
    {
      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no services to list) - 400361", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400361);
          done();
        });
      });
    });

    describe("#add()", function()
    {
      before('Setting the stage for service add', function(done)
      {
        validParams_ServiceEntry.cbid = cbOne.cbid;
        done();
      });
      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstUser.accTkn)
        .send(validParams_ServiceEntry)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter without servicename) - 400051", function(done)
      {
        var temp = validParams_ServiceEntry.servicename;
        delete validParams_ServiceEntry.servicename;
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validParams_ServiceEntry)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validParams_ServiceEntry.servicename = temp;
          done();
        });
      });

      it("should return error(parameter without cbcode) - 400051", function(done)
      {
        var temp = validParams_ServiceEntry.cbid;
        delete validParams_ServiceEntry.cbid;
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validParams_ServiceEntry)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validParams_ServiceEntry.cbid = temp;
          done();
        });
      });

      it("should return error(parameter with not finite cbcode) - 400051", function(done)
      {
        var temp = validParams_ServiceEntry.cbid;
        validParams_ServiceEntry.cbid = 'nkas';
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validParams_ServiceEntry)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validParams_ServiceEntry.cbid = temp;
          done();
        });
      });

      it("should return error(parameter with service stateid) - 400051", function(done)
      {
        validParams_ServiceEntry.servicestateid = 4;
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validParams_ServiceEntry)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          delete validParams_ServiceEntry.servicestateid;
          done();
        });
      });

      it("should return error(parameter with service name array) - 400051", function(done)
      {
        var temp = validParams_ServiceEntry.servicename;
        validParams_ServiceEntry.servicename = [ 'serv1', 'serv2' ];
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidParams_serviceNameArray)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validParams_ServiceEntry.servicename = temp;
          done();
        });
      });

      it("should return error(non existent cb code) - 400204", function(done)
      {
        var temp = validParams_ServiceEntry.cbid;
        validParams_ServiceEntry.cbid = 2432323;
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidParams_nonExistentcbCode)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400204);
          validParams_ServiceEntry.cbid = temp;
          done();
        });
      });

      it("should return error(inactive cb code) - 400203", function(done)
      {
        var temp = validParams_ServiceEntry.cbid;
        validParams_ServiceEntry.cbid = cbTwo.cbid;
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validParams_ServiceEntry)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400203);
          validParams_ServiceEntry.cbid = temp;
          done();
        });
      });

      it("should add a service to the system", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validParams_ServiceEntry)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("servicestateid").and.equal(1);
          serviceOne = res.body;
          done();
        });
      });
    });

    describe("#getdetail()", function()
    {

      it("should return error(parameter without serviceid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send({})
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter with invalid serviceid) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send({ serviceid: 'charid' })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter with non existent serviceid) - 400362", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send({ serviceid: 123123 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400362);
          done();
        });
      });

      it("should return details of requested service", function(done)
      {
        validParams_getServiceDetails.serviceid = serviceOne.serviceid;
        request(sails.hooks.http.app)
        .post("/service/getdetail")
        .set("Authorization", firstUser.accTkn)
        .send({ serviceid: serviceOne.serviceid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("servicestateid");
          done();
        });
      });
    });

    describe("#update()", function()
    {
      before('Setting the stage for service update', function(done)
      {
        validServiceUpdateParams.serviceid = serviceOne.serviceid;
        done();
      });
      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/update")
        .set("Authorization", firstUser.accTkn)
        .send()
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter without serviceid) - 400051", function(done)
      {
        var temp = validServiceUpdateParams.serviceid;
        delete validServiceUpdateParams.serviceid;
        request(sails.hooks.http.app)
        .post("/service/update")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidParams_noServiceId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validServiceUpdateParams.serviceid = temp;
          done();
        });
      });

      it("should return error(parameter with non existent serviceid) - 400362", function(done)
      {
        var temp = validServiceUpdateParams.serviceid;
        validServiceUpdateParams.serviceid = 234124;
        request(sails.hooks.http.app)
        .post("/service/update")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceUpdateParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400362);
          validServiceUpdateParams.serviceid = temp;
          done();
        });
      });

      it("should not update the service(service is frozen) - 400363", function(done)
      {
        Service.update({ serviceid: serviceOne.serviceid }, { isfrozen: true })
        .then(function(service)
        {
          expect(service[ 0 ]).to.exist;
          expect(service[ 0 ]).to.have.property("isfrozen").and.equal(true);
          return service;
        })
        .then(function(service)
        {
          validServiceUpdateParams.cbid = cbTwo.cbid;
          validServiceUpdateParams.serviceid = serviceOne.serviceid;
          return request(sails.hooks.http.app)
          .post("/service/update")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceUpdateParams);
        })
        .then(function(res)
        {
          expect(res.status).to.equal(400);
          expect(res.body).to.have.property("code").and.equal(400363);
          return Service.update({ serviceid: serviceOne.serviceid }, { isfrozen: false });
        })
        .then(function(service)
        {
          expect(service[ 0 ]).to.exist;
          expect(service[ 0 ]).to.have.property("isfrozen").and.equal(false);
          done();
        })
        .catch(done);
      });

      it("should not update the service(service is already approved) - 400376", function(done)
      {
        Service.update({ serviceid: serviceOne.serviceid }, { servicestateid: 2 })
        .then(function(service)
        {
          expect(service[ 0 ]).to.exist;
          expect(service[ 0 ]).to.have.property("servicestateid").and.equal(2);
          return service;
        })
        .then(function(service)
        {
          return request(sails.hooks.http.app)
          .post("/service/update")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceUpdateParams)
          .expect(400)
          .then(function(res)
          {
            expect(res.body).to.have.property("code").and.equal(400376);
            return Service.update({ serviceid: serviceOne.serviceid }, { servicestateid: 1 });
          });
        })
        .then(function(service)
        {
          expect(service[ 0 ]).to.exist;
          expect(service[ 0 ]).to.have.property("servicestateid").and.equal(1);
          done();
        })
        .catch(done);
      });

      it("should update the requested service", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/update")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceUpdateParams)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property("serviceid").and.equal(validServiceUpdateParams.serviceid);
          expect(res.body).to.have.property("details").and.equal("Updated description");
          expect(res.body).to.have.property("cbid").and.equal(cbTwo.cbid);
          return Service.update({ serviceid: validServiceUpdateParams.serviceid }, { cbid: cbOne.cbid, details: "" });
        })
        .then(function(services)
        {
          done();
        })
        .catch(done);
      });
      it("should update the requested service(changing only the CBid)", function(done)
      {
        delete validServiceUpdateParams.details;
        request(sails.hooks.http.app)
        .post("/service/update")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceUpdateParams)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property("cbid").and.equal(validServiceUpdateParams.cbid);
          return Service.update({ serviceid: validServiceUpdateParams.serviceid }, { cbid: cbOne.cbid, details: "" });
        })
        .then(function(services)
        {
          done();
        })
        .catch(done);
      });
    });

    describe("#cancel()", function()
    {
      before('Setting the stage for service cancel', function(done)
      {
        validServiceCancelParams.serviceid = serviceOne.serviceid;
        done();
      });
      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/cancel")
        .set("Authorization", firstUser.accTkn)
        .send(validServiceCancelParams)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter is missing serviceid) - 400051", function(done)
      {
        var temp = validServiceCancelParams.serviceid;
        delete validServiceCancelParams.serviceid;
        request(sails.hooks.http.app)
        .post("/service/cancel")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceCancelParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validServiceCancelParams.serviceid = temp;
          done();
        });
      });

      it("should return error(parameter contains non-integer serviceid) - 400051", function(done)
      {
        var temp = validServiceCancelParams.serviceid;
        validServiceCancelParams.serviceid = 'nonint';
        request(sails.hooks.http.app)
        .post("/service/cancel")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceCancelParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validServiceCancelParams.serviceid = temp;
          done();
        });
      });

      it("should return error(parameter contains invalid serviceid) - 400362", function(done)
      {
        var temp = validServiceCancelParams.serviceid;
        validServiceCancelParams.serviceid = 4352342;
        request(sails.hooks.http.app)
        .post("/service/cancel")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceCancelParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400362);
          validServiceCancelParams.serviceid = temp;
          done();
        });
      });

      it("should return error(service is frozen) - 400363", function(done)
      {
        validServiceCancelParams.serviceid = serviceOne.serviceid;
        Service.update({ serviceid: validServiceCancelParams.serviceid }, { isfrozen: true })
        .then(function(services)
        {
          expect(services).to.exist;
          expect(services[ 0 ]).to.have.property("isfrozen").and.equal(true);
          return services;
        })
        .then(function(services)
        {
          return request(sails.hooks.http.app)
          .post("/service/cancel")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceCancelParams)
          .expect(400)
          .then(function(res)
          {
            expect(res.body).to.have.property("code").and.equal(400363);
            return Service.update({ serviceid: validServiceCancelParams.serviceid }, { isfrozen: false });
          });
        })
        .then(function(services)
        {
          expect(services).to.exist;
          expect(services[ 0 ]).to.have.property("isfrozen").and.equal(false);
          done();
        })
        .catch(done);
      });

      it("should return error(service is not in registration phase) - 400374", function(done)
      {
        Service.update({ serviceid: validServiceCancelParams.serviceid }, { servicestateid: 2 })
        .then(function(services)
        {
          expect(services).to.exist;
          expect(services[ 0 ]).to.have.property("servicestateid").and.equal(2);
          return services;
        })
        .then(function(services)
        {
          return request(sails.hooks.http.app)
          .post("/service/cancel")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceCancelParams)
          .expect(400)
          .then(function(res)
          {
            expect(res.body).to.have.property("code").and.equal(400374);
            return Service.update({ serviceid: validServiceCancelParams.serviceid }, { servicestateid: 1 });
          });
        })
        .then(function(services)
        {
          expect(services).to.exist;
          expect(services[ 0 ]).to.have.property("servicestateid").and.equal(1);
          done();
        })
        .catch(done);
      });

      it("should cancel the requested Service", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/cancel")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceCancelParams)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property("servicestateid").and.equal(3);
          return Service.update({ serviceid: validServiceCancelParams.serviceid }, { servicestateid: 1 });
        })
        .then(function(services)
        {
          expect(services).to.exist;
          expect(services[ 0 ]).to.have.property("servicestateid").and.equal(1);
          done();
        })
        .catch(done);
      });
    });

    describe("#search()", function()
    {
      it("should return error(empty search is not allowed)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField1) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidServiceSearchParams_invalid1stParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField2) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidServiceSearchParams_invalid2ndParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField3) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidServiceSearchParams_invalid3rdParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn1) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidServiceSearchParams_nonExistent1stColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn2) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidServiceSearchParams_nonExistent2ndColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn3) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidServiceSearchParams_nonExistent3rdColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should search for the ci service", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/search")
        .set("Authorization", firstUser.accTkn)
        .send(validServiceSearchParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.be.above(0);
          done();
        });
      });
    });
  });

  describe("Cidetail Controller", function()
  {

    describe("#list()", function()
    {

      it("should return error(empty parameter list) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/list")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(incorrect parameters) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": "a" })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(no services to list) - 400381", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/list")
        .set("Authorization", firstUser.accTkn)
        .send({ "page": 1, "limit": 100 })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400381);
          done();
        });
      });
    });

    describe("#add()", function()
    {

      it("should return error(requester doesn't have auhorization) - 403502", function(done)
      {
        validCidetailAddParams.citypeid = citypeOne.citypeid;
        validCidetailAddParams.envid = firstEnv.envid;
        validCidetailAddParams.vendorid = vendorOne.vendorid;
        validCidetailAddParams.locationid = locationOne.locationid;
        validCidetailAddParams.ciownerid = firstUser.userid;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstUser.accTkn)
        .send(validCidetailAddParams)
        .expect(403)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(403502);
          done();
        });
      });

      it("should return error(parameter without ciname) - 400051", function(done)
      {
        var temp = validCidetailAddParams.ciname;
        delete validCidetailAddParams.ciname;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCidetailAddParams.ciname = temp;
          done();
        });
      });

      it("should return error(parameter without assettag) - 400051", function(done)
      {
        var temp = validCidetailAddParams.assettag;
        delete validCidetailAddParams.assettag;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCidetailAddParams.assettag = temp;
          done();
        });
      });

      it("should return error(parameter includes cistateid) - 400051", function(done)
      {
        validCidetailAddParams.cistateid = 1;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          delete validCidetailAddParams.cistateid;
          done();
        });
      });

      it("should return error(parameter missing citype) - 400051", function(done)
      {
        var temp = validCidetailAddParams.citypeid;
        delete validCidetailAddParams.citypeid;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCidetailAddParams.citypeid = temp;
          done();
        });
      });

      it("should return error(parameter includes serviceid) - 400051", function(done)
      {
        validCidetailAddParams.serviceids = [ serviceOne.serviceid, serviceTwo.serviceid ];
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          delete validCidetailAddParams.serviceids;
          done();
        });
      });

      it("should return error(parameter missing environment) - 400051", function(done)
      {
        var temp = validCidetailAddParams.envid;
        delete validCidetailAddParams.envid;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCidetailAddParams.envid = temp;
          done();
        });
      });

      it("should return error(parameter missing vendor) - 400051", function(done)
      {
        var temp = validCidetailAddParams.vendorid;
        delete validCidetailAddParams.vendorid;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCidetailAddParams.vendorid = temp;
          done();
        });
      });

      it("should return error(parameter missing location) - 400051", function(done)
      {
        var temp = validCidetailAddParams.locationid;
        delete validCidetailAddParams.locationid;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          validCidetailAddParams.locationid = temp;
          done();
        });
      });

      it("should return error(parameter includes isfrozen) - 400051", function(done)
      {
        validCidetailAddParams.isfrozen = true;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          delete validCidetailAddParams.isfrozen;
          done();
        });
      });

      it("should return error(value of customerfacing is non-boolean) - 400051", function(done)
      {
        validCidetailAddParams.customerfacing = 123;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          delete validCidetailAddParams.customerfacing;
          done();
        });
      });

      it("should return error(value of isundermaint is non-boolean) - 400051", function(done)
      {
        validCidetailAddParams.isundermaint = 123;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          delete validCidetailAddParams.isundermaint;
          done();
        });
      });

      it("should return error(parameter has invalid citype) - 400303", function(done)
      {
        var temp = validCidetailAddParams.citypeid;
        validCidetailAddParams.citypeid = 35;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400303);
          validCidetailAddParams.citypeid = temp;
          done();
        });
      });

      it("should return error(parameter has invalid environment) - 400313", function(done)
      {
        var temp = validCidetailAddParams.envid;
        validCidetailAddParams.envid = 245645;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400313);
          validCidetailAddParams.envid = temp;
          done();
        });
      });

      it("should return error(parameter has invalid vendor) - 400332", function(done)
      {
        var temp = validCidetailAddParams.vendorid;
        validCidetailAddParams.vendorid = 29845;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400332);
          validCidetailAddParams.vendorid = temp;
          done();
        });
      });

      it("should return error(parameter has invalid location) - 400322", function(done)
      {
        var temp = validCidetailAddParams.locationid;
        validCidetailAddParams.locationid = 19491;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400322);
          validCidetailAddParams.locationid = temp;
          done();
        });
      });

      it("should return error(parameter has invalid machine) - 400352", function(done)
      {
        var temp = validCidetailAddParams.machineid;
        validCidetailAddParams.machineid = 29431;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400352);
          validCidetailAddParams.machineid = temp;
          done();
        });
      });

      it("should return error(parameter has invalid network) - 400342", function(done)
      {
        var temp = validCidetailAddParams.cinetworkid;
        validCidetailAddParams.cinetworkid = 28420;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400342);
          validCidetailAddParams.cinetworkid = temp;
          done();
        });
      });

      it("should add the requested ci", function(done)
      {
        validCidetailAddParams.machineid = hardwareOne.machineid;
        validCidetailAddParams.cinetworkid = networkOne.cinetworkid;
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailAddParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("ciname").and.equal("HSG4-JSJKAQM-T");
          expect(res.body).to.have.property("assettag").and.equal("SAMAK82KD");
          expect(res.body).to.have.property("cistateid").and.equal(1);
          expect(res.body).to.have.property("citypeid").and.equal(citypeOne.citypeid);
          expect(res.body).to.have.property("envid").and.equal(firstEnv.envid);
          expect(res.body).to.have.property("vendorid").and.equal(vendorOne.vendorid);
          expect(res.body).to.have.property("customerfacing").and.equal(false);
          expect(res.body).to.have.property("isundermaint").and.equal(true);
          expect(res.body).to.have.property("ciownerid").and.equal(firstUser.userid);
          expect(res.body).to.have.property("installationdate").and.equal(validCidetailAddParams.installationdate);
          expect(res.body).to.have.property("isfrozen").and.equal(false);
          cidetailOne = res.body;
          done();
        });
      });
    });

    describe("#search()", function()
    {
      it("should return error(empty search is not allowed)", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField1) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCidetailSearchParams_invalid1stParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField2) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCidetailSearchParams_invalid2ndParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentField3) - 400051", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCidetailSearchParams_invalid3rdParam)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn1) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCidetailSearchParams_nonExistent1stColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn2) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCidetailSearchParams_nonExistent2ndColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should return an error(incorrect parameters - Non ExistentColumn3) - 400053", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(invalidCidetailSearchParams_nonExistent3rdColumn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400053);
          done();
        });
      });
      it("should search for the ci location", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/search")
        .set("Authorization", firstUser.accTkn)
        .send(validCidetailSearchParams)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ]).to.be.above(0);
          done();
        });
      });
    });
  });

  describe("#update()", function()
  {

    it("should return error(requester doesn't have auhorization) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstUser.accTkn)
      .send(validCidetailUpdateParams)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });

    it("should return error(parameter is missing ciid) - 400051", function(done)
    {
      var temp = validCidetailUpdateParams.ciid;
      delete validCidetailUpdateParams.ciid;
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        validCidetailUpdateParams.ciid = temp;
        done();
      });
    });

    it("should return error(parameter has non-integer ciid) - 400051", function(done)
    {
      var temp = validCidetailUpdateParams.ciid;
      validCidetailUpdateParams.ciid = 'shaft7';
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        validCidetailUpdateParams.ciid = temp;
        done();
      });
    });

    it("should return error(parameter includes ciname) - 400051", function(done)
    {
      validCidetailUpdateParams.ciname = 'somename';
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        delete validCidetailUpdateParams.ciname;
        done();
      });
    });

    it("should return error(parameter includes assettag) - 400051", function(done)
    {
      validCidetailUpdateParams.assettag = 'sometag';
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        delete validCidetailUpdateParams.assettag;
        done();
      });
    });

    it("should return error(parameter includes cistate) - 400051", function(done)
    {
      validCidetailUpdateParams.cistateid = 4;
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        delete validCidetailUpdateParams.cistateid;
        done();
      });
    });

    it("should return error(parameter includes isfrozen) - 400051", function(done)
    {
      validCidetailUpdateParams.isfrozen = true;
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        delete validCidetailUpdateParams.isfrozen;
        done();
      });
    });

    it("should return error(parameter includes serviceids) - 400051", function(done)
    {
      validCidetailUpdateParams.serviceids = [ serviceOne.serviceid, serviceTwo.serviceid ];
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        delete validCidetailUpdateParams.serviceids;
        done();
      });
    });

    it("should return error(parameter includes invalid ciid) - 400382", function(done)
    {
      var temp = validCidetailUpdateParams.ciid;
      validCidetailUpdateParams.ciid = 589284;
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400382);
        validCidetailUpdateParams.ciid = temp;
        done();
      });
    });

    it("should return error(parameter has frozen ci) - 400383", function(done)
    {
      validCidetailUpdateParams.ciid = cidetailOne.ciid;
      Cidetail.update({ ciid: validCidetailUpdateParams.ciid }, { isfrozen: true })
      .then(function(cis)
      {
        expect(cis[ 0 ]).to.exist;
        expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(true);
        return cis;
      })
      .then(function(cis)
      {
        return request(sails.hooks.http.app)
        .post("/cidetail/update")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailUpdateParams)
        .expect(400)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400383);
          return Cidetail.update({ ciid: validCidetailUpdateParams.ciid }, { isfrozen: false });
        });
      })
      .then(function(cis)
      {
        expect(cis[ 0 ]).to.exist;
        expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(false);
        done();
      })
      .catch(done);
    });

    it("should update the requested ci", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/update")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailUpdateParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("systemsoftware1").and.equal("Splunk");
        done();
      });
    });
  });

  describe("#getdetail()", function(done)
  {

    it("should return error(parameter is missing ciid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/getdetail")
      .set("Authorization", firstUser.accTkn)
      .send(invalidCidetailGetdetail_missingCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(ciid is an array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/getdetail")
      .set("Authorization", firstUser.accTkn)
      .send(invalidCidetailGetdetail_arrayCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(ciid is not an integer) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/getdetail")
      .set("Authorization", firstUser.accTkn)
      .send(invalidCidetailGetdetail_nonIntCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(paramter contains invalid ciid) - 400382", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/getdetail")
      .set("Authorization", firstUser.accTkn)
      .send(invalidCidetailGetdetail_invalidCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400382);
        done();
      });
    });

    it("should return the requested ci details", function(done)
    {
      validCidetailGetDetailParams.ciid = cidetailOne.ciid;
      request(sails.hooks.http.app)
      .post("/cidetail/getdetail")
      .set("Authorization", firstUser.accTkn)
      .send(validCidetailGetDetailParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("ciname").and.equal("HSG4-JSJKAQM-T");
        expect(res.body).to.have.property("isfrozen").and.equal(false);
        done();
      });
    });
  });

  describe("#cancel()", function()
  {

    it("should return error(requester doesn't have auhorization) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/cancel")
      .set("Authorization", firstUser.accTkn)
      .send(invalidCidetailCancelParams_missingCiid)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });

    it("should return error(parameter is missing ciid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/cancel")
      .set("Authorization", firstOperator.accTkn)
      .send(invalidCidetailCancelParams_missingCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(ciid is not an integer) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/cancel")
      .set("Authorization", firstOperator.accTkn)
      .send(invalidCidetailCancelParams_nonIntCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(paramter contains invalid ciid) - 400382", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/cancel")
      .set("Authorization", firstOperator.accTkn)
      .send(invalidCidetailCancelParams_invalidCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400382);
        done();
      });
    });

    it("should return error(ci is frozen) - 400383", function(done)
    {
      validCidetailCancelParams.ciid = cidetailOne.ciid;
      Cidetail.update({ ciid: validCidetailCancelParams.ciid }, { isfrozen: true })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(true);
        return cis;
      })
      .then(function(cis)
      {
        return request(sails.hooks.http.app)
        .post("/cidetail/cancel")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailCancelParams)
        .expect(400)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400383);
          return Cidetail.update({ ciid: validCidetailCancelParams.ciid }, { isfrozen: false });
        });
      })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(false);
        done();
      })
      .catch(done);
    });

    it("should return error(ci is in higher then Installed state) - 400387", function(done)
    {
      Cidetail.update({ ciid: validCidetailCancelParams.ciid }, { cistateid: 5 })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("cistateid").and.equal(5);
        return cis;
      })
      .then(function(cis)
      {
        return request(sails.hooks.http.app)
        .post("/cidetail/cancel")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailCancelParams)
        .expect(400)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400387);
          return Cidetail.update({ ciid: validCidetailCancelParams.ciid }, { cistateid: 1 });
        });
      })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("cistateid").and.equal(1);
        done();
      })
      .catch(done);
    });

    it("should cancel the requested CI", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/cancel")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailCancelParams)
      .expect(200)
      .then(function(res)
      {
        expect(res.body).to.have.property("cistateid").and.equal(6);
        return Cidetail.update({ ciid: validCidetailCancelParams.ciid }, { cistateid: 1 });
      })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("cistateid").and.equal(1);
        done();
      })
      .catch(done);
    });
  });
});

describe("CMDB Integration Tests", function()
{
  before("Staging Test Enviroment", function(done)
  {
    request(sails.hooks.http.app)
    .post('/auth/signin')
    .send(superUser)
    .end(function(err, res)
    {
      if(err) return done(err);

      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      done();
    });
  });

  describe("State Transfer of CI from Registration to Approved", function()
  {

    it("should return error(requester doesn't have auhorization) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstUser.accTkn)
      .send(invalidCidetailGetdetail_missingCiid)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });

    it("should return error(parameter is missing ciid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(invalidCidetailGetdetail_missingCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(parameter has non-integer ciid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(invalidCidetailNextParams_nonIntCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(parameter has invalid ciid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(invalidCidetailNextParams_invalidCiid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(ciid is frozen) - 400383", function(done)
    {
      validCidetailsNextParams.ciid = cidetailOne.ciid;
      Cidetail.update({ ciid: validCidetailsNextParams.ciid }, { isfrozen: true })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(true);
        return cis;
      })
      .then(function(cis)
      {
        return request(sails.hooks.http.app)
        .post("/cidetail/next")
        .set("Authorization", firstOperator.accTkn)
        .send(validCidetailsNextParams)
        .expect(400)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400383);
          return Cidetail.update({ ciid: validCidetailsNextParams.ciid }, { isfrozen: false });
        });
      })
      .then(function(cis)
      {
        expect(cis).to.exist;
        expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(false);
        done();
      })
      .catch(done);
    });

    it("should return error(ci not associated with any service) - 400386", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailsNextParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400386);
        done();
      });
    });

    describe("Associate a CI with Service", function()
    {

      it("should return error(parameter without any serviceid) - 400051", function(done)
      {
        invalidAssocParams_noServiceIds.ciid = cidetailOne.ciid;
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidAssocParams_noServiceIds)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter without any Ciid) - 400051", function(done)
      {
        invalidAssocParams_noCiId.serviceids = [ serviceOne.serviceid ];
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidAssocParams_noCiId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter with non integer Ciid) - 400051", function(done)
      {
        invalidAssocParams_nonIntegerCiId.serviceids = [ serviceOne.serviceid ];
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidAssocParams_nonIntegerCiId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter with duplicate serviceid) - 400051", function(done)
      {
        invalidAssocParams_duplicateServiceId.serviceids = [ serviceOne.serviceid, serviceOne.serviceid ];
        invalidAssocParams_duplicateServiceId.ciid = cidetailOne.ciid;
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidAssocParams_duplicateServiceId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400051);
          done();
        });
      });

      it("should return error(parameter with non existent serviceid) - 400051", function(done)
      {
        invalidAssocParams_nonExistentServiceId.serviceids = [ serviceOne.serviceid, 3817 ];
        invalidAssocParams_nonExistentServiceId.ciid = cidetailOne.ciid;
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(invalidAssocParams_nonExistentServiceId)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400362);
          done();
        });
      });

      it("should not initiate association(service is frozen) - 400363", function(done)
      {
        invalidAssocParams_FrozenServiceId.serviceids = [ serviceOne.serviceid ];
        invalidAssocParams_FrozenServiceId.ciid = cidetailOne.ciid;
        Service.update({ serviceid: invalidAssocParams_FrozenServiceId.serviceids[ 0 ] }, { isfrozen: true, details: "freezing for assoc check" })
        .then(function(service)
        {
          expect(service[ 0 ]).to.exist;
          expect(service[ 0 ]).to.have.property("isfrozen").and.equal(true);
          return service;
        })
        .then(function(service)
        {
          return request(sails.hooks.http.app)
          .post("/service/assoc")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(invalidAssocParams_FrozenServiceId)
          .expect(400)
          .then(function(res)
          {
            expect(res.body).to.have.property("code").and.equal(400363);
            return Service.update({ serviceid: invalidAssocParams_FrozenServiceId.serviceids[ 0 ] }, { isfrozen: false, details: "unfreezing after assoc check" });
          });
        })
        .then(function(service)
        {
          expect(service[ 0 ]).to.exist;
          expect(service[ 0 ]).to.have.property("isfrozen").and.equal(false);
          done();
        })
        .catch(done);
      });

      it("should return error(service is not in approved phase) - 400369", function(done)
      {
        validServiceAssocParams.serviceids = [ serviceOne.serviceid ];
        validServiceAssocParams.ciid = cidetailOne.ciid;
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceAssocParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400369);
          done();
        });
      });

      describe("Service state transition from Registered to Approved", function()
      {
        it("should return error(service deosn't have a support queue) - 400388", function(done)
        {
          validParams_serviceId.serviceid = serviceOne.serviceid;
          request(sails.hooks.http.app)
          .post("/service/next")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validParams_serviceId)
          .expect(400)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body).to.have.property("code").and.equal(400388);
            done();
          });
        });

        describe("Associate a Queue with the service", function()
        {

          it("should return error(invalid serviceid) - 400362", function(done)
          {
            validServiceQueueAssignParams.serviceid = 235;
            validServiceQueueAssignParams.supportqueue = 261;
            request(sails.hooks.http.app)
            .post("/service/queueassign")
            .set("Authorization", firstBDAdmin.accTkn)
            .send(validServiceQueueAssignParams)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400362);
              done();
            });
          });

          it("should return error(invalid queue) - 400254", function(done)
          {
            validServiceQueueAssignParams.serviceid = serviceOne.serviceid;
            request(sails.hooks.http.app)
            .post("/service/queueassign")
            .set("Authorization", firstBDAdmin.accTkn)
            .send(validServiceQueueAssignParams)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400254);
              done();
            });
          });

          it("should return error(requested service is frozen) - 400363", function(done)
          {
            Service.update({ serviceid: validServiceQueueAssignParams.serviceid }, { isfrozen: true })
            .then(function(service)
            {
              expect(service[ 0 ].isfrozen).to.equal(true);
              return request(sails.hooks.http.app)
              .post("/service/queueassign")
              .set("Authorization", firstBDAdmin.accTkn)
              .send(validServiceQueueAssignParams);
            })
            .then(function(res)
            {
              expect(res.body).to.have.property("code").and.equal(400363);
              return Service.update({ serviceid: validServiceQueueAssignParams.serviceid }, { isfrozen: false });
            })
            .then(function(service)
            {
              expect(service[ 0 ].isfrozen).to.equal(false);
              done();
            })
            .catch(done);
          });

          it("should return error(requested service is archived) - 400380", function(done)
          {
            Service.update({ serviceid: validServiceQueueAssignParams.serviceid }, { servicestateid: enums.serviceStateIds.ARCHIVED })
            .then(function(service)
            {
              expect(service[ 0 ].servicestateid).to.equal(enums.serviceStateIds.ARCHIVED);
              return request(sails.hooks.http.app)
              .post("/service/queueassign")
              .set("Authorization", firstBDAdmin.accTkn)
              .send(validServiceQueueAssignParams);
            })
            .then(function(res)
            {
              expect(res.body).to.have.property("code").and.equal(400380);
              return Service.update({ serviceid: validServiceQueueAssignParams.serviceid }, { servicestateid: enums.serviceStateIds.REGISTRATION });
            })
            .then(function(service)
            {
              expect(service[ 0 ].servicestateid).to.equal(enums.serviceStateIds.REGISTRATION);
              done();
            })
            .catch(done);
          });

          it("should return error(queue is frozen) - 400264", function(done)
          {
            validServiceQueueAssignParams.supportqueue = queueOne.queueid;
            Queue.update({ queueid: validServiceQueueAssignParams.supportqueue }, { isfrozen: true })
            .then(function(queue)
            {
              expect(queue[ 0 ].isfrozen).to.equal(true);
              return request(sails.hooks.http.app)
              .post("/service/queueassign")
              .set("Authorization", firstBDAdmin.accTkn)
              .send(validServiceQueueAssignParams);
            })
            .then(function(res)
            {
              expect(res.body).to.have.property("code").and.equal(400264);
              return Queue.update({ queueid: validServiceQueueAssignParams.supportqueue }, { isfrozen: false });
            })
            .then(function(queue)
            {
              expect(queue[ 0 ].isfrozen).to.equal(false);
              done();
            })
            .catch(done);
          });

          it("should return error(queue is inactive) - 400253", function(done)
          {
            Queue.update({ queueid: validServiceQueueAssignParams.supportqueue }, { isactive: false })
            .then(function(queue)
            {
              expect(queue[ 0 ].isactive).to.equal(false);
              return request(sails.hooks.http.app)
              .post("/service/queueassign")
              .set("Authorization", firstBDAdmin.accTkn)
              .send(validServiceQueueAssignParams);
            })
            .then(function(res)
            {
              expect(res.body).to.have.property("code").and.equal(400253);
              return Queue.update({ queueid: validServiceQueueAssignParams.supportqueue }, { isactive: true });
            })
            .then(function(queue)
            {
              expect(queue[ 0 ].isactive).to.equal(true);
              done();
            })
            .catch(done);
          });

          it("should initiate approval for service queue assisnment", function(done)
          {
            request(sails.hooks.http.app)
            .post("/service/queueassign")
            .set("Authorization", firstBDAdmin.accTkn)
            .send(validServiceQueueAssignParams)
            .expect(200)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body.typeofapproval).to.equal(enums.approval.ASSIGN_SERVICE2Q);
              expect(res.body.entityid).to.equal(validServiceQueueAssignParams.serviceid);
              expect(res.body.targetentityid).to.equal(validServiceQueueAssignParams.supportqueue);
              expect(res.body.isactive).to.equal(true);
              validServiceQueueAssignApprovalParams.approvalid = res.body.approvalid;
              done();
            });
          });

          it("should approve the service to queue assignment", function(done)
          {
            request(sails.hooks.http.app)
            .post("/orgapproval/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(validServiceQueueAssignApprovalParams)
            .expect(200)
            .end(function(err, res)
            {
              if(err) return done(err);
              done();
            });
          });
        });
      });

      describe("Service state transition from Registered to Approved - Attempt #2", function()
      {
        it("should return error(requester doesn't have auhorization) - 403502", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/next")
          .set("Authorization", firstUser.accTkn)
          .send(invalidParams_nonExistentServiceId)
          .expect(403)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body).to.have.property("code").and.equal(403502);
            done();
          });
        });

        it("should return error(parameter with non existent serviceid) - 400362", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/next")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(invalidParams_nonExistentServiceId)
          .expect(400)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body).to.have.property("code").and.equal(400362);
            done();
          });
        });

        it("should not move the service to next state(service is frozen) - 400363", function(done)
        {
          Service.update({ serviceid: validParams_serviceId.serviceid }, { isfrozen: true, details: "freezing for next check" })
          .then(function(service)
          {
            expect(service[ 0 ]).to.exist;
            expect(service[ 0 ]).to.have.property("isfrozen").and.equal(true);
            return service;
          })
          .then(function(service)
          {
            return request(sails.hooks.http.app)
            .post("/service/next")
            .set("Authorization", firstBDAdmin.accTkn)
            .send(validParams_serviceId)
            .expect(400)
            .then(function(res)
            {
              expect(res.body).to.have.property("code").and.equal(400363);
              return Service.update({ serviceid: validParams_serviceId.serviceid }, { isfrozen: false, details: "unfreezing after next check" });
            });
          })
          .then(function(service)
          {
            expect(service[ 0 ]).to.exist;
            expect(service[ 0 ]).to.have.property("isfrozen").and.equal(false);
            done();
          })
          .catch(done);
        });

        it("should not move the service to next state(service is Archived) - 400365", function(done)
        {
          Service.update({ serviceid: validParams_serviceId.serviceid }, { servicestateid: 3 })
          .then(function(service)
          {
            expect(service[ 0 ]).to.exist;
            expect(service[ 0 ]).to.have.property("servicestateid").and.equal(3);
            return service;
          })
          .then(function(service)
          {
            return request(sails.hooks.http.app)
            .post("/service/next")
            .set("Authorization", firstBDAdmin.accTkn)
            .send(validParams_serviceId)
            .expect(400)
            .then(function(res)
            {
              expect(res.body).to.have.property("code").and.equal(400365);
              return Service.update({ serviceid: validParams_serviceId.serviceid }, { servicestateid: 1 });
            });
          })
          .then(function(service)
          {
            expect(service[ 0 ]).to.exist;
            expect(service[ 0 ]).to.have.property("servicestateid").and.equal(1);
            done();
          })
          .catch(done);
        });

        it("should initiate approvals for next state(service is in Registration)", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/next")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validParams_serviceId)
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property("isfrozen").and.equal(true);
            return Serviceapproval.findOne({ approverserviceid: res.body.serviceid, type: "state", isactive: true });
          })
          .then(function(approval)
          {
            serviceApprovalOne = approval;
            return Service.findOne({ serviceid: validParams_serviceId.serviceid });
          })
          .then(function(service)
          {
            return Cb.findOne({ cbid: service.cbid });
          })
          .then(function(cb)
          {
            expect(serviceApprovalOne).to.have.property("approverid").and.equal(cb.cbownerid);
            expect(serviceApprovalOne).to.have.property("approverserviceid").and.equal(validParams_serviceId.serviceid);
            expect(serviceApprovalOne).to.have.property("ciid").and.equal(null);
            expect(serviceApprovalOne).to.have.property("isapproved").and.equal(null);
            done();
          })
          .catch(done);
        });

        describe("Approval of Registration to Approved state transisition request", function()
        {
          it("should return error(parameter without serviceid) - 400051", function(done)
          {
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidParams_noServiceId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter without type) - 400051", function(done)
          {
            invalidApproveParams_withoutType.approvalid = serviceApprovalOne.approvalid;
            invalidApproveParams_withoutType.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_withoutType)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter with invalid approvalid) - 400051", function(done)
          {
            invalidApproveParams_invalidApprovalId.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_invalidApprovalId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter without isApproved) - 400051", function(done)
          {
            invalidApproveParams_nonExistentisApproved.approvalid = serviceApprovalOne.approvalid;
            invalidApproveParams_nonExistentisApproved.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_nonExistentisApproved)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter with null isApproved) - 400051", function(done)
          {
            invalidApproveParams_nullIsApproved.approvalid = serviceApprovalOne.approvalid;
            invalidApproveParams_nullIsApproved.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_nullIsApproved)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter with non existent serviceid) - 400051", function(done)
          {
            invalidApproveParams_nonExistentServiceId.approvalid = serviceApprovalOne.approvalid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_nonExistentServiceId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter with non existent approval id) - 400368", function(done)
          {
            invalidApproveParams_nonExistentApprovalId.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_nonExistentApprovalId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400368);
              done();
            });
          });

          it("should return error(parameter with incorrect approver id) - 400367", function(done)
          {
            invalidApproveParams_incorrectApprover.approvalid = serviceApprovalOne.approvalid;
            invalidApproveParams_incorrectApprover.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstUser.accTkn)
            .send(invalidApproveParams_incorrectApprover)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400367);
              done();
            });
          });

          it("should return error(parameter with incorrect serviceid) - 400370", function(done)
          {
            invalidApproveParams_incorrectServiceId.approvalid = serviceApprovalOne.approvalid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(invalidApproveParams_incorrectServiceId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400370);
              done();
            });
          });

          it("should decline the approval request", function(done)
          {
            validApproveParams_declineStateRequest.approvalid = serviceApprovalOne.approvalid;
            validApproveParams_declineStateRequest.serviceid = serviceApprovalOne.approverserviceid;
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(validApproveParams_declineStateRequest)
            .expect(200)
            .then(function(res)
            {
              expect(res.body).to.have.property("servicestateid").and.equal(1);
              expect(res.body).to.have.property("isfrozen").and.equal(false);
              return Serviceapproval.findOne({ approvalid: validApproveParams_declineStateRequest.approvalid });
            })
            .then(function(approval)
            {
              expect(approval).to.have.property("isactive").and.equal(false);
              done();
            })
            .catch(done);
          });

          it("should return error(approval already processed) - 400366", function(done)
          {
            request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(validApproveParams_declineStateRequest)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400366);
              done();
            });
          });

          it("should approve the request and move the service to next state", function(done)
          {
            var approvalReq =
            {
              "type": "state",
              "isapproved": true
            };
            request(sails.hooks.http.app)
            .post("/service/next")
            .set("Authorization", firstBDAdmin.accTkn)
            .send(validParams_serviceId)
            .expect(200)
            .then(function(res)
            {
              expect(res.body).to.have.property("isfrozen").and.equal(true);
              return Serviceapproval.findOne({ approverserviceid: res.body.serviceid, type: "state", isactive: true });
            })
            .then(function(approval)
            {
              serviceApprovalOne = approval;
              approvalReq.approvalid = serviceApprovalOne.approvalid;
              approvalReq.serviceid = validParams_serviceId.serviceid;
              return request(sails.hooks.http.app)
              .post("/service/approve")
              .set("Authorization", firstCBOwner.accTkn)
              .send(approvalReq)
              .expect(200)
              .then(function(res)
              {
                expect(res.body).to.have.property("servicestateid").and.equal(2);
                expect(res.body).to.have.property("isfrozen").and.equal(false);
              });
            })
            .then(function(res)
            {
              return Serviceapproval.findOne({ approvalid: approvalReq.approvalid });
            })
            .then(function(approval)
            {
              expect(approval).to.have.property("isactive").and.equal(false);
              done();
            })
            .catch(done);
          });
        });

        describe("Transfer a Service across chargeback code", function()
        {

          it("should return error(requester doesn't have auhorization) - 403502", function(done)
          {
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstUser.accTkn)
            .send(invalidServiceTransferInitiateParams_missingEntityId)
            .expect(403)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(403502);
              done();
            });
          });

          it("should return error(parameter is missing entity id) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_missingEntityId.targetentityid = cbThree.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_missingEntityId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter has non-integer entity id) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_missingEntityId.targetentityid = cbThree.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_nonIntEntityId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter has invalid entity id) - 400362", function(done)
          {
            invalidServiceTransferInitiateParams_invalidEntityId.targetentityid = cbThree.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_invalidEntityId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400362);
              done();
            });
          });

          it("should return error(parameter is missing target entity id) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_missingTargetEntityId.entityid = serviceOne.serviceid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_missingTargetEntityId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter has non-integer target entity id) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_nonIntTargetEntityId.entityid = serviceOne.serviceid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_nonIntTargetEntityId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter has invalid target entity id) - 400204", function(done)
          {
            invalidServiceTransferInitiateParams_invalidTargetEntityId.entityid = serviceOne.serviceid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_invalidTargetEntityId)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400204);
              done();
            });
          });

          it("should return error(parameter is missing approval type) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_missingApprovalType.entityid = serviceOne.serviceid;
            invalidServiceTransferInitiateParams_missingApprovalType.targetentityid = cbThree.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_missingApprovalType)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter has non-integer approval type) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_nonIntApprovalType.entityid = serviceOne.serviceid;
            invalidServiceTransferInitiateParams_nonIntApprovalType.targetentityid = cbThree.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_nonIntApprovalType)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(parameter has invalid approval type) - 400051", function(done)
          {
            invalidServiceTransferInitiateParams_invalidApprovalType.entityid = serviceOne.serviceid;
            invalidServiceTransferInitiateParams_invalidApprovalType.targetentityid = cbThree.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(invalidServiceTransferInitiateParams_invalidApprovalType)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400051);
              done();
            });
          });

          it("should return error(entity is already owned by the target id)", function(done)
          {
            validServiceTransferInitiateParams.entityid = serviceOne.serviceid;
            validServiceTransferInitiateParams.targetentityid = cbOne.cbid;
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(validServiceTransferInitiateParams)
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body).to.have.property("code").and.equal(400210);
              validServiceTransferInitiateParams.targetentityid = cbThree.cbid;
              done();
            });
          });

          it("should successfully initiate transfer", function(done)
          {
            var approvals = [];
            request(sails.hooks.http.app)
            .post("/orgapproval/initiatetransfer")
            .set("Authorization", firstOperator.accTkn)
            .send(validServiceTransferInitiateParams)
            .expect(200)
            .then(function(res)
            {
              approvals = res.body[ 0 ];
              expect(res.body[ 1 ][ 0 ]).to.have.property("serviceid").and.equal(validServiceTransferInitiateParams.entityid);
              expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(true);
              orgApproval1 = res.body[ 0 ][ 0 ];
              orgApproval2 = res.body[ 0 ][ 1 ];
              done();
            });
          });

          describe("Approve the Service Transfer request", function()
          {
            it("OrgApproval:list() - should list approvals pending approval from the requester", function(done)
            {
              request(sails.hooks.http.app)
              .post("/orgapproval/list")
              .set("Authorization", thirdCBOwner.accTkn)
              .send({ "page": 1, "limit": 100 })
              .expect(200)
              .then(function(res)
              {
                expect(res.body).to.have.lengthOf(1);
                done();
              })
              .catch(done);
            });

            it("should successfully decline the request - 2nd Approver Declined", function(done)
            {
              request(sails.hooks.http.app)
              .post("/orgapproval/approve")
              .set("Authorization", thirdCBOwner.accTkn)
              .send({ approvalid: orgApproval2.approvalid, approved: false })
              .expect(200)
              .then(function(res)
              {
                expect(res.body[ 0 ].cbid).to.equal(cbOne.cbid);
                expect(res.body[ 0 ].isfrozen).to.equal(false);
                expect(res.body[ 0 ].freezecause).to.equal(0);
                done();
              })
              .catch(done);
            });

            it("should successfully decline the request - 1st Approver Approved, 2nd Approver Declined", function(done)
            {
              var approvals = [];
              request(sails.hooks.http.app)
              .post("/orgapproval/initiatetransfer")
              .set("Authorization", firstOperator.accTkn)
              .send(validServiceTransferInitiateParams)
              .expect(200)
              .then(function(res)
              {
                approvals = res.body[ 0 ];
                expect(res.body[ 1 ][ 0 ]).to.have.property("serviceid").and.equal(validServiceTransferInitiateParams.entityid);
                expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(true);
                orgApproval1 = res.body[ 0 ][ 0 ];
                orgApproval2 = res.body[ 0 ][ 1 ];
                return request(sails.hooks.http.app)
                .post("/orgapproval/approve")
                .set("Authorization", firstCBOwner.accTkn)
                .send({ approvalid: orgApproval1.approvalid, approved: true });
              })
              .then(function(res)
              {
                expect(res.statusCode).to.equal(200);
                return request(sails.hooks.http.app)
                .post("/orgapproval/approve")
                .set("Authorization", thirdCBOwner.accTkn)
                .send({ approvalid: orgApproval2.approvalid, approved: false });
              })
              .then(function(res)
              {
                expect(res.body[ 0 ].cbid).to.equal(cbOne.cbid);
                expect(res.body[ 0 ].isfrozen).to.equal(false);
                expect(res.body[ 0 ].freezecause).to.equal(0);
                done();
              })
              .catch(done);
            });

            it("should successfully approve the request - Both approvers approved(receiver approving before doner)", function(done)
            {
              var approvals = [];
              request(sails.hooks.http.app)
              .post("/orgapproval/initiatetransfer")
              .set("Authorization", firstOperator.accTkn)
              .send(validServiceTransferInitiateParams)
              .expect(200)
              .then(function(res)
              {
                approvals = res.body[ 0 ];
                expect(res.body[ 1 ][ 0 ]).to.have.property("serviceid").and.equal(validServiceTransferInitiateParams.entityid);
                expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(true);
                orgApproval1 = res.body[ 0 ][ 0 ];
                orgApproval2 = res.body[ 0 ][ 1 ];
                return request(sails.hooks.http.app)
                .post("/orgapproval/approve")
                .set("Authorization", thirdCBOwner.accTkn)
                .send({ approvalid: orgApproval2.approvalid, approved: true });
              })
              .then(function(res)
              {
                expect(res.statusCode).to.equal(200);
                return request(sails.hooks.http.app)
                .post("/orgapproval/approve")
                .set("Authorization", firstCBOwner.accTkn)
                .send({ approvalid: orgApproval1.approvalid, approved: true });
              })
              .then(function(res)
              {
                expect(res.body[ 0 ].cbid).to.equal(cbThree.cbid);
                expect(res.body[ 0 ].isfrozen).to.equal(false);
                expect(res.body[ 0 ].freezecause).to.equal(0);
                return Service.update({ serviceid: validServiceTransferInitiateParams.entityid }, { cbid: cbOne.cbid });
              })
              .then(function(services)
              {
                expect(services[ 0 ]).to.have.property("cbid").and.equal(cbOne.cbid);
                done();
              })
              .catch(done);
            });
          });
        });
      });
    });

    describe("Associate a CI with service. Attempt #2", function()
    {

      it("should return error(CI is decommissioned) - 400387", function(done)
      {
        Cidetail.update({ ciid: cidetailOne.ciid }, { cistateid: 7 })
        .then(function(cis)
        {
          expect(cis[ 0 ]).to.exist;
          expect(cis[ 0 ]).to.have.property("cistateid").and.equal(7);
          return cis;
        })
        .then(function(cis)
        {
          return request(sails.hooks.http.app)
          .post("/service/assoc")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceAssocParams)
          .expect(400)
          .then(function(res)
          {
            expect(res.body).to.have.property("code").and.equal(400387);
            return Cidetail.update({ ciid: cidetailOne.ciid }, { cistateid: 1 });
          });
        })
        .then(function(cis)
        {
          expect(cis[ 0 ]).to.exist;
          expect(cis[ 0 ]).to.have.property("cistateid").and.equal(1);
          done();
        })
        .catch(done);
      });

      it("should return error(CI is frozen) - 400383", function(done)
      {
        Cidetail.update({ ciid: cidetailOne.ciid }, { isfrozen: true })
        .then(function(cis)
        {
          expect(cis[ 0 ]).to.exist;
          expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(true);
          return cis;
        })
        .then(function(cis)
        {
          return request(sails.hooks.http.app)
          .post("/service/assoc")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceAssocParams)
          .expect(400)
          .then(function(res)
          {
            expect(res.body).to.have.property("code").and.equal(400383);
            return Cidetail.update({ ciid: cidetailOne.ciid }, { isfrozen: false });
          });
        })
        .then(function(cis)
        {
          expect(cis[ 0 ]).to.exist;
          expect(cis[ 0 ]).to.have.property("isfrozen").and.equal(false);
          done();
        })
        .catch(done);
      });

      it("should initiate approvals", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceAssocParams)
        .expect(200)
        .then(function(res)
        {
          expect(res.body[ 0 ][ 0 ]).to.have.property("isapproved").and.equal(null);
          expect(res.body[ 0 ][ 0 ]).to.have.property("approverserviceid").and.equal(serviceOne.serviceid);
          expect(res.body[ 0 ][ 0 ]).to.have.property("ciid").and.equal(cidetailOne.ciid);
          expect(res.body[ 0 ][ 0 ]).to.have.property("type").and.equal("assoc");
          expect(res.body[ 0 ][ 0 ]).to.have.property("isactive").and.equal(true);
          serviceApprovalOne = res.body[ 0 ][ 0 ];
          return Cidetail.findOne({ ciid: validServiceAssocParams.ciid });
        })
        .then(function(ci)
        {
          expect(ci).to.exist;
          expect(ci).to.have.property("isfrozen").and.equal(true);
          done();
        })
        .catch(done);
      });

      describe("Approve a service to CI association request", function()
      {

        it("should return error(parameter is missing ciid) - 400051", function(done)
        {
          invalidCiServiceApproveParams_missingCiid.approvalid = serviceApprovalOne.approvalid;
          invalidCiServiceApproveParams_missingCiid.serviceid = serviceOne.serviceid;
          request(sails.hooks.http.app)
          .post("/service/approve")
          .set("Authorization", firstCBOwner.accTkn)
          .send(invalidCiServiceApproveParams_missingCiid)
          .expect(400)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body).to.have.property("code").and.equal(400051);
            done();
          });
        });

        it("should decline the association request", function(done)
        {
          validCiServiceApproveParams_decline.approvalid = serviceApprovalOne.approvalid;
          validCiServiceApproveParams_decline.serviceid = serviceOne.serviceid;
          validCiServiceApproveParams_decline.ciid = cidetailOne.ciid;
          request(sails.hooks.http.app)
          .post("/service/approve")
          .set("Authorization", firstCBOwner.accTkn)
          .send(validCiServiceApproveParams_decline)
          .expect(200)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res).to.exist;
            expect(res.body[ 0 ]).to.have.property("isfrozen").and.equal(false);
            done();
          });
        });

        it("should approve the request and associate", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/assoc")
          .set("Authorization", firstBDAdmin.accTkn)
          .send(validServiceAssocParams)
          .expect(200)
          .then(function(res)
          {
            expect(res.body[ 0 ][ 0 ]).to.have.property("isapproved").and.equal(null);
            expect(res.body[ 0 ][ 0 ]).to.have.property("approverserviceid").and.equal(serviceOne.serviceid);
            expect(res.body[ 0 ][ 0 ]).to.have.property("ciid").and.equal(cidetailOne.ciid);
            expect(res.body[ 0 ][ 0 ]).to.have.property("type").and.equal("assoc");
            expect(res.body[ 0 ][ 0 ]).to.have.property("isactive").and.equal(true);
            serviceApprovalOne = res.body[ 0 ][ 0 ];
            return validCiServiceApproveParams_approve;
          })
          .then(function(validCiServiceApproveParams_approve)
          {
            validCiServiceApproveParams_approve.approvalid = serviceApprovalOne.approvalid;
            validCiServiceApproveParams_approve.serviceid = serviceOne.serviceid;
            validCiServiceApproveParams_approve.ciid = serviceApprovalOne.ciid;
            return request(sails.hooks.http.app)
            .post("/service/approve")
            .set("Authorization", firstCBOwner.accTkn)
            .send(validCiServiceApproveParams_approve)
            .expect(200)
            .then(function(res)
            {
              expect(res.body).to.have.property("ciid").and.equal(validCiServiceApproveParams_approve.ciid);
              expect(res.body).to.have.property("isfrozen").and.equal(false);
              done();
            });
          })
          .catch(done);
        });
      });
    });

    describe("Associate a CI with currently associated services", function()
    {

      it("should return error(CI is associated with the listed services) - 400372", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/assoc")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(validServiceAssocParams)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body).to.have.property("code").and.equal(400372);
          done();
        });
      });
    });
  });

  describe("State Transfer of CI from Registration to Approved. Attempt# 2", function()
  {

    it("should successfully transfer the CI Registration->Approved", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailsNextParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("ciid").and.equal(cidetailOne.ciid);
        expect(res.body).to.have.property("cistateid").and.equal(2);
        expect(res.body).to.have.property("isfrozen").and.equal(false);
        done();
      });
    });
  });

  describe("State Transfer of CI to subsequent states", function()
  {

    it("should successfully transfer the CI Approved->Planned", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailsNextParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("ciid").and.equal(cidetailOne.ciid);
        expect(res.body).to.have.property("cistateid").and.equal(3);
        expect(res.body).to.have.property("isfrozen").and.equal(false);
        done();
      });
    });

    it("should successfully transfer the CI Planned->Installed", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailsNextParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("ciid").and.equal(cidetailOne.ciid);
        expect(res.body).to.have.property("cistateid").and.equal(4);
        expect(res.body).to.have.property("isfrozen").and.equal(false);
        done();
      });
    });
  });

  describe("Transfer the CI to Commissioned. Attempt# 2", function()
  {
    it("should successfully transfer the CI Installed->Commissioned", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cidetail/next")
      .set("Authorization", firstOperator.accTkn)
      .send(validCidetailsNextParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("ciid").and.equal(cidetailOne.ciid);
        expect(res.body).to.have.property("cistateid").and.equal(5);
        done();
      });
    });
  });

  describe("Deactivate the support queue of this service", function()
  {
    it("should not deactivate(queue has associated services) - 400267", function(done)
    {
      validCidetailQueueAssignParams.ciid = cidetailOne.ciid;
      validCidetailQueueAssignParams.supportqueue = queueOne.queueid;
      request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ queueids: [ validCidetailQueueAssignParams.supportqueue ], action: false })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400267);
        done();
      });
    });
  });

  describe("Dissociate the support queue of this service", function()
  {
    it("should not dissociate (service is not in archived phase) - 400378", function(done)
    {
      request(sails.hooks.http.app)
      .post("/service/queueassign")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ serviceid: serviceOne.serviceid, supportqueue: validCidetailQueueAssignParams.supportqueue, assign: false })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400378);
        done();
      });
    });
    describe("Service state transition from Approved to Archived", function()
    {
      it("should return error(service is associated with CIs) - 400364", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/next")
        .set("Authorization", firstBDAdmin.accTkn)
        .send({ serviceid: serviceOne.serviceid })
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.code).to.equal(400364);
          done();
        });
      });
      describe("Dissociate all CIs from the service", function()
      {
        it("should not dissociate(CI is not in reclaim state) - 400373", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/assoc")
          .set("Authorization", firstBDAdmin.accTkn)
          .send({ serviceids: [], ciid: cidetailOne.ciid })
          .expect(400)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body.code).to.equal(400373);
            done();
          });
        });
        describe("Move the CI to next states", function()
        {
          it("should move the CI to reclaim state", function(done)
          {
            request(sails.hooks.http.app)
            .post("/cidetail/next")
            .set("Authorization", firstOperator.accTkn)
            .send({ ciid: cidetailOne.ciid })
            .expect(200)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body.ciid).to.equal(cidetailOne.ciid);
              expect(res.body.cistateid).to.equal(enums.ciStateIds.RECLAIM);
              done();
            });
          });
          it("should not move the CI to decommissioned state(CI has services associated with it) - 400385", function(done)
          {
            request(sails.hooks.http.app)
            .post("/cidetail/next")
            .set("Authorization", firstOperator.accTkn)
            .send({ ciid: cidetailOne.ciid })
            .expect(400)
            .end(function(err, res)
            {
              if(err) return done(err);
              expect(res.body.code).to.equal(400385);
              done();
            });
          });
        });
      });
      describe("Dissociate all CIs from the service. Attempt #2", function()
      {
        it("should generate approvals", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/assoc")
          .set("Authorization", firstBDAdmin.accTkn)
          .send({ serviceids: [], ciid: cidetailOne.ciid })
          .expect(200)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body[ 0 ][ 0 ].ciid).to.equal(cidetailOne.ciid);
            expect(res.body[ 0 ][ 0 ].type).to.equal('assoc');
            serviceApprovalOne = {};
            serviceApprovalOne.ciid = res.body[ 0 ][ 0 ].ciid;
            serviceApprovalOne.type = res.body[ 0 ][ 0 ].type;
            serviceApprovalOne.approvalid = res.body[ 0 ][ 0 ].approvalid;
            serviceApprovalOne.serviceid = res.body[ 0 ][ 0 ].approverserviceid;
            serviceApprovalOne.isapproved = true;
            done();
          });
        });
        it("service:listapproval() - should list approvals pending approval from the requester", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/listapproval")
          .set("Authorization", firstCBOwner.accTkn)
          .expect(200)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body.length).to.be.above(0);
            done();
          });
        });
        it("should approve the request and dissociate the services from CI", function(done)
        {
          request(sails.hooks.http.app)
          .post("/service/approve")
          .set("Authorization", firstCBOwner.accTkn)
          .send(serviceApprovalOne)
          .expect(200)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body.ciid).to.equal(cidetailOne.ciid);
            expect(res.body.primaryservice).isNull;
            done();
          });
        });
      });
      describe("move the CI to decommissioned state", function()
      {
        it("should move the CI to decommissioned state", function(done)
        {
          request(sails.hooks.http.app)
          .post("/cidetail/next")
          .set("Authorization", firstOperator.accTkn)
          .send({ ciid: cidetailOne.ciid })
          .expect(200)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body.ciid).to.equal(cidetailOne.ciid);
            expect(res.body.cistateid).to.equal(enums.ciStateIds.DECOMMISSIONED);
            done();
          });
        });
        it("should return error(decommissioned is the last state) - 400384", function(done)
        {
          request(sails.hooks.http.app)
          .post("/cidetail/next")
          .set("Authorization", firstOperator.accTkn)
          .send({ ciid: cidetailOne.ciid })
          .expect(400)
          .end(function(err, res)
          {
            if(err) return done(err);
            expect(res.body.code).to.equal(400384);
            done();
          });
        });
      });
    });
    describe("State transfer of service to Archived. Attempt #2", function()
    {
      it("should be able to transfer", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/next")
        .set("Authorization", firstBDAdmin.accTkn)
        .send({ serviceid: serviceOne.serviceid })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body.servicestateid).to.equal(enums.serviceStateIds.ARCHIVED);
          done();
        });
      });
    });
  });
  describe("Dissociate the support queue of this service. Attempt #2", function()
  {
    it("should be able to dissociate", function(done)
    {
      request(sails.hooks.http.app)
      .post("/service/queueassign")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ serviceid: serviceOne.serviceid, supportqueue: validCidetailQueueAssignParams.supportqueue, assign: false })
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.serviceid).to.equal(serviceOne.serviceid);
        done();
      });
    });
  });
  describe("Deactivate the support queue of this service. Attempt #2", function()
  {
    it("should deactivate the queue successfully", function(done)
    {
      validCidetailQueueAssignParams.supportqueue = queueOne.queueid;
      request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ queueids: [ validCidetailQueueAssignParams.supportqueue ], action: false })
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ].queueid).to.equal(validCidetailQueueAssignParams.supportqueue);
        expect(res.body[ 0 ].isactive).to.equal(false);
        done();
      });
    });
  });
  describe("Attempt to transition service from Archived", function()
  {
    it("should return error(archived is the last state) - 400365", function(done)
    {
      request(sails.hooks.http.app)
      .post("/service/next")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ serviceid: serviceOne.serviceid })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400365);
        done();
      });
    });
  });
});
