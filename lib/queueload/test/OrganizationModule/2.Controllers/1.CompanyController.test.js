/* FileName: 2.CompanyController.test.js
* Description: This file defines all the unit test cases for the Company Controller.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 25/11/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;

describe("Company Controller", function()
{
  var company =
  {
    "accountid": "qlodtest",
    "name": "Wayne Enterprises",
    "address": "Gotham City",
    "accntadmin": 1,
    "globalconfig": {}
  };

  var superUser =
  {
    "firstname": "Super",
    "lastname": "User",
    "fullname": "Super User",
    "username": "administrator",
    "password": "password",
    "emailid": "administrator@testdomain.com"
  };

  var additionalSuperUser =
  {
    "firstname": "addSuper",
    "lastname": "User",
    "username": "administrator2",
    "fullname": "Super User Two",
    "password": "password",
    "emailid": "administrator2@testdomain.com"
  };

  var firstUser =
  {
    "firstname": "companyfirst",
    "lastname": "companyUser",
    "fullname": "company firstuser",
    "username": "companyfirst.User",
    "password": "companypassword",
    "emailid": "companyfirst.user@testdomain.com"
  };
  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.userid = res.body.user.userid;
        superUser.acctoken = res.body.acctoken;
        superUser.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.acctoken)
        .send(firstUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add firstUser");
      }
      else
      {
        firstUser.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldnt signin firstUser");
      }
      else
      {
        firstUser.acctoken = res.body.acctoken;
        firstUser.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.acctoken)
        .send(additionalSuperUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add additionalSuperUser");
      }
      else
      {
        additionalSuperUser.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/makesuperuser')
        .set('Authorization', superUser.acctoken)
        .send({ userid: additionalSuperUser.userid, action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't make additionalSuperUser super user");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(additionalSuperUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldnt signin additionalSuperUser");
      }
      else
      {
        additionalSuperUser.acctoken = res.body.acctoken;
        additionalSuperUser.reftoken = res.body.reftoken;
        done();
      }
    })
    .catch(done);
  });
  describe("#update()", function()
  {
    it("should not update the company(must be a superUser) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/update')
      .set('Authorization', firstUser.acctoken)
      .send({ name: "doesn't matter" })
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(403502);
        done();
      });
    });
    it("should not update the company(attempt to change the active status) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/update')
      .set('Authorization', superUser.acctoken)
      .send({ isactive: false })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not update the company(attempt to change the active status) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/update')
      .set('Authorization', superUser.acctoken)
      .send({ isactive: false })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not update the company(attempt to assign a non-existent user as account admin) - 400104", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/update')
      .set('Authorization', superUser.acctoken)
      .send({ accntadmin: 123456 })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400104);
        done();
      });
    });
    it("should not update the company(attempt to assign an inactive user as account admin) - 400103", function(done)
    {
      User.update({ userid: additionalSuperUser.userid }, { isactive: false })
      .then(function(user)
      {
        expect(user[ 0 ].isactive).to.equal(false);
        return request(sails.hooks.http.app)
        .post('/company/update')
        .set('Authorization', superUser.acctoken)
        .send({ accntadmin: additionalSuperUser.userid });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400103);
        return User.update({ userid: additionalSuperUser.userid }, { isactive: true });
      })
      .then(function(user)
      {
        expect(user[ 0 ].isactive).to.equal(true);
        done();
      })
      .catch(done);
    });
    it("should not update the company(attempt to assign an non-superuser as account admin) - 400522", function(done)
    {
      Role.update({ userid: additionalSuperUser.userid }, { issuperuser: false })
      .then(function(role)
      {
        expect(role[ 0 ].issuperuser).to.equal(false);
        return request(sails.hooks.http.app)
        .post('/company/update')
        .set('Authorization', superUser.acctoken)
        .send({ accntadmin: additionalSuperUser.userid });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400522);
        return Role.update({ userid: additionalSuperUser.userid }, { issuperuser: true });
      })
      .then(function(role)
      {
        expect(role[ 0 ].issuperuser).to.equal(true);
        done();
      })
      .catch(done);
    });
    it("should update the company", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/update')
      .set('Authorization', superUser.acctoken)
      .send({ name: "Stark Industries", address: "New York", accntadmin: additionalSuperUser.userid })
      .expect(200)
      .then(function(res)
      {
        expect(res.body.accntadmin).to.equal(additionalSuperUser.userid);
        return request(sails.hooks.http.app)
        .post('/company/update')
        .set('Authorization', superUser.acctoken)
        .send({ name: company.name, address: company.address, accntadmin: superUser.userid });
      })
      .then(function(res)
      {
        expect(res.body.accntadmin).to.equal(superUser.userid);
        done();
      })
      .catch(done);
    });
  });
});
