/* FileName: BdController.test.js
* Description: This file defines all the unit test cases for the BD controller.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 19/04/2016          Initial file creation                           bones
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com"
};

var company =
{
  "accountid": "qlodtest",
  "name": "Wayne Enterprises",
  "address": "Gotham City",
  "accntadmin": 1,
  "globalconfig": {}
};

var firstBDAdmin =
{
  "firstname": "bdfirstAdmin",
  "lastname": "bdBDOne",
  "fullname":"bdfirstAdmin bdBDOne",
  "username": "bdfirst.bdoneadmin",
  "password": "password",
  "emailid": "bdfirst.bd1admin@testdomain.com"
};

var userNormal =
{
  "firstname": "bdnormal",
  "lastname": "bduser",
  "fullname":"bdnormal bduser",
  "username": "bdnormal.user",
  "password": "password",
  "emailid": "bdnormal.user@testdomain.com"
};

var secondBDAdmin =
{
  "firstname": "bdsecondAdmin",
  "lastname": "bdBDOne",
  "fullname":"bdsecondAdmin bdBDOne",
  "username": "bdsecond.bdoneadmin",
  "password": "password",
  "emailid": "bdsecond.bd1admin@testdomain.com"
};

var thirdBDAdmin =
{
  "firstname": "bdthirdAdmin",
  "lastname": "bdBDOne",
  "fullname":"bdthirdAdmin bdBDOne",
  "username": "bdthird.bdoneadmin",
  "password": "password",
  "emailid": "bdthird.bd1admin@testdomain.com"
};

var userNormal1 =
{
  "firstname": "bdnormalOne",
  "lastname": "bduser",
  "fullname":"bdnormalOne bduser",
  "username": "bdnormal1.user",
  "password": "password",
  "emailid": "bdnormal1.user@testdomain.com"
};

var fourthBDAdmin =
{
  "firstname": "bdfourthAdmin",
  "lastname": "bdBDOne",
  "fullname":"bdfourthAdmin bdBDOne",
  "username": "bdfourth.bdoneadmin",
  "password": "password",
  "emailid": "bdfourth.bd1admin@testdomain.com"
};

var firstActiveBD =
{
  "bdname": "BdBusiness-Department.2",
  "isactive": true
};

var secondActiveBD =
{
  "bdname": "BdBusiness-Department.3",
  "isactive": true
};

var thirdActiveBD =
{
  "bdname": "BdBusiness-Department.4"
};

var fourthActiveBD =
{
  "bdname": "BdBusiness-Department.5"
};

var user_CP_deactivate =
{
  "action": false
};

var assignBD_CP =
{
  "bdid": 2,
  "admins": [ 2 ],
  "assign": true
};

var unassignBD_CP =
{
  "bdid": 2,
  "admins": [ 2 ],
  "assign": false
};

var activateBD_CP =
{
  "action": true
};

var firstActiveCb;

describe("BD Controller", function()
{
  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      var id = user.userid;
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: id }, { userid: id, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.accTkn = res.body.acctoken;
        superUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st BDAdmin");
      }
      else
      {
        firstBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(userNormal);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st normal User");
      }
      else
      {
        userNormal.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(userNormal1);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd normal User");
      }
      else
      {
        userNormal1.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd BdAdmin");
      }
      else
      {
        secondBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 4th BdAdmin");
      }
      else
      {
        fourthBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd BDAdmin");
      }
      else
      {
        thirdBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(firstActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstActiveBD");
      }
      else
      {
        firstActiveBD.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(fourthActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create fourthActiveBD");
      }
      else
      {
        fourthActiveBD.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(secondActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add secondActiveBD");
      }
      else
      {
        secondActiveBD.bdid = res.body.bdid;
        assignBD_CP.bdid = firstActiveBD.bdid;
        assignBD_CP.admins = [ firstBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign firstBDAdmin to firstActiveBD");
      }
      else
      {
        firstActiveBD.admins = [ firstBDAdmin.userid ];
        activateBD_CP.bdids = [ firstActiveBD.bdid ];
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.accTkn)
        .send(activateBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate firstActiveBD");
      }
      else
      {
        firstActiveBD.isactive = true;
        assignBD_CP.bdid = fourthActiveBD.bdid;
        assignBD_CP.admins = [ fourthBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign fourthBDAdmin to fourthActiveBD");
      }
      else
      {
        fourthActiveBD.admins = [ fourthBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin firstBDAdmin");
      }
      else
      {
        fourthBDAdmin.accTkn = res.body.acctoken;
        activateBD_CP.bdids = [ fourthActiveBD.bdid ];
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.accTkn)
        .send(activateBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate fourthActiveBD");
      }
      else
      {
        fourthActiveBD.isactive = true;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ bdid: fourthActiveBD.bdid, ownerid: fourthBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        console.log(res.body);
        throw new Error("Couldn't create 1st CB for BD fourthActiveBD");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate the CB firstActiveCb");
      }
      else
      {
        firstActiveCb = res.body[ 0 ];
        done();
      }
    })
    .catch(done);
  });

  describe("#list()", function()
  {
    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(empty parameter list) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/list")
      .set("Authorization", firstBDAdmin.accTkn)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/list")
      .send({ "page": 1, "limit": "a" })
      .set("Authorization", firstBDAdmin.accTkn)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should list all bds in the system", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/list")
      .send({ "page": 1, "limit": 10 })
      .set("Authorization", firstBDAdmin.accTkn)
      .expect(200);
    });
  });

  describe("#add()", function()
  {
    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    var inActiveBD_WP_array =
    {
      "bdname": [ "Business-Department.5", "Business-Department.7", "Business-Department.6" ]
    };

    var inActiveBD_CP =
    {
      "bdname": "BDBusiness-Department.1"
    };

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(inActiveBD_WP_array)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(Array parameters) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/add")
      .set("Authorization", superUser.accTkn)
      .send(inActiveBD_WP_array)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should add a bd to the system", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/add")
      .set("Authorization", superUser.accTkn)
      .send(inActiveBD_CP)
      .then(function(res)
      {
        expect(res.body).to.have.property("isactive").and.equal(false);
      });
    });

    it("should add a bd to the system(isactive parameter is ignored)", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/add")
      .set("Authorization", superUser.accTkn)
      .send(thirdActiveBD)
      .then(function(res)
      {
        expect(res.body).to.have.property("isactive").and.equal(false);
        thirdActiveBD.bdid = res.body.bdid;
      });
    });
  });

  describe("#assign()", function()
  {
    var bd_WP_array =
    {
      "bdid": [ 1, 2, 3, 4 ],
      "admins": [ 10 ],
      "assign": false
    };

    var bd_WP_adminArray =
    {
      "bdid": 2,
      "admins": 10,
      "assign": false
    };

    var bd_WP_NAN =
    {
      "bdid": 2,
      "admins": [ 10, 3, 5, "g" ],
      "assign": false
    };

    var assignBD_WP =
    {
      "bdid": 34,
      "admins": [ 4 ],
      "assign": true
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          user_CP_deactivate.userids = [ userNormal.userid ];
          return request(sails.hooks.http.app)
          .post("/user/activation")
          .set('Authorization', superUser.accTkn)
          .send(user_CP_deactivate);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't make the normal user as inactive");
        }
        else
        {
          userNormal.isactive = false;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(userNormal1);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin normal user1");
        }
        else
        {
          userNormal1.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", userNormal1.accTkn)
      .send(bd_WP_array)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(Not Authorized - BD Admin of another BD) - 403501", function()
    {
      assignBD_WP.bdid = firstActiveBD.bdid;
      assignBD_WP.assign = true;
      assignBD_WP.admins = [ fourthBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", fourthBDAdmin.accTkn)
      .send(assignBD_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403501);
      });
    });

    it("should return an error(incorrect parameters - bdid Array) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP_array)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - admin Array) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP_adminArray)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - NAN) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP_NAN)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(action field should be a boolean) - 400051", function()
    {
      assignBD_WP.bdid = firstActiveBD.bdid;
      assignBD_WP.assign = "true";
      request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("action:true should return error(BD does not exist) - 400154", function()
    {
      assignBD_WP.bdid = 1086;
      assignBD_WP.assign = true;
      assignBD_WP.admins = [ superUser.userid, firstBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400154);
      });
    });

    it("action:true should return error(user does not exist) - 400104", function()
    {
      assignBD_WP.bdid = firstActiveBD.bdid;
      assignBD_WP.admins = [ superUser.userid, 1078 ];
      assignBD_WP.assign = true;
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400104);
      });
    });

    it("action:true should return error(user is not active) - 400103", function()
    {
      assignBD_WP.bdid = firstActiveBD.bdid;
      assignBD_WP.admins = [ userNormal.userid ];
      assignBD_WP.assign = true;

      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400103);
      });
    });

    it("action:true should return error(user already admin) - 400156", function()
    {
      assignBD_WP.bdid = firstActiveBD.bdid;
      assignBD_WP.admins = [ superUser.userid, firstBDAdmin.userid ];
      assignBD_WP.assign = true;
      request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400156);
      });
    });

    it("action:true should assign users to an active BD", function()
    {
      assignBD_CP.bdid = firstActiveBD.bdid;
      assignBD_CP.admins = [ secondBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_CP)
      .expect(200);
    });

    it("action:true should assign users to an inactive BD", function()
    {
      assignBD_CP.bdid = secondActiveBD.bdid;
      assignBD_CP.admins = [ thirdBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(assignBD_CP)
      .expect(200);
    });

    it("action:false should return error(admin does not exist) - 400157", function()
    {
      unassignBD_CP.bdid = firstActiveBD.bdid;
      unassignBD_CP.admins = [ thirdBDAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(unassignBD_CP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400157);
      });
    });

    it("action:false should unassign admins of an active bd", function()
    {
      unassignBD_CP.bdid = firstActiveBD.bdid;
      unassignBD_CP.admins = [ secondBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(unassignBD_CP)
      .expect(200);
    });

    it("action:false should return error(last active admin of an active bd) - 400158", function()
    {
      unassignBD_CP.bdid = firstActiveBD.bdid;
      unassignBD_CP.admins = [ firstBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(unassignBD_CP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400158);
      });
    });

    it("action:false should unassign all admins(last active admin of an inactive bd)", function()
    {
      unassignBD_CP.bdid = secondActiveBD.bdid;
      unassignBD_CP.admins = [ thirdBDAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", superUser.accTkn)
      .send(unassignBD_CP)
      .expect(200);
    });
  });

  describe("#activation()", function()
  {
    var bd_WP =
    {
      "bdids": 1118
    };

    var bd_CP =
    {
      "bdids": 1
    };

    before(function(done)
    {
      user_CP_deactivate.userids = [ thirdBDAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', superUser.accTkn)
      .send(user_CP_deactivate)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't make the thirdBDAdmin as inactive");
        }
        else
        {
          thirdBDAdmin.isactive = false;
          done();
        }
      })
      .catch(done);
    });

    it("should return an error(incorrect parameters - missing action field) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      bd_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - NAB) - 400051", function()
    {
      bd_WP.action = "true";
      bd_WP.bdids = [ 1118 ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - NAN) - 400051", function()
    {
      bd_WP.action = true;
      bd_WP.bdids = [ 1118, "a" ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - non existent bdid multiple) - 400154", function()
    {
      bd_WP.action = true;
      bd_WP.bdids = [ 1118 ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400154);
      });
    });

    it("action:true should return an error(all bdids are not inactive) - 400152", function()
    {
      bd_WP.action = true;
      bd_WP.bdids = [ secondActiveBD.bdid, thirdActiveBD.bdid, firstActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400152);
      });
    });

    it("action:true should return an error(no admins active) - 400158", function()
    {
      bd_WP.action = true;
      bd_WP.bdids = [ secondActiveBD.bdid, thirdActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400158);
      });
    });

    it("action:false should return an error(all bdids are not active) - 400153", function()
    {
      bd_WP.action = false;
      bd_WP.bdids = [ firstActiveBD.bdid, secondActiveBD.bdid, thirdActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400153);
      });
    });

    it("action:false should return an error(all cbids are not inactive) - 400159", function()
    {
      bd_WP.action = false;
      bd_WP.bdids = [ firstActiveBD.bdid, fourthActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400159);
      });
    });

    it("action:false should make the BD inactive", function()
    {
      bd_CP.action = false;
      bd_CP.bdids = [ firstActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
      });
    });

    it("action:true should make the BD active", function()
    {
      bd_CP.action = true;
      bd_CP.bdids = [ firstActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", superUser.accTkn)
      .send(bd_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
      });
    });
  });

  describe("#getdetail()", function()
  {
    var bd_WP =
    {
      "bdids": 1118
    };

    var bd_CP =
    {
      "bdids": 1
    };

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/getdetail")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - incorrect field value) - 400051", function()
    {
      bd_WP.bdids = [ 1, 2, 3, "salt" ];
      return request(sails.hooks.http.app)
      .post("/bd/getdetail")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - empty Array) - 400051", function()
    {
      bd_WP.bdids = [];
      return request(sails.hooks.http.app)
      .post("/bd/getdetail")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(non existent bdid) - 400154", function()
    {
      bd_WP.bdids = [ firstActiveBD.bdid, secondActiveBD.bdid,  1118 ];
      return request(sails.hooks.http.app)
      .post("/bd/getdetail")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400154);
      });
    });

    it("should get bd details multiple", function()
    {
      bd_CP.bdids = [ firstActiveBD.bdid, secondActiveBD.bdid, thirdActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/getdetail")
      .set("Authorization", superUser.accTkn)
      .send(bd_CP)
      .then(function(res)
      {
        expect(res.body).to.have.lengthOf(3);
      });
    });
  });

  describe("#getstatus()", function()
  {
    var bd_WP =
    {
      "bdids": 1118
    };

    var bd_CP =
    {
      "bdids": 1
    };

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/getstatus")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - empty Array) - 400051", function()
    {
      bd_WP.bdids = [];
      return request(sails.hooks.http.app)
      .post("/bd/getstatus")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - incorrect field value) - 400051", function()
    {
      bd_WP.bdids = [ 1, 2, 3, "salt" ];
      return request(sails.hooks.http.app)
      .post("/bd/getstatus")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(non existent bdid) - 400154", function()
    {
      bd_WP.bdids = [ firstActiveBD.bdid, secondActiveBD.bdid,  1118 ];
      return request(sails.hooks.http.app)
      .post("/bd/getstatus")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400154);
      });
    });

    it("should get bd details multiple", function()
    {
      bd_CP.bdids = [ firstActiveBD.bdid, secondActiveBD.bdid, thirdActiveBD.bdid ];
      return request(sails.hooks.http.app)
      .post("/bd/getstatus")
      .set("Authorization", superUser.accTkn)
      .send(bd_CP)
      .then(function(res)
      {
        expect(res.body).to.have.lengthOf(3);
      });
    });
  });

  describe("#search()", function()
  {
    var bd_WP_1 =
    {
      "searched":  { "username": "T3st.User1%" },
      "result": [ "firstname", "lastname", "username", "emailid" ],
      "orderby": "firstname"
    };

    var bd_WP_2 =
    {
      "search":  { "bdname": "B%" },
      "result": [ "bdid", "bdname", "isactive" ],
      "orderby": "bdid"
    };

    var bd_CP =
    {
      "search":  { "bdname": "B%" },
      "result": [ "bdid", "bdname", "isactive" ],
      "orderby": "bdid"
    };

    it("should return an error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/search")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP_1)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Non ExistentColumn) - 400053", function()
    {
      bd_WP_2.search = { "usersname": "T3st.User1%" };
      return request(sails.hooks.http.app)
      .post("/bd/search")
      .set("Authorization", superUser.accTkn)
      .send(bd_WP_2)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400053);
      });
    });

    it("should search for the Business-Departments", function()
    {
      return request(sails.hooks.http.app)
      .post("/bd/search")
      .set("Authorization", superUser.accTkn)
      .send(bd_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.be.above(0);
        expect(Object.keys(res.body[ 1 ][ 0 ]).length).to.be.above(1);
      });
    });
  });
});
