/* FileName: QueueController.test.js
* Description: This file defines all the unit test cases for the Queue controller.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 26/05/2016          Initial file creation                           bones
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var enums = require("../../../api/resources/Enums.js");

describe("Queue Controller", function()
{
  var superUser =
  {
    "firstname": "Super",
    "lastname": "User",
    "fullname":"Super User",
    "username": "administrator",
    "password": "password",
    "emailid": "administrator@testdomain.com"
  };

  var company =
  {
    "accountid": "qlodtest",
    "name": "Wayne Enterprises",
    "address": "Gotham City",
    "accntadmin": 1,
    "globalconfig": {}
  };

  var firstBDAdmin =
  {
    "firstname": "cbfirstAdmin",
    "lastname": "qBDOne",
    "fullname":"cbfirstAdmin qBDOne",
    "username": "qfirst.bdoneadmin",
    "password": "password",
    "emailid": "qfirst.bd1admin@testdomain.com"
  };

  var firstOperator =
  {
    "firstname": "qsecondAdmin",
    "lastname": "qBDOne",
    "fullname":"qsecondAdmin qBDOne",
    "username": "qsecond.bdoneadmin",
    "password": "password",
    "emailid": "qsecond.bd1admin@testdomain.com"
  };

  var secondOperator =
  {
    "firstname": "qthirdAdmin",
    "lastname": "qBDOne",
    "fullname":"qthirdAdmin qBDOne",
    "username": "qthird.bdoneadmin",
    "password": "password",
    "emailid": "qthird.bd1admin@testdomain.com"
  };

  var fourthOperator =
  {
    "firstname": "qnormalOne",
    "lastname": "quser",
    "fullname":"qnormalOne quser",
    "username": "qnormal1.user",
    "password": "password",
    "emailid": "qnormal1.user@testdomain.com"
  };

  var thirdOperator =
  {
    "firstname": "qfourthAdmin",
    "lastname": "qBDOne",
    "fullname":"qfourthAdmin qBDOne",
    "username": "qfourth.bdoneadmin",
    "password": "password",
    "emailid": "qfourth.bd1admin@testdomain.com"
  };

  var userNormal =
  {
    "firstname": "qnormal",
    "lastname": "quser",
    "fullname":"qnormal quser",
    "username": "qnormal.user",
    "password": "password",
    "emailid": "qnormal.user@testdomain.com"
  };

  var firstInactiveUser =
  {
    "firstname": "qfirst",
    "lastname": "qlast",
    "fullname":"qfirst qlast",
    "username": "qfirst.last",
    "password": "password",
    "emailid": "qfirst.last@testdomain.com"
  };

  var firstActiveBD =
  {
    "bdname": "qBusiness-Department.2",
    "isactive": true
  };

  var assignBD_CP =
  {
    "bdid": 2,
    "admins": [ 2 ],
    "assign": true
  };

  var activateBD_CP =
  {
    "action": true
  };

  var user_CP_deactivate =
  {
    "action": false
  };

  var firstActiveQueue =
  {
    "queuename":"qTestQueue1",
    "queueemail":"qtestqueue1@testdomain.com"
  };

  var firstInactiveQueue =
  {
    "queuename":"qTestQueue2",
    "queueemail":"qtestqueue2@testdomain.com"
  };

  var secondInactiveQueue =
  {
    "queuename":"qTestQueue3",
    "queueemail":"qtestqueue3@testdomain.com"
  };

  var firstActiveCb, secondActiveCb;
  var firstActiveService;
  var firstInactiveCb;
  var firstFrozenCb;
  var secondActiveQueue;

  var firstFrozenQueue;

  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      var id = user.userid;
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: id }, { userid: id, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.accTkn = res.body.acctoken;
        superUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st BDAdmin");
      }
      else
      {
        firstBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstOperator");
      }
      else
      {
        firstOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create secondOperator");
      }
      else
      {
        secondOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create thirdOperator");
      }
      else
      {
        thirdOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fourthOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create fourthOperator");
      }
      else
      {
        fourthOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(userNormal);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create userNormal");
      }
      else
      {
        userNormal.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(firstActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstActiveBD");
      }
      else
      {
        firstActiveBD.bdid = res.body.bdid;
        assignBD_CP.bdid = firstActiveBD.bdid;
        assignBD_CP.admins = [ firstBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign firstBDAdmin to firstActiveBD");
      }
      else
      {
        firstActiveBD.admins = [ firstBDAdmin.userid ];
        activateBD_CP.bdids = [ firstActiveBD.bdid ];
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.accTkn)
        .send(activateBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate firstActiveBD");
      }
      else
      {
        firstActiveBD.isactive = true;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st BDAdmin");
      }
      else
      {
        firstBDAdmin.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: firstActiveBD.bdid, ownerid: firstBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstActiveCb for BD firstActiveBD");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the CB firstActiveCb");
      }
      else
      {
        firstActiveCb = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: firstActiveBD.bdid, ownerid: firstBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create secondActiveCb for BD firstActiveBD");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the CB firstActiveCb");
      }
      else
      {
        secondActiveCb = res.body[ 0 ];
        firstActiveQueue.cbid = firstActiveCb.cbid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send(firstActiveQueue);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add firstActiveQueue");
      }
      else
      {
        var orgApproval = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 1");
      }
      else
      {
        firstActiveQueue = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueid: firstActiveQueue.queueid, members: [ firstOperator.userid, firstBDAdmin.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue firstActiveQueue");
      }
      else
      {
        firstActiveQueue.members = [ firstOperator.userid, firstBDAdmin.accTkn ];
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't sign in firstBDAdmin");
      }
      else
      {
        firstBDAdmin.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueids: [ firstActiveQueue.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 1");
      }
      else
      {
        firstActiveQueue.isactive = true;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: firstActiveBD.bdid, ownerid: firstBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstInactiveCb for BD firstActiveBD");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the CB firstInactiveCb");
      }
      else
      {
        firstInactiveCb = res.body[ 0 ];
        secondInactiveQueue.cbid = firstInactiveCb.cbid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send(secondInactiveQueue);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add secondInactiveQueue");
      }
      else
      {
        var orgApproval = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 3");
      }
      else
      {
        secondInactiveQueue = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/cb/activation')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ cbids:[ firstInactiveCb.cbid ], action: false });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't deactivate firstInactiveCb");
      }
      else
      {
        firstInactiveCb.isactive = false;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ bdid: firstActiveBD.bdid, ownerid: firstBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstFrozenCb for BD firstActiveBD");
      }
      else
      {
        firstFrozenCb = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstInactiveUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstInactiveUser");
      }
      else
      {
        firstInactiveUser.userid = res.body.userid;
        firstInactiveQueue.cbid = firstActiveCb.cbid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', firstBDAdmin.accTkn)
        .send(firstInactiveQueue);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add firstInactiveQueue");
      }
      else
      {
        var orgApproval = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 2");
      }
      else
      {
        firstInactiveQueue = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueid: firstInactiveQueue.queueid, members: [ firstInactiveUser.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue firstInactiveQueue");
      }
      else
      {
        firstActiveQueue.members = [ firstInactiveUser.userid, firstBDAdmin.accTkn ];
        user_CP_deactivate.userids = [ firstInactiveUser.userid ];
        return request(sails.hooks.http.app)
        .post("/user/activation")
        .set('Authorization', superUser.accTkn)
        .send(user_CP_deactivate);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't deactivate firstInactiveUser");
      }
      else
      {
        firstInactiveUser.isactive = false;
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', firstBDAdmin.accTkn)
        .send({ queueid: secondInactiveQueue.queueid, members: [ secondOperator.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue secondInactiveQueue");
      }
      else
      {
        secondInactiveQueue.members = [ secondOperator.userid ];
        done();
      }
    })
    .catch(done);
  });

  describe("#list()", function()
  {
    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(empty parameter list) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/list")
      .set("Authorization", firstBDAdmin.accTkn)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/list")
      .send({ "page": 1, "limit": "a" })
      .set("Authorization", firstBDAdmin.accTkn)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should list all queues in the system", function(done)
    {
      request(sails.hooks.http.app)
      .post("/queue/list")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ "page": 1, "limit": 100 })
      .expect(200, done);
    });
  });

  describe("#add()", function()
  {
    var queue_WP =
    {
      "queuename":"TestQueue14",
      "queueemail":"testqueue14@test.com"
    };

    var queue_CP =
    {
      "queuename":"qTestQueue-71",
      "queueemail":"qtestqueue71@testdomain.com"
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", superUser.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(incorrect parameters - nonExistentFields) - 400051", function()
    {
      queue_WP.cbids = 2;
      queue_WP.ownerids = 1;
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - missing mandatory fields) - 400051", function()
    {
      delete queue_WP.cbids;
      delete queue_WP.ownerids;
      delete queue_WP.queuename;
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters -array fields) - 400051", function()
    {
      queue_WP.cbid = [ firstActiveCb.cbid, 4 ];
      queue_WP.queuename = "QueueTestQ";
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(Non-existent CB) - 400204", function()
    {
      queue_WP.cbid = 1118;
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400204);
      });
    });

    it("should return error(InActive CB) - 400203", function()
    {
      queue_WP.cbid = firstInactiveCb.cbid;
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400203);
      });
    });

    it("should return error(Frozen CB) - 400208", function()
    {
      queue_WP.cbid = firstFrozenCb.cbid;
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400203);
      });
    });

    var approve_CP = {};
    it("should add queue to the system - Post Approval by CB code owner", function()
    {
      queue_CP.cbid = firstActiveCb.cbid;

      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 1 ]).to.have.property("typeofapproval").and.equal(enums.approval.ASSIGN_Q2CB);
        expect(res.body[ 1 ]).to.have.property("targetentityid").and.equal(firstActiveCb.cbid);
        expect(res.body[ 1 ]).to.have.property("entityid").and.equal(res.body[ 0 ].queueid);
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
        expect(res.body[ 0 ]).to.have.property("isfrozen").and.equal(true);
        expect(res.body[ 0 ]).to.have.property("cbid").and.equal(null);
        expect(res.body[ 0 ]).to.have.property("queuename").and.equal(queue_CP.queuename);
        approve_CP.approvalid = res.body[ 1 ].approvalid;
        approve_CP.approverid = res.body[ 1 ].approverid;
        approve_CP.approved = true;

        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP);
      })
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
        expect(res.body[ 0 ]).to.have.property("isfrozen").and.equal(false);
        expect(res.body[ 0 ]).to.have.property("cbid").and.equal(firstActiveCb.cbid);
        secondActiveQueue = res.body[ 0 ];
      });
    });

    it("should not add queue to the system - Post rejection by CB code owner", function()
    {
      queue_CP.queuename = "qTestQueue-78";
      return request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 1 ]).to.have.property("typeofapproval").and.equal(enums.approval.ASSIGN_Q2CB);
        expect(res.body[ 1 ]).to.have.property("targetentityid").and.equal(firstActiveCb.cbid);
        expect(res.body[ 1 ]).to.have.property("entityid").and.equal(res.body[ 0 ].queueid);
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
        expect(res.body[ 0 ]).to.have.property("isfrozen").and.equal(true);
        expect(res.body[ 0 ]).to.have.property("cbid").and.equal(null);
        expect(res.body[ 0 ]).to.have.property("queuename").and.equal(queue_CP.queuename);
        approve_CP.approvalid = res.body[ 1 ].approvalid;
        approve_CP.approverid = res.body[ 1 ].approverid;
        approve_CP.approved = false;
        firstFrozenQueue = res.body[ 0 ];

        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP);
      })
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("cbid").and.equal(null);
      });
    });
  });

  describe("#update()", function()
  {
    var queue_WP =
    {
      "queuenames":"TestQueue69",
      "cbid": 1,
      "queueemail":"testqueue69@test.com"
    };

    var queue_WP_NonUpdateFields =
    {
      "queueid": 5,
      "queuename":"TestQueue69",
      "cbid": 1,
      "isactive": false,
      "isfrozen": true
    };

    var queue_CP =
    {
      "queueemail": "updatedqueue@test.com"
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", superUser.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(incorrect parameters - Non-Existent Fields) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Wrong Value) - 400051", function()
    {
      delete queue_WP.queuenames;
      queue_WP.queueid = 'a';
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Array Fields) - 400051", function()
    {
      queue_WP.queueid = [ 1, 2, 3 ];
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Non-Existent Queue) - 400254", function()
    {
      queue_WP.queueid = 1118;
      queue_WP.cbid = firstActiveCb.cbid;
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400254);
      });
    });

    it("should return error(incorrect parameters - Frozen Queue) - 400264", function()
    {
      queue_WP.queueid = firstFrozenQueue.queueid;
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400264);
      });
    });

    it("should return error(incorrect parameters - No Valid Fields for Update) - 400052", function()
    {
      queue_WP_NonUpdateFields.cbid = firstActiveCb.cbid;
      queue_WP_NonUpdateFields.queueid = firstActiveQueue.queueid;
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP_NonUpdateFields)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400052);
      });
    });

    it("should update Queue correctly", function()
    {
      queue_CP.queueid = firstActiveQueue.queueid;
      return request(sails.hooks.http.app)
      .post("/queue/update")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("queueid").and.equal(firstActiveQueue.queueid);
        expect(res.body[ 0 ]).to.have.property("queueemail").and.equal("updatedqueue@test.com");
      });
    });
  });

  describe("#activation()", function()
  {
    var queue_WP =
    {
      "queue_ids": [ 6, 3 ],
      "action": true
    };

    var queue_CP =
    {
      "queueids": [ 3, 5 ],
      "action": false
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", superUser.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Wrong Value - NAN) - 400051", function()
    {
      delete queue_WP.queue_ids;
      queue_WP.queueids = [ 6, 3, 'a' ];
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Wrong Value - NAB) - 400051", function()
    {
      queue_WP.queueids = [ 6, 3 ];
      queue_WP.action = "true";
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Non Existent Queue) - 400254", function()
    {
      queue_WP.queueids = [ 1118 ];
      queue_WP.action = false;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400254);
      });
    });

    it("should return error(incorrect parameters - Frozen Queue) - 400264", function()
    {
      queue_WP.queueids = [ firstActiveQueue.queueid, firstFrozenQueue.queueid ];
      queue_WP.action = false;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400264);
      });
    });

    it("should return error(incorrect parameters - Already Active) - 400252", function()
    {
      queue_WP.queueids = [ firstActiveQueue.queueid ];
      queue_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400252);
      });
    });

    it("should return error(incorrect parameters - Already InActive) - 400253", function()
    {
      queue_WP.queueids = [ firstInactiveQueue.queueid ];
      queue_WP.action = false;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400253);
      });
    });

    it("should return error(incorrect parameters - InActive CB) - 400261", function()
    {
      queue_WP.queueids = [ secondInactiveQueue.queueid ];
      queue_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400261);
      });
    });

    it("should return error(incorrect parameters - No Active Members) - 400260", function()
    {
      queue_WP.queueids = [ firstInactiveQueue.queueid ];
      queue_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400260);
      });
    });

    it("should deactivate queue", function()
    {
      queue_CP.queueids = [ firstActiveQueue.queueid ];
      queue_CP.action = false;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
      });
    });

    it("should activate queue", function()
    {
      queue_CP.queueids = [ firstActiveQueue.queueid ];
      queue_CP.action = true;
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
      });
    });
  });

  describe("#memberassoc()", function()
  {

    var queue_CP =
    {
      "queueid": 5,
      "members": [ 6, 7, 8 ],
      "associate": true
    };

    var queue_WP =
    {
      "members": [ 5, 1 ],
      "associate": true
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/activation")
      .set("Authorization", superUser.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Array Fields) - 400051", function()
    {
      queue_WP.queueid = [ 1, 2, 3 ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Wrong Value - NAN) - 400051", function()
    {
      queue_WP.queueid = 'a';
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Wrong Value - NAB) - 400051", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = "true";
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Non Existent Queue) - 400254", function()
    {
      queue_WP.queueid = 1118;
      queue_WP.associate = true;
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400254);
      });
    });

    it("should return error(incorrect parameters - Frozen Queue) - 400264", function()
    {
      queue_WP.queueid = firstFrozenQueue.queueid;
      queue_WP.associate = true;
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400264);
      });
    });

    it("should return error(incorrect parameters - User does not Exist) - 400104", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = true;
      queue_WP.members = [ 1118 ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400104);
      });
    });

    it("should return error(incorrect parameters - User - NAN) - 400051", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = true;
      queue_WP.members = [ 'a' ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - Associating an inactive user) - 400103", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = true;
      queue_WP.members = [ firstInactiveUser.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400103);
      });
    });

    it("should return error(incorrect parameters - Associating an existing member of the queue) - 400256", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = true;
      queue_WP.members = [ firstOperator.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400256);
      });
    });

    it("should return error(incorrect parameters - Disassociating a user who is not a member of the queue) - 400258", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = false;
      queue_WP.members = [ secondOperator.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400258);
      });
    });

    it("should disassociate all users from an inactive queue", function()
    {
      queue_CP.queueid = secondInactiveQueue.queueid;
      queue_CP.associate = false;
      queue_CP.members = [ secondOperator.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("queueid").and.equal(secondInactiveQueue.queueid);
      });
    });

    it("should return error(incorrect parameters - Disassociating from a queue that has no members) - 400257", function()
    {
      queue_WP.queueid = secondInactiveQueue.queueid;
      queue_WP.associate = false;
      queue_WP.members = [ secondOperator.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400257);
      });
    });

    it("should associate users to an inactive queue", function()
    {
      queue_CP.queueid = secondInactiveQueue.queueid;
      queue_CP.associate = true;
      queue_CP.members = [ secondOperator.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("queueid").and.equal(secondInactiveQueue.queueid);
      });
    });

    it("should dissociate users from an active queue", function()
    {
      queue_CP.queueid = firstActiveQueue.queueid;
      queue_CP.associate = false;
      queue_CP.members = [ firstOperator.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("queueid").and.equal(firstActiveQueue.queueid);
      });
    });

    it("should return error(incorrect parameters - Dissociating last member of an active queue) - 400259", function()
    {
      queue_WP.queueid = firstActiveQueue.queueid;
      queue_WP.associate = false;
      queue_WP.members = [ firstBDAdmin.userid ];
      return request(sails.hooks.http.app)
      .post("/queue/memberassoc")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400259);
      });
    });
  });

  describe("#getdetail()", function()
  {
    var queue_CP = {},  queue_WP = {};

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      queue_WP.queue_ids = [ firstActiveQueue.queueid ];
      return request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Wrong Value - NAN) - 400051", function()
    {
      delete queue_WP.queue_ids;
      queue_WP.queueids = [ firstActiveQueue.queueid, 'a' ];
      return request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Wrong Value) - 400051", function()
    {
      queue_WP.queueids = firstActiveQueue.queueid;
      return request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Wrong Value - Empty Array) - 400051", function()
    {
      queue_WP.queueids = [];
      return request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Non Existent Queues) - 400254", function()
    {
      queue_WP.queueids = [ firstActiveQueue.queueid, 1118 ];
      return request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400254);
      });
    });

    it("should return details of the queues", function()
    {
      queue_CP.queueids = [ firstActiveQueue.queueid, secondActiveQueue.queueid, secondInactiveQueue.queueid ];
      return request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body).to.have.lengthOf(3);
      });
    });
  });

  describe("#getstatus()", function()
  {
    var queue_CP = {},  queue_WP = {};

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      queue_WP.queue_ids = [ firstActiveQueue.queueid ];
      return request(sails.hooks.http.app)
      .post("/queue/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Wrong Value - NAN) - 400051", function()
    {
      delete queue_WP.queue_ids;
      queue_WP.queueids = [ firstActiveQueue.queueid, 'a' ];
      return request(sails.hooks.http.app)
      .post("/queue/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Wrong Value) - 400051", function()
    {
      queue_WP.queueids = firstActiveQueue.queueid;
      return request(sails.hooks.http.app)
      .post("/queue/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Wrong Value - Empty Array) - 400051", function()
    {
      queue_WP.queueids = [];
      return request(sails.hooks.http.app)
      .post("/queue/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Non Existent Queues) - 400254", function()
    {
      queue_WP.queueids = [ firstActiveQueue.queueid, 1118 ];
      return request(sails.hooks.http.app)
      .post("/queue/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400254);
      });
    });

    it("should return details of the queues", function()
    {
      queue_CP.queueids = [ firstActiveQueue.queueid, secondActiveQueue.queueid, secondInactiveQueue.queueid ];
      return request(sails.hooks.http.app)
      .post("/queue/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body).to.have.lengthOf(3);
      });
    });
  });

  describe("#initiatetransfer()", function()
  {
    var queue_WP =
    {
      "entityids": 2,
      "targetentityid": 3,
      "approvaltype": 2
    };
    var queue_CP =
    {
      "entityid": 6,
      "targetentityid": 6,
      "approvaltype": enums.approval.TRANSFER_QUEUE
    };
    var approve_CP_1 =
    {
      "approvalid": 6,
      "approverid": 6,
      "approved": true
    };
    var approve_CP_2 =
    {
      "approvalid": 6,
      "approverid": 6,
      "approved": false
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return an error(incorrect parameters - Non-Existent Queue) - 400254", function()
    {
      queue_WP.entityid = 1118;
      queue_WP.targetentityid = secondActiveCb.cbid;
      queue_WP.approvaltype = enums.approval.TRANSFER_QUEUE;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400254);
      });
    });

    it("should return an error(incorrect parameters - Inactive Queue) - 400253", function()
    {
      queue_WP.entityid = firstInactiveQueue.queueid;
      queue_WP.targetentityid = secondActiveCb.cbid;
      queue_WP.approvaltype = enums.approval.TRANSFER_QUEUE;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400253);
      });
    });

    it("should initate transfer of the CB and create necessary approvals", function()
    {
      queue_CP.entityid = firstActiveQueue.queueid;
      queue_CP.targetentityid = secondActiveCb.cbid;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.lengthOf(2);
        expect(res.body[ 0 ][ 0 ]).to.have.property("approverid").and.equal(firstBDAdmin.userid);
        expect(res.body[ 0 ][ 1 ]).to.have.property("approverid").and.equal(firstBDAdmin.userid);
        expect(res.body[ 1 ][ 0 ]).to.have.property("queueid").and.equal(firstActiveQueue.queueid);
        expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(true);

        approve_CP_1 = res.body[ 0 ][ 0 ];
        approve_CP_2 = res.body[ 0 ][ 1 ];
      });
    });

    it("should not transfer the Queue - Approval rejected by present owner", function()
    {
      approve_CP_1.approved = false;
      return request(sails.hooks.http.app)
      .post("/orgapproval/approve")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(approve_CP_1)
      .then(function(res)
      {
        expect(res.body[ 0 ].cbid).to.not.equal(secondActiveCb.cbid);
        expect(res.body[ 0 ].isfrozen).to.equal(false);
        expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.NOT_FROZEN);
      });
    });

    it("should not transfer the Queue - Approval rejected by new owner", function()
    {
      queue_CP.entityid = firstActiveQueue.queueid;
      queue_CP.targetentityid = secondActiveCb.cbid;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        approve_CP_1 = res.body[ 0 ][ 0 ];
        approve_CP_2 = res.body[ 0 ][ 1 ];
        approve_CP_1.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP_1);
      })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        approve_CP_2.approved = false;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP_2);
      })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        expect(res.body[ 0 ].isfrozen).to.and.equal(false);
        expect(res.body[ 0 ].cbid).to.equal(firstActiveQueue.cbid);
        expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.NOT_FROZEN);
      });
    });

    it("should transfer the CB - Approval by both owners(1st receiver approval and then donor approval)", function()
    {
      queue_CP.entityid = firstActiveQueue.queueid;
      queue_CP.targetentityid = secondActiveCb.cbid;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        approve_CP_1 = res.body[ 0 ][ 0 ];
        approve_CP_2 = res.body[ 0 ][ 1 ];
        approve_CP_2.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP_2);
      })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        approve_CP_1.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP_1);
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].queueid).to.equal(firstActiveQueue.queueid);
        expect(res.body[ 0 ].isfrozen).to.equal(false);
        expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.NOT_FROZEN);
        expect(res.body[ 0 ].cbid).to.equal(secondActiveCb.cbid);
      });
    });
  });

  describe("#search()", function()
  {
    var queue_WP_1 =
    {
      "searched":  { "queuename": "2" },
      "result": [ "queuename", "queueemail", "isactive", "isfrozen" ],
      "orderby": "queuename"
    };

    var queue_WP_2 =
    {
      "search":  { "queuenames": "Te%" },
      "result": [ "queuename", "queueemail", "isactive", "isfrozen" ],
      "orderby": "queuename"
    };

    var queue_CP =
    {
      "search":  { "queuename": "qTe%" },
      "result": [ "queuename", "queueemail", "isactive", "isfrozen" ],
      "orderby": "queuename"
    };

    it("should return an error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/search")
      .set("Authorization", superUser.accTkn)
      .send(queue_WP_1)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Non ExistentColumn) - 400053", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/search")
      .set("Authorization", superUser.accTkn)
      .send(queue_WP_2)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400053);
      });
    });

    it("should search for the queues", function()
    {
      return request(sails.hooks.http.app)
      .post("/queue/search")
      .set("Authorization", superUser.accTkn)
      .send(queue_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.equal(5);
      });
    });
  });
});
