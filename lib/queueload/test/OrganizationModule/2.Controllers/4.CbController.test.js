/* FileName: CbController.test.js
* Description: This file defines all the unit test cases for the Cb controller.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 21/05/2016          Initial file creation                           bones
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var enums = require("../../../api/resources/Enums.js");

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname":"Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com"
};

var company =
{
  "accountid": "qlodtest",
  "name": "Wayne Enterprises",
  "address": "Gotham City",
  "accntadmin": 1,
  "globalconfig": {}
};

var firstBDAdmin =
{
  "firstname": "cbfirstAdmin",
  "lastname": "cbBDOne",
  "fullname":"cbfirstAdmin cbBDOne",
  "username": "cbfirst.bdoneadmin",
  "password": "password",
  "emailid": "cbfirst.bd1admin@testdomain.com"
};

var secondBDAdmin =
{
  "firstname": "cbsecondAdmin",
  "lastname": "cbBDOne",
  "fullname":"cbsecondAdmin cbBDOne",
  "username": "cbsecond.bdoneadmin",
  "password": "password",
  "emailid": "cbsecond.bd1admin@testdomain.com"
};

var thirdBDAdmin =
{
  "firstname": "cbthirdAdmin",
  "lastname": "cbBDOne",
  "fullname":"cbthirdAdmin cbBDOne",
  "username": "cbthird.bdoneadmin",
  "password": "password",
  "emailid": "cbthird.bd1admin@testdomain.com"
};

var fourthBDAdmin =
{
  "firstname": "cbfourthAdmin",
  "lastname": "cbBDOne",
  "fullname": "cbfourthAdmin cbBDOne",
  "username": "cbfourth.bdoneadmin",
  "password": "password",
  "emailid": "cbfourth.bd1admin@testdomain.com"
};

var userNormal =
{
  "firstname": "cbnormal",
  "lastname": "cbuser",
  "fullname":"cbnormal cbuser",
  "username": "cbnormal.user",
  "password": "password",
  "emailid": "cbnormal.user@testdomain.com"
};

var userNormal1 =
{
  "firstname": "cbnormalOne",
  "lastname": "cbuser",
  "fullname":"cbnormalOne cbuser",
  "username": "cbnormal1.user",
  "password": "password",
  "emailid": "cbnormal1.user@testdomain.com"
};

var userNormal2 =
{
  "firstname": "cbnormaltwo",
  "lastname": "cbuser",
  "fullname":"cbnormaltwo cbuser",
  "username": "cbnormal2.user",
  "password": "password",
  "emailid": "cbnormal2.user@testdomain.com"
};

var firstActiveBD =
{
  "bdname": "cbBusiness-Department.2",
  "isactive": true
};

var secondActiveBD =
{
  "bdname": "cbBusiness-Department.3",
  "isactive": true
};

var thirdActiveBD =
{
  "bdname": "cbBusiness-Department.4"
};

var fourthActiveBD =
{
  "bdname": "cbBusiness-Department.5"
};

var firstInactiveBD =
{
  "bdname": "cbBusiness-Department.6"
};

var assignBD_CP =
{
  "bdid": 2,
  "admins": [ 2 ],
  "assign": true
};

var activateBD_CP =
{
  "action": true
};

var user_CP_deactivate =
{
  "action": false
};

var firstActiveQueue =
{
  "queuename":"cbTestQueue1",
  "queueemail":"cbtestqueue1@testdomain.com",
  "alertl1":"cbtestqueue1alert1@testdomain.com",
  "alertl2":"cbtestqueue1alert2@testdomain.com",
  "alertl3":"cbtestqueue1alert3@testdomain.com"
};

var firstActiveCb, secondActiveCb;
var firstFrozenCb;
var firstInactiveCb, secondInactiveCb, thirdInactiveCb, fourthActiveCb;
var firstActiveService;

describe("CB Controller", function()
{
  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      var id = user.userid;
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: id }, { userid: id, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.accTkn = res.body.acctoken;
        superUser.refTkn = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(firstBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st BDAdmin");
      }
      else
      {
        firstBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(userNormal);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st normal User");
      }
      else
      {
        userNormal.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(userNormal1);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd normal User");
      }
      else
      {
        userNormal1.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(userNormal2);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd normal User");
      }
      else
      {
        userNormal2.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(secondBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 2nd BdAdmin");
      }
      else
      {
        secondBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 4th BdAdmin");
      }
      else
      {
        fourthBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.accTkn)
        .send(thirdBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 3rd BDAdmin");
      }
      else
      {
        thirdBDAdmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(firstInactiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstInactiveBD");
      }
      else
      {
        firstInactiveBD.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(firstActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstActiveBD");
      }
      else
      {
        firstActiveBD.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(fourthActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create fourthActiveBD");
      }
      else
      {
        fourthActiveBD.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.accTkn)
        .send(secondActiveBD);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add secondActiveBD");
      }
      else
      {
        secondActiveBD.bdid = res.body.bdid;
        assignBD_CP.bdid = secondActiveBD.bdid;
        assignBD_CP.admins = [ secondBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign secondBDAdmin to secondActiveBD");
      }
      else
      {
        secondActiveBD.admins = [ secondBDAdmin.userid ];
        assignBD_CP.bdid = firstInactiveBD.bdid;
        assignBD_CP.admins = [ firstBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign firstBDAdmin to firstInactiveBD");
      }
      else
      {
        firstActiveBD.admins = [ firstBDAdmin.userid ];
        activateBD_CP.bdids = [ firstInactiveBD.bdid ];
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.accTkn)
        .send(activateBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate firstInactiveBD");
      }
      else
      {
        firstInactiveBD.isactive = true;
        assignBD_CP.bdid = firstActiveBD.bdid;
        assignBD_CP.admins = [ firstBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign firstBDAdmin to firstActiveBD");
      }
      else
      {
        firstActiveBD.admins = [ firstBDAdmin.userid ];
        activateBD_CP.bdids = [ firstActiveBD.bdid ];
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.accTkn)
        .send(activateBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate firstActiveBD");
      }
      else
      {
        firstActiveBD.isactive = true;
        assignBD_CP.bdid = fourthActiveBD.bdid;
        assignBD_CP.admins = [ fourthBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send(assignBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign fourthBDAdmin to fourthActiveBD");
      }
      else
      {
        fourthActiveBD.admins = [ fourthBDAdmin.userid ];
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(fourthBDAdmin);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin firstBDAdmin");
      }
      else
      {
        fourthBDAdmin.accTkn = res.body.acctoken;
        activateBD_CP.bdids = [ fourthActiveBD.bdid ];
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.accTkn)
        .send(activateBD_CP);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate fourthActiveBD");
      }
      else
      {
        fourthActiveBD.isactive = true;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ bdid: fourthActiveBD.bdid, ownerid: fourthBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create 1st CB for BD fourthActiveBD");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the CB firstActiveCb");
      }
      else
      {
        firstActiveCb = res.body[ 0 ];
        firstActiveQueue.cbid = firstActiveCb.cbid;
        firstActiveQueue.queueownerid = firstBDAdmin.userid;
        firstActiveQueue.changemanagerid = firstBDAdmin.userid;
        firstActiveQueue.srtmanagerid = firstBDAdmin.userid;
        firstActiveQueue.incidentmanagerid = firstBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send(firstActiveQueue);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add queue 1");
      }
      else
      {
        var orgApproval = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue 1");
      }
      else
      {
        firstActiveQueue = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ queueid: firstActiveQueue.queueid, members: [ userNormal1.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue firstActiveQueue");
      }
      else
      {
        firstActiveQueue.members = [ userNormal1.userid ];
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ queueids: [ firstActiveQueue.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 1");
      }
      else
      {
        firstActiveQueue.isactive = true;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ bdid: fourthActiveBD.bdid, ownerid: fourthBDAdmin.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create fourthActiveCb CB for BD fourthActiveBD");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the CB firstActiveCb");
      }
      else
      {
        fourthActiveCb = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(userNormal1);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin userNormal1");
      }
      else
      {
        userNormal1.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post('/service/add')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ servicename: "cbservice1", cbid: fourthActiveCb.cbid, details: "cb service1 details" });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create firstActiveService");
      }
      else
      {
        firstActiveService = res.body;
        return request(sails.hooks.http.app)
        .post('/service/queueassign')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ serviceid: firstActiveService.serviceid, supportqueue: firstActiveQueue.queueid, assign: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approvals for firstActiveService queue association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ approvalid: res.body.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve firstActiveService queue association");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/next')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ serviceid: firstActiveService.serviceid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create approvals for firstActiveService state transition");
      }
      else
      {
        return Serviceapproval.findOne({ approverserviceid: firstActiveService.serviceid, type: "state", isactive: true });
      }
    })
    .then(function(approval)
    {
      if(!approval)
      {
        throw new Error("Couldn't find approvals for firstActiveService");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/service/approve')
        .set('Authorization', fourthBDAdmin.accTkn)
        .send({ type: approval.type, approvalid: approval.approvalid, serviceid: firstActiveService.serviceid, isapproved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve firstActiveService");
      }
      else
      {
        done();
      }
    })
    .catch(done);
  });

  describe("#list()", function()
  {
    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(empty parameter list) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/list")
      .set("Authorization", firstBDAdmin.accTkn)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/list")
      .send({ "page": 1, "limit": "a" })
      .set("Authorization", firstBDAdmin.accTkn)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should list all cb in the system", function(done)
    {
      request(sails.hooks.http.app)
      .post("/cb/list")
      .set("Authorization", firstBDAdmin.accTkn)
      .send({ "page": 1, "limit": 100 })
      .expect(200, done);
    });
  });

  describe("#add()", function()
  {
    var cb_WP = {};
    var cb_CP = {};
    var approve_CP = {};

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(secondBDAdmin);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin secondBDAdmin");
        }
        else
        {
          secondBDAdmin.accTkn = res.body.acctoken;
          user_CP_deactivate.userids = [ userNormal.userid ];
          return request(sails.hooks.http.app)
          .post("/user/activation")
          .set('Authorization', superUser.accTkn)
          .send(user_CP_deactivate);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't make the normal user as inactive");
        }
        else
        {
          userNormal.isactive = false;
          done();
        }
      })
      .catch(done);
    });

    it("should return error(Not Authorized) - 403502", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", superUser.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return error(incorrect parameters - nonExistentFields) - 400051", function()
    {
      cb_WP.bdids = 2;
      cb_WP.ownerids = 1;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - incorrect types - 1) - 400051", function()
    {
      cb_WP.bdid = firstActiveBD.bdid;
      cb_WP.ownerid = "1";
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - incorrect types - 2) - 400051", function()
    {
      cb_WP.bdid = "2";
      cb_WP.ownerid = firstBDAdmin.userid;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - isactive present) - 400051", function()
    {
      cb_WP.bdid = firstActiveBD.bdid;
      cb_WP.ownerid = firstBDAdmin.userid;
      cb_WP.isactive = true;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
        delete cb_WP.isactive;
      });
    });

    it("should return error(incorrect parameters - array fields) - 400051", function()
    {
      cb_WP.bdid = [ firstActiveBD.bdid, secondActiveBD.bdid ];
      cb_WP.ownerid = firstBDAdmin.userid;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return error(incorrect parameters - non-existent BD) - 400154", function()
    {
      cb_WP.bdid = 1118;
      cb_WP.ownerid = firstBDAdmin.userid;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400154);
      });
    });

    it("should return error(incorrect parameters - non-existent user) - 400104", function()
    {
      cb_WP.bdid = firstActiveBD.bdid;
      cb_WP.ownerid = 1118;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400104);
      });
    });

    it("should return error(incorrect parameters - InActive BD) - 400153", function()
    {
      cb_WP.bdid = secondActiveBD.bdid;
      cb_WP.ownerid = firstBDAdmin.userid;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", secondBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400153);
      });
    });

    it("should return error(incorrect parameters - InActive User) - 400103", function()
    {
      cb_WP.bdid = firstActiveBD.bdid;
      cb_WP.ownerid = userNormal.userid;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400103);
      });
    });

    it("should return error(requestor not admin of BD) - 403501", function()
    {
      cb_WP.bdid = firstActiveBD.bdid;
      cb_WP.ownerid = firstBDAdmin.userid;
      return request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", secondBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403501);
      });
    });

    describe('Adding a CB - successful Approval', function()
    {
      var addedCB1ApprovalId, addedCB1ApproverId;
      it("should add a CB to the system - Pre-Approval", function()
      {
        cb_CP.bdid = firstActiveBD.bdid;
        cb_CP.ownerid = firstBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post("/cb/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(cb_CP)
        .then(function(res)
        {
          expect(res.body[ 0 ]).to.have.property("typeofapproval").and.equal(enums.approval.ASSIGN_CB2U);
          expect(res.body[ 0 ]).to.have.property("targetentityid").and.equal(cb_CP.ownerid);
          expect(res.body[ 0 ]).to.have.property("entityid").and.equal(res.body[ 1 ].cbid);
          expect(res.body[ 1 ]).to.have.property("isactive").and.equal(false);
          expect(res.body[ 1 ]).to.have.property("isfrozen").and.equal(true);
          expect(res.body[ 1 ]).to.have.property("cbownerid").and.equal(null);
          addedCB1ApprovalId = res.body[ 0 ].approvalid;
          addedCB1ApproverId = res.body[ 0 ].approverid;
        });
      });

      it("should raise an error - Approval doesnt exist -400368", function()
      {
        approve_CP.approvalid = 1188;
        approve_CP.approved = false;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400368);
        });
      });

      it("should raise an error - Not Correct Approver - 400367", function()
      {
        approve_CP.approvalid = addedCB1ApprovalId;
        approve_CP.approved = false;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", secondBDAdmin.accTkn)
        .send(approve_CP)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400367);
        });
      });

      it("should add a CB to the system - Post-Approval", function()
      {
        approve_CP.approvalid = addedCB1ApprovalId;
        approve_CP.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP)
        .then(function(res)
        {
          expect(res.body[ 0 ].isactive).to.have.equal(true);
          expect(res.body[ 0 ].isfrozen).to.equal(false);
          expect(res.body[ 0 ].cbownerid).to.equal(cb_CP.ownerid);
          secondActiveCb = res.body[ 0 ];
        });
      });

      it("should raise an error - Approval already processed - 400263", function()
      {
        approve_CP.approvalid = addedCB1ApprovalId;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP)
        .then(function(res)
        {
          expect(res.body).to.have.property("code").and.equal(400263);
        });
      });
    });

    describe('Adding a CB - Rejecting Approval', function()
    {
      var addedCB1ApprovalId, addedCB1ApproverId;
      it("should add a CB to the system - Pre-Approval", function()
      {
        cb_CP.bdid = firstActiveBD.bdid;
        cb_CP.ownerid = firstBDAdmin.userid;
        return request(sails.hooks.http.app)
        .post("/cb/add")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(cb_CP)
        .then(function(res)
        {
          expect(res.body[ 0 ]).to.have.property("typeofapproval").and.equal(enums.approval.ASSIGN_CB2U);
          expect(res.body[ 0 ]).to.have.property("targetentityid").and.equal(cb_CP.ownerid);
          expect(res.body[ 0 ]).to.have.property("entityid").and.equal(res.body[ 1 ].cbid);
          expect(res.body[ 1 ]).to.have.property("isactive").and.equal(false);
          expect(res.body[ 1 ]).to.have.property("isfrozen").and.equal(true);
          expect(res.body[ 1 ]).to.have.property("cbownerid").and.equal(null);
          addedCB1ApprovalId = res.body[ 0 ].approvalid;
          addedCB1ApproverId = res.body[ 0 ].approverid;
        });
      });

      it("should not add a CB to the system - Post-Approval(Assignment not Approved)", function()
      {
        approve_CP.approvalid = addedCB1ApprovalId;
        approve_CP.approved = false;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP)
        .then(function(res)
        {
          expect(res.body[ 0 ].isactive).to.equal(false);
          expect(res.body[ 0 ].cbownerid).to.not.equal(firstBDAdmin.userid);
          expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.CREATION_DECLINED);
          firstFrozenCb = res.body[ 0 ];
        });
      });
    });
  });

  describe("#activation()", function()
  {
    var cb_WP = {};
    var cb_CP = {};
    var approve_CP = {};

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(userNormal2);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          userNormal2.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(secondBDAdmin);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin secondBDAdmin");
        }
        else
        {
          secondBDAdmin.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/cb/add')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ bdid: firstInactiveBD.bdid, ownerid: firstBDAdmin.userid });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create CB for BD firstInactiveBD");
        }
        else
        {
          var orgApproval = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ approvalid: orgApproval.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't activate the CB firstInactiveCb");
        }
        else
        {
          firstInactiveCb = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/cb/activation')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ cbids:[ firstInactiveCb.cbid ], action: false });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't deactivate the firstInactiveCb");
        }
        else
        {
          firstInactiveCb.isactive = false;
          return request(sails.hooks.http.app)
          .post('/bd/activation')
          .set('Authorization', superUser.accTkn)
          .send({ bdids:[ firstInactiveBD.bdid ], action: false });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't deactivate the firstInactiveBD");
        }
        else
        {
          firstInactiveBD.isactive = false;
          return request(sails.hooks.http.app)
          .post('/cb/add')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ bdid: firstActiveBD.bdid, ownerid: firstBDAdmin.userid });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create secondInactiveCb for BD firstActiveBD");
        }
        else
        {
          var orgApproval = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ approvalid: orgApproval.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve the CB secondInactiveCb");
        }
        else
        {
          secondInactiveCb = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/cb/activation')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ cbids:[ secondInactiveCb.cbid ], action: false });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't deactivate the secondInactiveCb");
        }
        else
        {
          secondInactiveCb.isactive = false;
          return request(sails.hooks.http.app)
          .post('/cb/add')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ bdid: firstActiveBD.bdid, ownerid: userNormal2.userid });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create thirdInactiveCb for BD firstActiveBD");
        }
        else
        {
          var orgApproval = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', userNormal2.accTkn)
          .send({ approvalid: orgApproval.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve the CB thirdInactiveCb");
        }
        else
        {
          thirdInactiveCb = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/cb/activation')
          .set('Authorization', firstBDAdmin.accTkn)
          .send({ cbids:[ thirdInactiveCb.cbid ], action: false });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't deactivate the thirdInactiveCb");
        }
        else
        {
          thirdInactiveCb.isactive = false;
          user_CP_deactivate.userids = [ userNormal2.userid ];
          return request(sails.hooks.http.app)
          .post("/user/activation")
          .set('Authorization', superUser.accTkn)
          .send(user_CP_deactivate);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't make normal user2 as inactive");
        }
        else
        {
          userNormal2.isactive = false;
          done();
        }
      })
      .catch(done);
    });

    it("should return an error(not authorized) - 403502", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, secondActiveCb.cbid ];
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", superUser.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
      });
    });

    it("should return an error(incorrect parameters - missing action field) - 400051", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, secondActiveCb.cbid ];
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(not authorized - requestor not admin of BD) - 403501", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, secondActiveCb.cbid ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403501);
      });
    });

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      cb_WP.cbids = firstActiveCb.cbid;
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - NAB) - 400051", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, secondActiveCb.cbid ];
      cb_WP.action = "true";
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - empty) - 400051", function()
    {
      cb_WP.cbids = [];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - NAN) - 400051", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, "a" ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - non existent cbids) - 400204", function()
    {
      cb_WP.cbids = [ secondActiveCb.cbid, 1118 ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400204);
      });
    });

    it("should return an error(incorrect parameters - frozen CB) - 400208", function()
    {
      cb_WP.cbids = [ secondActiveCb.cbid, firstFrozenCb.cbid ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400208);
      });
    });

    it("action:true should return an error(all cbids are not inactive) - 400202", function()
    {
      cb_WP.cbids = [ secondInactiveCb.cbid, secondActiveCb.cbid ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400202);
      });
    });

    it("action:true should return an error(inactive BD) - 400153", function()
    {
      cb_WP.cbids = [ firstInactiveCb.cbid ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400153);
      });
    });

    it("action:true should return an error(inactive owner) - 400206", function()
    {
      cb_WP.cbids = [ secondInactiveCb.cbid, thirdInactiveCb.cbid ];
      cb_WP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400206);
      });
    });

    it("action:false should return an error(all cbids are not active) - 400203", function()
    {
      cb_WP.cbids = [ secondInactiveCb.cbid, secondActiveCb.cbid ];
      cb_WP.action = false;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400203);
      });
    });

    it("action:false should return an error(queue owned by CB is active) - 400207", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid ];
      cb_WP.action = false;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", fourthBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400207);
      });
    });

    it("action:false should return an error(service owned by CB is active) - 400212", function()
    {
      cb_WP.cbids = [ fourthActiveCb.cbid ];
      cb_WP.action = false;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", fourthBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400212);
      });
    });

    it("action:true should make the CB active", function()
    {
      cb_CP.cbids = [ secondInactiveCb.cbid ];
      cb_CP.action = true;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
      });
    });

    it("action:false should make the CB inactive", function()
    {
      cb_CP.cbids = [ secondInactiveCb.cbid ];
      cb_CP.action = false;
      return request(sails.hooks.http.app)
      .post("/cb/activation")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
      });
    });
  });

  describe("#getdetail()", function()
  {
    var cb_WP =
    {
      "cbids": 1118
    };

    var cb_CP =
    {
      "cbids": 1
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - incorrect field value) - 400051", function()
    {
      cb_WP.cbids = [ 1, 2, 3, "salt" ];
      return request(sails.hooks.http.app)
      .post("/cb/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - empty Array) - 400051", function()
    {
      cb_WP.cbids = [];
      return request(sails.hooks.http.app)
      .post("/cb/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - missing) - 400051", function()
    {
      cb_WP.cbid = [];
      delete cb_WP.cbids;
      return request(sails.hooks.http.app)
      .post("/cb/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
        delete cb_WP.cbid;
      });
    });

    it("should return an error(non existent cbid multiple) - 400204", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, firstInactiveCb.cbid, 1118 ];
      return request(sails.hooks.http.app)
      .post("/cb/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400204);
      });
    });

    it("should get CB details multiple", function()
    {
      cb_CP.cbids = [ firstActiveCb.cbid, firstInactiveCb.cbid ];
      return request(sails.hooks.http.app)
      .post("/cb/getdetail")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        expect(res.body).to.have.lengthOf(2);
      });
    });
  });

  describe("#getstatus()", function()
  {
    var cb_WP =
    {
      "cbids": 1118
    };

    var cb_CP =
    {
      "cbids": 1
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          done();
        }
      })
      .catch(done);
    });

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - incorrect field value) - 400051", function()
    {
      cb_WP.cbids = [ 1, 2, 3, "salt" ];
      return request(sails.hooks.http.app)
      .post("/cb/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - empty Array) - 400051", function()
    {
      cb_WP.cbids = [];
      return request(sails.hooks.http.app)
      .post("/cb/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - missing) - 400051", function()
    {
      cb_WP.cbid = [];
      delete cb_WP.cbids;
      return request(sails.hooks.http.app)
      .post("/cb/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
        delete cb_WP.cbid;
      });
    });

    it("should return an error(non existent cbid multiple) - 400204", function()
    {
      cb_WP.cbids = [ firstActiveCb.cbid, firstInactiveCb.cbid, 1118 ];
      return request(sails.hooks.http.app)
      .post("/cb/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400204);
      });
    });

    it("should get CB status multiple", function()
    {
      cb_CP.cbids = [ firstActiveCb.cbid, firstInactiveCb.cbid ];
      return request(sails.hooks.http.app)
      .post("/cb/getstatus")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        expect(res.body).to.have.lengthOf(2);
      });
    });
  });

  describe("#search()", function()
  {
    var cb_WP_1 =
    {
      "searched":  { "cbid": "2" },
      "result": [ "firstname", "lastname", "username", "emailid" ],
      "orderby": "firstname"
    };

    var cb_WP_2 =
    {
      "search":  { "cbids": "8" },
      "result": [ "cbid", "bdid", "isactive", "isfrozen" ],
      "orderby": "bdid"
    };

    var cb_CP =
    {
      "search":  { "cbid": 1 },
      "result": [ "cbid", "bdid", "cbownerid", "isfrozen" ],
      "orderby": "bdid"
    };

    it("should return an error(incorrect parameters - Non ExistentField) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/search")
      .set("Authorization", superUser.accTkn)
      .send(cb_WP_1)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Non ExistentColumn) - 400053", function()
    {
      return request(sails.hooks.http.app)
      .post("/cb/search")
      .set("Authorization", superUser.accTkn)
      .send(cb_WP_2)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400053);
      });
    });

    it("should search for the chargeback codes", function()
    {
      cb_CP.search = { "cbid": firstActiveCb.cbid };
      return request(sails.hooks.http.app)
      .post("/cb/search")
      .set("Authorization", superUser.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.equal(1);
        expect(Object.keys(res.body[ 1 ][ 0 ]).length).to.equal(4);
      });
    });
  });

  describe("#initiatetransfer()", function()
  {
    var cb_WP =
    {
      "entityids": 2,
      "targetentityid": 3,
      "approvaltype": 2
    };
    var cb_CP =
    {
      "entityid": 6,
      "targetentityid": 6,
      "approvaltype": enums.approval.TRANSFER_CB
    };
    var approve_CP_1 =
    {
      "approvalid": 6,
      "approverid": 6,
      "approved": true
    };
    var approve_CP_2 =
    {
      "approvalid": 6,
      "approverid": 6,
      "approved": false
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(firstBDAdmin)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin firstBDAdmin");
        }
        else
        {
          firstBDAdmin.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(thirdBDAdmin);
        }
      })
      .then(function(res)
      {
        thirdBDAdmin.accTkn = res.body.acctoken;
        done();
      })
      .catch(done);
    });

    it("should return an error(incorrect parameters - incorrect parameter name) - 400051", function()
    {
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
        delete cb_WP.entityids;
      });
    });

    it("should return an error(incorrect parameters - Array) - 400051", function()
    {
      cb_WP.targetentityid = [ 1, 2, 3 ];
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
        delete cb_WP.entityids;
      });
    });

    it("should return an error(incorrect parameters - incorrect type) - 400051", function()
    {
      cb_WP.targetentityid = [ 1 ];
      cb_WP.approvaltype = "salt";
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Non-Existent CB) - 400204", function()
    {
      cb_WP.entityid = 1118;
      cb_WP.targetentityid = firstBDAdmin.userid;
      cb_WP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400204);
      });
    });

    it("should return an error(incorrect parameters - Non-Existent New Owner) - 400104", function()
    {
      cb_WP.entityid = secondActiveCb.cbid;
      cb_WP.targetentityid = 1118;
      cb_WP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400104);
      });
    });

    it("should return an error(incorrect parameters - Non-Existent Approval Type) - 400051", function()
    {
      cb_WP.entityid = secondActiveCb.cbid;
      cb_WP.targetentityid = firstBDAdmin.userid;
      cb_WP.approvaltype = 1118;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      });
    });

    it("should return an error(incorrect parameters - Inactive CB) - 400203", function()
    {
      cb_WP.entityid = secondInactiveCb.cbid;
      cb_WP.targetentityid = firstBDAdmin.userid;
      cb_WP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400203);
      });
    });

    it("should return an error(incorrect parameters - Inactive New Owner) - 400103", function()
    {
      cb_WP.entityid = secondInactiveCb.cbid;
      cb_WP.targetentityid = userNormal2.userid;
      cb_WP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400103);
      });
    });

    it("should return an error(incorrect parameters - frozen CB) - 400208", function()
    {
      cb_WP.entityid = firstFrozenCb.cbid;
      cb_WP.targetentityid = firstBDAdmin.userid;
      cb_WP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_WP)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400203);
      });
    });

    it("should initate transfer of the CB and create necessary approvals", function()
    {
      cb_CP.entityid = secondActiveCb.cbid;
      cb_CP.targetentityid = thirdBDAdmin.userid;
      cb_CP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        expect(res.body[ 0 ]).to.have.lengthOf(2);
        expect(res.body[ 0 ][ 0 ]).to.have.property("approverid").and.equal(firstBDAdmin.userid);
        expect(res.body[ 0 ][ 1 ]).to.have.property("approverid").and.equal(thirdBDAdmin.userid);
        expect(res.body[ 1 ][ 0 ]).to.have.property("cbid").and.equal(secondActiveCb.cbid);
        expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(true);

        approve_CP_1 = res.body[ 0 ][ 0 ];
        approve_CP_2 = res.body[ 0 ][ 1 ];
      });
    });

    it("should not transfer the CB - Approval rejected by present owner", function()
    {
      approve_CP_1.approved = false;
      return request(sails.hooks.http.app)
      .post("/orgapproval/approve")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(approve_CP_1)
      .then(function(res)
      {
        expect(res.body[ 0 ].cbownerid).to.not.equal(thirdBDAdmin.userid);
        expect(res.body[ 0 ].isfrozen).to.equal(false);
        expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.NOT_FROZEN);
      });
    });

    it("should not transfer CB - approval already processed - 400263", function()
    {
      approve_CP_2.approved = true;
      return request(sails.hooks.http.app)
      .post("/orgapproval/approve")
      .set("Authorization", thirdBDAdmin.accTkn)
      .send(approve_CP_2)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400263);
      });
    });

    it("should not transfer the CB - Approval rejected by new owner", function()
    {
      cb_CP.entityid = secondActiveCb.cbid;
      cb_CP.targetentityid = thirdBDAdmin.userid;
      cb_CP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        approve_CP_1 = res.body[ 0 ][ 0 ];
        approve_CP_2 = res.body[ 0 ][ 1 ];
        approve_CP_1.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP_1);
      })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        approve_CP_2.approved = false;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", thirdBDAdmin.accTkn)
        .send(approve_CP_2);
      })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        expect(res.body[ 0 ].isfrozen).to.and.equal(false);
        expect(res.body[ 0 ].cbownerid).to.equal(firstBDAdmin.userid);
        expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.NOT_FROZEN);
      });
    });
    it("should transfer the CB - Approval by both owners(1st receiver approval and then donor approval)", function()
    {
      cb_CP.entityid = secondActiveCb.cbid;
      cb_CP.targetentityid = thirdBDAdmin.userid;
      cb_CP.approvaltype = enums.approval.TRANSFER_CB;
      return request(sails.hooks.http.app)
      .post("/orgapproval/initiatetransfer")
      .set("Authorization", firstBDAdmin.accTkn)
      .send(cb_CP)
      .then(function(res)
      {
        approve_CP_1 = res.body[ 0 ][ 0 ];
        approve_CP_2 = res.body[ 0 ][ 1 ];
        approve_CP_2.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", thirdBDAdmin.accTkn)
        .send(approve_CP_2);
      })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        approve_CP_1.approved = true;
        return request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", firstBDAdmin.accTkn)
        .send(approve_CP_1);
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].cbid).to.equal(secondActiveCb.cbid);
        expect(res.body[ 0 ].isfrozen).to.equal(false);
        expect(res.body[ 0 ].freezecause).to.equal(enums.freezeCauses.NOT_FROZEN);
        expect(res.body[ 0 ].cbownerid).to.equal(thirdBDAdmin.userid);
      });
    });
  });
});
