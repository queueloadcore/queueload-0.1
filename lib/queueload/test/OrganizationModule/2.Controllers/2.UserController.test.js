/* FileName: UserController.test.js
* Description: This file defines all the unit test cases for the User Controller.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 17/03/2016          Initial file creation                           bones
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var _ = require("lodash");
var enums = require('../../../api/resources/Enums.js');

describe("User Controller", function()
{
  function user(userid, username, emailid, isactive, accTkn, refTkn, password)
  {
    this.userid = userid;
    this.username = username;
    this.emailid = emailid;
    this.isactive = isactive;
    this.accTkn = accTkn;
    this.refTkn = refTkn;
    this.password = password;
  }
  function bd(bdid, bdamin)
  {
    this.bdid = bdid;
    this.bdamin = bdamin;
  }
  var userSuper;
  var user1;
  var user2;
  var orgApproval1;

  var company =
  {
    "accountid": "qlodtest",
    "name": "Wayne Enterprises",
    "address": "Gotham City",
    "accntadmin": 1,
    "globalconfig": {}
  };

  var superUser =
  {
    "fullname": "Super User",
    "username": "administrator",
    "password": "password",
    "emailid": "administrator@testdomain.com",
    "userconfig": {}
  };

  var tempBDadmin =
  {
    "fullname": "newkid intown",
    "username":"newkid.intown",
    "password":"password",
    "emailid":"newkid.intown@test.com",
    "countrycode":"+91",
    "phonenumber":"9438738189",
    "userconfig": {}
  };

  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      var id = user.userid;
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: id }, { userid: id, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.accTkn = res.body.acctoken;
        userSuper = new user(res.body.user.userid, res.body.user.username, res.body.user.emailid, res.body.user.isactive, res.body.acctoken, res.body.reftoken, superUser.password);
        done();
      }
    })
    .catch(done);
  });

  var userBdAdmin;

  describe("User: Add + Signin", function()
  {
    var validUser =
    {
      "emailid":"kit.test@testclients.com",
      "password":"P@55word"
    };

    var activeUser_CP =
    {
      "fullname": "test bug",
      "username":"kit.walker",
      "password":"P@55word",
      "emailid":"kit.test@testclients.com",
      "countrycode":"+91",
      "phonenumber":"9536841379"
    };
    it("Add", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userSuper.accTkn)
      .send(activeUser_CP)
      .expect(200)
      .end(function(err)
      {
        if(err) return done(err);
        done();
      });
    });
    it("Signin", function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(validUser)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        userBdAdmin = new user(res.body.user.userid, res.body.user.username, res.body.user.emailid, res.body.user.isactive, res.body.acctoken, res.body.reftoken, validUser.password);
        done();
      });
    });
  });

  var bdDenali;
  describe("BD: Add + assign + activate", function()
  {
    var activeBD_CP =
    {
      "bdname": "Business-Department.D5-31"
    };
    var assignBD_CP_activeBD =
    {
      "bdid": 2,
      "admins": [ 2 ],
      "assign": true
    };
    var bd_CP_2 =
    {
      "bdids": [ 2 ],
      "action": true
    };
    it("Add", function(done)
    {
      request(sails.hooks.http.app)
      .post("/bd/add")
      .set("Authorization", userSuper.accTkn)
      .send(activeBD_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        bdDenali = new bd(res.body.bdid);
        done();
      });
    });
    it("Assign", function(done)
    {
      assignBD_CP_activeBD.bdid = bdDenali.bdid;
      assignBD_CP_activeBD.admins = [ userBdAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", userSuper.accTkn)
      .send(assignBD_CP_activeBD)
      .expect(200)
      .end(function(err)
      {
        if(err) return done(err);
        done();
      });
    });
    it("Activate", function(done)
    {
      bd_CP_2.bdids = [ bdDenali.bdid ];
      request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", userSuper.accTkn)
      .send(bd_CP_2)
      .expect(200)
      .end(function(err, res)
      {
        if(err)
        {
          return done(err);
        }
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
        done();
      });
    });
  });

  describe("#list()", function()
  {
    it("should return error(empty parameter list) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(userBdAdmin)
      .then(function(res)
      {
        userBdAdmin.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post("/user/list")
        .set("Authorization", userBdAdmin.accTkn);
      })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(incorrect parameters) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ "page": 1, "limit": "a" })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should list all users in the system", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ "page": 1, "limit": 100 })
      .expect(200, done);
    });
  });

  describe("#add()", function()
  {
    var inActiveUser_WP =
    {
      "lastname":"rat3%",
      "password":"P@55word",
      "emailid":"tester2.rat@@testclient3.com",
      "countrycode":"+91a",
      "phonenumber":"982238712a",
      "ownedcbs": [ 1, 2, 3 ],
      "queuesenrolled": [ 4, 5, 6 ],
      "isactive": false
    };

    var inActiveUser_WP_array =
    {
      "username":[ "T3st.cat3", "dev.ops" ],
      "password":"P@55word",
      "emailid":"tester3.rat@testclient3.com",
      "countrycode":"+92",
      "phonenumber":"9822387102",
      "isactive": false
    };

    var activeUser_CP =
    {
      "fullname":"gammatester rat",
      "username":"T3st.cat3",
      "password":"P@55word",
      "emailid":"tester3.rat@testclient3.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    it("should return error(incorrect parameters) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(inActiveUser_WP)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should return error(array parameters) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(inActiveUser_WP_array)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should return error only phonenumber added - 500", function(done)
    {
      activeUser_CP.phonenumber = "";
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(activeUser_CP)
      .expect(500)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal('E_VALIDATION');
        done();
      });
    });

    it("should return error only countrycode added - 500", function(done)
    {
      activeUser_CP.phonenumber = "9874563698";
      activeUser_CP.countrycode = "";
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(activeUser_CP)
      .expect(500)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal('E_VALIDATION');
        done();
      });
    });
    it("should return error incorrect countrycode - 500", function(done)
    {
      activeUser_CP.phonenumber = "9874563698";
      activeUser_CP.countrycode = "+iu900";
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(activeUser_CP)
      .expect(500)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal('E_VALIDATION');
        done();
      });
    });
    it("should add a user to the system", function(done)
    {
      activeUser_CP.phonenumber = "";
      activeUser_CP.countrycode = "";
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(activeUser_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("isactive").and.equal(true);
        user1 = new user(res.body.userid, res.body.username, res.body.emailid, res.body.isactive);
        done();
      });
    });
    it("should return error - not authorized - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(activeUser_CP)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't sign-in active user");
        }
        else
        {
          activeUser_CP.accTkn = res.body.acctoken;
          activeUser_CP.reftoken = res.body.reftoken;
          return request(sails.hooks.http.app)
          .post("/user/add")
          .set("Authorization", activeUser_CP.accTkn)
          .send(activeUser_CP);
        }
      })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      })
      .catch(done);
    });
  });

  describe("#update()", function()
  {
    var user_WP_fields =
    {
      "userid": 2,
      "username":"T3st.cat3",
      "emailid":"email@testemail.com"
    };
    var user_WP_collFields =
    {
      "userid": 2,
      "ownedcbs": [ 1, 2, 3 ],
      "queuesenrolled": [ 4, 5, 6 ],
      "countrycode": "+92"
    };
    var user_WP_arrFields =
    {
      "userid": 3,
      "emailid":[ "test@test.com", "testing@testing.com" ]
    };
    var user_WP_userid =
    {
      "userid": 89,
      "emailid": "fred.flintstone@bedrock.com"
    };
    var user_WP_field_values =
    {
      "userid": 2,
      "phonenumber":"982238712a",
      "emailid":"tester2.rat@@testclient3.com"
    };
    var user_WP_NonUpdateFields =
    {
      "userid": 2,
      "username": "red123"
    };
    var user_WP_password =
    {
      "userid": 2,
      "password": "LWER@1123123"
    };
    var user_CP =
    {
      "userid": 2,
      "emailid": "fred.flintstone@bedrock.com"
    };

    var inactiveUser_CP =
    {
      "fullname":"deltatester rat",
      "username":"T3st.cat4",
      "password":"P@55word",
      "emailid":"tester4.rat@testclient4.com",
      "countrycode":"+92",
      "phonenumber":"9822387102",
      "isactive": false
    };

    var activeUser_CP =
    {
      "fullname":"epsilontester rat",
      "username":"T3st.cat5",
      "password":"P@55word",
      "emailid":"tester4.rat@testclient5.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser2_CP =
    {
      "fullname":"epsilontester rat",
      "username":"T3st.ct6",
      "password":"P@55word",
      "emailid":"tester6.rat@testclient.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser3_CP =
    {
      "fullname":"epsilontester rat",
      "username":"T3st.ct7",
      "password":"P@55word",
      "emailid":"tester7.rat@testclient7.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var queue1 =
    {
      "queuename": "Test-Queue.6",
      "queueemail": "testqueue6@test.com",
      "alertl1": "testqueue6@test.com",
      "alertl2": "testqueue6@test.com",
      "alertl3": "testqueue6@test.com"
    };
    var cb1 = {};

    var user_CP_phonenumber =
    {
      "userid": 2,
      "phonenumber": "9876451473"
    };
    var user_CP_countrycode =
    {
      "userid": 2,
      "countrycode": "+87"
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userBdAdmin.accTkn)
      .send(inactiveUser_CP)
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add the inactive user");
        }
        expect(res.body).to.have.property("isactive").and.equal(false);
        user2 = new user(res.body.userid, res.body.username, res.body.emailid, res.body.isactive);
        return request(sails.hooks.http.app)
        .post("/user/add")
        .set("Authorization", userBdAdmin.accTkn)
        .send(activeUser_CP);
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add the inactive user");
        }
        activeUser_CP.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/user/add")
        .set("Authorization", userBdAdmin.accTkn)
        .send(activeUser2_CP);
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add the active user2");
        }
        activeUser2_CP.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/user/add")
        .set("Authorization", userBdAdmin.accTkn)
        .send(activeUser3_CP);
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add the active user3");
        }
        activeUser3_CP.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(activeUser_CP);
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't sign-in active user");
        }
        else
        {
          activeUser_CP.accTkn = res.body.acctoken;
          activeUser_CP.reftoken = res.body.reftoken;
          return request(sails.hooks.http.app)
          .post('/cb/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send({ bdid: bdDenali.bdid, ownerid: activeUser_CP.userid });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 1st CB for BD 1");
        }
        else
        {
          orgApproval1 = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', activeUser_CP.accTkn)
          .send({ approvalid: orgApproval1.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't activate the CB 3");
        }
        else
        {
          cb1 = res.body[ 0 ];
          queue1.cbid = cb1.cbid;
          queue1.queueownerid = activeUser_CP.userid;
          return request(sails.hooks.http.app)
          .post('/queue/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(queue1);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add active queue");
        }
        else
        {
          orgApproval1 = res.body[ 1 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', activeUser_CP.accTkn)
          .send({ approvalid: orgApproval1.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve active queue");
        }
        else
        {
          queue1 = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/queue/memberassoc')
          .set('Authorization', userBdAdmin.accTkn)
          .send({ queueid: queue1.queueid, members: [ userBdAdmin.userid, activeUser_CP.userid ], associate: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't associate members with queue1");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(activeUser_CP);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin activeUser_CP");
        }
        else
        {
          activeUser_CP.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(userBdAdmin);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin userBdAdmin");
        }
        else
        {
          userBdAdmin.accTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post('/user/makesuperuser')
          .set('Authorization', userSuper.accTkn)
          .send({ userid: activeUser3_CP.userid, action: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't make activeUser3_CP as super user");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(activeUser3_CP);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't signin activeUser3_CP");
        }
        else
        {
          activeUser3_CP.accTkn = res.body.acctoken;
          activeUser3_CP.reftoken = res.body.reftoken;
          done();
        }
      })
      .catch(done);
    });

    it("should not return error, silently ignores disallowed fields", function(done)
    {
      user_WP_fields.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_fields)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("emailid").and.not.equal("email@email.com");
        user1.emailid = res.body[ 0 ].emailid;
        user1.username = res.body[ 0 ].username;
        done();
      });
    });

    it("should return error, with field validation failed", function(done)
    {
      user_WP_field_values.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_field_values)
      .expect(500)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal("E_VALIDATION");
        var numOfAttributes = res.body.message.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("2");
        done();
      });
    });

    it("should not return error, silently ignores disallowed collection fields", function(done)
    {
      user_WP_collFields.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_collFields)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("countrycode").and.equal("+92");
        done();
      });
    });

    it("should return error(array fields) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_arrFields)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should return error(non existent user) - 400104", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_userid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });
    it("should return error(incorrect parameters - No Valid Fields for Update) - 400052", function(done)
    {
      user_WP_NonUpdateFields.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_NonUpdateFields)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400052);
        done();
      });
    });
    it("should return error(user not authorized to update) - 403502", function(done)
    {
      user_CP_phonenumber.userid = userBdAdmin.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", activeUser_CP.accTkn)
      .send(user_CP_phonenumber)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("should return error(user not authorized to update super user) - 403502", function(done)
    {
      user_CP_phonenumber.userid = userSuper.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_phonenumber)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("should return error(incorrect parameters - Trying to update password) - 400051", function(done)
    {
      user_WP_password.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_password)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should update user with active status", function(done)
    {
      user_CP.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("emailid").and.equal("fred.flintstone@bedrock.com");
        done();
      });
    });
    it("should update - user updating himself", function(done)
    {
      user_CP_phonenumber.userid = activeUser_CP.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", activeUser_CP.accTkn)
      .send(user_CP_phonenumber)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("phonenumber").and.equal("9876451473");
        done();
      });
    });
    it("should update - super user updating himself", function(done)
    {
      user_CP.userid = userSuper.userid;
      user_CP.emailid = "wilma.flintstone@bedrock.com";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userSuper.accTkn)
      .send(user_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("emailid").and.equal("wilma.flintstone@bedrock.com");
        done();
      });
    });
    it("should update - a super user updating another superuser", function(done)
    {
      user_CP.userid = userSuper.userid;
      user_CP.emailid = "barney.rubble@bedrock.com";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", activeUser3_CP.accTkn)
      .send(user_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("emailid").and.equal("barney.rubble@bedrock.com");
        done();
      });
    });
    it("should return error - user with inactive status", function(done)
    {
      user_CP.userid = user2.userid;
      user_CP.emailid = "race.bannon@questworld.com";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP)
      .expect(400)
      .end(function(err)
      {
        if(err) return done(err);
        done();
      });
    });
    it("should return error - user not member of default queue - 400258", function(done)
    {
      user_CP.userid = activeUser2_CP.userid;
      user_CP.defaultqueue = queue1.queueid;
      user_CP.emailid = "race.bannon@questworld.com";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP)
      .expect(400)
      .end(function(err, res)
      {
        expect(res.body).to.have.property("code").and.equal(400258);
        done();
      });
    });
    it("should update - user member of default queue", function(done)
    {
      user_CP.userid = activeUser_CP.userid;
      user_CP.defaultqueue = queue1.queueid;
      user_CP.emailid = "race.bannon@questworld.com";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP)
      .expect(200)
      .end(function(err, res)
      {
        expect(res.body[ 0 ]).to.have.property("defaultqueue").and.equal(queue1.queueid);
        done();
      });
    });
    it("should update user phonenumber", function(done)
    {
      user_CP_phonenumber.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_phonenumber)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("userid").and.equal(user1.userid);
        expect(res.body[ 0 ]).to.have.property("phonenumber").and.equal("9876451473");
        done();
      });
    });
    it("should update user countrycode", function(done)
    {
      user_CP_countrycode.userid = user1.userid;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_countrycode)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("userid").and.equal(user1.userid);
        expect(res.body[ 0 ]).to.have.property("countrycode").and.equal("+87");
        done();
      });
    });
    it("should return error - emptying only phonenumber", function(done)
    {
      user_CP_phonenumber.userid = user1.userid;
      user_CP_phonenumber.phonenumber = "";
      user_CP_phonenumber.countrycode = "+91";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_phonenumber)
      .expect(500)
      .end(function(err)
      {
        if(err) return done(err);
        done();
      });
    });
    it("should return error - emptying only countrycode", function(done)
    {
      user_CP_phonenumber.userid = user1.userid;
      user_CP_phonenumber.countrycode = "";
      user_CP_phonenumber.countrycode = "8472789242";
      delete user_CP_phonenumber.phonenumber;
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_phonenumber)
      .expect(500)
      .end(function(err)
      {
        if(err) return done(err);
        done();
      });
    });
    it("should return error - incorrect countrycode", function(done)
    {
      user_CP_phonenumber.userid = user1.userid;
      user_CP_phonenumber.countrycode = "+rtfgg";
      user_CP_phonenumber.phonenumber = "9854563214";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_phonenumber)
      .expect(500)
      .end(function(err, res)
      {
        if(err) return done(err);
        done();
      });
    });
    it("should update user emptying both phonenumber and countrycode", function(done)
    {
      user_CP_phonenumber.userid = userBdAdmin.userid;
      user_CP_phonenumber.phonenumber = "";
      user_CP_phonenumber.countrycode = "";
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_phonenumber)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("userid").and.equal(userBdAdmin.userid);
        expect(res.body[ 0 ]).to.have.property("phonenumber").and.equal("");
        expect(res.body[ 0 ]).to.have.property("countrycode").and.equal("");
        done();
      });
    });
    it("should not update the user - user is a superuser and the requester is not", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: superUser.userid, phonenumber: 8374828945 })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(403502);
        done();
      });
    });
    it("should not update the user - old password is missing", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: activeUser2_CP.userid, newpass: "password" })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not update the user - new password is missing", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: activeUser2_CP.userid, oldpass: "password" })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not update the user - both password are same", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: activeUser2_CP.userid, oldpass: "password", newpass: "password" })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not update the user - one of the passwords is not string", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: activeUser2_CP.userid, oldpass: "password", newpass: 123456 })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not update the user - requester is not the user being updated", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: activeUser2_CP.userid, oldpass: "password", newpass: "someotherpass" })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(403502);
        done();
      });
    });
    it("should not update the user - incorrect oldpass", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(activeUser2_CP)
      .expect(200)
      .then(function(res)
      {
        activeUser2_CP.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post("/user/update")
        .set("Authorization", activeUser2_CP.accTkn)
        .send({ userid: activeUser2_CP.userid, oldpass: "password", newpass: "someotherpass" });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(401509);
        done();
      })
      .catch(done);
    });
    it("should update the user password", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", activeUser2_CP.accTkn)
      .send({ userid: activeUser2_CP.userid, oldpass: "P@55word", newpass: "password" })
      .expect(200)
      .then(function(res)
      {
        expect(res.body[ 0 ].userid).to.equal(activeUser2_CP.userid);
        done();
      })
      .catch(done);
    });
    it("should not expire the password - requester is the user herself", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ userid: userBdAdmin.userid, passexpired: true })
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400052);
        done();
      });
    });
    it("should expire the password - requester is the super admin", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", superUser.accTkn)
      .send({ userid: userBdAdmin.userid, passexpired: true })
      .then(function(res)
      {
        expect(res.body[ 0 ].passexpired).to.equal(true);
        return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send({ emailid: userBdAdmin.emailid, password: userBdAdmin.password });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(401508);
        done();
      })
      .catch(done);
    });
  });

  describe("#getdetail()", function()
  {
    var user_WP_fields =
    {
      "userids": 11
    };

    var user_WP_fields_2 =
    {
      "userids": []
    };

    var user_WP_userids =
    {
      "userids": [ 1, 2, 88 ]
    };

    var user_WP_userid =
    {
      "userids": [ 89 ]
    };

    var user_WP_11userids =
    {
      "userids": [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ]
    };

    var user_WP_value =
    {
      "userids": [ 1, 2, "salt" ]
    };

    var user_CP_userids =
    {
      "userids": [ 2, 3 ]
    };

    var user_CP_userid =
    {
      "userids": [ 4 ]
    };

    it("should return error(input should be an array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_fields)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(input should not be an empty array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_fields_2)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(incorrect field value) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_value)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(non existent userid multiple) - 400104", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_userids)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });

    it("should return error(non existent userid single) - 400104", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_userid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });

    it("should return error more than 10 userids - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_11userids)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should get user details multiple", function(done)
    {
      user_CP_userids.userids = [ user1.userid, user2.userid ];
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_userids)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.lengthOf(2);
        done();
      });
    });

    it("should get user details single", function(done)
    {
      user_CP_userid.userids = [ userBdAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/user/getdetail")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_userid)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.lengthOf(1);
        done();
      });
    });
  });

  describe("#getstatus()", function()
  {
    var user_WP_fields =
    {
      "userids": 11
    };

    var user_WP_fields_2 =
    {
      "userids": []
    };

    var user_WP_userids =
    {
      "userids": [ 1, 2, 88 ]
    };

    var user_WP_userid =
    {
      "userids": [ 89 ]
    };

    var user_WP_value =
    {
      "userids": [ 1, 2, "salt" ]
    };

    var user_CP_userids =
    {
      "userids": [ 2, 3 ]
    };

    var user_CP_userid =
    {
      "userids": [ 4 ]
    };

    it("should return error(input should be an array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_fields)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(input should not be an empty array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_fields_2)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(incorrect field value) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_value)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(non existent userid multiple) - 400104", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_userids)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });

    it("should return error(non existent userid single) - 400104", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_userid)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });

    it("should get user details multiple", function(done)
    {
      user_CP_userids.userids = [ user1.userid, user2.userid ];
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_userids)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.lengthOf(2);
        done();
      });
    });

    it("should get user status single", function(done)
    {
      user_CP_userid.userids = [ userBdAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/user/getstatus")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_userid)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.lengthOf(1);
        done();
      });
    });
  });

  describe("#activation()", function()
  {
    var activeUser_1 =
    {
      "fullname": "panchwatester rat",
      "username":"T5st.cat5",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient5.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_2 =
    {
      "fullname": "chataatester rat",
      "username":"T3st.cat6",
      "password":"P@55word",
      "emailid":"tester.rat@testclient6.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_3 =
    {
      "fullname": "satwatester rat",
      "username":"T3st.cat7",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient7.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_4 =
    {
      "fullname": "athwantester rat",
      "username":"T3st.cat8",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient8.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_5 =
    {
      "fullname": "nowatester rat",
      "username":"T3st.cat9",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient9.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_6 =
    {
      "fullname": "dushwatester rat",
      "username":"T3st.cat10",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient10.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_7 =
    {
      "fullname": "gyarwantester rat",
      "username":"T3st.cat11",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient11.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_8 =
    {
      "fullname": "pandarwantester rat",
      "username":"T3st.cat15",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient15.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeUser_9 =
    {
      "fullname":"solwantester rat",
      "username":"T3st.cat16",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient16.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };
    var activeUser_10 =
    {
      "fullname":"satarwantester rat",
      "username":"T3st.cat17",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient17.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };
    var activeUser_11 =
    {
      "fullname": "atharwatester rat",
      "username":"T3st.cat18",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient18.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };
    var activeUser_12 =
    {
      "fullname": "uneeswatester rat",
      "username":"T3st.cat19",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient19.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };
    var activeUser_13 =
    {
      "fullname": "beswatester rat",
      "username":"T3st.cat20",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient20.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var inactiveUser_1 =
    {
      "fullname": "barwantester rat",
      "username":"T3st.cat12",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient12.com",
      "countrycode":"+92",
      "phonenumber":"9822387102",
      "isactive": false
    };

    var inactiveUser_2 =
    {
      "fullname": "terewantester rat",
      "username":"T3st.cat13",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient13.com",
      "countrycode":"+92",
      "phonenumber":"9822387102",
      "isactive": false
    };

    var inactiveUser_3 =
    {
      "fullname": "chodwantester rat",
      "username":"T3st.cat14",
      "password":"P@55word",
      "emailid":"tester5.rat@testclient14.com",
      "countrycode":"+92",
      "phonenumber":"9822387102"
    };

    var activeBd =
    {
      "bdname": "Business-Department.3",
      "isactive":true
    };
    var inactiveBd =
    {
      "bdname": "Business-Department.4",
      "isactive":true
    };

    var activeCb = {};

    var inactiveCb = {};

    var activeQueue =
    {
      "queuename": "Test-Queue.3",
      "queueemail": "testqueue3@test.com",
      "alertl1": "testqueue3alert1@test.com",
      "alertl2": "testqueue3alert2@test.com",
      "alertl3": "testqueue3alert3@test.com",
      "isactive": true
    };

    var inactiveQueue =
    {
      "queuename": "Test-Queue.4",
      "queueemail": "testqueue4@test.com",
      "alertl1": "testqueue4alert1@test.com",
      "alertl2": "testqueue4alert2@test.com",
      "alertl3": "testqueue4alert3@test.com",
      "isactive": false
    };

    var user_WP_fields_1 =
    {
      "userids": 2
    };

    var user_WP_fields_2 =
    {
      "userids": [ 1, 2, 88 ]
    };

    var user_WP_fields_3 =
    {
      "userids": [ 4 ],
      "action": "great"
    };

    var user_WP_deactivate =
    {
      "action": false
    };

    var user_WP_activate =
    {
      "action": true
    };

    var user_CP_activate =
    {
      "action": true
    };

    var user_CP_deactivate =
    {
      "action": false
    };

    before(function(done)
    {
      request(sails.hooks.http.app)
      .post('/user/add')
      .set('Authorization', userBdAdmin.accTkn)
      .send(activeUser_1)
      .then(function(res)
      {
        activeUser_1.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', userBdAdmin.accTkn)
        .send(activeUser_2);
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 2 active user");
        }
        else
        {
          activeUser_2.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_3);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 3 active user");
        }
        else
        {
          activeUser_3.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_4);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 4 active user");
        }
        else
        {
          activeUser_4.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_5);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 5 active user");
        }
        else
        {
          activeUser_5.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_6);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 6 active user");
        }
        else
        {
          activeUser_6.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_7);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 7 active user");
        }
        else
        {
          activeUser_7.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_8);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 8 active user");
        }
        else
        {
          activeUser_8.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_9);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 9 active user");
        }
        else
        {
          activeUser_9.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_10);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 10 active user");
        }
        else
        {
          activeUser_10.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_11);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 11 active user");
        }
        else
        {
          activeUser_11.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_12);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 12 active user");
        }
        else
        {
          activeUser_12.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(activeUser_13);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 12 active user");
        }
        else
        {
          activeUser_13.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(inactiveUser_1);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 1 inactive user");
        }
        else
        {
          inactiveUser_1.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(inactiveUser_2);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 2 inactive user");
        }
        else
        {
          inactiveUser_2.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/user/add')
          .set('Authorization', userBdAdmin.accTkn)
          .send(inactiveUser_3);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 3 inactive User");
        }
        else
        {
          inactiveUser_3.userid = res.body.userid;
          return request(sails.hooks.http.app)
          .post('/bd/add')
          .set('Authorization', userSuper.accTkn)
          .send(activeBd);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 3 inactive User");
        }
        else
        {
          activeBd.bdid = res.body.bdid;
          return request(sails.hooks.http.app)
          .post('/bd/add')
          .set('Authorization', userSuper.accTkn)
          .send(inactiveBd);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create Business Department");
        }
        else
        {
          inactiveBd.bdid = res.body.bdid;
          return request(sails.hooks.http.app)
          .post('/bd/assign')
          .set('Authorization', userSuper.accTkn)
          .send({ "bdid": activeBd.bdid, "admins": [ activeUser_3.userid, activeUser_5.userid, inactiveUser_3.userid ], "assign": true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create Business Department");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/bd/assign')
          .set('Authorization', userSuper.accTkn)
          .send({ "bdid": inactiveBd.bdid, "admins": [ activeUser_13.userid ], "assign": true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't assign admins to BD");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/bd/activation')
          .set('Authorization', userSuper.accTkn)
          .send({ bdids: [ activeBd.bdid ], action: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't activate bd");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(activeUser_7);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't sign-in 7th User");
        }
        else
        {
          activeUser_7.accTkn = res.body.acctoken;
          activeUser_7.refTkn = res.body.reftoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(activeUser_9);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't sign-in 7th User");
        }
        else
        {
          activeUser_9.accTkn = res.body.acctoken;
          activeUser_9.refTkn = res.body.reftoken;
          return request(sails.hooks.http.app)
          .post('/auth/signin')
          .send(activeUser_3);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't sign-in 3rd User");
        }
        else
        {
          activeUser_3.accTkn = res.body.acctoken;
          activeUser_3.refTkn = res.body.reftoken;
          return request(sails.hooks.http.app)
          .post('/cb/add')
          .set('Authorization', activeUser_3.accTkn)
          .send({ bdid: activeBd.bdid, ownerid: activeUser_7.userid });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 1st CB for BD 1");
        }
        else
        {
          orgApproval1 = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', activeUser_7.accTkn)
          .send({ approvalid: orgApproval1.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't activate the CB 3");
        }
        else
        {
          activeCb = res.body[ 0 ];
          activeQueue.cbid = activeCb.cbid;
          activeQueue.queueownerid = activeUser_4.userid;
          activeQueue.changemanagerid = activeUser_8.userid;
          activeQueue.srtmanagerid = activeUser_8.userid;
          activeQueue.incidentmanagerid = activeUser_8.userid;
          return request(sails.hooks.http.app)
          .post('/queue/add')
          .set('Authorization', activeUser_3.accTkn)
          .send(activeQueue);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add active queue");
        }
        else
        {
          orgApproval1 = res.body[ 1 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', activeUser_7.accTkn)
          .send({ approvalid: orgApproval1.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve active queue");
        }
        else
        {
          activeQueue = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/queue/memberassoc')
          .set('Authorization', activeUser_3.accTkn)
          .send({ "queueid": activeQueue.queueid, "members": [ activeUser_1.userid, activeUser_6.userid, inactiveUser_3.userid ], "associate": true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't associate members to active queue 1");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/queue/activation')
          .set('Authorization', activeUser_3.accTkn)
          .send({ queueids: [ activeQueue.queueid ], action: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't activate queue");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/cb/add')
          .set('Authorization', activeUser_3.accTkn)
          .send({ bdid: activeBd.bdid, ownerid: activeUser_9.userid });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't create 2 CB for BD 1");
        }
        else
        {
          orgApproval1 = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', activeUser_9.accTkn)
          .send({ approvalid: orgApproval1.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve the inactive CB");
        }
        else
        {
          inactiveCb = res.body[ 0 ];
          inactiveQueue.cbid = inactiveCb.cbid;
          inactiveQueue.queueownerid = activeUser_10.userid;
          inactiveQueue.changemanagerid = activeUser_11.userid;
          inactiveQueue.srtmanagerid = activeUser_11.userid;
          inactiveQueue.incidentmanagerid = activeUser_11.userid;
          return request(sails.hooks.http.app)
          .post('/queue/add')
          .set('Authorization', activeUser_3.accTkn)
          .send(inactiveQueue);
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't add inactive queue");
        }
        else
        {
          orgApproval1 = res.body[ 1 ];
          return request(sails.hooks.http.app)
          .post('/orgapproval/approve')
          .set('Authorization', activeUser_9.accTkn)
          .send({ approvalid: orgApproval1.approvalid, approved: true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve active queue");
        }
        else
        {
          inactiveQueue = res.body[ 0 ];
          return request(sails.hooks.http.app)
          .post('/queue/memberassoc')
          .set('Authorization', activeUser_3.accTkn)
          .send({ "queueid": inactiveQueue.queueid, "members": [ activeUser_12.userid ], "associate": true });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't approve active queue");
        }
        else
        {
          return request(sails.hooks.http.app)
          .post('/cb/activation')
          .set('Authorization', activeUser_3.accTkn)
          .send({ "cbids": [ inactiveCb.cbid ], "action": false });
        }
      })
      .then(function(res)
      {
        if(res.status !== 200)
        {
          throw new Error("Couldn't associate queue members");
        }
        else
        {
          done();
        }
      })
      .catch(done);
    });

    it("should return error(input should be an array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_fields_1)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(input should have action field) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_fields_2)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(action field should be a boolean) - 400051", function(done)
    {
      user_WP_fields_3.userids = [ activeUser_1.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_fields_3)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(input is not a number) - 400051", function(done)
    {
      user_WP_deactivate.userids = [ 1, 2, 'a' ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should return error(non existent userid multiple) - 400104", function(done)
    {
      user_WP_activate.userids = [ 1, 2, 88 ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_activate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });

    it("should return error(non existent userid single) - 400104", function(done)
    {
      user_WP_activate.userids = [ 88 ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_activate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });

    it("action:true should return an error(all userids are not inactive) - 400102", function(done)
    {
      user_WP_activate.userids = [ inactiveUser_1.userid, inactiveUser_2.userid, activeUser_1.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_activate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400102);
        done();
      });
    });

    it("action:false should return an error(all userids are not active) - 400103", function(done)
    {
      user_WP_deactivate.userids = [ activeUser_1.userid, activeUser_2.userid, inactiveUser_1.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400103);
        done();
      });
    });

    it("action:true should activate user", function(done)
    {
      user_CP_activate.userids = [ inactiveUser_1.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_activate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("userid").and.equal(inactiveUser_1.userid);
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
        done();
      });
    });

    it("action:false should deactivate user", function(done)
    {
      user_CP_deactivate.userids = [ inactiveUser_1.userid, inactiveUser_3.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_deactivate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        var user = _.find(res.body, { "userid": inactiveUser_1.userid });
        expect(user).to.to.have.property("isactive").and.equal(false);
        done();
      });
    });

    it("action:false should return an error(user owns an active cb) - 400107", function(done)
    {
      user_WP_deactivate.userids = [ activeUser_1.userid, activeUser_2.userid, activeUser_7.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400107);
        done();
      });
    });

    it("action:false should return error(user is only active admin of an active bd) - 400158", function(done)
    {
      user_WP_deactivate.userids = [ activeUser_1.userid, activeUser_3.userid, activeUser_5.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400158);
        done();
      });
    });
    it("action:false should return error(user is only active member of an active queue) - 400260", function(done)
    {
      user_WP_deactivate.userids = [ activeUser_1.userid, activeUser_6.userid, activeUser_5.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400260);
        done();
      });
    });
    it("action:false should return error(user is not a bd admin or Super user) - 403502", function(done)
    {
      user_WP_deactivate.userids = [ activeUser_1.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', activeUser_9.accTkn)
      .send(user_WP_deactivate)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("action:false should return error(user operate on itself) - 403502", function(done)
    {
      user_WP_deactivate.userids = [ userBdAdmin.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("action:false should return error(user is a super User) - 403502", function(done)
    {
      user_WP_deactivate.userids = [ userSuper.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_WP_deactivate)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("action:false should deactivate user - user owns inactive CB", function(done)
    {
      user_CP_deactivate.userids = [ activeUser_9.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_deactivate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        var user = _.find(res.body, { "userid": activeUser_9.userid });
        expect(user).to.to.have.property("isactive").and.equal(false);
        done();
      });
    });
    it("action:false should deactivate user - user owns inactive Queue", function(done)
    {
      user_CP_deactivate.userids = [ activeUser_10.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_deactivate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        var user = _.find(res.body, { "userid": activeUser_10.userid });
        expect(user).to.to.have.property("isactive").and.equal(false);
        done();
      });
    });
    it("action:false should deactivate user - user manager of an inactive Queue", function(done)
    {
      user_CP_deactivate.userids = [ activeUser_11.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_deactivate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        var user = _.find(res.body, { "userid": activeUser_11.userid });
        expect(user).to.to.have.property("isactive").and.equal(false);
        done();
      });
    });
    it("action:false should deactivate user - user member of an inactive Queue", function(done)
    {
      user_CP_deactivate.userids = [ activeUser_12.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_deactivate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        var user = _.find(res.body, { "userid": activeUser_12.userid });
        expect(user).to.to.have.property("isactive").and.equal(false);
        done();
      });
    });
    it("action:false should deactivate user - user is the admin of an inactive BD", function(done)
    {
      user_CP_deactivate.userids = [ activeUser_13.userid ];
      request(sails.hooks.http.app)
      .post("/user/activation")
      .set('Authorization', userBdAdmin.accTkn)
      .send(user_CP_deactivate)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        var user = _.find(res.body, { "userid": activeUser_13.userid });
        expect(user).to.to.have.property("isactive").and.equal(false);
        done();
      });
    });
  });
  describe("#search()", function()
  {
    var user_WP_nonExistentField_1 =
    {
      "searched":  { "username": "T3st.User1%" },
      "result": [ "username", "emailid" ],
      "orderby": "firstname"
    };
    var user_WP_nonExistentField_2 =
    {
      "search":  { "username": "T3st.User1%" },
      "results": [ "firstname", "lastname", "username", "emailid" ],
      "orderby": "firstname"
    };
    var user_WP_nonExistentField_3 =
    {
      "search":  { "username": "T3st.User1%" },
      "result": [ "firstname", "lastname", "username", "emailid" ],
      "ordersby": "firstname"
    };
    var user_WP_nonExistentColumn_1 =
    {
      "search":  { "usersname": "T3st.User1%" },
      "result": [ "firstname", "lastname", "username", "emailid" ],
      "orderby": "firstname"
    };
    var user_WP_nonExistentColumn_2 =
    {
      "search":  { "username": "T3st.User1%" },
      "result": [ "firstname", "lastname", "middlename", "emailid" ],
      "orderby": "firstname"
    };
    var user_WP_nonExistentColumn_3 =
    {
      "search":  { "usersname": "T3st.User1%" },
      "result": [ "firstname", "lastname", "username", "emailid" ],
      "orderby": "emailids"
    };
    var user_CP =
    {
      "search":  { "username": "T%" },
      "result": [ "username", "emailid" ],
      "orderby": "username"
    };
    it("should return an error(incorrect parameters - Non ExistentField1) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_nonExistentField_1)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should return an error(incorrect parameters - Non ExistentField2) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_nonExistentField_2)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should return an error(incorrect parameters - Non ExistentField3) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_nonExistentField_3)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });
    it("should return an error(incorrect parameters - Non ExistentColumn1) - 400053", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_nonExistentColumn_1)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400053);
        done();
      });
    });
    it("should return an error(incorrect parameters - Non ExistentColumn2) - 400053", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_nonExistentColumn_2)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400053);
        done();
      });
    });
    it("should return an error(incorrect parameters - Non ExistentColumn3) - 400053", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_WP_nonExistentColumn_3)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400053);
        done();
      });
    });
    it("should search for the users", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/search")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.be.above(0);
        expect(Object.keys(res.body[ 1 ][ 0 ]).length).to.equal(2);
        done();
      });
    });
  });
  describe("#makesuperuser()", function()
  {
    var user_CP_make =
    {
      "action": true
    };
    var newSU =
    {
      "fullname": "Naya.bakra",
      "username": "naya.bakra",
      "password": "password",
      "emailid": "naya.bakra@testdomain.com"
    };
    it("should return an error(not authorized) - 403502", function(done)
    {
      user_CP_make.userid = userBdAdmin.userid;
      request(sails.hooks.http.app)
      .post("/user/makesuperuser")
      .set("Authorization", userBdAdmin.accTkn)
      .send(user_CP_make)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("should return an error(cannot operate on self) - 403502", function(done)
    {
      user_CP_make.userid = userSuper.userid;
      request(sails.hooks.http.app)
      .post("/user/makesuperuser")
      .set("Authorization", userSuper.accTkn)
      .send(user_CP_make)
      .expect(403)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(403502);
        done();
      });
    });
    it("should return an error(user doesnt exist) - 400104", function(done)
    {
      user_CP_make.userid = 88;
      request(sails.hooks.http.app)
      .post("/user/makesuperuser")
      .set("Authorization", userSuper.accTkn)
      .send(user_CP_make)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400104);
        done();
      });
    });
    it("should make the user SU and signout the new super user", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", userSuper.accTkn)
      .send(newSU)
      .then(function(res)
      {
        expect(res.body.fullname).to.equal(newSU.fullname);
        newSU.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(newSU);
      })
      .then(function(res)
      {
        expect(res.body.user.fullname).to.equal(newSU.fullname);
        newSU.accTkn = res.body.acctoken;
        newSU.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post("/user/getdetail")
        .set("Authorization", newSU.accTkn)
        .send({ userids: [ newSU.userid ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].fullname).to.equal(newSU.fullname);
        return request(sails.hooks.http.app)
        .post("/user/makesuperuser")
        .set("Authorization", userSuper.accTkn)
        .send({ userid: newSU.userid, action: true });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].userid).to.equal(newSU.userid);
        expect(res.body[ 0 ]).to.have.property("issuperuser").and.equal(true);
        return request(sails.hooks.http.app)
        .post("/user/getdetail")
        .set("Authorization", newSU.accTkn)
        .send({ userids: [ newSU.userid ] });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(401503);
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(newSU);
      })
      .then(function(res)
      {
        newSU.accTkn = res.body.acctoken;
        newSU.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post("/user/getdetail")
        .set("Authorization", newSU.accTkn)
        .send({ userids: [ newSU.userid ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].fullname).to.equal(newSU.fullname);
        done();
      })
      .catch(done);
    });
    it("should remove the user's SU privilege and signout the new super user", function(done)
    {
      request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(newSU)
      .then(function(res)
      {
        expect(res.body.user.fullname).to.equal(newSU.fullname);
        newSU.accTkn = res.body.acctoken;
        newSU.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post("/user/getdetail")
        .set("Authorization", newSU.accTkn)
        .send({ userids: [ newSU.userid ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].fullname).to.equal(newSU.fullname);
        return request(sails.hooks.http.app)
        .post("/user/makesuperuser")
        .set("Authorization", userSuper.accTkn)
        .send({ userid: newSU.userid, action: false });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].userid).to.equal(newSU.userid);
        expect(res.body[ 0 ]).to.have.property("issuperuser").and.equal(false);
        return request(sails.hooks.http.app)
        .post("/user/getdetail")
        .set("Authorization", newSU.accTkn)
        .send({ userids: [ newSU.userid ] });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(401503);
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(newSU);
      })
      .then(function(res)
      {
        newSU.accTkn = res.body.acctoken;
        newSU.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post("/user/getdetail")
        .set("Authorization", newSU.accTkn)
        .send({ userids: [ newSU.userid ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].fullname).to.equal(newSU.fullname);
        done();
      })
      .catch(done);
    });

    it("should signout an user on being dissociated from BD administratorship", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send({ emailid: 'barney.rubble@bedrock.com', password: superUser.password })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
        superUser.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post("/user/add")
        .set("Authorization", superUser.accTkn)
        .send(tempBDadmin);
      })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
        tempBDadmin.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send({ bdid: 2, admins: [ tempBDadmin.userid ], assign: true });
      })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send(tempBDadmin);
      })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
        tempBDadmin.accTkn = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post("/user/list")
        .set("Authorization", tempBDadmin.accTkn)
        .send({ page: 1, limit: 10 });
      })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.accTkn)
        .send({ bdid: 2, admins: [ tempBDadmin.userid ], assign: false });
      })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
        return request(sails.hooks.http.app)
        .post("/user/list")
        .set("Authorization", tempBDadmin.accTkn)
        .send({ page: 1, limit: 10 });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(401503);
        done();
      })
      .catch(done);
    });
    it("updating the superuser emailid back to original", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/update")
      .set("Authorization", superUser.accTkn)
      .send({ userid: superUser.userid, emailid: superUser.emailid })
      .expect(200)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body[ 0 ].emailid).to.equal(superUser.emailid);
        done();
      });
    });
  });
  describe("setconfig()", function()
  {
    it("should not set the config(input not an array) - 400051", function(done)
    {
     request(sails.hooks.http.app)
     .post("/user/setconfig")
     .set("Authorization", superUser.accTkn)
     .send({ settings: { config: "timezone", value: "IST", mode: enums.configTypes.GLOBAL } })
     .expect(400)
     .end(function(err, res)
     {
       if(err)
         return done(err);
       expect(res.body.code).to.equal(400051);
       done();
     });
    });
    it("should not set the config(incorrect mode) - 400051", function(done)
    {
     request(sails.hooks.http.app)
     .post("/user/setconfig")
     .set("Authorization", superUser.accTkn)
     .send({ settings: [ { config: "timezone", value: "IST", mode: 3 } ] })
     .expect(400)
     .end(function(err, res)
     {
       if(err)
         return done(err);
       expect(res.body.code).to.equal(400051);
       done();
     });
    });
    it("should not set the config(no config sent) - 400051", function(done)
    {
     request(sails.hooks.http.app)
     .post("/user/setconfig")
     .set("Authorization", superUser.accTkn)
     .send({ settings: [ { value: "IST", mode: enums.configTypes.GLOBAL } ] })
     .expect(400)
     .end(function(err, res)
     {
       if(err)
         return done(err);
       expect(res.body.code).to.equal(400051);
       done();
     });
    });
    it("should not set the config(no value sent) - 400051", function(done)
    {
      request(sails.hooks.http.app)
     .post("/user/setconfig")
     .set("Authorization", superUser.accTkn)
     .send({ settings: [ { config: "timezone", mode: enums.configTypes.GLOBAL } ] })
     .expect(400)
     .end(function(err, res)
     {
       if(err)
         return done(err);
       expect(res.body.code).to.equal(400051);
       done();
     });
    });
    it("should not set the config(global config cannot be set by non-superuser) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/setconfig")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ settings: [ { config: "timezone", value: "IST", mode: enums.configTypes.GLOBAL } ] })
      .expect(403)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(403502);
        done();
      });
    });
    it("should set the global config", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/setconfig")
      .set("Authorization", superUser.accTkn)
      .send({ settings: [ { config: "timezone", value: "GMT", mode: enums.configTypes.GLOBAL } ] })
      .expect(200)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        done();
      });
    });
    it("should invalidate cache on global config set", function(done)
    {
      var cacheTimeStamp;
      request(sails.hooks.http.app)
      .post("/clientcache/getalldata")
      .set("Authorization", superUser.accTkn)
      .then(function(res)
      {
        cacheTimeStamp = res.body.timestamp;
        return request(sails.hooks.http.app)
        .post("/clientcache/isvalid")
        .set("Authorization", superUser.accTkn)
        .send({ timestamp: cacheTimeStamp });
      })
      .then(function(res)
      {
        expect(res.body).to.equal(true);
        return request(sails.hooks.http.app)
        .post("/user/setconfig")
        .set("Authorization", superUser.accTkn)
        .send({ settings: [ { config: "homescreen", value: "Sports", mode: enums.configTypes.GLOBAL } ] });
      })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post("/clientcache/isvalid")
        .set("Authorization", superUser.accTkn)
        .send({ timestamp: cacheTimeStamp });
      })
      .then(function(res)
      {
        expect(res.body).to.not.equal(true);
        done();
      })
      .catch(done);
    });
    it("should set the user config", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/setconfig")
      .set("Authorization", superUser.accTkn)
      .send({ settings: [ { config: "timezone", value: "IST", mode: enums.configTypes.USER } ] })
      .expect(200)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        done();
      });
    });
  });
  describe("getconfig()", function()
  {
    it("should not get the config(no config provided) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getconfig")
      .set("Authorization", superUser.accTkn)
      .send({ })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not get the config(configs is not an array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getconfig")
      .set("Authorization", superUser.accTkn)
      .send({ config: "boresize" })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not get the config(no config available) - 400112", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/getconfig")
      .set("Authorization", superUser.accTkn)
      .send({ configs: [ "hemisphere" ] })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400112);
        done();
      });
    });
    it("should get the user config(user config not present, global config present - gets the global value)", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/setconfig")
      .set("Authorization", superUser.accTkn)
      .send({ settings: [ { config: "defaultview", value: "incident", mode: enums.configTypes.GLOBAL } ] })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post("/user/getconfig")
        .set("Authorization", superUser.accTkn)
        .send({ configs: [ "defaultview" ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].value).to.equal("incident");
        done();
      })
      .catch(done);
    });
    it("should get the user config(both configs present - gets the user value)", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/setconfig")
      .set("Authorization", superUser.accTkn)
      .send({ settings: [ { config: "defaultview", value: "approval", mode: enums.configTypes.USER } ] })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post("/user/getconfig")
        .set("Authorization", superUser.accTkn)
        .send({ configs: [ "defaultview" ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].value).to.equal("approval");
        done();
      })
      .catch(done);
    });
  });
  describe("resetconfig()", function()
  {
    it("should not remove the config(no input configs) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/resetconfig")
      .set("Authorization", superUser.accTkn)
      .send({ cfglist: [ 'length', 'width' ] })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not remove the config(config list is not an array) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/resetconfig")
      .set("Authorization", superUser.accTkn)
      .send({ configs: "length" })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not remove the config(config list is empty) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/resetconfig")
      .set("Authorization", superUser.accTkn)
      .send({ configs: [] })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });
    it("should not remove the config(no user config present for the user) - 400112", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/resetconfig")
      .set("Authorization", userBdAdmin.accTkn)
      .send({ configs: [ "defaultview" ] })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400112);
        done();
      });
    });
    it("should not remove the config(one of the configs is not present) - 400112", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/resetconfig")
      .set("Authorization", superUser.accTkn)
      .send({ configs: [ "defaultview", "defaultaltitude" ] })
      .expect(400)
      .end(function(err, res)
      {
        if(err)
          return done(err);
        expect(res.body.code).to.equal(400112);
        done();
      });
    });
    it("should remove the config", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/resetconfig")
      .set("Authorization", superUser.accTkn)
      .send({ configs: [ "defaultview" ] })
      .then(function(res)
      {
        expect(res.statusCode).to.equal(200);
        return request(sails.hooks.http.app)
        .post("/user/getconfig")
        .set("Authorization", superUser.accTkn)
        .send({ configs: [ "defaultview" ] });
      })
      .then(function(res)
      {
        expect(res.body[ 0 ].value).to.equal("incident");
        expect(res.body[ 0 ].mode).to.equal(enums.configTypes.GLOBAL);
        done();
      });
    });
  });
});
