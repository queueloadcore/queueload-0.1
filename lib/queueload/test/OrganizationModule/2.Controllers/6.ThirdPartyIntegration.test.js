/* FileName: ThirdPartyIntegration.test.js
* Description: This file defines all the unit test cases for the Integration APIs in Company controller
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 28/08/2017          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var enums = require('../../../api/resources/Enums.js');

describe("3rd Party Integration Tests", function()
{
  var company =
  {
    "accountid": "qlodtest",
    "name": "Wayne Enterprises",
    "address": "Gotham City",
    "accntadmin": 1,
    "globalconfig": {}
  };

  var superUser =
  {
    "firstname": "Super",
    "lastname": "User",
    "fullname": "Super User",
    "username": "administrator",
    "password": "password",
    "emailid": "administrator@testdomain.com"
  };

  var allPurposeUser =
  {
    "firstname": "integration",
    "lastname": "user",
    "fullname":"integration user",
    "username": "integration.user",
    "password": "password",
    "emailid": "integration.user@testdomain.com"
  };

  var extraOperator =
  {
    "firstname": "extra",
    "lastname": "operator",
    "fullname":"extra operator",
    "username": "extra.operator",
    "password": "password",
    "emailid": "extra.operator@testdomain.com"
  };

  var businessDepartment =
  {
    "bdname": "3pintegrationBd",
    "isactive": true
  };

  var chargeBackCode = {};

  var intQueue =
  {
    "queuename":"integration.queue",
    "queueemail":"integration.queue@testdomain.com"
  };

  var serviceOne =
  {
    "servicename": "firstIntService"
  };

  before("Staging Test Enviroment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send(superUser);
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin Superuser");
      }
      else
      {
        superUser.userid = res.body.user.userid;
        superUser.acctoken = res.body.acctoken;
        superUser.reftoken = res.body.reftoken;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.acctoken)
        .send(allPurposeUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create all purpose User");
      }
      else
      {
        allPurposeUser.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post('/user/add')
        .set('Authorization', superUser.acctoken)
        .send(extraOperator);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create extra operator");
      }
      else
      {
        extraOperator.userid = res.body.userid;
        return request(sails.hooks.http.app)
        .post("/bd/add")
        .set("Authorization", superUser.acctoken)
        .send(businessDepartment);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create businessDepartment");
      }
      else
      {
        businessDepartment.bdid = res.body.bdid;
        return request(sails.hooks.http.app)
        .post("/bd/assign")
        .set("Authorization", superUser.acctoken)
        .send({ bdid: businessDepartment.bdid, admins: [ allPurposeUser.userid ], assign: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign firstBDAdmin to firstActiveBD");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/bd/activation")
        .set("Authorization", superUser.acctoken)
        .send({ bdids: [ businessDepartment.bdid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate the BD");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(allPurposeUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin 1st normal user");
      }
      else
      {
        allPurposeUser.acctoken = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post('/cb/add')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ bdid: businessDepartment.bdid, ownerid: allPurposeUser.userid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't create chargeBackCode");
      }
      else
      {
        var orgApproval = res.body[ 0 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve the CB firstActiveCb");
      }
      else
      {
        chargeBackCode = res.body[ 0 ];
        intQueue.cbid = chargeBackCode.cbid;
        return request(sails.hooks.http.app)
        .post('/queue/add')
        .set('Authorization', allPurposeUser.acctoken)
        .send(intQueue);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add intQueue");
      }
      else
      {
        var orgApproval = res.body[ 1 ];
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ approvalid: orgApproval.approvalid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue creation");
      }
      else
      {
        intQueue = res.body[ 0 ];
        serviceOne.cbid = chargeBackCode.cbid;
        return request(sails.hooks.http.app)
        .post('/queue/memberassoc')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ queueid: intQueue.queueid, members: [ allPurposeUser.userid, extraOperator.userid ], associate: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't associate members to queue intQueue");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(allPurposeUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin 1st normal user");
      }
      else
      {
        allPurposeUser.acctoken = res.body.acctoken;
        return request(sails.hooks.http.app)
        .post('/queue/activation')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ queueids: [ intQueue.queueid ], action: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't activate queue 1");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/service/add")
        .set("Authorization", allPurposeUser.acctoken)
        .send(serviceOne);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't add serviceOne");
      }
      else
      {
        serviceOne = res.body;
        return request(sails.hooks.http.app)
        .post('/service/queueassign')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ serviceid: serviceOne.serviceid, supportqueue: intQueue.queueid, assign: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't assign queue to service");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/orgapproval/approve')
        .set('Authorization', allPurposeUser.acctoken)
        .send({ approvalid: res.body.approvalid, serviceid: serviceOne.serviceid, approved: true });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve queue to service assignment");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/service/next")
        .set("Authorization", allPurposeUser.acctoken)
        .send(serviceOne);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't move serviceOne");
      }
      else
      {
        return Serviceapproval.findOne({ approverserviceid: res.body.serviceid, type: "state", isactive: true });
      }
    })
    .then(function(approval)
    {
      if(!approval)
      {
        throw new Error("Couldn't find service approval");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/service/approve")
        .set("Authorization", allPurposeUser.acctoken)
        .send({ type: "state", approvalid: approval.approvalid, isapproved: true, serviceid: serviceOne.serviceid });
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't approve service state transition");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post('/auth/signin')
        .send(allPurposeUser);
      }
    })
    .then(function(res)
    {
      if(res.status !== 200)
      {
        throw new Error("Couldn't signin 1st normal user");
      }
      else
      {
        allPurposeUser.acctoken = res.body.acctoken;
        done();
      }
    })
    .catch(done);
  });
  describe("#getintegrationinfo()", function()
  {
    it("should not get integration info(requester is not superUser) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', allPurposeUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH })
      .then(function(res)
      {
        expect(res.body.code).to.equal(403502);
        done();
      })
      .catch(done);
    });
    it("should not get integration info(request doesn't contain integrationtype) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationcolor: "red" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not get integration info(integrationtype is a non positive number) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: -23 })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not get integration info(integrationtype is outside the limit) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.LIMIT + 1 })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for CLOUDWATCH", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH })
      .then(function(res)
      {
        expect(res.body.endpoint).to.exist;
        expect(res.body.integrationtype).to.equal(enums.integrationService.CLOUDWATCH);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for SENTRY", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.SENTRY })
      .then(function(res)
      {
        expect(res.body.endpoint).to.exist;
        expect(res.body.integrationtype).to.equal(enums.integrationService.SENTRY);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for WEAVED", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.WEAVED })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.WEAVED);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for NAGIOS", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.NAGIOS })
      .then(function(res)
      {
        expect(res.body.endpoint).to.exist;
        expect(res.body.integrationtype).to.equal(enums.integrationService.NAGIOS);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for SLACK", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.SLACK })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.SLACK);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for LOGGLY", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.LOGGLY })
      .then(function(res)
      {
        expect(res.body.endpoint).to.exist;
        expect(res.body.integrationtype).to.equal(enums.integrationService.LOGGLY);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for CUSTOM", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CUSTOM })
      .then(function(res)
      {
        expect(res.body.endpoint).to.exist;
        expect(res.body.integrationtype).to.equal(enums.integrationService.CUSTOM);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
    it("should get the integration info for AZURE", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.AZURE })
      .then(function(res)
      {
        expect(res.body.endpoint).to.exist;
        expect(res.body.integrationtype).to.equal(enums.integrationService.AZURE);
        expect(res.body.isintegrated).to.equal(false);
        done();
      })
      .catch(done);
    });
  });
  describe("#addintegration()", function()
  {
    it("should not add the integration(requester is not a superuser) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', allPurposeUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(403502);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(missing endpoint) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(missing ownerqueueid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(missing owneruserid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(missing integrationtype) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(user not a member of queue) - 400258", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: superUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400258);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(user not active) - 400103", function(done)
    {
      User.update({ userid: allPurposeUser.userid }, { isactive: false })
      .then(function()
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400103);
        return User.update({ userid: allPurposeUser.userid }, { isactive: true });
      })
      .then(function()
      {
        done();
      })
      .catch(done);
    });
    it("should not add the integration(queue not active) - 400253", function(done)
    {
      Queue.update({ queueid: intQueue.queueid }, { isactive: false })
      .then(function()
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400253);
        return Queue.update({ queueid: intQueue.queueid }, { isactive: true });
      })
      .then(function()
      {
        done();
      })
      .catch(done);
    });
    it("should not add the integration(malformed CW URL) - 400528", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400528);
        done();
      })
      .catch(done);
    });
    it("should add the CLOUDWATCH integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.CLOUDWATCH);
        done();
      })
      .catch(done);
    });
    it("should not add the CLOUDWATCH integration(already integrated) - 400527", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400527);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(malformed SENTRY URL) - 400528", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.SENTRY, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400528);
        done();
      })
      .catch(done);
    });
    it("should add the SENTRY integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.SENTRY })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.SENTRY, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.SENTRY);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(malformed NAGIOS URL) - 400528", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.NAGIOS, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400528);
        done();
      })
      .catch(done);
    });
    it("should add the NAGIOS integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.NAGIOS })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.NAGIOS, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.NAGIOS);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(malformed LOGGLY URL) - 400528", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.LOGGLY, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400528);
        done();
      })
      .catch(done);
    });
    it("should add the LOGGLY integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.LOGGLY })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.LOGGLY, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.LOGGLY);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(malformed CUSTOM URL) - 400528", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CUSTOM, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400528);
        done();
      })
      .catch(done);
    });
    it("should add the CUSTOM integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CUSTOM })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CUSTOM, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.CUSTOM);
        done();
      })
      .catch(done);
    });
    it("should not add the integration(malformed AZURE URL) - 400528", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/addintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.AZURE, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400528);
        done();
      })
      .catch(done);
    });
    it("should add the AZURE integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/getintegrationinfo')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.AZURE })
      .then(function(res)
      {
        return request(sails.hooks.http.app)
        .post('/company/addintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.AZURE, ownerqueueid: intQueue.queueid, owneruserid: allPurposeUser.userid, endpoint: res.body.endpoint });
      })
      .then(function(res)
      {
        expect(res.body.integrationtype).to.equal(enums.integrationService.AZURE);
        done();
      })
      .catch(done);
    });
  });
  describe("#updateintegration()", function()
  {
    it("should not update the integration(requester is not a superuser) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', allPurposeUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: extraOperator.userid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(403502);
        done();
      })
      .catch(done);
    });
    it("should not update the integration(payload contains endpoint) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: extraOperator.userid, endpoint: "URL" })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not update the integration(ownerqueueid is missing) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, owneruserid: extraOperator.userid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not update the integration(owneruserid is missing) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not update the integration(integrationtype is missing) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ owneruserid: extraOperator.userid, ownerqueueid: intQueue.queueid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not update the integration(user not a member of the queue) - 400258", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: superUser.userid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400258);
        done();
      })
      .catch(done);
    });
    it("should not upddate the integration(user not active) - 400103", function(done)
    {
      User.update({ userid: extraOperator.userid }, { isactive: false })
      .then(function()
      {
        return request(sails.hooks.http.app)
        .post('/company/updateintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: extraOperator.userid });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400103);
        return User.update({ userid: extraOperator.userid }, { isactive: true });
      })
      .then(function()
      {
        done();
      })
      .catch(done);
    });
    it("should not add the integration(queue not active) - 400253", function(done)
    {
      Queue.update({ queueid: intQueue.queueid }, { isactive: false })
      .then(function()
      {
        return request(sails.hooks.http.app)
        .post('/company/updateintegration')
        .set('Authorization', superUser.acctoken)
        .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: extraOperator.userid });
      })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400253);
        return Queue.update({ queueid: intQueue.queueid }, { isactive: true });
      })
      .then(function()
      {
        done();
      })
      .catch(done);
    });
    it("should update the CLOUDWATCH integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CLOUDWATCH, ownerqueueid: intQueue.queueid, owneruserid: extraOperator.userid })
      .then(function(res)
      {
        expect(res.body[ 0 ].config.owneruserid).to.equal(extraOperator.userid);
        done();
      })
      .catch(done);
    });
    it("should not update the WEAVED integration(not integrated) - 400526", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/updateintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.WEAVED, ownerqueueid: intQueue.queueid, owneruserid: extraOperator.userid })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400526);
        done();
      })
      .catch(done);
    });
  });
  describe("#removeintegration()", function()
  {
    it("should not remove the integration(requester is not superuser) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/removeintegration')
      .set('Authorization', allPurposeUser.acctoken)
      .send({ integrationtype: enums.integrationService.SENTRY })
      .then(function(res)
      {
        expect(res.body.code).to.equal(403502);
        done();
      })
      .catch(done);
    });
    it("should not remove the integration(missing integrationtype) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/removeintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationcolor: 'RED' })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not remove the integration(integrationtype is not a positive number) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/removeintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: -23 })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not remove the integration(integrationtype is outside the limit) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/removeintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.LIMIT + 1 })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should remove the integration", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/removeintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CUSTOM })
      .then(function(res)
      {
        expect(res.body[ 0 ].integrationtype).to.equal(enums.integrationService.CUSTOM);
        done();
      })
      .catch(done);
    });
    it("should not remove the integration(CUSTOM is not integrated) - 400526", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/removeintegration')
      .set('Authorization', superUser.acctoken)
      .send({ integrationtype: enums.integrationService.CUSTOM })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400526);
        done();
      })
      .catch(done);
    });
  });
  describe("#logglyalertmap()", function()
  {
    it("should not map the alerts(requester is not a super user) - 403502", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/logglyalertmap')
      .set('Authorization', allPurposeUser.acctoken)
      .send({ maps: [ { serviceid: serviceOne.serviceid, alertname: "authServerHighLoad" } ] })
      .then(function(res)
      {
        expect(res.body.code).to.equal(403502);
        done();
      })
      .catch(done);
    });
    it("should not map the alerts(empty maps) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/logglyalertmap')
      .set('Authorization', superUser.acctoken)
      .send({ maps: [] })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not map the alerts(empty maps) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/logglyalertmap')
      .set('Authorization', superUser.acctoken)
      .send({ maps: [] })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not map the alerts(duplicate alertnames) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/logglyalertmap')
      .set('Authorization', superUser.acctoken)
      .send({ maps: [ { serviceid: serviceOne.serviceid, alertname: "authServerHighLoad" }, { serviceid: 23, alertname: "authServerHighLoad" } ] })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400051);
        done();
      })
      .catch(done);
    });
    it("should not map the alerts(inexistent serviceids) - 400362", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/logglyalertmap')
      .set('Authorization', superUser.acctoken)
      .send({ maps: [ { serviceid: serviceOne.serviceid, alertname: "authServerHighLoad" }, { serviceid: 23, alertname: "lowTrafficAlert" } ] })
      .then(function(res)
      {
        expect(res.body.code).to.equal(400362);
        done();
      })
      .catch(done);
    });
    it("should map the alerts", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/logglyalertmap')
      .set('Authorization', superUser.acctoken)
      .send({ maps: [ { serviceid: serviceOne.serviceid, alertname: "authServerHighLoad" } ] })
      .then(function(res)
      {
        expect(res.body[ 0 ].config.servicetoalertmaps[ 0 ].serviceid).to.equal(serviceOne.serviceid);
        done();
      })
      .catch(done);
    });
  });
  describe("#listintegration()", function()
  {
    it("should list the integrations", function(done)
    {
      request(sails.hooks.http.app)
      .post('/company/listintegrations')
      .set('Authorization', allPurposeUser.acctoken)
      .then(function(res)
      {
        expect(res.body.length).to.be.at.least(1);
        done();
      })
      .catch(done);
    });
  });
});
