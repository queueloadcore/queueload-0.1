/* FileName: 3.Cb.test.js
* Description: This file defines all the unit test cases for the Cb Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 16/03/2016          Initial file creation                           bones
*
*/

var chai = require("chai");
var expect = chai.expect;

describe("Cb Model", function()
{
  describe("#Create", function()
  {
    var activeCb_CP =
    {
      "bdid": 2,
      "cbownerid": 1,
      "isfrozen": true
    };

    var inactiveCb_CP =
    {
      "bdid": 2,
      "cbownerid": 1,
      "isactive": false,
      "isfrozen": false
    };

    var activeCb_WP_value =
    {
      "bdid": "a",
      "cbownerid": "ab"
    };

    var activeCb_WP_exist =
    {
      "bdid": 65,
      "cbownerid": 99
    };

    it("should add an inactive Cb to the system with isactive sent as true", function()
    {
      return Cb.create(activeCb_CP)
      .then(function createCb(created)
      {
        expect(created).to.exist;
        expect(created).to.have.property("isactive").and.equal(false);
      });
    });

    it("should add a Inactive Cb to the system", function()
    {
      return Cb.create(inactiveCb_CP)
      .then(function createCb(created)
      {
          expect(created).to.exist;
          expect(created).to.have.property("isactive").and.equal(false);
      });
    });

    it("should not add Cb with incorrect parameters (Value validation)", function()
    {
      return Cb.create(activeCb_WP_value)
      .then(function createCb(created)
      {
          expect(created).to.not.exist;
      })
      .catch(function(err)
      {
        expect(err).to.exist;
        var numOfAttributes = err.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1"); //TODO: test this weird validation error
      });
    });
  });

  describe("#Update", function()
  {
    it("should update Cb with given values", function()
    {
      return Cb.update({ cbid: 1 }, { bdid: 1, cbownerid: 2, isactive: false })
      .then(function updatedCb(updated)
      {
        expect(updated).to.exist;
        expect(updated[ 0 ]).to.have.property("cbid", 1);
        expect(updated[ 0 ]).to.have.property("bdid", 1); //Check for updated value
        expect(updated[ 0 ]).to.have.property("cbownerid", 2);
        expect(updated[ 0 ]).to.have.property("isactive", false);
      });
    });
  });

  describe("#Destroy", function()
  {
    it("should delete a Cb from the database", function()
    {
      return Cb.destroy({ cbid: 1 })
      .then(function deletedCb(deleted)
      {
        return Cb.find({ cbid: 1 });
      })
      .then(function found(cb)
      {
        expect(cb).to.be.empty; //Bd should have been deleted
      });
    });
  });
});
