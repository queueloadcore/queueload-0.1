/* FileName: 1.User.test.js
* Description: This file defines all the unit test cases for the User Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 16/03/2016          Initial file creation                           bones
*
*/

var chai = require("chai");
var expect = chai.expect;
var errorMessage = require("../../../api/resources/ErrorCodes.js");

describe("User Model", function()
{
  var userid;
  describe("#Create", function()
  {
    var company =
    {
      "accountid": "qlodtest",
      "name": "Wayne Enterprises",
      "address": "Gotham City",
      "globalconfig": {},
      "accntadmin": 1
    };

    var activeUser_CP =
    {
      "fullname":"alpha.tester",
      "username":"T3st.Rat3",
      "password":"P@5#word",
      "emailid":"tester1rat@testclient3.com",
      "countrycode":"+91",
      "phonenumber":"9872387192",
      "employeeid":"FTE817345"
    };

    var inActiveUser_CP =
    {
      "fullname":"beta.tester",
      "username":"T3st.cat2",
      "password":"P@55word",
      "emailid":"tester2.rat@testclient3.com",
      "countrycode":"+92",
      "phonenumber":"9822387102",
      "employeeid":"FTE217345",
      "isactive": false
    };

    var inActiveUser_WP =
    {
      "fullname":"gamma_tester",
      "username":"T3st.Rat_$4",
      "password":"P@55word",
      "emailid":"tester2.rat@@testclient3.com",
      "countrycode":"+91a",
      "phonenumber":"982238712a",
      "employeeid":"FTE217345#"
    };

    var oneName =
    {
      "fullname":"Name.Tester One",
      "username":"Name.tester1",
      "password":"P@55word",
      "emailid":"name.tester1@testclient3.com"
    };

    it("should add an active User to the system", function(done)
    {
      Company.findOrCreate( { name: company.name }, company)
      .then(function(created)
      {
        if(!created)
        {
          throw new Error("Couldn't findOrCreate the company");
        }
        company = created;
        activeUser_CP.accountid = created.accountid;
        inActiveUser_CP.accountid = created.accountid;
        inActiveUser_WP.accountid = created.accountid;
        oneName.accountid = created.accountid;
        return User.create(activeUser_CP);
      })
      .then(function createUser(created)
      {
        expect(created).to.exist;
        expect(created).to.have.property("password").and.not.equal(activeUser_CP.password); //Password should be hashed
        expect(created).to.have.property("defaultqueue", null);
        expect(created).to.have.property("isactive").and.equal(true); //Default value supplied
        userid = created.userid;
        return Role.create({ userid: created.userid });
      })
      .then(function createRole(role)
      {
        expect(role).to.exist;
        expect(role).to.have.property("issuperuser", false);
        expect(role).to.have.property("isbdadmin", false);
        expect(role).to.have.property("isoperator", false);
        done();
      })
      .catch(done);
    });

    it("should add a Inactive User to the system", function(done)
    {
      User.create(inActiveUser_CP)
      .then(function createUser(created)
      {
        expect(created).to.exist;
        expect(created).to.have.property("isactive").and.equal(false);
        return Role.create({ userid: created.userid });
      })
      .then(function createRole(role)
      {
        expect(role).to.exist;
        expect(role).to.have.property("issuperuser", false);
        expect(role).to.have.property("isbdadmin", false);
        expect(role).to.have.property("isoperator", false);
        done();
      })
      .catch(done);
    });

    it("should add a User without first or last name to the system", function(done)
    {
      User.create(oneName)
      .then(function createUser(created)
      {
        expect(created).to.exist;
        expect(created).to.have.property("fullname", oneName.fullname);
        expect(created).to.have.property("isactive").and.equal(true);
        return Role.create({ userid: created.userid });
      })
      .then(function createRole(role)
      {
        expect(role).to.exist;
        expect(role).to.have.property("issuperuser", false);
        expect(role).to.have.property("isbdadmin", false);
        expect(role).to.have.property("isoperator", false);
        done();
      })
      .catch(done);
    });

    it("should not add user with incorrect parameters (Value validation)", function(done)
    {
      User.create(inActiveUser_WP)
      .then(function createUser(created)
      {
        expect(created).to.not.exist;
      })
      .catch(function(err)
      {
        expect(err).to.exist;
        var numOfAttributes = err.reason.split(" ", 1)[ 0 ];
        expect(parseInt(numOfAttributes)).to.equal(5);
        done();
      })
      .catch(done);
    });
  });

  describe("#Update", function()
  {
    it("should update user with given values", function(done)
    {
      User.update({ userid: userid }, { fullname: "deepika padukone" })
      .then(function updatedUser(updated)
      {
        expect(updated).to.exist;
        expect(updated[ 0 ]).to.have.property("fullname", "deepika padukone"); //Check for updated value
        done();
      })
      .catch(done);
    });
  });

  describe("#Destroy", function()
  {
    it("should delete a user from the database", function(done)
    {
      User.destroy({ userid: userid })
      .then(function deletedUser()
      {
        return User.find({ userid: userid });
      })
      .then(function found(user)
      {
        expect(user).to.be.empty;
        return Role.destroy({ userid: userid });
      })
      .then(function deletedRole()
      {
        return Role.find({ userid: userid });
      })
      .then(function found(role)
      {
        expect(role).to.be.empty;
        done();
      })
      .catch(done);
    });
  });
});
