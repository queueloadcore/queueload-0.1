/* FileName: 1.Company.test.js
* Description: This file defines all the unit test cases for the Company Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 25/11/2016          Initial file creation                           SMandal
*
*/

var chai = require("chai");
var expect = chai.expect;

describe("Company Model", function()
{
  var company =
  {
    "accountid": "qlodtest",
    "name": "Wayne Enterprises",
    "accntadmin": 1
  };
  describe("#Create()", function()
  {
    it("should not create the company(missing name and accntadmin)", function(done)
    {
      var name = company.name;
      delete company.name;
      var accntadmin = company.accntadmin;
      delete company.accntadmin;
      Company.create(company)
      .then(function(result)
      {
        expect(result).to.not.exist;
      })
      .catch(function(err)
      {
        var numOfAttributes = err.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("2");
        company.name = name;
        company.accntadmin = accntadmin;
        done();
      })
      .catch(done);
    });
    it("should add the company", function(done)
    {
      Company.findOrCreate(company)
      .then(function(result)
      {
        expect(result.name).to.equal(company.name);
        company = result;
        done();
      })
      .catch(done);
    });
  });
  describe("#Update()", function()
  {
    it("should not update the company(non-existent company id)", function(done)
    {
      Company.update({ accountid: 123456 }, { name: "doesn't matter" })
      .then(function(result)
      {
        expect(result).to.be.empty;
        done();
      })
      .catch(done);
    });
    it("should update the company", function(done)
    {
      Company.update({ accountid: company.accountid }, { name: "Avenger Enterprises" })
      .then(function(result)
      {
        expect(result[ 0 ].name).to.equal("Avenger Enterprises");
        done();
      })
      .catch(done);
    });
  });
  describe("#Destroy()", function()
  {
    it("should not delete the company(non-existent company id)", function(done)
    {
      Company.destroy({ accountid: 123456 })
      .then(function(result)
      {
        expect(result).to.be.empty;
        done();
      })
      .catch(done);
    });
    it("should delete the company", function(done)
    {
      Company.destroy({ accountid: company.accountid })
      .then(function(result)
      {
        expect(result[ 0 ].accountid).to.equal(company.accountid);
        done();
      })
      .catch(done);
    });
  });
});
