/* FileName: 2.Bd.test.js
* Description: This file defines all the unit test cases for the Bd Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 08/03/2016          Initial file creation                           bones
*
*/

var chai = require("chai");
var expect = chai.expect;

describe("Bd Model", function()
{
  describe("#Create", function()
  {
    var activeBd_CP =
    {
      "bdname": "Business-Department.1",
      "isactive":true
    };

    var inActiveBD_CP =
    {
      "bdname": "Business-Department.temp 2"
    };

    var inActiveBD_WP =
    {
      "bdname": "BusinessDepartment 3#"
    };

    it("should add an active BD to the system", function()
    {
      return Bd.create(activeBd_CP)
      .then(function createBd(created)
      {
        expect(created).to.exist;
        expect(created).to.have.property("bdname", activeBd_CP.bdname);
        expect(created).to.have.property("isactive").and.equal(true);
      });
    });

    it("should add a Inactive BD to the system", function()
    {
      return Bd.create(inActiveBD_CP)
      .then(function createBd(created)
      {
          expect(created).to.exist;
          expect(created).to.have.property("bdname", inActiveBD_CP.bdname);
          expect(created).to.have.property("isactive").and.equal(false);  //Default value supplied
      });
    });

    it("should not add BD with incorrect parameters (Value validation)", function()
    {
      return Bd.create(inActiveBD_WP)
      .then(function createBd(created)
      {
          expect(created).to.not.exist;
      })
      .catch(function(err)
      {
        expect(err).to.exist;
        var numOfAttributes = err.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
      });
    });
  });

  describe("#Update", function()
  {
    it("should update BD with given values", function()
    {
      return Bd.update({ bdid: 1 }, { bdname: "BusinessDepartment 4" })
      .then(function updatedBd(updated)
      {
        expect(updated).to.exist;
        expect(updated[ 0 ]).to.have.property("bdname", "BusinessDepartment 4"); //Check for updated value
      });
    });
  });

  describe("#Destroy", function()
  {
    it("should delete a BD from the database", function()
    {
      return Bd.destroy({ bdid: 1 })
      .then(function deletedBd(deleted)
      {
        return Bd.find({ bdid: 1 });
      })
      .then(function found(bd)
      {
        expect(bd).to.be.empty; //Bd should have been deleted
      });
    });
  });

});
