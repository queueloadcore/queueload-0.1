/* FileName: 4.Queue.test.js
* Description: This file defines all the unit test cases for the Queue Model.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 16/03/2016          Initial file creation                           bones
*
*/

var chai = require("chai");
var expect = chai.expect;

describe("Queue Model", function()
{
  describe("#Create", function()
  {
    var InactiveQueue_CP =
    {
      "queuename":"Test-Queue.1",
      "cbid": 1,
      "queueemail":"testqueue1@test.com"
    };

    var activeQueue_CP =
    {
      "queuename": "Test-Queue.2",
      "cbid": 3,
      "queueemail": "testqueue2@test.com",
      "isactive": true,
      "isfrozen": false
    };

    var activeQueue_WP =
    {
      "queuename": "Test-Queue.3#",
      "cbid": 3,
      "queueemail": "testqueue2@test.com@gmail.com",
      "isactive": true
    };

    it("should add an Inactive Queue to the system", function()
    {
      return Queue.create(InactiveQueue_CP)
      .then(function createQueue(created)
      {
        expect(created).to.exist;
        expect(created).to.have.property("queuename", InactiveQueue_CP.queuename);
        expect(created).to.have.property("isactive").and.equal(false);
        expect(created).to.have.property("isfrozen").and.equal(true); //Default value supplied
      });
    });

    it("should add a active Queue to the system", function()
    {
      return Queue.create(activeQueue_CP)
      .then(function createQueue(created)
      {
          expect(created).to.exist;
          expect(created).to.have.property("queuename", activeQueue_CP.queuename);
          expect(created).to.have.property("isactive").and.equal(true);
          expect(created).to.have.property("isfrozen").and.equal(false);
      });
    });

    it("should not add Queue with incorrect parameters (Value validation)", function()
    {
      return Queue.create(activeQueue_WP)
      .then(function createQueue(created)
      {
          expect(created).to.not.exist;
      })
      .catch(function(err)
      {
        expect(err).to.exist;
        var numOfAttributes = err.reason.charAt(0);
        expect(parseInt(numOfAttributes)).to.equal(2);
      });
    });
  });

  describe("#Update", function()
  {
    it("should update queue with given values", function()
    {
      return Queue.update({ queueid: 1 }, { queueemail: "testqueue3@test.com" })
      .then(function updatedQueue(updated)
      {
        expect(updated).to.exist;
        expect(updated[ 0 ]).to.have.property("queueemail", "testqueue3@test.com"); //Check for updated value
      });
    });
  });

  describe("#Destroy", function()
  {
    it("should delete a queue from the database", function()
    {
      return Queue.destroy({ queueid: 1 })
      .then(function deletedUser()
      {
        return Queue.find({ queueid: 1 });
      })
      .then(function found(queue)
      {
        expect(queue).to.be.empty; //Queue should have been deleted
        return Queue.destroy({ queueid: 2 });
      });
    });
  });
});
