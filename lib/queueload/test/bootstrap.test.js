var Sails = require("sails");

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(30000);

  var environment;
  if(process.env.NODE_ENV === 'test')
  {
    console.log("Testing locally");
    environment = 'test';
  }
  else if(process.env.NODE_ENV === 'staging')
  {
    console.log("Testing in Cloud");
    environment = 'staging';
  }
  else
  {
    console.log("No environment set!!");
  }

  Sails.lift({
    // Configuration for testing purposes
    log: {
      level: "silent"
    },
    models: {
      migrate: "drop"
      },
    environment: environment,
    port: 9999,
    hooks: {
        "csrf": false,
        "grunt": false,
        "i18n": false,
        "pubsub": false,
        "session": false,
        "sockets": false,
        "views": false
    }
  },
  function(err, server)
  {
    if (err) return done(err);

    // Here you can load fixtures, etc.

    console.log();
    done(err, server);
  });
});

after(function(done) {
  // Here you can clear fixtures, etc.
  console.log();
  Sails.lower(done);
});
