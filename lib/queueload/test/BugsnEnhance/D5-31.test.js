/* FileName: D5-31.test.js
* Description: This file defines all the unit test cases bug D5-31
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 30/08/2016          Initial file creation                           bones
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var enums = require("../../api/resources/Enums.js");

describe("Bug D5-31", function()
{
  var validUser =
  {
    "emailid":"d5-31.test@testclients.com",
    "password":"P@55word"
  };

  var activeUser_CP =
  {
    "firstname":"test",
    "lastname":"bug",
    "username":"D5-31.test",
    "password":"P@55word",
    "emailid":"d5-31.test@testclients.com",
    "countrycode":"+91",
    "phonenumber":"9536841379",
    "employeeid":"FTE564586"
  };

  var accTkn, refTkn;
  var user, bd, cb, queue;

  before("Signing-in", function(done)
  {
    return User.create(activeUser_CP)
    .then(function createUser(created)
    {
      user = created.userid;
      return request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(validUser)
      .expect(200)
      .then(function(res)
      {
        accTkn = res.body.acctoken;
        refTkn = res.body.reftoken;
        done();
      });
    })
    .catch(done);
  });

  it("test the tokens", function(done)
  {
    expect(accTkn).to.exist;
    expect(refTkn).to.exist;
    done();
  });

  describe("BD: Add + assign + activate", function()
  {
    var activeBD_CP =
    {
      "bdname": "Business-Department.D5-31"
    };
    var assignBD_CP_activeBD =
    {
      "bdid": bd,
      "admins": [ user ],
      "assign": true
    };
    var bd_CP_2 =
    {
      "bdids": [ bd ],
      "action": true
    };
    it("Add", function(done)
    {
      request(sails.hooks.http.app)
      .post("/bd/add")
      .set("Authorization", accTkn)
      .send(activeBD_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        bd = res.body.bdid;
        done();
      });
    });
    it("Assign", function(done)
    {
      assignBD_CP_activeBD.bdid = bd;
      assignBD_CP_activeBD.admins = [ user ];
      request(sails.hooks.http.app)
      .post("/bd/assign")
      .set("Authorization", accTkn)
      .send(assignBD_CP_activeBD)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        done();
      });
    });
    it("Activate", function(done)
    {
      bd_CP_2.bdids = [ bd ];
      request(sails.hooks.http.app)
      .post("/bd/activation")
      .set("Authorization", accTkn)
      .send(bd_CP_2)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
        done();
      });
    });
  });

  describe("CB: Add + Approve", function()
  {
    var cb_CP_1 =
    {
      "bdid": bd,
      "ownerid": user
    };
    var approve_CP_1 =
    {
      "approvalid": 7,
      "approved": true,
      "approverid": user
    };

    it("Add", function(done)
    {
      cb_CP_1.bdid = bd;
      cb_CP_1.ownerid = user;
      request(sails.hooks.http.app)
      .post("/cb/add")
      .set("Authorization", accTkn)
      .send(cb_CP_1)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("typeofapproval").and.equal(enums.approval.ASSIGN_CB2U);
        expect(res.body[ 0 ]).to.have.property("targetentityid").and.equal(cb_CP_1.ownerid);
        expect(res.body[ 0 ]).to.have.property("entityid").and.equal(res.body[ 1 ].cbid);
        expect(res.body[ 1 ]).to.have.property("isactive").and.equal(true);
        expect(res.body[ 1 ]).to.have.property("isfrozen").and.equal(true);
        expect(res.body[ 1 ]).to.have.property("cbownerid").and.equal(null);
        approve_CP_1.approvalid  = res.body[ 0 ].approvalid;
        cb = res.body[ 1 ].cbid;
        done();
      });
    });
    it("Approve", function(done)
    {
      request(sails.hooks.http.app)
      .post("/orgapproval/approve")
      .set("Authorization", accTkn)
      .send(approve_CP_1)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ][ 0 ]).to.have.property("approvalid").and.equal(approve_CP_1.approvalid);
        expect(res.body[ 0 ][ 0 ]).to.have.property("isactive").and.equal(false);
        expect(res.body[ 0 ][ 0 ]).to.have.property("isapproved").and.equal(true);
        expect(res.body[ 1 ][ 0 ]).to.have.property("isactive").and.equal(true);
        expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(false);
        expect(res.body[ 1 ][ 0 ]).to.have.property("cbownerid").and.equal(cb_CP_1.ownerid);
        done();
      });
    });
  });

  describe("#Queue - Add + Approve", function()
  {
    var queue_CP_1 =
    {
      "queuename":"TestQueueD5-31",
      "cbid": cb,
      "queueemail":"testqueue14@test.com",
      "queueownerid": user,
      "incidentmanagerid": user,
      "changemanagerid": user,
      "srtmanagerid": user,
      "alertl1":"testqueue1alert1@test.com",
      "alertl2":"testqueue1alert2@test.com",
      "alertl3":"testqueue1alert3@test.com"
    };
    var approve_CP_1 =
    {
      "approvalid": 6,
      "approved": true
    };
    it("should add queue to the system - Post Approval by CB code owner", function(done)
    {
      queue_CP_1.cbid = cb;
      queue_CP_1.queueownerid = user;
      queue_CP_1.incidentmanagerid = user;
      queue_CP_1.changemanagerid = user;
      queue_CP_1.srtmanagerid = user;

      request(sails.hooks.http.app)
      .post("/queue/add")
      .set("Authorization", accTkn)
      .send(queue_CP_1)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.have.property("typeofapproval").and.equal(enums.approval.ASSIGN_Q2CB);
        expect(res.body[ 0 ]).to.have.property("targetentityid").and.equal(queue_CP_1.cbid);
        expect(res.body[ 0 ]).to.have.property("entityid").and.equal(res.body[ 1 ].queueid);
        expect(res.body[ 1 ]).to.have.property("isactive").and.equal(false);
        expect(res.body[ 1 ]).to.have.property("isfrozen").and.equal(true);
        expect(res.body[ 1 ]).to.have.property("cbid").and.equal(null);
        expect(res.body[ 1 ]).to.have.property("queuename").and.equal(queue_CP_1.queuename);
        approve_CP_1.approvalid = res.body[ 0 ].approvalid;
        approve_CP_1.approverid = res.body[ 0 ].approverid;
        queue = res.body[ 1 ].queueid;

        request(sails.hooks.http.app)
        .post("/orgapproval/approve")
        .set("Authorization", accTkn)
        .send(approve_CP_1)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(res.body[ 0 ][ 0 ]).to.have.property("approvalid").and.equal(approve_CP_1.approvalid);
          expect(res.body[ 0 ][ 0 ]).to.have.property("isactive").and.equal(false);
          expect(res.body[ 0 ][ 0 ]).to.have.property("isapproved").and.equal(true);
          expect(res.body[ 1 ][ 0 ]).to.have.property("isactive").and.equal(false);
          expect(res.body[ 1 ][ 0 ]).to.have.property("isfrozen").and.equal(false);
          expect(res.body[ 1 ][ 0 ]).to.have.property("cbid").and.equal(queue_CP_1.cbid);
          done();
        });
      });
    });
  });

  describe("Test D5-31 - Search for queue", function()
  {
    var queue_CP =
    {
      "search":  { "queuename": "Te%" },
      "result": [ "queuename", "queueemail", "queueownerid", "incidentmanagerid", "changemanagerid", "alertl1", "isactive", "isfrozen" ],
      "orderby": "queuename"
    };
    var queue_CP_2 =
    {
      "search":  { "queuename": "Te%" },
      "result": [ "queuename", "queueemail", "alertl1", "isactive", "isfrozen" ],
      "orderby": "queuename"
    };

    it("should search for the Queues - populate owner and managers", function(done)
    {
      request(sails.hooks.http.app)
      .post("/queue/search")
      .set("Authorization", accTkn)
      .send(queue_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.be.above(0);
        expect(Object.keys(res.body[ 1 ][ 0 ]).length).to.equal(10); //TBD: D5-33
        expect(res.body[ 1 ][ 0 ].queueownerid).to.be.an('object');
        expect(res.body[ 1 ][ 0 ].incidentmanagerid).to.be.an('object');
        expect(res.body[ 1 ][ 0 ].changemanagerid).to.be.an('object');
        done();
      });
    });
    it("should search for the Queues - not populate owner and managers", function(done)
    {
      request(sails.hooks.http.app)
      .post("/queue/search")
      .set("Authorization", accTkn)
      .send(queue_CP_2)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body[ 0 ]).to.be.above(0);
        expect(Object.keys(res.body[ 1 ][ 0 ]).length).to.equal(7); //TBD: D5-33
        expect(res.body[ 1 ][ 0 ].queueownerid).to.not.exist;
        expect(res.body[ 1 ][ 0 ].incidentmanagerid).to.not.exist;
        expect(res.body[ 1 ][ 0 ].changemanagerid).to.not.exist;
        done();
      });
    });
  });
  describe("Test D5-31 - getdetails of queue", function()
  {
    var queue_CP =
    {
      "queueids": [ 5, 6, 9 ]
    };

    it("should get details of the queue - populate owner, managers, members, and managed cis", function(done)
    {
      queue_CP.queueids = [ queue ];
      request(sails.hooks.http.app)
      .post("/queue/getdetail")
      .set("Authorization", accTkn)
      .send(queue_CP)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.lengthOf(1);
        expect(res.body[ 0 ].queueownerid).to.be.an('object');
        expect(res.body[ 0 ].incidentmanagerid).to.be.an('object');
        expect(res.body[ 0 ].changemanagerid).to.be.an('object');
        expect(res.body[ 0 ].members).to.be.an('array');
        expect(res.body[ 0 ].cismanaged).to.be.an('array');
        done();
      });
    });
  });
});
