/* FileName: ClientCacheModule.test.js
* Description: This file defines the unit and integration test cases for the ClientCache Module.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 06/10/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;

var superuserid, validAccTkn, validRefTkn, cacheTimestamp, queueCreateApprovalId, newUserValidAccTkn, newQueueId;

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001"
};

var batuser =
{
  "firstname":"clntchefirst",
  "lastname":"clntcheuser",
  "fullname": "clntchefirst clntcheuser",
  "username":"clntchefirst.clntcheuser",
  "password":"passowrd",
  "emailid":"clntchefirst.clntcheuser@testdomain.com",
  "countrycode":"+91",
  "phonenumber":"8375768373",
  "employeeid":"CLNTCHEFTE000001"
};

var newUser =
{
  "firstname":"clientcacheNew",
  "lastname":"User",
  "fullname": "clientcacheNew User",
  "username":"cacheuser",
  "password":"passowrd",
  "emailid":"clientcachenew.user@testdomain.com",
  "countrycode":"+91",
  "phonenumber":"8375768373",
  "employeeid":"FTE837576"
};

var newBd =
{
  "bdname": "ClientCacheTestBd"
};

var newCb = {
  "isactive": true,
  "isfrozen": false,
  "freecause": 0
};

var newService = {
  "servicename": "Clientcache Test Service",
  "details": "This is to test clientcache"
  };

var newQueue =
{
  "queuename":"ClientCacheQueue",
  "queueemail":"clientcachenew.queue@testdomain.com",
  "alertl1":"clientcachenew.queue.alert1@testdomain.com",
  "alertl2":"clientcachenew.queue.alert2@testdomain.com",
  "alertl3":"clientcachenew.queue.alert3@testdomain.com"
};

var newCi =
{
  "ciname": "CLIENT-CACHE-CI-NAME",
  "assettag": "CLIENT-CACHE-CI-TAG",
  "installationdate": 1480515761
};

var newLocation =
{
  "locationname": "clientcache-loc",
  "locationcode": "CLNT-CHE-CODE",
  "buildingname": "RD TS",
  "sitecategory": "TEST site"
};

describe("ClientCache Module Tests", function()
{
	before("Setting up the stage", function(done)
  {
    Cilocation.create(newLocation)
    .then(function(location)
    {
      newCi.locationid = location.locationid;
      return User.findOrCreate({ emailid: "administrator@testdomain.com" }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send({ emailid: superUser.emailid, password: superUser.password });
      }
    })
    .then(function(res)
    {
      var superToken = res.body.acctoken;
      return request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", superToken)
      .send(batuser);
    })
    .then(function(res)
    {
      batuser.userid = res.body.userid;
      return Role.update({ userid: batuser.userid }, { userid: batuser.userid, issuperuser: true, isbdadmin: true, isoperator: true });
    })
    .then(function(res)
    {
      return request(sails.hooks.http.app)
      .post("/auth/signin")
      .send({ emailid: batuser.emailid, password: batuser.password });
    })
    .then(function(res)
    {
      expect(res.body.acctoken).to.exist;
      expect(res.body.reftoken).to.exist;
      validAccTkn = res.body.acctoken;
      validRefTkn = res.body.reftoken;
      done();
    })
    .catch(done);
  });
  describe("Controller Tests", function()
  {
    describe("#getalldata()", function()
    {
      it("should generate and return data", function(done)
      {
        request(sails.hooks.http.app)
        .post("/clientcache/getalldata")
        .set('Authorization', validAccTkn)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.have.property('users');
          expect(res.body).to.have.property('bds');
          expect(res.body).to.have.property('queues');
          expect(res.body).to.have.property('services');
          expect(res.body).to.have.property('cis');
          expect(res.body).to.have.property('citypes');
          expect(res.body).to.have.property('cienvironments');
          expect(res.body).to.have.property('vendors');
          expect(res.body).to.have.property('timestamp');
          cacheTimestamp = res.body.timestamp;
          done();
        });
      });

      it("should return data from cache", function(done)
      {
        request(sails.hooks.http.app)
        .post("/clientcache/getalldata")
        .set('Authorization', validAccTkn)
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(200);
          expect(res.body.timestamp).to.equal(cacheTimestamp);
          done();
        });
      });
    });
    describe("#isvalid()", function()
    {
      it("should not return result - No timestamp", function(done)
      {
        request(sails.hooks.http.app)
        .post("/clientcache/isvalid")
        .set('Authorization', validAccTkn)
        .expect(400)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(400);
          expect(res.body).to.have.property('code', 400051);
          done();
        });
      });
      it("should return true - client.timestamp = cache.timestamp", function(done)
      {
        request(sails.hooks.http.app)
        .post("/clientcache/isvalid")
        .set('Authorization', validAccTkn)
        .send({ timestamp: cacheTimestamp })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.equal(true);
          done();
        });
      });
      it("should return result from cache - client.timestamp > cache.timestamp", function(done)
      {
        request(sails.hooks.http.app)
        .post("/clientcache/isvalid")
        .set('Authorization', validAccTkn)
        .send({ timestamp: 9999999999999 })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.have.property('timestamp').and.equal(cacheTimestamp);
          done();
        });
      });
      it("should return result from cache - client.timestamp < cache.timestamp", function(done)
      {
        request(sails.hooks.http.app)
        .post("/clientcache/isvalid")
        .set('Authorization', validAccTkn)
        .send({ timestamp: 1 })
        .expect(200)
        .end(function(err, res)
        {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.have.property('timestamp').and.equal(cacheTimestamp);
          done();
        });
      });
      it("should generate the data from the server - new user created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/user/add")
        .set('Authorization', validAccTkn)
        .send(newUser)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property("isactive").and.equal(true);
          newCb.cbownerid = res.body.userid;
          newQueue.queueownerid = res.body.userid;
          newQueue.incidentmanagerid = res.body.userid;
          newQueue.changemanagerid = res.body.userid;
          newQueue.srtmanagerid = res.body.userid;
          newCi.ciownerid = res.body.userid;

          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(200);
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new bd created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/bd/add")
        .set('Authorization', validAccTkn)
        .send(newBd)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property("isactive").and.equal(false);
          newCb.bdid = res.body.bdid;

          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(200);
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new queue created", function(done)
      {
        Cb.create(newCb)
        .then(function(cb)
        {
          newQueue.cbid = cb.cbid;
          newService.cbid = cb.cbid;

          return request(sails.hooks.http.app)
          .post("/bd/assign")
          .set('Authorization', validAccTkn)
          .send({ bdid: cb.bdid, admins: [ batuser.userid ], "assign": true })
          .expect(200)
          .then(function(res)
          {
            expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
          });
        })
        .then(function(bdid)
        {
          return request(sails.hooks.http.app)
          .post("/auth/signin")
          .send(batuser);
        })
        .then(function(res)
        {
          validAccTkn = res.body.acctoken;
          return request(sails.hooks.http.app)
          .post("/bd/activation")
          .set('Authorization', validAccTkn)
          .send({ bdids: [ newCb.bdid ], action: true })
          .expect(200)
          .then(function(res)
          {
            expect(res.body[ 0 ]).to.have.property("isactive").and.equal(true);
          });
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/queue/add")
          .set('Authorization', validAccTkn)
          .send(newQueue)
          .expect(200)
          .then(function( res)
          {
            expect(res.body[ 0 ]).to.have.property("isactive").and.equal(false);
            queueCreateApprovalId = res.body[ 1 ].approvalid;
            newQueue.queueid = res.body[ 0 ].queueid;
          });
        })
        .then(function(res)
        {
          var newUser =
          {
            "emailid":"clientcachenew.user@testdomain.com",
            "password":"passowrd"
          };
          return request(sails.hooks.http.app)
          .post("/auth/signin")
          .send(newUser)
          .expect(200)
          .then(function(res)
          {
            expect(res.body.acctoken).to.exist;
            expect(res.body.reftoken).to.exist;
            newUserValidAccTkn = res.body.acctoken;
          });
        })
        .then(function(res)
        {
          var approval =
          {
            "approvalid": queueCreateApprovalId,
            "approved": true
          };
          return request(sails.hooks.http.app)
          .post("/orgapproval/approve")
          .set('Authorization', newUserValidAccTkn)
          .send(approval)
          .expect(200)
          .then(function(res)
          {
            expect(res.body[ 0 ].isfrozen).to.equal(false);
          });
        })
        .then(function(res)
        {
          var assoc =
          {
            "queueid": newQueue.queueid,
            "members": [ newQueue.queueownerid ],
            "associate": true
          };
          return request(sails.hooks.http.app)
          .post("/queue/memberassoc")
          .set('Authorization', validAccTkn)
          .send(assoc)
          .then(function(res)
          {
            expect(res.body[ 0 ].queueid).to.equal(newQueue.queueid);
          });
        })
        .then(function(res)
        {
          var activate =
          {
            "queueids": [ newQueue.queueid ],
            "action": true
          };
          return request(sails.hooks.http.app)
          .post("/queue/activation")
          .set('Authorization', validAccTkn)
          .send(activate)
          .then(function(res)
          {
            expect(res.body[ 0 ]).to.have.property('isactive').and.equal(true);
          });
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(200);
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new service created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/add")
        .set('Authorization', validAccTkn)
        .send(newService)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property('isfrozen').and.equal(false);
          newService.serviceid = res.body.serviceid;
          return Service.update({ serviceid: newService.serviceid }, { servicestateid: 2 });
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(200);
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new cienvironment created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/add")
        .set('Authorization', validAccTkn)
        .send({ envnames: [ 'goli', 'vada', 'pao' ] })
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.lengthOf(3);
          newCi.envid = res.body[ 0 ].envid;
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new citype created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/add")
        .set('Authorization', validAccTkn)
        .send({ "citypes": [ "mudhi dabba" ] })
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.lengthOf(1);
          newCi.citypeid = res.body[ 0 ].citypeid;
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new vendor created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/add")
        .set('Authorization', validAccTkn)
        .send({ "vendornames": [ "Grand Super Market", "Aishwarya", "Bangalore Rice Traders" ] })
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.lengthOf(3);
          newCi.vendorid = res.body[ 0 ].vendorid;
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - new CI created", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cidetail/add")
        .set('Authorization', validAccTkn)
        .send(newCi)
        .expect(200)
        .then(function(res)
        {
          expect(res.body).to.have.property('isfrozen').and.equal(false);
        })
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - cienvironment updated", function(done)
      {
        request(sails.hooks.http.app)
        .post("/cienvironment/update")
        .set('Authorization', validAccTkn)
        .send({ envid: newCi.envid }, { envname: "updated env name" })
        .expect(200)
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - citype updated", function(done)
      {
        request(sails.hooks.http.app)
        .post("/citype/update")
        .set('Authorization', validAccTkn)
        .send([ { citypeid: newCi.citypeid, citype: "updatedtype" } ])
        .expect(200)
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - service updated", function(done)
      {
        request(sails.hooks.http.app)
        .post("/service/update")
        .set('Authorization', validAccTkn)
        .send({ serviceid: newService.serviceid, details: "updated details" })
        .expect(200)
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
      it("should generate the data from the server - vendor updated", function(done)
      {
        request(sails.hooks.http.app)
        .post("/vendor/update")
        .set('Authorization', validAccTkn)
        .send([ { vendorid: newCi.vendorid, vendorname: "updated vendorname" } ])
        .expect(200)
        .then(function(res)
        {
          return request(sails.hooks.http.app)
          .post("/clientcache/isvalid")
          .set('Authorization', validAccTkn)
          .send({ timestamp: cacheTimestamp })
          .expect(200)
          .then(function(res)
          {
            expect(res.body).to.have.property('users');
            expect(res.body).to.have.property('bds');
            expect(res.body).to.have.property('queues');
            expect(res.body).to.have.property('services');
            expect(res.body).to.have.property('cis');
            expect(res.body).to.have.property('citypes');
            expect(res.body).to.have.property('cienvironments');
            expect(res.body).to.have.property('vendors');
            expect(res.body).to.have.property('timestamp');
            expect(res.body).to.have.property('timestamp').and.not.equal(cacheTimestamp);
            cacheTimestamp = res.body.timestamp;
            done();
          });
        });
      });
    });
  });
});
