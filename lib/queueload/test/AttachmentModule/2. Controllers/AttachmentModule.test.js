/* FileName: AttachmentModule.test.js
* Description: This file defines the unit and integration test cases for the Attachment Module.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 14/09/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var exec = require('child_process').exec;
var create = 'dd if=/dev/zero of=bigfile bs=1024 count=6144';
var purge = 'rm bigfile';

var validAccTkn, validRefTkn, validAttachmentId;

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001"
};

var firstUser =
{
  "firstname":"attchtestbatch",
  "lastname":"attchtesting",
  "username":"attchT3st.bat1",
  "fullname": "attchtestbatch attchtesting",
  "password":"P@55word",
  "emailid":"attchtest.bat1@testclients.com",
  "countrycode":"+91",
  "phonenumber":"9351256745",
  "employeeid":"ATTCHFTE000001"
};

describe('Attachment Module Unit Tests', function()
{
  before('Staging Test Environment', function(done)
  {
    User.findOrCreate({ emailid: "administrator@testdomain.com" }, superUser)
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      else
      {
        return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send({ emailid: superUser.emailid, password: superUser.password });
      }
    })
    .then(function(res)
    {
      var superToken = res.body.acctoken;
      return request(sails.hooks.http.app)
      .post("/user/add")
      .set("Authorization", superToken)
      .send(firstUser);
    })
    .then(function(res)
    {
      return request(sails.hooks.http.app)
      .post("/auth/signin")
      .send({ emailid: firstUser.emailid, password: firstUser.password });
    })
    .then(function(res)
    {
      expect(res.body.acctoken).to.exist;
      expect(res.body.reftoken).to.exist;
      validAccTkn = res.body.acctoken;
      validRefTkn = res.body.reftoken;
      done();
    })
    .catch(done);
  });
  describe('Attachment Controller', function()
  {
    describe('#upload()', function()
    {
      it('should not upload the document(no attachment)', function(done)
      {
        request(sails.hooks.http.app)
        .post('/attachment/upload')
        .set('Authorization', validAccTkn)
        .end(function(err, res) {
          if(err) return done(err);
          expect(500);
          done();
        });
      });

      it('should not upload the document(size>5MB) - 400403', function(done)
      {
        exec(create, function(err)
        {
          if(err) return done(err);
          else
          {
            request(sails.hooks.http.app)
            .post('/attachment/upload')
            .attach('attachment', 'bigfile')
            .set('Authorization', validAccTkn)
            .end(function(err, res) {
              if(err) return done(err);
              expect(res.body.code).to.equal(400403);
              exec(purge, function(err)
              {
                if(err) return done(err);
                else  done();
              });
            });
          }
        });
      });

      it('should upload the document', function(done)
      {
        request(sails.hooks.http.app)
        .post('/attachment/upload')
        .attach('attachment', 'README.md')
        .set('Authorization', validAccTkn)
        .end(function(err, res) {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.have.property('attachmentid');
          validAttachmentId = res.body.attachmentid;
          done();
        });
      });

      it('upload the same document again', function(done)
      {
        request(sails.hooks.http.app)
        .post('/attachment/upload')
        .attach('attachment', 'README.md')
        .set('Authorization', validAccTkn)
        .end(function(err, res) {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.have.property('attachmentid');
          validAttachmentId = res.body.attachmentid;
          done();
        });
      });

    });
    describe('#download()', function()
    {
      it('should not download the attachment(no attachmentid) - 400051', function(done)
      {
        request(sails.hooks.http.app)
        .post('/attachment/download')
        .set('Authorization', validAccTkn)
        .end(function(err, res) {
          if(err) return done(err);
          expect(400);
          expect(res.body).to.have.property('code', 400051);
          done();
        });
      });
      it('should not download the attachment(incorrect attachmentid) - 400402', function(done)
      {
        request(sails.hooks.http.app)
        .post('/attachment/download')
        .set('Authorization', validAccTkn)
        .send({ attachmentid: 23884271 })
        .end(function(err, res) {
          if(err) return done(err);
          expect(400);
          expect(res.body).to.have.property('code', 400402);
          done();
        });
      });
      it('should download the attachment', function(done)
      {
        request(sails.hooks.http.app)
        .post('/attachment/download')
        .set('Authorization', validAccTkn)
        .send({ attachmentid: validAttachmentId })
        .end(function(err, res) {
          if(err) return done(err);
          expect(200);
          expect(res.body).to.have.property('url');
          done();
        });
      });
    });
  });
  after('Signing Out', function(done)
  {
    delete firstUser.password;
    firstUser.reftkn = validRefTkn;
    request(sails.hooks.http.app)
    .post("/auth/signout")
    .send(firstUser)
    .expect(200)
    .end(function(err, res)
    {
      if(err) return done(err);
      done();
    });
  });
});
