/* FileName: Attachment.test.js
 * Description: This file defines all the unit test cases for the Attachment Model.
 *
 *   Date                Change Description                            Author
 * ---------         ---------------------------                     ---------
 * 14/09/2016          Initial file creation                           SMandal
 *
 */

var chai = require("chai");
var expect = chai.expect;

var validAttachementId;

var validAttachmentDetail =
{
   "key": "436230f6978bc3e5af03b18f3af7381a",
   "filename": "avatar_4cce408ca303_128.png",
   "size": 24183,
   "etag": "3d6bee2ca7ef2f35eea3dfdb3aae23d4"
};

var invalidAttachmentDetail_missingKey =
{
  "filename": "avatar_4cce408ca303_128.png",
  "size": 24183,
  "etag": "3d6bee2ca7ef2f35eea3dfdb3aae23d4"
};

var invalidAttachmentDetail_missingName =
{
  "key": "436230f6978bc3e5af03b18f3af7381a",
  "size": 24183,
  "etag": "3d6bee2ca7ef2f35eea3dfdb3aae23d4"
};

var invalidAttachmentDetail_missingSize =
{
  "key": "436230f6978bc3e5af03b18f3af7381a",
  "filename": "avatar_4cce408ca303_128.png",
  "etag": "3d6bee2ca7ef2f35eea3dfdb3aae23d4"
};

var invalidAttachmentDetail_missingEtag =
{
  "key": "436230f6978bc3e5af03b18f3af7381a",
  "filename": "avatar_4cce408ca303_128.png",
  "size": 24183
};

var validAttachmentUpdate =
{
  "key": "436230f6978bc3e5af03b18f3af7381a",
  "filename": "avatar_newid_number.png",
  "size": 37216
};

var invalidAttachmentUpdate_nonExistentAttr =
{
  "sizes": 37216
};

describe('Attachment Model', function()
{
  describe('#Create', function()
  {
    it('should not create an attachment entry(keyMissing)', function()
    {
      return Attachment.create(invalidAttachmentDetail_missingKey)
      .then(function createdAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
      });
    });

    it('should not create an attachment entry(nameMissing)', function()
    {
      return Attachment.create(invalidAttachmentDetail_missingName)
      .then(function createdAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
      });
    });

    it('should not create an attachment entry(sizeMissing)', function()
    {
      return Attachment.create(invalidAttachmentDetail_missingSize)
      .then(function createdAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
      });
    });

    it('should not create an attachment entry(linkMissing)', function()
    {
      return Attachment.create(invalidAttachmentDetail_missingEtag)
      .then(function createdAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
        var numOfAttributes = error.reason.split(" ", 1)[ 0 ];
        expect(numOfAttributes).to.equal("1");
      });
    });

    it('should create an attachment entry', function()
    {
      return Attachment.create(validAttachmentDetail)
      .then(function createdAttachment(attachment)
      {
        expect(attachment).to.exist;
        validAttachementId = attachment.attachmentid;
      })
      .catch();
    });

    it('should not create an attachment entry(dupHash)', function()
    {
      return Attachment.create(validAttachmentDetail)
      .then(function createdAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
      });
    });
  });
  describe('#Update', function()
  {
    it('should not update the attachment entry(non-existent attachment id)', function()
    {
      return Attachment.update({ attachmentid: -1 }, validAttachmentUpdate)
      .then(function updatedAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
      });
    });

    it('should not update the attachment entry(non-existent attribute)', function()
    {
      return Attachment.update({ attachmentid: validAttachementId }, invalidAttachmentUpdate_nonExistentAttr)
      .then(function updatedAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
      });
    });

    it('should update the attachment entry', function()
    {
      return Attachment.update({ attachmentid: validAttachementId }, validAttachmentUpdate)
      .then(function updatedAttachment(attachment)
      {
        expect(attachment).to.exist;
        expect(attachment[ 0 ]).to.have.property('key', '436230f6978bc3e5af03b18f3af7381a');
        expect(attachment[ 0 ]).to.have.property('filename', 'avatar_newid_number.png');
        expect(attachment[ 0 ]).to.have.property('size', '37216');
        expect(attachment[ 0 ]).to.have.property('etag', '3d6bee2ca7ef2f35eea3dfdb3aae23d4');
      })
      .catch(function(error)
      {
        console.log("Error", error);
        expect(error).to.not.exist;
      });
    });
  });
  describe('#Find', function()
  {
    it('should not find the attachment(non-existent id)', function()
    {
      return Attachment.findOne({ attachmentid: 24 })
      .then(function foundAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
      });
    });
    it('should find the attachment', function()
    {
      return Attachment.findOne({ attachmentid: validAttachementId })
      .then(function foundAttachment(attachment)
      {
        expect(attachment).to.exist;
        expect(attachment).to.have.property('key', '436230f6978bc3e5af03b18f3af7381a');
        expect(attachment).to.have.property('filename', 'avatar_newid_number.png');
        expect(attachment).to.have.property('size', '37216');
        expect(attachment).to.have.property('etag', '3d6bee2ca7ef2f35eea3dfdb3aae23d4');
      })
      .catch(function(error)
      {
        expect(error).to.not.exist;
      });
    });
  });
  describe('#Delete', function()
  {
    it('should not delete the attachment(non-existent id)', function()
    {
      return Attachment.destroy({ attachmentid: 24 })
      .then(function destroyedAttachment(attachment)
      {
        expect(attachment).to.not.exist;
      })
      .catch(function(error)
      {
        expect(error).to.exist;
      });
    });
    it('should delete the attachment', function()
    {
      return Attachment.destroy({ attachmentid: validAttachementId })
      .then(function destroyedAttachment(attachment)
      {
        expect(attachment).to.exist;
        expect(attachment[ 0 ]).to.have.property('attachmentid', validAttachementId);
        expect(attachment[ 0 ]).to.have.property('key', '436230f6978bc3e5af03b18f3af7381a');
        expect(attachment[ 0 ]).to.have.property('filename', 'avatar_newid_number.png');
        expect(attachment[ 0 ]).to.have.property('size', '37216');
        expect(attachment[ 0 ]).to.have.property('etag', '3d6bee2ca7ef2f35eea3dfdb3aae23d4');
      })
      .catch(function(error)
      {
        expect(error).to.not.exist;
      });
    });
  });
});
