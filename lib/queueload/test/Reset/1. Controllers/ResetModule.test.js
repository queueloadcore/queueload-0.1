/* FileName: ClientCacheModule.test.js
* Description: This file defines the unit and integration test cases for the ClientCache Module.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 06/10/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;

var company =
{
  "accountid": "qlodtest",
  "name": "Wayne Enterprises",
  "address": "Gotham City",
  "accntadmin": 1,
  "globalconfig": {}
};

var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001",
  "userconfig": {}
};

var resetOperator =
{
  "firstname":"reset",
  "lastname":"operator",
  "fullname":"reset operator",
  "username":"reset.operator",
  "password":"password",
  "emailid":"reset.operator@testdomain.com",
  "countrycode":"+91",
  "phonenumber": "8458753214",
  "employeeid":"FTE245731",
  "userconfig": {}
};

describe("Resetall Controller Test", function()
{
  before("Setting up the stage", function(done)
  {
    Company.findOrCreate({ accountid: company.accountid }, company)
    .then(function(company)
    {
      superUser.accountid = company.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send({ emailid: superUser.emailid, password: superUser.password });
    })
    .then(function(result)
    {
      superUser.acctoken = result.body.acctoken;
      return request(sails.hooks.http.app)
      .post('/user/add')
      .set('Authorization', superUser.acctoken)
      .send(resetOperator);
    })
    .then(function(user)
    {
      return request(sails.hooks.http.app)
      .post('/auth/signin')
      .send({ emailid: resetOperator.emailid, password: resetOperator.password });
    })
    .then(function(result)
    {
      resetOperator.acctoken = result.body.acctoken;
      done();
    })
    .catch(done);
  });
  it("should not reset the system - requester is not a superuser", function(done)
  {
    request(sails.hooks.http.app)
    .post("/reset/resetall")
    .set("Authorization", resetOperator.acctoken)
    .expect(403)
    .end(function(err, res)
    {
      if(err) return done(err);
      expect(res.body.code).to.equal(403502);
      done();
    });
  });
  it("should reset the system", function(done)
  {
    request(sails.hooks.http.app)
    .post("/reset/resetall")
    .set("Authorization", superUser.acctoken)
    .expect(200)
    .end(function(err, res)
    {
      if(err) return done(err);
      done();
    });
  });
  it("super user should be able to login", function(done)
  {
    request(sails.hooks.http.app)
    .post('/auth/signin')
    .send({ emailid: superUser.emailid, password: superUser.password })
    .expect(200)
    .end(function(err, res)
    {
      if(err) return done(err);
      expect(res.body.user.userid).to.equal(superUser.userid);
      expect(res.body.roles.issuperuser).to.equal(true);
      done();
    });
  });
});
