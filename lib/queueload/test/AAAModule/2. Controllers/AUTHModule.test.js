/* FileName: AUTHModule.test.js
* Description: This file defines the unit and integration test cases for the Auth Module.
*
*   Date                Change Description                            Author
* ---------         ---------------------------                     ---------
* 27/07/2016          Initial file creation                           SMandal
*
*/

var request = require("supertest");
var chai = require("chai");
var expect = chai.expect;
var jwt = require("jsonwebtoken");
var bluebird = require("bluebird");
var redis = require("redis");
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var randomBytes = bluebird.promisify(require("crypto").randomBytes);

if(process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'staging')
{
  var client = redis.createClient({ host: 'localhost', port: 6379 });
}
else
{
  console.error("Error: Environment not set. Possible values: test/staging");
  console.error("Exiting test!!");
  process.exit(1);
}

client.on("error", function(err)
  {
    sails.log.debug("Error " + err);
  }
);

var company =
{
  "accountid": "qlodtest",
  "name": "Wayne Enterprises",
  "address": "Gotham City",
  "accntadmin": 1,
  "globalconfig": {}
};
var superUser =
{
  "firstname": "Super",
  "lastname": "User",
  "fullname": "Super User",
  "username": "administrator",
  "password": "password",
  "emailid": "administrator@testdomain.com",
  "employeeid": "FTE000001"
};

var validSignInParams =
{
  "emailid":"authtest.bat1@testclients.com",
  "password":"P@55word"
};

var invalidSignInParams_missingEmail =
{
  "password":"P@55word"
};

var invalidSignInParams_missingPassword =
{
  "emailid":"authtest.bat1@testclients.com"
};

var invalidSignInParams_nonExistentUser =
{
  "emailid":"gatecrasher@testclients.com",
  "password":"G@7e(r@$#3r"
};

var invalidSignInParams_inactiveUser =
{
  "emailid":"authtest.bat2@testclients.com",
  "password":"P@55wordd"
};

var invalidSignInParams_incorrectPassword =
{
  "emailid":"authtest.bat1@testclients.com",
  "password":"incorrectPass"
};

var listParams =
{
  "page": 1,
  "limit": 2
};

var invalidListParams_includesRequesterId =
{
  "page": 1,
  "limit": 2,
  "requesterid": 2
};

var invalidListParams_includesRoles =
{
  "page": 1,
  "limit": 2,
  "userroles": 2
};

var validRefTkn;

var validAccTkn;

var fakeRefTkn;

var validRefTknParams =
{
  "emailid":"authtest.bat1@testclients.com",
  "reftkn":validRefTkn
};

var invalidRefTknParams_incorrectToken =
{
  "emailid":"authtest.bat1@testclients.com",
  "reftkn":'fakeRefTkn'
};

var invalidRefTknParams_missingToken =
{
  "emailid":"authtest.bat1@testclients.com"
};

var invalidRefTknParams_missingEmail =
{
  "incrctAttr":"fakeemail@noemail.com",
  "reftkn":validRefTkn
};

var invalidRefTknParams_inactiveUser =
{
  "emailid":"authtest.bat2@testclients.com",
  "reftkn":validRefTkn
};

var invalidRefTknParams_unsignedInUser =
{
  "emailid":"authtest.bat2@testclients.com",
  "reftkn":"someIrrelevantToken"
};

var validSignOutParams =
{
  "emailid":"authtest.bat1@testclients.com",
  "reftkn":validRefTkn
};

var invalidSignOutParams_incorrectToken =
{
  "emailid":"authtest.bat1@testclients.com",
  "reftkn":'incorrectToken'
};

var invalidSignOutParams_missingEmail =
{
  "incrctAttr":"fakeemail@noemail.com",
  "reftkn":'someIrrelevantToken'
};

var invalidSignOutParams_missingRefTkn =
{
  "emailid":"authtest.bat1@testclients.com"
};

var invalidSignOutParams_unsigneIndUser =
{
  "emailid":"authtest.bat2@testclients.com",
  "reftkn":'someIrrelevantToken'
};

var activeUser_CP =
{
  "firstname":"authtestbatch",
  "lastname":"authtesting",
  "username":"authT3st.bat1",
  "password":"P@55word",
  "emailid":"authtest.bat1@testclients.com",
  "countrycode":"+91",
  "phonenumber":"9536841379",
  "employeeid":"AUTHFTE000001"
};

var inactiveUser_CP =
{
  "firstname":"authtestbatch",
  "lastname":"authtest",
  "username":"authT3st.bat2",
  "password":"P@55wordd",
  "emailid":"authtest.bat2@testclients.com",
  "countrycode":"+91",
  "phonenumber":"9548634572",
  "employeeid":"AUTHFTE000002",
  "isactive":false
};

var userWithNoRoles = {
  "firstname":"fake",
  "lastname":"user",
  "username":"fake.user",
  "password":"P@55word",
  "emailid":"fake.user@testclients.com",
  "employeeid":"FAKEEMPID"
};

var forgetfulUser =
{
  "firstname":"authforget",
  "lastname":"authuser",
  "username":"authforgetfuluser",
  "password":"password",
  "emailid":"smruti.mandal@queueload.com",
  "countrycode":"+91",
  "phonenumber":"9536841379",
  "employeeid":"AUTHFTE008765"
};

describe("Auth Unit Tests", function()
{
  var expiredJwtToken;
  before("Staging Test Environment", function(done)
  {
    Company.findOrCreate( { name: company.name }, company)
    .then(function(result)
    {
      if(!result)
      {
        throw new Error("Couldn't findOrCreate the company");
      }
      company = result;
      superUser.accountid = result.accountid;
      activeUser_CP.accountid = result.accountid;
      inactiveUser_CP.accountid = result.accountid;
      userWithNoRoles.accountid = result.accountid;
      forgetfulUser.accountid = result.accountid;
      return User.findOrCreate({ emailid: superUser.emailid }, superUser);
    })
    .then(function(user)
    {
      if(!user)
      {
        throw new Error("Couldn't findOrCreate the superuser");
      }
      superUser.userid = user.userid;
      return Role.findOrCreate({ userid: superUser.userid }, { userid: superUser.userid, issuperuser: true, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      if(!role)
      {
        throw new Error("Couldn't findOrCreate the superuser role");
      }
      else
      {
        expiredJwtToken = jwt.sign(
        {
          user: activeUser_CP
        },
        sails.config.authcfg.accTknSecret,
        {
          algorithm: sails.config.authcfg.accTknAlgorithm,
          expiresIn: 0,
          issuer: sails.config.authcfg.accTknIssuer,
          audience: sails.config.authcfg.accTknAudience
        });
        return User.create(activeUser_CP);
      }
    })
    .then(function(acUser)
    {
      expect(acUser).to.exist;
      expect(acUser).to.have.property("isactive").and.equal(true);
      return Role.create({ userid: acUser.userid, issuperuser: true, isbdadmin: true, isoperator: true });
    })
    .then(function(role)
    {
      expect(role).to.exist;
      expect(role).to.have.property("issuperuser").and.equal(true);
      expect(role).to.have.property("isbdadmin").and.equal(true);
      expect(role).to.have.property("isoperator").and.equal(true);
      return User.create(inactiveUser_CP);
    })
    .then(function(inUser)
    {
      expect(inUser).to.exist;
      expect(inUser).to.have.property("isactive").and.equal(false);
      return User.create(userWithNoRoles);
    })
    .then(function(user)
    {
      expect(user).to.exist;
      return User.create(forgetfulUser);
    })
    .then(function(user)
    {
      expect(user).to.exist;
      forgetfulUser.userid = user.userid;
      return Role.create({ userid: forgetfulUser.userid, issuperuser: false, isbdadmin: false, isoperator: false });
    })
    .then(function(role)
    {
      expect(role).to.exist;
      return done();
    })
    .catch(done);
  });

  describe("#signin()", function()
  {
    it("should not signin(empty request body) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not signin(missing email) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(invalidSignInParams_missingEmail)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not signin(missing password) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(invalidSignInParams_missingPassword)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not signin(non-existent user) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(invalidSignInParams_nonExistentUser)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not signin(inactive user) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(invalidSignInParams_inactiveUser)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not signin(incorrect password) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(invalidSignInParams_incorrectPassword)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not signin successfully(password expired) - 401508", function(done)
    {
      User.update({ emailid: validSignInParams.emailid }, { passexpired: true })
      .then(function(user)
      {
        return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send(validSignInParams);
      })
      .then(function(res)
      {
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401508);
        return User.update({ emailid: validSignInParams.emailid }, { passexpired: false });
      })
      .then(function(users)
      {
        expect(users[ 0 ]).to.have.property("passexpired").and.equal(false);
        done();
      })
      .catch(done);
    });

    it("should signin successfully", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send(validSignInParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.acctoken).to.exist;
        expect(res.body.reftoken).to.exist;
        expect(res.body.roles).to.exist;
        validAccTkn = res.body.acctoken;
        validRefTkn = res.body.reftoken;
        validRefTknParams.reftkn = res.body.reftoken;
        validSignOutParams.reftkn = res.body.reftoken;
        done();
      });
    });
  });

  describe("#isauthenticated()", function()
  {
    it("should not authenticate(missing access token) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .send(listParams)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not authenticate(invalid JWT) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", "InvalidJWT")
      .send(listParams)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not authenticate(expired token) - 401", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", expiredJwtToken)
      .send(listParams)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401505);
        done();
      });
    });

    it("should not authenticate(includes requesterid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", validAccTkn)
      .send(invalidListParams_includesRequesterId)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not authenticate(includes roles) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", validAccTkn)
      .send(invalidListParams_includesRoles)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not authenticate(user without anyrole) - 500", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signin")
      .send({ "emailid": "fake.user@testclients.com", "password": "P@55word" })
      .expect(500)
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(500001);
        done();
      });
    });

    it("should authenticate successfully", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", validAccTkn)
      .send(listParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.exist.and.have.lengthOf(2);
        done();
      });
    });
  });

  describe("#reftkn()", function()
  {
    it("should not refresh(empty request body) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/reftkn")
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.acctoken).to.not.exist;
        expect(res.body.reftoken).to.not.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not refresh(missing emailid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/reftkn")
      .send(invalidRefTknParams_missingEmail)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.acctoken).to.not.exist;
        expect(res.body.reftoken).to.not.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not refresh(missing token) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/reftkn")
      .send(invalidRefTknParams_missingToken)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.acctoken).to.not.exist;
        expect(res.body.reftoken).to.not.exist;
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not refresh(inactive user) - 401503", function(done)
    {
      randomBytes(32)
      .then(function(bytes)
      {
        fakeRefTkn = bytes.toString("hex");
        return client.hmsetAsync(process.env.ACCOUNTID + ":" + invalidRefTknParams_inactiveUser.emailid, [ "reftkn", fakeRefTkn ]);
      })
      .then(function(token)
      {
        invalidRefTknParams_inactiveUser.reftkn = fakeRefTkn;
        return request(sails.hooks.http.app)
        .post("/auth/reftkn")
        .send(invalidRefTknParams_inactiveUser)
        .expect(401)
        .then(function(res)
        {
          expect(res.body.acctoken).to.not.exist;
          expect(res.body.reftoken).to.not.exist;
          expect(res.body).to.have.property("code").and.equal(401503);
          return client.delAsync(process.env.ACCOUNTID + ":" + invalidRefTknParams_inactiveUser.emailid, [ "reftkn" ]);
        });
      })
      .then(function(token)
      {
        done();
      })
      .catch(function(error)
      {
        done(error);
      });
    });

    it("should not refresh(user not signed-in) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/reftkn")
      .send(invalidRefTknParams_unsignedInUser)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.acctoken).to.not.exist;
        expect(res.body.reftoken).to.not.exist;
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not refresh(token not matching) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/reftkn")
      .send(invalidRefTknParams_incorrectToken)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.acctoken).to.not.exist;
        expect(res.body.reftoken).to.not.exist;
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not refresh successfully(password expired) - 401508", function(done)
    {
      User.update({ emailid: validRefTknParams.emailid }, { passexpired: true })
      .then(function(user)
      {
        return request(sails.hooks.http.app)
        .post("/auth/reftkn")
        .send(validRefTknParams);
      })
      .then(function(res)
      {
        expect(res.body).to.exist;
        expect(res.body).to.have.property("code").and.equal(401508);
        return User.update({ emailid: validRefTknParams.emailid }, { passexpired: false });
      })
      .then(function(users)
      {
        expect(users[ 0 ]).to.have.property("passexpired").and.equal(false);
        done();
      })
      .catch(done);
    });

    it("should refresh successfully", function()
    {
      return request(sails.hooks.http.app)
        .post("/auth/signin")
        .send(validSignInParams)
      .then(function(res)
      {
        validAccTkn = res.body.acctoken;
        validRefTkn = res.body.reftoken;
        validRefTknParams.reftkn = res.body.reftoken;
        validSignOutParams.reftkn = res.body.reftoken;
        return request(sails.hooks.http.app)
          .post("/auth/reftkn")
          .send(validRefTknParams);
      })
      .then(function(res)
      {
        expect(res.body.acctoken).to.exist;
        expect(res.body.reftoken).to.exist;
        expect(res.body.roles).to.exist;
        validAccTkn = res.body.acctoken;
        validRefTkn = res.body.reftoken;
        validRefTknParams.reftkn = res.body.reftoken;
        validSignOutParams.reftkn = res.body.reftoken;
      })
      .catch();
    });
  });

  describe("#forgotpass", function()
  {
    it("should not generate token(missing email) 400051", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/forgotpass')
        .send()
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      })
      .catch();
    });

    it("should not generate token(invalid email) 401503", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/forgotpass')
        .send({ emailid: 'gatecrasher@nodomain.com' })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(401503);
      })
      .catch();
    });

    it("should generate token", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/forgotpass')
        .send({ emailid: forgetfulUser.emailid })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
      })
      .catch();
    });
  });

  describe("#setpass", function()
  {
    it("should not set password(empty input) - 400051", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send()
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      })
      .catch();
    });

    it("should not set password(no email) - 400051", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send({ token: "irrelevant7)k39", newpass: "dontbother" })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      })
      .catch();
    });

    it("should not set password(no token) - 400051", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send({ emailid: forgetfulUser.emailid, newpass: "dontbother" })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      })
      .catch();
    });

    it("should not set password(no password) - 400051", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send({ emailid: forgetfulUser.emailid, token: "irrelevant7)k39" })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(400051);
      })
      .catch();
    });

    it("should not set password(non-existent email) - 401503", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send({ emailid: "gatecrasher@nodomain.com", token: "irrelevant7)k39", newpass: 'password' })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(401503);
      })
      .catch();
    });

    it("should not set password(incorrect token) - 401503", function()
    {
      return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send({ emailid: forgetfulUser.emailid, token: "incorrectToken", newpass: 'password' })
      .then(function(res)
      {
        expect(res.body).to.have.property("code").and.equal(401503);
      })
      .catch();
    });

    it("should set password", function(done)
    {
      client.getAsync(process.env.ACCOUNTID + ":" + 'passsettkn:' + forgetfulUser.emailid)
      .then(function(token)
      {
        return request(sails.hooks.http.app)
        .post('/auth/setpass')
        .send({ emailid: forgetfulUser.emailid, token: token, newpass: 'password' });
      })
      .then(function(res)
      {
        expect(res.status).to.equal(200);
        done();
      });
    });
  });

  describe('#resetpass', function()
  {
    it("should not reset the password(empty input) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send()
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });

    it("should not reset the password(missing email) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ oldpass: 'oldpass', newpass: 'newpass' })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });

    it("should not reset the password(missing oldpass) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ emailid: forgetfulUser.emailid, newpass: 'newpass' })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });

    it("should not reset the password(missing newpass) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ emailid: forgetfulUser.emailid, oldpass: 'oldpass' })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });

    it("should not reset the password(same pass) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ emailid: forgetfulUser.emailid, oldpass: 'samepass', newpass: 'samepass' })
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(400051);
        done();
      });
    });

    it("should not reset the password(non-existent email) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ emailid: 'gatecrasher@nodomain.com', oldpass: 'oldpass', newpass: 'newpass' })
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(401503);
        done();
      });
    });

    it("should not reset the password(incorrect old password) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ emailid: forgetfulUser.emailid, oldpass: 'oldpass', newpass: 'newpass' })
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body.code).to.equal(401503);
        done();
      });
    });

    it("should reset the password", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/resetpass")
      .send({ emailid: forgetfulUser.emailid, oldpass: 'password', newpass: 'password1' })
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.status).to.equal(200);
        done();
      });
    });
  });

  describe("#signout()", function()
  {
    it("should not signout(empty request body) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signout")
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not signout(missing emailid) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signout")
      .send(invalidSignOutParams_missingEmail)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not signout(missing token) - 400051", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signout")
      .send(invalidSignOutParams_missingRefTkn)
      .expect(400)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(400051);
        done();
      });
    });

    it("should not signout(email doesn't exist in redis) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signout")
      .send(invalidSignOutParams_unsigneIndUser)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should not signout(token doesn't match) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signout")
      .send(invalidSignOutParams_incorrectToken)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });

    it("should signout successfully", function(done)
    {
      request(sails.hooks.http.app)
      .post("/auth/signout")
      .send(validSignOutParams)
      .expect(200)
      .end(function(err, res)
      {
        if(err) return done(err);
        done();
      });
    });
    it("should not call api(user signed out) - 401503", function(done)
    {
      request(sails.hooks.http.app)
      .post("/user/list")
      .set("Authorization", validAccTkn)
      .send(listParams)
      .expect(401)
      .end(function(err, res)
      {
        if(err) return done(err);
        expect(res.body).to.have.property("code").and.equal(401503);
        done();
      });
    });
  });
});
