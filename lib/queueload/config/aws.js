module.exports.awscfg = 
{
  s3region: process.env.AWSREGION,
  s3apiversion: process.env.S3APIVERSION,
  attachmentbucket: process.env.ACCOUNTID,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  accountId: process.env.AWS_ACCOUNTID
};