/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  if(process.env.ACCOUNTID !== undefined)
  {
    cb();
  }
  else
  {
    console.error('Could not determine account!');
    console.error('Please set up a valid account before starting sails.');
    console.error('To resolve this, run:');
    console.error('export ACCOUNTID=<accountid>');
    var error = "Can't find account";
    cb(error);
  }
};
