/*
*	Added to take care of occasional extremely slow response time from AWS
*/
module.exports.orm = {
  _hookTimeout: 60000
};
