/*
*	Added to take care of occasional extremely slow response time from AWS
*/
module.exports.pubsub = {
  _hookTimeout: 60000
};
