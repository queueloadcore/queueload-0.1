/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  //'/': {
  //  view: 'homepage'
  //},

  '/' : {response: 'notFound'},
  // AAA 
  'POST /auth/signin' : {controller: 'auth', action: 'signin', skipAssets: true},
  'POST /auth/reftkn' : {controller: 'auth', action: 'reftkn', skipAssets: true},
  'POST /auth/signout' : {controller: 'auth', action: 'signout', skipAssets: true},
  'POST /auth/resetpass' : {controller: 'auth', action: 'resetpass', skipAssets: true},
  'POST /auth/forgotpass' : {controller: 'auth', action: 'forgotpass', skipAssets: true},
  'POST /auth/setpass' : {controller: 'auth', action: 'setpass', skipAssets: true},
  // User 
  'POST /user/controlTest' : {controller: 'user', action: 'controlTest', skipAssets: true},
  'POST /user/list' : {controller: 'user', action: 'list', skipAssets: true},
  'POST /user/add' : {controller: 'user', action: 'add', skipAssets: true},
  'POST /user/update' : {controller: 'user', action: 'update', skipAssets: true},
  'POST /user/getdetail' : {controller: 'user', action: 'getdetail', skipAssets: true},
  'POST /user/search' : {controller: 'user', action: 'search', skipAssets: true},
  'POST /user/activation' : {controller: 'user', action: 'activation', skipAssets: true},
  'POST /user/getstatus' : {controller: 'user', action: 'getstatus', skipAssets: true},
  'POST /user/makesuperuser' : {controller: 'user', action: 'makesuperuser', skipAssets: true},
  'POST /user/setconfig' : {controller: 'user', action: 'setconfig', skipAssets: true},
  'POST /user/getconfig' : {controller: 'user', action: 'getconfig', skipAssets: true},
  'POST /user/resetconfig' : {controller: 'user', action: 'resetconfig', skipAssets: true},
  //// BD 
  'POST /bd/controlTest' : {controller: 'bd', action: 'controlTest', skipAssets: true},
  'POST /bd/list' : {controller: 'bd', action: 'list', skipAssets: true},
  'POST /bd/add' : {controller: 'bd', action: 'add', skipAssets: true},
  'POST /bd/assign' : {controller: 'bd', action: 'assign', skipAssets: true},
  'POST /bd/activation' : {controller: 'bd', action: 'activation', skipAssets: true},
  'POST /bd/getstatus' : {controller: 'bd', action: 'getstatus', skipAssets: true},
  'POST /bd/getdetail' : {controller: 'bd', action: 'getdetail', skipAssets: true},
  'POST /bd/search' : {controller: 'bd', action: 'search', skipAssets: true},
  // Cb 
  'POST /cb/controlTest' : {controller: 'cb', action: 'controlTest', skipAssets: true},
  'POST /cb/list' : {controller: 'cb', action: 'list', skipAssets: true},
  'POST /cb/add' : {controller: 'cb', action: 'add', skipAssets: true},
  'POST /cb/activation' : {controller: 'cb', action: 'activation', skipAssets: true},
  'POST /cb/getstatus' : {controller: 'cb', action: 'getstatus', skipAssets: true},
  'POST /cb/getdetail' : {controller: 'cb', action: 'getdetail', skipAssets: true},
  'POST /cb/search' : {controller: 'cb', action: 'search', skipAssets: true},
  // Ci Detail 
  'POST /cidetail/list' : {controller: 'cidetail', action: 'list', skipAssets: true},
  'POST /cidetail/getdetail' : {controller: 'cidetail', action: 'getdetail', skipAssets: true},
  'POST /cidetail/add' : {controller: 'cidetail', action: 'add', skipAssets: true},
  'POST /cidetail/update' : {controller: 'cidetail', action: 'update', skipAssets: true},
  'POST /cidetail/next' : {controller: 'cidetail', action: 'next', skipAssets: true},
  'POST /cidetail/cancel' : {controller: 'cidetail', action: 'cancel', skipAssets: true},
  'POST /cidetail/search' : {controller: 'cidetail', action: 'search', skipAssets: true},
  'POST /cidetail/makeserviceprimary' : {controller: 'cidetail', action: 'makeserviceprimary', skipAssets: true},
  // Ci Env 
  'POST /cienvironment/list' : {controller: 'cienvironment', action: 'list', skipAssets: true},
  'POST /cienvironment/add' : {controller: 'cienvironment', action: 'add', skipAssets: true},
  'POST /cienvironment/update' : {controller: 'cienvironment', action: 'update', skipAssets: true},
  // Ci Hardware 
  'POST /cihardware/list' : {controller: 'cihardware', action: 'list', skipAssets: true},
  'POST /cihardware/add' : {controller: 'cihardware', action: 'add', skipAssets: true},
  'POST /cihardware/getdetail' : {controller: 'cihardware', action: 'getdetail', skipAssets: true},
  'POST /cihardware/update' : {controller: 'cihardware', action: 'update', skipAssets: true},
  'POST /cihardware/search' : {controller: 'cihardware', action: 'search', skipAssets: true},
  // Ci Location
  'POST /cilocation/list' : {controller: 'cilocation', action: 'list', skipAssets: true},
  'POST /cilocation/add' : {controller: 'cilocation', action: 'add', skipAssets: true},
  'POST /cilocation/update' : {controller: 'cilocation', action: 'update', skipAssets: true},
  'POST /cilocation/search' : {controller: 'cilocation', action: 'search', skipAssets: true},
  // Ci Network
  'POST /cinetwork/list' : {controller: 'cinetwork', action: 'list', skipAssets: true},
  'POST /cinetwork/add' : {controller: 'cinetwork', action: 'add', skipAssets: true},
  'POST /cinetwork/getdetail' : {controller: 'cinetwork', action: 'getdetail', skipAssets: true},
  'POST /cinetwork/update' : {controller: 'cinetwork', action: 'update', skipAssets: true},
  'POST /cinetwork/search' : {controller: 'cinetwork', action: 'search', skipAssets: true},
  // Ci Type
  'POST /citype/controlTest' : {controller: 'citype', action: 'controlTest', skipAssets: true},
  'POST /citype/list' : {controller: 'citype', action: 'list', skipAssets: true},
  'POST /citype/add' : {controller: 'citype', action: 'add', skipAssets: true},
  'POST /citype/update' : {controller: 'citype', action: 'update', skipAssets: true},
  // Client clientcache
  'POST /clientcache/getalldata' : {controller: 'clientcache', action: 'getalldata', skipAssets: true},
  'POST /clientcache/isvalid' : {controller: 'clientcache', action: 'isvalid', skipAssets: true},
  // Company
  'POST /company/update' : {controller: 'company', action: 'update', skipAssets: true},
  'POST /company/logglyalertmap' : {controller: 'company', action: 'logglyalertmap', skipAssets: true},
  'POST /company/listintegrations' : {controller: 'company', action: 'listintegrations', skipAssets: true},
  'POST /company/getintegrationinfo' : {controller: 'company', action: 'getintegrationinfo', skipAssets: true},
  'POST /company/addintegration' : {controller: 'company', action: 'addintegration', skipAssets: true},
  'POST /company/updateintegration' : {controller: 'company', action: 'updateintegration', skipAssets: true},
  'POST /company/removeintegration' : {controller: 'company', action: 'removeintegration', skipAssets: true},
  // Incident
  'POST /incident/list' : {controller: 'incident', action: 'list', skipAssets: true},
  'POST /incident/add' : {controller: 'incident', action: 'add', skipAssets: true},
  'POST /incident/update' : {controller: 'incident', action: 'update', skipAssets: true},
  'POST /incident/cancel' : {controller: 'incident', action: 'cancel', skipAssets: true},
  //'POST /incident/relatetickets' : {controller: 'incident', action: 'relatetickets', skipAssets: true},
  'POST /incident/getdetail' : {controller: 'incident', action: 'getdetail', skipAssets: true},
  'POST /incident/userlist' : {controller: 'incident', action: 'userlist', skipAssets: true},
  'POST /incident/queuelist' : {controller: 'incident', action: 'queuelist', skipAssets: true},
  'POST /incident/search' : {controller: 'incident', action: 'search', skipAssets: true},
  'POST /incident/addbytemplate' : {controller: 'incident', action: 'addbytemplate', skipAssets: true},
  'POST /incident/assocalarms' : {controller: 'incident', action: 'assocalarms', skipAssets: true},
  // Org Approval
  'POST /orgapproval/list' : {controller: 'orgapproval', action: 'list', skipAssets: true},
  'POST /orgapproval/initiatetransfer' : {controller: 'orgapproval', action: 'initiatetransfer', skipAssets: true},
  'POST /orgapproval/approve' : {controller: 'orgapproval', action: 'approve', skipAssets: true},
  // Queue
  'POST /queue/list' : {controller: 'queue', action: 'list', skipAssets: true},
  'POST /queue/add' : {controller: 'queue', action: 'add', skipAssets: true},
  'POST /queue/update' : {controller: 'queue', action: 'update', skipAssets: true},
  'POST /queue/memberassoc' : {controller: 'queue', action: 'memberassoc', skipAssets: true},
  'POST /queue/activation' : {controller: 'queue', action: 'activation', skipAssets: true},
  'POST /queue/getdetail' : {controller: 'queue', action: 'getdetail', skipAssets: true},
  'POST /queue/getstatus' : {controller: 'queue', action: 'getstatus', skipAssets: true},
  'POST /queue/search' : {controller: 'queue', action: 'search', skipAssets: true},
  // Service
  'POST /service/list' : {controller: 'service', action: 'list', skipAssets: true},
  'POST /service/add' : {controller: 'service', action: 'add', skipAssets: true},
  'POST /service/getdetail' : {controller: 'service', action: 'getdetail', skipAssets: true},
  'POST /service/update' : {controller: 'service', action: 'update', skipAssets: true},
  'POST /service/next' : {controller: 'service', action: 'next', skipAssets: true},
  'POST /service/approve' : {controller: 'service', action: 'approve', skipAssets: true},
  'POST /service/assoc' : {controller: 'service', action: 'assoc', skipAssets: true},
  'POST /service/cancel' : {controller: 'service', action: 'cancel', skipAssets: true},
  'POST /service/listapproval' : {controller: 'service', action: 'listapproval', skipAssets: true},
  'POST /service/search' : {controller: 'service', action: 'search', skipAssets: true},
  'POST /service/queueassign' : {controller: 'service', action: 'queueassign', skipAssets: true},  
  // Template
  'POST /template/list' : {controller: 'template', action: 'list', skipAssets: true},
  'POST /template/add' : {controller: 'template', action: 'add', skipAssets: true},
  'POST /template/getdetail' : {controller: 'template', action: 'getdetail', skipAssets: true},
  'POST /template/update' : {controller: 'template', action: 'update', skipAssets: true},
  'POST /template/delete' : {controller: 'template', action: 'delete', skipAssets: true},
  'POST /template/search' : {controller: 'template', action: 'search', skipAssets: true},
  // Vendor
  'POST /vendor/list' : {controller: 'vendor', action: 'list', skipAssets: true},
  'POST /vendor/add' : {controller: 'vendor', action: 'add', skipAssets: true},
  'POST /vendor/update' : {controller: 'vendor', action: 'update', skipAssets: true},
  // Attachment
  'POST /attachment/upload' : {controller: 'attachment', action: 'upload', skipAssets: true},
  'POST /attachment/download' : {controller: 'attachment', action: 'download', skipAssets: true},
  // Reset
  'POST /reset/resetall' : {controller: 'reset', action: 'resetall', skipAssets: true},
  // Subscribe Actions
  'POST /subscribe/subscribetoalarms' : {controller: 'subscribe', action: 'subscribetoalarms', skipAssets: true},
  'POST /subscribe/subscribetoincidents' : {controller: 'subscribe', action: 'subscribetoincidents', skipAssets: true},
  // Alarms
  'POST /alarm/acknowledge' : {controller: 'alarm', action: 'acknowledge', skipAssets: true},
  'POST /alarm/search' : {controller: 'alarm', action: 'search', skipAssets: true},
  ///***************************************************************************
  //*                                                                          *
  //* Custom routes here...                                                    *
  //*                                                                          *
  //* If a request to a URL doesn't match any of the custom routes above, it   *
  //* is matched against Sails route blueprints. See `config/blueprints.js`    *
  //* for configuration options and examples.                                  *
  //*                                                                          *
  //***************************************************************************/
};
