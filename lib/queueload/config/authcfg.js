module.exports.authcfg = 
{
  accTknSecret: process.env.TKNSECRET || "-Y*fnR(md_]Jzk(bU4TP)fNF`vAC=cD6k8}UJ;hQg3s6A`Y[%PzD5H-j66TFjxD",
  accTknIssuer: "www.queueload.com",
  accTknAudience: "www.queueload.com",
  accTknAlgorithm: "HS256",
  accExpiresIn: process.env.ACCEXPIRY || 21600,
  refTknkAlgorithm: "sha256",
  refTknLen: 64,
  refExpiresIn: process.env.REFEXPIRY || 86400,
  saltFactor: 10
};