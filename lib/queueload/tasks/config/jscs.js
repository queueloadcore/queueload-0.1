module.exports = function(grunt) {

  grunt.config.init(
    {
      jscs:
      {
        main:
        {
          src: "api/**/*.js",
          filter: 'isFile',
          options:
          {
            config: ".jscsrc",
            fix: true // Autofix code style violations when possible.
          }
        },
        test:
        {
          src: "test/**/*.test.js",
          filter: 'isFile',
          options:
          {
            config: ".jscsrc",
            fix: true // Autofix code style violations when possible.
          }
        }
      }
    });

  grunt.loadNpmTasks("grunt-jscs");
};
