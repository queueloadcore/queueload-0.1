/* FileName: _readme.txt
* Description: This file desribes the source code tree of the project.
* 
* Date				Change Description							Author
* ---------			---------------------------						-------
* 04/12/2015			Initial file creation							SMandal
* 06/12/2015			Development Dependency							SMandal
*
*/			 

Dependencies:
nodejs		4.2.3
sails		0.11.2
