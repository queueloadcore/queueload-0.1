#!/bin/bash
if [ ! -d /home/ubuntu/queueload-0.1 ]
then
echo "Project directory doesn't exist. Cloning the repository."
git clone git@bitbucket.org:29bones/queueload-0.1.git
fi
cd /home/ubuntu/queueload-0.1;
git checkout master;
git pull;
cd /home/ubuntu/queueload-0.1/lib/queueload;
sudo npm install
if [ $? != 0 ]
  then
  echo "npm install failed!!!!!!!!!! Please Check."
  exit 1;
fi

currimageid=$(sudo docker images --format "{{.ID}}" | awk 'NR==1');
echo "Current Image $currimageid";
sudo docker build -t local/qlapibroker:`git rev-parse HEAD | cut -c 1-7` .;
if [ $? != 0 ]
  then
  echo "Docker build failed!!!!!!!!!! Please Check."
  exit 1;
fi
sudo docker tag local/qlapibroker:`git rev-parse HEAD | cut -c 1-7` 741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
ecrlogincmd=$(aws ecr get-login --region ap-southeast-1);
eval $ecrlogincmd;
sudo docker push 741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
if [ $? != 0 ]
  then
  echo "Uploading to docker library failed!!!!!!!!!! Please Check."
  exit 1;
fi
sudo docker rmi local/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;


aws s3 cp dockerenv.lst s3://qlsetupconfig/dockerenv.lst;
newimage=741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
echo $newimage > dockerimg;
aws s3 cp dockerimg s3://qlsetupconfig/dockerimg;
rm dockerimg;

newimageid=$(sudo docker images --format "{{.ID}}" | awk 'NR==1');
echo "New Image $newimageid";

if [ -z $currimageid ]
then
  echo "Cleaning up";
  docker rmi node:4.2.3;
else
  echo "Cln check";
  if [ $currimageid != $newimageid ]
  then
    echo "Cleaning up";
    echo "Current Image $currimageid";
    echo "New Image $newimageid";
    docker rmi $currimageid;
    docker rmi node:4.2.3;
  fi;
fi
