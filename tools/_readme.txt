/* FileName: _readme.txt
* Description: This file lists all the tools used in this project.
* 
* Date                          Change Description                                                      Author
* ---------                     ---------------------------                                             -------
* 04/12/2015                    Initial file creation                                                   SMandal
*
*/                       

Name				Description
------------			-------------------
appmon.sh			This scripts monitors resource usage of sails, pgsql and the system.
