#!/bin/bash
if [ -z ${PROJDIR+x} ]
  then
  echo "Environment variable PROJDIR is not set!!"
  exit 1
fi
cd $PROJDIR/lib/queueload/
export NODE_ENV=test
export ACCOUNTID='qlodtest'
npm test
if [ $? != 0 ]
  then
  echo "Test failed!!!!!!!!!! Please review the code."
  exit 1;
else
  echo "Test passed"
fi
grunt jscs
if [ $? != 0 ]
  then
  echo "Linting failed!!!!!!!!!! Please review the code."
  exit 1;
else
  echo "Linting passed"
fi
$PROJDIR/lib/queueload/node_modules/apidoc/bin/apidoc -i $PROJDIR/lib/queueload/api/controllers/ -o $PROJDIR/docs/apiBrokerDocs/ -c $PROJDIR/docs/apiBrokerDocs/
if [ $? != 0 ]
  then
  echo "API docs generations failed!!!!!!!!!! Please review."
  exit 1;
else
  echo "API docs genetation passed"
fi
sudo rm -rf /var/www/apidocs.queueload.com/html/*
sudo cp -Rp $PROJDIR/docs/apiBrokerDocs/* /var/www/apidocs.queueload.com/html/
echo "############################Backing up the schema of development database##########################"
read -p "Enter the database name:[Default: qlodtest. Hit enter to accept or enter the DB name] " DB_NAME
DB_NAME=${DB_NAME:-qlodtest}
read -p "Enter the username:[Default: qlodtest. Hit enter to accept or enter the user name] " DB_USER
DB_USER=${DB_USER:-qlodtest}
if [ $DB_USER == 'qlodtest' ]
  then
  echo "Using the default password for $DB_USER"
  DB_PASS=${DB_PASS:-qlodtest}
else
  read -p "Enter the password for $DB_USER: " -s DB_PASS
fi
read -p "Enter the location to store the dump file:[Default: $PROJDIR/tools/dbscripts/db_initialization/rollout/schema_dump.sql. Hit enter to accept or enter location] " DUMP_LOC
DUMP_LOC=${DUMP_LOC:-$PROJDIR/tools/dbscripts/db_initialization/rollout/schema_dump.sql}
echo ""
echo 127.0.0.1:5432:$DB_NAME:$DB_USER:$DB_PASS > ~/.pgpass
chmod 600 ~/.pgpass
echo "###########################Configuration being used for backup######################################"
echo "DATABASE=$DB_NAME"
echo "DB USER=$DB_USER"
echo "DB PASSWORD=******"
echo "DB HOST=127.0.0.1"
echo "PORT=5432"
echo "SCHEMA=public"
echo "DUMP LOCATION=$DUMP_LOC"
psql -h 127.0.0.1 -p 5432 -d $DB_NAME -s -U $DB_USER -w -t -q -c "select 'DROP TABLE IF EXISTS \"' || tablename || '\" CASCADE;' FROM pg_tables WHERE SCHEMANAME='public'" -o $DUMP_LOC;
pg_dump -h 127.0.0.1 -p 5432 -n public -d $DB_NAME -s -U $DB_USER -x -w >> $DUMP_LOC
if [ $? != 0 ]
  then
  echo "Dump generation failed!!!!!!!!!! Please Check."
  exit 1;
fi
echo -e "--\n-- Company Addition\n--\n" >> $DUMP_LOC
echo "INSERT INTO companies (accountid, name, accntadmin, supportmgr, isactive, \"createdAt\", \"updatedAt\") VALUES ('qlodtest', 'First Customer', 1, 'support@queueload.com', true, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));" >> $DUMP_LOC
echo -e "--\n-- Super User Addition with Role\n--\n" >> $DUMP_LOC
echo "INSERT INTO users (fullname, username, password, emailid, countrycode, phonenumber, employeeid, isactive, accountid, passexpired, \"createdAt\", \"updatedAt\")
VALUES ('Super User', 'administrator', '\$2a\$10\$WkeZ2NZGu/bSOyccHiCa2emkIEwn6X.T2tTjhPft0ot5lAGkBFke2', 'administrator@testdomain.com', '+91', '9999999999', 'FTE000001', true, 'qlodtest', false, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));
" >> $DUMP_LOC
echo "INSERT INTO roles (userid, issuperuser, isbdadmin, isoperator, \"createdAt\", \"updatedAt\")
VALUES (1, true, false, false, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));
" >> $DUMP_LOC
echo "Dump stored in $DUMP_LOC"
aws s3 cp $DUMP_LOC s3://qlsetupconfig/schema_dump.sql
echo "Schema dump uploaded to AWS S3 bucket qlsetupconfig"
rm ~/.pgpass
