#!/bin/bash
read -p "Enter the number of desired containers:[Enter to accept or enter a postive number] " NUM_INSTANCES
re='^[0-9]+$'
if ! [[ $NUM_INSTANCES =~ $re ]] ; then
   echo "Not a postive number" >&2; exit 1
fi
if [ $NUM_INSTANCES -gt 3 ]
then
  echo "The desired number of containers must be a positive number. Please retry. NOTE: LB is configured for max 3.";
  exit; 
fi
NUM_INSTANCES=${NUM_INSTANCES:-3}

read -p "Do you want to recreate the database from the schema?[Default: NO. Input is case sensitive.]: (YES/NO)] " DB_RECREATE
DB_RECREATE=${DB_RECREATE:-NO}
if [ $DB_RECREATE == 'YES' ]
then
  DB_RECREATE='';
  read -p "This will delete all data!! Are you sure?[Default: no. Input is case sensitive.]: (recreate/no)] " DB_RECREATE
  DB_RECREATE=${DB_RECREATE:-RECREATE}
  read -p "Please enter the database hostname[Default: qlrds1.ckhrxytuacwp.ap-south-1.rds.amazonaws.com]: " DB_HOST
  DB_HOST=${DB_HOST:-qlrds1.ckhrxytuacwp.ap-south-1.rds.amazonaws.com}
  read -p "Please enter the port[Default: 5432]: " DB_PORT
  DB_PORT=${DB_PORT:-5432}
  read -p "Enter the database name:[Default: dq0q4y34. Hit enter to accept or enter the DB name] " DB_NAME
  DB_NAME=${DB_NAME:-dq0q4y34}
  read -p "Enter the username:[Default: dq0q4y34. Hit enter to accept or enter the user name] " DB_USER
  DB_USER=${DB_USER:-dq0q4y34}
  if [ $DB_USER == 'dq0q4y34' ]
    then
    echo "Using the default password for $DB_USER"
    DB_PASS=${DB_PASS:-j0NjnATo3VyPi5tXhTO3dXEXhz8Jce6b}
  else
    read -p "Enter the password for $DB_USER: " -s DB_PASS
  fi
  echo $DB_HOST:$DB_PORT:$DB_NAME:$DB_USER:$DB_PASS > ~/.pgpass
  chmod 600 ~/.pgpass
fi

if [ ! -d /home/ubuntu/queueload-0.1 ]
then
echo "Project directory doesn't exist. Cloning the repository."
git clone git@bitbucket.org:29bones/queueload-0.1.git
fi
cd /home/ubuntu/queueload-0.1;
git checkout queueload-dev;
git pull;
cd /home/ubuntu/queueload-0.1/lib/queueload;
sudo npm install

if [ $DB_RECREATE == 'recreate' ]
then
  echo "#################TESTING THE DATABASE CONNECTIVITY################"
  nc -z -w 1 $DB_HOST $DB_PORT
  if [ $? -ne 0 ]
  then
    echo "Not able to connect to the database!!!!";
    exit 1;
  else
    echo "Database is reachable"
    echo "#############RECREATING THE DATABASE########################"
    cat /home/ubuntu/queueload-0.1/tools/dbscripts/db_initialization/rollout/schema_dump.sql | psql -h $DB_HOST -U $DB_USER -d $DB_NAME -w
    rm ~/.pgpass
  fi
fi
currimage=$(docker ps --format "{{.Image}}" | awk 'NR==1');
currimageid=$(docker images $currimage --format "{{.ID}}" | awk 'NR==1');
echo "Current Image $currimageid";
docker build -t local/qlapibroker:`git rev-parse HEAD | cut -c 1-7` .;
docker tag local/qlapibroker:`git rev-parse HEAD | cut -c 1-7` 741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
ecrlogincmd=$(aws ecr get-login --region ap-southeast-1);
eval $ecrlogincmd;
docker push 741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
docker rmi 741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
docker rmi local/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;

iter=0;
hostport=1420;

aws s3 cp s3://qlsetupconfig/dockerenv.lst dockerenv.lst;

while [ $iter -lt $NUM_INSTANCES ]
do
  echo 'Working on broker-'$iter;
  docker stop 'broker-'$iter;
  docker rm 'broker-'$iter;
  docker run --name 'dq0q4y34'-$iter -d -e ACCOUNTID='dq0q4y34' --env-file dockerenv.lst --restart always -p $hostport:1337 741241887808.dkr.ecr.ap-southeast-1.amazonaws.com/qlapibroker:`git rev-parse HEAD | cut -c 1-7`;
  constatus=$(docker ps --format "{{.Names}}: {{.Status}}" | grep broker-$iter | awk '{ print $2 }');
  if [ $constatus != 'Up' ]
  then
    echo 'broker-$iter did not start properly';
    echo 'Status' $constatus;
    break;
  fi
  iter=`expr $iter + 1`;
  hostport=`expr $hostport + 1`;
done;

rm dockerenv.lst;

newimage=$(docker ps --format "{{.Image}}" | awk 'NR==1');
newimageid=$(docker images $newimage --format "{{.ID}}" | awk 'NR==1');
echo "New Image $newimageid";

if [ -z $currimage ]
then
  echo "Cleaning up";
  docker rmi node:4.2.3;
else
  echo "Cln check";
  if [ $currimageid != $newimageid ]
  then
    echo "Cleaning up";
    echo "Current Image $currimageid";
    echo "New Image $newimageid";
    docker rmi $currimageid;
    docker rmi node:4.2.3;
  fi;
fi
