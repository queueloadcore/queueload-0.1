CREATE ROLE qlodtest;
ALTER ROLE qlodtest WITH PASSWORD 'qlodtest';
alter ROLE qlodtest LOGIN;
create DATABASE qlodtest;
REVOKE CONNECT ON DATABASE qlodtest FROM PUBLIC;
GRANT CONNECT ON DATABASE qlodtest TO qlodtest;
