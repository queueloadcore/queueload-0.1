 DROP TABLE IF EXISTS "alarms" CASCADE;
 DROP TABLE IF EXISTS "attachments" CASCADE;
 DROP TABLE IF EXISTS "cihardwares" CASCADE;
 DROP TABLE IF EXISTS "incidents" CASCADE;
 DROP TABLE IF EXISTS "roles" CASCADE;
 DROP TABLE IF EXISTS "vendors" CASCADE;
 DROP TABLE IF EXISTS "alarm_id__incident_alarmids" CASCADE;
 DROP TABLE IF EXISTS "businessdepartments" CASCADE;
 DROP TABLE IF EXISTS "cilocations" CASCADE;
 DROP TABLE IF EXISTS "inctemplates" CASCADE;
 DROP TABLE IF EXISTS "services" CASCADE;
 DROP TABLE IF EXISTS "attachment_incidents__incident_incidentid" CASCADE;
 DROP TABLE IF EXISTS "attachment_attachmentid__inctemplate_attachments" CASCADE;
 DROP TABLE IF EXISTS "chargebackcodes" CASCADE;
 DROP TABLE IF EXISTS "integrationconfigs" CASCADE;
 DROP TABLE IF EXISTS "cinetworks" CASCADE;
 DROP TABLE IF EXISTS "serviceapprovals" CASCADE;
 DROP TABLE IF EXISTS "bd_admins__user_administeredbds" CASCADE;
 DROP TABLE IF EXISTS "queue_members__user_queuesenrolled" CASCADE;
 DROP TABLE IF EXISTS "cidetails" CASCADE;
 DROP TABLE IF EXISTS "citypes" CASCADE;
 DROP TABLE IF EXISTS "organizationapprovals" CASCADE;
 DROP TABLE IF EXISTS "authtokens" CASCADE;
 DROP TABLE IF EXISTS "cidetail_serviceids__service_ciids" CASCADE;
 DROP TABLE IF EXISTS "service_crntsrvsundappvl__serviceapproval_crntsrvids" CASCADE;
 DROP TABLE IF EXISTS "attachment_attachmentid__incident_attachments" CASCADE;
 DROP TABLE IF EXISTS "cienvironments" CASCADE;
 DROP TABLE IF EXISTS "companies" CASCADE;
 DROP TABLE IF EXISTS "queues" CASCADE;
 DROP TABLE IF EXISTS "users" CASCADE;
 DROP TABLE IF EXISTS "service_newsrvsundappvl__serviceapproval_newserviceids" CASCADE;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alarm_id__incident_alarmids; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE alarm_id__incident_alarmids (
    id integer NOT NULL,
    incident_alarmids integer,
    alarm_id integer
);


ALTER TABLE alarm_id__incident_alarmids OWNER TO qlodtest;

--
-- Name: alarm_id__incident_alarmids_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE alarm_id__incident_alarmids_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarm_id__incident_alarmids_id_seq OWNER TO qlodtest;

--
-- Name: alarm_id__incident_alarmids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE alarm_id__incident_alarmids_id_seq OWNED BY alarm_id__incident_alarmids.id;


--
-- Name: alarms; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE alarms (
    id integer NOT NULL,
    type integer,
    ci text,
    service text,
    "reportedAt" text,
    metric text,
    component text,
    summary text,
    state integer,
    region text,
    accountid text,
    name text,
    source integer,
    isack boolean,
    incidentid integer,
    ackby integer,
    rawdata json,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE alarms OWNER TO qlodtest;

--
-- Name: alarms_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE alarms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarms_id_seq OWNER TO qlodtest;

--
-- Name: alarms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE alarms_id_seq OWNED BY alarms.id;


--
-- Name: attachment_attachmentid__incident_attachments; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE attachment_attachmentid__incident_attachments (
    id integer NOT NULL,
    incident_attachments integer,
    attachment_attachmentid integer
);


ALTER TABLE attachment_attachmentid__incident_attachments OWNER TO qlodtest;

--
-- Name: attachment_attachmentid__incident_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE attachment_attachmentid__incident_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attachment_attachmentid__incident_attachments_id_seq OWNER TO qlodtest;

--
-- Name: attachment_attachmentid__incident_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE attachment_attachmentid__incident_attachments_id_seq OWNED BY attachment_attachmentid__incident_attachments.id;


--
-- Name: attachment_attachmentid__inctemplate_attachments; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE attachment_attachmentid__inctemplate_attachments (
    id integer NOT NULL,
    inctemplate_attachments integer,
    attachment_attachmentid integer
);


ALTER TABLE attachment_attachmentid__inctemplate_attachments OWNER TO qlodtest;

--
-- Name: attachment_attachmentid__inctemplate_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE attachment_attachmentid__inctemplate_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attachment_attachmentid__inctemplate_attachments_id_seq OWNER TO qlodtest;

--
-- Name: attachment_attachmentid__inctemplate_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE attachment_attachmentid__inctemplate_attachments_id_seq OWNED BY attachment_attachmentid__inctemplate_attachments.id;


--
-- Name: attachment_incidents__incident_incidentid; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE attachment_incidents__incident_incidentid (
    id integer NOT NULL,
    attachment_incidents integer,
    incident_incidentid integer
);


ALTER TABLE attachment_incidents__incident_incidentid OWNER TO qlodtest;

--
-- Name: attachment_incidents__incident_incidentid_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE attachment_incidents__incident_incidentid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attachment_incidents__incident_incidentid_id_seq OWNER TO qlodtest;

--
-- Name: attachment_incidents__incident_incidentid_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE attachment_incidents__incident_incidentid_id_seq OWNED BY attachment_incidents__incident_incidentid.id;


--
-- Name: attachments; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE attachments (
    attachmentid integer NOT NULL,
    key text,
    filename text,
    size text,
    etag text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE attachments OWNER TO qlodtest;

--
-- Name: attachments_attachmentid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE attachments_attachmentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attachments_attachmentid_seq OWNER TO qlodtest;

--
-- Name: attachments_attachmentid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE attachments_attachmentid_seq OWNED BY attachments.attachmentid;


--
-- Name: authtokens; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE authtokens (
    emailid text,
    tokentype integer,
    token text,
    "createdAt" text,
    "updatedAt" text,
    id integer NOT NULL
);


ALTER TABLE authtokens OWNER TO qlodtest;

--
-- Name: authtokens_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE authtokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authtokens_id_seq OWNER TO qlodtest;

--
-- Name: authtokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE authtokens_id_seq OWNED BY authtokens.id;


--
-- Name: bd_admins__user_administeredbds; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE bd_admins__user_administeredbds (
    id integer NOT NULL,
    bd_admins integer,
    user_administeredbds integer
);


ALTER TABLE bd_admins__user_administeredbds OWNER TO qlodtest;

--
-- Name: bd_admins__user_administeredbds_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE bd_admins__user_administeredbds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bd_admins__user_administeredbds_id_seq OWNER TO qlodtest;

--
-- Name: bd_admins__user_administeredbds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE bd_admins__user_administeredbds_id_seq OWNED BY bd_admins__user_administeredbds.id;


--
-- Name: businessdepartments; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE businessdepartments (
    bdid integer NOT NULL,
    bdname text,
    isactive boolean,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE businessdepartments OWNER TO qlodtest;

--
-- Name: businessdepartments_bdid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE businessdepartments_bdid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE businessdepartments_bdid_seq OWNER TO qlodtest;

--
-- Name: businessdepartments_bdid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE businessdepartments_bdid_seq OWNED BY businessdepartments.bdid;


--
-- Name: chargebackcodes; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE chargebackcodes (
    cbid integer NOT NULL,
    bdid integer,
    isactive boolean,
    cbownerid integer,
    isfrozen boolean,
    freezecause integer,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE chargebackcodes OWNER TO qlodtest;

--
-- Name: chargebackcodes_cbid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE chargebackcodes_cbid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE chargebackcodes_cbid_seq OWNER TO qlodtest;

--
-- Name: chargebackcodes_cbid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE chargebackcodes_cbid_seq OWNED BY chargebackcodes.cbid;


--
-- Name: cidetail_serviceids__service_ciids; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE cidetail_serviceids__service_ciids (
    id integer NOT NULL,
    cidetail_serviceids integer,
    service_ciids integer
);


ALTER TABLE cidetail_serviceids__service_ciids OWNER TO qlodtest;

--
-- Name: cidetail_serviceids__service_ciids_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE cidetail_serviceids__service_ciids_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cidetail_serviceids__service_ciids_id_seq OWNER TO qlodtest;

--
-- Name: cidetail_serviceids__service_ciids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE cidetail_serviceids__service_ciids_id_seq OWNED BY cidetail_serviceids__service_ciids.id;


--
-- Name: cidetails; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE cidetails (
    ciid integer NOT NULL,
    ciname text,
    assettag text,
    cistateid integer,
    citypeid integer,
    envid integer,
    contractid text,
    vendorid integer,
    warrantyexpiry integer,
    maintenanceend integer,
    customerfacing boolean,
    isundermaint boolean,
    machineid integer,
    systemsoftware1 text,
    systemsoftware2 text,
    systemsoftware3 text,
    customapp1 text,
    customapp2 text,
    customapp3 text,
    ciownerid integer,
    locationid integer,
    installationdate integer,
    isfrozen boolean,
    freezecause integer,
    ipaddress text,
    primaryservice integer,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE cidetails OWNER TO qlodtest;

--
-- Name: cidetails_ciid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE cidetails_ciid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cidetails_ciid_seq OWNER TO qlodtest;

--
-- Name: cidetails_ciid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE cidetails_ciid_seq OWNED BY cidetails.ciid;


--
-- Name: cienvironments; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE cienvironments (
    envid integer NOT NULL,
    envname text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE cienvironments OWNER TO qlodtest;

--
-- Name: cienvironments_envid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE cienvironments_envid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cienvironments_envid_seq OWNER TO qlodtest;

--
-- Name: cienvironments_envid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE cienvironments_envid_seq OWNED BY cienvironments.envid;


--
-- Name: cihardwares; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE cihardwares (
    machineid integer NOT NULL,
    hardwaretag text,
    manufacturervendorid integer,
    hwmodel text,
    serialnumber text,
    rack text,
    rackunit integer,
    gridloc text,
    slot text,
    macid text,
    osname text,
    osvendorid integer,
    osversion text,
    biosserial text,
    biosvendorid integer,
    biosmodel text,
    phymemmb integer,
    psuwattage integer,
    partnum text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE cihardwares OWNER TO qlodtest;

--
-- Name: cihardwares_machineid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE cihardwares_machineid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cihardwares_machineid_seq OWNER TO qlodtest;

--
-- Name: cihardwares_machineid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE cihardwares_machineid_seq OWNED BY cihardwares.machineid;


--
-- Name: cilocations; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE cilocations (
    locationid integer NOT NULL,
    locationname text,
    locationcode text,
    buildingname text,
    sitecategory text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE cilocations OWNER TO qlodtest;

--
-- Name: cilocations_locationid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE cilocations_locationid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cilocations_locationid_seq OWNER TO qlodtest;

--
-- Name: cilocations_locationid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE cilocations_locationid_seq OWNED BY cilocations.locationid;


--
-- Name: cinetworks; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE cinetworks (
    cinetworkid integer NOT NULL,
    primaryip text,
    networkname text,
    subnet text,
    gateway text,
    additionalip text,
    additionalsubnet text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE cinetworks OWNER TO qlodtest;

--
-- Name: cinetworks_cinetworkid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE cinetworks_cinetworkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cinetworks_cinetworkid_seq OWNER TO qlodtest;

--
-- Name: cinetworks_cinetworkid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE cinetworks_cinetworkid_seq OWNED BY cinetworks.cinetworkid;


--
-- Name: citypes; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE citypes (
    citypeid integer NOT NULL,
    citype text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE citypes OWNER TO qlodtest;

--
-- Name: citypes_citypeid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE citypes_citypeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE citypes_citypeid_seq OWNER TO qlodtest;

--
-- Name: citypes_citypeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE citypes_citypeid_seq OWNED BY citypes.citypeid;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE companies (
    accountid text NOT NULL,
    name text,
    accntadmin integer,
    supportmgr text,
    isactive boolean,
    globalconfig json,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE companies OWNER TO qlodtest;

--
-- Name: incidents; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE incidents (
    incidentid integer NOT NULL,
    state integer,
    title text,
    description text,
    serviceid integer,
    ciid integer,
    ownerqueueid integer,
    owneruserid integer,
    assignedqueueid integer,
    assigneduserid integer,
    startedat integer,
    endedat integer,
    severity integer,
    impact integer,
    vendorid integer,
    vendorsr text,
    pendinginc integer,
    journal json,
    resolutioncode integer,
    resolutionnotes text,
    closurecomments text,
    visibility text,
    resolvedat integer,
    rating integer,
    dupticket integer,
    logglylogs json,
    reportertype integer,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE incidents OWNER TO qlodtest;

--
-- Name: incidents_incidentid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE incidents_incidentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE incidents_incidentid_seq OWNER TO qlodtest;

--
-- Name: incidents_incidentid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE incidents_incidentid_seq OWNED BY incidents.incidentid;


--
-- Name: inctemplates; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE inctemplates (
    templateid integer NOT NULL,
    templatename text,
    lasteditorid integer,
    title text,
    description text,
    ownerqueuename text,
    ownerusername text,
    assignedqueuename text,
    assignedusername text,
    servicename text,
    ciname text,
    severity integer,
    impact integer,
    isactive boolean,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE inctemplates OWNER TO qlodtest;

--
-- Name: inctemplates_templateid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE inctemplates_templateid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inctemplates_templateid_seq OWNER TO qlodtest;

--
-- Name: inctemplates_templateid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE inctemplates_templateid_seq OWNED BY inctemplates.templateid;


--
-- Name: integrationconfigs; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE integrationconfigs (
    configid integer NOT NULL,
    integrationtype integer,
    integratorid integer,
    config json,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE integrationconfigs OWNER TO qlodtest;

--
-- Name: integrationconfigs_configid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE integrationconfigs_configid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE integrationconfigs_configid_seq OWNER TO qlodtest;

--
-- Name: integrationconfigs_configid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE integrationconfigs_configid_seq OWNED BY integrationconfigs.configid;


--
-- Name: organizationapprovals; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE organizationapprovals (
    approvalid integer NOT NULL,
    typeofapproval integer,
    entityid integer,
    targetentityid integer,
    approverid integer,
    initiatorid integer,
    isapproved boolean,
    isactive boolean,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE organizationapprovals OWNER TO qlodtest;

--
-- Name: organizationapprovals_approvalid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE organizationapprovals_approvalid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE organizationapprovals_approvalid_seq OWNER TO qlodtest;

--
-- Name: organizationapprovals_approvalid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE organizationapprovals_approvalid_seq OWNED BY organizationapprovals.approvalid;


--
-- Name: queue_members__user_queuesenrolled; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE queue_members__user_queuesenrolled (
    id integer NOT NULL,
    queue_members integer,
    user_queuesenrolled integer
);


ALTER TABLE queue_members__user_queuesenrolled OWNER TO qlodtest;

--
-- Name: queue_members__user_queuesenrolled_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE queue_members__user_queuesenrolled_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE queue_members__user_queuesenrolled_id_seq OWNER TO qlodtest;

--
-- Name: queue_members__user_queuesenrolled_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE queue_members__user_queuesenrolled_id_seq OWNED BY queue_members__user_queuesenrolled.id;


--
-- Name: queues; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE queues (
    queueid integer NOT NULL,
    queuename text,
    cbid integer,
    queueemail text,
    queueownerid integer,
    incidentmanagerid integer,
    changemanagerid integer,
    srtmanagerid integer,
    alertl1 text,
    alertl2 text,
    alertl3 text,
    isactive boolean,
    isfrozen boolean,
    freezecause integer,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE queues OWNER TO qlodtest;

--
-- Name: queues_queueid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE queues_queueid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE queues_queueid_seq OWNER TO qlodtest;

--
-- Name: queues_queueid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE queues_queueid_seq OWNED BY queues.queueid;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE roles (
    roleid integer NOT NULL,
    userid integer,
    issuperuser boolean,
    isbdadmin boolean,
    isoperator boolean,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE roles OWNER TO qlodtest;

--
-- Name: roles_roleid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE roles_roleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_roleid_seq OWNER TO qlodtest;

--
-- Name: roles_roleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE roles_roleid_seq OWNED BY roles.roleid;


--
-- Name: service_crntsrvsundappvl__serviceapproval_crntsrvids; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE service_crntsrvsundappvl__serviceapproval_crntsrvids (
    id integer NOT NULL,
    service_crntsrvsundappvl integer,
    serviceapproval_crntsrvids integer
);


ALTER TABLE service_crntsrvsundappvl__serviceapproval_crntsrvids OWNER TO qlodtest;

--
-- Name: service_crntsrvsundappvl__serviceapproval_crntsrvids_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE service_crntsrvsundappvl__serviceapproval_crntsrvids_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE service_crntsrvsundappvl__serviceapproval_crntsrvids_id_seq OWNER TO qlodtest;

--
-- Name: service_crntsrvsundappvl__serviceapproval_crntsrvids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE service_crntsrvsundappvl__serviceapproval_crntsrvids_id_seq OWNED BY service_crntsrvsundappvl__serviceapproval_crntsrvids.id;


--
-- Name: service_newsrvsundappvl__serviceapproval_newserviceids; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE service_newsrvsundappvl__serviceapproval_newserviceids (
    id integer NOT NULL,
    service_newsrvsundappvl integer,
    serviceapproval_newserviceids integer
);


ALTER TABLE service_newsrvsundappvl__serviceapproval_newserviceids OWNER TO qlodtest;

--
-- Name: service_newsrvsundappvl__serviceapproval_newserviceids_id_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE service_newsrvsundappvl__serviceapproval_newserviceids_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE service_newsrvsundappvl__serviceapproval_newserviceids_id_seq OWNER TO qlodtest;

--
-- Name: service_newsrvsundappvl__serviceapproval_newserviceids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE service_newsrvsundappvl__serviceapproval_newserviceids_id_seq OWNED BY service_newsrvsundappvl__serviceapproval_newserviceids.id;


--
-- Name: serviceapprovals; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE serviceapprovals (
    approvalid integer NOT NULL,
    isapproved boolean,
    approverid integer,
    approverserviceid integer,
    ciid integer,
    type text,
    isactive boolean,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE serviceapprovals OWNER TO qlodtest;

--
-- Name: serviceapprovals_approvalid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE serviceapprovals_approvalid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE serviceapprovals_approvalid_seq OWNER TO qlodtest;

--
-- Name: serviceapprovals_approvalid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE serviceapprovals_approvalid_seq OWNED BY serviceapprovals.approvalid;


--
-- Name: services; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE services (
    serviceid integer NOT NULL,
    servicename text,
    cbid integer,
    servicestateid integer,
    details text,
    isfrozen boolean,
    freezecause integer,
    supportqueue integer,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE services OWNER TO qlodtest;

--
-- Name: services_serviceid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE services_serviceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE services_serviceid_seq OWNER TO qlodtest;

--
-- Name: services_serviceid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE services_serviceid_seq OWNED BY services.serviceid;


--
-- Name: users; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE users (
    userid integer NOT NULL,
    fullname text,
    username text,
    password text,
    emailid text,
    countrycode text,
    phonenumber text,
    employeeid text,
    isactive boolean,
    passexpired boolean,
    defaultqueue integer,
    accountid text,
    userconfig json,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE users OWNER TO qlodtest;

--
-- Name: users_userid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE users_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_userid_seq OWNER TO qlodtest;

--
-- Name: users_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE users_userid_seq OWNED BY users.userid;


--
-- Name: vendors; Type: TABLE; Schema: public; Owner: qlodtest; Tablespace: 
--

CREATE TABLE vendors (
    vendorid integer NOT NULL,
    vendorname text,
    "createdAt" text,
    "updatedAt" text
);


ALTER TABLE vendors OWNER TO qlodtest;

--
-- Name: vendors_vendorid_seq; Type: SEQUENCE; Schema: public; Owner: qlodtest
--

CREATE SEQUENCE vendors_vendorid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vendors_vendorid_seq OWNER TO qlodtest;

--
-- Name: vendors_vendorid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qlodtest
--

ALTER SEQUENCE vendors_vendorid_seq OWNED BY vendors.vendorid;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY alarm_id__incident_alarmids ALTER COLUMN id SET DEFAULT nextval('alarm_id__incident_alarmids_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY alarms ALTER COLUMN id SET DEFAULT nextval('alarms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY attachment_attachmentid__incident_attachments ALTER COLUMN id SET DEFAULT nextval('attachment_attachmentid__incident_attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY attachment_attachmentid__inctemplate_attachments ALTER COLUMN id SET DEFAULT nextval('attachment_attachmentid__inctemplate_attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY attachment_incidents__incident_incidentid ALTER COLUMN id SET DEFAULT nextval('attachment_incidents__incident_incidentid_id_seq'::regclass);


--
-- Name: attachmentid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY attachments ALTER COLUMN attachmentid SET DEFAULT nextval('attachments_attachmentid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY authtokens ALTER COLUMN id SET DEFAULT nextval('authtokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY bd_admins__user_administeredbds ALTER COLUMN id SET DEFAULT nextval('bd_admins__user_administeredbds_id_seq'::regclass);


--
-- Name: bdid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY businessdepartments ALTER COLUMN bdid SET DEFAULT nextval('businessdepartments_bdid_seq'::regclass);


--
-- Name: cbid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY chargebackcodes ALTER COLUMN cbid SET DEFAULT nextval('chargebackcodes_cbid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY cidetail_serviceids__service_ciids ALTER COLUMN id SET DEFAULT nextval('cidetail_serviceids__service_ciids_id_seq'::regclass);


--
-- Name: ciid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY cidetails ALTER COLUMN ciid SET DEFAULT nextval('cidetails_ciid_seq'::regclass);


--
-- Name: envid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY cienvironments ALTER COLUMN envid SET DEFAULT nextval('cienvironments_envid_seq'::regclass);


--
-- Name: machineid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY cihardwares ALTER COLUMN machineid SET DEFAULT nextval('cihardwares_machineid_seq'::regclass);


--
-- Name: locationid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY cilocations ALTER COLUMN locationid SET DEFAULT nextval('cilocations_locationid_seq'::regclass);


--
-- Name: cinetworkid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY cinetworks ALTER COLUMN cinetworkid SET DEFAULT nextval('cinetworks_cinetworkid_seq'::regclass);


--
-- Name: citypeid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY citypes ALTER COLUMN citypeid SET DEFAULT nextval('citypes_citypeid_seq'::regclass);


--
-- Name: incidentid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY incidents ALTER COLUMN incidentid SET DEFAULT nextval('incidents_incidentid_seq'::regclass);


--
-- Name: templateid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY inctemplates ALTER COLUMN templateid SET DEFAULT nextval('inctemplates_templateid_seq'::regclass);


--
-- Name: configid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY integrationconfigs ALTER COLUMN configid SET DEFAULT nextval('integrationconfigs_configid_seq'::regclass);


--
-- Name: approvalid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY organizationapprovals ALTER COLUMN approvalid SET DEFAULT nextval('organizationapprovals_approvalid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY queue_members__user_queuesenrolled ALTER COLUMN id SET DEFAULT nextval('queue_members__user_queuesenrolled_id_seq'::regclass);


--
-- Name: queueid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY queues ALTER COLUMN queueid SET DEFAULT nextval('queues_queueid_seq'::regclass);


--
-- Name: roleid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY roles ALTER COLUMN roleid SET DEFAULT nextval('roles_roleid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY service_crntsrvsundappvl__serviceapproval_crntsrvids ALTER COLUMN id SET DEFAULT nextval('service_crntsrvsundappvl__serviceapproval_crntsrvids_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY service_newsrvsundappvl__serviceapproval_newserviceids ALTER COLUMN id SET DEFAULT nextval('service_newsrvsundappvl__serviceapproval_newserviceids_id_seq'::regclass);


--
-- Name: approvalid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY serviceapprovals ALTER COLUMN approvalid SET DEFAULT nextval('serviceapprovals_approvalid_seq'::regclass);


--
-- Name: serviceid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY services ALTER COLUMN serviceid SET DEFAULT nextval('services_serviceid_seq'::regclass);


--
-- Name: userid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY users ALTER COLUMN userid SET DEFAULT nextval('users_userid_seq'::regclass);


--
-- Name: vendorid; Type: DEFAULT; Schema: public; Owner: qlodtest
--

ALTER TABLE ONLY vendors ALTER COLUMN vendorid SET DEFAULT nextval('vendors_vendorid_seq'::regclass);


--
-- Name: alarm_id__incident_alarmids_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY alarm_id__incident_alarmids
    ADD CONSTRAINT alarm_id__incident_alarmids_pkey PRIMARY KEY (id);


--
-- Name: alarms_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY alarms
    ADD CONSTRAINT alarms_pkey PRIMARY KEY (id);


--
-- Name: attachment_attachmentid__incident_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY attachment_attachmentid__incident_attachments
    ADD CONSTRAINT attachment_attachmentid__incident_attachments_pkey PRIMARY KEY (id);


--
-- Name: attachment_attachmentid__inctemplate_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY attachment_attachmentid__inctemplate_attachments
    ADD CONSTRAINT attachment_attachmentid__inctemplate_attachments_pkey PRIMARY KEY (id);


--
-- Name: attachment_incidents__incident_incidentid_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY attachment_incidents__incident_incidentid
    ADD CONSTRAINT attachment_incidents__incident_incidentid_pkey PRIMARY KEY (id);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (attachmentid);


--
-- Name: authtokens_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY authtokens
    ADD CONSTRAINT authtokens_pkey PRIMARY KEY (id);


--
-- Name: bd_admins__user_administeredbds_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY bd_admins__user_administeredbds
    ADD CONSTRAINT bd_admins__user_administeredbds_pkey PRIMARY KEY (id);


--
-- Name: businessdepartments_bdname_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY businessdepartments
    ADD CONSTRAINT businessdepartments_bdname_key UNIQUE (bdname);


--
-- Name: businessdepartments_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY businessdepartments
    ADD CONSTRAINT businessdepartments_pkey PRIMARY KEY (bdid);


--
-- Name: chargebackcodes_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY chargebackcodes
    ADD CONSTRAINT chargebackcodes_pkey PRIMARY KEY (cbid);


--
-- Name: cidetail_serviceids__service_ciids_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cidetail_serviceids__service_ciids
    ADD CONSTRAINT cidetail_serviceids__service_ciids_pkey PRIMARY KEY (id);


--
-- Name: cidetails_assettag_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cidetails
    ADD CONSTRAINT cidetails_assettag_key UNIQUE (assettag);


--
-- Name: cidetails_ciname_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cidetails
    ADD CONSTRAINT cidetails_ciname_key UNIQUE (ciname);


--
-- Name: cidetails_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cidetails
    ADD CONSTRAINT cidetails_pkey PRIMARY KEY (ciid);


--
-- Name: cienvironments_envname_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cienvironments
    ADD CONSTRAINT cienvironments_envname_key UNIQUE (envname);


--
-- Name: cienvironments_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cienvironments
    ADD CONSTRAINT cienvironments_pkey PRIMARY KEY (envid);


--
-- Name: cihardwares_hardwaretag_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cihardwares
    ADD CONSTRAINT cihardwares_hardwaretag_key UNIQUE (hardwaretag);


--
-- Name: cihardwares_macid_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cihardwares
    ADD CONSTRAINT cihardwares_macid_key UNIQUE (macid);


--
-- Name: cihardwares_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cihardwares
    ADD CONSTRAINT cihardwares_pkey PRIMARY KEY (machineid);


--
-- Name: cilocations_buildingname_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cilocations
    ADD CONSTRAINT cilocations_buildingname_key UNIQUE (buildingname);


--
-- Name: cilocations_locationcode_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cilocations
    ADD CONSTRAINT cilocations_locationcode_key UNIQUE (locationcode);


--
-- Name: cilocations_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cilocations
    ADD CONSTRAINT cilocations_pkey PRIMARY KEY (locationid);


--
-- Name: cinetworks_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cinetworks
    ADD CONSTRAINT cinetworks_pkey PRIMARY KEY (cinetworkid);


--
-- Name: cinetworks_primaryip_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY cinetworks
    ADD CONSTRAINT cinetworks_primaryip_key UNIQUE (primaryip);


--
-- Name: citypes_citype_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY citypes
    ADD CONSTRAINT citypes_citype_key UNIQUE (citype);


--
-- Name: citypes_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY citypes
    ADD CONSTRAINT citypes_pkey PRIMARY KEY (citypeid);


--
-- Name: companies_name_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_name_key UNIQUE (name);


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (accountid);


--
-- Name: incidents_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY incidents
    ADD CONSTRAINT incidents_pkey PRIMARY KEY (incidentid);


--
-- Name: inctemplates_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY inctemplates
    ADD CONSTRAINT inctemplates_pkey PRIMARY KEY (templateid);


--
-- Name: inctemplates_templatename_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY inctemplates
    ADD CONSTRAINT inctemplates_templatename_key UNIQUE (templatename);


--
-- Name: integrationconfigs_integrationtype_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY integrationconfigs
    ADD CONSTRAINT integrationconfigs_integrationtype_key UNIQUE (integrationtype);


--
-- Name: integrationconfigs_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY integrationconfigs
    ADD CONSTRAINT integrationconfigs_pkey PRIMARY KEY (configid);


--
-- Name: organizationapprovals_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY organizationapprovals
    ADD CONSTRAINT organizationapprovals_pkey PRIMARY KEY (approvalid);


--
-- Name: queue_members__user_queuesenrolled_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY queue_members__user_queuesenrolled
    ADD CONSTRAINT queue_members__user_queuesenrolled_pkey PRIMARY KEY (id);


--
-- Name: queues_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY queues
    ADD CONSTRAINT queues_pkey PRIMARY KEY (queueid);


--
-- Name: queues_queuename_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY queues
    ADD CONSTRAINT queues_queuename_key UNIQUE (queuename);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (roleid);


--
-- Name: roles_userid_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_userid_key UNIQUE (userid);


--
-- Name: service_crntsrvsundappvl__serviceapproval_crntsrvids_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY service_crntsrvsundappvl__serviceapproval_crntsrvids
    ADD CONSTRAINT service_crntsrvsundappvl__serviceapproval_crntsrvids_pkey PRIMARY KEY (id);


--
-- Name: service_newsrvsundappvl__serviceapproval_newserviceids_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY service_newsrvsundappvl__serviceapproval_newserviceids
    ADD CONSTRAINT service_newsrvsundappvl__serviceapproval_newserviceids_pkey PRIMARY KEY (id);


--
-- Name: serviceapprovals_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY serviceapprovals
    ADD CONSTRAINT serviceapprovals_pkey PRIMARY KEY (approvalid);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (serviceid);


--
-- Name: services_servicename_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_servicename_key UNIQUE (servicename);


--
-- Name: users_emailid_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_emailid_key UNIQUE (emailid);


--
-- Name: users_employeeid_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_employeeid_key UNIQUE (employeeid);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


--
-- Name: users_username_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY vendors
    ADD CONSTRAINT vendors_pkey PRIMARY KEY (vendorid);


--
-- Name: vendors_vendorname_key; Type: CONSTRAINT; Schema: public; Owner: qlodtest; Tablespace: 
--

ALTER TABLE ONLY vendors
    ADD CONSTRAINT vendors_vendorname_key UNIQUE (vendorname);


--
-- PostgreSQL database dump complete
--

--
-- Company Addition
--

INSERT INTO companies (accountid, name, accntadmin, supportmgr, isactive, "createdAt", "updatedAt") VALUES ('qlodtest', 'First Customer', 1, 'support@queueload.com', true, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));
--
-- Super User Addition with Role
--

INSERT INTO users (fullname, username, password, emailid, countrycode, phonenumber, employeeid, isactive, accountid, passexpired, "createdAt", "updatedAt")
VALUES ('Super User', 'administrator', '$2a$10$WkeZ2NZGu/bSOyccHiCa2emkIEwn6X.T2tTjhPft0ot5lAGkBFke2', 'administrator@testdomain.com', '+91', '9999999999', 'FTE000001', true, 'qlodtest', false, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));

INSERT INTO roles (userid, issuperuser, isbdadmin, isoperator, "createdAt", "updatedAt")
VALUES (1, true, false, false, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));

