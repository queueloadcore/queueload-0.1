﻿DELETE FROM servicestates;

ALTER SEQUENCE servicestates_servicestateid_seq RESTART WITH 1;

INSERT INTO servicestates(
            servicestatename, "createdAt", "updatedAt")
    VALUES ('Registration', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO servicestates(
            servicestatename, "createdAt", "updatedAt")
    VALUES ('Approved', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO servicestates(
            servicestatename, "createdAt", "updatedAt")
    VALUES ('Archived', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
