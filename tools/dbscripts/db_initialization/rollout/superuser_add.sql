﻿/*Enter the company*/

INSERT INTO companies (companyid, name, address, accntadmin, supportmgr, isactive, "createdAt", "updatedAt") VALUES (1, 'Wayne Enterprise', 'Gotham', 1, 'support@queueload.com', true, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));

/* Insert the super user with password 'P@5sW0rd'*/

INSERT INTO users (userid, firstname, lastname, username, password, emailid, countrycode, phonenumber, employeeid, isactive, passexpired, companyid, "createdAt", "updatedAt") VALUES (1, 'Super', 'User', 'administrator', '$2a$10$WkeZ2NZGu/bSOyccHiCa2emkIEwn6X.T2tTjhPft0ot5lAGkBFke2', 'administrator@testdomain.com', '+91', '9999999999', 'FTE000001', true, false, 1, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));

/* Make the user a super user*/

INSERT INTO roles (id, userid, issuperuser, isbdadmin, isoperator, "createdAt", "updatedAt") VALUES (1, 1, true, false, false, TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'), TO_TIMESTAMP(TO_CHAR(current_timestamp, 'YYYY-MM-DD HH24:MI:SS+05:30'), 'YYYY-MM-DD HH24:MI:SS+05:30'));

SELECT * FROM USERS;

SELECT * FROM ROLES;
