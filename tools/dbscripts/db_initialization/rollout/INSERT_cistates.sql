﻿DELETE FROM cistates;

ALTER SEQUENCE cistates_cistateid_seq RESTART WITH 1;

INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Registration', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Approved', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Planned', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Installed', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Commissioned', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Inactive', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Reclaim', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
INSERT INTO cistates(
            cistatename, "createdAt", "updatedAt")
    VALUES ('Decommissioned', CURRENT_TIMESTAMP(0), CURRENT_TIMESTAMP(0));
