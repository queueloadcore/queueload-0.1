#!/bin/bash

CURRSEC=0
LASTITERSEC=0
shutdownfile="/home/smruti/queueload-0.1/tools/stopmonwrap"
while [ TRUE ]
do
    CURRSEC=`date +%s`
    if [ $CURRSEC -eq $LASTITERSEC ]
    then
        continue;
    fi;
	/home/smruti/queueload-0.1/tools/appmon.sh;
    if [ -f "$shutdownfile" ]
    then
        rm $shutdownfile;
        break;
    fi;
    LASTITERSEC=`date +%s`
done