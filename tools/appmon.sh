#!/bin/bash

#  FileName: appmon.sh
#  Description: This script monitors the resource usage of sails, pgsql and system.
#		It takes about 1-2 seconds to execute the script so the resolution cannot be below 2 seconds.
#		This is advised to be run on a minute basis from cron. 
#		
#		The result is saved in /var/log/mondata.csv
# 		If the script is run as a non-root user, please ensure the above file exists and the user has write permission on it.
# 
#  Output Format: timestamp(in epoch seconds),sailsram(in bytes),sailsswap(in bytes),sailscpu(in %cpu),pgsqlram(in bytes),pgsqlswap(in bytes),pgsqlcpu(in %cpu),sysram(in bytes),sysswap(in bytes),syscpu(in %cpu)
#
#    Date            Change Description                            Author
#  ---------     ---------------------------                     ----------
#  24/2/2016        Initial file creation                         SMandal
#  

#STARTTIME=$(($(date +%s%N)/1000000))
#echo 'Start time(in miliseconds):' $STARTTIME;

#STAMP=$STARTTIME
#echo $STAMP

SAILSPPID=0
PGSQLPPID=0
SAILSRAMUSAGE=0
SAILSVMUSAGE=0
SAILSCPUUSAGE=0
PGSQLRAMUSAGE=0
PGSQLVMUSAGE=0
PGSQLCPUUSAGE=0
SYSRAMUSAGE=0
SYSVMUSAGE=0
SYSCPUUSAGE=0
OUTFILE=/var/log/mondata.csv

#CURRENTTIME=$(($(date +%s%N)/1000000))
#echo 'Variable declaration(in miliseconds):' $(expr $CURRENTTIME - $STAMP)
#STAMP=$CURRENTTIME

SAILSPPID=`ps aux | grep '[s]ails' | awk '{print $2}'`
PGSQLPPID=`ps aux | grep '[/p]ostgresql' | awk '{print $2}'`

#CURRENTTIME=$(($(date +%s%N)/1000000)) 
#echo 'After both ps(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
#STAMP=$CURRENTTIME

echo -n `date +%s` >> $OUTFILE

if [[ "$SAILSPPID" -ne 0 ]];
then
	mapfile -t SAILSPROCS < <(pstree -a -p -c $SAILSPPID | awk -F\, '{print $2}'|awk '{print $1}')
	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#echo 'After SAILS pstree(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#STAMP=$CURRENTTIME;
	for ((i=0;i<${#SAILSPROCS[*]};i++)) 
	do
		#CURRENTTIME=$(($(date +%s%N)/1000000))
		#echo 'Before running ps inside sails loop' $(expr $CURRENTTIME - $STAMP)
		#STAMP=$CURRENTTIME;
		RAMUSAGE=`ps -o rss ${SAILSPROCS[i]} | sed 1d | bc`
		#CURRENTTIME=$(($(date +%s%N)/1000000))
		#echo 'ps inside sails loop(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
		#STAMP=$CURRENTTIME;
		VMUSAGE=`awk '/VmSwap/{print $2 " " $3}' /proc/${SAILSPROCS[i]}/status | awk '{print $1}' | bc`
		CPUUSAGE=`ps -o pcpu ${SAILSPROCS[i]} | sed 1d | bc`
		if [[ "$RAMUSAGE" -ne 0 ]]; 
		then
			SAILSRAMUSAGE=$(($SAILSRAMUSAGE+$RAMUSAGE));
			SAILSCPUUSAGE=`echo $SAILSCPUUSAGE + $CPUUSAGE | bc`;
			SAILSVMUSAGE=$(($SAILSVMUSAGE+$VMUSAGE));
		fi
		#CURRENTTIME=$(($(date +%s%N)/1000000))
		#echo 'Looping SAILSPROCS(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
		#STAMP=$CURRENTTIME;
	done
	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#echo 'Before sails data write out(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#STAMP=$CURRENTTIME;
	echo -n ','$SAILSRAMUSAGE','$SAILSVMUSAGE','$SAILSCPUUSAGE >> $OUTFILE;
	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#echo 'After sails data write out(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#STAMP=$CURRENTTIME;		
else
	echo -n ',0,0,0' >> $OUTFILE;
fi
#CURRENTTIME=$(($(date +%s%N)/1000000))
#echo 'After sails block(in nanoseconds)' $(expr $CURRENTTIME - $STAMP)
#STAMP=$CURRENTTIME;
	
if [[ "$PGSQLPPID" -ne 0 ]];
then
	mapfile -t PGSQLPROCS < <(pstree -a -p -c $PGSQLPPID | awk -F\, '{print $2}'|awk '{print $1}')
	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#echo 'After PGSQL pstree(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#STAMP=$CURRENTTIME;
	for ((i=0;i<${#PGSQLPROCS[*]};i++))
	do
		RAMUSAGE=`ps -o rss ${PGSQLPROCS[i]} | sed 1d | bc`
		VMUSAGE=`awk '/VmSwap/{print $2 " " $3}' /proc/${PGSQLPROCS[i]}/status | awk '{print $1}' | bc`
		CPUUSAGE=`ps -o pcpu ${PGSQLPROCS[i]} | sed 1d | bc`
    	if [[ "$RAMUSAGE" -ne 0 ]]; 
    	then    
			PGSQLRAMUSAGE=$(($PGSQLRAMUSAGE+$RAMUSAGE));
			PGSQLCPUUSAGE=`echo $PGSQLCPUUSAGE + $CPUUSAGE | bc`;
			PGSQLVMUSAGE=$(($PGSQLVMUSAGE+$VMUSAGE));
    	fi
    	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#	echo 'Looping PGSQLPROCS(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#	STAMP=$CURRENTTIME;      
	done
	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#echo 'Before pgsql data write out(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#STAMP=$CURRENTTIME;
	echo -n ','$PGSQLRAMUSAGE','$PGSQLVMUSAGE','$PGSQLCPUUSAGE >> $OUTFILE;
	#CURRENTTIME=$(($(date +%s%N)/1000000))
	#echo 'After pgsql data write out(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
	#STAMP=$CURRENTTIME;
else
	echo -n ',0,0,0' >> $OUTFILE;
fi
#CURRENTTIME=$(($(date +%s%N)/1000000))
#echo 'After postgres block(in miliseconds)' $(expr $CURRENTTIME - $STAMP)
#STAMP=$CURRENTTIME;

vmstat -s | awk '{print $1}' | awk 'FNR == 2 || FNR == 9' | while read record
do
	echo -n ','$record >> $OUTFILE
done

SYSCPUUSAGE=`vmstat | awk 'FNR == 3 {print $14}'`
echo ','$SYSCPUUSAGE >> $OUTFILE

#CURRENTTIME=$(($(date +%s%N)/1000000))
#echo 'End time(in miliseconds)' $CURRENTTIME
#echo 'Run duration: ' $(expr $CURRENTTIME - $STARTTIME)
