echo "Downloading the dockerimg"
aws s3 cp s3://qlsetupconfig/dockerimg dockerimg;
echo "Upgrading the services with image" `cat dockerimg`;
ecrlogincmd=$(aws ecr get-login --region ap-southeast-1 | sed -e "s/-e none //g");
echo "Swarm manager logging into ECR for pulling the image"
sudo docker-machine ssh swarm-manager1 sudo $ecrlogincmd;
export services=`sudo docker-machine ssh swarm-manager1 sudo docker service ls -q`
echo "### State of the estate before the upgrade ###" 
sudo docker-machine ssh swarm-manager1 sudo docker service ls
for i in $services
do
  echo -n "Upgrading" "$i" "."
  sudo docker-machine ssh swarm-manager1 sudo docker service update --with-registry-auth=true --image=`cat dockerimg` "$i" > /dev/null
  for i in 1 2 3 4 5
  do
    sleep 3
    echo -n "."
  done
  echo 
done
echo "### State of the estate after the upgrade ###" 
sudo docker-machine ssh swarm-manager1 sudo docker service ls
rm dockerimg;
