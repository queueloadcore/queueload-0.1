#!/bin/bash

#  FileName: dataingest.sh
#  Description: This script enters users into sails app hosted locally..
#		It takes three parameters.
#			- SEEK This sets the first userids to be used. Subsequent ids will be increments of this seek value.
#			- COUNT This sets the number of users to be created.
#                       - URL Accept the url to the user add API (e.g. http://localhost:1337/user/add)
# 
#
#    Date            Change Description                             Author
#  ---------     ---------------------------                      ----------
#  24/2/2016        Initial file creation                          SMandal
# 

SEEK=$1
COUNT=$2
URL=$3

iter=$SEEK;

if [ $# -ne 3 ] 
then
	echo "USAGE: ./dataingest.sh <SEEK> <COUNT> <URL>"
else

cat /dev/null > total_time.out

	while [ "$iter" -lt `expr $SEEK + $COUNT` ]
	do
		PASSWORD=`cat /dev/urandom | tr -dc '0-9A-Za-z._!@#$%^&*()' | head -c 16`
       		PHONENUMBER=`cat /dev/urandom | tr -dc '0-9' | head -c 10`
        	curlcmd=`echo -e '\n\ncurl -X POST -s -w %{time_total} -H "Content-Type: application/json" -d '\'{\"firstname\": \"Test\",\"lastname\": \"User\",\"username\": \"T3st.User$iter\"',"password":'\"$PASSWORD\"',"emailid": "test.user'$iter'@betaclient.com","countrycode": "+1","phonenumber":'\"$PHONENUMBER\"',"employeeid":'\"$iter\"}\' \"$URL\"`
		echo $curlcmd
        	eval "$curlcmd"
        	while [ $? != 0 ]
        	do
        		echo -e "\n\nAttempt failed. Retrying the following...\n\n"
                	echo $curlcmd
                	eval "$curlcmd"
        	done
        	echo -e "\n\nThe user $iter has been entered successfully.\n"
        	echo -e "\nSleeping for a second.\n"
        	sleep 1
        	iter=`expr $iter + 1`
	done
fi
