/* FileName: _readme.txt
* Description: This file lists the documents created for this project.
* 
* Date				Change Description							Author
* ---------			---------------------------						-------
* 06/12/2015			Initial file creation							SMandal
*
*/			 

This directory is for keep all the written and collected documents related to the project. Anything meant to document a process to be followed or a project documentation is to be captured as a .odt file. The rest can be captured as a .md file.
