define({
  "name": "QueueLoad API Broker Documentation",
  "version": "0.0.1",
  "description": "This documents the API characteristics of the broker",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-09-12T07:09:02.610Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
