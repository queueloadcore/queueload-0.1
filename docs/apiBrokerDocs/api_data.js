define({ "api": [
  {
    "group": "AAA",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "name": ""
  },
  {
    "group": "AAA",
    "name": "forgotpass",
    "type": "post",
    "url": "/auth/forgotpass",
    "title": "ForgotPass",
    "description": "<p>This function checks the availability of the email and sends a token to be used to set new password</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": " curl -X POST -H \"Content-Type: application/json\" -d\n'{\n  \"emailid\" : \"test.user11@testclient1.com\"\n}' \"http://localhost:1337/auth/forgotpass\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Email",
            "optional": false,
            "field": "emailid",
            "description": "<p>EmailID of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"emailid\" : \"test.user11@testclient1.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "NA",
            "description": "<p>No return on sucess</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401503",
            "description": "<p>Provided credentials are incorrect</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "401503",
          "content": "{\n \"code\": 401503\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "AAA",
    "name": "reftkn",
    "type": "post",
    "url": "/auth/reftkn",
    "title": "RefTkn",
    "description": "<p>This function checks the authenticity of the supplied refresh token and on success issues details of the user along with an access token and refresh token</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -d\n'{\n   \"emailid\":\"administrator@testdomain.com\",\n   \"reftkn\": \"ed5758c7197f6c031889575bbe1fa164ea39337de30e93736ba36f3308b03d1d\"\n }' \"http://localhost:1337/auth/reftkn\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Email",
            "optional": false,
            "field": "emailid",
            "description": "<p>EmailID of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reftoken",
            "description": "<p>Refresh Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"emailid\":\"administrator@testdomain.com\",\n  \"reftkn\": \"ed5758c7197f6c031889575bbe1fa164ea39337de30e93736ba36f3308b03d1d\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Details of the user</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "acctoken",
            "description": "<p>Access Token</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "reftoken",
            "description": "<p>Refresh Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"user\": {\n    \"userid\": 1,\n    \"firstname\": \"Super\",\n    \"lastname\": \"User\",\n    \"username\": \"administrator\",\n    \"emailid\": \"administrator@testdomain.com\",\n    \"countrycode\": \"+91\",\n    \"phonenumber\": \"9999999999\",\n    \"employeeid\": \"FTE000001\",\n    \"isactive\": true\n  },\n  \"acctoken\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZ\n  XIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0\n  ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI\n  6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyODI4NDI3LCJleHAiOjE0NzI4NTAwMjcsImF1ZCI6Ind3dy\n  5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.iMnmXbaiBpo1bD5M9d5uIJURmPAMNKufXfjb2IrFzRw\",\n  \"reftoken\": \"0670696aca6c5d8665098aaa1b9e63a8040354cd35754f22680f17bad9768956\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401503",
            "description": "<p>Provided credentials are incorrect</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401508",
            "description": "<p>User password expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "401503",
          "content": "{\n \"code\": 401503\n}",
          "type": "json"
        },
        {
          "title": "401508",
          "content": "{\n \"code\": 401508\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "AAA",
    "name": "resetpass",
    "type": "post",
    "url": "/auth/resetpass",
    "title": "ResetPass",
    "description": "<p>This function checks the authenticity of the old password and on success sets user password to the provided new passowrd</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": " curl -X POST -H \"Content-Type: application/json\" -d\n'{\n  \"emailid\" : \"test.user11@testclient1.com\",\n  \"oldpass\" : \"password\",\n  \"newpass\" : \"password1\"\n}' \"http://localhost:1337/auth/resetpass\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Email",
            "optional": false,
            "field": "emailid",
            "description": "<p>EmailID of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "oldpass",
            "description": "<p>Old password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newpass",
            "description": "<p>New password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"emailid\" : \"test.user11@testclient1.com\",\n  \"oldpass\" : \"password\",\n  \"newpass\" : \"password1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "NA",
            "description": "<p>No return on sucess }</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401503",
            "description": "<p>Provided credentials are incorrect</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "401503",
          "content": "{\n \"code\": 401503\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "AAA",
    "name": "setpass",
    "type": "post",
    "url": "/auth/setpass",
    "title": "SetPass",
    "description": "<p>This function checks the authenticity of the provided token and on success sets user password to the provided new passowrd</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -d\n'{\n  \"emailid\" : \"test.user11@testclient1.com\",\n  \"token\" : \"0a70693fe3babe1bdd57f3ef9ea9b6827cc77a8460c3c96b27d0673a7c539e4c\",\n  \"newpass\" : \"password\"\n}' \"http://localhost:1337/auth/setpass\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Email",
            "optional": false,
            "field": "emailid",
            "description": "<p>EmailID of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token recevied in the email</p>"
          },
          {
            "group": "Parameter",
            "type": "Password",
            "optional": false,
            "field": "newpass",
            "description": "<p>New password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"emailid\" : \"test.user11@testclient1.com\",\n  \"token\" : \"0a70693fe3babe1bdd57f3ef9ea9b6827cc77a8460c3c96b27d0673a7c539e4c\",\n  \"newpass\" : \"password\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "optional": false,
            "field": "No",
            "description": "<p>return on sucess</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401503",
            "description": "<p>Provided credentials are incorrect</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401508",
            "description": "<p>User password expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "401503",
          "content": "{\n \"code\": 401503\n}",
          "type": "json"
        },
        {
          "title": "401508",
          "content": "{\n \"code\": 401508\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "AAA",
    "name": "signin",
    "type": "post",
    "url": "/auth/signin",
    "title": "Signin",
    "description": "<p>This function compares the supplied credentials with the stored ones and on success issues details of the user along with an access token and refresh token</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -d\n'{\n     \"emailid\" : \"administrator@testdomain.com\",\n     \"password\" : \"P@5sW0rd\"\n }' \"http://localhost:1337/auth/signin\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Email",
            "optional": false,
            "field": "emailid",
            "description": "<p>EmailID of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n   \"emailid\":\"marco.polo@testclient.com\",\n   \"password\":\"P@5#word\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Details of the user</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "acctoken",
            "description": "<p>Access Token</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "reftoken",
            "description": "<p>Refresh Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"user\": {\n    \"userid\": 1,\n    \"firstname\": \"Super\",\n    \"lastname\": \"User\",\n    \"username\": \"administrator\",\n    \"emailid\": \"administrator@testdomain.com\",\n    \"countrycode\": \"+91\",\n    \"phonenumber\": \"9999999999\",\n    \"employeeid\": \"FTE000001\",\n    \"isactive\": true\n  },\n  \"acctoken\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZ\n  XIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0\n  ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI\n  6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyODI3OTA5LCJleHAiOjE0NzI4NDk1MDksImF1ZCI6Ind3dy\n  5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.6THJtbRD0igM-jONnfe73ISr2MhKEwEHjGjnd4RSn-0\",\n  \"reftoken\": \"38452cdf995851becd15ad0d81623177fa2b7f5eaafec268de18c8f3dd165bc1\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401503",
            "description": "<p>Provided credentials are incorrect</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401508",
            "description": "<p>User password expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "401503",
          "content": "{\n \"code\": 401503\n}",
          "type": "json"
        },
        {
          "title": "401508",
          "content": "{\n \"code\": 401508\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "AAA",
    "name": "signout",
    "type": "post",
    "url": "/auth/signout",
    "title": "Signout",
    "description": "<p>This function checks the authenticity of the supplied refresh token and on success signs out the user from the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -d\n'{\n   \"emailid\":\"administrator@testdomain.com\",\n   \"reftkn\": \"4bb0a7a5b5c8a679abe5d54c8c0017f494f7ed703310bda5cbd8acb5cfe20b7e\"\n }' \"http://localhost:1337/auth/signout\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Email",
            "optional": false,
            "field": "emailid",
            "description": "<p>EmailID of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reftoken",
            "description": "<p>Refresh Token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"emailid\":\"administrator@testdomain.com\",\n  \"reftkn\": \"4bb0a7a5b5c8a679abe5d54c8c0017f494f7ed703310bda5cbd8acb5cfe20b7e\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "NA",
            "description": "<p>No return on sucess</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AuthController.js",
    "groupTitle": "AAA",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401503",
            "description": "<p>Provided credentials are incorrect</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "401503",
          "content": "{\n \"code\": 401503\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Alarm",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AlarmController.js",
    "groupTitle": "Alarm",
    "name": ""
  },
  {
    "group": "Attachment",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AttachmentController.js",
    "groupTitle": "Attachment",
    "name": ""
  },
  {
    "group": "Attachment",
    "name": "download",
    "type": "post",
    "url": "/attachment/download",
    "title": "Download",
    "description": "<p>This API downloads a file</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImF\nkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiw\nicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0Ijo\nxNDc0NjA5NTE2LCJleHAiOjE0NzQ2MzExMTYsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5\njb20ifQ.L5VK-C4lLqiEjl-opeKW_mJ71sIxFWiSTKnlb-5mHUc\" -d\n'{\n   \"attachmentid\" : 3\n }' \"http://localhost:1337/attachment/download\"",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "filename",
            "description": "<p>Name of the file</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>Download link of the file</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"filename\": \"37774-1920x1200.jpg\",\n  \"url\": \"https://qlticketattachments-test.s3.ap-south-1.amazonaws.com/30ece804a870b66900fbb53bafefe5a7\n  ?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIK5FY66CJ7TFKHBA%2F20160923%2Fap-south-1%2Fs3\n  %2Faws4_request&X-Amz-Date=20160923T055120Z&X-Amz-Expires=10&X-Amz-Signature=a7544c36d746c5144130bf30\n  e7a6d6265bc509a3f4d786d45ddbb4dc3eb08a7b&X-Amz-SignedHeaders=host\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AttachmentController.js",
    "groupTitle": "Attachment",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400402",
            "description": "<p>Requested file doesn't exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400402",
          "content": "{\n  \"code\": 400402\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Attachment",
    "name": "upload",
    "type": "post",
    "url": "/attachment/upload",
    "title": "Upload",
    "description": "<p>This API uploads a file</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "Strng",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>multipart/form-data</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "Send a file setting the UI element name attribute as \"attachment\"",
        "type": "web-form"
      }
    ],
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Key to the file</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "filename",
            "description": "<p>Name of the file</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "size",
            "description": "<p>Size of the file in bytes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  attachmentid: 3,\n  filename: '37774-1920x1200.jpg',\n  size: '3135190'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/AttachmentController.js",
    "groupTitle": "Attachment",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400401",
            "description": "<p>No file attached</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400401",
          "content": "{\n  \"code\": 400401\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Business_Department",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/BdController.js",
    "groupTitle": "Business_Department",
    "name": ""
  },
  {
    "group": "CI",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "name": ""
  },
  {
    "group": "CI",
    "name": "addCI",
    "type": "post",
    "url": "/cidetail/add",
    "title": "Add",
    "description": "<p>On input of all valid parameters this function will add a CI</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGVzdC5\n1c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGx\nveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3NTU1ODMyMCwiZXhwIjoxNDc1NTc5OTIwLCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.KGRM4EMjlM1vTh3jmm8ZdFE4cFRsO1a71zm\nLNY4q9ac\" -H \"Content-Type: application/json\" -d\n'{\n     \"ciname\": \"HJ4K2-74JK\",\n     \"assettag\": \"JFU5-73JD\",\n     \"citypeid\": 1,\n     \"envid\": 1,\n     \"vendorid\": 1,\n     \"ciownerid\": 2,\n     \"locationid\": 1,\n     \"installationdate\": 1480516228\n }' \"http://localhost:1337/cidetail/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ciname",
            "description": "<p>Name of the CI</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assettag",
            "description": "<p>Unique tag assigned to the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "citypeid",
            "description": "<p>Ci Type ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "envid",
            "description": "<p>Env Type ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "installationdate",
            "description": "<p>Time in epoch format</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "vendorid",
            "description": "<p>Vendor ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciownerid",
            "description": "<p>User ID of the CI Owner</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "locationid",
            "description": "<p>ID of the location</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "warrantyexpiry",
            "description": "<p>Time in epoch format</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "maintenanceend",
            "description": "<p>Time in epoch format</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "customerfacing",
            "description": "<p>If customer facing-Yes, else-No. default: No</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isundermaint",
            "description": "<p>If CI under maintenance</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "machineid",
            "description": "<p>ID of the hardware</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "cinetworkid",
            "description": "<p>ID of the network</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "contractid",
            "description": "<p>ID of the maintenance contract</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "systemsoftware1",
            "description": "<p>Name of Installed System Software 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "systemsoftware2",
            "description": "<p>Name of Installed System Software 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "systemsoftware3",
            "description": "<p>Name of Installed System Software 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "customapp1",
            "description": "<p>Name of the custom application 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "customapp2",
            "description": "<p>Name of the custom application 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "customapp3",
            "description": "<p>Name of the custom application 3</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"ciname\": \"HJ4K2-74JK\",\n  \"assettag\": \"JFU5-73JD\",\n  \"citypeid\": 1,\n  \"envid\": 1,\n  \"vendorid\": 1,\n  \"ciownerid\": 2,\n  \"locationid\": 1,\n  \"installationdate\": 1480516283\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "ci",
            "description": "<p>Added CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"ciid\": 4,\n  \"ciname\": \"HJ4K2-74JK\",\n  \"assettag\": \"JFU5-73JD\",\n  \"cistateid\": 1,\n  \"citypeid\": 1,\n  \"envid\": 1,\n  \"contractid\": null,\n  \"vendorid\": 1,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"machineid\": null,\n  \"cinetworkid\": null,\n  \"systemsoftware1\": null,\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"ciownerid\": 2,\n  \"locationid\": 1,\n  \"installationdate\": 1480516283,\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400104",
            "description": "<p>One or more of the entered users do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400103",
            "description": "<p>One or more of the entered users are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400303",
            "description": "<p>One or more entered Configuration Item Types do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400313",
            "description": "<p>The entered environments do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400322",
            "description": "<p>The entered location does not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400332",
            "description": "<p>One or more of entered vendors do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400342",
            "description": "<p>The entered Configuration Item Network does not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400352",
            "description": "<p>The entered Configuration Item Hardware does not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "400103",
          "content": "{\n  \"code\": 400103\n}",
          "type": "json"
        },
        {
          "title": "400303",
          "content": "{\n  \"code\": 400303\n}",
          "type": "json"
        },
        {
          "title": "400313",
          "content": "{\n  \"code\": 400313\n}",
          "type": "json"
        },
        {
          "title": "400322",
          "content": "{\n  \"code\": 400322\n}",
          "type": "json"
        },
        {
          "title": "400332",
          "content": "{\n  \"code\": 400332\n}",
          "type": "json"
        },
        {
          "title": "400342",
          "content": "{\n  \"code\": 400342\n}",
          "type": "json"
        },
        {
          "title": "400352",
          "content": "{\n  \"code\": 400352\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "cancel",
    "type": "post",
    "url": "/cidetail/cancel",
    "title": "Cancel",
    "description": "<p>Cancels a CI</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I\nm1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv\nbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ\n3MjcyNDk3NiwiZXhwIjoxNDcyNzQ2NTc2LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2\n9tIn0.asVcWWy3NlnF130JNOw3rA5epEBBcfIik0TPJ-6Xu0A\" d\n'{\n  \"ciid\" : 1\n}' \"http://localhost:1337/cidetail/cancel\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciid",
            "description": "<p>ID of the CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"ciid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "service",
            "description": "<p>Details of the cancelled service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"ciid\": 1,\n  \"ciname\": \"HD63J-34JD3\",\n  \"assettag\": \"UJF73-DJRI\",\n  \"cistateid\": 6,\n  \"citypeid\": 1,\n  \"envid\": 1,\n  \"contractid\": null,\n  \"vendorid\": 1,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"machineid\": null,\n  \"cinetworkid\": null,\n  \"systemsoftware1\": \"Splunk\",\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"ciownerid\": 1,\n  \"locationid\": 1,\n  \"installationdate\": 1480516283,\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400382",
            "description": "<p>One or more of entered Configuration Items do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400383",
            "description": "<p>Requested Configuration Item is frozen at the moment.</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400387",
            "description": "<p>Configuration Item state doesn't support the requested operation</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400382",
          "content": "{\n  \"code\": 400382\n}",
          "type": "json"
        },
        {
          "title": "400383",
          "content": "{\n  \"code\": 400383\n}",
          "type": "json"
        },
        {
          "title": "400387",
          "content": "{\n  \"code\": 400387\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "getdetail",
    "type": "post",
    "url": "/cidetail/getdetail",
    "title": "Getdetail",
    "description": "<p>This API fetches the details of a configuration item</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImF\nkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiw\nicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0Ijo\nxNDcyODI2ODAzLCJleHAiOjE0NzI4NDg0MDMsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5\njb20ifQ.7kaen692bFKfxERnBZgXxm0l0GUbJ0biz6xY7Fime9s\" -d '{\"ciid\" :1}' \"http://localhost:1337/cidetail/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciid",
            "description": "<p>ID of the CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\"ciid\" :1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "ci",
            "description": "<p>Array of CIs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"citypeid\": {\n    \"citypeid\": 1,\n    \"citype\": \"network switches\",\n    \"cikey\": 6\n  },\n  \"envid\": {\n    \"envid\": 1,\n    \"envname\": \"development\"\n  },\n  \"vendorid\": {\n    \"vendorid\": 1,\n    \"vendorname\": \"Cyberdyne Systems\"\n  },\n  \"machineid\": null,\n  \"cinetworkid\": null,\n  \"ciownerid\": {\n    \"userid\": 1,\n    \"firstname\": \"Super\",\n    \"lastname\": \"User\",\n    \"username\": \"administrator\",\n    \"emailid\": \"administrator@testdomain.com\",\n    \"countrycode\": \"+91\",\n    \"phonenumber\": \"9999999999\",\n    \"employeeid\": \"FTE000001\",\n    \"isactive\": true\n  },\n  \"locationid\": {\n    \"locationid\": 1,\n    \"locationname\": \"Rome-9W\",\n    \"locationcode\": \"IT-9W-F0L\",\n    \"buildingname\": \"Pani house\",\n    \"sitecategory\": \"Live Site\"\n  },\n  \"ciid\": 1,\n  \"ciname\": \"HD63J-34JD3\",\n  \"assettag\": \"UJF73-DJRI\",\n  \"contractid\": null,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"systemsoftware1\": \"Splunk\",\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"installationdate\": 1480516228,\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400382",
            "description": "<p>One or more of entered Configuration Items do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400382",
          "content": "{\n  \"code\": 400382\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "listCis",
    "type": "post",
    "url": "/cidetail/list",
    "title": "List",
    "description": "<p>This API lists configuration items in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ey\nJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iL\nCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5\nOTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjUzMjQ3MywiZXhwIjoxNDcyNTU0MDc\nzLCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.-S8nsNHDf2OlWzdCbYz96ambz1HsslwhiU\nNmcV73bHY\" -d '{\"page\" : 1, \"limit\" : 100}' \"http://localhost:1337//list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "ci",
            "description": "<p>Array of CIs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"cistateid\": 6,\n    \"citypeid\": 1,\n    \"envid\": 1,\n    \"vendorid\": 1,\n    \"machineid\": null,\n    \"cinetworkid\": null,\n    \"ciownerid\": 1,\n    \"locationid\": 1,\n    \"ciid\": 1,\n    \"ciname\": \"HD63J-34JD3\",\n    \"assettag\": \"UJF73-DJRI\",\n    \"contractid\": null,\n    \"warrantyexpiry\": null,\n    \"maintenanceend\": null,\n    \"customerfacing\": false,\n    \"isundermaint\": true,\n    \"systemsoftware1\": \"Splunk\",\n    \"systemsoftware2\": null,\n    \"systemsoftware3\": null,\n    \"customapp1\": null,\n    \"customapp2\": null,\n    \"customapp3\": null,\n    \"installationdate\": 1480516228,\n    \"isfrozen\": false,\n    \"freezecause\": \"0\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400381",
            "description": "<p>No Configuration Items available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400381",
          "content": "{\n  \"code\": 400381\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "makeserviceprimary",
    "type": "post",
    "url": "/cidetail/makeserviceprimary",
    "title": "Makeserviceprimary",
    "description": "<p>Makes the supplied service ID, the primary service of the requested configuration item</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6\nbnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjI3LCJmdWxsbmFtZSI6ImluY2ZpcnN0IGluY09wZXJhdG9yIiwiZ\nmlyc3RuYW1lIjoiaW5jZmlyc3QiLCJsYXN0bmFtZSI6ImluY09wZXJhdG9yIiwidXNlcm5hbWUiOiJpbmNmaXJzdC5vcGVyYXRvci\nIsImVtYWlsaWQiOiJpbmNmaXJzdC5vcGVyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjpudWxsLCJwaG9uZW51bWJ\nlciI6bnVsbCwiZW1wbG95ZWVpZCI6IklOQ0ZURTAwMDAwOSIsImlzYWN0aXZlIjp0cnVlLCJwYXNzZXhwaXJlZCI6ZmFsc2UsInVz\nZXJzZXR0aW5ncyI6bnVsbH0sImlhdCI6MTQ4OTQwNzQ3OCwiZXhwIjoxNDg5NDI5MDc4LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvb\nSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.TPycl3k6BQ-ZiXNr2lpT78nWBmX9k4WzfGIpRktYQfs\"\n-H \"Content-Type: application/json\" -d\n  '{\n     \"ciid\" : 4,\n     \"serviceid\" : 4\n   }' \"http://localhost:1337/cidetail/makeserviceprimary\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciid",
            "description": "<p>ID of the CI</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the Service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"ciid\" : 4,\n  \"serviceid\" : 4\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "cidetil",
            "description": "<p>Details of the CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"ciid\": 4,\n  \"ciname\": \"HJ4K2-74JK\",\n  \"assettag\": \"JFU5-73JD\",\n  \"cistateid\": 1,\n  \"citypeid\": 9,\n  \"envid\": 8,\n  \"contractid\": null,\n  \"vendorid\": 9,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"machineid\": null,\n  \"systemsoftware1\": null,\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"ciownerid\": 33,\n  \"locationid\": 5,\n  \"installationdate\": 1480515761,\n  \"isfrozen\": false,\n  \"freezecause\": 0,\n  \"ipaddress\": null,\n  \"primaryservice\": 4\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400389",
            "description": "<p>Configuration Item is not associated with the service</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400389",
          "content": "{\n  \"code\": 400389\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "next",
    "type": "post",
    "url": "/cidetail/next",
    "title": "Next",
    "description": "<p>Initiates state transition of a Configuration Item</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Mjc\nyNDk3NiwiZXhwIjoxNDcyNzQ2NTc2LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0\n.asVcWWy3NlnF130JNOw3rA5epEBBcfIik0TPJ-6Xu0A\" -d\n'{\n   \"ciid\" : 1\n }' \"http://localhost:1337/cidetail/next\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciid",
            "description": "<p>ID of the CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"ciid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "ci",
            "description": "<p>Details of the CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"ciid\": 1,\n  \"ciname\": \"HD63J-34JD3\",\n  \"assettag\": \"UJF73-DJRI\",\n  \"cistateid\": 2,\n  \"citypeid\": 1,\n  \"envid\": 1,\n  \"contractid\": null,\n  \"vendorid\": 1,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"machineid\": null,\n  \"cinetworkid\": null,\n  \"systemsoftware1\": \"Splunk\",\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"ciownerid\": 1,\n  \"locationid\": 1,\n  \"installationdate\": 1480516283,\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400382",
            "description": "<p>One or more of entered Configuration Items do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400383",
            "description": "<p>Requested Configuration Item is frozen at the moment.</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400384",
            "description": "<p>Configuration Item cannot be moved to undefined state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400385",
            "description": "<p>Configuration Item is associated with services</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400386",
            "description": "<p>Configuration Item must be associted with atleast one service</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400388",
            "description": "<p>Configuration Item doesn't have any support queue</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400382",
          "content": "{\n  \"code\": 400382\n}",
          "type": "json"
        },
        {
          "title": "400383",
          "content": "{\n  \"code\": 400383\n}",
          "type": "json"
        },
        {
          "title": "400384",
          "content": "{\n  \"code\": 400384\n}",
          "type": "json"
        },
        {
          "title": "400385",
          "content": "{\n  \"code\": 400385\n}",
          "type": "json"
        },
        {
          "title": "400386",
          "content": "{\n  \"code\": 400386\n}",
          "type": "json"
        },
        {
          "title": "400388",
          "content": "{\n  \"code\": 400388\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "search",
    "type": "post",
    "url": "/cidetail/search",
    "title": "Search",
    "description": "<p>Searches the model and returns the result. The number of search column and order by criteria are flexible.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjcyNDk3NiwiZXhwIjoxNDcyNzQ2NTc2LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.asVcWWy3NlnF130JNOw3rA5epEBBcfIik0T\nPJ-6Xu0A\" -H \"Content-Type: application/json\" -d\n'{\n   \"search\":  { \"ciname\": \"HD63J%\" },\n   \"result\" : [ \"ciname\", \"assettag\", \"cistateid\", \"citypeid\", \"envid\", \"ciownerid\", \"locationid\", \"installationdate\"],\n   \"orderby\": \"assettag\"\n }' \"http://localhost:1337/cidetail/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched CI</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"search\":  { \"ciname\": \"HD63J%\" },\n  \"result\" : [ \"ciname\", \"assettag\", \"cistateid\", \"citypeid\", \"envid\", \"ciownerid\", \"locationid\", \"installationdate\"],\n  \"orderby\": \"assettag\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>First element-Number of records, Second element-Array of results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  1,\n  [\n    {\n      \"cistateid\": 6,\n      \"citypeid\": 1,\n      \"envid\": 1,\n      \"ciownerid\": 1,\n      \"locationid\": 1,\n      \"ciname\": \"HD63J-34JD3\",\n      \"assettag\": \"UJF73-DJRI\",\n      \"installationdate\": 1480516283\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "CI",
    "name": "update",
    "type": "post",
    "url": "/cidetail/update",
    "title": "Update",
    "description": "<p>This function updates a CI in the system</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Mjc\nwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0\n.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM\" -d\n'{\n   \"ciid\" : 1,\n   \"systemsoftware1\" : \"Splunk\"\n }' \"http://localhost:1337/cidetail/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciid",
            "description": "<p>ID of the CI</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "citypeid",
            "description": "<p>CI Type ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "envid",
            "description": "<p>Env Type ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "installationdate",
            "description": "<p>Time in epoch format</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "vendorid",
            "description": "<p>Vendor ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ciownerid",
            "description": "<p>User ID of the CI Owner</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "locationid",
            "description": "<p>ID of the location</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "warrantyexpiry",
            "description": "<p>Time in epoch format</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "maintenanceend",
            "description": "<p>Time in epoch format</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "customerfacing",
            "description": "<p>If customer facing-Yes, else-No. default: No</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isundermaint",
            "description": "<p>If CI under maintenance</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "machineid",
            "description": "<p>ID of the hardware</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "cinetworkid",
            "description": "<p>ID of the network</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "contractid",
            "description": "<p>ID of the maintenance contract</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "systemsoftware1",
            "description": "<p>Name of Installed System Software 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "systemsoftware2",
            "description": "<p>Name of Installed System Software 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "systemsoftware3",
            "description": "<p>Name of Installed System Software 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "customapp1",
            "description": "<p>Name of the custom application 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "customapp2",
            "description": "<p>Name of the custom application 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "customapp3",
            "description": "<p>Name of the custom application 3</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"ciid\" : 1,\n  \"systemsoftware1\" : \"Splunk\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "ci",
            "description": "<p>Details of the updated CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"ciid\": 1,\n  \"ciname\": \"HD63J-34JD3\",\n  \"assettag\": \"UJF73-DJRI\",\n  \"cistateid\": 1,\n  \"citypeid\": 1,\n  \"envid\": 1,\n  \"contractid\": null,\n  \"vendorid\": 1,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"machineid\": null,\n  \"cinetworkid\": null,\n  \"systemsoftware1\": \"Splunk\",\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"ciownerid\": 1,\n  \"locationid\": 1,\n  \"installationdate\": 1480516283,\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CidetailController.js",
    "groupTitle": "CI",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400382",
            "description": "<p>One or more of entered Configuration Items do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400383",
            "description": "<p>Requested Configuration Item is frozen at the moment.</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400382",
          "content": "{\n  \"code\": 400382\n}",
          "type": "json"
        },
        {
          "title": "400383",
          "content": "{\n  \"code\": 400383\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Chargeback",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CbController.js",
    "groupTitle": "Chargeback",
    "name": ""
  },
  {
    "group": "Cinetwork",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CinetworkController.js",
    "groupTitle": "Cinetwork",
    "name": ""
  },
  {
    "group": "Cinetwork",
    "name": "addCinetwork",
    "type": "post",
    "url": "/cinetwork/add",
    "title": "Add",
    "description": "<p>On input of all valid parameters this function will add Ci networks</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ\n3fqnXwhQ\" -H \"Content-Type: application/json\" -d\n'{\n   \"primaryip\" : \"142.17.83.73\",\n   \"networkname\" : \"Upstream-Strike3-Gateway-Log\",\n   \"subnet\" : \"255.255.255.0\",\n   \"gateway\" : \"142.17.83.1\",\n   \"additionalip\" : \"142.17.83.91\",\n   \"additionalsubnet\" : \"255.255.255.0\"\n }' \"http://localhost:1337/cinetwork/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "IP",
            "optional": false,
            "field": "primaryip",
            "description": "<p>Unique IP of the network entity</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "networkname",
            "description": "<p>The name of the network</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": false,
            "field": "subnet",
            "description": "<p>Subnet</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": false,
            "field": "gateway",
            "description": "<p>Gateway</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": true,
            "field": "additionalip",
            "description": "<p>Secondary IP of this network asset</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": true,
            "field": "additionalsubnet",
            "description": "<p>Subnet for the secondary IP</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"primaryip\" : \"10.83.219.34\",\n  \"networkname\" : \"Upstream-Strike3-Gateway-Log\",\n  \"subnet\" : \"255.255.255.0\",\n  \"gateway\" : \"10.83.219.1\",\n  \"additionalip\" : \"10.46.39.173\",\n  \"additionalsubnet\" : \"255.255.255.0\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "network",
            "description": "<p>Added network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"cinetworkid\": 1,\n  \"primaryip\": \"142.17.83.73\",\n  \"networkname\": \"Upstream-Strike3-Gateway-Log\",\n  \"subnet\": \"255.255.255.0\",\n  \"gateway\": \"142.17.83.1\",\n  \"additionalip\": \"142.17.83.91\",\n  \"additionalsubnet\": \"255.255.255.0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CinetworkController.js",
    "groupTitle": "Cinetwork",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Cinetwork",
    "name": "getDetail",
    "type": "post",
    "url": "/cinetwork/getdetail",
    "title": "Getdetail",
    "description": "<p>On input of a Cinetworkid it retrieves the corresponding Cinetwork details</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ\n3fqnXwhQ\" -H \"Content-Type: application/json\" -d\n'{\"cinetworkid\": 1}' \"http://localhost:1337/cinetwork/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cinetworkid",
            "description": "<p>ID of the network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"cinetworkid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "network",
            "description": "<p>Details of the requested network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"cinetworkid\": 1,\n  \"primaryip\": \"142.17.83.73\",\n  \"networkname\": \"Upstream-Strike3-Gateway-Log\",\n  \"subnet\": \"255.255.255.0\",\n  \"gateway\": \"142.17.83.1\",\n  \"additionalip\": \"142.17.83.91\",\n  \"additionalsubnet\": \"255.255.255.0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CinetworkController.js",
    "groupTitle": "Cinetwork",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400342",
            "description": "<p>The entered Configuration Item Network does not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400342",
          "content": "{\n  \"code\": 400342\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Cinetwork",
    "name": "listCinetwork",
    "type": "post",
    "url": "/cinetwork/list",
    "title": "List",
    "description": "<p>This function lists all the Ci Networks in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmly\nc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28uc\nG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZW\nVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ\n3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\"\n-H \"Content-Type: application/json\" -d '{ \"limit\": 10, \"page\": 1}' \"http://localhost:1337/cinetwork/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "network",
            "description": "<p>Array of Networks</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  \"cinetworkid\": 1,\n  \"primaryip\": \"142.17.83.73\",\n  \"networkname\": \"Upstream-Strike3-Gateway-Log\",\n  \"subnet\": \"255.255.255.0\",\n  \"gateway\": \"142.17.83.1\",\n  \"additionalip\": \"142.17.83.91\",\n  \"additionalsubnet\": \"255.255.255.0\"\n  \"numberofcis\" : 1\n},\n{\n  \"cinetworkid\": 3,\n  \"primaryip\": \"161.10.18.83\",\n  \"networkname\": \"Westwind-DMZ2-DNS\",\n  \"subnet\": \"255.255.255.0\",\n  \"gateway\": \"161.10.18.1\",\n  \"additionalip\": \"161.10.18.14\",\n  \"additionalsubnet\": \"255.255.255.0\"\n  \"numberofcis\" : 1\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CinetworkController.js",
    "groupTitle": "Cinetwork",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400341",
            "description": "<p>No Configuration Item Network available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400341",
          "content": "{\n  \"code\": 400341\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Cinetwork",
    "name": "search",
    "type": "post",
    "url": "/cinetwork/search",
    "title": "Search",
    "description": "<p>Searches the model and returns the result. The number of search column and order by criteria are flexible.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ\n3fqnXwhQ\" -H \"Content-Type: application/json\" -d\n'{\n   \"search\":  { \"gateway\": \"142.17.83%\" },\n   \"result\" : [ \"primaryip\", \"networkname\", \"subnet\", \"gateway\"],\n   \"orderby\": \"networkname\"\n }' \"http://localhost:1337/cinetwork/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"search\":  { \"gateway\": \"142.17.83%\" },\n  \"result\" : [ \"primaryip\", \"networkname\", \"subnet\", \"gateway\"],\n  \"orderby\": \"networkname\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>First element-Number of records, Second element-Array of results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  2,\n  [\n    {\n      \"primaryip\": \"142.17.83.73\",\n      \"networkname\": \"JellyStub\",\n      \"subnet\": \"255.255.255.0\",\n      \"gateway\": \"142.17.83.1\"\n    },\n    {\n      \"primaryip\": \"142.17.83.74\",\n      \"networkname\": \"Upstream-Strike3-Gateway-Log\",\n      \"subnet\": \"255.255.255.0\",\n      \"gateway\": \"142.17.83.1\"\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CinetworkController.js",
    "groupTitle": "Cinetwork",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Cinetwork",
    "name": "update",
    "type": "post",
    "url": "/cinetwork/update",
    "title": "Update",
    "description": "<p>Updates a network</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZm\nlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY\n28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1w\nbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJ\nhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJ\nScPrQ3fqnXwhQ\" -H \"Content-Type: application/json\" -d\n  '{\n    \"cinetworkid\" : 1,\n    \"networkname\" : \"JellyStub\"\n  }' \"http://localhost:1337/cinetwork/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "primaryip",
            "description": "<p>ID of the network entity</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "networkname",
            "description": "<p>The name of the network</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": true,
            "field": "subnet",
            "description": "<p>Subnet</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": true,
            "field": "gateway",
            "description": "<p>Gateway</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": true,
            "field": "additionalip",
            "description": "<p>Secondary IP of this network asset</p>"
          },
          {
            "group": "Parameter",
            "type": "IP",
            "optional": true,
            "field": "additionalsubnet",
            "description": "<p>Subnet for the secondary IP</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"cinetworkid\" : 1,\n  \"networkname\" : \"JellyStub\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "network",
            "description": "<p>Details of the updated network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"cinetworkid\": 1,\n  \"primaryip\": \"142.17.83.73\",\n  \"networkname\": \"JellyStub\",\n  \"subnet\": \"255.255.255.0\",\n  \"gateway\": \"142.17.83.1\",\n  \"additionalip\": \"142.17.83.91\",\n  \"additionalsubnet\": \"255.255.255.0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CinetworkController.js",
    "groupTitle": "Cinetwork",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400342",
            "description": "<p>The entered Configuration Item Network does not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400342",
          "content": "{\n  \"code\": 400342\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Citype",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CitypeController.js",
    "groupTitle": "Citype",
    "name": ""
  },
  {
    "type": "post",
    "url": "/citype/add",
    "title": "Add",
    "name": "addCitype",
    "group": "Citype",
    "description": "<p>On input of all valid parameters this function will add ci types</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGVzdC5\n1c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGx\nveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3NTczMTgyMywiZXhwIjoxNDc1NzUzNDIzLCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.RCcGmdowferp2sMlFWTKRHkZkeowIs4Twl4\nZiCzlDck\" -H \"Content-Type: application/json\" -d\n'{\n   \"citypes\": [\"mudhi dabba\"]\n }' \"http://localhost:1337/citype/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "citypes",
            "description": "<p>Array of citypes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": " {\n  \"citypes\": [\"mudhi dabba\"]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "Citype",
            "description": "<p>Array of added CI Types</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"citypeid\": 5,\n    \"citype\": \"mudhi dabba\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CitypeController.js",
    "groupTitle": "Citype",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400302",
            "description": "<p>One or more entered Configuration Item Types already exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400302",
          "content": "{\n  \"code\": 400302\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Citype",
    "name": "listCitypes",
    "type": "post",
    "url": "/citype/list",
    "title": "List",
    "description": "<p>This API lists the CI types in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjY\nyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.\n2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\" -d '{ \"limit\": 10, \"page\": 1}'\n\"http://localhost:1337/citype/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "locations",
            "description": "<p>Array of Citypes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  \"citypeid\": 1,\n  \"citype\": \"switch\"\n},\n{\n  \"citypeid\": 2,\n  \"citype\": \"rack\"\n},\n{\n  \"citypeid\": 3,\n  \"citype\": \"blade\"\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CitypeController.js",
    "groupTitle": "Citype",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400321",
            "description": "<p>No Configuration Item locations available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400321",
          "content": "{\n  \"code\": 400321\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/citype/update",
    "title": "Update",
    "name": "updateCitype",
    "group": "Citype",
    "description": "<p>On input of all valid parameters this function updates the type definitions</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGVzdC5\n1c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGx\nveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3NTczMTgyMywiZXhwIjoxNDc1NzUzNDIzLCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.RCcGmdowferp2sMlFWTKRHkZkeowIs4Twl4\nZiCzlDck\" -H \"Content-Type: application/json\" -d\n '[\n    {\"citypeid\": 2, \"citype\": \"abra ka dabra\"},\n    {\"citypeid\": 3, \"citype\": \"khulja simsim\"}\n  ]'\n \"http://localhost:1337/citype/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "citype",
            "description": "<p>Array of Citypes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": " [\n  {\"citypeid\": 2, \"citype\": \"abra ka dabra\"},\n  {\"citypeid\": 3, \"citype\": \"khulja simsim\"}\n]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "location",
            "description": "<p>Details of the added location</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  [\n    {\n      \"citypeid\": 2,\n      \"citype\": \"abra ka dabra\"\n    }\n  ],\n  [\n    {\n      \"citypeid\": 3,\n      \"citype\": \"khulja simsim\"\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CitypeController.js",
    "groupTitle": "Citype",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400302",
            "description": "<p>One or more entered Configuration Item Types already exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400303",
            "description": "<p>One or more entered Configuration Item Types do not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400302",
          "content": "{\n  \"code\": 400302\n}",
          "type": "json"
        },
        {
          "title": "400303",
          "content": "{\n  \"code\": 400303\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Clientcache",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ClientcacheController.js",
    "groupTitle": "Clientcache",
    "name": ""
  },
  {
    "group": "Clientcache",
    "name": "Getdallata",
    "type": "post",
    "url": "/clientcache/getalldata",
    "title": "Getalldata",
    "description": "<p>Sends key-value pairs to the client for caching</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmlyc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkb\nWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRtaW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicG\nhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc\n1NjQ4MDQyLCJleHAiOjE0NzU2Njk2NDIsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20i\nfQ.YUjKq-HPEH9_SiNE8f0-PlL0KBB6W2URoIq1eDfXLHw\" \"http://localhost:1337/clientcache/getalldata\"",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "cachedata",
            "description": "<p>Data to be cached</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"users\": [\n    {\n      \"userid\": 1,\n      \"username\": \"administrator\"\n    }\n  ],\n  \"bds\": [\n    {\n      \"bdid\": 1,\n      \"bdname\": \"BusinessDepartment 1\"\n    }\n  ],\n  \"queues\": [\n    {\n      \"queueid\": 1,\n      \"queuename\": \"TestQueue1\"\n    }\n  ],\n  \"services\": [\n    {\n      \"serviceid\": 1,\n      \"servicename\": \"Cinderella Spanking Workshop\"\n    }\n  ],\n  \"cis\": [\n    {\n      \"ciid\": 1,\n      \"ciname\": \"HD63J-34JD3\"\n    }\n  ],\n  \"citypes\": [\n    {\n      \"citypeid\": 1,\n      \"citype\": \"switch\"\n    }\n  ],\n  \"vendors\": [\n    {\n      \"vendorid\": 1,\n      \"vendorname\": \"IBM\"\n    }\n  ],\n  \"cienvironments\": [\n    {\n      \"envid\": 1,\n      \"envname\": \"development\"\n    }\n  ],\n  \"timestamp\": 1475757978527\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ClientcacheController.js",
    "groupTitle": "Clientcache",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Clientcache",
    "name": "isvalid",
    "type": "post",
    "url": "/clientcache/isvalid",
    "title": "isValid",
    "description": "<p>Validates the cache and sends a new data set incase of validation failure. Sending a timestamp earlier then 1 results in error.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "Strng",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MSwiZmly\nc3RuYW1lIjoiU3VwZXIiLCJsYXN0bmFtZSI6IlVzZXIiLCJ1c2VybmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbGlkIjoiYWRta\nW5pc3RyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW\n1wbG95ZWVpZCI6IkZURTAwMDAwMSIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc1NjU0MTY3LCJleHAiOjE0NzU2NzU3NjcsImF\n1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.iZAEucKg4Oaf2lwuPi4LE3sChJdn31rSa\nBagO7zu0Fg\" -H \"Content-Type: application/json\" -d '{ \"timestamp\" : 1475675137161 }'\n\"http://localhost:1337/clientcache/isvalid\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "timestamp",
            "description": "<p>Timestamp of the existig cache on the client</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"timestamp\" : 1475675137161\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "cachedata",
            "description": "<p>Data to be cached</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "'Client Cache Invalid'",
          "content": "{\n  \"users\": [\n    {\n      \"userid\": 1,\n      \"username\": \"administrator\"\n    }\n  ],\n  \"bds\": [\n    {\n      \"bdid\": 1,\n      \"bdname\": \"BusinessDepartment 1\"\n    }\n  ],\n  \"queues\": [\n    {\n      \"queueid\": 1,\n      \"queuename\": \"TestQueue1\"\n    }\n  ],\n  \"services\": [\n    {\n      \"serviceid\": 1,\n      \"servicename\": \"Cinderella Spanking Workshop\"\n    }\n  ],\n  \"cis\": [\n    {\n      \"ciid\": 1,\n      \"ciname\": \"HD63J-34JD3\"\n    }\n  ],\n  \"citypes\": [\n    {\n      \"citypeid\": 1,\n      \"citype\": \"switch\"\n    }\n  ],\n  \"vendors\": [\n    {\n      \"vendorid\": 1,\n      \"vendorname\": \"IBM\"\n    }\n  ],\n  \"cienvironments\": [\n    {\n      \"envid\": 1,\n      \"envname\": \"development\"\n    }\n  ],\n  \"timestamp\": 1475757978527\n}",
          "type": "json"
        },
        {
          "title": "'Client Cache Valid'",
          "content": "true",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ClientcacheController.js",
    "groupTitle": "Clientcache",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/company/update",
    "title": "Update",
    "name": "updateLocation",
    "group": "Company",
    "description": "<p>On input of all valid parameters this function will update the company details</p>",
    "permission": [
      {
        "name": "superuser",
        "title": "Superuser access rights needed.",
        "description": "<p>This is a cross business department action and requires superuser access rights.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwiY29tcGFueWlkIjoxLCJ1c2VyaWQiOjEsImZpcnN0bmFtZSI6IlN1cGV\nyIiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJhZG1pbmlzdHJhdG9yIiwiZW1haWxpZCI6ImFkbWluaXN0cmF0b3JAdGV\nzdGRvbWFpbi5jb20iLCJjb3VudHJ5Y29kZSI6Iis5MSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGxveWVlaWQiOiJ\nGVEUwMDAwMDEiLCJpc2FjdGl2ZSI6dHJ1ZSwicGFzc2V4cGlyZWQiOm51bGx9LCJpYXQiOjE0ODAwNjA1OTEsImV4cCI6MTQ4MDA\n4MjE5MSwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3MiOiJ3d3cucXVldWVsb2FkLmNvbSJ9.76qrozCqQyYMALABqcBFeM\nTQYVdPLlJSQlN0t4cfbiM\" -d\n '{\n   \"supportmgr\" : \"support@queueload.com\"\n }' \"http://localhost:1337/company/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>Name of the company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Address</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "accntadmin",
            "description": "<p>User id of the acount admin(Must be an active superuser)</p>"
          },
          {
            "group": "Parameter",
            "type": "Email",
            "optional": true,
            "field": "supportmgr",
            "description": "<p>Email ID of the support manager from queueload</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"supportmgr\" : \"support@queueload.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "location",
            "description": "<p>Details of the added location</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"accountid\": \"qlodtest\",\n    \"name\": \"Wayne Enterprise\",\n    \"address\": \"Gotham\",\n    \"accntadmin\": 1,\n    \"supportmgr\": \"support@queueload.com\",\n    \"isactive\": null\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CompanyController.js",
    "groupTitle": "Company",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400103",
            "description": "<p>One or more of the entered users are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400322",
            "description": "<p>The entered location does not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400521",
            "description": "<p>Company doesn't exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400522",
            "description": "<p>Account administrator must have the super user privilege</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "400103",
          "content": "{\n  \"code\": 400103\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400322",
          "content": "{\n  \"code\": 400322\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        },
        {
          "title": "400521",
          "content": "{\n  \"code\": 400521\n}",
          "type": "json"
        },
        {
          "title": "400522",
          "content": "{\n  \"code\": 400522\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Environment",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CienvironmentController.js",
    "groupTitle": "Environment",
    "name": ""
  },
  {
    "group": "Environment",
    "name": "addEnvs",
    "type": "post",
    "url": "/cienvironment/add",
    "title": "Add",
    "description": "<p>This API Add an environment to the system</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "Strng",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZmlyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpc\nnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bW\nJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNTQ3MjA\nwLCJleHAiOjE0NzI1Njg4MDAsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.RmAS8\nLT-9ZKSCAUrky_LoRFmY_zMhFksNSNPu45X3dM\" -H \"Content-Type: application/json\"\n-d '{\"envnames\": [\"development\", \"testing\", \"production\"]}' \"http://localhost:1337/cienvironment/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "envnames",
            "description": "<p>Array of environments</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"envnames\": [\"development\", \"testing\", \"production\"]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "environments",
            "description": "<p>Array of added environments</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  \"envid\": 1,\n  \"envname\": \"development\"\n}, {\n  \"envid\": 2,\n  \"envname\": \"testing\"\n}, {\n  \"envid\": 3,\n  \"envname\": \"production\"\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CienvironmentController.js",
    "groupTitle": "Environment",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400312",
            "description": "<p>One or more environments entered already exist in the system</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400312",
          "content": "{\n  \"code\": 400312\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Environment",
    "name": "listEnvs",
    "type": "post",
    "url": "/cienvironment/list",
    "title": "List",
    "description": "<p>This API lists environments in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "Strng",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.ey\nJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iL\nCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5\nOTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjUzMjQ3MywiZXhwIjoxNDcyNTU0MDc\nzLCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.-S8nsNHDf2OlWzdCbYz96ambz1HsslwhiU\nNmcV73bHY\" -H \"Content-Type: application/json\" -d '{\"page\" : 1, \"limit\" : 100}' \"http://localhost:1337/cienvironment/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "environments",
            "description": "<p>Array of environments</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"envid\": 1,\n    \"envname\": \"development\",\n    \"numberofcis\": 4\n  },\n  {\n    \"envid\": 2,\n    \"envname\": \"testing\",\n    \"numberofcis\": 4\n  },\n  {\n    \"envid\": 3,\n    \"envname\": \"production\",\n    \"numberofcis\": 4\n  },\n  {\n    \"envid\": 4,\n    \"envname\": \"yelpdoc\",\n    \"numberofcis\": 4\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CienvironmentController.js",
    "groupTitle": "Environment",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400311",
            "description": "<p>No Configuration Item environments available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400311",
          "content": "{\n \"code\": 400311\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Environment",
    "name": "updateEnvs",
    "type": "post",
    "url": "/cienvironment/update",
    "title": "Update",
    "description": "<p>This API updates environments in the system</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "Strng",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hc\nmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW\n1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU0NzA\n4NiwiZXhwIjoxNDcyNTY4Njg2LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.OVDR\nHfrePDa69X6aTaeUU46ptr6Hbg4FRH4t9sWII8w\" -H \"Content-Type: application/json\"\n    -d '[{\"envid\": 2, \"envname\": \"mobproduction\"},{\"envid\": 3, \"envname\": \"mobdev\"}]'\n\"http://localhost:1337/cienvironment/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "envname",
            "description": "<p>Array of environments</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "[\n  {\"envid\": 2, \"envname\": \"mobproduction\"},\n  {\"envid\": 3, \"envname\": \"mobdev\"}\n]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "environments",
            "description": "<p>Array of updated environments</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  [{\n    \"envid\": 2,\n    \"envname\": \"mobproduction\"\n  }],\n  [{\n    \"envid\": 3,\n    \"envname\": \"mobdev\"\n  }]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CienvironmentController.js",
    "groupTitle": "Environment",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400312",
            "description": "<p>One or more environments entered already exist in the system</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400313",
            "description": "<p>The entered environments do not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400312",
          "content": "{\n  \"code\": 400312\n}",
          "type": "json"
        },
        {
          "title": "400313",
          "content": "{\n  \"code\": 400313\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Hardware",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CihardwareController.js",
    "groupTitle": "Hardware",
    "name": ""
  },
  {
    "group": "Hardware",
    "name": "addHardware",
    "type": "post",
    "url": "/cihardware/add",
    "title": "Add",
    "description": "<p>On input of all valid parameters this function will add Ci hardware to the system</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYE\nJdx6oNW4\" -H \"Content-Type: application/json\"\n -d '{\n   \"hardwaretag\" : \"TGY47IK829\",\n   \"manufacturervendorid\" : 3,\n   \"hwmodel\" : \"HF-482R\",\n   \"gridloc\" : \"AF-YH5\",\n   \"partnum\" : \"IYU3M14O4Q\"\n   }' \"http://localhost:1337/cihardware/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hardwaretag",
            "description": "<p>Unique Tag ID of the hardware asset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "manufacturervendorid",
            "description": "<p>ID of the Vendor</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hwmodel",
            "description": "<p>Model of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "partnum",
            "description": "<p>Unique partnumber for the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "serialnumber",
            "description": "<p>Unique serial number of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "rack",
            "description": "<p>Rack# of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "rackunit",
            "description": "<p>Form factor of the hardware in RUs</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gridloc",
            "description": "<p>Grid Location of the hardware</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "slot",
            "description": "<p>Slot number of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "macid",
            "description": "<p>MAC Address (Valid delimiters between octets are : and -)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "osname",
            "description": "<p>Operating System</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "osvendorid",
            "description": "<p>Vendor ID of the operating system</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "osversion",
            "description": "<p>Release version of the operating system</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "biosserial",
            "description": "<p>Serial Number of the BIOS</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "biosvendorid",
            "description": "<p>Vendor ID of the BIOS manufacturer</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "biosmodel",
            "description": "<p>Model of the BIOS</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "phymemmb",
            "description": "<p>Physical Memory in MB</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "psuwattage",
            "description": "<p>Power Consumption of PSU in Watts</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"hardwaretag\": \"TGY47IK829\",\n  \"manufacturervendorid\": 3,\n  \"hwmodel\": \"HF-482R\",\n  \"gridloc\": \"AF-YH5\",\n  \"partnum\": \"IYU3M14O4Q\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "hardware",
            "description": "<p>Added hardware</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"machineid\": 1,\n  \"hardwaretag\": \"TGY47IK829\",\n  \"manufacturervendorid\": 1,\n  \"hwmodel\": \"HF-482R\",\n  \"serialnumber\": null,\n  \"rack\": null,\n  \"rackunit\": null,\n  \"gridloc\": \"AF-YH5\",\n  \"slot\": null,\n  \"macid\": null,\n  \"osname\": null,\n  \"osvendorid\": null,\n  \"osversion\": null,\n  \"biosserial\": null,\n  \"biosvendorid\": null,\n  \"biosmodel\": null,\n  \"phymemmb\": null,\n  \"psuwattage\": null,\n  \"partnum\": \"IYU3M14O4Q\",\n  \"createdAt\": \"2016-08-30T13:25:50.000Z\",\n  \"updatedAt\": \"2016-08-30T13:25:50.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CihardwareController.js",
    "groupTitle": "Hardware",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400332",
            "description": "<p>One or more of entered vendors do not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400332",
          "content": "{\n  \"code\": 400332\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Hardware",
    "name": "getDetail",
    "type": "post",
    "url": "/cihardware/getdetail",
    "title": "Getdetail",
    "description": "<p>On input of a machineid it retrieves the corresponding Cihardware details</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml\nyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R\n1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l\nlZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNTU3OTk0LCJleHAiOjE0NzI1Nzk1OTQsImF1ZCI\n6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.M4X6sDthnrHlIyoztUDoOB37OOSAg4EpZMhr\nnAQUxYc\" -H \"Content-Type: application/json\" -d '{\"machineid\": 1}' \"http://localhost:1337/cihardware/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "machineid",
            "description": "<p>ID of the machine</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n \"machineid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "hardware",
            "description": "<p>Details of the requested hardware</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"manufacturervendorid\": {\n    \"vendorid\": 1,\n    \"vendorname\": \"Cyberdyne Systems\"\n  },\n  \"osvendorid\": null,\n  \"biosvendorid\": null,\n  \"machineid\": 1,\n  \"hardwaretag\": \"TGY47IK829\",\n  \"hwmodel\": \"HF-482R\",\n  \"serialnumber\": null,\n  \"rack\": null,\n  \"rackunit\": null,\n  \"gridloc\": \"AF-YH5\",\n  \"slot\": null,\n  \"macid\": null,\n  \"osname\": null,\n  \"osversion\": null,\n  \"biosserial\": null,\n  \"biosmodel\": null,\n  \"phymemmb\": null,\n  \"psuwattage\": null,\n  \"partnum\": \"IYU3M14O4Q\",\n  \"createdAt\": \"2016-08-30T13:25:50.000Z\",\n  \"updatedAt\": \"2016-08-30T13:25:50.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CihardwareController.js",
    "groupTitle": "Hardware",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400352",
            "description": "<p>The entered Configuration Item Hardware does not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400352",
          "content": "{\n  \"code\": 400352\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Hardware",
    "name": "listHardware",
    "type": "post",
    "url": "/cihardware/list",
    "title": "List",
    "description": "<p>On input of all valid parameters this function will list the hardware assets</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml\nyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R\n1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l\nlZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNTU3OTk0LCJleHAiOjE0NzI1Nzk1OTQsImF1ZCI\n6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.M4X6sDthnrHlIyoztUDoOB37OOSAg4EpZMhr\nnAQUxYc\" -H \"Content-Type: application/json\" -d '{\"page\":1,\"limit\": 10}' \"http://localhost:1337/cihardware/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "hardwares",
            "description": "<p>Array of Hardwares</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  \"manufacturervendorid\": 1,\n  \"osvendorid\": null,\n  \"biosvendorid\": null,\n  \"machineid\": 1,\n  \"hardwaretag\": \"TGY47IK829\",\n  \"hwmodel\": \"HF-482R\",\n  \"serialnumber\": null,\n  \"rack\": null,\n  \"rackunit\": null,\n  \"gridloc\": \"AF-YH5\",\n  \"slot\": null,\n  \"macid\": null,\n  \"osname\": null,\n  \"osversion\": null,\n  \"biosserial\": null,\n  \"biosmodel\": null,\n  \"phymemmb\": null,\n  \"psuwattage\": null,\n  \"partnum\": \"IYU3M14O4Q\",\n  \"createdAt\": \"2016-08-30T13:25:50.000Z\",\n  \"updatedAt\": \"2016-08-30T13:25:50.000Z\"\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CihardwareController.js",
    "groupTitle": "Hardware",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400351",
            "description": "<p>No Configuration Item Hardware available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400351",
          "content": "{\n  \"code\": 400351\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Hardware",
    "name": "search",
    "type": "post",
    "url": "/cihardware/search",
    "title": "Search",
    "description": "<p>Searches the model and returns the result. The number of search column and order by criteria are flexible.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": " curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\n yc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\n ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n 5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQ\n iOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYE\n Jdx6oNW4\" -H \"Content-Type: application/json\" -d\n'{\n  \"search\": {\"partnum\": \"IYU3M14%\"},\n  \"result\": [\"hardwaretag\", \"manufacturervendorid\", \"hwmodel\", \"serialnumber\", \"gridloc\", \"partnum\"],\n  \"orderby\": \"hardwaretag\"\n}' \"http://localhost:1337/cihardware/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"search\":  { \"partnum\": \"IYU3M14%\" },\n  \"result\" : [ \"hardwaretag\", \"manufacturervendorid\", \"hwmodel\", \"serialnumber\", \"gridloc\", \"partnum\"],\n  \"orderby\": \"hardwaretag\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>First element-Number of records, Second element-Array of results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  1, [{\n    \"manufacturervendorid\": 1,\n    \"hardwaretag\": \"TGY47IK829\",\n    \"hwmodel\": \"HF-482R\",\n    \"serialnumber\": null,\n    \"gridloc\": \"BK-LR9\",\n    \"partnum\": \"IYU3M14O4Q\"\n  }]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CihardwareController.js",
    "groupTitle": "Hardware",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Hardware",
    "name": "update",
    "type": "post",
    "url": "/cihardware/update",
    "title": "Update",
    "description": "<p>On input of a machineid it updates a Hardware</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYE\nJdx6oNW4\" -H \"Content-Type: application/json\" -d '\n {\n   \"machineid\" : 1,\n   \"gridloc\" : \"BK-LR9\"\n }'\n \"http://localhost:1337/cihardware/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "machineid",
            "description": "<p>ID of the machine</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "hardwaretag",
            "description": "<p>Unique Tag ID of the hardware asset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "manufacturervendorid",
            "description": "<p>ID of the Vendor</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "hwmodel",
            "description": "<p>Model of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "partnum",
            "description": "<p>Unique partnumber for the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "serialnumber",
            "description": "<p>Unique serial number of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "rack",
            "description": "<p>Rack# of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "rackunit",
            "description": "<p>Form factor of the hardware in RUs</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "gridloc",
            "description": "<p>Grid Location of the hardware</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "slot",
            "description": "<p>Slot number of the asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "macid",
            "description": "<p>MAC Address (Valid delimiters between octets are : and -)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "osname",
            "description": "<p>Operating System</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "osvendorid",
            "description": "<p>Vendor ID of the operating system</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "osversion",
            "description": "<p>Release version of the operating system</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "biosserial",
            "description": "<p>Serial Number of the BIOS</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "biosvendorid",
            "description": "<p>Vendor ID of the BIOS manufacturer</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "biosmodel",
            "description": "<p>Model of the BIOS</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "phymemmb",
            "description": "<p>Physical Memory in MB</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "psuwattage",
            "description": "<p>Power Consumption of PSU in Watts</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"machineid\" : 1,\n  \"gridloc\" : \"BK-LR9\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "hardware",
            "description": "<p>Details of the updated hardware</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"machineid\": 1,\n  \"hardwaretag\": \"TGY47IK829\",\n  \"manufacturervendorid\": 1,\n  \"hwmodel\": \"HF-482R\",\n  \"serialnumber\": null,\n  \"rack\": null,\n  \"rackunit\": null,\n  \"gridloc\": \"BK-LR9\",\n  \"slot\": null,\n  \"macid\": null,\n  \"osname\": null,\n  \"osvendorid\": null,\n  \"osversion\": null,\n  \"biosserial\": null,\n  \"biosvendorid\": null,\n  \"biosmodel\": null,\n  \"phymemmb\": null,\n  \"psuwattage\": null,\n  \"partnum\": \"IYU3M14O4Q\",\n  \"createdAt\": \"2016-08-30T13:25:50.000Z\",\n  \"updatedAt\": \"2016-08-30T14:06:57.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CihardwareController.js",
    "groupTitle": "Hardware",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400352",
            "description": "<p>The entered Configuration Item Hardware does not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400352",
          "content": "{\n  \"code\": 400352\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/add",
    "title": "Add",
    "name": "addIncident",
    "group": "Incident",
    "description": "<p>On input of all valid parameters this function will add an incident</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n   \"title\": \"This is a test incident ticket\",\n   \"description\": \"Description of the test incident ticket\",\n   \"serviceid\": 1,\n   \"ownerqueueid\": 3,\n   \"owneruserid\": 2,\n   \"assignedqueueid\": 3,\n   \"severity\": 3,\n   \"impact\": 1,\n   \"startedat\" : 1476788690,\n   \"attachments\" : [1,2,3]\n }' \"http://localhost:1337/incident/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the affected service</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ciid",
            "description": "<p>ID of the affected CI</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ownerqueueid",
            "description": "<p>ID of the queue owning the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "owneruserid",
            "description": "<p>ID of the user owning the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assignedqueueid",
            "description": "<p>ID of the queue the ticket being assigned to</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assigneduserid",
            "description": "<p>ID of the user the ticket being assigned to</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "startedat",
            "description": "<p>Time in epoch format. Must be equal or less then the current time and endedat(if present)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "endedat",
            "description": "<p>Time in epoch format. Must be equal or less then the current time. Must be equal or more then the starttime.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "severity",
            "description": "<p>Severity of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "attachments",
            "description": "<p>Array of attachments IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"title\": \"This is the 1st test inc\",\n  \"description\": \"Description of the test incident ticket\",\n  \"serviceid\": 1,\n  \"ownerqueueid\": 3,\n  \"owneruserid\": 2,\n  \"assignedqueueid\": 3,\n  \"severity\": 3,\n  \"impact\": 1,\n  \"startedat\" : 1476788690,\n  \"attachments\" : [1,3,5]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "incident",
            "description": "<p>Details of the added incident</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"attachments\": [\n    {\n      \"attachmentid\": 1,\n      \"filename\": \"51xk3hyQvHL.jpg\",\n      \"size\": \"33003\"\n    },\n    {\n      \"attachmentid\": 3,\n      \"filename\": \"37774-1920x1200.jpg\",\n      \"size\": \"3135190\"\n    },\n    {\n      \"attachmentid\": 5,\n      \"filename\": \"steam.desktop\",\n      \"size\": \"2191\"\n    }\n  ],\n  \"relatedinc\": [],\n  \"serviceid\": {\n    \"serviceid\": 1,\n    \"servicename\": \"Cinderella Spanking Workshop\",\n    \"servicestateid\": 2,\n    \"details\": \"The castle has much more to offer.\",\n    \"isfrozen\": false,\n    \"freezecause\": 0,\n    \"cbid\": 6,\n    \"bdid\": 1\n  },\n  \"ownerqueueid\": {\n    \"queueid\": 3,\n    \"queuename\": \"TestQueue3\",\n    \"queueemail\": \"testqueue3@test.com\",\n    \"alertl1\": \"testqueue3alert1@test.com\",\n    \"alertl2\": \"testqueue3alert2@test.com\",\n    \"alertl3\": \"testqueue3alert3@test.com\",\n    \"isactive\": true,\n    \"isfrozen\": false,\n    \"freezecause\": 0,\n    \"cbid\": 6,\n    \"queueownerid\": 2,\n    \"incidentmanagerid\": 2,\n    \"changemanagerid\": 2,\n    \"srtmanagerid\": 2\n  },\n  \"owneruserid\": {\n    \"userid\": 2,\n    \"firstname\": \"Test\",\n    \"lastname\": \"User\",\n    \"username\": \"T3st.User11\",\n    \"emailid\": \"test.user11@testclient1.com\",\n    \"countrycode\": \"+1\",\n    \"phonenumber\": \"9999999999\",\n    \"employeeid\": \"FTE000002\",\n    \"isactive\": true,\n    \"defaultqueue\": 4\n  },\n  \"assignedqueueid\": {\n    \"queueid\": 3,\n    \"queuename\": \"TestQueue3\",\n    \"queueemail\": \"testqueue3@test.com\",\n    \"alertl1\": \"testqueue3alert1@test.com\",\n    \"alertl2\": \"testqueue3alert2@test.com\",\n    \"alertl3\": \"testqueue3alert3@test.com\",\n    \"isactive\": true,\n    \"isfrozen\": false,\n    \"freezecause\": 0,\n    \"cbid\": 6,\n    \"queueownerid\": 2,\n    \"incidentmanagerid\": 2,\n    \"changemanagerid\": 2,\n    \"srtmanagerid\": 2\n  },\n  \"incidentid\": 1,\n  \"state\": 2,\n  \"title\": \"This is the 1st test inc\",\n  \"description\": \"Description of the test incident ticket\",\n  \"startedat\": 1476788690,\n  \"endedat\": null,\n  \"severity\": 3,\n  \"vendorsr\": null,\n  \"journal\": [\n    {\n      \"type\": 1,\n      \"time\": 1484226873,\n      \"operatorid\": 27\n    }\n  ],\n  \"resolutioncode\": null,\n  \"resolutionnotes\": null,\n  \"closurecomments\": null,\n  \"visibility\": \"operator\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400452",
            "description": "<p>Invalid severity code</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400453",
            "description": "<p>Invalid impact code</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400103",
            "description": "<p>One or more of the entered users are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400253",
            "description": "<p>One or more of the entered Queues are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400254",
            "description": "<p>One or more of the entered Queues do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400258",
            "description": "<p>One or more of the entered users is not a member of the queue</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400369",
            "description": "<p>One or more services is not in approved state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400402",
            "description": "<p>Requested file doesn't exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        },
        {
          "title": "400452",
          "content": "{\n  \"code\": 400452\n}",
          "type": "json"
        },
        {
          "title": "400453",
          "content": "{\n  \"code\": 400453\n}",
          "type": "json"
        },
        {
          "title": "400103",
          "content": "{\n  \"code\": 400103\n}",
          "type": "json"
        },
        {
          "title": "400253",
          "content": "{\n  \"code\": 400253\n}",
          "type": "json"
        },
        {
          "title": "400254",
          "content": "{\n  \"code\": 400254\n}",
          "type": "json"
        },
        {
          "title": "400258",
          "content": "{\n  \"code\": 400258\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400369",
          "content": "{\n  \"code\": 400369\n}",
          "type": "json"
        },
        {
          "title": "400402",
          "content": "{\n  \"code\": 400402\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/cancel",
    "title": "Cancel",
    "name": "cancelIncident",
    "group": "Incident",
    "description": "<p>On input of all valid parameters this function will cancel an incident</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS\nI6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM\nSIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi\nOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDYxMzc0LCJ\nleHAiOjE0Nzc0ODI5NzQsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.16VKw8l\n4qWn9_dgQ4VvPfNgMWgftBXVMESSMvnS4C7I\" -H \"Content-Type: application/json\" -d\n'{\n  \"incidentid\": 2,\n  \"journal\" : \"auto fixed\"\n}' \"http://localhost:1337/incident/cancel\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "incidentid",
            "description": "<p>ID of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "journal",
            "description": "<p>Comments to be entered in the journal</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"incidentid\": 2,\n  \"journal\" : \"auto fixed\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "incident",
            "description": "<p>Details of the added incident</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"incidentid\": 2,\n  \"state\": 7,\n  \"title\": \"This is the 2nd test inc\",\n  \"description\": \"Description of the test incident ticket\",\n  \"serviceid\": 1,\n  \"ciid\": null,\n  \"ownerqueueid\": 1,\n  \"owneruserid\": 2,\n  \"assignedqueueid\": 3,\n  \"assigneduserid\": null,\n  \"startedat\": 1476788690,\n  \"endedat\": null,\n  \"severity\": 3,\n  \"impact\": 4,\n  \"vendorid\": null,\n  \"vendorsr\": null,\n  \"pendinginc\": null,\n  \"journal\": [\n    {\n      \"type\": 1,\n      \"time\": 1484226873,\n      \"operatorid\": 27\n    }\n  ],\n  \"resolutioncode\": 3,\n  \"resolutionnotes\": \"Cancelled\",\n  \"closurecomments\": \"Cancelled\",\n  \"visibility\": null\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400454",
            "description": "<p>One or more of the entered Incidents do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400461",
            "description": "<p>Please provide an explanation</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400471",
            "description": "<p>Requester doesn't have the privilege to perform the action</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400473",
            "description": "<p>Current state of the incident doesn't allow cancelation</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400454",
          "content": "{\n  \"code\": 400454\n}",
          "type": "json"
        },
        {
          "title": "400461",
          "content": "{\n  \"code\": 400461\n}",
          "type": "json"
        },
        {
          "title": "400471",
          "content": "{\n  \"code\": 400471\n}",
          "type": "json"
        },
        {
          "title": "400473",
          "content": "{\n  \"code\": 400473\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/getdetail",
    "title": "GetDetail",
    "name": "getDetail",
    "group": "Incident",
    "description": "<p>On input of all valid parameters this function will receive details of the an incident</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS\n6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxMS\nIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIiO\niI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDYxMzc0LCJl\neHAiOjE0Nzc0ODI5NzQsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.16VKw8l4\nqWn9_dgQ4VvPfNgMWgftBXVMESSMvnS4C7I\" -H \"Content-Type: application/json\" -d\n'{\n  \"incidentid\" : 1\n}' \"http://localhost:1337/incident/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "incidentid",
            "description": "<p>ID of the incident</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"incidentid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "incident",
            "description": "<p>Details of the modified incident</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"attachments\": [],\n  \"relatedinc\": [],\n  \"serviceid\": {\n    \"serviceid\": 1,\n    \"servicename\": \"Cinderella Spanking Workshop\",\n    \"servicestateid\": 2,\n    \"details\": \"The castle has much more to offer.\",\n    \"isfrozen\": false,\n    \"freezecause\": 0,\n    \"cbid\": 6,\n    \"bdid\": 1\n  },\n  \"ownerqueueid\": {\n    \"queueid\": 1,\n    \"queuename\": \"TestQueue1\",\n    \"queueemail\": \"testqueue1@test.com\",\n    \"alertl1\": \"testqueue1alert1@test.com\",\n    \"alertl2\": \"testqueue1alert2@test.com\",\n    \"alertl3\": \"testqueue1alert3@test.com\",\n    \"isactive\": true,\n    \"isfrozen\": false,\n    \"freezecause\": null,\n    \"cbid\": 5,\n    \"queueownerid\": 2,\n    \"incidentmanagerid\": 2,\n    \"changemanagerid\": 2,\n    \"srtmanagerid\": 2\n  },\n  \"owneruserid\": {\n    \"userid\": 2,\n    \"firstname\": \"Test\",\n    \"lastname\": \"User\",\n    \"username\": \"T3st.User11\",\n    \"emailid\": \"test.user11@testclient1.com\",\n    \"countrycode\": \"+1\",\n    \"phonenumber\": \"9999999999\",\n    \"employeeid\": \"FTE000002\",\n    \"isactive\": true,\n    \"defaultqueue\": 4\n  },\n  \"assignedqueueid\": {\n    \"queueid\": 3,\n    \"queuename\": \"TestQueue3\",\n    \"queueemail\": \"testqueue3@test.com\",\n    \"alertl1\": \"testqueue3alert1@test.com\",\n    \"alertl2\": \"testqueue3alert2@test.com\",\n    \"alertl3\": \"testqueue3alert3@test.com\",\n    \"isactive\": true,\n    \"isfrozen\": false,\n    \"freezecause\": 0,\n    \"cbid\": 6,\n    \"queueownerid\": 2,\n    \"incidentmanagerid\": 2,\n    \"changemanagerid\": 2,\n    \"srtmanagerid\": 2\n  },\n  \"pendinginc\": {\n    \"incidentid\": 7,\n    \"state\": 2,\n    \"title\": \"This is the 1st test inc\",\n    \"description\": \"Description of the test incident ticket\",\n    \"startedat\": 1476788690,\n    \"endedat\": null,\n    \"severity\": 3,\n    \"impact\": 4,\n    \"vendorsr\": null,\n    \"journal\": [\n      {\n        \"type\": 1,\n        \"time\": 1484226873,\n        \"operatorid\": 27\n      }\n    ],\n    \"resolutioncode\": null,\n    \"resolutionnotes\": null,\n    \"closurecomments\": null,\n    \"visibility\": \"operator\",\n    \"serviceid\": 1,\n    \"ciid\": null,\n    \"ownerqueueid\": 1,\n    \"owneruserid\": 2,\n    \"assignedqueueid\": 3,\n    \"assigneduserid\": null,\n    \"vendorid\": null,\n    \"pendinginc\": 7\n  },\n  \"incidentid\": 1,\n  \"state\": 7,\n  \"title\": \"This is the 1st test inc\",\n  \"description\": \"Description of the test incident ticket\",\n  \"startedat\": 1476788690,\n  \"endedat\": null,\n  \"severity\": 3,\n  \"impact\": 4,\n  \"vendorsr\": null,\n  \"journal\": [\n    {\n      \"type\": 1,\n      \"time\": 1484226873,\n      \"operatorid\": 27\n    }\n  ],\n  \"resolutioncode\": 3,\n  \"resolutionnotes\": \"Cancelled\",\n  \"closurecomments\": \"Cancelled\",\n  \"visibility\": null\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400454",
            "description": "<p>One or more of the entered Incidents do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400454",
          "content": "{\n  \"code\": 400454\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/search",
    "title": "Search",
    "name": "incidentSearch",
    "group": "Incident",
    "description": "<p>On input of a valid search parameters this function returns the selected attributes in the chosen sorting method</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS\nI6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM\nSIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi\nOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDg4Mjc5LCJ\nleHAiOjE0Nzc1MDk4NzksImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.o5tOeaK\n0w5BihwC2jEa59RpTKcHF3J9lmJ43BUjxxH4\" -H \"Content-Type: application/json\" -d\n  '{\n    \"search\":  { \"state\": 3 },\n    \"result\" : [ \"incidentid\", \"title\", \"state\", \"assignedqueueid\", \"assigneduserid\"],\n    \"orderby\": \"incidentid\"\n  }' \"http://localhost:1337/incident/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object search criteria</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example-Simple",
          "content": "{\n  \"search\":  { \"state\": 3 },\n  \"result\" : [ \"incidentid\", \"title\", \"state\", \"assignedqueueid\", \"assigneduserid\"],\n  \"orderby\": \"incidentid\"\n}",
          "type": "json"
        },
        {
          "title": "Example-Before startedat",
          "content": "{\n  \"search\":  {\"startedat\": { \"<=\": 1489740150 }},\n  \"result\" : [\"incidentid\", \"serviceid\", \"startedat\", \"endedat\", \"resolvedat\"],\n  \"orderby\": \"incidentid\"\n}",
          "type": "json"
        },
        {
          "title": "Example-After endedat",
          "content": "{\n  \"search\":  {\"endedat\": { \">=\": 1489740000 }},\n  \"result\" : [\"incidentid\", \"serviceid\", \"startedat\", \"endedat\", \"resolvedat\"],\n  \"orderby\": \"incidentid\"\n}",
          "type": "json"
        },
        {
          "title": "Example-Between startime and endedat",
          "content": "{\n  \"search\":  {\"startedat\": { \">=\": 1489501000}, \"endedat\": { \"<=\": 1489740200 }},\n  \"result\" : [\"incidentid\", \"serviceid\", \"startedat\", \"endedat\", \"resolvedat\"],\n  \"orderby\": \"incidentid\"\n}",
          "type": "json"
        },
        {
          "title": "Example-Between startime range",
          "content": "{\n  \"search\":  {\"startedat\": { \">=\": 1489749058, \"<=\": 1489749070 }},\n  \"result\" : [\"incidentid\", \"serviceid\", \"startedat\", \"endedat\", \"resolvedat\"],\n  \"orderby\": \"incidentid\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "incidentList",
            "description": "<p>List of incidents</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  1,\n  [\n    {\n      \"assignedqueueid\": 3,\n      \"assigneduserid\": 8,\n      \"incidentid\": 1,\n      \"title\": \"This is the 1st test inc\",\n      \"state\": 3\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/list",
    "title": "List",
    "name": "listIncident",
    "group": "Incident",
    "description": "<p>On input of all valid parameters this function will list incidents</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n   \"page\" : 1,\n   \"limit\" : 100\n }' \"http://localhost:1337/incident/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "incidents",
            "description": "<p>Array of incidents</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"serviceid\": 1,\n    \"ciid\": null,\n    \"ownerqueueid\": 3,\n    \"owneruserid\": 2,\n    \"assignedqueueid\": 3,\n    \"assigneduserid\": null,\n    \"vendorid\": null,\n    \"pendinginc\": null,\n    \"attachments\": null,\n    \"relatedinc\": null,\n    \"incidentid\": 1,\n    \"state\": \"open\",\n    \"title\": \"This is a test incident ticket\",\n    \"description\": \"Description of the test incident ticket\",\n    \"startedat\": \"1476788690\",\n    \"endedat\": null,\n    \"severity\": 3,\n    \"vendorsr\": null,\n    \"journal\": [\n      {\n        \"type\": 1,\n        \"time\": 1484226873,\n        \"operatorid\": 27\n      }\n    ],\n    \"resolutioncode\": null,\n    \"resolutionnotes\": null,\n    \"closurecomments\": null,\n    \"visibility\": \"operator\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400451",
            "description": "<p>No incidents in the system</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400451",
          "content": "{\n  \"code\": 400451\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/queuelist",
    "title": "QueueList",
    "name": "queueList",
    "group": "Incident",
    "description": "<p>On input of a valid queueid this function will send all incidents currently requiring attention from the queue</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS\nI6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM\nSIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi\nOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDg4Mjc5LCJ\nleHAiOjE0Nzc1MDk4NzksImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.o5tOeaK\n0w5BihwC2jEa59RpTKcHF3J9lmJ43BUjxxH4\" -H \"Content-Type: application/json\" -d\n'{\n  \"queueid\": 3\n}' \"http://localhost:1337/incident/queuelist\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "queueid",
            "description": "<p>ID of the queue</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"queueid\": 3\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "incidentList",
            "description": "<p>List of incidents</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"serviceid\": 1,\n    \"ciid\": null,\n    \"ownerqueueid\": 1,\n    \"owneruserid\": 2,\n    \"assignedqueueid\": 3,\n    \"assigneduserid\": null,\n    \"incidentid\": 2,\n    \"title\": \"This is the 1st test inc\",\n    \"state\": 2,\n    \"startedat\": 1476788690,\n    \"endedat\": null,\n    \"severity\": 3,\n    \"impact\": 4,\n    \"updatedAt\": \"2016-10-28T08:03:06.000Z\",\n    \"visibility\": \"operator\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/update",
    "title": "Update",
    "name": "updateIncident",
    "group": "Incident",
    "description": "<p>On input of all valid parameters this function will update an incident</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxd\nWV1ZSI6bnVsbCwidXNlcmlkIjo4LCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJ\nUM3N0LlVzZXIxMyIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxM0B0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxI\niwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAxMyIsImlzYWN0aXZlIjp0cnVlfSw\niaWF0IjoxNDc3Mzk3NjQyLCJleHAiOjE0Nzc0MTkyNDIsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3L\nnF1ZXVlbG9hZC5jb20ifQ.tWn2v19UkgGWVPHwHPd2SwDxwp7vD6-ps0LlJTUattU\" -H \"Content-Type: application/json\" -d\n  '{\n    \"incidentid\": 1,\n    \"state\" : 4\n  }' \"http://localhost:1337/incident/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "incidentid",
            "description": "<p>ID of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "state",
            "description": "<p>State the ticket to be transitioned into</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ciid",
            "description": "<p>ID of the affected CI</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "owneruserid",
            "description": "<p>ID of the user owning the incident. The user must be a member of ownerqueueid</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assignedqueueid",
            "description": "<p>ID of the queue the ticket being assigned to</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assigneduserid",
            "description": "<p>ID of the user the ticket being assigned to. The user must be a member of assignedqueueid</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "startedat",
            "description": "<p>Time in epoch format. Must be equal or less then the current time and endedat(if present)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "endedat",
            "description": "<p>Time in epoch format. Must be equal or less then the current time. Must be equal or more then the starttime.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "severity",
            "description": "<p>Severity of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "impact",
            "description": "<p>Impact of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "vendorid",
            "description": "<p>ID of the vendor</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "vendorsr",
            "description": "<p>SR# of opened with the vendor</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pendinginc",
            "description": "<p>ID of the incident causing pendency</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "journal",
            "description": "<p>Comments to be entered in the journal</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": true,
            "field": "relatedinc",
            "description": "<p>ID of the related incidents</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "resolutioncode",
            "description": "<p>Code of the resolution</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "resolutionnotes",
            "description": "<p>Notes from the operator</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "closurecomments",
            "description": "<p>Notes from the owner at the time of closure</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "attachments",
            "description": "<p>Array of attachments IDs</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "rating",
            "description": "<p>Rating of service satisfaction</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"incidentid\": 1,\n  \"state\" : 4\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "incident",
            "description": "<p>Details of the added incident</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"incidentid\": 1,\n  \"state\": 4,\n  \"title\": \"This is the 1st test inc\",\n  \"description\": \"Description of the test incident ticket\",\n  \"serviceid\": 1,\n  \"ciid\": null,\n  \"ownerqueueid\": 1,\n  \"owneruserid\": 2,\n  \"assignedqueueid\": 3,\n  \"assigneduserid\": 8,\n  \"startedat\": 1476788690,\n  \"endedat\": null,\n  \"severity\": 1,\n  \"impact\": 1,\n  \"vendorid\": null,\n  \"vendorsr\": null,\n  \"pendinginc\": null,\n  \"journal\": [\n    {\n      \"type\": 1,\n      \"time\": 1484226873,\n      \"operatorid\": 27\n    }\n  ],\n  \"resolutioncode\": null,\n  \"resolutionnotes\": null,\n  \"closurecomments\": null,\n  \"visibility\": \"operator\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400253",
            "description": "<p>One or more of the entered Queues are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400258",
            "description": "<p>One or more of the entered users is not a member of the queue</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400332",
            "description": "<p>One or more of entered vendors do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400389",
            "description": "<p>Configuration Item is not associated with the service</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400405",
            "description": "<p>One or more of the attachments do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400406",
            "description": "<p>Please provide unique set of attachments</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400454",
            "description": "<p>One or more of the entered Incidents do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400456",
            "description": "<p>The current state doesn't allow transition to the requested state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400458",
            "description": "<p>Ticket needs an assignee to move to further states</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400459",
            "description": "<p>Must provide a queueid to transisition into OPEN state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400461",
            "description": "<p>Please provide an explanation</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400462",
            "description": "<p>Please provide the res code and res notes and endtime</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400463",
            "description": "<p>Only journal entries are allowed while marking UNRESOLVED</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400464",
            "description": "<p>Please provide the closure notes</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400465",
            "description": "<p>Only closure notes are allowed while marking CLOSED</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400466",
            "description": "<p>Please provide the incident number causing the pendency of this ticket</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400467",
            "description": "<p>Please provide the vendor</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400468",
            "description": "<p>The ticket is already assigned to the requested member</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400469",
            "description": "<p>End time cannot be earlier then start time</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400470",
            "description": "<p>Requester is not part of any of the queues { &quot;code&quot;: 400470 }</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400471",
            "description": "<p>Requester doesn't have the privilege to perform the action</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400472",
            "description": "<p>All the sent attachments are already attached</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400477",
            "description": "<p>Invalid satisfaction rating</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400253",
          "content": "{\n  \"code\": 400253\n}",
          "type": "json"
        },
        {
          "title": "400258",
          "content": "{\n  \"code\": 400258\n}",
          "type": "json"
        },
        {
          "title": "400332",
          "content": "{\n  \"code\": 400332\n}",
          "type": "json"
        },
        {
          "title": "400389",
          "content": "{\n  \"code\": 400389\n}",
          "type": "json"
        },
        {
          "title": "400405",
          "content": "{\n  \"code\": 400405\n}",
          "type": "json"
        },
        {
          "title": "400406",
          "content": "{\n  \"code\": 400406\n}",
          "type": "json"
        },
        {
          "title": "400454",
          "content": "{\n  \"code\": 400454\n}",
          "type": "json"
        },
        {
          "title": "400456",
          "content": "{\n  \"code\": 400456\n}",
          "type": "json"
        },
        {
          "title": "400458",
          "content": "{\n  \"code\": 400458\n}",
          "type": "json"
        },
        {
          "title": "400459",
          "content": "{\n  \"code\": 400459\n}",
          "type": "json"
        },
        {
          "title": "400461",
          "content": "{\n  \"code\": 400461\n}",
          "type": "json"
        },
        {
          "title": "400462",
          "content": "{\n  \"code\": 400462\n}",
          "type": "json"
        },
        {
          "title": "400463",
          "content": "{\n  \"code\": 400463\n}",
          "type": "json"
        },
        {
          "title": "400464",
          "content": "{\n  \"code\": 400464\n}",
          "type": "json"
        },
        {
          "title": "400465",
          "content": "{\n  \"code\": 400465\n}",
          "type": "json"
        },
        {
          "title": "400466",
          "content": "{\n  \"code\": 400466\n}",
          "type": "json"
        },
        {
          "title": "400467",
          "content": "{\n  \"code\": 400467\n}",
          "type": "json"
        },
        {
          "title": "400468",
          "content": "{\n  \"code\": 400468\n}",
          "type": "json"
        },
        {
          "title": "400469",
          "content": "{\n  \"code\": 400469\n}",
          "type": "json"
        },
        {
          "title": "400471",
          "content": "{\n  \"code\": 400471\n}",
          "type": "json"
        },
        {
          "title": "400472",
          "content": "{\n  \"code\": 400472\n}",
          "type": "json"
        },
        {
          "title": "400477",
          "content": "{\n  \"code\": 400477\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/incident/userlist",
    "title": "UserList",
    "name": "userList",
    "group": "Incident",
    "description": "<p>On input of a valid userid this function will send all incidents currently requiring attention from the user</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS\nI6NCwidXNlcmlkIjoyLCJmaXJzdG5hbWUiOiJUZXN0IiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJUM3N0LlVzZXIxM\nSIsImVtYWlsaWQiOiJ0ZXN0LnVzZXIxMUB0ZXN0Y2xpZW50MS5jb20iLCJjb3VudHJ5Y29kZSI6IisxIiwicGhvbmVudW1iZXIi\nOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTAwMDAwMiIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc3NDg4Mjc5LCJ\nleHAiOjE0Nzc1MDk4NzksImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.o5tOeaK\n0w5BihwC2jEa59RpTKcHF3J9lmJ43BUjxxH4\" -H \"Content-Type: application/json\" -d\n'{\n  \"userid\": 8\n}' \"http://localhost:1337/incident/userlist\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userid",
            "description": "<p>ID of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"userid\": 8\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "incidentList",
            "description": "<p>List of incidents</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"serviceid\": 1,\n    \"ciid\": null,\n    \"ownerqueueid\": 1,\n    \"owneruserid\": 2,\n    \"assignedqueueid\": 3,\n    \"assigneduserid\": 8,\n    \"incidentid\": 2,\n    \"title\": \"This is the 1st test inc\",\n    \"state\": 3,\n    \"startedat\": 1476788690,\n    \"endedat\": null,\n    \"severity\": 3,\n    \"impact\": 4,\n    \"updatedAt\": \"2016-10-28T12:55:24.000Z\",\n    \"visibility\": \"operator\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/IncidentController.js",
    "groupTitle": "Incident",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Location",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CilocationController.js",
    "groupTitle": "Location",
    "name": ""
  },
  {
    "type": "post",
    "url": "/cilocation/add",
    "title": "Add",
    "name": "addLocation",
    "group": "Location",
    "description": "<p>On input of all valid parameters this function will add locations to the system</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ\n3fqnXwhQ\" -H \"Content-Type: application/json\" -d\n '{\n   \"locationname\": \"Rome-9W\",\n   \"locationcode\": \"IT-9W-F0L\",\n   \"buildingname\": \"EU AD\",\n   \"sitecategory\": \"Live Site\"\n }' \"http://localhost:1337/cilocation/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locationname",
            "description": "<p>Name of the location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locationcode",
            "description": "<p>Code of the location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "buildingname",
            "description": "<p>Name of the building</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sitecategory",
            "description": "<p>Category of the site</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n   \"locationname\": \"Rome-9W\",\n   \"locationcode\": \"IT-9W-F0L\",\n   \"buildingname\": \"EU AD\",\n   \"sitecategory\": \"Live Site\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "location",
            "description": "<p>Details of the added location</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"locationid\": 1,\n  \"locationname\": \"Rome-9W\",\n  \"locationcode\": \"IT-9W-F0L\",\n  \"buildingname\": \"EU AD\",\n  \"sitecategory\": \"Live Site\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CilocationController.js",
    "groupTitle": "Location",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Location",
    "name": "listLocations",
    "type": "post",
    "url": "/cilocation/list",
    "title": "List",
    "description": "<p>This API lists the locations in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml\nyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R\n1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l\nlZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNjI0NzAzLCJleHAiOjE0NzI2NDYzMDMsImF1ZCI\n6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.2QdRMod08w12s83GjbLduW5RRyCi2npgbhOt\njj2_Lk8\" -H \"Content-Type: application/json\" -d '{\"page\": 1,\"limit\": 10}' \"http://localhost:1337/cilocation/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "locations",
            "description": "<p>Array of Locations</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"locationid\": 1,\n    \"locationname\": \"Rome-9W\",\n    \"locationcode\": \"IT-9W-F0L\",\n    \"buildingname\": \"EU AD\",\n    \"sitecategory\": \"Live Site\",\n    \"numberofcis\": 4\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CilocationController.js",
    "groupTitle": "Location",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400321",
            "description": "<p>No Configuration Item locations available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400321",
          "content": "{\n  \"code\": 400321\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/cilocation/search",
    "title": "Search",
    "name": "searchLocation",
    "group": "Location",
    "description": "<p>searches the model and returns the result. The number of search column and order by criteria are flexible.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml\nyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R\n1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l\nlZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNjI0NzAzLCJleHAiOjE0NzI2NDYzMDMsImF1ZCI\n6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.2QdRMod08w12s83GjbLduW5RRyCi2npgbhOt\njj2_Lk8\" -H \"Content-Type: application/json\" -d\n'{\n   \"search\":  { \"sitecategory\": \"Live%\" },\n   \"result\" : [ \"locationname\", \"locationcode\", \"buildingname\", \"sitecategory\"],\n   \"orderby\": \"locationcode\"\n }' \"http://localhost:1337/cilocation/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched asset</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n   \"search\":  { \"sitecategory\": \"Live%\" },\n   \"result\" : [ \"locationname\", \"locationcode\", \"buildingname\", \"sitecategory\"],\n   \"orderby\": \"locationcode\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>First element-Number of records, Second element-Array of results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  1, [{\n    \"locationname\": \"Rome-9W\",\n    \"locationcode\": \"IT-9W-F0L\",\n    \"buildingname\": \"Pani house\",\n    \"sitecategory\": \"Live Site\"\n  }]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CilocationController.js",
    "groupTitle": "Location",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/cilocation/update",
    "title": "Update",
    "name": "updateLocation",
    "group": "Location",
    "description": "<p>On input of all valid parameters this function will update the location</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZml\nyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28\nucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG9\n5ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQ\niOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ\n3fqnXwhQ\" -H \"Content-Type: application/json\" -d\n '{\n   \"locationid\": 1,\n   \"buildingname\": \"Pani house\"\n }' \"http://localhost:1337/cilocation/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locationid",
            "description": "<p>ID of the location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "locationname",
            "description": "<p>Name of the location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "buildingname",
            "description": "<p>Name of the building</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sitecategory",
            "description": "<p>Category of the site</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n   \"locationid\": 1,\n   \"buildingname\": \"Pani house\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "location",
            "description": "<p>Details of the added location</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  \"locationid\": 1,\n  \"locationname\": \"Rome-9W\",\n  \"locationcode\": \"IT-9W-F0L\",\n  \"buildingname\": \"Pani house\",\n  \"sitecategory\": \"Live Site\"\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/CilocationController.js",
    "groupTitle": "Location",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400322",
            "description": "<p>The entered location does not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400322",
          "content": "{\n  \"code\": 400322\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "OrgApproval",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/OrgapprovalController.js",
    "groupTitle": "OrgApproval",
    "name": ""
  },
  {
    "group": "Queue",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/QueueController.js",
    "groupTitle": "Queue",
    "name": ""
  },
  {
    "group": "Reset",
    "name": "resetall",
    "type": "post",
    "url": "/reset/resetall",
    "title": "Resetall",
    "description": "<p>Destroys all data in the system except the primary super user details</p>",
    "permission": [
      {
        "name": "superuser",
        "title": "Superuser access rights needed.",
        "description": "<p>This is a cross business department action and requires superuser access rights.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6I\nkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwiYWNjb3VudGlkIjoiRFEwUTRZMzQiLCJ1c2VyaWQiOjEsImZpc\nnN0bmFtZSI6IlN1cGVyIiwibGFzdG5hbWUiOiJVc2VyIiwidXNlcm5hbWUiOiJhZG1pbmlzdHJhdG9yIiwiZW1haWxpZCI6Im\nFkbWluaXN0cmF0b3JAdGVzdGRvbWFpbi5jb20iLCJjb3VudHJ5Y29kZSI6bnVsbCwicGhvbmVudW1iZXIiOm51bGwsImVtcGx\nveWVlaWQiOiJGVEUwMDAwMDEiLCJpc2FjdGl2ZSI6dHJ1ZSwicGFzc2V4cGlyZWQiOmZhbHNlfSwiaWF0IjoxNDgzNTk4MDI4\nLCJleHAiOjE0ODM2MTk2MjgsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.TU\nVqDow7VdUiYqpDpHcF3oT2T3GlrbrgJSNI3vRz0mw\" \"http://localhost:1337/reset/resetall\"",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "NA",
            "description": "<p>No return on sucess</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ResetController.js",
    "groupTitle": "Reset",
    "error": {
      "fields": {
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "name": ""
  },
  {
    "group": "Service",
    "name": "addService",
    "type": "post",
    "url": "/service/add",
    "title": "Add",
    "description": "<p>On input of all valid parameters this function will add a service</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjY\nyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0\n.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\" -d\n'{\n   \"servicename\" : \"Cinderella Spanking Workshop\",\n   \"cbid\" : 1,\n   \"details\" : \"The castle has much more to offer.\"\n }' \"http://localhost:1337/service/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "servicename",
            "description": "<p>Name of the service</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cbid",
            "description": "<p>Chargeback code of the service</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "details",
            "description": "<p>Description of service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"servicename\" : \"Cinderella Spanking Workshop\",\n  \"cbid\" : 1,\n  \"details\" : \"The castle has much more to offer.\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "network",
            "description": "<p>Added network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"serviceid\": 1,\n  \"servicename\": \"Cinderella Spanking Workshop\",\n  \"cbid\": 1,\n  \"servicestateid\": 1,\n  \"details\": \"The castle has much more to offer.\",\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400203",
            "description": "<p>One or more of the entered Chargeback codes are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400204",
            "description": "<p>One or more of the entered Chargeback codes do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400208",
            "description": "<p>One or more of the chargeback codes entered is frozen. Frozen chargeback codes cannot be processed</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400203",
          "content": "{\n  \"code\": 400203\n}",
          "type": "json"
        },
        {
          "title": "400204",
          "content": "{\n  \"code\": 400204\n}",
          "type": "json"
        },
        {
          "title": "400208",
          "content": "{\n  \"code\": 400208\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "approve",
    "type": "post",
    "url": "/service/approve",
    "title": "Approve",
    "description": "<p>Approves service state transfer requests</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "State Transition",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZmlyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6I\nmZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9u\nZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc\nyNjUxNDA1LCJleHAiOjE0NzI2NzMwMDUsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb2\n0ifQ.q71r83U-qIXUc8gQB2CJv2HNRCtz0KMrpGGQMCbwO4Y\" -d\n  '{\n    \"type\" : \"state\",\n    \"approvalid\" : 1,\n    \"isapproved\" : true,\n    \"serviceid\" : 1\n  }' \"http://localhost:1337/service/approve\"",
        "type": "curl"
      },
      {
        "title": "Association",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZmlyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6I\nmZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9u\nZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDc\nyNjUxNDA1LCJleHAiOjE0NzI2NzMwMDUsImF1ZCI6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb2\n0ifQ.q71r83U-qIXUc8gQB2CJv2HNRCtz0KMrpGGQMCbwO4Y\" -d\n  '{\n    \"type\" : \"assoc\",\n    \"approvalid\" : 4,\n    \"serviceid\" : 1,\n    \"isapproved\" : true,\n    \"ciid\" : 1\n  }' \"http://localhost:1337/service/approve\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the request. Possible values are &quot;assoc&quot;, &quot;state&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "approvalid",
            "description": "<p>ID of the approval</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "serviceid",
            "description": "<p>ID of the service</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isapproved",
            "description": "<p>'true' for approved, 'false' for reject</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ciid",
            "description": "<p>ID of the CI</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "State Transition",
          "content": "{\n  \"type\" : \"state\",\n  \"approvalid\" : 1,\n  \"isapproved\" : true,\n  \"serviceid\" : 1\n}",
          "type": "json"
        },
        {
          "title": "Association",
          "content": "{\n  \"type\" : \"assoc\",\n  \"approvalid\" : 4,\n  \"serviceid\" : 1,\n  \"isapproved\" : true,\n  \"ciid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "service",
            "description": "<p>Details of the service</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "approval",
            "description": "<p>Details of the approval</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "State Transition",
          "content": "{\n  \"serviceid\": 1,\n  \"servicename\": \"Wayne Trainning Service\",\n  \"cbid\": 1,\n  \"servicestateid\": 2,\n  \"details\": \"Trainning service for Wayne Family\",\n  \"isfrozen\": false,\n  \"freezecause\" : \"0\"\n}",
          "type": "json"
        },
        {
          "title": "Association - Pending approvals",
          "content": "{\n  \"approverid\": 1,\n  \"approverserviceid\": 2,\n  \"ciid\": 1,\n  \"approvalid\": 5,\n  \"isapproved\": null,\n  \"type\": \"assoc\",\n  \"isactive\": true\n}",
          "type": "json"
        },
        {
          "title": "Association - Last approval",
          "content": "{\n  \"ciid\": 1,\n  \"ciname\": \"HSG4-JSJKAQM-T\",\n  \"assettag\": \"SAMAK82KD\",\n  \"cistateid\": 1,\n  \"citypeid\": 3,\n  \"envid\": 2,\n  \"supportqueue\": 1,\n  \"contractid\": null,\n  \"vendorid\": 2,\n  \"warrantyexpiry\": null,\n  \"maintenanceend\": null,\n  \"customerfacing\": false,\n  \"isundermaint\": true,\n  \"machineid\": null,\n  \"cinetworkid\": null,\n  \"systemsoftware1\": \"Splunk\",\n  \"systemsoftware2\": null,\n  \"systemsoftware3\": null,\n  \"customapp1\": null,\n  \"customapp2\": null,\n  \"customapp3\": null,\n  \"ciownerid\": 8,\n  \"locationid\": 1,\n  \"installationdate\": 1480516283,\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400366",
            "description": "<p>The approval has already been processed</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400367",
            "description": "<p>The user is not the designated approver</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400368",
            "description": "<p>The approval ID doesn't exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400370",
            "description": "<p>Incorrect service</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400375",
            "description": "<p>Incorrect CI</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400366",
          "content": "{\n  \"code\": 400366\n}",
          "type": "json"
        },
        {
          "title": "400367",
          "content": "{\n  \"code\": 400367\n}",
          "type": "json"
        },
        {
          "title": "400368",
          "content": "{\n  \"code\": 400368\n}",
          "type": "json"
        },
        {
          "title": "400370",
          "content": "{\n  \"code\": 400370\n}",
          "type": "json"
        },
        {
          "title": "400375",
          "content": "{\n  \"code\": 400375\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "assoc",
    "type": "post",
    "url": "/service/assoc",
    "title": "Assoc",
    "description": "<p>associated CI to Service</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I\nm1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv\nbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ\n3MjcwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2\n9tIn0.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM\" -d\n'{\n  \"serviceids\": [4],\n  \"ciid\" : 1\n}' \"http://localhost:1337/service/assoc\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "serviceid",
            "description": "<p>Array of service IDs</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ciid",
            "description": "<p>Name of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"serviceids\": [4],\n  \"ciid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "approval",
            "description": "<p>List of approvals to obtain</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  [\n    {\n      \"approvalid\": 4,\n      \"isapproved\": null,\n      \"approverid\": 2,\n      \"approverserviceid\": 1,\n      \"ciid\": 1,\n      \"type\": \"assoc\",\n      \"isactive\": true,\n      \"createdAt\": \"2016-05-10T16:50:21.000Z\",\n      \"updatedAt\": \"2016-05-10T16:50:21.000Z\"\n    }\n  ],\n  [\n    {\n      \"approvalid\": 5,\n      \"isapproved\": null,\n      \"approverid\": 1,\n      \"approverserviceid\": 2,\n      \"ciid\": 1,\n      \"type\": \"assoc\",\n      \"isactive\": true,\n      \"createdAt\": \"2016-05-10T16:50:21.000Z\",\n      \"updatedAt\": \"2016-05-10T16:50:21.000Z\"\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400363",
            "description": "<p>The requested service is frozen</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400369",
            "description": "<p>One or more services is not in approved state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400372",
            "description": "<p>No new service association requested</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400373",
            "description": "<p>Configuration Item between Approved and Commissioned state must be associated with a service</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400382",
            "description": "<p>One or more of entered Configuration Items do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400383",
            "description": "<p>Requested Configuration Item is frozen at the moment.</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400387",
            "description": "<p>Configuration Item state doesn't support the requested operation</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400363",
          "content": "{\n  \"code\": 400363\n}",
          "type": "json"
        },
        {
          "title": "400369",
          "content": "{\n  \"code\": 400369\n}",
          "type": "json"
        },
        {
          "title": "400372",
          "content": "{\n  \"code\": 400372\n}",
          "type": "json"
        },
        {
          "title": "400373",
          "content": "{\n  \"code\": 400373\n}",
          "type": "json"
        },
        {
          "title": "400382",
          "content": "{\n  \"code\": 400382\n}",
          "type": "json"
        },
        {
          "title": "400383",
          "content": "{\n  \"code\": 400383\n}",
          "type": "json"
        },
        {
          "title": "400387",
          "content": "{\n  \"code\": 400387\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "cancel",
    "type": "post",
    "url": "/service/cancel",
    "title": "Cancel",
    "description": "<p>Cancels a Service in Registration Phase</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I\nm1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv\nbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ\n3MjcwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2\n9tIn0.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM\" -d '{\"serviceid\" : 1}'\n\"http://localhost:1337/service/cancel\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"serviceid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "service",
            "description": "<p>Details of the cancelled service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"serviceid\": 1,\n  \"servicename\": \"Wayne Trainning Service\",\n  \"cbid\": 1,\n  \"servicestateid\": 3,\n  \"details\": \"Trainning service for Wayne Family\",\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400363",
            "description": "<p>The requested service is frozen</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400374",
            "description": "<p>Service cannot be cancelled as it's not in the registration state</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400363",
          "content": "{\n  \"code\": 400363\n}",
          "type": "json"
        },
        {
          "title": "400374",
          "content": "{\n  \"code\": 400374\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "getDetail",
    "type": "post",
    "url": "/service/getdetail",
    "title": "Getdetail",
    "description": "<p>On input of a ServiceId it retrieves the corresponding service details</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjY\nyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0\n.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\" -d '{\"serviceid\": 1}' \"http://localhost:1337/service/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"serviceid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "service",
            "description": "<p>Details of the requested service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"ciids\":\n   [\n    {\n      \"ciid\": 1,\n      \"ciname\": \"HJ4K2-74JK\",\n      \"assettag\": \"JFU5-73JD\",\n      \"cistateid\": 1,\n      \"contractid\": null,\n      \"warrantyexpiry\": null,\n      \"maintenanceend\": null,\n      \"customerfacing\": false,\n      \"isundermaint\": true,\n      \"systemsoftware1\": null,\n      \"systemsoftware2\": null,\n      \"systemsoftware3\": null,\n      \"customapp1\": null,\n      \"customapp2\": null,\n      \"customapp3\": null,\n      \"installationdate\": 1480516283,\n      \"isfrozen\": false,\n      \"freezecause\": 0,\n      \"citypeid\": 1,\n      \"envid\": 1,\n      \"supportqueue\": null,\n      \"vendorid\": 1,\n      \"machineid\": null,\n      \"cinetworkid\": null,\n      \"ciownerid\": 15,\n      \"locationid\": 1\n    },\n    {\n      \"ciid\": 3,\n      \"ciname\": \"JSDKA-23L\",\n      \"assettag\": \"FJ72-KWDJ\",\n      \"cistateid\": 1,\n      \"contractid\": null,\n      \"warrantyexpiry\": null,\n      \"maintenanceend\": null,\n      \"customerfacing\": false,\n      \"isundermaint\": true,\n      \"systemsoftware1\": null,\n      \"systemsoftware2\": null,\n      \"systemsoftware3\": null,\n      \"customapp1\": null,\n      \"customapp2\": null,\n      \"customapp3\": null,\n      \"installationdate\": 1480516283,\n      \"isfrozen\": false,\n      \"freezecause\": 0,\n      \"citypeid\": 1,\n      \"envid\": 1,\n      \"supportqueue\": null,\n      \"vendorid\": 1,\n      \"machineid\": null,\n      \"cinetworkid\": null,\n      \"ciownerid\": 15,\n      \"locationid\": 1\n    }\n  ],\n  \"crntsrvsundappvl\": [],\n  \"newsrvsundappvl\": [],\n  \"serviceidsforapproval\": [],\n  \"incidents\":\n  [\n    {\n      \"incidentid\": 2,\n      \"state\": 5,\n      \"title\": \"This is a test incident ticket\",\n      \"description\": \"Description of the test incident ticket\",\n      \"serviceid\": 1,\n      \"ciid\": 1,\n      \"ownerqueueid\": 1,\n      \"owneruserid\": 9,\n      \"assignedqueueid\": 3,\n      \"assigneduserid\": 13,\n      \"startedat\": 1479845009,\n      \"endtime\": 1480084112,\n      \"severity\": 2,\n      \"impact\": 3,\n      \"vendorid\": null,\n      \"vendorsr\": null,\n      \"pendinginc\": null,\n      \"journal\": [Object],\n      \"resolutioncode\": 1,\n      \"resolutionnotes\": \"resolved\",\n      \"closurecomments\": null,\n      \"visibility\": \"owner\",\n      \"rating\": null,\n      \"updatedAt\": '2016-11-25T14:28:32.000Z'\n    },\n    {\n      \"incidentid\": 3,\n      \"state\": 2,\n      \"title\": \"This is a test incident ticket\",\n      \"description\": \"Description of the test incident ticket\",\n      \"serviceid\": 1,\n      \"ciid\": null,\n      \"ownerqueueid\": 2,\n      \"owneruserid\": 11,\n      \"assignedqueueid\": 3,\n      \"assigneduserid\": 13,\n      \"startedat\": 1480084108,\n      \"endtime\": null,\n      \"severity\": 3,\n      \"impact\": 4,\n      \"vendorid\": null,\n      \"vendorsr\": null,\n      \"pendinginc\": null,\n      \"journal\": [Object],\n      \"resolutioncode\": null,\n      \"resolutionnotes\": null,\n      \"closurecomments\": null,\n      \"visibility\": \"operator\",\n      \"rating\": null,\n      \"updatedAt\": \"2016-11-25T14:28:30.000Z\"\n    }\n  ],\n  \"cbid:\n  {\n    \"cbid\": 1,\n    \"isactive\": true,\n    \"isfrozen\": false,\n    \"freezecause\": 0,\n    \"bdid\": 1,\n    \"cbownerid\": 6\n  },\n  \"serviceid\": 1,\n  \"servicename\": \"incservice1\",\n  \"servicestateid\": 2,\n  \"details\": \"inc service1 details\",\n  \"isfrozen\": false,\n  \"freezecause\": 0\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "listService",
    "type": "post",
    "url": "/service/list",
    "title": "List",
    "description": "<p>This function lists all the services in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1hc\nmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmVudW\n1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjYyNDc\n1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.2Ayj\nN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\" -d\n'{\n  \"page\" : 1,\n  \"limit\" : 100\n}' \"http://localhost:1337/service/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "service",
            "description": "<p>Array of Services</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"cbid\": 1,\n    \"servicestateid\": 1,\n    \"serviceid\": 1,\n    \"servicename\": \"Wayne Trainning Service\",\n    \"details\": \"Trainning service for Wayne Family\",\n    \"isfrozen\": false,\n    \"freezecause\": \"0\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400361",
            "description": "<p>No Services available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400361",
          "content": "{\n  \"code\": 400361\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "listapproval",
    "type": "post",
    "url": "/service/listapproval",
    "title": "ListApproval",
    "description": "<p>Lists all the active service approvals for the requester</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I\nm1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv\nbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ\n3MjcwNDI3OSwiZXhwIjoxNDcyNzI1ODc5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2\n9tIn0.awgUCyNtcG75UgLzg7WuTblcFseXUXb3Cgr5jBYV5vM\" \"http://localhost:1337/service/listapporval\"",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "approval",
            "description": "<p>List of approvals pending for the requester</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"crntsrvids\":\n      [\n        {\n          \"serviceid\": 2,\n          \"servicename\": \"Sober Name\",\n          \"servicestateid\": 2,\n          \"details\": \"unfreezing after next check\",\n          \"isfrozen\": false,\n          \"freezecause\": 0,\n          \"cbid\": 1\n        }\n      ],\n    \"newserviceids\": [],\n    \"approverid\": 6,\n    \"approverserviceid\": 2,\n    \"ciid\": 2,\n    \"approvalid\": 5,\n    \"isapproved\": null,\n    \"type\": 'assoc',\n    \"isactive\": true\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400377",
            "description": "<p>The requester has no pending service approvals</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400377",
          "content": "{\n  \"code\": 400377\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "next",
    "type": "post",
    "url": "/service/next",
    "title": "Next",
    "description": "<p>Initiates the state transition of a service</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I\nm1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv\nbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ\n3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2\n9tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\" -d\n'{\n  \"serviceid\" : 1\n}' \"http://localhost:1337/service/next\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"serviceid\" : 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "service",
            "description": "<p>Details of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "'Registration' ==> 'Approved'",
          "content": "{\n  \"serviceid\": 1,\n  \"servicename\": \"Wayne Trainning Service\",\n  \"cbid\": 1,\n  \"servicestateid\": 1,\n  \"details\": \"Trainning service for Wayne Family\",\n  \"isfrozen\": true,\n  \"freezecause\": \"5\"\n}",
          "type": "json"
        },
        {
          "title": "'Approved' ==> 'Archived'",
          "content": "{\n  \"serviceid\": 1,\n  \"servicename\": \"Wayne Trainning Service\",\n  \"cbid\": 1,\n  \"servicestateid\": 3,\n  \"details\": \"Trainning service for Wayne Family\",\n  \"isfrozen\": false,\n  \"stateapprovalids\": null,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400208",
            "description": "<p>One or more of the chargeback codes entered is frozen. Frozen chargeback codes cannot be processed</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400363",
            "description": "<p>The requested service is frozen</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400364",
            "description": "<p>The service is associated with one or more CIs</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400365",
            "description": "<p>The service cannot be taken to an undefined state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400371",
            "description": "<p>The service has pending approvals associated with it</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400208",
          "content": "{\n  \"code\": 400208\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400363",
          "content": "{\n  \"code\": 400363\n}",
          "type": "json"
        },
        {
          "title": "400364",
          "content": "{\n  \"code\": 400364\n}",
          "type": "json"
        },
        {
          "title": "400365",
          "content": "{\n  \"code\": 400365\n}",
          "type": "json"
        },
        {
          "title": "400371",
          "content": "{\n  \"code\": 400371\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "queueassign",
    "type": "post",
    "url": "/service/queueassign",
    "title": "Queueassign",
    "description": "<p>Assigns a queue to the service</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI\n6bnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjI3LCJmdWxsbmFtZSI6ImluY2ZpcnN0IGluY09wZXJhdG9yIiw\niZmlyc3RuYW1lIjoiaW5jZmlyc3QiLCJsYXN0bmFtZSI6ImluY09wZXJhdG9yIiwidXNlcm5hbWUiOiJpbmNmaXJzdC5vcGVyYXR\nvciIsImVtYWlsaWQiOiJpbmNmaXJzdC5vcGVyYXRvckB0ZXN0ZG9tYWluLmNvbSIsImNvdW50cnljb2RlIjpudWxsLCJwaG9uZW5\n1bWJlciI6bnVsbCwiZW1wbG95ZWVpZCI6IklOQ0ZURTAwMDAwOSIsImlzYWN0aXZlIjp0cnVlLCJwYXNzZXhwaXJlZCI6ZmFsc2U\nsInVzZXJzZXR0aW5ncyI6bnVsbH0sImlhdCI6MTQ4OTQwNzQ3OCwiZXhwIjoxNDg5NDI5MDc4LCJhdWQiOiJ3d3cucXVldWVsb2F\nkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.TPycl3k6BQ-ZiXNr2lpT78nWBmX9k4WzfGIpRktYQfs\"\n-H \"Content-Type: application/json\" -d\n'{\n  \"serviceid\" : 4,\n  \"supportqueue\" : 3,\n  \"assign\" : true\n}' \"http://localhost:1337/service/queueassign\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"serviceid\" : 4,\n  \"supportqueue\" : 3,\n  \"assign\" : true\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "approval",
            "description": "<p>Details of the approval</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"approvalid\": 60,\n  \"typeofapproval\": 7,\n  \"entityid\": 4,\n  \"targetentityid\": 3,\n  \"approverid\": 24,\n  \"initiatorid\": 20,\n  \"isapproved\": null,\n  \"isactive\": true\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400253",
            "description": "<p>One or more of the entered Queues are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400254",
            "description": "<p>One or more of the entered Queues do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400363",
            "description": "<p>The requested service is frozen</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400264",
            "description": "<p>One or more of the queues entered is frozen. Frozen queues cannot be processed</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400378",
            "description": "<p>Service not in 'approved' state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400380",
            "description": "<p>Service is in Archived state</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400388",
            "description": "<p>Configuration Item doesn't have any support queue</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400391",
            "description": "<p>Configuration Item has open incidents with the requested queue</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403501",
            "description": "<p>The requester is not an admin of the BD</p>"
          },
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400253",
          "content": "{\n  \"code\": 400253\n}",
          "type": "json"
        },
        {
          "title": "400254",
          "content": "{\n  \"code\": 400254\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400363",
          "content": "{\n  \"code\": 400363\n}",
          "type": "json"
        },
        {
          "title": "400264",
          "content": "{\n  \"code\": 400264\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400378",
          "content": "{\n  \"code\": 400378\n}",
          "type": "json"
        },
        {
          "title": "400380",
          "content": "{\n  \"code\": 400380\n}",
          "type": "json"
        },
        {
          "title": "400388",
          "content": "{\n  \"code\": 400388\n}",
          "type": "json"
        },
        {
          "title": "400391",
          "content": "{\n  \"code\": 400391\n}",
          "type": "json"
        },
        {
          "title": "403501",
          "content": "{\n  \"code\": 403501\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "search",
    "type": "post",
    "url": "/service/search",
    "title": "Search",
    "description": "<p>Searches the model and returns the result. The number of search column and order by criteria are flexible.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MywiZml\nyc3RuYW1lIjoiZmlyc3QiLCJsYXN0bmFtZSI6InVzZXIiLCJ1c2VybmFtZSI6ImZpcnN0LnVzZXIiLCJlbWFpbGlkIjoiZmlyc3R\n1c2VyQHRlc3RjbGllbnQuY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3l\nlZWlkIjoiRlRFODc2MjgyMyIsImlzYWN0aXZlIjp0cnVlfSwiaWF0IjoxNDcyNzEyNTkxLCJleHAiOjE0NzI3MzQxOTEsImF1ZCI\n6Ind3dy5xdWV1ZWxvYWQuY29tIiwiaXNzIjoid3d3LnF1ZXVlbG9hZC5jb20ifQ.ySVS_rf2Qw-9U1c6pcxliNDE-JQiZ7gQ5eJG\naXt2Z7Q\" -H \"Content-Type: application/json\" -d\n'{\n   \"search\":  { \"servicename\": \"Wayne%\" },\n   \"result\" : [ \"servicename\", \"cbid\", \"servicestateid\", \"details\" ],\n   \"orderby\": \"servicename\"\n }' \"http://localhost:1337/service/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched service</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"search\":  { \"servicename\": \"Wayne%\" },\n  \"result\" : [ \"servicename\", \"cbid\", \"servicestateid\", \"details\" ],\n  \"orderby\": \"servicename\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>First element-Number of records, Second element-Array of results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  1,\n  [\n    {\n      \"cbid\": 1,\n      \"servicestateid\": 1,\n      \"servicename\": \"Wayne Trainning Service\",\n      \"details\": \"Trainning service for Wayne Family\"\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Service",
    "name": "update",
    "type": "post",
    "url": "/service/update",
    "title": "Update",
    "description": "<p>Updates a service</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6Ikp\nXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6I\nm1hcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhv\nbmVudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ\n3MjYyNDc1NCwiZXhwIjoxNDcyNjQ2MzU0LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY2\n9tIn0.2AyjN_GIY_YtBuxDTZnFL1T4UfrPxJScPrQ3fqnXwhQ\" -d\n'{\n  \"serviceid\" : 2,\n  \"servicename\" : \"Wayne Trainning Service\",\n  \"details\" : \"Trainning service for Wayne Family\"\n}' \"http://localhost:1337/service/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>ID of the service</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "servicename",
            "description": "<p>Name of the service</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "details",
            "description": "<p>Short detail of the service</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "cbid",
            "description": "<p>Chargeback code of the service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"serviceid\" : 1,\n  \"servicename\" : \"Wayne Trainning Service\",\n  \"details\" : \"Trainning service for Wayne Family\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "service",
            "description": "<p>Details of the updated service</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"serviceid\": 1,\n  \"servicename\": \"Wayne Trainning Service\",\n  \"cbid\": 1,\n  \"servicestateid\": 1,\n  \"details\": \"Trainning service for Wayne Family\",\n  \"isfrozen\": false,\n  \"freezecause\": \"0\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/ServiceController.js",
    "groupTitle": "Service",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400362",
            "description": "<p>One or more of the entered service do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400363",
            "description": "<p>The requested service is frozen</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400376",
            "description": "<p>Service state doesn't permit action. Please use '/orgapproval/initiatetransfer' to update chargeback code.</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400362",
          "content": "{\n  \"code\": 400362\n}",
          "type": "json"
        },
        {
          "title": "400363",
          "content": "{\n  \"code\": 400363\n}",
          "type": "json"
        },
        {
          "title": "400376",
          "content": "{\n  \"code\": 400376\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Template",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "name": ""
  },
  {
    "group": "Template",
    "name": "addTemplate",
    "type": "post",
    "url": "/template/add",
    "title": "Add",
    "description": "<p>This function adds a template to the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV\nXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2\n91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R\npdmUiOnRydWV9LCJpYXQiOjE0Nzc1NzQ5NTQsImV4cCI6MTQ3NzU5NjU1NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi\nOiJ3d3cucXVldWVsb2FkLmNvbSJ9.Nmadtgu6EXBxMJMc8ZcmNvo92V2aqATW5ZcU4IiecTo\" -d\n  '{\n    \"type\" : 1,\n    \"templatename\" : \"test inc templt5\",\n    \"title\" : \"title of the inc\",\n    \"description\" : \"description of the inc\",\n    \"servicename\" : \"some servive\",\n    \"ciname\" : \"some ci\",\n    \"severity\" : 1,\n    \"impact\" : 2,\n    \"attachments\" : [1,2,3]\n  }' \"http://localhost:1337/template/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the ticket template being created. E.g. Incident: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "templatename",
            "description": "<p>Name of the template being created. Must be unique</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Title of the ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description of the ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "servicename",
            "description": "<p>Name of the affected service</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "ciname",
            "description": "<p>Name of the affected configuration item</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ownerqueuename",
            "description": "<p>Name of the owner queue</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ownerusername",
            "description": "<p>Name of the owner user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assignedqueuename",
            "description": "<p>Name of the assigned queue</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assignedusername",
            "description": "<p>Name of the assgined user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "severity",
            "description": "<p>Severity of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "impact",
            "description": "<p>Impact of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": true,
            "field": "attachments",
            "description": "<p>Attachments</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isactive",
            "defaultValue": "true",
            "description": "<p>Visibility status of the template</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"type\" : 1,\n  \"templatename\" : \"test inc templt5\",\n  \"title\" : \"title of the inc\",\n  \"description\" : \"description of the inc\",\n  \"servicename\" : \"some servive\",\n  \"ciname\" : \"some ci\",\n  \"severity\" : 1,\n  \"impact\" : 2,\n  \"attachments\" : [1,2,3]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "templates",
            "description": "<p>Details of the added template</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"templateid\": 7,\n  \"templatename\": \"test inc templt5\",\n  \"lasteditorid\": 1,\n  \"title\": \"title of the inc\",\n  \"description\": \"description of the inc\",\n  \"ownerqueuename\": null,\n  \"ownerusername\": null,\n  \"assignedqueuename\": null,\n  \"assignedusername\": null,\n  \"servicename\": \"some servive\",\n  \"ciname\": \"some ci\",\n  \"severity\": 1,\n  \"impact\": 2,\n  \"isactive\": true,\n  \"updatedAt\": \"2016-10-29T04:21:06.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Template",
    "name": "deleteTemplate",
    "type": "post",
    "url": "/template/delete",
    "title": "Delete",
    "description": "<p>This function deletes the mentioned template</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV\nXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2\n91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R\npdmUiOnRydWV9LCJpYXQiOjE0Nzc1NTE1NDQsImV4cCI6MTQ3NzU3MzE0NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi\nOiJ3d3cucXVldWVsb2FkLmNvbSJ9.tFwQlQZ8CTABuxEas87c5uzOCgJH9-6tGh10nBAbP6E\" -d\n'{\n  \"type\" : 1,\n  \"templateid\" : 7\n}' \"http://localhost:1337/template/delete\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "templateid",
            "description": "<p>ID of the template being sought the details of</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the ticket template being created. E.g. Incident: 1</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"type\" : 1,\n  \"templateid\" : 7\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "template",
            "description": "<p>Details of the deleted template</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"templateid\": 7,\n  \"templatename\": \"test inc templt5\",\n  \"lasteditorid\": 1,\n  \"title\": \"title of the inc\",\n  \"description\": \"description of the inc\",\n  \"ownerqueuename\" : \"pen-making\",\n  \"ownerusername\" : \"ek.worker\",\n  \"assignedqueuename\" : \"refill-making\",\n  \"assignedusername\" : \"aurek.worker\",\n  \"servicename\": \"some servive\",\n  \"ciname\": \"some ci\",\n  \"severity\": 1,\n  \"impact\": 2,\n  \"isactive\": true,\n  \"updatedAt\": \"2016-10-29T04:25:56.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400501",
            "description": "<p>The requested template doesn't exist</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400501",
          "content": "{\n  \"code\": 400501\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Template",
    "name": "getdetailTemplate",
    "type": "post",
    "url": "/template/getdetail",
    "title": "Getdetail",
    "description": "<p>This function requests for the detail of a template</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV\nXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2\n91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R\npdmUiOnRydWV9LCJpYXQiOjE0Nzc1NzQ5NTQsImV4cCI6MTQ3NzU5NjU1NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi\nOiJ3d3cucXVldWVsb2FkLmNvbSJ9.Nmadtgu6EXBxMJMc8ZcmNvo92V2aqATW5ZcU4IiecTo\" -d\n'{\n  \"type\" : 1,\n  \"templateid\" : 7\n}' \"http://localhost:1337/template/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the ticket template being created. E.g. Incident: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "templateid",
            "description": "<p>ID of the template being sought the details of</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"type\" : 1,\n  \"templateid\" : 7\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "templates",
            "description": "<p>Details of the sought template</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"lasteditorid\": 1,\n  \"templateid\": 7,\n  \"templatename\": \"test inc templt5\",\n  \"title\": \"title of the inc\",\n  \"description\": \"description of the inc\",\n  \"ownerqueuename\": null,\n  \"ownerusername\": null,\n  \"assignedqueuename\": null,\n  \"assignedusername\": null,\n  \"servicename\": \"some servive\",\n  \"ciname\": \"some ci\",\n  \"severity\": 1,\n  \"impact\": 2,\n  \"isactive\": true,\n  \"updatedAt\": \"2016-10-29T04:21:06.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400501",
            "description": "<p>The requested template doesn't exist</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400501",
          "content": "{\n  \"code\": 400501\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Template",
    "name": "listTemplate",
    "type": "post",
    "url": "/template/list",
    "title": "List",
    "description": "<p>This function lists all the templates in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV\nXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2\n91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R\npdmUiOnRydWV9LCJpYXQiOjE0Nzc1NzQ5NTQsImV4cCI6MTQ3NzU5NjU1NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi\nOiJ3d3cucXVldWVsb2FkLmNvbSJ9.Nmadtgu6EXBxMJMc8ZcmNvo92V2aqATW5ZcU4IiecTo\" -d\n'{\n  \"type\" : 1\n}' \"http://localhost:1337/template/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "templates",
            "description": "<p>Array of templates</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"lasteditorid\": 1,\n    \"templateid\": 1,\n    \"templatename\": \"test inc templt1\",\n    \"title\": \"title of the inc\",\n    \"description\": \"description of the inc\",\n    \"servicename\": \"some service\",\n    \"ciname\": \"some ci\",\n    \"severity\": 1,\n    \"impact\": 2,\n    \"isactive\": true,\n    \"updatedAt\": \"2016-10-27T13:29:26.000Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Template",
    "name": "templateSearch",
    "type": "post",
    "url": "/template/search",
    "title": "Search",
    "description": "<p>On input of a valid search parameters this function returns the selected attributes in the chosen sorting method</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZS\nI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc\n3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY291bnRyeWNvZGUiOiIrOTEiLCJwaG9u\nZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3RpdmUiOnRydWV9LCJpYXQiOjE0Nzc\n1NTE1NDQsImV4cCI6MTQ3NzU3MzE0NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3MiOiJ3d3cucXVldWVsb2FkLmNvbS\nJ9.tFwQlQZ8CTABuxEas87c5uzOCgJH9-6tGh10nBAbP6E\" -H \"Content-Type: application/json\" -d\n'{\n   \"type\" : 1,\n   \"search\":  { \"templateid\" : 1 },\n   \"result\" : [ \"templateid\", \"lasteditorid\", \"isactive\"],\n   \"orderby\": \"templateid\"\n}' \"http://localhost:1337/template/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched CI</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n   \"type\" : 1,\n   \"search\":  { \"templateid\" : 1 },\n   \"result\" : [ \"templateid\", \"lasteditorid\", \"isactive\"],\n   \"orderby\": \"templateid\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "templateList",
            "description": "<p>List of templates</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  1,\n  [\n    {\n      \"lasteditorid\": 1,\n      \"templateid\": 1,\n      \"isactive\": true\n    }\n  ]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Template",
    "name": "updateTemplate",
    "type": "post",
    "url": "/template/update",
    "title": "Update",
    "description": "<p>This function updates attributes of a template</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV\nCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6bnVsbCwidXNlcmlkIjoxLCJmaXJzdG5hbWUiOiJTdXBlciIsImxhc3RuYW1lIjoiV\nXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb21haW4uY29tIiwiY2\n91bnRyeWNvZGUiOiIrOTEiLCJwaG9uZW51bWJlciI6Ijk5OTk5OTk5OTkiLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXNhY3R\npdmUiOnRydWV9LCJpYXQiOjE0Nzc1NTE1NDQsImV4cCI6MTQ3NzU3MzE0NCwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3Mi\nOiJ3d3cucXVldWVsb2FkLmNvbSJ9.tFwQlQZ8CTABuxEas87c5uzOCgJH9-6tGh10nBAbP6E\" -d\n  '{\n    \"type\" : 1,\n    \"templateid\" : 7,\n    \"ownerqueuename\" : \"pen-making\",\n    \"ownerusername\" : \"ek.worker\",\n    \"assignedqueuename\" : \"refill-making\",\n    \"assignedusername\" : \"aurek.worker\"\n  }' \"http://localhost:1337/template/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "templateid",
            "description": "<p>ID of the template being modified</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the ticket template being created. E.g. Incident: 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>Title of the ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "description": "<p>Description of the ticket</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "servicename",
            "description": "<p>Name of the affected service</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "ciname",
            "description": "<p>Name of the affected configuration item</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ownerqueuename",
            "description": "<p>Name of the owner queue</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "ownerusername",
            "description": "<p>Name of the owner user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assignedqueuename",
            "description": "<p>Name of the assigned queue</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "assignedusername",
            "description": "<p>Name of the assgined user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "severity",
            "description": "<p>Severity of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "impact",
            "description": "<p>Impact of the incident</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": true,
            "field": "attachments",
            "description": "<p>Attachments</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isactive",
            "defaultValue": "true",
            "description": "<p>Visibility status of the template</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"type\" : 1,\n  \"templateid\" : 7,\n  \"ownerqueuename\" : \"pen-making\",\n  \"ownerusername\" : \"ek.worker\",\n  \"assignedqueuename\" : \"refill-making\",\n  \"assignedusername\" : \"aurek.worker\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "template",
            "description": "<p>Details of the updated template</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"templateid\": 7,\n  \"templatename\": \"test inc templt5\",\n  \"lasteditorid\": 1,\n  \"title\": \"title of the inc\",\n  \"description\": \"description of the inc\",\n  \"ownerqueuename\" : \"pen-making\",\n  \"ownerusername\" : \"ek.worker\",\n  \"assignedqueuename\" : \"refill-making\",\n  \"assignedusername\" : \"aurek.worker\",\n  \"servicename\": \"some servive\",\n  \"ciname\": \"some ci\",\n  \"severity\": 1,\n  \"impact\": 2,\n  \"isactive\": true,\n  \"updatedAt\": \"2016-10-29T04:25:56.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/TemplateController.js",
    "groupTitle": "Template",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400501",
            "description": "<p>The requested template doesn't exist</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "type": "String",
            "optional": false,
            "field": "500001",
            "description": "<p>System Error. Please contact your administrator</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400501",
          "content": "{\n  \"code\": 400501\n}",
          "type": "json"
        },
        {
          "title": "500001",
          "content": "{\n  \"code\": 500001\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/activation",
    "title": "Activation",
    "name": "activationUser",
    "group": "User",
    "description": "<p>Checks the validity and active status of the userids supplied. If the states are as expected the API proceeds to make changes to the users.</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n  \"userids\": [ 2],\n  \"action\": true\n }' \"http://localhost:1337/user/activation\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "userids",
            "description": "<p>array of userids to activate/deactivate</p>"
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "action",
            "description": "<p>true to activate and false to deactivate</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"userids\": [ 2 ],\n  \"action\": true\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "updated",
            "description": "<p>users result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"userid\": 2,\n    \"firstname\": \"betatester\",\n    \"lastname\": \"rat\",\n    \"username\": \"T3st.cat2\",\n    \"emailid\": \"tester2.rat@testclient3.com\",\n    \"countrycode\": \"+92\",\n    \"phonenumber\": \"9822387102\",\n    \"isactive\": true,\n    \"passexpired\": null,\n    \"defaultqueue\": null\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400104",
            "description": "<p>One or more of the entered users do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400102",
            "description": "<p>One or more of the entered users are active</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400103",
            "description": "<p>One or more of the entered users are inactive</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400107",
            "description": "<p>One of the users is an owner of an active Chargeback code</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400109",
            "description": "<p>One of the users is a manager of an active queue</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400111",
            "description": "<p>One or more users have open incidents</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400158",
            "description": "<p>An Active Business Department must have atleast one active admin</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400258",
            "description": "<p>One or more of the entered users is not a member of the queue</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400260",
            "description": "<p>There must be atleast one active member in the queue</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "400102",
          "content": "{\n  \"code\": 400102\n}",
          "type": "json"
        },
        {
          "title": "400103",
          "content": "{\n  \"code\": 400103\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "400107",
          "content": "{\n  \"code\": 400107\n}",
          "type": "json"
        },
        {
          "title": "400109",
          "content": "{\n  \"code\": 400109\n}",
          "type": "json"
        },
        {
          "title": "400111",
          "content": "{\n  \"code\": 400111\n}",
          "type": "json"
        },
        {
          "title": "400158",
          "content": "{\n  \"code\": 400158\n}",
          "type": "json"
        },
        {
          "title": "400258",
          "content": "{\n  \"code\": 400258\n}",
          "type": "json"
        },
        {
          "title": "400260",
          "content": "{\n  \"code\": 400260\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/add",
    "title": "Add",
    "name": "addUser",
    "group": "User",
    "description": "<p>On input of all valid parameters this function will add one user to the system</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n  \"firstname\": \"Test\",\n  \"lastname\": \"User\",\n  \"username\": \"T3st.User11\",\n  \"password\": \"P@5#word\",\n  \"emailid\": \"test.user11@testclient1.com\",\n  \"countrycode\": \"+1\",\n  \"phonenumber\": \"9999999999\",\n }' \"http://localhost:1337/user/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>First name of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Last name of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>username of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "emailid",
            "description": "<p>email of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "countrycode",
            "description": "<p>countrycode of the phone number of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phonenumber",
            "description": "<p>phone number of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"firstname\": \"Test\",\n  \"lastname\": \"User\",\n  \"username\": \"T3st.User11\",\n  \"password\": \"P@5#word\",\n  \"emailid\": \"test.user11@testclient1.com\",\n  \"countrycode\": \"+1\",\n  \"phonenumber\": \"9999999999\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "user",
            "description": "<p>user added to the system</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "{\n  \"userid\": 2,\n  \"firstname\": \"Test\",\n  \"lastname\": \"User\",\n  \"username\": \"T3st.User11\",\n  \"emailid\": \"test.user11@testclient1.com\",\n  \"countrycode\": \"+1\",\n  \"phonenumber\": \"9999999999\",\n  \"isactive\": true,\n  \"passexpired\": false,\n  \"defaultqueue\": null,\n  \"accountid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/getconfig",
    "title": "GetConfig",
    "name": "getconfig",
    "group": "User",
    "description": "<p>This gets user and global configuration settings</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6\nbnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjEsImZ1bGxuYW1lIjpudWxsLCJmaXJzdG5hbWUiOiJTdXBlciIsI\nmxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb2\n1haW4uY29tIiwiY291bnRyeWNvZGUiOm51bGwsInBob25lbnVtYmVyIjpudWxsLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXN\nhY3RpdmUiOnRydWUsInBhc3NleHBpcmVkIjpmYWxzZSwidXNlcmNvbmZpZyI6eyJ0aW1lem9uZSI6IklTVCIsImRlZmF1bHR2aWV3\nIjoiYXBwcm92YWwifX0sImlhdCI6MTQ5MDc3NzA4NywiZXhwIjoxNDkwNzk4Njg3LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsI\nmlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.CBN4V1XpUGyzuZJR6WFo1LSAsbJkGUn0udDcQBI1LbY\"\n-H \"Content-Type: application/json\" -d\n'{\n   \"configs\" : [\"defaultview\"]\n }' \"http://localhost:1337/user/getconfig\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "config",
            "description": "<p>Array of configurations</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"configs\": [\"defaultview\"]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "updated",
            "description": "<p>users result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  \"config\": \"defaultview\",\n  \"value\": \"approval\",\n  \"mode\": 2\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400112",
            "description": "<p>Requested configuration doesn't exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400112",
          "content": "{\n  \"code\": 400112\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/getdetail",
    "title": "GetDetail",
    "name": "getdetailUser",
    "group": "User",
    "description": "<p>Fetches the details of the userids supplied. Max 10 userids allowed.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n  \"userids\": [3]\n }' \"http://localhost:1337/user/getdetail\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userids",
            "description": "<p>array of user IDs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"userids\": [3]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "user",
            "description": "<p>array of user details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"administeredbds\": [],\n    \"ownedcbs\": [\n      {\n        \"cbid\": 1,\n        \"bdid\": 1,\n        \"isactive\": true,\n        \"cbownerid\": 3,\n        \"isfrozen\": false,\n        \"freezecause\": 0\n      }\n    ],\n    \"ownedqueues\": [\n      {\n        \"queueid\": 2,\n        \"queuename\": \"secondQueue\",\n        \"cbid\": 1,\n        \"queueemail\": \"secondqueue@queueload.com\",\n        \"queueownerid\": 3,\n        \"incidentmanagerid\": 3,\n        \"changemanagerid\": 3,\n        \"srtmanagerid\": 3,\n        \"alertl1\": \"secondqueue@queueload.com\",\n        \"alertl2\": \"secondqueue@queueload.com\",\n        \"alertl3\": \"secondqueue@queueload.com\",\n        \"isactive\": false,\n        \"isfrozen\": false,\n        \"freezecause\": 0\n      }\n    ],\n    \"queuesenrolled\": [],\n    \"servicestateapprovalids\": [],\n    \"roles\": [\n      {\n        \"userid\": 3,\n        \"issuperuser\": false,\n        \"isbdadmin\": false,\n        \"isoperator\": false\n      }\n    ],\n    \"defaultqueue\": null,\n    \"accountid\": {\n      \"accountid\": 1,\n      \"name\": \"Wayne Enterprise\",\n      \"address\": \"Gotham\",\n      \"supportmgr\": \"support@queueload.com\",\n      \"isactive\": true,\n      \"accntadmin\": 1\n    },\n    \"userid\": 3,\n    \"firstname\": \"firstCB\",\n    \"lastname\": \"owner\",\n    \"username\": \"firstcbowner\",\n    \"emailid\": \"firstcbowner@queueload.com\",\n    \"countrycode\": \"\",\n    \"phonenumber\": \"\",\n    \"isactive\": true,\n    \"passexpired\": false\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400052",
            "description": "<p>No Valid fields submitted for update.</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400104",
            "description": "<p>One or more of the entered users do not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400052",
          "content": "{\n \"code\": 400052\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/getstatus",
    "title": "GetStatus",
    "name": "getstatusUser",
    "group": "User",
    "description": "<p>Gets the Status of the user ids supplied.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n  \"userids\": [ 2, 3 ],\n }' \"http://localhost:1337/user/getstatus\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "userids",
            "description": "<p>array of userids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"userids\": [ 2, 3 ],\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "updated",
            "description": "<p>users result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"userid\": 2,\n    \"status\": false\n  },\n  {\n    \"userid\": 3,\n    \"status\": false\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400104",
            "description": "<p>One or more of the entered users do not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/list",
    "title": "List",
    "name": "listUser",
    "group": "User",
    "description": "<p>On input of all valid parameters this function will list all users in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n   \"page\" : 1,\n   \"limit\" : 100\n }' \"http://localhost:1337/user/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>Array of users</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400101",
            "description": "<p>No users available in the system</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400101",
          "content": "{\n  \"code\": 400101\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/makesuperuser",
    "title": "MakeSuperUser",
    "name": "makesuperuser",
    "group": "User",
    "description": "<p>This add or removes the superuser role to an user</p>",
    "permission": [
      {
        "name": "superuser",
        "title": "Superuser access rights needed.",
        "description": "<p>This is a cross business department action and requires superuser access rights.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n  \"userid\" : 2,\n  \"action\" : true\n }' \"http://localhost:1337/user/makesuperuser\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "userid",
            "description": "<p>user id of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "action",
            "description": "<p>true to activate and false to deactivate</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"userid\" : 2,\n  \"action\" : true\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "updated",
            "description": "<p>users result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"id\": 2,\n    \"userid\": 2,\n    \"issuperuser\": true,\n    \"isbdadmin\": true,\n    \"isoperator\": false,\n    \"isuser\": true,\n    \"createdAt\": \"2016-08-26T08:55:52.000Z\",\n    \"updatedAt\": \"2016-08-26T14:05:15.000Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400104",
            "description": "<p>One or more of the entered users do not exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/resetconfig",
    "title": "ResetConfig",
    "name": "resetconfig",
    "group": "User",
    "description": "<p>This removes user configuration settings</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6\nbnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjEsImZ1bGxuYW1lIjpudWxsLCJmaXJzdG5hbWUiOiJTdXBlciIsI\nmxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb2\n1haW4uY29tIiwiY291bnRyeWNvZGUiOm51bGwsInBob25lbnVtYmVyIjpudWxsLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXN\nhY3RpdmUiOnRydWUsInBhc3NleHBpcmVkIjpmYWxzZSwidXNlcmNvbmZpZyI6eyJ0aW1lem9uZSI6IklTVCIsImRlZmF1bHR2aWV3\nIjoiYXBwcm92YWwifX0sImlhdCI6MTQ5MDc3NzA4NywiZXhwIjoxNDkwNzk4Njg3LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsI\nmlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.CBN4V1XpUGyzuZJR6WFo1LSAsbJkGUn0udDcQBI1LbY\"\n-H \"Content-Type: application/json\" -d\n'{\n   \"configs\" : [\"defaultview\"]\n }' \"http://localhost:1337/user/resetconfig\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "config",
            "description": "<p>Array of configurations</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"configs\": [\"defaultview\"]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "optional": false,
            "field": "No",
            "description": "<p>return on sucess</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400112",
            "description": "<p>Requested configuration doesn't exist</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400112",
          "content": "{\n  \"code\": 400112\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/search",
    "title": "Search",
    "name": "searchUser",
    "group": "User",
    "description": "<p>searches the system and returns the result. The number of search column and order by criteria are flexible.</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n   \"search\":  { \"username\": \"T3st.User1%\" },\n   \"result\" : [ \"firstname\", \"lastname\", \"username\", \"emailid\"],\n   \"orderby\": \"firstname\"\n }' \"http://localhost:1337/user/search\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "search",
            "description": "<p>Object listing the attrbutes of the searched user</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "result",
            "description": "<p>Array of attributes requested</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "orderby",
            "description": "<p>Attribute to order by the results</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n \"search\":  { \"username\": \"T3st.User1%\" },\n \"result\" : [ \"firstname\", \"lastname\", \"username\", \"emailid\"],\n \"orderby\": \"firstname\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "search",
            "description": "<p>result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n49,\n[\n  {\n    \"firstname\": \"Test\",\n    \"lastname\": \"User\",\n    \"username\": \"T3st.User1\",\n    \"emailid\": \"test.user1@betaclient.com\"\n  },\n  {\n    \"firstname\": \"Test\",\n    \"lastname\": \"User\",\n    \"username\": \"T3st.User10\",\n    \"emailid\": \"test.user10@betaclient.com\"\n  },\n  ...\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400053",
            "description": "<p>One or more entered columns in the search parameters do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400258",
            "description": "<p>One or more of the entered users is not a member of the queue</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400053",
          "content": "{\n  \"code\": 400053\n}",
          "type": "json"
        },
        {
          "title": "400258",
          "content": "{\n  \"code\": 400258\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/setconfig",
    "title": "SetConfig",
    "name": "setconfig",
    "group": "User",
    "description": "<p>This sets user and global configuration settings</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImRlZmF1bHRxdWV1ZSI6\nbnVsbCwiYWNjb3VudGlkIjoicWxvZHRlc3QiLCJ1c2VyaWQiOjEsImZ1bGxuYW1lIjpudWxsLCJmaXJzdG5hbWUiOiJTdXBlciIsI\nmxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsaWQiOiJhZG1pbmlzdHJhdG9yQHRlc3Rkb2\n1haW4uY29tIiwiY291bnRyeWNvZGUiOm51bGwsInBob25lbnVtYmVyIjpudWxsLCJlbXBsb3llZWlkIjoiRlRFMDAwMDAxIiwiaXN\nhY3RpdmUiOnRydWUsInBhc3NleHBpcmVkIjpmYWxzZSwidXNlcmNvbmZpZyI6bnVsbH0sImlhdCI6MTQ5MDc1OTQ1OSwiZXhwIjox\nNDkwNzgxMDU5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.iR1BcWQXV0CUCgrvg\napd20kTvysQSFJ3Tt7ht7xMhKE\" -H \"Content-Type: application/json\" -d\n'{\n   \"settings\" :\n   [{\n     \"config\" : \"tz\",\n     \"value\" : \"IST\",\n     \"mode\" : 2\n   }]\n }' \"http://localhost:1337/user/setconfig\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "config",
            "description": "<p>Name of the cofiguration</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "value",
            "description": "<p>Value of the config</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "allowedValues": [
              "1",
              "2"
            ],
            "optional": false,
            "field": "mode",
            "description": "<p>Settings type being changed</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"settings\" :\n  [{\n    \"config\" : \"tz\",\n    \"value\" : \"IST\",\n    \"mode\" : 2\n  }]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user/update",
    "title": "Update",
    "name": "updateUser",
    "group": "User",
    "description": "<p>On input of valid parameters this function will update the fields of a user</p>",
    "permission": [
      {
        "name": "bdadmin",
        "title": "Business Department administrator access rights needed.",
        "description": "<p>This action is allowed for all active bdadmins in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZ\nmlyc3RuYW1lIjoiVGVzdCIsImxhc3RuYW1lIjoiVXNlciIsInVzZXJuYW1lIjoiVDNzdC5Vc2VyMTEiLCJlbWFpbGlkIjoidGV\nzdC51c2VyMTFAdGVzdGNsaWVudDEuY29tIiwiY291bnRyeWNvZGUiOiIrMSIsInBob25lbnVtYmVyIjoiOTk5OTk5OTk5OSIsI\nmVtcGxveWVlaWQiOiJGVEUwMDAwMDIiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3Njc4ODY0NSwiZXhwIjoxNDc2ODEwMjQ\n1LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0.AZApoP31yRW4wydCYblVsqIvW\nW6z9ZSV89AUWUFZG_0\" -H \"Content-Type: application/json\" -d\n'{\n  \"userid\": 5,\n  \"emailid\": \"test.user11@testclient1.com\",\n}' \"http://localhost:1337/user/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userid",
            "description": "<p>user ID of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "emailid",
            "description": "<p>email of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "countrycode",
            "description": "<p>countrycode of the phone number of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "phonenumber",
            "description": "<p>phone number of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "defaultqueue",
            "description": "<p>Queue ID of the default queue for the user</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "passexpired",
            "description": "<p>force user to change password on next login</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "oldpass",
            "description": "<p>Old password of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "newpass",
            "description": "<p>New password of the user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n  \"userid\": 5,\n  \"emailid\": \"test.user11@testclient1.com\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "user",
            "description": "<p>updated user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[{\n  defaultqueue: null,\n  userid: 5,\n  firstname: 'Super',\n  lastname: 'User',\n  username: 'administrator',\n  emailid: 'test.user11@testclient1.com',\n  countrycode: null,\n  phonenumber: null,\n  isactive: true\n}]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/UserController.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400052",
            "description": "<p>No Valid fields submitted for update.</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400104",
            "description": "<p>One or more of the entered users do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400258",
            "description": "<p>One or more of the entered users is not a member of the queue</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400052",
          "content": "{\n \"code\": 400052\n}",
          "type": "json"
        },
        {
          "title": "400104",
          "content": "{\n  \"code\": 400104\n}",
          "type": "json"
        },
        {
          "title": "400258",
          "content": "{\n  \"code\": 400258\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Vendor",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/VendorController.js",
    "groupTitle": "Vendor",
    "name": ""
  },
  {
    "type": "post",
    "url": "/vendor/add",
    "title": "Add",
    "name": "addVendors",
    "group": "Vendor",
    "description": "<p>On input of all valid parameters this function will add vendors to the system</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU\n1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0\n.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYEJdx6oNW4\" -d\n '{\"vendornames\": [\"IBM\", \"Quantum\", \"Hewlett Packard\"]}'\n \"http://localhost:1337/vendor/add\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "vendornames",
            "description": "<p>Array of vendor names</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{\n   \"vendornames\": [\"IBM\", \"Quantum\", \"Hewlett Packard\"]\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "vendors",
            "description": "<p>Array of Vendors</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"vendorid\": 1,\n    \"vendorname\": \"IBM\"\n  },\n  {\n    \"vendorid\": 2,\n    \"vendorname\": \"Quantum\"\n  },\n  {\n    \"vendorid\": 3,\n    \"vendorname\": \"Hewlett Packard\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/VendorController.js",
    "groupTitle": "Vendor",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400333",
            "description": "<p>One or more vendors entered already exist in the system</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400333",
          "content": "{\n  \"code\": 400333\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "Vendor",
    "name": "listVendors",
    "type": "post",
    "url": "/vendor/list",
    "title": "List",
    "description": "<p>This API lists the vendors in the system</p>",
    "permission": [
      {
        "name": "user",
        "title": "User access rights needed.",
        "description": "<p>This action is allowed for all active users in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MTYsImZpcnN0bmFtZSI6Ik1hcmNvIiwibGFzdG5hbWUiOiJQb2xvIiwidXNlcm5hbWUiOiJ\ntYXJjby5wb2xvIiwiZW1haWxpZCI6Im1hcmNvLnBvbG9AdGVzdGNsaWVudC5jb20iLCJjb3VudHJ5Y29kZSI6Iis5MSIsInBob25\nlbnVtYmVyIjoiOTk5OTk5OTk5OSIsImVtcGxveWVlaWQiOiJGVEU4NzYyMzg5IiwiaXNhY3RpdmUiOnRydWV9LCJpYXQiOjE0NzI\nxMzExNDYsImV4cCI6MTQ3MjE1Mjc0NiwiYXVkIjoid3d3LnF1ZXVlbG9hZC5jb20iLCJpc3MiOiJ3d3cucXVldWVsb2FkLmNvbSJ\n9.Hq0pyeg3CxoJTaBDhHCQ26blL98n_Ac17ep2vixJmmc\" -d '{\"page\" : 1,\"limit\" : 100}'\n\"http://localhost:1337/vendor/list\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>Maximum number of results to show in a page</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>The number of page to show</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "{ \"limit\": 10, \"page\": 1}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "vendors",
            "description": "<p>Array of Vendors</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  {\n    \"vendorid\": 1,\n    \"vendorname\": \"IBM\",\n    \"numberofcis\": 4\n  },\n  {\n    \"vendorid\": 2,\n    \"vendorname\": \"Quantum\",\n    \"numberofcis\": 0\n  },\n  {\n    \"vendorid\": 3,\n    \"vendorname\": \"Hewlett Packard\",\n    \"numberofcis\": 0\n  },\n  {\n    \"vendorid\": 4,\n    \"vendorname\": \"Keystashio\",\n    \"numberofcis\": 0\n  }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/VendorController.js",
    "groupTitle": "Vendor",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400331",
            "description": "<p>No vendors available in the system</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400331",
          "content": "{\n  \"code\": 400331\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/vendor/update",
    "title": "Update",
    "name": "updateVendors",
    "group": "Vendor",
    "description": "<p>Updates a vendor name.</p>",
    "permission": [
      {
        "name": "operator",
        "title": "Operator access rights needed.",
        "description": "<p>This action is allowed for all active operators in the system.</p>"
      }
    ],
    "header": {
      "fields": {
        "Content-Type": [
          {
            "group": "Content-Type",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          }
        ],
        "Authorization": [
          {
            "group": "Authorization",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>unique access-key of the requester</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example Usage:",
        "content": "curl -X POST -H \"Content-Type: application/json\" -H \"Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX\nVCJ9.eyJ1c2VyIjp7InVzZXJpZCI6MiwiZmlyc3RuYW1lIjoiTWFyY28iLCJsYXN0bmFtZSI6IlBvbG8iLCJ1c2VybmFtZSI6Im1\nhcmNvLnBvbG8iLCJlbWFpbGlkIjoibWFyY28ucG9sb0B0ZXN0Y2xpZW50LmNvbSIsImNvdW50cnljb2RlIjoiKzkxIiwicGhvbmV\nudW1iZXIiOiI5OTk5OTk5OTk5IiwiZW1wbG95ZWVpZCI6IkZURTg3NjIzODkiLCJpc2FjdGl2ZSI6dHJ1ZX0sImlhdCI6MTQ3MjU\n1Nzk4OSwiZXhwIjoxNDcyNTc5NTg5LCJhdWQiOiJ3d3cucXVldWVsb2FkLmNvbSIsImlzcyI6Ind3dy5xdWV1ZWxvYWQuY29tIn0\n.jWHq99Oj_SDGTB0Blf0kp69rCpRbwy6WlYEJdx6oNW4\" -d\n   '[{\"vendorid\": 2, \"vendorname\": \"Stark Enterprises\"},\n     {\"vendorid\": 1, \"vendorname\": \"Cyberdyne Systems\"}]'\n \"http://localhost:1337/vendor/update\"",
        "type": "curl"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "vendors",
            "description": "<p>Array of vendor id:name pairs</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example",
          "content": "[{\n  \"vendorid\": 2,\n  \"vendorname\": \"Stark Enterprises\"\n}, {\n  \"vendorid\": 1,\n  \"vendorname\": \"Cyberdyne Systems\"\n}]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "vendors",
            "description": "<p>Array of Vendors</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "[\n  [{\n    \"vendorid\": 2,\n    \"vendorname\": \"Stark Enterprises\"\n  }],\n  [{\n    \"vendorid\": 1,\n    \"vendorname\": \"Cyberdyne Systems\"\n  }]\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "/home/smruti/Dropbox/queueload-0.1/lib/queueload/api/controllers/VendorController.js",
    "groupTitle": "Vendor",
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400051",
            "description": "<p>Incorrect Parameters. Correct and try again</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400332",
            "description": "<p>One or more of entered vendors do not exist</p>"
          },
          {
            "group": "400",
            "type": "String",
            "optional": false,
            "field": "400333",
            "description": "<p>One or more vendors entered already exist in the system</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "type": "String",
            "optional": false,
            "field": "403502",
            "description": "<p>The requester doesn't have the required privilege</p>"
          }
        ],
        "": [
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401",
            "description": "<p>Unauthorized user</p>"
          },
          {
            "group": "401",
            "type": "String",
            "optional": false,
            "field": "401505",
            "description": "<p>Token is expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "401",
          "content": "{\n \"code\": 401\n }",
          "type": "json"
        },
        {
          "title": "401505",
          "content": "{\n \"code\": 401505\n}",
          "type": "json"
        },
        {
          "title": "400051",
          "content": "{\n \"code\": 400051\n}",
          "type": "json"
        },
        {
          "title": "400332",
          "content": "{\n  \"code\": 400332\n}",
          "type": "json"
        },
        {
          "title": "400333",
          "content": "{\n  \"code\": 400333\n}",
          "type": "json"
        },
        {
          "title": "403502",
          "content": "{\n  \"code\": 403502\n}",
          "type": "json"
        }
      ]
    }
  }
] });
