PLEASE CONTACT IF YOU WOULD LIKE TO MAINTAIN THIS PROJECT

QueueLoad is an attempt to simplify IT Service Management by brining intelligence to IT operations. We strive to reduce the daily fatigue due to the avalanche of alerts operations has to deal with by tying multiple incidents into single actionable tasks. We also attempt to reduce the time lost in paper work.

The software will be built-up from ground as a Software-as-a-Service(SaaS) in mind.

For details please refer to docs/SRS and docs/test.